/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/acorn/dist/acorn.js":
/*!******************************************!*\
  !*** ./node_modules/acorn/dist/acorn.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function (global, factory) {
   true ? factory(exports) :
  undefined;
}(this, function (exports) { 'use strict';

  // Reserved word lists for various dialects of the language

  var reservedWords = {
    3: "abstract boolean byte char class double enum export extends final float goto implements import int interface long native package private protected public short static super synchronized throws transient volatile",
    5: "class enum extends super const export import",
    6: "enum",
    7: "enum",
    strict: "implements interface let package private protected public static yield",
    strictBind: "eval arguments"
  }

  // And the keywords

  var ecma5AndLessKeywords = "break case catch continue debugger default do else finally for function if return switch throw try var while with null true false instanceof typeof void delete new in this"

  var keywords = {
    5: ecma5AndLessKeywords,
    6: ecma5AndLessKeywords + " const class extends export import super"
  }

  // ## Character categories

  // Big ugly regular expressions that match characters in the
  // whitespace, identifier, and identifier-start categories. These
  // are only applied when a character is found to actually have a
  // code point above 128.
  // Generated by `bin/generate-identifier-regex.js`.

  var nonASCIIidentifierStartChars = "\xaa\xb5\xba\xc0-\xd6\xd8-\xf6\xf8-\u02c1\u02c6-\u02d1\u02e0-\u02e4\u02ec\u02ee\u0370-\u0374\u0376\u0377\u037a-\u037d\u037f\u0386\u0388-\u038a\u038c\u038e-\u03a1\u03a3-\u03f5\u03f7-\u0481\u048a-\u052f\u0531-\u0556\u0559\u0561-\u0587\u05d0-\u05ea\u05f0-\u05f2\u0620-\u064a\u066e\u066f\u0671-\u06d3\u06d5\u06e5\u06e6\u06ee\u06ef\u06fa-\u06fc\u06ff\u0710\u0712-\u072f\u074d-\u07a5\u07b1\u07ca-\u07ea\u07f4\u07f5\u07fa\u0800-\u0815\u081a\u0824\u0828\u0840-\u0858\u08a0-\u08b4\u08b6-\u08bd\u0904-\u0939\u093d\u0950\u0958-\u0961\u0971-\u0980\u0985-\u098c\u098f\u0990\u0993-\u09a8\u09aa-\u09b0\u09b2\u09b6-\u09b9\u09bd\u09ce\u09dc\u09dd\u09df-\u09e1\u09f0\u09f1\u0a05-\u0a0a\u0a0f\u0a10\u0a13-\u0a28\u0a2a-\u0a30\u0a32\u0a33\u0a35\u0a36\u0a38\u0a39\u0a59-\u0a5c\u0a5e\u0a72-\u0a74\u0a85-\u0a8d\u0a8f-\u0a91\u0a93-\u0aa8\u0aaa-\u0ab0\u0ab2\u0ab3\u0ab5-\u0ab9\u0abd\u0ad0\u0ae0\u0ae1\u0af9\u0b05-\u0b0c\u0b0f\u0b10\u0b13-\u0b28\u0b2a-\u0b30\u0b32\u0b33\u0b35-\u0b39\u0b3d\u0b5c\u0b5d\u0b5f-\u0b61\u0b71\u0b83\u0b85-\u0b8a\u0b8e-\u0b90\u0b92-\u0b95\u0b99\u0b9a\u0b9c\u0b9e\u0b9f\u0ba3\u0ba4\u0ba8-\u0baa\u0bae-\u0bb9\u0bd0\u0c05-\u0c0c\u0c0e-\u0c10\u0c12-\u0c28\u0c2a-\u0c39\u0c3d\u0c58-\u0c5a\u0c60\u0c61\u0c80\u0c85-\u0c8c\u0c8e-\u0c90\u0c92-\u0ca8\u0caa-\u0cb3\u0cb5-\u0cb9\u0cbd\u0cde\u0ce0\u0ce1\u0cf1\u0cf2\u0d05-\u0d0c\u0d0e-\u0d10\u0d12-\u0d3a\u0d3d\u0d4e\u0d54-\u0d56\u0d5f-\u0d61\u0d7a-\u0d7f\u0d85-\u0d96\u0d9a-\u0db1\u0db3-\u0dbb\u0dbd\u0dc0-\u0dc6\u0e01-\u0e30\u0e32\u0e33\u0e40-\u0e46\u0e81\u0e82\u0e84\u0e87\u0e88\u0e8a\u0e8d\u0e94-\u0e97\u0e99-\u0e9f\u0ea1-\u0ea3\u0ea5\u0ea7\u0eaa\u0eab\u0ead-\u0eb0\u0eb2\u0eb3\u0ebd\u0ec0-\u0ec4\u0ec6\u0edc-\u0edf\u0f00\u0f40-\u0f47\u0f49-\u0f6c\u0f88-\u0f8c\u1000-\u102a\u103f\u1050-\u1055\u105a-\u105d\u1061\u1065\u1066\u106e-\u1070\u1075-\u1081\u108e\u10a0-\u10c5\u10c7\u10cd\u10d0-\u10fa\u10fc-\u1248\u124a-\u124d\u1250-\u1256\u1258\u125a-\u125d\u1260-\u1288\u128a-\u128d\u1290-\u12b0\u12b2-\u12b5\u12b8-\u12be\u12c0\u12c2-\u12c5\u12c8-\u12d6\u12d8-\u1310\u1312-\u1315\u1318-\u135a\u1380-\u138f\u13a0-\u13f5\u13f8-\u13fd\u1401-\u166c\u166f-\u167f\u1681-\u169a\u16a0-\u16ea\u16ee-\u16f8\u1700-\u170c\u170e-\u1711\u1720-\u1731\u1740-\u1751\u1760-\u176c\u176e-\u1770\u1780-\u17b3\u17d7\u17dc\u1820-\u1877\u1880-\u18a8\u18aa\u18b0-\u18f5\u1900-\u191e\u1950-\u196d\u1970-\u1974\u1980-\u19ab\u19b0-\u19c9\u1a00-\u1a16\u1a20-\u1a54\u1aa7\u1b05-\u1b33\u1b45-\u1b4b\u1b83-\u1ba0\u1bae\u1baf\u1bba-\u1be5\u1c00-\u1c23\u1c4d-\u1c4f\u1c5a-\u1c7d\u1c80-\u1c88\u1ce9-\u1cec\u1cee-\u1cf1\u1cf5\u1cf6\u1d00-\u1dbf\u1e00-\u1f15\u1f18-\u1f1d\u1f20-\u1f45\u1f48-\u1f4d\u1f50-\u1f57\u1f59\u1f5b\u1f5d\u1f5f-\u1f7d\u1f80-\u1fb4\u1fb6-\u1fbc\u1fbe\u1fc2-\u1fc4\u1fc6-\u1fcc\u1fd0-\u1fd3\u1fd6-\u1fdb\u1fe0-\u1fec\u1ff2-\u1ff4\u1ff6-\u1ffc\u2071\u207f\u2090-\u209c\u2102\u2107\u210a-\u2113\u2115\u2118-\u211d\u2124\u2126\u2128\u212a-\u2139\u213c-\u213f\u2145-\u2149\u214e\u2160-\u2188\u2c00-\u2c2e\u2c30-\u2c5e\u2c60-\u2ce4\u2ceb-\u2cee\u2cf2\u2cf3\u2d00-\u2d25\u2d27\u2d2d\u2d30-\u2d67\u2d6f\u2d80-\u2d96\u2da0-\u2da6\u2da8-\u2dae\u2db0-\u2db6\u2db8-\u2dbe\u2dc0-\u2dc6\u2dc8-\u2dce\u2dd0-\u2dd6\u2dd8-\u2dde\u3005-\u3007\u3021-\u3029\u3031-\u3035\u3038-\u303c\u3041-\u3096\u309b-\u309f\u30a1-\u30fa\u30fc-\u30ff\u3105-\u312d\u3131-\u318e\u31a0-\u31ba\u31f0-\u31ff\u3400-\u4db5\u4e00-\u9fd5\ua000-\ua48c\ua4d0-\ua4fd\ua500-\ua60c\ua610-\ua61f\ua62a\ua62b\ua640-\ua66e\ua67f-\ua69d\ua6a0-\ua6ef\ua717-\ua71f\ua722-\ua788\ua78b-\ua7ae\ua7b0-\ua7b7\ua7f7-\ua801\ua803-\ua805\ua807-\ua80a\ua80c-\ua822\ua840-\ua873\ua882-\ua8b3\ua8f2-\ua8f7\ua8fb\ua8fd\ua90a-\ua925\ua930-\ua946\ua960-\ua97c\ua984-\ua9b2\ua9cf\ua9e0-\ua9e4\ua9e6-\ua9ef\ua9fa-\ua9fe\uaa00-\uaa28\uaa40-\uaa42\uaa44-\uaa4b\uaa60-\uaa76\uaa7a\uaa7e-\uaaaf\uaab1\uaab5\uaab6\uaab9-\uaabd\uaac0\uaac2\uaadb-\uaadd\uaae0-\uaaea\uaaf2-\uaaf4\uab01-\uab06\uab09-\uab0e\uab11-\uab16\uab20-\uab26\uab28-\uab2e\uab30-\uab5a\uab5c-\uab65\uab70-\uabe2\uac00-\ud7a3\ud7b0-\ud7c6\ud7cb-\ud7fb\uf900-\ufa6d\ufa70-\ufad9\ufb00-\ufb06\ufb13-\ufb17\ufb1d\ufb1f-\ufb28\ufb2a-\ufb36\ufb38-\ufb3c\ufb3e\ufb40\ufb41\ufb43\ufb44\ufb46-\ufbb1\ufbd3-\ufd3d\ufd50-\ufd8f\ufd92-\ufdc7\ufdf0-\ufdfb\ufe70-\ufe74\ufe76-\ufefc\uff21-\uff3a\uff41-\uff5a\uff66-\uffbe\uffc2-\uffc7\uffca-\uffcf\uffd2-\uffd7\uffda-\uffdc"
  var nonASCIIidentifierChars = "\u200c\u200d\xb7\u0300-\u036f\u0387\u0483-\u0487\u0591-\u05bd\u05bf\u05c1\u05c2\u05c4\u05c5\u05c7\u0610-\u061a\u064b-\u0669\u0670\u06d6-\u06dc\u06df-\u06e4\u06e7\u06e8\u06ea-\u06ed\u06f0-\u06f9\u0711\u0730-\u074a\u07a6-\u07b0\u07c0-\u07c9\u07eb-\u07f3\u0816-\u0819\u081b-\u0823\u0825-\u0827\u0829-\u082d\u0859-\u085b\u08d4-\u08e1\u08e3-\u0903\u093a-\u093c\u093e-\u094f\u0951-\u0957\u0962\u0963\u0966-\u096f\u0981-\u0983\u09bc\u09be-\u09c4\u09c7\u09c8\u09cb-\u09cd\u09d7\u09e2\u09e3\u09e6-\u09ef\u0a01-\u0a03\u0a3c\u0a3e-\u0a42\u0a47\u0a48\u0a4b-\u0a4d\u0a51\u0a66-\u0a71\u0a75\u0a81-\u0a83\u0abc\u0abe-\u0ac5\u0ac7-\u0ac9\u0acb-\u0acd\u0ae2\u0ae3\u0ae6-\u0aef\u0b01-\u0b03\u0b3c\u0b3e-\u0b44\u0b47\u0b48\u0b4b-\u0b4d\u0b56\u0b57\u0b62\u0b63\u0b66-\u0b6f\u0b82\u0bbe-\u0bc2\u0bc6-\u0bc8\u0bca-\u0bcd\u0bd7\u0be6-\u0bef\u0c00-\u0c03\u0c3e-\u0c44\u0c46-\u0c48\u0c4a-\u0c4d\u0c55\u0c56\u0c62\u0c63\u0c66-\u0c6f\u0c81-\u0c83\u0cbc\u0cbe-\u0cc4\u0cc6-\u0cc8\u0cca-\u0ccd\u0cd5\u0cd6\u0ce2\u0ce3\u0ce6-\u0cef\u0d01-\u0d03\u0d3e-\u0d44\u0d46-\u0d48\u0d4a-\u0d4d\u0d57\u0d62\u0d63\u0d66-\u0d6f\u0d82\u0d83\u0dca\u0dcf-\u0dd4\u0dd6\u0dd8-\u0ddf\u0de6-\u0def\u0df2\u0df3\u0e31\u0e34-\u0e3a\u0e47-\u0e4e\u0e50-\u0e59\u0eb1\u0eb4-\u0eb9\u0ebb\u0ebc\u0ec8-\u0ecd\u0ed0-\u0ed9\u0f18\u0f19\u0f20-\u0f29\u0f35\u0f37\u0f39\u0f3e\u0f3f\u0f71-\u0f84\u0f86\u0f87\u0f8d-\u0f97\u0f99-\u0fbc\u0fc6\u102b-\u103e\u1040-\u1049\u1056-\u1059\u105e-\u1060\u1062-\u1064\u1067-\u106d\u1071-\u1074\u1082-\u108d\u108f-\u109d\u135d-\u135f\u1369-\u1371\u1712-\u1714\u1732-\u1734\u1752\u1753\u1772\u1773\u17b4-\u17d3\u17dd\u17e0-\u17e9\u180b-\u180d\u1810-\u1819\u18a9\u1920-\u192b\u1930-\u193b\u1946-\u194f\u19d0-\u19da\u1a17-\u1a1b\u1a55-\u1a5e\u1a60-\u1a7c\u1a7f-\u1a89\u1a90-\u1a99\u1ab0-\u1abd\u1b00-\u1b04\u1b34-\u1b44\u1b50-\u1b59\u1b6b-\u1b73\u1b80-\u1b82\u1ba1-\u1bad\u1bb0-\u1bb9\u1be6-\u1bf3\u1c24-\u1c37\u1c40-\u1c49\u1c50-\u1c59\u1cd0-\u1cd2\u1cd4-\u1ce8\u1ced\u1cf2-\u1cf4\u1cf8\u1cf9\u1dc0-\u1df5\u1dfb-\u1dff\u203f\u2040\u2054\u20d0-\u20dc\u20e1\u20e5-\u20f0\u2cef-\u2cf1\u2d7f\u2de0-\u2dff\u302a-\u302f\u3099\u309a\ua620-\ua629\ua66f\ua674-\ua67d\ua69e\ua69f\ua6f0\ua6f1\ua802\ua806\ua80b\ua823-\ua827\ua880\ua881\ua8b4-\ua8c5\ua8d0-\ua8d9\ua8e0-\ua8f1\ua900-\ua909\ua926-\ua92d\ua947-\ua953\ua980-\ua983\ua9b3-\ua9c0\ua9d0-\ua9d9\ua9e5\ua9f0-\ua9f9\uaa29-\uaa36\uaa43\uaa4c\uaa4d\uaa50-\uaa59\uaa7b-\uaa7d\uaab0\uaab2-\uaab4\uaab7\uaab8\uaabe\uaabf\uaac1\uaaeb-\uaaef\uaaf5\uaaf6\uabe3-\uabea\uabec\uabed\uabf0-\uabf9\ufb1e\ufe00-\ufe0f\ufe20-\ufe2f\ufe33\ufe34\ufe4d-\ufe4f\uff10-\uff19\uff3f"

  var nonASCIIidentifierStart = new RegExp("[" + nonASCIIidentifierStartChars + "]")
  var nonASCIIidentifier = new RegExp("[" + nonASCIIidentifierStartChars + nonASCIIidentifierChars + "]")

  nonASCIIidentifierStartChars = nonASCIIidentifierChars = null

  // These are a run-length and offset encoded representation of the
  // >0xffff code points that are a valid part of identifiers. The
  // offset starts at 0x10000, and each pair of numbers represents an
  // offset to the next range, and then a size of the range. They were
  // generated by bin/generate-identifier-regex.js
  var astralIdentifierStartCodes = [0,11,2,25,2,18,2,1,2,14,3,13,35,122,70,52,268,28,4,48,48,31,17,26,6,37,11,29,3,35,5,7,2,4,43,157,19,35,5,35,5,39,9,51,157,310,10,21,11,7,153,5,3,0,2,43,2,1,4,0,3,22,11,22,10,30,66,18,2,1,11,21,11,25,71,55,7,1,65,0,16,3,2,2,2,26,45,28,4,28,36,7,2,27,28,53,11,21,11,18,14,17,111,72,56,50,14,50,785,52,76,44,33,24,27,35,42,34,4,0,13,47,15,3,22,0,2,0,36,17,2,24,85,6,2,0,2,3,2,14,2,9,8,46,39,7,3,1,3,21,2,6,2,1,2,4,4,0,19,0,13,4,159,52,19,3,54,47,21,1,2,0,185,46,42,3,37,47,21,0,60,42,86,25,391,63,32,0,449,56,264,8,2,36,18,0,50,29,881,921,103,110,18,195,2749,1070,4050,582,8634,568,8,30,114,29,19,47,17,3,32,20,6,18,881,68,12,0,67,12,65,0,32,6124,20,754,9486,1,3071,106,6,12,4,8,8,9,5991,84,2,70,2,1,3,0,3,1,3,3,2,11,2,0,2,6,2,64,2,3,3,7,2,6,2,27,2,3,2,4,2,0,4,6,2,339,3,24,2,24,2,30,2,24,2,30,2,24,2,30,2,24,2,30,2,24,2,7,4149,196,60,67,1213,3,2,26,2,1,2,0,3,0,2,9,2,3,2,0,2,0,7,0,5,0,2,0,2,0,2,2,2,1,2,0,3,0,2,0,2,0,2,0,2,0,2,1,2,0,3,3,2,6,2,3,2,3,2,0,2,9,2,16,6,2,2,4,2,16,4421,42710,42,4148,12,221,3,5761,10591,541]
  var astralIdentifierCodes = [509,0,227,0,150,4,294,9,1368,2,2,1,6,3,41,2,5,0,166,1,1306,2,54,14,32,9,16,3,46,10,54,9,7,2,37,13,2,9,52,0,13,2,49,13,10,2,4,9,83,11,7,0,161,11,6,9,7,3,57,0,2,6,3,1,3,2,10,0,11,1,3,6,4,4,193,17,10,9,87,19,13,9,214,6,3,8,28,1,83,16,16,9,82,12,9,9,84,14,5,9,423,9,838,7,2,7,17,9,57,21,2,13,19882,9,135,4,60,6,26,9,1016,45,17,3,19723,1,5319,4,4,5,9,7,3,6,31,3,149,2,1418,49,513,54,5,49,9,0,15,0,23,4,2,14,1361,6,2,16,3,6,2,1,2,4,2214,6,110,6,6,9,792487,239]

  // This has a complexity linear to the value of the code. The
  // assumption is that looking up astral identifier characters is
  // rare.
  function isInAstralSet(code, set) {
    var pos = 0x10000
    for (var i = 0; i < set.length; i += 2) {
      pos += set[i]
      if (pos > code) return false
      pos += set[i + 1]
      if (pos >= code) return true
    }
  }

  // Test whether a given character code starts an identifier.

  function isIdentifierStart(code, astral) {
    if (code < 65) return code === 36
    if (code < 91) return true
    if (code < 97) return code === 95
    if (code < 123) return true
    if (code <= 0xffff) return code >= 0xaa && nonASCIIidentifierStart.test(String.fromCharCode(code))
    if (astral === false) return false
    return isInAstralSet(code, astralIdentifierStartCodes)
  }

  // Test whether a given character is part of an identifier.

  function isIdentifierChar(code, astral) {
    if (code < 48) return code === 36
    if (code < 58) return true
    if (code < 65) return false
    if (code < 91) return true
    if (code < 97) return code === 95
    if (code < 123) return true
    if (code <= 0xffff) return code >= 0xaa && nonASCIIidentifier.test(String.fromCharCode(code))
    if (astral === false) return false
    return isInAstralSet(code, astralIdentifierStartCodes) || isInAstralSet(code, astralIdentifierCodes)
  }

  // ## Token types

  // The assignment of fine-grained, information-carrying type objects
  // allows the tokenizer to store the information it has about a
  // token in a way that is very cheap for the parser to look up.

  // All token type variables start with an underscore, to make them
  // easy to recognize.

  // The `beforeExpr` property is used to disambiguate between regular
  // expressions and divisions. It is set on all token types that can
  // be followed by an expression (thus, a slash after them would be a
  // regular expression).
  //
  // The `startsExpr` property is used to check if the token ends a
  // `yield` expression. It is set on all token types that either can
  // directly start an expression (like a quotation mark) or can
  // continue an expression (like the body of a string).
  //
  // `isLoop` marks a keyword as starting a loop, which is important
  // to know when parsing a label, in order to allow or disallow
  // continue jumps to that label.

  var TokenType = function TokenType(label, conf) {
    if ( conf === void 0 ) conf = {};

    this.label = label
    this.keyword = conf.keyword
    this.beforeExpr = !!conf.beforeExpr
    this.startsExpr = !!conf.startsExpr
    this.isLoop = !!conf.isLoop
    this.isAssign = !!conf.isAssign
    this.prefix = !!conf.prefix
    this.postfix = !!conf.postfix
    this.binop = conf.binop || null
    this.updateContext = null
  };

  function binop(name, prec) {
    return new TokenType(name, {beforeExpr: true, binop: prec})
  }
  var beforeExpr = {beforeExpr: true};
  var startsExpr = {startsExpr: true};
  // Map keyword names to token types.

  var keywordTypes = {}

  // Succinct definitions of keyword token types
  function kw(name, options) {
    if ( options === void 0 ) options = {};

    options.keyword = name
    return keywordTypes[name] = new TokenType(name, options)
  }

  var tt = {
    num: new TokenType("num", startsExpr),
    regexp: new TokenType("regexp", startsExpr),
    string: new TokenType("string", startsExpr),
    name: new TokenType("name", startsExpr),
    eof: new TokenType("eof"),

    // Punctuation token types.
    bracketL: new TokenType("[", {beforeExpr: true, startsExpr: true}),
    bracketR: new TokenType("]"),
    braceL: new TokenType("{", {beforeExpr: true, startsExpr: true}),
    braceR: new TokenType("}"),
    parenL: new TokenType("(", {beforeExpr: true, startsExpr: true}),
    parenR: new TokenType(")"),
    comma: new TokenType(",", beforeExpr),
    semi: new TokenType(";", beforeExpr),
    colon: new TokenType(":", beforeExpr),
    dot: new TokenType("."),
    question: new TokenType("?", beforeExpr),
    arrow: new TokenType("=>", beforeExpr),
    template: new TokenType("template"),
    ellipsis: new TokenType("...", beforeExpr),
    backQuote: new TokenType("`", startsExpr),
    dollarBraceL: new TokenType("${", {beforeExpr: true, startsExpr: true}),

    // Operators. These carry several kinds of properties to help the
    // parser use them properly (the presence of these properties is
    // what categorizes them as operators).
    //
    // `binop`, when present, specifies that this operator is a binary
    // operator, and will refer to its precedence.
    //
    // `prefix` and `postfix` mark the operator as a prefix or postfix
    // unary operator.
    //
    // `isAssign` marks all of `=`, `+=`, `-=` etcetera, which act as
    // binary operators with a very low precedence, that should result
    // in AssignmentExpression nodes.

    eq: new TokenType("=", {beforeExpr: true, isAssign: true}),
    assign: new TokenType("_=", {beforeExpr: true, isAssign: true}),
    incDec: new TokenType("++/--", {prefix: true, postfix: true, startsExpr: true}),
    prefix: new TokenType("prefix", {beforeExpr: true, prefix: true, startsExpr: true}),
    logicalOR: binop("||", 1),
    logicalAND: binop("&&", 2),
    bitwiseOR: binop("|", 3),
    bitwiseXOR: binop("^", 4),
    bitwiseAND: binop("&", 5),
    equality: binop("==/!=", 6),
    relational: binop("</>", 7),
    bitShift: binop("<</>>", 8),
    plusMin: new TokenType("+/-", {beforeExpr: true, binop: 9, prefix: true, startsExpr: true}),
    modulo: binop("%", 10),
    star: binop("*", 10),
    slash: binop("/", 10),
    starstar: new TokenType("**", {beforeExpr: true}),

    // Keyword token types.
    _break: kw("break"),
    _case: kw("case", beforeExpr),
    _catch: kw("catch"),
    _continue: kw("continue"),
    _debugger: kw("debugger"),
    _default: kw("default", beforeExpr),
    _do: kw("do", {isLoop: true, beforeExpr: true}),
    _else: kw("else", beforeExpr),
    _finally: kw("finally"),
    _for: kw("for", {isLoop: true}),
    _function: kw("function", startsExpr),
    _if: kw("if"),
    _return: kw("return", beforeExpr),
    _switch: kw("switch"),
    _throw: kw("throw", beforeExpr),
    _try: kw("try"),
    _var: kw("var"),
    _const: kw("const"),
    _while: kw("while", {isLoop: true}),
    _with: kw("with"),
    _new: kw("new", {beforeExpr: true, startsExpr: true}),
    _this: kw("this", startsExpr),
    _super: kw("super", startsExpr),
    _class: kw("class"),
    _extends: kw("extends", beforeExpr),
    _export: kw("export"),
    _import: kw("import"),
    _null: kw("null", startsExpr),
    _true: kw("true", startsExpr),
    _false: kw("false", startsExpr),
    _in: kw("in", {beforeExpr: true, binop: 7}),
    _instanceof: kw("instanceof", {beforeExpr: true, binop: 7}),
    _typeof: kw("typeof", {beforeExpr: true, prefix: true, startsExpr: true}),
    _void: kw("void", {beforeExpr: true, prefix: true, startsExpr: true}),
    _delete: kw("delete", {beforeExpr: true, prefix: true, startsExpr: true})
  }

  // Matches a whole line break (where CRLF is considered a single
  // line break). Used to count lines.

  var lineBreak = /\r\n?|\n|\u2028|\u2029/
  var lineBreakG = new RegExp(lineBreak.source, "g")

  function isNewLine(code) {
    return code === 10 || code === 13 || code === 0x2028 || code == 0x2029
  }

  var nonASCIIwhitespace = /[\u1680\u180e\u2000-\u200a\u202f\u205f\u3000\ufeff]/

  var skipWhiteSpace = /(?:\s|\/\/.*|\/\*[^]*?\*\/)*/g

  function isArray(obj) {
    return Object.prototype.toString.call(obj) === "[object Array]"
  }

  // Checks if an object has a property.

  function has(obj, propName) {
    return Object.prototype.hasOwnProperty.call(obj, propName)
  }

  // These are used when `options.locations` is on, for the
  // `startLoc` and `endLoc` properties.

  var Position = function Position(line, col) {
    this.line = line
    this.column = col
  };

  Position.prototype.offset = function offset (n) {
    return new Position(this.line, this.column + n)
  };

  var SourceLocation = function SourceLocation(p, start, end) {
    this.start = start
    this.end = end
    if (p.sourceFile !== null) this.source = p.sourceFile
  };

  // The `getLineInfo` function is mostly useful when the
  // `locations` option is off (for performance reasons) and you
  // want to find the line/column position for a given character
  // offset. `input` should be the code string that the offset refers
  // into.

  function getLineInfo(input, offset) {
    for (var line = 1, cur = 0;;) {
      lineBreakG.lastIndex = cur
      var match = lineBreakG.exec(input)
      if (match && match.index < offset) {
        ++line
        cur = match.index + match[0].length
      } else {
        return new Position(line, offset - cur)
      }
    }
  }

  // A second optional argument can be given to further configure
  // the parser process. These options are recognized:

  var defaultOptions = {
    // `ecmaVersion` indicates the ECMAScript version to parse. Must
    // be either 3, or 5, or 6. This influences support for strict
    // mode, the set of reserved words, support for getters and
    // setters and other features. The default is 6.
    ecmaVersion: 6,
    // Source type ("script" or "module") for different semantics
    sourceType: "script",
    // `onInsertedSemicolon` can be a callback that will be called
    // when a semicolon is automatically inserted. It will be passed
    // th position of the comma as an offset, and if `locations` is
    // enabled, it is given the location as a `{line, column}` object
    // as second argument.
    onInsertedSemicolon: null,
    // `onTrailingComma` is similar to `onInsertedSemicolon`, but for
    // trailing commas.
    onTrailingComma: null,
    // By default, reserved words are only enforced if ecmaVersion >= 5.
    // Set `allowReserved` to a boolean value to explicitly turn this on
    // an off. When this option has the value "never", reserved words
    // and keywords can also not be used as property names.
    allowReserved: null,
    // When enabled, a return at the top level is not considered an
    // error.
    allowReturnOutsideFunction: false,
    // When enabled, import/export statements are not constrained to
    // appearing at the top of the program.
    allowImportExportEverywhere: false,
    // When enabled, hashbang directive in the beginning of file
    // is allowed and treated as a line comment.
    allowHashBang: false,
    // When `locations` is on, `loc` properties holding objects with
    // `start` and `end` properties in `{line, column}` form (with
    // line being 1-based and column 0-based) will be attached to the
    // nodes.
    locations: false,
    // A function can be passed as `onToken` option, which will
    // cause Acorn to call that function with object in the same
    // format as tokens returned from `tokenizer().getToken()`. Note
    // that you are not allowed to call the parser from the
    // callback—that will corrupt its internal state.
    onToken: null,
    // A function can be passed as `onComment` option, which will
    // cause Acorn to call that function with `(block, text, start,
    // end)` parameters whenever a comment is skipped. `block` is a
    // boolean indicating whether this is a block (`/* */`) comment,
    // `text` is the content of the comment, and `start` and `end` are
    // character offsets that denote the start and end of the comment.
    // When the `locations` option is on, two more parameters are
    // passed, the full `{line, column}` locations of the start and
    // end of the comments. Note that you are not allowed to call the
    // parser from the callback—that will corrupt its internal state.
    onComment: null,
    // Nodes have their start and end characters offsets recorded in
    // `start` and `end` properties (directly on the node, rather than
    // the `loc` object, which holds line/column data. To also add a
    // [semi-standardized][range] `range` property holding a `[start,
    // end]` array with the same numbers, set the `ranges` option to
    // `true`.
    //
    // [range]: https://bugzilla.mozilla.org/show_bug.cgi?id=745678
    ranges: false,
    // It is possible to parse multiple files into a single AST by
    // passing the tree produced by parsing the first file as
    // `program` option in subsequent parses. This will add the
    // toplevel forms of the parsed file to the `Program` (top) node
    // of an existing parse tree.
    program: null,
    // When `locations` is on, you can pass this to record the source
    // file in every node's `loc` object.
    sourceFile: null,
    // This value, if given, is stored in every node, whether
    // `locations` is on or off.
    directSourceFile: null,
    // When enabled, parenthesized expressions are represented by
    // (non-standard) ParenthesizedExpression nodes
    preserveParens: false,
    plugins: {}
  }

  // Interpret and default an options object

  function getOptions(opts) {
    var options = {}
    for (var opt in defaultOptions)
      options[opt] = opts && has(opts, opt) ? opts[opt] : defaultOptions[opt]
    if (options.allowReserved == null)
      options.allowReserved = options.ecmaVersion < 5

    if (isArray(options.onToken)) {
      var tokens = options.onToken
      options.onToken = function (token) { return tokens.push(token); }
    }
    if (isArray(options.onComment))
      options.onComment = pushComment(options, options.onComment)

    return options
  }

  function pushComment(options, array) {
    return function (block, text, start, end, startLoc, endLoc) {
      var comment = {
        type: block ? 'Block' : 'Line',
        value: text,
        start: start,
        end: end
      }
      if (options.locations)
        comment.loc = new SourceLocation(this, startLoc, endLoc)
      if (options.ranges)
        comment.range = [start, end]
      array.push(comment)
    }
  }

  // Registered plugins
  var plugins = {}

  function keywordRegexp(words) {
    return new RegExp("^(" + words.replace(/ /g, "|") + ")$")
  }

  var Parser = function Parser(options, input, startPos) {
    this.options = options = getOptions(options)
    this.sourceFile = options.sourceFile
    this.keywords = keywordRegexp(keywords[options.ecmaVersion >= 6 ? 6 : 5])
    var reserved = options.allowReserved ? "" :
        reservedWords[options.ecmaVersion] + (options.sourceType == "module" ? " await" : "")
    this.reservedWords = keywordRegexp(reserved)
    var reservedStrict = (reserved ? reserved + " " : "") + reservedWords.strict
    this.reservedWordsStrict = keywordRegexp(reservedStrict)
    this.reservedWordsStrictBind = keywordRegexp(reservedStrict + " " + reservedWords.strictBind)
    this.input = String(input)

    // Used to signal to callers of `readWord1` whether the word
    // contained any escape sequences. This is needed because words with
    // escape sequences must not be interpreted as keywords.
    this.containsEsc = false

    // Load plugins
    this.loadPlugins(options.plugins)

    // Set up token state

    // The current position of the tokenizer in the input.
    if (startPos) {
      this.pos = startPos
      this.lineStart = Math.max(0, this.input.lastIndexOf("\n", startPos))
      this.curLine = this.input.slice(0, this.lineStart).split(lineBreak).length
    } else {
      this.pos = this.lineStart = 0
      this.curLine = 1
    }

    // Properties of the current token:
    // Its type
    this.type = tt.eof
    // For tokens that include more information than their type, the value
    this.value = null
    // Its start and end offset
    this.start = this.end = this.pos
    // And, if locations are used, the {line, column} object
    // corresponding to those offsets
    this.startLoc = this.endLoc = this.curPosition()

    // Position information for the previous token
    this.lastTokEndLoc = this.lastTokStartLoc = null
    this.lastTokStart = this.lastTokEnd = this.pos

    // The context stack is used to superficially track syntactic
    // context to predict whether a regular expression is allowed in a
    // given position.
    this.context = this.initialContext()
    this.exprAllowed = true

    // Figure out if it's a module code.
    this.strict = this.inModule = options.sourceType === "module"

    // Used to signify the start of a potential arrow function
    this.potentialArrowAt = -1

    // Flags to track whether we are in a function, a generator.
    this.inFunction = this.inGenerator = false
    // Labels in scope.
    this.labels = []

    // If enabled, skip leading hashbang line.
    if (this.pos === 0 && options.allowHashBang && this.input.slice(0, 2) === '#!')
      this.skipLineComment(2)
  };

  // DEPRECATED Kept for backwards compatibility until 3.0 in case a plugin uses them
  Parser.prototype.isKeyword = function isKeyword (word) { return this.keywords.test(word) };
  Parser.prototype.isReservedWord = function isReservedWord (word) { return this.reservedWords.test(word) };

  Parser.prototype.extend = function extend (name, f) {
    this[name] = f(this[name])
  };

  Parser.prototype.loadPlugins = function loadPlugins (pluginConfigs) {
      var this$1 = this;

    for (var name in pluginConfigs) {
      var plugin = plugins[name]
      if (!plugin) throw new Error("Plugin '" + name + "' not found")
      plugin(this$1, pluginConfigs[name])
    }
  };

  Parser.prototype.parse = function parse () {
    var node = this.options.program || this.startNode()
    this.nextToken()
    return this.parseTopLevel(node)
  };

  var pp = Parser.prototype

  // ## Parser utilities

  // Test whether a statement node is the string literal `"use strict"`.

  pp.isUseStrict = function(stmt) {
    return this.options.ecmaVersion >= 5 && stmt.type === "ExpressionStatement" &&
      stmt.expression.type === "Literal" &&
      stmt.expression.raw.slice(1, -1) === "use strict"
  }

  // Predicate that tests whether the next token is of the given
  // type, and if yes, consumes it as a side effect.

  pp.eat = function(type) {
    if (this.type === type) {
      this.next()
      return true
    } else {
      return false
    }
  }

  // Tests whether parsed token is a contextual keyword.

  pp.isContextual = function(name) {
    return this.type === tt.name && this.value === name
  }

  // Consumes contextual keyword if possible.

  pp.eatContextual = function(name) {
    return this.value === name && this.eat(tt.name)
  }

  // Asserts that following token is given contextual keyword.

  pp.expectContextual = function(name) {
    if (!this.eatContextual(name)) this.unexpected()
  }

  // Test whether a semicolon can be inserted at the current position.

  pp.canInsertSemicolon = function() {
    return this.type === tt.eof ||
      this.type === tt.braceR ||
      lineBreak.test(this.input.slice(this.lastTokEnd, this.start))
  }

  pp.insertSemicolon = function() {
    if (this.canInsertSemicolon()) {
      if (this.options.onInsertedSemicolon)
        this.options.onInsertedSemicolon(this.lastTokEnd, this.lastTokEndLoc)
      return true
    }
  }

  // Consume a semicolon, or, failing that, see if we are allowed to
  // pretend that there is a semicolon at this position.

  pp.semicolon = function() {
    if (!this.eat(tt.semi) && !this.insertSemicolon()) this.unexpected()
  }

  pp.afterTrailingComma = function(tokType) {
    if (this.type == tokType) {
      if (this.options.onTrailingComma)
        this.options.onTrailingComma(this.lastTokStart, this.lastTokStartLoc)
      this.next()
      return true
    }
  }

  // Expect a token of a given type. If found, consume it, otherwise,
  // raise an unexpected token error.

  pp.expect = function(type) {
    this.eat(type) || this.unexpected()
  }

  // Raise an unexpected token error.

  pp.unexpected = function(pos) {
    this.raise(pos != null ? pos : this.start, "Unexpected token")
  }

  var DestructuringErrors = function DestructuringErrors() {
    this.shorthandAssign = 0
    this.trailingComma = 0
  };

  pp.checkPatternErrors = function(refDestructuringErrors, andThrow) {
    var trailing = refDestructuringErrors && refDestructuringErrors.trailingComma
    if (!andThrow) return !!trailing
    if (trailing) this.raise(trailing, "Comma is not permitted after the rest element")
  }

  pp.checkExpressionErrors = function(refDestructuringErrors, andThrow) {
    var pos = refDestructuringErrors && refDestructuringErrors.shorthandAssign
    if (!andThrow) return !!pos
    if (pos) this.raise(pos, "Shorthand property assignments are valid only in destructuring patterns")
  }

  var pp$1 = Parser.prototype

  // ### Statement parsing

  // Parse a program. Initializes the parser, reads any number of
  // statements, and wraps them in a Program node.  Optionally takes a
  // `program` argument.  If present, the statements will be appended
  // to its body instead of creating a new node.

  pp$1.parseTopLevel = function(node) {
    var this$1 = this;

    var first = true
    if (!node.body) node.body = []
    while (this.type !== tt.eof) {
      var stmt = this$1.parseStatement(true, true)
      node.body.push(stmt)
      if (first) {
        if (this$1.isUseStrict(stmt)) this$1.setStrict(true)
        first = false
      }
    }
    this.next()
    if (this.options.ecmaVersion >= 6) {
      node.sourceType = this.options.sourceType
    }
    return this.finishNode(node, "Program")
  }

  var loopLabel = {kind: "loop"};
  var switchLabel = {kind: "switch"};
  pp$1.isLet = function() {
    if (this.type !== tt.name || this.options.ecmaVersion < 6 || this.value != "let") return false
    skipWhiteSpace.lastIndex = this.pos
    var skip = skipWhiteSpace.exec(this.input)
    var next = this.pos + skip[0].length, nextCh = this.input.charCodeAt(next)
    if (nextCh === 91 || nextCh == 123) return true // '{' and '['
    if (isIdentifierStart(nextCh, true)) {
      for (var pos = next + 1; isIdentifierChar(this.input.charCodeAt(pos), true); ++pos) {}
      var ident = this.input.slice(next, pos)
      if (!this.isKeyword(ident)) return true
    }
    return false
  }

  // Parse a single statement.
  //
  // If expecting a statement and finding a slash operator, parse a
  // regular expression literal. This is to handle cases like
  // `if (foo) /blah/.exec(foo)`, where looking at the previous token
  // does not help.

  pp$1.parseStatement = function(declaration, topLevel) {
    var starttype = this.type, node = this.startNode(), kind

    if (this.isLet()) {
      starttype = tt._var
      kind = "let"
    }

    // Most types of statements are recognized by the keyword they
    // start with. Many are trivial to parse, some require a bit of
    // complexity.

    switch (starttype) {
    case tt._break: case tt._continue: return this.parseBreakContinueStatement(node, starttype.keyword)
    case tt._debugger: return this.parseDebuggerStatement(node)
    case tt._do: return this.parseDoStatement(node)
    case tt._for: return this.parseForStatement(node)
    case tt._function:
      if (!declaration && this.options.ecmaVersion >= 6) this.unexpected()
      return this.parseFunctionStatement(node)
    case tt._class:
      if (!declaration) this.unexpected()
      return this.parseClass(node, true)
    case tt._if: return this.parseIfStatement(node)
    case tt._return: return this.parseReturnStatement(node)
    case tt._switch: return this.parseSwitchStatement(node)
    case tt._throw: return this.parseThrowStatement(node)
    case tt._try: return this.parseTryStatement(node)
    case tt._const: case tt._var:
      kind = kind || this.value
      if (!declaration && kind != "var") this.unexpected()
      return this.parseVarStatement(node, kind)
    case tt._while: return this.parseWhileStatement(node)
    case tt._with: return this.parseWithStatement(node)
    case tt.braceL: return this.parseBlock()
    case tt.semi: return this.parseEmptyStatement(node)
    case tt._export:
    case tt._import:
      if (!this.options.allowImportExportEverywhere) {
        if (!topLevel)
          this.raise(this.start, "'import' and 'export' may only appear at the top level")
        if (!this.inModule)
          this.raise(this.start, "'import' and 'export' may appear only with 'sourceType: module'")
      }
      return starttype === tt._import ? this.parseImport(node) : this.parseExport(node)

      // If the statement does not start with a statement keyword or a
      // brace, it's an ExpressionStatement or LabeledStatement. We
      // simply start parsing an expression, and afterwards, if the
      // next token is a colon and the expression was a simple
      // Identifier node, we switch to interpreting it as a label.
    default:
      var maybeName = this.value, expr = this.parseExpression()
      if (starttype === tt.name && expr.type === "Identifier" && this.eat(tt.colon))
        return this.parseLabeledStatement(node, maybeName, expr)
      else return this.parseExpressionStatement(node, expr)
    }
  }

  pp$1.parseBreakContinueStatement = function(node, keyword) {
    var this$1 = this;

    var isBreak = keyword == "break"
    this.next()
    if (this.eat(tt.semi) || this.insertSemicolon()) node.label = null
    else if (this.type !== tt.name) this.unexpected()
    else {
      node.label = this.parseIdent()
      this.semicolon()
    }

    // Verify that there is an actual destination to break or
    // continue to.
    for (var i = 0; i < this.labels.length; ++i) {
      var lab = this$1.labels[i]
      if (node.label == null || lab.name === node.label.name) {
        if (lab.kind != null && (isBreak || lab.kind === "loop")) break
        if (node.label && isBreak) break
      }
    }
    if (i === this.labels.length) this.raise(node.start, "Unsyntactic " + keyword)
    return this.finishNode(node, isBreak ? "BreakStatement" : "ContinueStatement")
  }

  pp$1.parseDebuggerStatement = function(node) {
    this.next()
    this.semicolon()
    return this.finishNode(node, "DebuggerStatement")
  }

  pp$1.parseDoStatement = function(node) {
    this.next()
    this.labels.push(loopLabel)
    node.body = this.parseStatement(false)
    this.labels.pop()
    this.expect(tt._while)
    node.test = this.parseParenExpression()
    if (this.options.ecmaVersion >= 6)
      this.eat(tt.semi)
    else
      this.semicolon()
    return this.finishNode(node, "DoWhileStatement")
  }

  // Disambiguating between a `for` and a `for`/`in` or `for`/`of`
  // loop is non-trivial. Basically, we have to parse the init `var`
  // statement or expression, disallowing the `in` operator (see
  // the second parameter to `parseExpression`), and then check
  // whether the next token is `in` or `of`. When there is no init
  // part (semicolon immediately after the opening parenthesis), it
  // is a regular `for` loop.

  pp$1.parseForStatement = function(node) {
    this.next()
    this.labels.push(loopLabel)
    this.expect(tt.parenL)
    if (this.type === tt.semi) return this.parseFor(node, null)
    var isLet = this.isLet()
    if (this.type === tt._var || this.type === tt._const || isLet) {
      var init$1 = this.startNode(), kind = isLet ? "let" : this.value
      this.next()
      this.parseVar(init$1, true, kind)
      this.finishNode(init$1, "VariableDeclaration")
      if ((this.type === tt._in || (this.options.ecmaVersion >= 6 && this.isContextual("of"))) && init$1.declarations.length === 1 &&
          !(kind !== "var" && init$1.declarations[0].init))
        return this.parseForIn(node, init$1)
      return this.parseFor(node, init$1)
    }
    var refDestructuringErrors = new DestructuringErrors
    var init = this.parseExpression(true, refDestructuringErrors)
    if (this.type === tt._in || (this.options.ecmaVersion >= 6 && this.isContextual("of"))) {
      this.checkPatternErrors(refDestructuringErrors, true)
      this.toAssignable(init)
      this.checkLVal(init)
      return this.parseForIn(node, init)
    } else {
      this.checkExpressionErrors(refDestructuringErrors, true)
    }
    return this.parseFor(node, init)
  }

  pp$1.parseFunctionStatement = function(node) {
    this.next()
    return this.parseFunction(node, true)
  }

  pp$1.parseIfStatement = function(node) {
    this.next()
    node.test = this.parseParenExpression()
    node.consequent = this.parseStatement(false)
    node.alternate = this.eat(tt._else) ? this.parseStatement(false) : null
    return this.finishNode(node, "IfStatement")
  }

  pp$1.parseReturnStatement = function(node) {
    if (!this.inFunction && !this.options.allowReturnOutsideFunction)
      this.raise(this.start, "'return' outside of function")
    this.next()

    // In `return` (and `break`/`continue`), the keywords with
    // optional arguments, we eagerly look for a semicolon or the
    // possibility to insert one.

    if (this.eat(tt.semi) || this.insertSemicolon()) node.argument = null
    else { node.argument = this.parseExpression(); this.semicolon() }
    return this.finishNode(node, "ReturnStatement")
  }

  pp$1.parseSwitchStatement = function(node) {
    var this$1 = this;

    this.next()
    node.discriminant = this.parseParenExpression()
    node.cases = []
    this.expect(tt.braceL)
    this.labels.push(switchLabel)

    // Statements under must be grouped (by label) in SwitchCase
    // nodes. `cur` is used to keep the node that we are currently
    // adding statements to.

    for (var cur, sawDefault = false; this.type != tt.braceR;) {
      if (this$1.type === tt._case || this$1.type === tt._default) {
        var isCase = this$1.type === tt._case
        if (cur) this$1.finishNode(cur, "SwitchCase")
        node.cases.push(cur = this$1.startNode())
        cur.consequent = []
        this$1.next()
        if (isCase) {
          cur.test = this$1.parseExpression()
        } else {
          if (sawDefault) this$1.raiseRecoverable(this$1.lastTokStart, "Multiple default clauses")
          sawDefault = true
          cur.test = null
        }
        this$1.expect(tt.colon)
      } else {
        if (!cur) this$1.unexpected()
        cur.consequent.push(this$1.parseStatement(true))
      }
    }
    if (cur) this.finishNode(cur, "SwitchCase")
    this.next() // Closing brace
    this.labels.pop()
    return this.finishNode(node, "SwitchStatement")
  }

  pp$1.parseThrowStatement = function(node) {
    this.next()
    if (lineBreak.test(this.input.slice(this.lastTokEnd, this.start)))
      this.raise(this.lastTokEnd, "Illegal newline after throw")
    node.argument = this.parseExpression()
    this.semicolon()
    return this.finishNode(node, "ThrowStatement")
  }

  // Reused empty array added for node fields that are always empty.

  var empty = []

  pp$1.parseTryStatement = function(node) {
    this.next()
    node.block = this.parseBlock()
    node.handler = null
    if (this.type === tt._catch) {
      var clause = this.startNode()
      this.next()
      this.expect(tt.parenL)
      clause.param = this.parseBindingAtom()
      this.checkLVal(clause.param, true)
      this.expect(tt.parenR)
      clause.body = this.parseBlock()
      node.handler = this.finishNode(clause, "CatchClause")
    }
    node.finalizer = this.eat(tt._finally) ? this.parseBlock() : null
    if (!node.handler && !node.finalizer)
      this.raise(node.start, "Missing catch or finally clause")
    return this.finishNode(node, "TryStatement")
  }

  pp$1.parseVarStatement = function(node, kind) {
    this.next()
    this.parseVar(node, false, kind)
    this.semicolon()
    return this.finishNode(node, "VariableDeclaration")
  }

  pp$1.parseWhileStatement = function(node) {
    this.next()
    node.test = this.parseParenExpression()
    this.labels.push(loopLabel)
    node.body = this.parseStatement(false)
    this.labels.pop()
    return this.finishNode(node, "WhileStatement")
  }

  pp$1.parseWithStatement = function(node) {
    if (this.strict) this.raise(this.start, "'with' in strict mode")
    this.next()
    node.object = this.parseParenExpression()
    node.body = this.parseStatement(false)
    return this.finishNode(node, "WithStatement")
  }

  pp$1.parseEmptyStatement = function(node) {
    this.next()
    return this.finishNode(node, "EmptyStatement")
  }

  pp$1.parseLabeledStatement = function(node, maybeName, expr) {
    var this$1 = this;

    for (var i = 0; i < this.labels.length; ++i)
      if (this$1.labels[i].name === maybeName) this$1.raise(expr.start, "Label '" + maybeName + "' is already declared")
    var kind = this.type.isLoop ? "loop" : this.type === tt._switch ? "switch" : null
    for (var i$1 = this.labels.length - 1; i$1 >= 0; i$1--) {
      var label = this$1.labels[i$1]
      if (label.statementStart == node.start) {
        label.statementStart = this$1.start
        label.kind = kind
      } else break
    }
    this.labels.push({name: maybeName, kind: kind, statementStart: this.start})
    node.body = this.parseStatement(true)
    this.labels.pop()
    node.label = expr
    return this.finishNode(node, "LabeledStatement")
  }

  pp$1.parseExpressionStatement = function(node, expr) {
    node.expression = expr
    this.semicolon()
    return this.finishNode(node, "ExpressionStatement")
  }

  // Parse a semicolon-enclosed block of statements, handling `"use
  // strict"` declarations when `allowStrict` is true (used for
  // function bodies).

  pp$1.parseBlock = function(allowStrict) {
    var this$1 = this;

    var node = this.startNode(), first = true, oldStrict
    node.body = []
    this.expect(tt.braceL)
    while (!this.eat(tt.braceR)) {
      var stmt = this$1.parseStatement(true)
      node.body.push(stmt)
      if (first && allowStrict && this$1.isUseStrict(stmt)) {
        oldStrict = this$1.strict
        this$1.setStrict(this$1.strict = true)
      }
      first = false
    }
    if (oldStrict === false) this.setStrict(false)
    return this.finishNode(node, "BlockStatement")
  }

  // Parse a regular `for` loop. The disambiguation code in
  // `parseStatement` will already have parsed the init statement or
  // expression.

  pp$1.parseFor = function(node, init) {
    node.init = init
    this.expect(tt.semi)
    node.test = this.type === tt.semi ? null : this.parseExpression()
    this.expect(tt.semi)
    node.update = this.type === tt.parenR ? null : this.parseExpression()
    this.expect(tt.parenR)
    node.body = this.parseStatement(false)
    this.labels.pop()
    return this.finishNode(node, "ForStatement")
  }

  // Parse a `for`/`in` and `for`/`of` loop, which are almost
  // same from parser's perspective.

  pp$1.parseForIn = function(node, init) {
    var type = this.type === tt._in ? "ForInStatement" : "ForOfStatement"
    this.next()
    node.left = init
    node.right = this.parseExpression()
    this.expect(tt.parenR)
    node.body = this.parseStatement(false)
    this.labels.pop()
    return this.finishNode(node, type)
  }

  // Parse a list of variable declarations.

  pp$1.parseVar = function(node, isFor, kind) {
    var this$1 = this;

    node.declarations = []
    node.kind = kind
    for (;;) {
      var decl = this$1.startNode()
      this$1.parseVarId(decl)
      if (this$1.eat(tt.eq)) {
        decl.init = this$1.parseMaybeAssign(isFor)
      } else if (kind === "const" && !(this$1.type === tt._in || (this$1.options.ecmaVersion >= 6 && this$1.isContextual("of")))) {
        this$1.unexpected()
      } else if (decl.id.type != "Identifier" && !(isFor && (this$1.type === tt._in || this$1.isContextual("of")))) {
        this$1.raise(this$1.lastTokEnd, "Complex binding patterns require an initialization value")
      } else {
        decl.init = null
      }
      node.declarations.push(this$1.finishNode(decl, "VariableDeclarator"))
      if (!this$1.eat(tt.comma)) break
    }
    return node
  }

  pp$1.parseVarId = function(decl) {
    decl.id = this.parseBindingAtom()
    this.checkLVal(decl.id, true)
  }

  // Parse a function declaration or literal (depending on the
  // `isStatement` parameter).

  pp$1.parseFunction = function(node, isStatement, allowExpressionBody) {
    this.initFunction(node)
    if (this.options.ecmaVersion >= 6)
      node.generator = this.eat(tt.star)
    var oldInGen = this.inGenerator
    this.inGenerator = node.generator
    if (isStatement || this.type === tt.name)
      node.id = this.parseIdent()
    this.parseFunctionParams(node)
    this.parseFunctionBody(node, allowExpressionBody)
    this.inGenerator = oldInGen
    return this.finishNode(node, isStatement ? "FunctionDeclaration" : "FunctionExpression")
  }

  pp$1.parseFunctionParams = function(node) {
    this.expect(tt.parenL)
    node.params = this.parseBindingList(tt.parenR, false, false, true)
  }

  // Parse a class declaration or literal (depending on the
  // `isStatement` parameter).

  pp$1.parseClass = function(node, isStatement) {
    var this$1 = this;

    this.next()
    this.parseClassId(node, isStatement)
    this.parseClassSuper(node)
    var classBody = this.startNode()
    var hadConstructor = false
    classBody.body = []
    this.expect(tt.braceL)
    while (!this.eat(tt.braceR)) {
      if (this$1.eat(tt.semi)) continue
      var method = this$1.startNode()
      var isGenerator = this$1.eat(tt.star)
      var isMaybeStatic = this$1.type === tt.name && this$1.value === "static"
      this$1.parsePropertyName(method)
      method.static = isMaybeStatic && this$1.type !== tt.parenL
      if (method.static) {
        if (isGenerator) this$1.unexpected()
        isGenerator = this$1.eat(tt.star)
        this$1.parsePropertyName(method)
      }
      method.kind = "method"
      var isGetSet = false
      if (!method.computed) {
        var key = method.key;
        if (!isGenerator && key.type === "Identifier" && this$1.type !== tt.parenL && (key.name === "get" || key.name === "set")) {
          isGetSet = true
          method.kind = key.name
          key = this$1.parsePropertyName(method)
        }
        if (!method.static && (key.type === "Identifier" && key.name === "constructor" ||
            key.type === "Literal" && key.value === "constructor")) {
          if (hadConstructor) this$1.raise(key.start, "Duplicate constructor in the same class")
          if (isGetSet) this$1.raise(key.start, "Constructor can't have get/set modifier")
          if (isGenerator) this$1.raise(key.start, "Constructor can't be a generator")
          method.kind = "constructor"
          hadConstructor = true
        }
      }
      this$1.parseClassMethod(classBody, method, isGenerator)
      if (isGetSet) {
        var paramCount = method.kind === "get" ? 0 : 1
        if (method.value.params.length !== paramCount) {
          var start = method.value.start
          if (method.kind === "get")
            this$1.raiseRecoverable(start, "getter should have no params")
          else
            this$1.raiseRecoverable(start, "setter should have exactly one param")
        }
        if (method.kind === "set" && method.value.params[0].type === "RestElement")
          this$1.raise(method.value.params[0].start, "Setter cannot use rest params")
      }
    }
    node.body = this.finishNode(classBody, "ClassBody")
    return this.finishNode(node, isStatement ? "ClassDeclaration" : "ClassExpression")
  }

  pp$1.parseClassMethod = function(classBody, method, isGenerator) {
    method.value = this.parseMethod(isGenerator)
    classBody.body.push(this.finishNode(method, "MethodDefinition"))
  }

  pp$1.parseClassId = function(node, isStatement) {
    node.id = this.type === tt.name ? this.parseIdent() : isStatement ? this.unexpected() : null
  }

  pp$1.parseClassSuper = function(node) {
    node.superClass = this.eat(tt._extends) ? this.parseExprSubscripts() : null
  }

  // Parses module export declaration.

  pp$1.parseExport = function(node) {
    var this$1 = this;

    this.next()
    // export * from '...'
    if (this.eat(tt.star)) {
      this.expectContextual("from")
      node.source = this.type === tt.string ? this.parseExprAtom() : this.unexpected()
      this.semicolon()
      return this.finishNode(node, "ExportAllDeclaration")
    }
    if (this.eat(tt._default)) { // export default ...
      var parens = this.type == tt.parenL
      var expr = this.parseMaybeAssign()
      var needsSemi = true
      if (!parens && (expr.type == "FunctionExpression" ||
                      expr.type == "ClassExpression")) {
        needsSemi = false
        if (expr.id) {
          expr.type = expr.type == "FunctionExpression"
            ? "FunctionDeclaration"
            : "ClassDeclaration"
        }
      }
      node.declaration = expr
      if (needsSemi) this.semicolon()
      return this.finishNode(node, "ExportDefaultDeclaration")
    }
    // export var|const|let|function|class ...
    if (this.shouldParseExportStatement()) {
      node.declaration = this.parseStatement(true)
      node.specifiers = []
      node.source = null
    } else { // export { x, y as z } [from '...']
      node.declaration = null
      node.specifiers = this.parseExportSpecifiers()
      if (this.eatContextual("from")) {
        node.source = this.type === tt.string ? this.parseExprAtom() : this.unexpected()
      } else {
        // check for keywords used as local names
        for (var i = 0; i < node.specifiers.length; i++) {
          if (this$1.keywords.test(node.specifiers[i].local.name) || this$1.reservedWords.test(node.specifiers[i].local.name)) {
            this$1.unexpected(node.specifiers[i].local.start)
          }
        }

        node.source = null
      }
      this.semicolon()
    }
    return this.finishNode(node, "ExportNamedDeclaration")
  }

  pp$1.shouldParseExportStatement = function() {
    return this.type.keyword || this.isLet()
  }

  // Parses a comma-separated list of module exports.

  pp$1.parseExportSpecifiers = function() {
    var this$1 = this;

    var nodes = [], first = true
    // export { x, y as z } [from '...']
    this.expect(tt.braceL)
    while (!this.eat(tt.braceR)) {
      if (!first) {
        this$1.expect(tt.comma)
        if (this$1.afterTrailingComma(tt.braceR)) break
      } else first = false

      var node = this$1.startNode()
      node.local = this$1.parseIdent(this$1.type === tt._default)
      node.exported = this$1.eatContextual("as") ? this$1.parseIdent(true) : node.local
      nodes.push(this$1.finishNode(node, "ExportSpecifier"))
    }
    return nodes
  }

  // Parses import declaration.

  pp$1.parseImport = function(node) {
    this.next()
    // import '...'
    if (this.type === tt.string) {
      node.specifiers = empty
      node.source = this.parseExprAtom()
    } else {
      node.specifiers = this.parseImportSpecifiers()
      this.expectContextual("from")
      node.source = this.type === tt.string ? this.parseExprAtom() : this.unexpected()
    }
    this.semicolon()
    return this.finishNode(node, "ImportDeclaration")
  }

  // Parses a comma-separated list of module imports.

  pp$1.parseImportSpecifiers = function() {
    var this$1 = this;

    var nodes = [], first = true
    if (this.type === tt.name) {
      // import defaultObj, { x, y as z } from '...'
      var node = this.startNode()
      node.local = this.parseIdent()
      this.checkLVal(node.local, true)
      nodes.push(this.finishNode(node, "ImportDefaultSpecifier"))
      if (!this.eat(tt.comma)) return nodes
    }
    if (this.type === tt.star) {
      var node$1 = this.startNode()
      this.next()
      this.expectContextual("as")
      node$1.local = this.parseIdent()
      this.checkLVal(node$1.local, true)
      nodes.push(this.finishNode(node$1, "ImportNamespaceSpecifier"))
      return nodes
    }
    this.expect(tt.braceL)
    while (!this.eat(tt.braceR)) {
      if (!first) {
        this$1.expect(tt.comma)
        if (this$1.afterTrailingComma(tt.braceR)) break
      } else first = false

      var node$2 = this$1.startNode()
      node$2.imported = this$1.parseIdent(true)
      if (this$1.eatContextual("as")) {
        node$2.local = this$1.parseIdent()
      } else {
        node$2.local = node$2.imported
        if (this$1.isKeyword(node$2.local.name)) this$1.unexpected(node$2.local.start)
        if (this$1.reservedWordsStrict.test(node$2.local.name)) this$1.raise(node$2.local.start, "The keyword '" + node$2.local.name + "' is reserved")
      }
      this$1.checkLVal(node$2.local, true)
      nodes.push(this$1.finishNode(node$2, "ImportSpecifier"))
    }
    return nodes
  }

  var pp$2 = Parser.prototype

  // Convert existing expression atom to assignable pattern
  // if possible.

  pp$2.toAssignable = function(node, isBinding) {
    var this$1 = this;

    if (this.options.ecmaVersion >= 6 && node) {
      switch (node.type) {
      case "Identifier":
      case "ObjectPattern":
      case "ArrayPattern":
        break

      case "ObjectExpression":
        node.type = "ObjectPattern"
        for (var i = 0; i < node.properties.length; i++) {
          var prop = node.properties[i]
          if (prop.kind !== "init") this$1.raise(prop.key.start, "Object pattern can't contain getter or setter")
          this$1.toAssignable(prop.value, isBinding)
        }
        break

      case "ArrayExpression":
        node.type = "ArrayPattern"
        this.toAssignableList(node.elements, isBinding)
        break

      case "AssignmentExpression":
        if (node.operator === "=") {
          node.type = "AssignmentPattern"
          delete node.operator
          // falls through to AssignmentPattern
        } else {
          this.raise(node.left.end, "Only '=' operator can be used for specifying default value.")
          break
        }

      case "AssignmentPattern":
        if (node.right.type === "YieldExpression")
          this.raise(node.right.start, "Yield expression cannot be a default value")
        break

      case "ParenthesizedExpression":
        node.expression = this.toAssignable(node.expression, isBinding)
        break

      case "MemberExpression":
        if (!isBinding) break

      default:
        this.raise(node.start, "Assigning to rvalue")
      }
    }
    return node
  }

  // Convert list of expression atoms to binding list.

  pp$2.toAssignableList = function(exprList, isBinding) {
    var this$1 = this;

    var end = exprList.length
    if (end) {
      var last = exprList[end - 1]
      if (last && last.type == "RestElement") {
        --end
      } else if (last && last.type == "SpreadElement") {
        last.type = "RestElement"
        var arg = last.argument
        this.toAssignable(arg, isBinding)
        if (arg.type !== "Identifier" && arg.type !== "MemberExpression" && arg.type !== "ArrayPattern")
          this.unexpected(arg.start)
        --end
      }

      if (isBinding && last && last.type === "RestElement" && last.argument.type !== "Identifier")
        this.unexpected(last.argument.start)
    }
    for (var i = 0; i < end; i++) {
      var elt = exprList[i]
      if (elt) this$1.toAssignable(elt, isBinding)
    }
    return exprList
  }

  // Parses spread element.

  pp$2.parseSpread = function(refDestructuringErrors) {
    var node = this.startNode()
    this.next()
    node.argument = this.parseMaybeAssign(false, refDestructuringErrors)
    return this.finishNode(node, "SpreadElement")
  }

  pp$2.parseRest = function(allowNonIdent) {
    var node = this.startNode()
    this.next()

    // RestElement inside of a function parameter must be an identifier
    if (allowNonIdent) node.argument = this.type === tt.name ? this.parseIdent() : this.unexpected()
    else node.argument = this.type === tt.name || this.type === tt.bracketL ? this.parseBindingAtom() : this.unexpected()

    return this.finishNode(node, "RestElement")
  }

  // Parses lvalue (assignable) atom.

  pp$2.parseBindingAtom = function() {
    if (this.options.ecmaVersion < 6) return this.parseIdent()
    switch (this.type) {
    case tt.name:
      return this.parseIdent()

    case tt.bracketL:
      var node = this.startNode()
      this.next()
      node.elements = this.parseBindingList(tt.bracketR, true, true)
      return this.finishNode(node, "ArrayPattern")

    case tt.braceL:
      return this.parseObj(true)

    default:
      this.unexpected()
    }
  }

  pp$2.parseBindingList = function(close, allowEmpty, allowTrailingComma, allowNonIdent) {
    var this$1 = this;

    var elts = [], first = true
    while (!this.eat(close)) {
      if (first) first = false
      else this$1.expect(tt.comma)
      if (allowEmpty && this$1.type === tt.comma) {
        elts.push(null)
      } else if (allowTrailingComma && this$1.afterTrailingComma(close)) {
        break
      } else if (this$1.type === tt.ellipsis) {
        var rest = this$1.parseRest(allowNonIdent)
        this$1.parseBindingListItem(rest)
        elts.push(rest)
        if (this$1.type === tt.comma) this$1.raise(this$1.start, "Comma is not permitted after the rest element")
        this$1.expect(close)
        break
      } else {
        var elem = this$1.parseMaybeDefault(this$1.start, this$1.startLoc)
        this$1.parseBindingListItem(elem)
        elts.push(elem)
      }
    }
    return elts
  }

  pp$2.parseBindingListItem = function(param) {
    return param
  }

  // Parses assignment pattern around given atom if possible.

  pp$2.parseMaybeDefault = function(startPos, startLoc, left) {
    left = left || this.parseBindingAtom()
    if (this.options.ecmaVersion < 6 || !this.eat(tt.eq)) return left
    var node = this.startNodeAt(startPos, startLoc)
    node.left = left
    node.right = this.parseMaybeAssign()
    return this.finishNode(node, "AssignmentPattern")
  }

  // Verify that a node is an lval — something that can be assigned
  // to.

  pp$2.checkLVal = function(expr, isBinding, checkClashes) {
    var this$1 = this;

    switch (expr.type) {
    case "Identifier":
      if (this.strict && this.reservedWordsStrictBind.test(expr.name))
        this.raiseRecoverable(expr.start, (isBinding ? "Binding " : "Assigning to ") + expr.name + " in strict mode")
      if (checkClashes) {
        if (has(checkClashes, expr.name))
          this.raiseRecoverable(expr.start, "Argument name clash")
        checkClashes[expr.name] = true
      }
      break

    case "MemberExpression":
      if (isBinding) this.raiseRecoverable(expr.start, (isBinding ? "Binding" : "Assigning to") + " member expression")
      break

    case "ObjectPattern":
      for (var i = 0; i < expr.properties.length; i++)
        this$1.checkLVal(expr.properties[i].value, isBinding, checkClashes)
      break

    case "ArrayPattern":
      for (var i$1 = 0; i$1 < expr.elements.length; i$1++) {
        var elem = expr.elements[i$1]
        if (elem) this$1.checkLVal(elem, isBinding, checkClashes)
      }
      break

    case "AssignmentPattern":
      this.checkLVal(expr.left, isBinding, checkClashes)
      break

    case "RestElement":
      this.checkLVal(expr.argument, isBinding, checkClashes)
      break

    case "ParenthesizedExpression":
      this.checkLVal(expr.expression, isBinding, checkClashes)
      break

    default:
      this.raise(expr.start, (isBinding ? "Binding" : "Assigning to") + " rvalue")
    }
  }

  var pp$3 = Parser.prototype

  // Check if property name clashes with already added.
  // Object/class getters and setters are not allowed to clash —
  // either with each other or with an init property — and in
  // strict mode, init properties are also not allowed to be repeated.

  pp$3.checkPropClash = function(prop, propHash) {
    if (this.options.ecmaVersion >= 6 && (prop.computed || prop.method || prop.shorthand))
      return
    var key = prop.key;
    var name
    switch (key.type) {
    case "Identifier": name = key.name; break
    case "Literal": name = String(key.value); break
    default: return
    }
    var kind = prop.kind;
    if (this.options.ecmaVersion >= 6) {
      if (name === "__proto__" && kind === "init") {
        if (propHash.proto) this.raiseRecoverable(key.start, "Redefinition of __proto__ property")
        propHash.proto = true
      }
      return
    }
    name = "$" + name
    var other = propHash[name]
    if (other) {
      var isGetSet = kind !== "init"
      if ((this.strict || isGetSet) && other[kind] || !(isGetSet ^ other.init))
        this.raiseRecoverable(key.start, "Redefinition of property")
    } else {
      other = propHash[name] = {
        init: false,
        get: false,
        set: false
      }
    }
    other[kind] = true
  }

  // ### Expression parsing

  // These nest, from the most general expression type at the top to
  // 'atomic', nondivisible expression types at the bottom. Most of
  // the functions will simply let the function(s) below them parse,
  // and, *if* the syntactic construct they handle is present, wrap
  // the AST node that the inner parser gave them in another node.

  // Parse a full expression. The optional arguments are used to
  // forbid the `in` operator (in for loops initalization expressions)
  // and provide reference for storing '=' operator inside shorthand
  // property assignment in contexts where both object expression
  // and object pattern might appear (so it's possible to raise
  // delayed syntax error at correct position).

  pp$3.parseExpression = function(noIn, refDestructuringErrors) {
    var this$1 = this;

    var startPos = this.start, startLoc = this.startLoc
    var expr = this.parseMaybeAssign(noIn, refDestructuringErrors)
    if (this.type === tt.comma) {
      var node = this.startNodeAt(startPos, startLoc)
      node.expressions = [expr]
      while (this.eat(tt.comma)) node.expressions.push(this$1.parseMaybeAssign(noIn, refDestructuringErrors))
      return this.finishNode(node, "SequenceExpression")
    }
    return expr
  }

  // Parse an assignment expression. This includes applications of
  // operators like `+=`.

  pp$3.parseMaybeAssign = function(noIn, refDestructuringErrors, afterLeftParse) {
    if (this.inGenerator && this.isContextual("yield")) return this.parseYield()

    var ownDestructuringErrors = false
    if (!refDestructuringErrors) {
      refDestructuringErrors = new DestructuringErrors
      ownDestructuringErrors = true
    }
    var startPos = this.start, startLoc = this.startLoc
    if (this.type == tt.parenL || this.type == tt.name)
      this.potentialArrowAt = this.start
    var left = this.parseMaybeConditional(noIn, refDestructuringErrors)
    if (afterLeftParse) left = afterLeftParse.call(this, left, startPos, startLoc)
    if (this.type.isAssign) {
      this.checkPatternErrors(refDestructuringErrors, true)
      if (!ownDestructuringErrors) DestructuringErrors.call(refDestructuringErrors)
      var node = this.startNodeAt(startPos, startLoc)
      node.operator = this.value
      node.left = this.type === tt.eq ? this.toAssignable(left) : left
      refDestructuringErrors.shorthandAssign = 0 // reset because shorthand default was used correctly
      this.checkLVal(left)
      this.next()
      node.right = this.parseMaybeAssign(noIn)
      return this.finishNode(node, "AssignmentExpression")
    } else {
      if (ownDestructuringErrors) this.checkExpressionErrors(refDestructuringErrors, true)
    }
    return left
  }

  // Parse a ternary conditional (`?:`) operator.

  pp$3.parseMaybeConditional = function(noIn, refDestructuringErrors) {
    var startPos = this.start, startLoc = this.startLoc
    var expr = this.parseExprOps(noIn, refDestructuringErrors)
    if (this.checkExpressionErrors(refDestructuringErrors)) return expr
    if (this.eat(tt.question)) {
      var node = this.startNodeAt(startPos, startLoc)
      node.test = expr
      node.consequent = this.parseMaybeAssign()
      this.expect(tt.colon)
      node.alternate = this.parseMaybeAssign(noIn)
      return this.finishNode(node, "ConditionalExpression")
    }
    return expr
  }

  // Start the precedence parser.

  pp$3.parseExprOps = function(noIn, refDestructuringErrors) {
    var startPos = this.start, startLoc = this.startLoc
    var expr = this.parseMaybeUnary(refDestructuringErrors, false)
    if (this.checkExpressionErrors(refDestructuringErrors)) return expr
    return this.parseExprOp(expr, startPos, startLoc, -1, noIn)
  }

  // Parse binary operators with the operator precedence parsing
  // algorithm. `left` is the left-hand side of the operator.
  // `minPrec` provides context that allows the function to stop and
  // defer further parser to one of its callers when it encounters an
  // operator that has a lower precedence than the set it is parsing.

  pp$3.parseExprOp = function(left, leftStartPos, leftStartLoc, minPrec, noIn) {
    var prec = this.type.binop
    if (prec != null && (!noIn || this.type !== tt._in)) {
      if (prec > minPrec) {
        var logical = this.type === tt.logicalOR || this.type === tt.logicalAND
        var op = this.value
        this.next()
        var startPos = this.start, startLoc = this.startLoc
        var right = this.parseExprOp(this.parseMaybeUnary(null, false), startPos, startLoc, prec, noIn)
        var node = this.buildBinary(leftStartPos, leftStartLoc, left, right, op, logical)
        return this.parseExprOp(node, leftStartPos, leftStartLoc, minPrec, noIn)
      }
    }
    return left
  }

  pp$3.buildBinary = function(startPos, startLoc, left, right, op, logical) {
    var node = this.startNodeAt(startPos, startLoc)
    node.left = left
    node.operator = op
    node.right = right
    return this.finishNode(node, logical ? "LogicalExpression" : "BinaryExpression")
  }

  // Parse unary operators, both prefix and postfix.

  pp$3.parseMaybeUnary = function(refDestructuringErrors, sawUnary) {
    var this$1 = this;

    var startPos = this.start, startLoc = this.startLoc, expr
    if (this.type.prefix) {
      var node = this.startNode(), update = this.type === tt.incDec
      node.operator = this.value
      node.prefix = true
      this.next()
      node.argument = this.parseMaybeUnary(null, true)
      this.checkExpressionErrors(refDestructuringErrors, true)
      if (update) this.checkLVal(node.argument)
      else if (this.strict && node.operator === "delete" &&
               node.argument.type === "Identifier")
        this.raiseRecoverable(node.start, "Deleting local variable in strict mode")
      else sawUnary = true
      expr = this.finishNode(node, update ? "UpdateExpression" : "UnaryExpression")
    } else {
      expr = this.parseExprSubscripts(refDestructuringErrors)
      if (this.checkExpressionErrors(refDestructuringErrors)) return expr
      while (this.type.postfix && !this.canInsertSemicolon()) {
        var node$1 = this$1.startNodeAt(startPos, startLoc)
        node$1.operator = this$1.value
        node$1.prefix = false
        node$1.argument = expr
        this$1.checkLVal(expr)
        this$1.next()
        expr = this$1.finishNode(node$1, "UpdateExpression")
      }
    }

    if (!sawUnary && this.eat(tt.starstar))
      return this.buildBinary(startPos, startLoc, expr, this.parseMaybeUnary(null, false), "**", false)
    else
      return expr
  }

  // Parse call, dot, and `[]`-subscript expressions.

  pp$3.parseExprSubscripts = function(refDestructuringErrors) {
    var startPos = this.start, startLoc = this.startLoc
    var expr = this.parseExprAtom(refDestructuringErrors)
    var skipArrowSubscripts = expr.type === "ArrowFunctionExpression" && this.input.slice(this.lastTokStart, this.lastTokEnd) !== ")"
    if (this.checkExpressionErrors(refDestructuringErrors) || skipArrowSubscripts) return expr
    return this.parseSubscripts(expr, startPos, startLoc)
  }

  pp$3.parseSubscripts = function(base, startPos, startLoc, noCalls) {
    var this$1 = this;

    for (;;) {
      if (this$1.eat(tt.dot)) {
        var node = this$1.startNodeAt(startPos, startLoc)
        node.object = base
        node.property = this$1.parseIdent(true)
        node.computed = false
        base = this$1.finishNode(node, "MemberExpression")
      } else if (this$1.eat(tt.bracketL)) {
        var node$1 = this$1.startNodeAt(startPos, startLoc)
        node$1.object = base
        node$1.property = this$1.parseExpression()
        node$1.computed = true
        this$1.expect(tt.bracketR)
        base = this$1.finishNode(node$1, "MemberExpression")
      } else if (!noCalls && this$1.eat(tt.parenL)) {
        var node$2 = this$1.startNodeAt(startPos, startLoc)
        node$2.callee = base
        node$2.arguments = this$1.parseExprList(tt.parenR, false)
        base = this$1.finishNode(node$2, "CallExpression")
      } else if (this$1.type === tt.backQuote) {
        var node$3 = this$1.startNodeAt(startPos, startLoc)
        node$3.tag = base
        node$3.quasi = this$1.parseTemplate()
        base = this$1.finishNode(node$3, "TaggedTemplateExpression")
      } else {
        return base
      }
    }
  }

  // Parse an atomic expression — either a single token that is an
  // expression, an expression started by a keyword like `function` or
  // `new`, or an expression wrapped in punctuation like `()`, `[]`,
  // or `{}`.

  pp$3.parseExprAtom = function(refDestructuringErrors) {
    var node, canBeArrow = this.potentialArrowAt == this.start
    switch (this.type) {
    case tt._super:
      if (!this.inFunction)
        this.raise(this.start, "'super' outside of function or class")

    case tt._this:
      var type = this.type === tt._this ? "ThisExpression" : "Super"
      node = this.startNode()
      this.next()
      return this.finishNode(node, type)

    case tt.name:
      var startPos = this.start, startLoc = this.startLoc
      var id = this.parseIdent(this.type !== tt.name)
      if (canBeArrow && !this.canInsertSemicolon() && this.eat(tt.arrow))
        return this.parseArrowExpression(this.startNodeAt(startPos, startLoc), [id])
      return id

    case tt.regexp:
      var value = this.value
      node = this.parseLiteral(value.value)
      node.regex = {pattern: value.pattern, flags: value.flags}
      return node

    case tt.num: case tt.string:
      return this.parseLiteral(this.value)

    case tt._null: case tt._true: case tt._false:
      node = this.startNode()
      node.value = this.type === tt._null ? null : this.type === tt._true
      node.raw = this.type.keyword
      this.next()
      return this.finishNode(node, "Literal")

    case tt.parenL:
      return this.parseParenAndDistinguishExpression(canBeArrow)

    case tt.bracketL:
      node = this.startNode()
      this.next()
      node.elements = this.parseExprList(tt.bracketR, true, true, refDestructuringErrors)
      return this.finishNode(node, "ArrayExpression")

    case tt.braceL:
      return this.parseObj(false, refDestructuringErrors)

    case tt._function:
      node = this.startNode()
      this.next()
      return this.parseFunction(node, false)

    case tt._class:
      return this.parseClass(this.startNode(), false)

    case tt._new:
      return this.parseNew()

    case tt.backQuote:
      return this.parseTemplate()

    default:
      this.unexpected()
    }
  }

  pp$3.parseLiteral = function(value) {
    var node = this.startNode()
    node.value = value
    node.raw = this.input.slice(this.start, this.end)
    this.next()
    return this.finishNode(node, "Literal")
  }

  pp$3.parseParenExpression = function() {
    this.expect(tt.parenL)
    var val = this.parseExpression()
    this.expect(tt.parenR)
    return val
  }

  pp$3.parseParenAndDistinguishExpression = function(canBeArrow) {
    var this$1 = this;

    var startPos = this.start, startLoc = this.startLoc, val
    if (this.options.ecmaVersion >= 6) {
      this.next()

      var innerStartPos = this.start, innerStartLoc = this.startLoc
      var exprList = [], first = true
      var refDestructuringErrors = new DestructuringErrors, spreadStart, innerParenStart
      while (this.type !== tt.parenR) {
        first ? first = false : this$1.expect(tt.comma)
        if (this$1.type === tt.ellipsis) {
          spreadStart = this$1.start
          exprList.push(this$1.parseParenItem(this$1.parseRest()))
          break
        } else {
          if (this$1.type === tt.parenL && !innerParenStart) {
            innerParenStart = this$1.start
          }
          exprList.push(this$1.parseMaybeAssign(false, refDestructuringErrors, this$1.parseParenItem))
        }
      }
      var innerEndPos = this.start, innerEndLoc = this.startLoc
      this.expect(tt.parenR)

      if (canBeArrow && !this.canInsertSemicolon() && this.eat(tt.arrow)) {
        this.checkPatternErrors(refDestructuringErrors, true)
        if (innerParenStart) this.unexpected(innerParenStart)
        return this.parseParenArrowList(startPos, startLoc, exprList)
      }

      if (!exprList.length) this.unexpected(this.lastTokStart)
      if (spreadStart) this.unexpected(spreadStart)
      this.checkExpressionErrors(refDestructuringErrors, true)

      if (exprList.length > 1) {
        val = this.startNodeAt(innerStartPos, innerStartLoc)
        val.expressions = exprList
        this.finishNodeAt(val, "SequenceExpression", innerEndPos, innerEndLoc)
      } else {
        val = exprList[0]
      }
    } else {
      val = this.parseParenExpression()
    }

    if (this.options.preserveParens) {
      var par = this.startNodeAt(startPos, startLoc)
      par.expression = val
      return this.finishNode(par, "ParenthesizedExpression")
    } else {
      return val
    }
  }

  pp$3.parseParenItem = function(item) {
    return item
  }

  pp$3.parseParenArrowList = function(startPos, startLoc, exprList) {
    return this.parseArrowExpression(this.startNodeAt(startPos, startLoc), exprList)
  }

  // New's precedence is slightly tricky. It must allow its argument to
  // be a `[]` or dot subscript expression, but not a call — at least,
  // not without wrapping it in parentheses. Thus, it uses the noCalls
  // argument to parseSubscripts to prevent it from consuming the
  // argument list.

  var empty$1 = []

  pp$3.parseNew = function() {
    var node = this.startNode()
    var meta = this.parseIdent(true)
    if (this.options.ecmaVersion >= 6 && this.eat(tt.dot)) {
      node.meta = meta
      node.property = this.parseIdent(true)
      if (node.property.name !== "target")
        this.raiseRecoverable(node.property.start, "The only valid meta property for new is new.target")
      if (!this.inFunction)
        this.raiseRecoverable(node.start, "new.target can only be used in functions")
      return this.finishNode(node, "MetaProperty")
    }
    var startPos = this.start, startLoc = this.startLoc
    node.callee = this.parseSubscripts(this.parseExprAtom(), startPos, startLoc, true)
    if (this.eat(tt.parenL)) node.arguments = this.parseExprList(tt.parenR, false)
    else node.arguments = empty$1
    return this.finishNode(node, "NewExpression")
  }

  // Parse template expression.

  pp$3.parseTemplateElement = function() {
    var elem = this.startNode()
    elem.value = {
      raw: this.input.slice(this.start, this.end).replace(/\r\n?/g, '\n'),
      cooked: this.value
    }
    this.next()
    elem.tail = this.type === tt.backQuote
    return this.finishNode(elem, "TemplateElement")
  }

  pp$3.parseTemplate = function() {
    var this$1 = this;

    var node = this.startNode()
    this.next()
    node.expressions = []
    var curElt = this.parseTemplateElement()
    node.quasis = [curElt]
    while (!curElt.tail) {
      this$1.expect(tt.dollarBraceL)
      node.expressions.push(this$1.parseExpression())
      this$1.expect(tt.braceR)
      node.quasis.push(curElt = this$1.parseTemplateElement())
    }
    this.next()
    return this.finishNode(node, "TemplateLiteral")
  }

  // Parse an object literal or binding pattern.

  pp$3.parseObj = function(isPattern, refDestructuringErrors) {
    var this$1 = this;

    var node = this.startNode(), first = true, propHash = {}
    node.properties = []
    this.next()
    while (!this.eat(tt.braceR)) {
      if (!first) {
        this$1.expect(tt.comma)
        if (this$1.afterTrailingComma(tt.braceR)) break
      } else first = false

      var prop = this$1.startNode(), isGenerator, startPos, startLoc
      if (this$1.options.ecmaVersion >= 6) {
        prop.method = false
        prop.shorthand = false
        if (isPattern || refDestructuringErrors) {
          startPos = this$1.start
          startLoc = this$1.startLoc
        }
        if (!isPattern)
          isGenerator = this$1.eat(tt.star)
      }
      this$1.parsePropertyName(prop)
      this$1.parsePropertyValue(prop, isPattern, isGenerator, startPos, startLoc, refDestructuringErrors)
      this$1.checkPropClash(prop, propHash)
      node.properties.push(this$1.finishNode(prop, "Property"))
    }
    return this.finishNode(node, isPattern ? "ObjectPattern" : "ObjectExpression")
  }

  pp$3.parsePropertyValue = function(prop, isPattern, isGenerator, startPos, startLoc, refDestructuringErrors) {
    if (this.eat(tt.colon)) {
      prop.value = isPattern ? this.parseMaybeDefault(this.start, this.startLoc) : this.parseMaybeAssign(false, refDestructuringErrors)
      prop.kind = "init"
    } else if (this.options.ecmaVersion >= 6 && this.type === tt.parenL) {
      if (isPattern) this.unexpected()
      prop.kind = "init"
      prop.method = true
      prop.value = this.parseMethod(isGenerator)
    } else if (this.options.ecmaVersion >= 5 && !prop.computed && prop.key.type === "Identifier" &&
               (prop.key.name === "get" || prop.key.name === "set") &&
               (this.type != tt.comma && this.type != tt.braceR)) {
      if (isGenerator || isPattern) this.unexpected()
      prop.kind = prop.key.name
      this.parsePropertyName(prop)
      prop.value = this.parseMethod(false)
      var paramCount = prop.kind === "get" ? 0 : 1
      if (prop.value.params.length !== paramCount) {
        var start = prop.value.start
        if (prop.kind === "get")
          this.raiseRecoverable(start, "getter should have no params")
        else
          this.raiseRecoverable(start, "setter should have exactly one param")
      }
      if (prop.kind === "set" && prop.value.params[0].type === "RestElement")
        this.raiseRecoverable(prop.value.params[0].start, "Setter cannot use rest params")
    } else if (this.options.ecmaVersion >= 6 && !prop.computed && prop.key.type === "Identifier") {
      if (this.keywords.test(prop.key.name) ||
          (this.strict ? this.reservedWordsStrictBind : this.reservedWords).test(prop.key.name) ||
          (this.inGenerator && prop.key.name == "yield"))
        this.raiseRecoverable(prop.key.start, "'" + prop.key.name + "' can not be used as shorthand property")
      prop.kind = "init"
      if (isPattern) {
        prop.value = this.parseMaybeDefault(startPos, startLoc, prop.key)
      } else if (this.type === tt.eq && refDestructuringErrors) {
        if (!refDestructuringErrors.shorthandAssign)
          refDestructuringErrors.shorthandAssign = this.start
        prop.value = this.parseMaybeDefault(startPos, startLoc, prop.key)
      } else {
        prop.value = prop.key
      }
      prop.shorthand = true
    } else this.unexpected()
  }

  pp$3.parsePropertyName = function(prop) {
    if (this.options.ecmaVersion >= 6) {
      if (this.eat(tt.bracketL)) {
        prop.computed = true
        prop.key = this.parseMaybeAssign()
        this.expect(tt.bracketR)
        return prop.key
      } else {
        prop.computed = false
      }
    }
    return prop.key = this.type === tt.num || this.type === tt.string ? this.parseExprAtom() : this.parseIdent(true)
  }

  // Initialize empty function node.

  pp$3.initFunction = function(node) {
    node.id = null
    if (this.options.ecmaVersion >= 6) {
      node.generator = false
      node.expression = false
    }
  }

  // Parse object or class method.

  pp$3.parseMethod = function(isGenerator) {
    var node = this.startNode(), oldInGen = this.inGenerator
    this.inGenerator = isGenerator
    this.initFunction(node)
    this.expect(tt.parenL)
    node.params = this.parseBindingList(tt.parenR, false, false)
    if (this.options.ecmaVersion >= 6)
      node.generator = isGenerator
    this.parseFunctionBody(node, false)
    this.inGenerator = oldInGen
    return this.finishNode(node, "FunctionExpression")
  }

  // Parse arrow function expression with given parameters.

  pp$3.parseArrowExpression = function(node, params) {
    var oldInGen = this.inGenerator
    this.inGenerator = false
    this.initFunction(node)
    node.params = this.toAssignableList(params, true)
    this.parseFunctionBody(node, true)
    this.inGenerator = oldInGen
    return this.finishNode(node, "ArrowFunctionExpression")
  }

  // Parse function body and check parameters.

  pp$3.parseFunctionBody = function(node, isArrowFunction) {
    var isExpression = isArrowFunction && this.type !== tt.braceL

    if (isExpression) {
      node.body = this.parseMaybeAssign()
      node.expression = true
    } else {
      // Start a new scope with regard to labels and the `inFunction`
      // flag (restore them to their old value afterwards).
      var oldInFunc = this.inFunction, oldLabels = this.labels
      this.inFunction = true; this.labels = []
      node.body = this.parseBlock(true)
      node.expression = false
      this.inFunction = oldInFunc; this.labels = oldLabels
    }

    // If this is a strict mode function, verify that argument names
    // are not repeated, and it does not try to bind the words `eval`
    // or `arguments`.
    var useStrict = (!isExpression && node.body.body.length && this.isUseStrict(node.body.body[0])) ? node.body.body[0] : null;
    if (this.strict || useStrict) {
      var oldStrict = this.strict
      this.strict = true
      if (node.id)
        this.checkLVal(node.id, true)
      this.checkParams(node, useStrict)
      this.strict = oldStrict
    } else if (isArrowFunction) {
      this.checkParams(node, useStrict)
    }
  }

  // Checks function params for various disallowed patterns such as using "eval"
  // or "arguments" and duplicate parameters.

  pp$3.checkParams = function(node, useStrict) {
      var this$1 = this;

      var nameHash = {}
      for (var i = 0; i < node.params.length; i++) {
        if (useStrict && this$1.options.ecmaVersion >= 7 && node.params[i].type !== "Identifier")
          this$1.raiseRecoverable(useStrict.start, "Illegal 'use strict' directive in function with non-simple parameter list");
        this$1.checkLVal(node.params[i], true, nameHash)
      }
  }

  // Parses a comma-separated list of expressions, and returns them as
  // an array. `close` is the token type that ends the list, and
  // `allowEmpty` can be turned on to allow subsequent commas with
  // nothing in between them to be parsed as `null` (which is needed
  // for array literals).

  pp$3.parseExprList = function(close, allowTrailingComma, allowEmpty, refDestructuringErrors) {
    var this$1 = this;

    var elts = [], first = true
    while (!this.eat(close)) {
      if (!first) {
        this$1.expect(tt.comma)
        if (allowTrailingComma && this$1.afterTrailingComma(close)) break
      } else first = false

      var elt
      if (allowEmpty && this$1.type === tt.comma)
        elt = null
      else if (this$1.type === tt.ellipsis) {
        elt = this$1.parseSpread(refDestructuringErrors)
        if (this$1.type === tt.comma && refDestructuringErrors && !refDestructuringErrors.trailingComma) {
          refDestructuringErrors.trailingComma = this$1.lastTokStart
        }
      } else
        elt = this$1.parseMaybeAssign(false, refDestructuringErrors)
      elts.push(elt)
    }
    return elts
  }

  // Parse the next token as an identifier. If `liberal` is true (used
  // when parsing properties), it will also convert keywords into
  // identifiers.

  pp$3.parseIdent = function(liberal) {
    var node = this.startNode()
    if (liberal && this.options.allowReserved == "never") liberal = false
    if (this.type === tt.name) {
      if (!liberal && (this.strict ? this.reservedWordsStrict : this.reservedWords).test(this.value) &&
          (this.options.ecmaVersion >= 6 ||
           this.input.slice(this.start, this.end).indexOf("\\") == -1))
        this.raiseRecoverable(this.start, "The keyword '" + this.value + "' is reserved")
      if (!liberal && this.inGenerator && this.value === "yield")
        this.raiseRecoverable(this.start, "Can not use 'yield' as identifier inside a generator")
      node.name = this.value
    } else if (liberal && this.type.keyword) {
      node.name = this.type.keyword
    } else {
      this.unexpected()
    }
    this.next()
    return this.finishNode(node, "Identifier")
  }

  // Parses yield expression inside generator.

  pp$3.parseYield = function() {
    var node = this.startNode()
    this.next()
    if (this.type == tt.semi || this.canInsertSemicolon() || (this.type != tt.star && !this.type.startsExpr)) {
      node.delegate = false
      node.argument = null
    } else {
      node.delegate = this.eat(tt.star)
      node.argument = this.parseMaybeAssign()
    }
    return this.finishNode(node, "YieldExpression")
  }

  var pp$4 = Parser.prototype

  // This function is used to raise exceptions on parse errors. It
  // takes an offset integer (into the current `input`) to indicate
  // the location of the error, attaches the position to the end
  // of the error message, and then raises a `SyntaxError` with that
  // message.

  pp$4.raise = function(pos, message) {
    var loc = getLineInfo(this.input, pos)
    message += " (" + loc.line + ":" + loc.column + ")"
    var err = new SyntaxError(message)
    err.pos = pos; err.loc = loc; err.raisedAt = this.pos
    throw err
  }

  pp$4.raiseRecoverable = pp$4.raise

  pp$4.curPosition = function() {
    if (this.options.locations) {
      return new Position(this.curLine, this.pos - this.lineStart)
    }
  }

  var Node = function Node(parser, pos, loc) {
    this.type = ""
    this.start = pos
    this.end = 0
    if (parser.options.locations)
      this.loc = new SourceLocation(parser, loc)
    if (parser.options.directSourceFile)
      this.sourceFile = parser.options.directSourceFile
    if (parser.options.ranges)
      this.range = [pos, 0]
  };

  // Start an AST node, attaching a start offset.

  var pp$5 = Parser.prototype

  pp$5.startNode = function() {
    return new Node(this, this.start, this.startLoc)
  }

  pp$5.startNodeAt = function(pos, loc) {
    return new Node(this, pos, loc)
  }

  // Finish an AST node, adding `type` and `end` properties.

  function finishNodeAt(node, type, pos, loc) {
    node.type = type
    node.end = pos
    if (this.options.locations)
      node.loc.end = loc
    if (this.options.ranges)
      node.range[1] = pos
    return node
  }

  pp$5.finishNode = function(node, type) {
    return finishNodeAt.call(this, node, type, this.lastTokEnd, this.lastTokEndLoc)
  }

  // Finish node at given position

  pp$5.finishNodeAt = function(node, type, pos, loc) {
    return finishNodeAt.call(this, node, type, pos, loc)
  }

  var TokContext = function TokContext(token, isExpr, preserveSpace, override) {
    this.token = token
    this.isExpr = !!isExpr
    this.preserveSpace = !!preserveSpace
    this.override = override
  };

  var types = {
    b_stat: new TokContext("{", false),
    b_expr: new TokContext("{", true),
    b_tmpl: new TokContext("${", true),
    p_stat: new TokContext("(", false),
    p_expr: new TokContext("(", true),
    q_tmpl: new TokContext("`", true, true, function (p) { return p.readTmplToken(); }),
    f_expr: new TokContext("function", true)
  }

  var pp$6 = Parser.prototype

  pp$6.initialContext = function() {
    return [types.b_stat]
  }

  pp$6.braceIsBlock = function(prevType) {
    if (prevType === tt.colon) {
      var parent = this.curContext()
      if (parent === types.b_stat || parent === types.b_expr)
        return !parent.isExpr
    }
    if (prevType === tt._return)
      return lineBreak.test(this.input.slice(this.lastTokEnd, this.start))
    if (prevType === tt._else || prevType === tt.semi || prevType === tt.eof || prevType === tt.parenR)
      return true
    if (prevType == tt.braceL)
      return this.curContext() === types.b_stat
    return !this.exprAllowed
  }

  pp$6.updateContext = function(prevType) {
    var update, type = this.type
    if (type.keyword && prevType == tt.dot)
      this.exprAllowed = false
    else if (update = type.updateContext)
      update.call(this, prevType)
    else
      this.exprAllowed = type.beforeExpr
  }

  // Token-specific context update code

  tt.parenR.updateContext = tt.braceR.updateContext = function() {
    if (this.context.length == 1) {
      this.exprAllowed = true
      return
    }
    var out = this.context.pop()
    if (out === types.b_stat && this.curContext() === types.f_expr) {
      this.context.pop()
      this.exprAllowed = false
    } else if (out === types.b_tmpl) {
      this.exprAllowed = true
    } else {
      this.exprAllowed = !out.isExpr
    }
  }

  tt.braceL.updateContext = function(prevType) {
    this.context.push(this.braceIsBlock(prevType) ? types.b_stat : types.b_expr)
    this.exprAllowed = true
  }

  tt.dollarBraceL.updateContext = function() {
    this.context.push(types.b_tmpl)
    this.exprAllowed = true
  }

  tt.parenL.updateContext = function(prevType) {
    var statementParens = prevType === tt._if || prevType === tt._for || prevType === tt._with || prevType === tt._while
    this.context.push(statementParens ? types.p_stat : types.p_expr)
    this.exprAllowed = true
  }

  tt.incDec.updateContext = function() {
    // tokExprAllowed stays unchanged
  }

  tt._function.updateContext = function(prevType) {
    if (prevType.beforeExpr && prevType !== tt.semi && prevType !== tt._else &&
        !((prevType === tt.colon || prevType === tt.braceL) && this.curContext() === types.b_stat))
      this.context.push(types.f_expr)
    this.exprAllowed = false
  }

  tt.backQuote.updateContext = function() {
    if (this.curContext() === types.q_tmpl)
      this.context.pop()
    else
      this.context.push(types.q_tmpl)
    this.exprAllowed = false
  }

  // Object type used to represent tokens. Note that normally, tokens
  // simply exist as properties on the parser object. This is only
  // used for the onToken callback and the external tokenizer.

  var Token = function Token(p) {
    this.type = p.type
    this.value = p.value
    this.start = p.start
    this.end = p.end
    if (p.options.locations)
      this.loc = new SourceLocation(p, p.startLoc, p.endLoc)
    if (p.options.ranges)
      this.range = [p.start, p.end]
  };

  // ## Tokenizer

  var pp$7 = Parser.prototype

  // Are we running under Rhino?
  var isRhino = typeof Packages == "object" && Object.prototype.toString.call(Packages) == "[object JavaPackage]"

  // Move to the next token

  pp$7.next = function() {
    if (this.options.onToken)
      this.options.onToken(new Token(this))

    this.lastTokEnd = this.end
    this.lastTokStart = this.start
    this.lastTokEndLoc = this.endLoc
    this.lastTokStartLoc = this.startLoc
    this.nextToken()
  }

  pp$7.getToken = function() {
    this.next()
    return new Token(this)
  }

  // If we're in an ES6 environment, make parsers iterable
  if (typeof Symbol !== "undefined")
    pp$7[Symbol.iterator] = function () {
      var self = this
      return {next: function () {
        var token = self.getToken()
        return {
          done: token.type === tt.eof,
          value: token
        }
      }}
    }

  // Toggle strict mode. Re-reads the next number or string to please
  // pedantic tests (`"use strict"; 010;` should fail).

  pp$7.setStrict = function(strict) {
    var this$1 = this;

    this.strict = strict
    if (this.type !== tt.num && this.type !== tt.string) return
    this.pos = this.start
    if (this.options.locations) {
      while (this.pos < this.lineStart) {
        this$1.lineStart = this$1.input.lastIndexOf("\n", this$1.lineStart - 2) + 1
        --this$1.curLine
      }
    }
    this.nextToken()
  }

  pp$7.curContext = function() {
    return this.context[this.context.length - 1]
  }

  // Read a single token, updating the parser object's token-related
  // properties.

  pp$7.nextToken = function() {
    var curContext = this.curContext()
    if (!curContext || !curContext.preserveSpace) this.skipSpace()

    this.start = this.pos
    if (this.options.locations) this.startLoc = this.curPosition()
    if (this.pos >= this.input.length) return this.finishToken(tt.eof)

    if (curContext.override) return curContext.override(this)
    else this.readToken(this.fullCharCodeAtPos())
  }

  pp$7.readToken = function(code) {
    // Identifier or keyword. '\uXXXX' sequences are allowed in
    // identifiers, so '\' also dispatches to that.
    if (isIdentifierStart(code, this.options.ecmaVersion >= 6) || code === 92 /* '\' */)
      return this.readWord()

    return this.getTokenFromCode(code)
  }

  pp$7.fullCharCodeAtPos = function() {
    var code = this.input.charCodeAt(this.pos)
    if (code <= 0xd7ff || code >= 0xe000) return code
    var next = this.input.charCodeAt(this.pos + 1)
    return (code << 10) + next - 0x35fdc00
  }

  pp$7.skipBlockComment = function() {
    var this$1 = this;

    var startLoc = this.options.onComment && this.curPosition()
    var start = this.pos, end = this.input.indexOf("*/", this.pos += 2)
    if (end === -1) this.raise(this.pos - 2, "Unterminated comment")
    this.pos = end + 2
    if (this.options.locations) {
      lineBreakG.lastIndex = start
      var match
      while ((match = lineBreakG.exec(this.input)) && match.index < this.pos) {
        ++this$1.curLine
        this$1.lineStart = match.index + match[0].length
      }
    }
    if (this.options.onComment)
      this.options.onComment(true, this.input.slice(start + 2, end), start, this.pos,
                             startLoc, this.curPosition())
  }

  pp$7.skipLineComment = function(startSkip) {
    var this$1 = this;

    var start = this.pos
    var startLoc = this.options.onComment && this.curPosition()
    var ch = this.input.charCodeAt(this.pos+=startSkip)
    while (this.pos < this.input.length && ch !== 10 && ch !== 13 && ch !== 8232 && ch !== 8233) {
      ++this$1.pos
      ch = this$1.input.charCodeAt(this$1.pos)
    }
    if (this.options.onComment)
      this.options.onComment(false, this.input.slice(start + startSkip, this.pos), start, this.pos,
                             startLoc, this.curPosition())
  }

  // Called at the start of the parse and after every token. Skips
  // whitespace and comments, and.

  pp$7.skipSpace = function() {
    var this$1 = this;

    loop: while (this.pos < this.input.length) {
      var ch = this$1.input.charCodeAt(this$1.pos)
      switch (ch) {
        case 32: case 160: // ' '
          ++this$1.pos
          break
        case 13:
          if (this$1.input.charCodeAt(this$1.pos + 1) === 10) {
            ++this$1.pos
          }
        case 10: case 8232: case 8233:
          ++this$1.pos
          if (this$1.options.locations) {
            ++this$1.curLine
            this$1.lineStart = this$1.pos
          }
          break
        case 47: // '/'
          switch (this$1.input.charCodeAt(this$1.pos + 1)) {
            case 42: // '*'
              this$1.skipBlockComment()
              break
            case 47:
              this$1.skipLineComment(2)
              break
            default:
              break loop
          }
          break
        default:
          if (ch > 8 && ch < 14 || ch >= 5760 && nonASCIIwhitespace.test(String.fromCharCode(ch))) {
            ++this$1.pos
          } else {
            break loop
          }
      }
    }
  }

  // Called at the end of every token. Sets `end`, `val`, and
  // maintains `context` and `exprAllowed`, and skips the space after
  // the token, so that the next one's `start` will point at the
  // right position.

  pp$7.finishToken = function(type, val) {
    this.end = this.pos
    if (this.options.locations) this.endLoc = this.curPosition()
    var prevType = this.type
    this.type = type
    this.value = val

    this.updateContext(prevType)
  }

  // ### Token reading

  // This is the function that is called to fetch the next token. It
  // is somewhat obscure, because it works in character codes rather
  // than characters, and because operator parsing has been inlined
  // into it.
  //
  // All in the name of speed.
  //
  pp$7.readToken_dot = function() {
    var next = this.input.charCodeAt(this.pos + 1)
    if (next >= 48 && next <= 57) return this.readNumber(true)
    var next2 = this.input.charCodeAt(this.pos + 2)
    if (this.options.ecmaVersion >= 6 && next === 46 && next2 === 46) { // 46 = dot '.'
      this.pos += 3
      return this.finishToken(tt.ellipsis)
    } else {
      ++this.pos
      return this.finishToken(tt.dot)
    }
  }

  pp$7.readToken_slash = function() { // '/'
    var next = this.input.charCodeAt(this.pos + 1)
    if (this.exprAllowed) {++this.pos; return this.readRegexp()}
    if (next === 61) return this.finishOp(tt.assign, 2)
    return this.finishOp(tt.slash, 1)
  }

  pp$7.readToken_mult_modulo_exp = function(code) { // '%*'
    var next = this.input.charCodeAt(this.pos + 1)
    var size = 1
    var tokentype = code === 42 ? tt.star : tt.modulo

    // exponentiation operator ** and **=
    if (this.options.ecmaVersion >= 7 && next === 42) {
      ++size
      tokentype = tt.starstar
      next = this.input.charCodeAt(this.pos + 2)
    }

    if (next === 61) return this.finishOp(tt.assign, size + 1)
    return this.finishOp(tokentype, size)
  }

  pp$7.readToken_pipe_amp = function(code) { // '|&'
    var next = this.input.charCodeAt(this.pos + 1)
    if (next === code) return this.finishOp(code === 124 ? tt.logicalOR : tt.logicalAND, 2)
    if (next === 61) return this.finishOp(tt.assign, 2)
    return this.finishOp(code === 124 ? tt.bitwiseOR : tt.bitwiseAND, 1)
  }

  pp$7.readToken_caret = function() { // '^'
    var next = this.input.charCodeAt(this.pos + 1)
    if (next === 61) return this.finishOp(tt.assign, 2)
    return this.finishOp(tt.bitwiseXOR, 1)
  }

  pp$7.readToken_plus_min = function(code) { // '+-'
    var next = this.input.charCodeAt(this.pos + 1)
    if (next === code) {
      if (next == 45 && this.input.charCodeAt(this.pos + 2) == 62 &&
          lineBreak.test(this.input.slice(this.lastTokEnd, this.pos))) {
        // A `-->` line comment
        this.skipLineComment(3)
        this.skipSpace()
        return this.nextToken()
      }
      return this.finishOp(tt.incDec, 2)
    }
    if (next === 61) return this.finishOp(tt.assign, 2)
    return this.finishOp(tt.plusMin, 1)
  }

  pp$7.readToken_lt_gt = function(code) { // '<>'
    var next = this.input.charCodeAt(this.pos + 1)
    var size = 1
    if (next === code) {
      size = code === 62 && this.input.charCodeAt(this.pos + 2) === 62 ? 3 : 2
      if (this.input.charCodeAt(this.pos + size) === 61) return this.finishOp(tt.assign, size + 1)
      return this.finishOp(tt.bitShift, size)
    }
    if (next == 33 && code == 60 && this.input.charCodeAt(this.pos + 2) == 45 &&
        this.input.charCodeAt(this.pos + 3) == 45) {
      if (this.inModule) this.unexpected()
      // `<!--`, an XML-style comment that should be interpreted as a line comment
      this.skipLineComment(4)
      this.skipSpace()
      return this.nextToken()
    }
    if (next === 61) size = 2
    return this.finishOp(tt.relational, size)
  }

  pp$7.readToken_eq_excl = function(code) { // '=!'
    var next = this.input.charCodeAt(this.pos + 1)
    if (next === 61) return this.finishOp(tt.equality, this.input.charCodeAt(this.pos + 2) === 61 ? 3 : 2)
    if (code === 61 && next === 62 && this.options.ecmaVersion >= 6) { // '=>'
      this.pos += 2
      return this.finishToken(tt.arrow)
    }
    return this.finishOp(code === 61 ? tt.eq : tt.prefix, 1)
  }

  pp$7.getTokenFromCode = function(code) {
    switch (code) {
      // The interpretation of a dot depends on whether it is followed
      // by a digit or another two dots.
    case 46: // '.'
      return this.readToken_dot()

      // Punctuation tokens.
    case 40: ++this.pos; return this.finishToken(tt.parenL)
    case 41: ++this.pos; return this.finishToken(tt.parenR)
    case 59: ++this.pos; return this.finishToken(tt.semi)
    case 44: ++this.pos; return this.finishToken(tt.comma)
    case 91: ++this.pos; return this.finishToken(tt.bracketL)
    case 93: ++this.pos; return this.finishToken(tt.bracketR)
    case 123: ++this.pos; return this.finishToken(tt.braceL)
    case 125: ++this.pos; return this.finishToken(tt.braceR)
    case 58: ++this.pos; return this.finishToken(tt.colon)
    case 63: ++this.pos; return this.finishToken(tt.question)

    case 96: // '`'
      if (this.options.ecmaVersion < 6) break
      ++this.pos
      return this.finishToken(tt.backQuote)

    case 48: // '0'
      var next = this.input.charCodeAt(this.pos + 1)
      if (next === 120 || next === 88) return this.readRadixNumber(16) // '0x', '0X' - hex number
      if (this.options.ecmaVersion >= 6) {
        if (next === 111 || next === 79) return this.readRadixNumber(8) // '0o', '0O' - octal number
        if (next === 98 || next === 66) return this.readRadixNumber(2) // '0b', '0B' - binary number
      }
      // Anything else beginning with a digit is an integer, octal
      // number, or float.
    case 49: case 50: case 51: case 52: case 53: case 54: case 55: case 56: case 57: // 1-9
      return this.readNumber(false)

      // Quotes produce strings.
    case 34: case 39: // '"', "'"
      return this.readString(code)

      // Operators are parsed inline in tiny state machines. '=' (61) is
      // often referred to. `finishOp` simply skips the amount of
      // characters it is given as second argument, and returns a token
      // of the type given by its first argument.

    case 47: // '/'
      return this.readToken_slash()

    case 37: case 42: // '%*'
      return this.readToken_mult_modulo_exp(code)

    case 124: case 38: // '|&'
      return this.readToken_pipe_amp(code)

    case 94: // '^'
      return this.readToken_caret()

    case 43: case 45: // '+-'
      return this.readToken_plus_min(code)

    case 60: case 62: // '<>'
      return this.readToken_lt_gt(code)

    case 61: case 33: // '=!'
      return this.readToken_eq_excl(code)

    case 126: // '~'
      return this.finishOp(tt.prefix, 1)
    }

    this.raise(this.pos, "Unexpected character '" + codePointToString(code) + "'")
  }

  pp$7.finishOp = function(type, size) {
    var str = this.input.slice(this.pos, this.pos + size)
    this.pos += size
    return this.finishToken(type, str)
  }

  // Parse a regular expression. Some context-awareness is necessary,
  // since a '/' inside a '[]' set does not end the expression.

  function tryCreateRegexp(src, flags, throwErrorAt, parser) {
    try {
      return new RegExp(src, flags)
    } catch (e) {
      if (throwErrorAt !== undefined) {
        if (e instanceof SyntaxError) parser.raise(throwErrorAt, "Error parsing regular expression: " + e.message)
        throw e
      }
    }
  }

  var regexpUnicodeSupport = !!tryCreateRegexp("\uffff", "u")

  pp$7.readRegexp = function() {
    var this$1 = this;

    var escaped, inClass, start = this.pos
    for (;;) {
      if (this$1.pos >= this$1.input.length) this$1.raise(start, "Unterminated regular expression")
      var ch = this$1.input.charAt(this$1.pos)
      if (lineBreak.test(ch)) this$1.raise(start, "Unterminated regular expression")
      if (!escaped) {
        if (ch === "[") inClass = true
        else if (ch === "]" && inClass) inClass = false
        else if (ch === "/" && !inClass) break
        escaped = ch === "\\"
      } else escaped = false
      ++this$1.pos
    }
    var content = this.input.slice(start, this.pos)
    ++this.pos
    // Need to use `readWord1` because '\uXXXX' sequences are allowed
    // here (don't ask).
    var mods = this.readWord1()
    var tmp = content, tmpFlags = ""
    if (mods) {
      var validFlags = /^[gim]*$/
      if (this.options.ecmaVersion >= 6) validFlags = /^[gimuy]*$/
      if (!validFlags.test(mods)) this.raise(start, "Invalid regular expression flag")
      if (mods.indexOf("u") >= 0) {
        if (regexpUnicodeSupport) {
          tmpFlags = "u"
        } else {
          // Replace each astral symbol and every Unicode escape sequence that
          // possibly represents an astral symbol or a paired surrogate with a
          // single ASCII symbol to avoid throwing on regular expressions that
          // are only valid in combination with the `/u` flag.
          // Note: replacing with the ASCII symbol `x` might cause false
          // negatives in unlikely scenarios. For example, `[\u{61}-b]` is a
          // perfectly valid pattern that is equivalent to `[a-b]`, but it would
          // be replaced by `[x-b]` which throws an error.
          tmp = tmp.replace(/\\u\{([0-9a-fA-F]+)\}/g, function (_match, code, offset) {
            code = Number("0x" + code)
            if (code > 0x10FFFF) this$1.raise(start + offset + 3, "Code point out of bounds")
            return "x"
          })
          tmp = tmp.replace(/\\u([a-fA-F0-9]{4})|[\uD800-\uDBFF][\uDC00-\uDFFF]/g, "x")
          tmpFlags = tmpFlags.replace("u", "")
        }
      }
    }
    // Detect invalid regular expressions.
    var value = null
    // Rhino's regular expression parser is flaky and throws uncatchable exceptions,
    // so don't do detection if we are running under Rhino
    if (!isRhino) {
      tryCreateRegexp(tmp, tmpFlags, start, this)
      // Get a regular expression object for this pattern-flag pair, or `null` in
      // case the current environment doesn't support the flags it uses.
      value = tryCreateRegexp(content, mods)
    }
    return this.finishToken(tt.regexp, {pattern: content, flags: mods, value: value})
  }

  // Read an integer in the given radix. Return null if zero digits
  // were read, the integer value otherwise. When `len` is given, this
  // will return `null` unless the integer has exactly `len` digits.

  pp$7.readInt = function(radix, len) {
    var this$1 = this;

    var start = this.pos, total = 0
    for (var i = 0, e = len == null ? Infinity : len; i < e; ++i) {
      var code = this$1.input.charCodeAt(this$1.pos), val
      if (code >= 97) val = code - 97 + 10 // a
      else if (code >= 65) val = code - 65 + 10 // A
      else if (code >= 48 && code <= 57) val = code - 48 // 0-9
      else val = Infinity
      if (val >= radix) break
      ++this$1.pos
      total = total * radix + val
    }
    if (this.pos === start || len != null && this.pos - start !== len) return null

    return total
  }

  pp$7.readRadixNumber = function(radix) {
    this.pos += 2 // 0x
    var val = this.readInt(radix)
    if (val == null) this.raise(this.start + 2, "Expected number in radix " + radix)
    if (isIdentifierStart(this.fullCharCodeAtPos())) this.raise(this.pos, "Identifier directly after number")
    return this.finishToken(tt.num, val)
  }

  // Read an integer, octal integer, or floating-point number.

  pp$7.readNumber = function(startsWithDot) {
    var start = this.pos, isFloat = false, octal = this.input.charCodeAt(this.pos) === 48
    if (!startsWithDot && this.readInt(10) === null) this.raise(start, "Invalid number")
    var next = this.input.charCodeAt(this.pos)
    if (next === 46) { // '.'
      ++this.pos
      this.readInt(10)
      isFloat = true
      next = this.input.charCodeAt(this.pos)
    }
    if (next === 69 || next === 101) { // 'eE'
      next = this.input.charCodeAt(++this.pos)
      if (next === 43 || next === 45) ++this.pos // '+-'
      if (this.readInt(10) === null) this.raise(start, "Invalid number")
      isFloat = true
    }
    if (isIdentifierStart(this.fullCharCodeAtPos())) this.raise(this.pos, "Identifier directly after number")

    var str = this.input.slice(start, this.pos), val
    if (isFloat) val = parseFloat(str)
    else if (!octal || str.length === 1) val = parseInt(str, 10)
    else if (/[89]/.test(str) || this.strict) this.raise(start, "Invalid number")
    else val = parseInt(str, 8)
    return this.finishToken(tt.num, val)
  }

  // Read a string value, interpreting backslash-escapes.

  pp$7.readCodePoint = function() {
    var ch = this.input.charCodeAt(this.pos), code

    if (ch === 123) {
      if (this.options.ecmaVersion < 6) this.unexpected()
      var codePos = ++this.pos
      code = this.readHexChar(this.input.indexOf('}', this.pos) - this.pos)
      ++this.pos
      if (code > 0x10FFFF) this.raise(codePos, "Code point out of bounds")
    } else {
      code = this.readHexChar(4)
    }
    return code
  }

  function codePointToString(code) {
    // UTF-16 Decoding
    if (code <= 0xFFFF) return String.fromCharCode(code)
    code -= 0x10000
    return String.fromCharCode((code >> 10) + 0xD800, (code & 1023) + 0xDC00)
  }

  pp$7.readString = function(quote) {
    var this$1 = this;

    var out = "", chunkStart = ++this.pos
    for (;;) {
      if (this$1.pos >= this$1.input.length) this$1.raise(this$1.start, "Unterminated string constant")
      var ch = this$1.input.charCodeAt(this$1.pos)
      if (ch === quote) break
      if (ch === 92) { // '\'
        out += this$1.input.slice(chunkStart, this$1.pos)
        out += this$1.readEscapedChar(false)
        chunkStart = this$1.pos
      } else {
        if (isNewLine(ch)) this$1.raise(this$1.start, "Unterminated string constant")
        ++this$1.pos
      }
    }
    out += this.input.slice(chunkStart, this.pos++)
    return this.finishToken(tt.string, out)
  }

  // Reads template string tokens.

  pp$7.readTmplToken = function() {
    var this$1 = this;

    var out = "", chunkStart = this.pos
    for (;;) {
      if (this$1.pos >= this$1.input.length) this$1.raise(this$1.start, "Unterminated template")
      var ch = this$1.input.charCodeAt(this$1.pos)
      if (ch === 96 || ch === 36 && this$1.input.charCodeAt(this$1.pos + 1) === 123) { // '`', '${'
        if (this$1.pos === this$1.start && this$1.type === tt.template) {
          if (ch === 36) {
            this$1.pos += 2
            return this$1.finishToken(tt.dollarBraceL)
          } else {
            ++this$1.pos
            return this$1.finishToken(tt.backQuote)
          }
        }
        out += this$1.input.slice(chunkStart, this$1.pos)
        return this$1.finishToken(tt.template, out)
      }
      if (ch === 92) { // '\'
        out += this$1.input.slice(chunkStart, this$1.pos)
        out += this$1.readEscapedChar(true)
        chunkStart = this$1.pos
      } else if (isNewLine(ch)) {
        out += this$1.input.slice(chunkStart, this$1.pos)
        ++this$1.pos
        switch (ch) {
          case 13:
            if (this$1.input.charCodeAt(this$1.pos) === 10) ++this$1.pos
          case 10:
            out += "\n"
            break
          default:
            out += String.fromCharCode(ch)
            break
        }
        if (this$1.options.locations) {
          ++this$1.curLine
          this$1.lineStart = this$1.pos
        }
        chunkStart = this$1.pos
      } else {
        ++this$1.pos
      }
    }
  }

  // Used to read escaped characters

  pp$7.readEscapedChar = function(inTemplate) {
    var ch = this.input.charCodeAt(++this.pos)
    ++this.pos
    switch (ch) {
    case 110: return "\n" // 'n' -> '\n'
    case 114: return "\r" // 'r' -> '\r'
    case 120: return String.fromCharCode(this.readHexChar(2)) // 'x'
    case 117: return codePointToString(this.readCodePoint()) // 'u'
    case 116: return "\t" // 't' -> '\t'
    case 98: return "\b" // 'b' -> '\b'
    case 118: return "\u000b" // 'v' -> '\u000b'
    case 102: return "\f" // 'f' -> '\f'
    case 13: if (this.input.charCodeAt(this.pos) === 10) ++this.pos // '\r\n'
    case 10: // ' \n'
      if (this.options.locations) { this.lineStart = this.pos; ++this.curLine }
      return ""
    default:
      if (ch >= 48 && ch <= 55) {
        var octalStr = this.input.substr(this.pos - 1, 3).match(/^[0-7]+/)[0]
        var octal = parseInt(octalStr, 8)
        if (octal > 255) {
          octalStr = octalStr.slice(0, -1)
          octal = parseInt(octalStr, 8)
        }
        if (octalStr !== "0" && (this.strict || inTemplate)) {
          this.raise(this.pos - 2, "Octal literal in strict mode")
        }
        this.pos += octalStr.length - 1
        return String.fromCharCode(octal)
      }
      return String.fromCharCode(ch)
    }
  }

  // Used to read character escape sequences ('\x', '\u', '\U').

  pp$7.readHexChar = function(len) {
    var codePos = this.pos
    var n = this.readInt(16, len)
    if (n === null) this.raise(codePos, "Bad character escape sequence")
    return n
  }

  // Read an identifier, and return it as a string. Sets `this.containsEsc`
  // to whether the word contained a '\u' escape.
  //
  // Incrementally adds only escaped chars, adding other chunks as-is
  // as a micro-optimization.

  pp$7.readWord1 = function() {
    var this$1 = this;

    this.containsEsc = false
    var word = "", first = true, chunkStart = this.pos
    var astral = this.options.ecmaVersion >= 6
    while (this.pos < this.input.length) {
      var ch = this$1.fullCharCodeAtPos()
      if (isIdentifierChar(ch, astral)) {
        this$1.pos += ch <= 0xffff ? 1 : 2
      } else if (ch === 92) { // "\"
        this$1.containsEsc = true
        word += this$1.input.slice(chunkStart, this$1.pos)
        var escStart = this$1.pos
        if (this$1.input.charCodeAt(++this$1.pos) != 117) // "u"
          this$1.raise(this$1.pos, "Expecting Unicode escape sequence \\uXXXX")
        ++this$1.pos
        var esc = this$1.readCodePoint()
        if (!(first ? isIdentifierStart : isIdentifierChar)(esc, astral))
          this$1.raise(escStart, "Invalid Unicode escape")
        word += codePointToString(esc)
        chunkStart = this$1.pos
      } else {
        break
      }
      first = false
    }
    return word + this.input.slice(chunkStart, this.pos)
  }

  // Read an identifier or keyword token. Will check for reserved
  // words when necessary.

  pp$7.readWord = function() {
    var word = this.readWord1()
    var type = tt.name
    if ((this.options.ecmaVersion >= 6 || !this.containsEsc) && this.keywords.test(word))
      type = keywordTypes[word]
    return this.finishToken(type, word)
  }

  var version = "3.3.0"

  // The main exported interface (under `self.acorn` when in the
  // browser) is a `parse` function that takes a code string and
  // returns an abstract syntax tree as specified by [Mozilla parser
  // API][api].
  //
  // [api]: https://developer.mozilla.org/en-US/docs/SpiderMonkey/Parser_API

  function parse(input, options) {
    return new Parser(options, input).parse()
  }

  // This function tries to parse a single expression at a given
  // offset in a string. Useful for parsing mixed-language formats
  // that embed JavaScript expressions.

  function parseExpressionAt(input, pos, options) {
    var p = new Parser(options, input, pos)
    p.nextToken()
    return p.parseExpression()
  }

  // Acorn is organized as a tokenizer and a recursive-descent parser.
  // The `tokenizer` export provides an interface to the tokenizer.

  function tokenizer(input, options) {
    return new Parser(options, input)
  }

  exports.version = version;
  exports.parse = parse;
  exports.parseExpressionAt = parseExpressionAt;
  exports.tokenizer = tokenizer;
  exports.Parser = Parser;
  exports.plugins = plugins;
  exports.defaultOptions = defaultOptions;
  exports.Position = Position;
  exports.SourceLocation = SourceLocation;
  exports.getLineInfo = getLineInfo;
  exports.Node = Node;
  exports.TokenType = TokenType;
  exports.tokTypes = tt;
  exports.TokContext = TokContext;
  exports.tokContexts = types;
  exports.isIdentifierChar = isIdentifierChar;
  exports.isIdentifierStart = isIdentifierStart;
  exports.Token = Token;
  exports.isNewLine = isNewLine;
  exports.lineBreak = lineBreak;
  exports.lineBreakG = lineBreakG;

  Object.defineProperty(exports, '__esModule', { value: true });

}));

/***/ }),

/***/ "./node_modules/paper/dist/paper-full.js":
/*!***********************************************!*\
  !*** ./node_modules/paper/dist/paper-full.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
 * Paper.js v0.12.0 - The Swiss Army Knife of Vector Graphics Scripting.
 * http://paperjs.org/
 *
 * Copyright (c) 2011 - 2016, Juerg Lehni & Jonathan Puckey
 * http://scratchdisk.com/ & https://puckey.studio/
 *
 * Distributed under the MIT license. See LICENSE file for details.
 *
 * All rights reserved.
 *
 * Date: Mon Dec 3 14:19:11 2018 +0100
 *
 ***
 *
 * Straps.js - Class inheritance library with support for bean-style accessors
 *
 * Copyright (c) 2006 - 2016 Juerg Lehni
 * http://scratchdisk.com/
 *
 * Distributed under the MIT license.
 *
 ***
 *
 * Acorn.js
 * https://marijnhaverbeke.nl/acorn/
 *
 * Acorn is a tiny, fast JavaScript parser written in JavaScript,
 * created by Marijn Haverbeke and released under an MIT license.
 *
 */

var paper = function(self, undefined) {

self = self || __webpack_require__(/*! ./node/self.js */ 0);
var window = self.window,
	document = self.document;

var Base = new function() {
	var hidden = /^(statics|enumerable|beans|preserve)$/,
		array = [],
		slice = array.slice,
		create = Object.create,
		describe = Object.getOwnPropertyDescriptor,
		define = Object.defineProperty,

		forEach = array.forEach || function(iter, bind) {
			for (var i = 0, l = this.length; i < l; i++) {
				iter.call(bind, this[i], i, this);
			}
		},

		forIn = function(iter, bind) {
			for (var i in this) {
				if (this.hasOwnProperty(i))
					iter.call(bind, this[i], i, this);
			}
		},

		set = Object.assign || function(dst) {
			for (var i = 1, l = arguments.length; i < l; i++) {
				var src = arguments[i];
				for (var key in src) {
					if (src.hasOwnProperty(key))
						dst[key] = src[key];
				}
			}
			return dst;
		},

		each = function(obj, iter, bind) {
			if (obj) {
				var desc = describe(obj, 'length');
				(desc && typeof desc.value === 'number' ? forEach : forIn)
					.call(obj, iter, bind = bind || obj);
			}
			return bind;
		};

	function inject(dest, src, enumerable, beans, preserve) {
		var beansNames = {};

		function field(name, val) {
			val = val || (val = describe(src, name))
					&& (val.get ? val : val.value);
			if (typeof val === 'string' && val[0] === '#')
				val = dest[val.substring(1)] || val;
			var isFunc = typeof val === 'function',
				res = val,
				prev = preserve || isFunc && !val.base
						? (val && val.get ? name in dest : dest[name])
						: null,
				bean;
			if (!preserve || !prev) {
				if (isFunc && prev)
					val.base = prev;
				if (isFunc && beans !== false
						&& (bean = name.match(/^([gs]et|is)(([A-Z])(.*))$/)))
					beansNames[bean[3].toLowerCase() + bean[4]] = bean[2];
				if (!res || isFunc || !res.get || typeof res.get !== 'function'
						|| !Base.isPlainObject(res)) {
					res = { value: res, writable: true };
				}
				if ((describe(dest, name)
						|| { configurable: true }).configurable) {
					res.configurable = true;
					res.enumerable = enumerable != null ? enumerable : !bean;
				}
				define(dest, name, res);
			}
		}
		if (src) {
			for (var name in src) {
				if (src.hasOwnProperty(name) && !hidden.test(name))
					field(name);
			}
			for (var name in beansNames) {
				var part = beansNames[name],
					set = dest['set' + part],
					get = dest['get' + part] || set && dest['is' + part];
				if (get && (beans === true || get.length === 0))
					field(name, { get: get, set: set });
			}
		}
		return dest;
	}

	function Base() {
		for (var i = 0, l = arguments.length; i < l; i++) {
			var src = arguments[i];
			if (src)
				set(this, src);
		}
		return this;
	}

	return inject(Base, {
		inject: function(src) {
			if (src) {
				var statics = src.statics === true ? src : src.statics,
					beans = src.beans,
					preserve = src.preserve;
				if (statics !== src)
					inject(this.prototype, src, src.enumerable, beans, preserve);
				inject(this, statics, null, beans, preserve);
			}
			for (var i = 1, l = arguments.length; i < l; i++)
				this.inject(arguments[i]);
			return this;
		},

		extend: function() {
			var base = this,
				ctor,
				proto;
			for (var i = 0, obj, l = arguments.length;
					i < l && !(ctor && proto); i++) {
				obj = arguments[i];
				ctor = ctor || obj.initialize;
				proto = proto || obj.prototype;
			}
			ctor = ctor || function() {
				base.apply(this, arguments);
			};
			proto = ctor.prototype = proto || create(this.prototype);
			define(proto, 'constructor',
					{ value: ctor, writable: true, configurable: true });
			inject(ctor, this);
			if (arguments.length)
				this.inject.apply(ctor, arguments);
			ctor.base = base;
			return ctor;
		}
	}).inject({
		enumerable: false,

		initialize: Base,

		set: Base,

		inject: function() {
			for (var i = 0, l = arguments.length; i < l; i++) {
				var src = arguments[i];
				if (src) {
					inject(this, src, src.enumerable, src.beans, src.preserve);
				}
			}
			return this;
		},

		extend: function() {
			var res = create(this);
			return res.inject.apply(res, arguments);
		},

		each: function(iter, bind) {
			return each(this, iter, bind);
		},

		clone: function() {
			return new this.constructor(this);
		},

		statics: {
			set: set,
			each: each,
			create: create,
			define: define,
			describe: describe,

			clone: function(obj) {
				return set(new obj.constructor(), obj);
			},

			isPlainObject: function(obj) {
				var ctor = obj != null && obj.constructor;
				return ctor && (ctor === Object || ctor === Base
						|| ctor.name === 'Object');
			},

			pick: function(a, b) {
				return a !== undefined ? a : b;
			},

			slice: function(list, begin, end) {
				return slice.call(list, begin, end);
			}
		}
	});
};

if (true)
	module.exports = Base;

Base.inject({
	enumerable: false,

	toString: function() {
		return this._id != null
			?  (this._class || 'Object') + (this._name
				? " '" + this._name + "'"
				: ' @' + this._id)
			: '{ ' + Base.each(this, function(value, key) {
				if (!/^_/.test(key)) {
					var type = typeof value;
					this.push(key + ': ' + (type === 'number'
							? Formatter.instance.number(value)
							: type === 'string' ? "'" + value + "'" : value));
				}
			}, []).join(', ') + ' }';
	},

	getClassName: function() {
		return this._class || '';
	},

	importJSON: function(json) {
		return Base.importJSON(json, this);
	},

	exportJSON: function(options) {
		return Base.exportJSON(this, options);
	},

	toJSON: function() {
		return Base.serialize(this);
	},

	set: function(props, exclude) {
		if (props)
			Base.filter(this, props, exclude, this._prioritize);
		return this;
	}
}, {

beans: false,
statics: {
	exports: {},

	extend: function extend() {
		var res = extend.base.apply(this, arguments),
			name = res.prototype._class;
		if (name && !Base.exports[name])
			Base.exports[name] = res;
		return res;
	},

	equals: function(obj1, obj2) {
		if (obj1 === obj2)
			return true;
		if (obj1 && obj1.equals)
			return obj1.equals(obj2);
		if (obj2 && obj2.equals)
			return obj2.equals(obj1);
		if (obj1 && obj2
				&& typeof obj1 === 'object' && typeof obj2 === 'object') {
			if (Array.isArray(obj1) && Array.isArray(obj2)) {
				var length = obj1.length;
				if (length !== obj2.length)
					return false;
				while (length--) {
					if (!Base.equals(obj1[length], obj2[length]))
						return false;
				}
			} else {
				var keys = Object.keys(obj1),
					length = keys.length;
				if (length !== Object.keys(obj2).length)
					return false;
				while (length--) {
					var key = keys[length];
					if (!(obj2.hasOwnProperty(key)
							&& Base.equals(obj1[key], obj2[key])))
						return false;
				}
			}
			return true;
		}
		return false;
	},

	read: function(list, start, options, amount) {
		if (this === Base) {
			var value = this.peek(list, start);
			list.__index++;
			return value;
		}
		var proto = this.prototype,
			readIndex = proto._readIndex,
			begin = start || readIndex && list.__index || 0,
			length = list.length,
			obj = list[begin];
		amount = amount || length - begin;
		if (obj instanceof this
			|| options && options.readNull && obj == null && amount <= 1) {
			if (readIndex)
				list.__index = begin + 1;
			return obj && options && options.clone ? obj.clone() : obj;
		}
		obj = Base.create(proto);
		if (readIndex)
			obj.__read = true;
		obj = obj.initialize.apply(obj, begin > 0 || begin + amount < length
				? Base.slice(list, begin, begin + amount)
				: list) || obj;
		if (readIndex) {
			list.__index = begin + obj.__read;
			var filtered = obj.__filtered;
			if (filtered) {
				list.__filtered = filtered;
				obj.__filtered = undefined;
			}
			obj.__read = undefined;
		}
		return obj;
	},

	peek: function(list, start) {
		return list[list.__index = start || list.__index || 0];
	},

	remain: function(list) {
		return list.length - (list.__index || 0);
	},

	readList: function(list, start, options, amount) {
		var res = [],
			entry,
			begin = start || 0,
			end = amount ? begin + amount : list.length;
		for (var i = begin; i < end; i++) {
			res.push(Array.isArray(entry = list[i])
					? this.read(entry, 0, options)
					: this.read(list, i, options, 1));
		}
		return res;
	},

	readNamed: function(list, name, start, options, amount) {
		var value = this.getNamed(list, name),
			hasObject = value !== undefined;
		if (hasObject) {
			var filtered = list.__filtered;
			if (!filtered) {
				filtered = list.__filtered = Base.create(list[0]);
				filtered.__unfiltered = list[0];
			}
			filtered[name] = undefined;
		}
		var l = hasObject ? [value] : list,
			res = this.read(l, start, options, amount);
		return res;
	},

	getNamed: function(list, name) {
		var arg = list[0];
		if (list._hasObject === undefined)
			list._hasObject = list.length === 1 && Base.isPlainObject(arg);
		if (list._hasObject)
			return name ? arg[name] : list.__filtered || arg;
	},

	hasNamed: function(list, name) {
		return !!this.getNamed(list, name);
	},

	filter: function(dest, source, exclude, prioritize) {
		var processed;

		function handleKey(key) {
			if (!(exclude && key in exclude) &&
				!(processed && key in processed)) {
				var value = source[key];
				if (value !== undefined)
					dest[key] = value;
			}
		}

		if (prioritize) {
			var keys = {};
			for (var i = 0, key, l = prioritize.length; i < l; i++) {
				if ((key = prioritize[i]) in source) {
					handleKey(key);
					keys[key] = true;
				}
			}
			processed = keys;
		}

		Object.keys(source.__unfiltered || source).forEach(handleKey);
		return dest;
	},

	isPlainValue: function(obj, asString) {
		return Base.isPlainObject(obj) || Array.isArray(obj)
				|| asString && typeof obj === 'string';
	},

	serialize: function(obj, options, compact, dictionary) {
		options = options || {};

		var isRoot = !dictionary,
			res;
		if (isRoot) {
			options.formatter = new Formatter(options.precision);
			dictionary = {
				length: 0,
				definitions: {},
				references: {},
				add: function(item, create) {
					var id = '#' + item._id,
						ref = this.references[id];
					if (!ref) {
						this.length++;
						var res = create.call(item),
							name = item._class;
						if (name && res[0] !== name)
							res.unshift(name);
						this.definitions[id] = res;
						ref = this.references[id] = [id];
					}
					return ref;
				}
			};
		}
		if (obj && obj._serialize) {
			res = obj._serialize(options, dictionary);
			var name = obj._class;
			if (name && !obj._compactSerialize && (isRoot || !compact)
					&& res[0] !== name) {
				res.unshift(name);
			}
		} else if (Array.isArray(obj)) {
			res = [];
			for (var i = 0, l = obj.length; i < l; i++)
				res[i] = Base.serialize(obj[i], options, compact, dictionary);
		} else if (Base.isPlainObject(obj)) {
			res = {};
			var keys = Object.keys(obj);
			for (var i = 0, l = keys.length; i < l; i++) {
				var key = keys[i];
				res[key] = Base.serialize(obj[key], options, compact,
						dictionary);
			}
		} else if (typeof obj === 'number') {
			res = options.formatter.number(obj, options.precision);
		} else {
			res = obj;
		}
		return isRoot && dictionary.length > 0
				? [['dictionary', dictionary.definitions], res]
				: res;
	},

	deserialize: function(json, create, _data, _setDictionary, _isRoot) {
		var res = json,
			isFirst = !_data,
			hasDictionary = isFirst && json && json.length
				&& json[0][0] === 'dictionary';
		_data = _data || {};
		if (Array.isArray(json)) {
			var type = json[0],
				isDictionary = type === 'dictionary';
			if (json.length == 1 && /^#/.test(type)) {
				return _data.dictionary[type];
			}
			type = Base.exports[type];
			res = [];
			for (var i = type ? 1 : 0, l = json.length; i < l; i++) {
				res.push(Base.deserialize(json[i], create, _data,
						isDictionary, hasDictionary));
			}
			if (type) {
				var args = res;
				if (create) {
					res = create(type, args, isFirst || _isRoot);
				} else {
					res = new type(args);
				}
			}
		} else if (Base.isPlainObject(json)) {
			res = {};
			if (_setDictionary)
				_data.dictionary = res;
			for (var key in json)
				res[key] = Base.deserialize(json[key], create, _data);
		}
		return hasDictionary ? res[1] : res;
	},

	exportJSON: function(obj, options) {
		var json = Base.serialize(obj, options);
		return options && options.asString == false
				? json
				: JSON.stringify(json);
	},

	importJSON: function(json, target) {
		return Base.deserialize(
				typeof json === 'string' ? JSON.parse(json) : json,
				function(ctor, args, isRoot) {
					var useTarget = isRoot && target
							&& target.constructor === ctor,
						obj = useTarget ? target
							: Base.create(ctor.prototype);
					if (args.length === 1 && obj instanceof Item
							&& (useTarget || !(obj instanceof Layer))) {
						var arg = args[0];
						if (Base.isPlainObject(arg))
							arg.insert = false;
					}
					(useTarget ? obj.set : ctor).apply(obj, args);
					if (useTarget)
						target = null;
					return obj;
				});
	},

	push: function(list, items) {
		var itemsLength = items.length;
		if (itemsLength < 4096) {
			list.push.apply(list, items);
		} else {
			var startLength = list.length;
			list.length += itemsLength;
			for (var i = 0; i < itemsLength; i++) {
				list[startLength + i] = items[i];
			}
		}
		return list;
	},

	splice: function(list, items, index, remove) {
		var amount = items && items.length,
			append = index === undefined;
		index = append ? list.length : index;
		if (index > list.length)
			index = list.length;
		for (var i = 0; i < amount; i++)
			items[i]._index = index + i;
		if (append) {
			Base.push(list, items);
			return [];
		} else {
			var args = [index, remove];
			if (items)
				Base.push(args, items);
			var removed = list.splice.apply(list, args);
			for (var i = 0, l = removed.length; i < l; i++)
				removed[i]._index = undefined;
			for (var i = index + amount, l = list.length; i < l; i++)
				list[i]._index = i;
			return removed;
		}
	},

	capitalize: function(str) {
		return str.replace(/\b[a-z]/g, function(match) {
			return match.toUpperCase();
		});
	},

	camelize: function(str) {
		return str.replace(/-(.)/g, function(match, chr) {
			return chr.toUpperCase();
		});
	},

	hyphenate: function(str) {
		return str.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
	}
}});

var Emitter = {
	on: function(type, func) {
		if (typeof type !== 'string') {
			Base.each(type, function(value, key) {
				this.on(key, value);
			}, this);
		} else {
			var types = this._eventTypes,
				entry = types && types[type],
				handlers = this._callbacks = this._callbacks || {};
			handlers = handlers[type] = handlers[type] || [];
			if (handlers.indexOf(func) === -1) {
				handlers.push(func);
				if (entry && entry.install && handlers.length === 1)
					entry.install.call(this, type);
			}
		}
		return this;
	},

	off: function(type, func) {
		if (typeof type !== 'string') {
			Base.each(type, function(value, key) {
				this.off(key, value);
			}, this);
			return;
		}
		var types = this._eventTypes,
			entry = types && types[type],
			handlers = this._callbacks && this._callbacks[type],
			index;
		if (handlers) {
			if (!func || (index = handlers.indexOf(func)) !== -1
					&& handlers.length === 1) {
				if (entry && entry.uninstall)
					entry.uninstall.call(this, type);
				delete this._callbacks[type];
			} else if (index !== -1) {
				handlers.splice(index, 1);
			}
		}
		return this;
	},

	once: function(type, func) {
		return this.on(type, function handler() {
			func.apply(this, arguments);
			this.off(type, handler);
		});
	},

	emit: function(type, event) {
		var handlers = this._callbacks && this._callbacks[type];
		if (!handlers)
			return false;
		var args = Base.slice(arguments, 1),
			setTarget = event && event.target && !event.currentTarget;
		handlers = handlers.slice();
		if (setTarget)
			event.currentTarget = this;
		for (var i = 0, l = handlers.length; i < l; i++) {
			if (handlers[i].apply(this, args) == false) {
				if (event && event.stop)
					event.stop();
				break;
		   }
		}
		if (setTarget)
			delete event.currentTarget;
		return true;
	},

	responds: function(type) {
		return !!(this._callbacks && this._callbacks[type]);
	},

	attach: '#on',
	detach: '#off',
	fire: '#emit',

	_installEvents: function(install) {
		var types = this._eventTypes,
			handlers = this._callbacks,
			key = install ? 'install' : 'uninstall';
		if (types) {
			for (var type in handlers) {
				if (handlers[type].length > 0) {
					var entry = types[type],
						func = entry && entry[key];
					if (func)
						func.call(this, type);
				}
			}
		}
	},

	statics: {
		inject: function inject(src) {
			var events = src._events;
			if (events) {
				var types = {};
				Base.each(events, function(entry, key) {
					var isString = typeof entry === 'string',
						name = isString ? entry : key,
						part = Base.capitalize(name),
						type = name.substring(2).toLowerCase();
					types[type] = isString ? {} : entry;
					name = '_' + name;
					src['get' + part] = function() {
						return this[name];
					};
					src['set' + part] = function(func) {
						var prev = this[name];
						if (prev)
							this.off(type, prev);
						if (func)
							this.on(type, func);
						this[name] = func;
					};
				});
				src._eventTypes = types;
			}
			return inject.base.apply(this, arguments);
		}
	}
};

var PaperScope = Base.extend({
	_class: 'PaperScope',

	initialize: function PaperScope() {
		paper = this;
		this.settings = new Base({
			applyMatrix: true,
			insertItems: true,
			handleSize: 4,
			hitTolerance: 0
		});
		this.project = null;
		this.projects = [];
		this.tools = [];
		this._id = PaperScope._id++;
		PaperScope._scopes[this._id] = this;
		var proto = PaperScope.prototype;
		if (!this.support) {
			var ctx = CanvasProvider.getContext(1, 1) || {};
			proto.support = {
				nativeDash: 'setLineDash' in ctx || 'mozDash' in ctx,
				nativeBlendModes: BlendMode.nativeModes
			};
			CanvasProvider.release(ctx);
		}
		if (!this.agent) {
			var user = self.navigator.userAgent.toLowerCase(),
				os = (/(darwin|win|mac|linux|freebsd|sunos)/.exec(user)||[])[0],
				platform = os === 'darwin' ? 'mac' : os,
				agent = proto.agent = proto.browser = { platform: platform };
			if (platform)
				agent[platform] = true;
			user.replace(
				/(opera|chrome|safari|webkit|firefox|msie|trident|atom|node)\/?\s*([.\d]+)(?:.*version\/([.\d]+))?(?:.*rv\:v?([.\d]+))?/g,
				function(match, n, v1, v2, rv) {
					if (!agent.chrome) {
						var v = n === 'opera' ? v2 :
								/^(node|trident)$/.test(n) ? rv : v1;
						agent.version = v;
						agent.versionNumber = parseFloat(v);
						n = n === 'trident' ? 'msie' : n;
						agent.name = n;
						agent[n] = true;
					}
				}
			);
			if (agent.chrome)
				delete agent.webkit;
			if (agent.atom)
				delete agent.chrome;
		}
	},

	version: "0.12.0",

	getView: function() {
		var project = this.project;
		return project && project._view;
	},

	getPaper: function() {
		return this;
	},

	execute: function(code, options) {
			var exports = paper.PaperScript.execute(code, this, options);
			View.updateFocus();
			return exports;
	},

	install: function(scope) {
		var that = this;
		Base.each(['project', 'view', 'tool'], function(key) {
			Base.define(scope, key, {
				configurable: true,
				get: function() {
					return that[key];
				}
			});
		});
		for (var key in this)
			if (!/^_/.test(key) && this[key])
				scope[key] = this[key];
	},

	setup: function(element) {
		paper = this;
		this.project = new Project(element);
		return this;
	},

	createCanvas: function(width, height) {
		return CanvasProvider.getCanvas(width, height);
	},

	activate: function() {
		paper = this;
	},

	clear: function() {
		var projects = this.projects,
			tools = this.tools;
		for (var i = projects.length - 1; i >= 0; i--)
			projects[i].remove();
		for (var i = tools.length - 1; i >= 0; i--)
			tools[i].remove();
	},

	remove: function() {
		this.clear();
		delete PaperScope._scopes[this._id];
	},

	statics: new function() {
		function handleAttribute(name) {
			name += 'Attribute';
			return function(el, attr) {
				return el[name](attr) || el[name]('data-paper-' + attr);
			};
		}

		return {
			_scopes: {},
			_id: 0,

			get: function(id) {
				return this._scopes[id] || null;
			},

			getAttribute: handleAttribute('get'),
			hasAttribute: handleAttribute('has')
		};
	}
});

var PaperScopeItem = Base.extend(Emitter, {

	initialize: function(activate) {
		this._scope = paper;
		this._index = this._scope[this._list].push(this) - 1;
		if (activate || !this._scope[this._reference])
			this.activate();
	},

	activate: function() {
		if (!this._scope)
			return false;
		var prev = this._scope[this._reference];
		if (prev && prev !== this)
			prev.emit('deactivate');
		this._scope[this._reference] = this;
		this.emit('activate', prev);
		return true;
	},

	isActive: function() {
		return this._scope[this._reference] === this;
	},

	remove: function() {
		if (this._index == null)
			return false;
		Base.splice(this._scope[this._list], null, this._index, 1);
		if (this._scope[this._reference] == this)
			this._scope[this._reference] = null;
		this._scope = null;
		return true;
	},

	getView: function() {
		return this._scope.getView();
	}
});

var Formatter = Base.extend({
	initialize: function(precision) {
		this.precision = Base.pick(precision, 5);
		this.multiplier = Math.pow(10, this.precision);
	},

	number: function(val) {
		return this.precision < 16
				? Math.round(val * this.multiplier) / this.multiplier : val;
	},

	pair: function(val1, val2, separator) {
		return this.number(val1) + (separator || ',') + this.number(val2);
	},

	point: function(val, separator) {
		return this.number(val.x) + (separator || ',') + this.number(val.y);
	},

	size: function(val, separator) {
		return this.number(val.width) + (separator || ',')
				+ this.number(val.height);
	},

	rectangle: function(val, separator) {
		return this.point(val, separator) + (separator || ',')
				+ this.size(val, separator);
	}
});

Formatter.instance = new Formatter();

var Numerical = new function() {

	var abscissas = [
		[  0.5773502691896257645091488],
		[0,0.7745966692414833770358531],
		[  0.3399810435848562648026658,0.8611363115940525752239465],
		[0,0.5384693101056830910363144,0.9061798459386639927976269],
		[  0.2386191860831969086305017,0.6612093864662645136613996,0.9324695142031520278123016],
		[0,0.4058451513773971669066064,0.7415311855993944398638648,0.9491079123427585245261897],
		[  0.1834346424956498049394761,0.5255324099163289858177390,0.7966664774136267395915539,0.9602898564975362316835609],
		[0,0.3242534234038089290385380,0.6133714327005903973087020,0.8360311073266357942994298,0.9681602395076260898355762],
		[  0.1488743389816312108848260,0.4333953941292471907992659,0.6794095682990244062343274,0.8650633666889845107320967,0.9739065285171717200779640],
		[0,0.2695431559523449723315320,0.5190961292068118159257257,0.7301520055740493240934163,0.8870625997680952990751578,0.9782286581460569928039380],
		[  0.1252334085114689154724414,0.3678314989981801937526915,0.5873179542866174472967024,0.7699026741943046870368938,0.9041172563704748566784659,0.9815606342467192506905491],
		[0,0.2304583159551347940655281,0.4484927510364468528779129,0.6423493394403402206439846,0.8015780907333099127942065,0.9175983992229779652065478,0.9841830547185881494728294],
		[  0.1080549487073436620662447,0.3191123689278897604356718,0.5152486363581540919652907,0.6872929048116854701480198,0.8272013150697649931897947,0.9284348836635735173363911,0.9862838086968123388415973],
		[0,0.2011940939974345223006283,0.3941513470775633698972074,0.5709721726085388475372267,0.7244177313601700474161861,0.8482065834104272162006483,0.9372733924007059043077589,0.9879925180204854284895657],
		[  0.0950125098376374401853193,0.2816035507792589132304605,0.4580167776572273863424194,0.6178762444026437484466718,0.7554044083550030338951012,0.8656312023878317438804679,0.9445750230732325760779884,0.9894009349916499325961542]
	];

	var weights = [
		[1],
		[0.8888888888888888888888889,0.5555555555555555555555556],
		[0.6521451548625461426269361,0.3478548451374538573730639],
		[0.5688888888888888888888889,0.4786286704993664680412915,0.2369268850561890875142640],
		[0.4679139345726910473898703,0.3607615730481386075698335,0.1713244923791703450402961],
		[0.4179591836734693877551020,0.3818300505051189449503698,0.2797053914892766679014678,0.1294849661688696932706114],
		[0.3626837833783619829651504,0.3137066458778872873379622,0.2223810344533744705443560,0.1012285362903762591525314],
		[0.3302393550012597631645251,0.3123470770400028400686304,0.2606106964029354623187429,0.1806481606948574040584720,0.0812743883615744119718922],
		[0.2955242247147528701738930,0.2692667193099963550912269,0.2190863625159820439955349,0.1494513491505805931457763,0.0666713443086881375935688],
		[0.2729250867779006307144835,0.2628045445102466621806889,0.2331937645919904799185237,0.1862902109277342514260976,0.1255803694649046246346943,0.0556685671161736664827537],
		[0.2491470458134027850005624,0.2334925365383548087608499,0.2031674267230659217490645,0.1600783285433462263346525,0.1069393259953184309602547,0.0471753363865118271946160],
		[0.2325515532308739101945895,0.2262831802628972384120902,0.2078160475368885023125232,0.1781459807619457382800467,0.1388735102197872384636018,0.0921214998377284479144218,0.0404840047653158795200216],
		[0.2152638534631577901958764,0.2051984637212956039659241,0.1855383974779378137417166,0.1572031671581935345696019,0.1215185706879031846894148,0.0801580871597602098056333,0.0351194603317518630318329],
		[0.2025782419255612728806202,0.1984314853271115764561183,0.1861610000155622110268006,0.1662692058169939335532009,0.1395706779261543144478048,0.1071592204671719350118695,0.0703660474881081247092674,0.0307532419961172683546284],
		[0.1894506104550684962853967,0.1826034150449235888667637,0.1691565193950025381893121,0.1495959888165767320815017,0.1246289712555338720524763,0.0951585116824927848099251,0.0622535239386478928628438,0.0271524594117540948517806]
	];

	var abs = Math.abs,
		sqrt = Math.sqrt,
		pow = Math.pow,
		log2 = Math.log2 || function(x) {
			return Math.log(x) * Math.LOG2E;
		},
		EPSILON = 1e-12,
		MACHINE_EPSILON = 1.12e-16;

	function clamp(value, min, max) {
		return value < min ? min : value > max ? max : value;
	}

	function getDiscriminant(a, b, c) {
		function split(v) {
			var x = v * 134217729,
				y = v - x,
				hi = y + x,
				lo = v - hi;
			return [hi, lo];
		}

		var D = b * b - a * c,
			E = b * b + a * c;
		if (abs(D) * 3 < E) {
			var ad = split(a),
				bd = split(b),
				cd = split(c),
				p = b * b,
				dp = (bd[0] * bd[0] - p + 2 * bd[0] * bd[1]) + bd[1] * bd[1],
				q = a * c,
				dq = (ad[0] * cd[0] - q + ad[0] * cd[1] + ad[1] * cd[0])
						+ ad[1] * cd[1];
			D = (p - q) + (dp - dq);
		}
		return D;
	}

	function getNormalizationFactor() {
		var norm = Math.max.apply(Math, arguments);
		return norm && (norm < 1e-8 || norm > 1e8)
				? pow(2, -Math.round(log2(norm)))
				: 0;
	}

	return {
		EPSILON: EPSILON,
		MACHINE_EPSILON: MACHINE_EPSILON,
		CURVETIME_EPSILON: 1e-8,
		GEOMETRIC_EPSILON: 1e-7,
		TRIGONOMETRIC_EPSILON: 1e-8,
		KAPPA: 4 * (sqrt(2) - 1) / 3,

		isZero: function(val) {
			return val >= -EPSILON && val <= EPSILON;
		},

		clamp: clamp,

		integrate: function(f, a, b, n) {
			var x = abscissas[n - 2],
				w = weights[n - 2],
				A = (b - a) * 0.5,
				B = A + a,
				i = 0,
				m = (n + 1) >> 1,
				sum = n & 1 ? w[i++] * f(B) : 0;
			while (i < m) {
				var Ax = A * x[i];
				sum += w[i++] * (f(B + Ax) + f(B - Ax));
			}
			return A * sum;
		},

		findRoot: function(f, df, x, a, b, n, tolerance) {
			for (var i = 0; i < n; i++) {
				var fx = f(x),
					dx = fx / df(x),
					nx = x - dx;
				if (abs(dx) < tolerance) {
					x = nx;
					break;
				}
				if (fx > 0) {
					b = x;
					x = nx <= a ? (a + b) * 0.5 : nx;
				} else {
					a = x;
					x = nx >= b ? (a + b) * 0.5 : nx;
				}
			}
			return clamp(x, a, b);
		},

		solveQuadratic: function(a, b, c, roots, min, max) {
			var x1, x2 = Infinity;
			if (abs(a) < EPSILON) {
				if (abs(b) < EPSILON)
					return abs(c) < EPSILON ? -1 : 0;
				x1 = -c / b;
			} else {
				b *= -0.5;
				var D = getDiscriminant(a, b, c);
				if (D && abs(D) < MACHINE_EPSILON) {
					var f = getNormalizationFactor(abs(a), abs(b), abs(c));
					if (f) {
						a *= f;
						b *= f;
						c *= f;
						D = getDiscriminant(a, b, c);
					}
				}
				if (D >= -MACHINE_EPSILON) {
					var Q = D < 0 ? 0 : sqrt(D),
						R = b + (b < 0 ? -Q : Q);
					if (R === 0) {
						x1 = c / a;
						x2 = -x1;
					} else {
						x1 = R / a;
						x2 = c / R;
					}
				}
			}
			var count = 0,
				boundless = min == null,
				minB = min - EPSILON,
				maxB = max + EPSILON;
			if (isFinite(x1) && (boundless || x1 > minB && x1 < maxB))
				roots[count++] = boundless ? x1 : clamp(x1, min, max);
			if (x2 !== x1
					&& isFinite(x2) && (boundless || x2 > minB && x2 < maxB))
				roots[count++] = boundless ? x2 : clamp(x2, min, max);
			return count;
		},

		solveCubic: function(a, b, c, d, roots, min, max) {
			var f = getNormalizationFactor(abs(a), abs(b), abs(c), abs(d)),
				x, b1, c2, qd, q;
			if (f) {
				a *= f;
				b *= f;
				c *= f;
				d *= f;
			}

			function evaluate(x0) {
				x = x0;
				var tmp = a * x;
				b1 = tmp + b;
				c2 = b1 * x + c;
				qd = (tmp + b1) * x + c2;
				q = c2 * x + d;
			}

			if (abs(a) < EPSILON) {
				a = b;
				b1 = c;
				c2 = d;
				x = Infinity;
			} else if (abs(d) < EPSILON) {
				b1 = b;
				c2 = c;
				x = 0;
			} else {
				evaluate(-(b / a) / 3);
				var t = q / a,
					r = pow(abs(t), 1/3),
					s = t < 0 ? -1 : 1,
					td = -qd / a,
					rd = td > 0 ? 1.324717957244746 * Math.max(r, sqrt(td)) : r,
					x0 = x - s * rd;
				if (x0 !== x) {
					do {
						evaluate(x0);
						x0 = qd === 0 ? x : x - q / qd / (1 + MACHINE_EPSILON);
					} while (s * x0 > s * x);
					if (abs(a) * x * x > abs(d / x)) {
						c2 = -d / x;
						b1 = (c2 - c) / x;
					}
				}
			}
			var count = Numerical.solveQuadratic(a, b1, c2, roots, min, max),
				boundless = min == null;
			if (isFinite(x) && (count === 0
					|| count > 0 && x !== roots[0] && x !== roots[1])
					&& (boundless || x > min - EPSILON && x < max + EPSILON))
				roots[count++] = boundless ? x : clamp(x, min, max);
			return count;
		}
	};
};

var UID = {
	_id: 1,
	_pools: {},

	get: function(name) {
		if (name) {
			var pool = this._pools[name];
			if (!pool)
				pool = this._pools[name] = { _id: 1 };
			return pool._id++;
		} else {
			return this._id++;
		}
	}
};

var Point = Base.extend({
	_class: 'Point',
	_readIndex: true,

	initialize: function Point(arg0, arg1) {
		var type = typeof arg0,
			reading = this.__read,
			read = 0;
		if (type === 'number') {
			var hasY = typeof arg1 === 'number';
			this._set(arg0, hasY ? arg1 : arg0);
			if (reading)
				read = hasY ? 2 : 1;
		} else if (type === 'undefined' || arg0 === null) {
			this._set(0, 0);
			if (reading)
				read = arg0 === null ? 1 : 0;
		} else {
			var obj = type === 'string' ? arg0.split(/[\s,]+/) || [] : arg0;
			read = 1;
			if (Array.isArray(obj)) {
				this._set(+obj[0], +(obj.length > 1 ? obj[1] : obj[0]));
			} else if ('x' in obj) {
				this._set(obj.x || 0, obj.y || 0);
			} else if ('width' in obj) {
				this._set(obj.width || 0, obj.height || 0);
			} else if ('angle' in obj) {
				this._set(obj.length || 0, 0);
				this.setAngle(obj.angle || 0);
			} else {
				this._set(0, 0);
				read = 0;
			}
		}
		if (reading)
			this.__read = read;
		return this;
	},

	set: '#initialize',

	_set: function(x, y) {
		this.x = x;
		this.y = y;
		return this;
	},

	equals: function(point) {
		return this === point || point
				&& (this.x === point.x && this.y === point.y
					|| Array.isArray(point)
						&& this.x === point[0] && this.y === point[1])
				|| false;
	},

	clone: function() {
		return new Point(this.x, this.y);
	},

	toString: function() {
		var f = Formatter.instance;
		return '{ x: ' + f.number(this.x) + ', y: ' + f.number(this.y) + ' }';
	},

	_serialize: function(options) {
		var f = options.formatter;
		return [f.number(this.x), f.number(this.y)];
	},

	getLength: function() {
		return Math.sqrt(this.x * this.x + this.y * this.y);
	},

	setLength: function(length) {
		if (this.isZero()) {
			var angle = this._angle || 0;
			this._set(
				Math.cos(angle) * length,
				Math.sin(angle) * length
			);
		} else {
			var scale = length / this.getLength();
			if (Numerical.isZero(scale))
				this.getAngle();
			this._set(
				this.x * scale,
				this.y * scale
			);
		}
	},
	getAngle: function() {
		return this.getAngleInRadians.apply(this, arguments) * 180 / Math.PI;
	},

	setAngle: function(angle) {
		this.setAngleInRadians.call(this, angle * Math.PI / 180);
	},

	getAngleInDegrees: '#getAngle',
	setAngleInDegrees: '#setAngle',

	getAngleInRadians: function() {
		if (!arguments.length) {
			return this.isZero()
					? this._angle || 0
					: this._angle = Math.atan2(this.y, this.x);
		} else {
			var point = Point.read(arguments),
				div = this.getLength() * point.getLength();
			if (Numerical.isZero(div)) {
				return NaN;
			} else {
				var a = this.dot(point) / div;
				return Math.acos(a < -1 ? -1 : a > 1 ? 1 : a);
			}
		}
	},

	setAngleInRadians: function(angle) {
		this._angle = angle;
		if (!this.isZero()) {
			var length = this.getLength();
			this._set(
				Math.cos(angle) * length,
				Math.sin(angle) * length
			);
		}
	},

	getQuadrant: function() {
		return this.x >= 0 ? this.y >= 0 ? 1 : 4 : this.y >= 0 ? 2 : 3;
	}
}, {
	beans: false,

	getDirectedAngle: function() {
		var point = Point.read(arguments);
		return Math.atan2(this.cross(point), this.dot(point)) * 180 / Math.PI;
	},

	getDistance: function() {
		var point = Point.read(arguments),
			x = point.x - this.x,
			y = point.y - this.y,
			d = x * x + y * y,
			squared = Base.read(arguments);
		return squared ? d : Math.sqrt(d);
	},

	normalize: function(length) {
		if (length === undefined)
			length = 1;
		var current = this.getLength(),
			scale = current !== 0 ? length / current : 0,
			point = new Point(this.x * scale, this.y * scale);
		if (scale >= 0)
			point._angle = this._angle;
		return point;
	},

	rotate: function(angle, center) {
		if (angle === 0)
			return this.clone();
		angle = angle * Math.PI / 180;
		var point = center ? this.subtract(center) : this,
			sin = Math.sin(angle),
			cos = Math.cos(angle);
		point = new Point(
			point.x * cos - point.y * sin,
			point.x * sin + point.y * cos
		);
		return center ? point.add(center) : point;
	},

	transform: function(matrix) {
		return matrix ? matrix._transformPoint(this) : this;
	},

	add: function() {
		var point = Point.read(arguments);
		return new Point(this.x + point.x, this.y + point.y);
	},

	subtract: function() {
		var point = Point.read(arguments);
		return new Point(this.x - point.x, this.y - point.y);
	},

	multiply: function() {
		var point = Point.read(arguments);
		return new Point(this.x * point.x, this.y * point.y);
	},

	divide: function() {
		var point = Point.read(arguments);
		return new Point(this.x / point.x, this.y / point.y);
	},

	modulo: function() {
		var point = Point.read(arguments);
		return new Point(this.x % point.x, this.y % point.y);
	},

	negate: function() {
		return new Point(-this.x, -this.y);
	},

	isInside: function() {
		return Rectangle.read(arguments).contains(this);
	},

	isClose: function() {
		var point = Point.read(arguments),
			tolerance = Base.read(arguments);
		return this.getDistance(point) <= tolerance;
	},

	isCollinear: function() {
		var point = Point.read(arguments);
		return Point.isCollinear(this.x, this.y, point.x, point.y);
	},

	isColinear: '#isCollinear',

	isOrthogonal: function() {
		var point = Point.read(arguments);
		return Point.isOrthogonal(this.x, this.y, point.x, point.y);
	},

	isZero: function() {
		var isZero = Numerical.isZero;
		return isZero(this.x) && isZero(this.y);
	},

	isNaN: function() {
		return isNaN(this.x) || isNaN(this.y);
	},

	isInQuadrant: function(q) {
		return this.x * (q > 1 && q < 4 ? -1 : 1) >= 0
			&& this.y * (q > 2 ? -1 : 1) >= 0;
	},

	dot: function() {
		var point = Point.read(arguments);
		return this.x * point.x + this.y * point.y;
	},

	cross: function() {
		var point = Point.read(arguments);
		return this.x * point.y - this.y * point.x;
	},

	project: function() {
		var point = Point.read(arguments),
			scale = point.isZero() ? 0 : this.dot(point) / point.dot(point);
		return new Point(
			point.x * scale,
			point.y * scale
		);
	},

	statics: {
		min: function() {
			var point1 = Point.read(arguments),
				point2 = Point.read(arguments);
			return new Point(
				Math.min(point1.x, point2.x),
				Math.min(point1.y, point2.y)
			);
		},

		max: function() {
			var point1 = Point.read(arguments),
				point2 = Point.read(arguments);
			return new Point(
				Math.max(point1.x, point2.x),
				Math.max(point1.y, point2.y)
			);
		},

		random: function() {
			return new Point(Math.random(), Math.random());
		},

		isCollinear: function(x1, y1, x2, y2) {
			return Math.abs(x1 * y2 - y1 * x2)
					<= Math.sqrt((x1 * x1 + y1 * y1) * (x2 * x2 + y2 * y2))
						* 1e-8;
		},

		isOrthogonal: function(x1, y1, x2, y2) {
			return Math.abs(x1 * x2 + y1 * y2)
					<= Math.sqrt((x1 * x1 + y1 * y1) * (x2 * x2 + y2 * y2))
						* 1e-8;
		}
	}
}, Base.each(['round', 'ceil', 'floor', 'abs'], function(key) {
	var op = Math[key];
	this[key] = function() {
		return new Point(op(this.x), op(this.y));
	};
}, {}));

var LinkedPoint = Point.extend({
	initialize: function Point(x, y, owner, setter) {
		this._x = x;
		this._y = y;
		this._owner = owner;
		this._setter = setter;
	},

	_set: function(x, y, _dontNotify) {
		this._x = x;
		this._y = y;
		if (!_dontNotify)
			this._owner[this._setter](this);
		return this;
	},

	getX: function() {
		return this._x;
	},

	setX: function(x) {
		this._x = x;
		this._owner[this._setter](this);
	},

	getY: function() {
		return this._y;
	},

	setY: function(y) {
		this._y = y;
		this._owner[this._setter](this);
	},

	isSelected: function() {
		return !!(this._owner._selection & this._getSelection());
	},

	setSelected: function(selected) {
		this._owner._changeSelection(this._getSelection(), selected);
	},

	_getSelection: function() {
		return this._setter === 'setPosition' ? 4 : 0;
	}
});

var Size = Base.extend({
	_class: 'Size',
	_readIndex: true,

	initialize: function Size(arg0, arg1) {
		var type = typeof arg0,
			reading = this.__read,
			read = 0;
		if (type === 'number') {
			var hasHeight = typeof arg1 === 'number';
			this._set(arg0, hasHeight ? arg1 : arg0);
			if (reading)
				read = hasHeight ? 2 : 1;
		} else if (type === 'undefined' || arg0 === null) {
			this._set(0, 0);
			if (reading)
				read = arg0 === null ? 1 : 0;
		} else {
			var obj = type === 'string' ? arg0.split(/[\s,]+/) || [] : arg0;
			read = 1;
			if (Array.isArray(obj)) {
				this._set(+obj[0], +(obj.length > 1 ? obj[1] : obj[0]));
			} else if ('width' in obj) {
				this._set(obj.width || 0, obj.height || 0);
			} else if ('x' in obj) {
				this._set(obj.x || 0, obj.y || 0);
			} else {
				this._set(0, 0);
				read = 0;
			}
		}
		if (reading)
			this.__read = read;
		return this;
	},

	set: '#initialize',

	_set: function(width, height) {
		this.width = width;
		this.height = height;
		return this;
	},

	equals: function(size) {
		return size === this || size && (this.width === size.width
				&& this.height === size.height
				|| Array.isArray(size) && this.width === size[0]
					&& this.height === size[1]) || false;
	},

	clone: function() {
		return new Size(this.width, this.height);
	},

	toString: function() {
		var f = Formatter.instance;
		return '{ width: ' + f.number(this.width)
				+ ', height: ' + f.number(this.height) + ' }';
	},

	_serialize: function(options) {
		var f = options.formatter;
		return [f.number(this.width),
				f.number(this.height)];
	},

	add: function() {
		var size = Size.read(arguments);
		return new Size(this.width + size.width, this.height + size.height);
	},

	subtract: function() {
		var size = Size.read(arguments);
		return new Size(this.width - size.width, this.height - size.height);
	},

	multiply: function() {
		var size = Size.read(arguments);
		return new Size(this.width * size.width, this.height * size.height);
	},

	divide: function() {
		var size = Size.read(arguments);
		return new Size(this.width / size.width, this.height / size.height);
	},

	modulo: function() {
		var size = Size.read(arguments);
		return new Size(this.width % size.width, this.height % size.height);
	},

	negate: function() {
		return new Size(-this.width, -this.height);
	},

	isZero: function() {
		var isZero = Numerical.isZero;
		return isZero(this.width) && isZero(this.height);
	},

	isNaN: function() {
		return isNaN(this.width) || isNaN(this.height);
	},

	statics: {
		min: function(size1, size2) {
			return new Size(
				Math.min(size1.width, size2.width),
				Math.min(size1.height, size2.height));
		},

		max: function(size1, size2) {
			return new Size(
				Math.max(size1.width, size2.width),
				Math.max(size1.height, size2.height));
		},

		random: function() {
			return new Size(Math.random(), Math.random());
		}
	}
}, Base.each(['round', 'ceil', 'floor', 'abs'], function(key) {
	var op = Math[key];
	this[key] = function() {
		return new Size(op(this.width), op(this.height));
	};
}, {}));

var LinkedSize = Size.extend({
	initialize: function Size(width, height, owner, setter) {
		this._width = width;
		this._height = height;
		this._owner = owner;
		this._setter = setter;
	},

	_set: function(width, height, _dontNotify) {
		this._width = width;
		this._height = height;
		if (!_dontNotify)
			this._owner[this._setter](this);
		return this;
	},

	getWidth: function() {
		return this._width;
	},

	setWidth: function(width) {
		this._width = width;
		this._owner[this._setter](this);
	},

	getHeight: function() {
		return this._height;
	},

	setHeight: function(height) {
		this._height = height;
		this._owner[this._setter](this);
	}
});

var Rectangle = Base.extend({
	_class: 'Rectangle',
	_readIndex: true,
	beans: true,

	initialize: function Rectangle(arg0, arg1, arg2, arg3) {
		var type = typeof arg0,
			read;
		if (type === 'number') {
			this._set(arg0, arg1, arg2, arg3);
			read = 4;
		} else if (type === 'undefined' || arg0 === null) {
			this._set(0, 0, 0, 0);
			read = arg0 === null ? 1 : 0;
		} else if (arguments.length === 1) {
			if (Array.isArray(arg0)) {
				this._set.apply(this, arg0);
				read = 1;
			} else if (arg0.x !== undefined || arg0.width !== undefined) {
				this._set(arg0.x || 0, arg0.y || 0,
						arg0.width || 0, arg0.height || 0);
				read = 1;
			} else if (arg0.from === undefined && arg0.to === undefined) {
				this._set(0, 0, 0, 0);
				Base.filter(this, arg0);
				read = 1;
			}
		}
		if (read === undefined) {
			var frm = Point.readNamed(arguments, 'from'),
				next = Base.peek(arguments),
				x = frm.x,
				y = frm.y,
				width,
				height;
			if (next && next.x !== undefined
					|| Base.hasNamed(arguments, 'to')) {
				var to = Point.readNamed(arguments, 'to');
				width = to.x - x;
				height = to.y - y;
				if (width < 0) {
					x = to.x;
					width = -width;
				}
				if (height < 0) {
					y = to.y;
					height = -height;
				}
			} else {
				var size = Size.read(arguments);
				width = size.width;
				height = size.height;
			}
			this._set(x, y, width, height);
			read = arguments.__index;
			var filtered = arguments.__filtered;
			if (filtered)
				this.__filtered = filtered;
		}
		if (this.__read)
			this.__read = read;
		return this;
	},

	set: '#initialize',

	_set: function(x, y, width, height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		return this;
	},

	clone: function() {
		return new Rectangle(this.x, this.y, this.width, this.height);
	},

	equals: function(rect) {
		var rt = Base.isPlainValue(rect)
				? Rectangle.read(arguments)
				: rect;
		return rt === this
				|| rt && this.x === rt.x && this.y === rt.y
					&& this.width === rt.width && this.height === rt.height
				|| false;
	},

	toString: function() {
		var f = Formatter.instance;
		return '{ x: ' + f.number(this.x)
				+ ', y: ' + f.number(this.y)
				+ ', width: ' + f.number(this.width)
				+ ', height: ' + f.number(this.height)
				+ ' }';
	},

	_serialize: function(options) {
		var f = options.formatter;
		return [f.number(this.x),
				f.number(this.y),
				f.number(this.width),
				f.number(this.height)];
	},

	getPoint: function(_dontLink) {
		var ctor = _dontLink ? Point : LinkedPoint;
		return new ctor(this.x, this.y, this, 'setPoint');
	},

	setPoint: function() {
		var point = Point.read(arguments);
		this.x = point.x;
		this.y = point.y;
	},

	getSize: function(_dontLink) {
		var ctor = _dontLink ? Size : LinkedSize;
		return new ctor(this.width, this.height, this, 'setSize');
	},

	_fw: 1,
	_fh: 1,

	setSize: function() {
		var size = Size.read(arguments),
			sx = this._sx,
			sy = this._sy,
			w = size.width,
			h = size.height;
		if (sx) {
			this.x += (this.width - w) * sx;
		}
		if (sy) {
			this.y += (this.height - h) * sy;
		}
		this.width = w;
		this.height = h;
		this._fw = this._fh = 1;
	},

	getLeft: function() {
		return this.x;
	},

	setLeft: function(left) {
		if (!this._fw) {
			var amount = left - this.x;
			this.width -= this._sx === 0.5 ? amount * 2 : amount;
		}
		this.x = left;
		this._sx = this._fw = 0;
	},

	getTop: function() {
		return this.y;
	},

	setTop: function(top) {
		if (!this._fh) {
			var amount = top - this.y;
			this.height -= this._sy === 0.5 ? amount * 2 : amount;
		}
		this.y = top;
		this._sy = this._fh = 0;
	},

	getRight: function() {
		return this.x + this.width;
	},

	setRight: function(right) {
		if (!this._fw) {
			var amount = right - this.x;
			this.width = this._sx === 0.5 ? amount * 2 : amount;
		}
		this.x = right - this.width;
		this._sx = 1;
		this._fw = 0;
	},

	getBottom: function() {
		return this.y + this.height;
	},

	setBottom: function(bottom) {
		if (!this._fh) {
			var amount = bottom - this.y;
			this.height = this._sy === 0.5 ? amount * 2 : amount;
		}
		this.y = bottom - this.height;
		this._sy = 1;
		this._fh = 0;
	},

	getCenterX: function() {
		return this.x + this.width / 2;
	},

	setCenterX: function(x) {
		if (this._fw || this._sx === 0.5) {
			this.x = x - this.width / 2;
		} else {
			if (this._sx) {
				this.x += (x - this.x) * 2 * this._sx;
			}
			this.width = (x - this.x) * 2;
		}
		this._sx = 0.5;
		this._fw = 0;
	},

	getCenterY: function() {
		return this.y + this.height / 2;
	},

	setCenterY: function(y) {
		if (this._fh || this._sy === 0.5) {
			this.y = y - this.height / 2;
		} else {
			if (this._sy) {
				this.y += (y - this.y) * 2 * this._sy;
			}
			this.height = (y - this.y) * 2;
		}
		this._sy = 0.5;
		this._fh = 0;
	},

	getCenter: function(_dontLink) {
		var ctor = _dontLink ? Point : LinkedPoint;
		return new ctor(this.getCenterX(), this.getCenterY(), this, 'setCenter');
	},

	setCenter: function() {
		var point = Point.read(arguments);
		this.setCenterX(point.x);
		this.setCenterY(point.y);
		return this;
	},

	getArea: function() {
		return this.width * this.height;
	},

	isEmpty: function() {
		return this.width === 0 || this.height === 0;
	},

	contains: function(arg) {
		return arg && arg.width !== undefined
				|| (Array.isArray(arg) ? arg : arguments).length === 4
				? this._containsRectangle(Rectangle.read(arguments))
				: this._containsPoint(Point.read(arguments));
	},

	_containsPoint: function(point) {
		var x = point.x,
			y = point.y;
		return x >= this.x && y >= this.y
				&& x <= this.x + this.width
				&& y <= this.y + this.height;
	},

	_containsRectangle: function(rect) {
		var x = rect.x,
			y = rect.y;
		return x >= this.x && y >= this.y
				&& x + rect.width <= this.x + this.width
				&& y + rect.height <= this.y + this.height;
	},

	intersects: function() {
		var rect = Rectangle.read(arguments),
			epsilon = Base.read(arguments) || 0;
		return rect.x + rect.width > this.x - epsilon
				&& rect.y + rect.height > this.y - epsilon
				&& rect.x < this.x + this.width + epsilon
				&& rect.y < this.y + this.height + epsilon;
	},

	intersect: function() {
		var rect = Rectangle.read(arguments),
			x1 = Math.max(this.x, rect.x),
			y1 = Math.max(this.y, rect.y),
			x2 = Math.min(this.x + this.width, rect.x + rect.width),
			y2 = Math.min(this.y + this.height, rect.y + rect.height);
		return new Rectangle(x1, y1, x2 - x1, y2 - y1);
	},

	unite: function() {
		var rect = Rectangle.read(arguments),
			x1 = Math.min(this.x, rect.x),
			y1 = Math.min(this.y, rect.y),
			x2 = Math.max(this.x + this.width, rect.x + rect.width),
			y2 = Math.max(this.y + this.height, rect.y + rect.height);
		return new Rectangle(x1, y1, x2 - x1, y2 - y1);
	},

	include: function() {
		var point = Point.read(arguments);
		var x1 = Math.min(this.x, point.x),
			y1 = Math.min(this.y, point.y),
			x2 = Math.max(this.x + this.width, point.x),
			y2 = Math.max(this.y + this.height, point.y);
		return new Rectangle(x1, y1, x2 - x1, y2 - y1);
	},

	expand: function() {
		var amount = Size.read(arguments),
			hor = amount.width,
			ver = amount.height;
		return new Rectangle(this.x - hor / 2, this.y - ver / 2,
				this.width + hor, this.height + ver);
	},

	scale: function(hor, ver) {
		return this.expand(this.width * hor - this.width,
				this.height * (ver === undefined ? hor : ver) - this.height);
	}
}, Base.each([
		['Top', 'Left'], ['Top', 'Right'],
		['Bottom', 'Left'], ['Bottom', 'Right'],
		['Left', 'Center'], ['Top', 'Center'],
		['Right', 'Center'], ['Bottom', 'Center']
	],
	function(parts, index) {
		var part = parts.join(''),
			xFirst = /^[RL]/.test(part);
		if (index >= 4)
			parts[1] += xFirst ? 'Y' : 'X';
		var x = parts[xFirst ? 0 : 1],
			y = parts[xFirst ? 1 : 0],
			getX = 'get' + x,
			getY = 'get' + y,
			setX = 'set' + x,
			setY = 'set' + y,
			get = 'get' + part,
			set = 'set' + part;
		this[get] = function(_dontLink) {
			var ctor = _dontLink ? Point : LinkedPoint;
			return new ctor(this[getX](), this[getY](), this, set);
		};
		this[set] = function() {
			var point = Point.read(arguments);
			this[setX](point.x);
			this[setY](point.y);
		};
	}, {
		beans: true
	}
));

var LinkedRectangle = Rectangle.extend({
	initialize: function Rectangle(x, y, width, height, owner, setter) {
		this._set(x, y, width, height, true);
		this._owner = owner;
		this._setter = setter;
	},

	_set: function(x, y, width, height, _dontNotify) {
		this._x = x;
		this._y = y;
		this._width = width;
		this._height = height;
		if (!_dontNotify)
			this._owner[this._setter](this);
		return this;
	}
},
new function() {
	var proto = Rectangle.prototype;

	return Base.each(['x', 'y', 'width', 'height'], function(key) {
		var part = Base.capitalize(key),
			internal = '_' + key;
		this['get' + part] = function() {
			return this[internal];
		};

		this['set' + part] = function(value) {
			this[internal] = value;
			if (!this._dontNotify)
				this._owner[this._setter](this);
		};
	}, Base.each(['Point', 'Size', 'Center',
			'Left', 'Top', 'Right', 'Bottom', 'CenterX', 'CenterY',
			'TopLeft', 'TopRight', 'BottomLeft', 'BottomRight',
			'LeftCenter', 'TopCenter', 'RightCenter', 'BottomCenter'],
		function(key) {
			var name = 'set' + key;
			this[name] = function() {
				this._dontNotify = true;
				proto[name].apply(this, arguments);
				this._dontNotify = false;
				this._owner[this._setter](this);
			};
		}, {
			isSelected: function() {
				return !!(this._owner._selection & 2);
			},

			setSelected: function(selected) {
				var owner = this._owner;
				if (owner._changeSelection) {
					owner._changeSelection(2, selected);
				}
			}
		})
	);
});

var Matrix = Base.extend({
	_class: 'Matrix',

	initialize: function Matrix(arg, _dontNotify) {
		var count = arguments.length,
			ok = true;
		if (count >= 6) {
			this._set.apply(this, arguments);
		} else if (count === 1 || count === 2) {
			if (arg instanceof Matrix) {
				this._set(arg._a, arg._b, arg._c, arg._d, arg._tx, arg._ty,
						_dontNotify);
			} else if (Array.isArray(arg)) {
				this._set.apply(this,
						_dontNotify ? arg.concat([_dontNotify]) : arg);
			} else {
				ok = false;
			}
		} else if (!count) {
			this.reset();
		} else {
			ok = false;
		}
		if (!ok) {
			throw new Error('Unsupported matrix parameters');
		}
		return this;
	},

	set: '#initialize',

	_set: function(a, b, c, d, tx, ty, _dontNotify) {
		this._a = a;
		this._b = b;
		this._c = c;
		this._d = d;
		this._tx = tx;
		this._ty = ty;
		if (!_dontNotify)
			this._changed();
		return this;
	},

	_serialize: function(options, dictionary) {
		return Base.serialize(this.getValues(), options, true, dictionary);
	},

	_changed: function() {
		var owner = this._owner;
		if (owner) {
			if (owner._applyMatrix) {
				owner.transform(null, true);
			} else {
				owner._changed(25);
			}
		}
	},

	clone: function() {
		return new Matrix(this._a, this._b, this._c, this._d,
				this._tx, this._ty);
	},

	equals: function(mx) {
		return mx === this || mx && this._a === mx._a && this._b === mx._b
				&& this._c === mx._c && this._d === mx._d
				&& this._tx === mx._tx && this._ty === mx._ty;
	},

	toString: function() {
		var f = Formatter.instance;
		return '[[' + [f.number(this._a), f.number(this._c),
					f.number(this._tx)].join(', ') + '], ['
				+ [f.number(this._b), f.number(this._d),
					f.number(this._ty)].join(', ') + ']]';
	},

	reset: function(_dontNotify) {
		this._a = this._d = 1;
		this._b = this._c = this._tx = this._ty = 0;
		if (!_dontNotify)
			this._changed();
		return this;
	},

	apply: function(recursively, _setApplyMatrix) {
		var owner = this._owner;
		if (owner) {
			owner.transform(null, true, Base.pick(recursively, true),
					_setApplyMatrix);
			return this.isIdentity();
		}
		return false;
	},

	translate: function() {
		var point = Point.read(arguments),
			x = point.x,
			y = point.y;
		this._tx += x * this._a + y * this._c;
		this._ty += x * this._b + y * this._d;
		this._changed();
		return this;
	},

	scale: function() {
		var scale = Point.read(arguments),
			center = Point.read(arguments, 0, { readNull: true });
		if (center)
			this.translate(center);
		this._a *= scale.x;
		this._b *= scale.x;
		this._c *= scale.y;
		this._d *= scale.y;
		if (center)
			this.translate(center.negate());
		this._changed();
		return this;
	},

	rotate: function(angle ) {
		angle *= Math.PI / 180;
		var center = Point.read(arguments, 1),
			x = center.x,
			y = center.y,
			cos = Math.cos(angle),
			sin = Math.sin(angle),
			tx = x - x * cos + y * sin,
			ty = y - x * sin - y * cos,
			a = this._a,
			b = this._b,
			c = this._c,
			d = this._d;
		this._a = cos * a + sin * c;
		this._b = cos * b + sin * d;
		this._c = -sin * a + cos * c;
		this._d = -sin * b + cos * d;
		this._tx += tx * a + ty * c;
		this._ty += tx * b + ty * d;
		this._changed();
		return this;
	},

	shear: function() {
		var shear = Point.read(arguments),
			center = Point.read(arguments, 0, { readNull: true });
		if (center)
			this.translate(center);
		var a = this._a,
			b = this._b;
		this._a += shear.y * this._c;
		this._b += shear.y * this._d;
		this._c += shear.x * a;
		this._d += shear.x * b;
		if (center)
			this.translate(center.negate());
		this._changed();
		return this;
	},

	skew: function() {
		var skew = Point.read(arguments),
			center = Point.read(arguments, 0, { readNull: true }),
			toRadians = Math.PI / 180,
			shear = new Point(Math.tan(skew.x * toRadians),
				Math.tan(skew.y * toRadians));
		return this.shear(shear, center);
	},

	append: function(mx, _dontNotify) {
		if (mx) {
			var a1 = this._a,
				b1 = this._b,
				c1 = this._c,
				d1 = this._d,
				a2 = mx._a,
				b2 = mx._c,
				c2 = mx._b,
				d2 = mx._d,
				tx2 = mx._tx,
				ty2 = mx._ty;
			this._a = a2 * a1 + c2 * c1;
			this._c = b2 * a1 + d2 * c1;
			this._b = a2 * b1 + c2 * d1;
			this._d = b2 * b1 + d2 * d1;
			this._tx += tx2 * a1 + ty2 * c1;
			this._ty += tx2 * b1 + ty2 * d1;
			if (!_dontNotify)
				this._changed();
		}
		return this;
	},

	prepend: function(mx, _dontNotify) {
		if (mx) {
			var a1 = this._a,
				b1 = this._b,
				c1 = this._c,
				d1 = this._d,
				tx1 = this._tx,
				ty1 = this._ty,
				a2 = mx._a,
				b2 = mx._c,
				c2 = mx._b,
				d2 = mx._d,
				tx2 = mx._tx,
				ty2 = mx._ty;
			this._a = a2 * a1 + b2 * b1;
			this._c = a2 * c1 + b2 * d1;
			this._b = c2 * a1 + d2 * b1;
			this._d = c2 * c1 + d2 * d1;
			this._tx = a2 * tx1 + b2 * ty1 + tx2;
			this._ty = c2 * tx1 + d2 * ty1 + ty2;
			if (!_dontNotify)
				this._changed();
		}
		return this;
	},

	appended: function(mx) {
		return this.clone().append(mx);
	},

	prepended: function(mx) {
		return this.clone().prepend(mx);
	},

	invert: function() {
		var a = this._a,
			b = this._b,
			c = this._c,
			d = this._d,
			tx = this._tx,
			ty = this._ty,
			det = a * d - b * c,
			res = null;
		if (det && !isNaN(det) && isFinite(tx) && isFinite(ty)) {
			this._a = d / det;
			this._b = -b / det;
			this._c = -c / det;
			this._d = a / det;
			this._tx = (c * ty - d * tx) / det;
			this._ty = (b * tx - a * ty) / det;
			res = this;
		}
		return res;
	},

	inverted: function() {
		return this.clone().invert();
	},

	concatenate: '#append',
	preConcatenate: '#prepend',
	chain: '#appended',

	_shiftless: function() {
		return new Matrix(this._a, this._b, this._c, this._d, 0, 0);
	},

	_orNullIfIdentity: function() {
		return this.isIdentity() ? null : this;
	},

	isIdentity: function() {
		return this._a === 1 && this._b === 0 && this._c === 0 && this._d === 1
				&& this._tx === 0 && this._ty === 0;
	},

	isInvertible: function() {
		var det = this._a * this._d - this._c * this._b;
		return det && !isNaN(det) && isFinite(this._tx) && isFinite(this._ty);
	},

	isSingular: function() {
		return !this.isInvertible();
	},

	transform: function( src, dst, count) {
		return arguments.length < 3
			? this._transformPoint(Point.read(arguments))
			: this._transformCoordinates(src, dst, count);
	},

	_transformPoint: function(point, dest, _dontNotify) {
		var x = point.x,
			y = point.y;
		if (!dest)
			dest = new Point();
		return dest._set(
				x * this._a + y * this._c + this._tx,
				x * this._b + y * this._d + this._ty,
				_dontNotify);
	},

	_transformCoordinates: function(src, dst, count) {
		for (var i = 0, max = 2 * count; i < max; i += 2) {
			var x = src[i],
				y = src[i + 1];
			dst[i] = x * this._a + y * this._c + this._tx;
			dst[i + 1] = x * this._b + y * this._d + this._ty;
		}
		return dst;
	},

	_transformCorners: function(rect) {
		var x1 = rect.x,
			y1 = rect.y,
			x2 = x1 + rect.width,
			y2 = y1 + rect.height,
			coords = [ x1, y1, x2, y1, x2, y2, x1, y2 ];
		return this._transformCoordinates(coords, coords, 4);
	},

	_transformBounds: function(bounds, dest, _dontNotify) {
		var coords = this._transformCorners(bounds),
			min = coords.slice(0, 2),
			max = min.slice();
		for (var i = 2; i < 8; i++) {
			var val = coords[i],
				j = i & 1;
			if (val < min[j]) {
				min[j] = val;
			} else if (val > max[j]) {
				max[j] = val;
			}
		}
		if (!dest)
			dest = new Rectangle();
		return dest._set(min[0], min[1], max[0] - min[0], max[1] - min[1],
				_dontNotify);
	},

	inverseTransform: function() {
		return this._inverseTransform(Point.read(arguments));
	},

	_inverseTransform: function(point, dest, _dontNotify) {
		var a = this._a,
			b = this._b,
			c = this._c,
			d = this._d,
			tx = this._tx,
			ty = this._ty,
			det = a * d - b * c,
			res = null;
		if (det && !isNaN(det) && isFinite(tx) && isFinite(ty)) {
			var x = point.x - this._tx,
				y = point.y - this._ty;
			if (!dest)
				dest = new Point();
			res = dest._set(
					(x * d - y * c) / det,
					(y * a - x * b) / det,
					_dontNotify);
		}
		return res;
	},

	decompose: function() {
		var a = this._a,
			b = this._b,
			c = this._c,
			d = this._d,
			det = a * d - b * c,
			sqrt = Math.sqrt,
			atan2 = Math.atan2,
			degrees = 180 / Math.PI,
			rotate,
			scale,
			skew;
		if (a !== 0 || b !== 0) {
			var r = sqrt(a * a + b * b);
			rotate = Math.acos(a / r) * (b > 0 ? 1 : -1);
			scale = [r, det / r];
			skew = [atan2(a * c + b * d, r * r), 0];
		} else if (c !== 0 || d !== 0) {
			var s = sqrt(c * c + d * d);
			rotate = Math.asin(c / s)  * (d > 0 ? 1 : -1);
			scale = [det / s, s];
			skew = [0, atan2(a * c + b * d, s * s)];
		} else {
			rotate = 0;
			skew = scale = [0, 0];
		}
		return {
			translation: this.getTranslation(),
			rotation: rotate * degrees,
			scaling: new Point(scale),
			skewing: new Point(skew[0] * degrees, skew[1] * degrees)
		};
	},

	getValues: function() {
		return [ this._a, this._b, this._c, this._d, this._tx, this._ty ];
	},

	getTranslation: function() {
		return new Point(this._tx, this._ty);
	},

	getScaling: function() {
		return this.decompose().scaling;
	},

	getRotation: function() {
		return this.decompose().rotation;
	},

	applyToContext: function(ctx) {
		if (!this.isIdentity()) {
			ctx.transform(this._a, this._b, this._c, this._d,
					this._tx, this._ty);
		}
	}
}, Base.each(['a', 'b', 'c', 'd', 'tx', 'ty'], function(key) {
	var part = Base.capitalize(key),
		prop = '_' + key;
	this['get' + part] = function() {
		return this[prop];
	};
	this['set' + part] = function(value) {
		this[prop] = value;
		this._changed();
	};
}, {}));

var Line = Base.extend({
	_class: 'Line',

	initialize: function Line(arg0, arg1, arg2, arg3, arg4) {
		var asVector = false;
		if (arguments.length >= 4) {
			this._px = arg0;
			this._py = arg1;
			this._vx = arg2;
			this._vy = arg3;
			asVector = arg4;
		} else {
			this._px = arg0.x;
			this._py = arg0.y;
			this._vx = arg1.x;
			this._vy = arg1.y;
			asVector = arg2;
		}
		if (!asVector) {
			this._vx -= this._px;
			this._vy -= this._py;
		}
	},

	getPoint: function() {
		return new Point(this._px, this._py);
	},

	getVector: function() {
		return new Point(this._vx, this._vy);
	},

	getLength: function() {
		return this.getVector().getLength();
	},

	intersect: function(line, isInfinite) {
		return Line.intersect(
				this._px, this._py, this._vx, this._vy,
				line._px, line._py, line._vx, line._vy,
				true, isInfinite);
	},

	getSide: function(point, isInfinite) {
		return Line.getSide(
				this._px, this._py, this._vx, this._vy,
				point.x, point.y, true, isInfinite);
	},

	getDistance: function(point) {
		return Math.abs(this.getSignedDistance(point));
	},

	getSignedDistance: function(point) {
		return Line.getSignedDistance(this._px, this._py, this._vx, this._vy,
				point.x, point.y, true);
	},

	isCollinear: function(line) {
		return Point.isCollinear(this._vx, this._vy, line._vx, line._vy);
	},

	isOrthogonal: function(line) {
		return Point.isOrthogonal(this._vx, this._vy, line._vx, line._vy);
	},

	statics: {
		intersect: function(p1x, p1y, v1x, v1y, p2x, p2y, v2x, v2y, asVector,
				isInfinite) {
			if (!asVector) {
				v1x -= p1x;
				v1y -= p1y;
				v2x -= p2x;
				v2y -= p2y;
			}
			var cross = v1x * v2y - v1y * v2x;
			if (!Numerical.isZero(cross)) {
				var dx = p1x - p2x,
					dy = p1y - p2y,
					u1 = (v2x * dy - v2y * dx) / cross,
					u2 = (v1x * dy - v1y * dx) / cross,
					epsilon = 1e-12,
					uMin = -epsilon,
					uMax = 1 + epsilon;
				if (isInfinite
						|| uMin < u1 && u1 < uMax && uMin < u2 && u2 < uMax) {
					if (!isInfinite) {
						u1 = u1 <= 0 ? 0 : u1 >= 1 ? 1 : u1;
					}
					return new Point(
							p1x + u1 * v1x,
							p1y + u1 * v1y);
				}
			}
		},

		getSide: function(px, py, vx, vy, x, y, asVector, isInfinite) {
			if (!asVector) {
				vx -= px;
				vy -= py;
			}
			var v2x = x - px,
				v2y = y - py,
				ccw = v2x * vy - v2y * vx;
			if (!isInfinite && Numerical.isZero(ccw)) {
				ccw = (v2x * vx + v2x * vx) / (vx * vx + vy * vy);
				if (ccw >= 0 && ccw <= 1)
					ccw = 0;
			}
			return ccw < 0 ? -1 : ccw > 0 ? 1 : 0;
		},

		getSignedDistance: function(px, py, vx, vy, x, y, asVector) {
			if (!asVector) {
				vx -= px;
				vy -= py;
			}
			return vx === 0 ? vy > 0 ? x - px : px - x
				 : vy === 0 ? vx < 0 ? y - py : py - y
				 : ((x-px) * vy - (y-py) * vx) / Math.sqrt(vx * vx + vy * vy);
		},

		getDistance: function(px, py, vx, vy, x, y, asVector) {
			return Math.abs(
					Line.getSignedDistance(px, py, vx, vy, x, y, asVector));
		}
	}
});

var Project = PaperScopeItem.extend({
	_class: 'Project',
	_list: 'projects',
	_reference: 'project',
	_compactSerialize: true,

	initialize: function Project(element) {
		PaperScopeItem.call(this, true);
		this._children = [];
		this._namedChildren = {};
		this._activeLayer = null;
		this._currentStyle = new Style(null, null, this);
		this._view = View.create(this,
				element || CanvasProvider.getCanvas(1, 1));
		this._selectionItems = {};
		this._selectionCount = 0;
		this._updateVersion = 0;
	},

	_serialize: function(options, dictionary) {
		return Base.serialize(this._children, options, true, dictionary);
	},

	_changed: function(flags, item) {
		if (flags & 1) {
			var view = this._view;
			if (view) {
				view._needsUpdate = true;
				if (!view._requested && view._autoUpdate)
					view.requestUpdate();
			}
		}
		var changes = this._changes;
		if (changes && item) {
			var changesById = this._changesById,
				id = item._id,
				entry = changesById[id];
			if (entry) {
				entry.flags |= flags;
			} else {
				changes.push(changesById[id] = { item: item, flags: flags });
			}
		}
	},

	clear: function() {
		var children = this._children;
		for (var i = children.length - 1; i >= 0; i--)
			children[i].remove();
	},

	isEmpty: function() {
		return !this._children.length;
	},

	remove: function remove() {
		if (!remove.base.call(this))
			return false;
		if (this._view)
			this._view.remove();
		return true;
	},

	getView: function() {
		return this._view;
	},

	getCurrentStyle: function() {
		return this._currentStyle;
	},

	setCurrentStyle: function(style) {
		this._currentStyle.set(style);
	},

	getIndex: function() {
		return this._index;
	},

	getOptions: function() {
		return this._scope.settings;
	},

	getLayers: function() {
		return this._children;
	},

	getActiveLayer: function() {
		return this._activeLayer || new Layer({ project: this, insert: true });
	},

	getSymbolDefinitions: function() {
		var definitions = [],
			ids = {};
		this.getItems({
			class: SymbolItem,
			match: function(item) {
				var definition = item._definition,
					id = definition._id;
				if (!ids[id]) {
					ids[id] = true;
					definitions.push(definition);
				}
				return false;
			}
		});
		return definitions;
	},

	getSymbols: 'getSymbolDefinitions',

	getSelectedItems: function() {
		var selectionItems = this._selectionItems,
			items = [];
		for (var id in selectionItems) {
			var item = selectionItems[id],
				selection = item._selection;
			if ((selection & 1) && item.isInserted()) {
				items.push(item);
			} else if (!selection) {
				this._updateSelection(item);
			}
		}
		return items;
	},

	_updateSelection: function(item) {
		var id = item._id,
			selectionItems = this._selectionItems;
		if (item._selection) {
			if (selectionItems[id] !== item) {
				this._selectionCount++;
				selectionItems[id] = item;
			}
		} else if (selectionItems[id] === item) {
			this._selectionCount--;
			delete selectionItems[id];
		}
	},

	selectAll: function() {
		var children = this._children;
		for (var i = 0, l = children.length; i < l; i++)
			children[i].setFullySelected(true);
	},

	deselectAll: function() {
		var selectionItems = this._selectionItems;
		for (var i in selectionItems)
			selectionItems[i].setFullySelected(false);
	},

	addLayer: function(layer) {
		return this.insertLayer(undefined, layer);
	},

	insertLayer: function(index, layer) {
		if (layer instanceof Layer) {
			layer._remove(false, true);
			Base.splice(this._children, [layer], index, 0);
			layer._setProject(this, true);
			var name = layer._name;
			if (name)
				layer.setName(name);
			if (this._changes)
				layer._changed(5);
			if (!this._activeLayer)
				this._activeLayer = layer;
		} else {
			layer = null;
		}
		return layer;
	},

	_insertItem: function(index, item, _created) {
		item = this.insertLayer(index, item)
				|| (this._activeLayer || this._insertItem(undefined,
						new Layer(Item.NO_INSERT), true))
						.insertChild(index, item);
		if (_created && item.activate)
			item.activate();
		return item;
	},

	getItems: function(options) {
		return Item._getItems(this, options);
	},

	getItem: function(options) {
		return Item._getItems(this, options, null, null, true)[0] || null;
	},

	importJSON: function(json) {
		this.activate();
		var layer = this._activeLayer;
		return Base.importJSON(json, layer && layer.isEmpty() && layer);
	},

	removeOn: function(type) {
		var sets = this._removeSets;
		if (sets) {
			if (type === 'mouseup')
				sets.mousedrag = null;
			var set = sets[type];
			if (set) {
				for (var id in set) {
					var item = set[id];
					for (var key in sets) {
						var other = sets[key];
						if (other && other != set)
							delete other[item._id];
					}
					item.remove();
				}
				sets[type] = null;
			}
		}
	},

	draw: function(ctx, matrix, pixelRatio) {
		this._updateVersion++;
		ctx.save();
		matrix.applyToContext(ctx);
		var children = this._children,
			param = new Base({
				offset: new Point(0, 0),
				pixelRatio: pixelRatio,
				viewMatrix: matrix.isIdentity() ? null : matrix,
				matrices: [new Matrix()],
				updateMatrix: true
			});
		for (var i = 0, l = children.length; i < l; i++) {
			children[i].draw(ctx, param);
		}
		ctx.restore();

		if (this._selectionCount > 0) {
			ctx.save();
			ctx.strokeWidth = 1;
			var items = this._selectionItems,
				size = this._scope.settings.handleSize,
				version = this._updateVersion;
			for (var id in items) {
				items[id]._drawSelection(ctx, matrix, size, items, version);
			}
			ctx.restore();
		}
	}
});

var Item = Base.extend(Emitter, {
	statics: {
		extend: function extend(src) {
			if (src._serializeFields)
				src._serializeFields = Base.set({},
					this.prototype._serializeFields, src._serializeFields);
			return extend.base.apply(this, arguments);
		},

		NO_INSERT: { insert: false }
	},

	_class: 'Item',
	_name: null,
	_applyMatrix: true,
	_canApplyMatrix: true,
	_canScaleStroke: false,
	_pivot: null,
	_visible: true,
	_blendMode: 'normal',
	_opacity: 1,
	_locked: false,
	_guide: false,
	_clipMask: false,
	_selection: 0,
	_selectBounds: true,
	_selectChildren: false,
	_serializeFields: {
		name: null,
		applyMatrix: null,
		matrix: new Matrix(),
		pivot: null,
		visible: true,
		blendMode: 'normal',
		opacity: 1,
		locked: false,
		guide: false,
		clipMask: false,
		selected: false,
		data: {}
	},
	_prioritize: ['applyMatrix']
},
new function() {
	var handlers = ['onMouseDown', 'onMouseUp', 'onMouseDrag', 'onClick',
			'onDoubleClick', 'onMouseMove', 'onMouseEnter', 'onMouseLeave'];
	return Base.each(handlers,
		function(name) {
			this._events[name] = {
				install: function(type) {
					this.getView()._countItemEvent(type, 1);
				},

				uninstall: function(type) {
					this.getView()._countItemEvent(type, -1);
				}
			};
		}, {
			_events: {
				onFrame: {
					install: function() {
						this.getView()._animateItem(this, true);
					},

					uninstall: function() {
						this.getView()._animateItem(this, false);
					}
				},

				onLoad: {},
				onError: {}
			},
			statics: {
				_itemHandlers: handlers
			}
		}
	);
}, {
	initialize: function Item() {
	},

	_initialize: function(props, point) {
		var hasProps = props && Base.isPlainObject(props),
			internal = hasProps && props.internal === true,
			matrix = this._matrix = new Matrix(),
			project = hasProps && props.project || paper.project,
			settings = paper.settings;
		this._id = internal ? null : UID.get();
		this._parent = this._index = null;
		this._applyMatrix = this._canApplyMatrix && settings.applyMatrix;
		if (point)
			matrix.translate(point);
		matrix._owner = this;
		this._style = new Style(project._currentStyle, this, project);
		if (internal || hasProps && props.insert == false
			|| !settings.insertItems && !(hasProps && props.insert === true)) {
			this._setProject(project);
		} else {
			(hasProps && props.parent || project)
					._insertItem(undefined, this, true);
		}
		if (hasProps && props !== Item.NO_INSERT) {
			this.set(props, {
				internal: true, insert: true, project: true, parent: true
			});
		}
		return hasProps;
	},

	_serialize: function(options, dictionary) {
		var props = {},
			that = this;

		function serialize(fields) {
			for (var key in fields) {
				var value = that[key];
				if (!Base.equals(value, key === 'leading'
						? fields.fontSize * 1.2 : fields[key])) {
					props[key] = Base.serialize(value, options,
							key !== 'data', dictionary);
				}
			}
		}

		serialize(this._serializeFields);
		if (!(this instanceof Group))
			serialize(this._style._defaults);
		return [ this._class, props ];
	},

	_changed: function(flags) {
		var symbol = this._symbol,
			cacheParent = this._parent || symbol,
			project = this._project;
		if (flags & 8) {
			this._bounds = this._position = this._decomposed = undefined;
		}
		if (flags & 16) {
			this._globalMatrix = undefined;
		}
		if (cacheParent
				&& (flags & 72)) {
			Item._clearBoundsCache(cacheParent);
		}
		if (flags & 2) {
			Item._clearBoundsCache(this);
		}
		if (project)
			project._changed(flags, this);
		if (symbol)
			symbol._changed(flags);
	},

	getId: function() {
		return this._id;
	},

	getName: function() {
		return this._name;
	},

	setName: function(name) {

		if (this._name)
			this._removeNamed();
		if (name === (+name) + '')
			throw new Error(
					'Names consisting only of numbers are not supported.');
		var owner = this._getOwner();
		if (name && owner) {
			var children = owner._children,
				namedChildren = owner._namedChildren;
			(namedChildren[name] = namedChildren[name] || []).push(this);
			if (!(name in children))
				children[name] = this;
		}
		this._name = name || undefined;
		this._changed(256);
	},

	getStyle: function() {
		return this._style;
	},

	setStyle: function(style) {
		this.getStyle().set(style);
	}
}, Base.each(['locked', 'visible', 'blendMode', 'opacity', 'guide'],
	function(name) {
		var part = Base.capitalize(name),
			key = '_' + name,
			flags = {
				locked: 256,
				visible: 265
			};
		this['get' + part] = function() {
			return this[key];
		};
		this['set' + part] = function(value) {
			if (value != this[key]) {
				this[key] = value;
				this._changed(flags[name] || 257);
			}
		};
	},
{}), {
	beans: true,

	getSelection: function() {
		return this._selection;
	},

	setSelection: function(selection) {
		if (selection !== this._selection) {
			this._selection = selection;
			var project = this._project;
			if (project) {
				project._updateSelection(this);
				this._changed(257);
			}
		}
	},

	_changeSelection: function(flag, selected) {
		var selection = this._selection;
		this.setSelection(selected ? selection | flag : selection & ~flag);
	},

	isSelected: function() {
		if (this._selectChildren) {
			var children = this._children;
			for (var i = 0, l = children.length; i < l; i++)
				if (children[i].isSelected())
					return true;
		}
		return !!(this._selection & 1);
	},

	setSelected: function(selected) {
		if (this._selectChildren) {
			var children = this._children;
			for (var i = 0, l = children.length; i < l; i++)
				children[i].setSelected(selected);
		}
		this._changeSelection(1, selected);
	},

	isFullySelected: function() {
		var children = this._children,
			selected = !!(this._selection & 1);
		if (children && selected) {
			for (var i = 0, l = children.length; i < l; i++)
				if (!children[i].isFullySelected())
					return false;
			return true;
		}
		return selected;
	},

	setFullySelected: function(selected) {
		var children = this._children;
		if (children) {
			for (var i = 0, l = children.length; i < l; i++)
				children[i].setFullySelected(selected);
		}
		this._changeSelection(1, selected);
	},

	isClipMask: function() {
		return this._clipMask;
	},

	setClipMask: function(clipMask) {
		if (this._clipMask != (clipMask = !!clipMask)) {
			this._clipMask = clipMask;
			if (clipMask) {
				this.setFillColor(null);
				this.setStrokeColor(null);
			}
			this._changed(257);
			if (this._parent)
				this._parent._changed(2048);
		}
	},

	getData: function() {
		if (!this._data)
			this._data = {};
		return this._data;
	},

	setData: function(data) {
		this._data = data;
	},

	getPosition: function(_dontLink) {
		var ctor = _dontLink ? Point : LinkedPoint;
		var position = this._position ||
			(this._position = this._getPositionFromBounds());
		return new ctor(position.x, position.y, this, 'setPosition');
	},

	setPosition: function() {
		this.translate(Point.read(arguments).subtract(this.getPosition(true)));
	},

	_getPositionFromBounds: function(bounds) {
		return this._pivot
				? this._matrix._transformPoint(this._pivot)
				: (bounds || this.getBounds()).getCenter(true);
	},

	getPivot: function() {
		var pivot = this._pivot;
		return pivot
				? new LinkedPoint(pivot.x, pivot.y, this, 'setPivot')
				: null;
	},

	setPivot: function() {
		this._pivot = Point.read(arguments, 0, { clone: true, readNull: true });
		this._position = undefined;
	}
}, Base.each({
		getStrokeBounds: { stroke: true },
		getHandleBounds: { handle: true },
		getInternalBounds: { internal: true }
	},
	function(options, key) {
		this[key] = function(matrix) {
			return this.getBounds(matrix, options);
		};
	},
{
	beans: true,

	getBounds: function(matrix, options) {
		var hasMatrix = options || matrix instanceof Matrix,
			opts = Base.set({}, hasMatrix ? options : matrix,
					this._boundsOptions);
		if (!opts.stroke || this.getStrokeScaling())
			opts.cacheItem = this;
		var rect = this._getCachedBounds(hasMatrix && matrix, opts).rect;
		return !arguments.length
				? new LinkedRectangle(rect.x, rect.y, rect.width, rect.height,
					this, 'setBounds')
				: rect;
	},

	setBounds: function() {
		var rect = Rectangle.read(arguments),
			bounds = this.getBounds(),
			_matrix = this._matrix,
			matrix = new Matrix(),
			center = rect.getCenter();
		matrix.translate(center);
		if (rect.width != bounds.width || rect.height != bounds.height) {
			if (!_matrix.isInvertible()) {
				_matrix.set(_matrix._backup
						|| new Matrix().translate(_matrix.getTranslation()));
				bounds = this.getBounds();
			}
			matrix.scale(
					bounds.width !== 0 ? rect.width / bounds.width : 0,
					bounds.height !== 0 ? rect.height / bounds.height : 0);
		}
		center = bounds.getCenter();
		matrix.translate(-center.x, -center.y);
		this.transform(matrix);
	},

	_getBounds: function(matrix, options) {
		var children = this._children;
		if (!children || !children.length)
			return new Rectangle();
		Item._updateBoundsCache(this, options.cacheItem);
		return Item._getBounds(children, matrix, options);
	},

	_getBoundsCacheKey: function(options, internal) {
		return [
			options.stroke ? 1 : 0,
			options.handle ? 1 : 0,
			internal ? 1 : 0
		].join('');
	},

	_getCachedBounds: function(matrix, options, noInternal) {
		matrix = matrix && matrix._orNullIfIdentity();
		var internal = options.internal && !noInternal,
			cacheItem = options.cacheItem,
			_matrix = internal ? null : this._matrix._orNullIfIdentity(),
			cacheKey = cacheItem && (!matrix || matrix.equals(_matrix))
				&& this._getBoundsCacheKey(options, internal),
			bounds = this._bounds;
		Item._updateBoundsCache(this._parent || this._symbol, cacheItem);
		if (cacheKey && bounds && cacheKey in bounds) {
			var cached = bounds[cacheKey];
			return {
				rect: cached.rect.clone(),
				nonscaling: cached.nonscaling
			};
		}
		var res = this._getBounds(matrix || _matrix, options),
			rect = res.rect || res,
			style = this._style,
			nonscaling = res.nonscaling || style.hasStroke()
				&& !style.getStrokeScaling();
		if (cacheKey) {
			if (!bounds) {
				this._bounds = bounds = {};
			}
			var cached = bounds[cacheKey] = {
				rect: rect.clone(),
				nonscaling: nonscaling,
				internal: internal
			};
		}
		return {
			rect: rect,
			nonscaling: nonscaling
		};
	},

	_getStrokeMatrix: function(matrix, options) {
		var parent = this.getStrokeScaling() ? null
				: options && options.internal ? this
					: this._parent || this._symbol && this._symbol._item,
			mx = parent ? parent.getViewMatrix().invert() : matrix;
		return mx && mx._shiftless();
	},

	statics: {
		_updateBoundsCache: function(parent, item) {
			if (parent && item) {
				var id = item._id,
					ref = parent._boundsCache = parent._boundsCache || {
						ids: {},
						list: []
					};
				if (!ref.ids[id]) {
					ref.list.push(item);
					ref.ids[id] = item;
				}
			}
		},

		_clearBoundsCache: function(item) {
			var cache = item._boundsCache;
			if (cache) {
				item._bounds = item._position = item._boundsCache = undefined;
				for (var i = 0, list = cache.list, l = list.length; i < l; i++){
					var other = list[i];
					if (other !== item) {
						other._bounds = other._position = undefined;
						if (other._boundsCache)
							Item._clearBoundsCache(other);
					}
				}
			}
		},

		_getBounds: function(items, matrix, options) {
			var x1 = Infinity,
				x2 = -x1,
				y1 = x1,
				y2 = x2,
				nonscaling = false;
			options = options || {};
			for (var i = 0, l = items.length; i < l; i++) {
				var item = items[i];
				if (item._visible && !item.isEmpty()) {
					var bounds = item._getCachedBounds(
						matrix && matrix.appended(item._matrix), options, true),
						rect = bounds.rect;
					x1 = Math.min(rect.x, x1);
					y1 = Math.min(rect.y, y1);
					x2 = Math.max(rect.x + rect.width, x2);
					y2 = Math.max(rect.y + rect.height, y2);
					if (bounds.nonscaling)
						nonscaling = true;
				}
			}
			return {
				rect: isFinite(x1)
					? new Rectangle(x1, y1, x2 - x1, y2 - y1)
					: new Rectangle(),
				nonscaling: nonscaling
			};
		}
	}

}), {
	beans: true,

	_decompose: function() {
		return this._applyMatrix
			? null
			: this._decomposed || (this._decomposed = this._matrix.decompose());
	},

	getRotation: function() {
		var decomposed = this._decompose();
		return decomposed ? decomposed.rotation : 0;
	},

	setRotation: function(rotation) {
		var current = this.getRotation();
		if (current != null && rotation != null) {
			var decomposed = this._decomposed;
			this.rotate(rotation - current);
			if (decomposed) {
				decomposed.rotation = rotation;
				this._decomposed = decomposed;
			}
		}
	},

	getScaling: function() {
		var decomposed = this._decompose(),
			s = decomposed && decomposed.scaling;
		return new LinkedPoint(s ? s.x : 1, s ? s.y : 1, this, 'setScaling');
	},

	setScaling: function() {
		var current = this.getScaling(),
			scaling = Point.read(arguments, 0, { clone: true, readNull: true });
		if (current && scaling && !current.equals(scaling)) {
			var rotation = this.getRotation(),
				decomposed = this._decomposed,
				matrix = new Matrix(),
				center = this.getPosition(true);
			matrix.translate(center);
			if (rotation)
				matrix.rotate(rotation);
			matrix.scale(scaling.x / current.x, scaling.y / current.y);
			if (rotation)
				matrix.rotate(-rotation);
			matrix.translate(center.negate());
			this.transform(matrix);
			if (decomposed) {
				decomposed.scaling = scaling;
				this._decomposed = decomposed;
			}
		}
	},

	getMatrix: function() {
		return this._matrix;
	},

	setMatrix: function() {
		var matrix = this._matrix;
		matrix.initialize.apply(matrix, arguments);
	},

	getGlobalMatrix: function(_dontClone) {
		var matrix = this._globalMatrix;
		if (matrix) {
			var parent = this._parent;
			var parents = [];
			while (parent) {
				if (!parent._globalMatrix) {
					matrix = null;
					for (var i = 0, l = parents.length; i < l; i++) {
						parents[i]._globalMatrix = null;
					}
					break;
				}
				parents.push(parent);
				parent = parent._parent;
			}
		}
		if (!matrix) {
			matrix = this._globalMatrix = this._matrix.clone();
			var parent = this._parent;
			if (parent)
				matrix.prepend(parent.getGlobalMatrix(true));
		}
		return _dontClone ? matrix : matrix.clone();
	},

	getViewMatrix: function() {
		return this.getGlobalMatrix().prepend(this.getView()._matrix);
	},

	getApplyMatrix: function() {
		return this._applyMatrix;
	},

	setApplyMatrix: function(apply) {
		if (this._applyMatrix = this._canApplyMatrix && !!apply)
			this.transform(null, true);
	},

	getTransformContent: '#getApplyMatrix',
	setTransformContent: '#setApplyMatrix',
}, {
	getProject: function() {
		return this._project;
	},

	_setProject: function(project, installEvents) {
		if (this._project !== project) {
			if (this._project)
				this._installEvents(false);
			this._project = project;
			var children = this._children;
			for (var i = 0, l = children && children.length; i < l; i++)
				children[i]._setProject(project);
			installEvents = true;
		}
		if (installEvents)
			this._installEvents(true);
	},

	getView: function() {
		return this._project._view;
	},

	_installEvents: function _installEvents(install) {
		_installEvents.base.call(this, install);
		var children = this._children;
		for (var i = 0, l = children && children.length; i < l; i++)
			children[i]._installEvents(install);
	},

	getLayer: function() {
		var parent = this;
		while (parent = parent._parent) {
			if (parent instanceof Layer)
				return parent;
		}
		return null;
	},

	getParent: function() {
		return this._parent;
	},

	setParent: function(item) {
		return item.addChild(this);
	},

	_getOwner: '#getParent',

	getChildren: function() {
		return this._children;
	},

	setChildren: function(items) {
		this.removeChildren();
		this.addChildren(items);
	},

	getFirstChild: function() {
		return this._children && this._children[0] || null;
	},

	getLastChild: function() {
		return this._children && this._children[this._children.length - 1]
				|| null;
	},

	getNextSibling: function() {
		var owner = this._getOwner();
		return owner && owner._children[this._index + 1] || null;
	},

	getPreviousSibling: function() {
		var owner = this._getOwner();
		return owner && owner._children[this._index - 1] || null;
	},

	getIndex: function() {
		return this._index;
	},

	equals: function(item) {
		return item === this || item && this._class === item._class
				&& this._style.equals(item._style)
				&& this._matrix.equals(item._matrix)
				&& this._locked === item._locked
				&& this._visible === item._visible
				&& this._blendMode === item._blendMode
				&& this._opacity === item._opacity
				&& this._clipMask === item._clipMask
				&& this._guide === item._guide
				&& this._equals(item)
				|| false;
	},

	_equals: function(item) {
		return Base.equals(this._children, item._children);
	},

	clone: function(options) {
		var copy = new this.constructor(Item.NO_INSERT),
			children = this._children,
			insert = Base.pick(options ? options.insert : undefined,
					options === undefined || options === true),
			deep = Base.pick(options ? options.deep : undefined, true);
		if (children)
			copy.copyAttributes(this);
		if (!children || deep)
			copy.copyContent(this);
		if (!children)
			copy.copyAttributes(this);
		if (insert)
			copy.insertAbove(this);
		var name = this._name,
			parent = this._parent;
		if (name && parent) {
			var children = parent._children,
				orig = name,
				i = 1;
			while (children[name])
				name = orig + ' ' + (i++);
			if (name !== orig)
				copy.setName(name);
		}
		return copy;
	},

	copyContent: function(source) {
		var children = source._children;
		for (var i = 0, l = children && children.length; i < l; i++) {
			this.addChild(children[i].clone(false), true);
		}
	},

	copyAttributes: function(source, excludeMatrix) {
		this.setStyle(source._style);
		var keys = ['_locked', '_visible', '_blendMode', '_opacity',
				'_clipMask', '_guide'];
		for (var i = 0, l = keys.length; i < l; i++) {
			var key = keys[i];
			if (source.hasOwnProperty(key))
				this[key] = source[key];
		}
		if (!excludeMatrix)
			this._matrix.set(source._matrix, true);
		this.setApplyMatrix(source._applyMatrix);
		this.setPivot(source._pivot);
		this.setSelection(source._selection);
		var data = source._data,
			name = source._name;
		this._data = data ? Base.clone(data) : null;
		if (name)
			this.setName(name);
	},

	rasterize: function(resolution, insert) {
		var bounds = this.getStrokeBounds(),
			scale = (resolution || this.getView().getResolution()) / 72,
			topLeft = bounds.getTopLeft().floor(),
			bottomRight = bounds.getBottomRight().ceil(),
			size = new Size(bottomRight.subtract(topLeft)),
			raster = new Raster(Item.NO_INSERT);
		if (!size.isZero()) {
			var canvas = CanvasProvider.getCanvas(size.multiply(scale)),
				ctx = canvas.getContext('2d'),
				matrix = new Matrix().scale(scale).translate(topLeft.negate());
			ctx.save();
			matrix.applyToContext(ctx);
			this.draw(ctx, new Base({ matrices: [matrix] }));
			ctx.restore();
			raster.setCanvas(canvas);
		}
		raster.transform(new Matrix().translate(topLeft.add(size.divide(2)))
				.scale(1 / scale));
		if (insert === undefined || insert)
			raster.insertAbove(this);
		return raster;
	},

	contains: function() {
		return !!this._contains(
				this._matrix._inverseTransform(Point.read(arguments)));
	},

	_contains: function(point) {
		var children = this._children;
		if (children) {
			for (var i = children.length - 1; i >= 0; i--) {
				if (children[i].contains(point))
					return true;
			}
			return false;
		}
		return point.isInside(this.getInternalBounds());
	},

	isInside: function() {
		return Rectangle.read(arguments).contains(this.getBounds());
	},

	_asPathItem: function() {
		return new Path.Rectangle({
			rectangle: this.getInternalBounds(),
			matrix: this._matrix,
			insert: false,
		});
	},

	intersects: function(item, _matrix) {
		if (!(item instanceof Item))
			return false;
		return this._asPathItem().getIntersections(item._asPathItem(), null,
				_matrix, true).length > 0;
	}
},
new function() {
	function hitTest() {
		return this._hitTest(
				Point.read(arguments),
				HitResult.getOptions(arguments));
	}

	function hitTestAll() {
		var point = Point.read(arguments),
			options = HitResult.getOptions(arguments),
			all = [];
		this._hitTest(point, Base.set({ all: all }, options));
		return all;
	}

	function hitTestChildren(point, options, viewMatrix, _exclude) {
		var children = this._children;
		if (children) {
			for (var i = children.length - 1; i >= 0; i--) {
				var child = children[i];
				var res = child !== _exclude && child._hitTest(point, options,
						viewMatrix);
				if (res && !options.all)
					return res;
			}
		}
		return null;
	}

	Project.inject({
		hitTest: hitTest,
		hitTestAll: hitTestAll,
		_hitTest: hitTestChildren
	});

	return {
		hitTest: hitTest,
		hitTestAll: hitTestAll,
		_hitTestChildren: hitTestChildren,
	};
}, {

	_hitTest: function(point, options, parentViewMatrix) {
		if (this._locked || !this._visible || this._guide && !options.guides
				|| this.isEmpty()) {
			return null;
		}

		var matrix = this._matrix,
			viewMatrix = parentViewMatrix
					? parentViewMatrix.appended(matrix)
					: this.getGlobalMatrix().prepend(this.getView()._matrix),
			tolerance = Math.max(options.tolerance, 1e-12),
			tolerancePadding = options._tolerancePadding = new Size(
					Path._getStrokePadding(tolerance,
						matrix._shiftless().invert()));
		point = matrix._inverseTransform(point);
		if (!point || !this._children &&
			!this.getBounds({ internal: true, stroke: true, handle: true })
				.expand(tolerancePadding.multiply(2))._containsPoint(point)) {
			return null;
		}

		var checkSelf = !(options.guides && !this._guide
				|| options.selected && !this.isSelected()
				|| options.type && options.type !== Base.hyphenate(this._class)
				|| options.class && !(this instanceof options.class)),
			match = options.match,
			that = this,
			bounds,
			res;

		function filter(hit) {
			if (hit && match && !match(hit))
				hit = null;
			if (hit && options.all)
				options.all.push(hit);
			return hit;
		}

		function checkPoint(type, part) {
			var pt = part ? bounds['get' + part]() : that.getPosition();
			if (point.subtract(pt).divide(tolerancePadding).length <= 1) {
				return new HitResult(type, that, {
					name: part ? Base.hyphenate(part) : type,
					point: pt
				});
			}
		}

		var checkPosition = options.position,
			checkCenter = options.center,
			checkBounds = options.bounds;
		if (checkSelf && this._parent
				&& (checkPosition || checkCenter || checkBounds)) {
			if (checkCenter || checkBounds) {
				bounds = this.getInternalBounds();
			}
			res = checkPosition && checkPoint('position') ||
					checkCenter && checkPoint('center', 'Center');
			if (!res && checkBounds) {
				var points = [
					'TopLeft', 'TopRight', 'BottomLeft', 'BottomRight',
					'LeftCenter', 'TopCenter', 'RightCenter', 'BottomCenter'
				];
				for (var i = 0; i < 8 && !res; i++) {
					res = checkPoint('bounds', points[i]);
				}
			}
			res = filter(res);
		}

		if (!res) {
			res = this._hitTestChildren(point, options, viewMatrix)
				|| checkSelf
					&& filter(this._hitTestSelf(point, options, viewMatrix,
						this.getStrokeScaling() ? null
							: viewMatrix._shiftless().invert()))
				|| null;
		}
		if (res && res.point) {
			res.point = matrix.transform(res.point);
		}
		return res;
	},

	_hitTestSelf: function(point, options) {
		if (options.fill && this.hasFill() && this._contains(point))
			return new HitResult('fill', this);
	},

	matches: function(name, compare) {
		function matchObject(obj1, obj2) {
			for (var i in obj1) {
				if (obj1.hasOwnProperty(i)) {
					var val1 = obj1[i],
						val2 = obj2[i];
					if (Base.isPlainObject(val1) && Base.isPlainObject(val2)) {
						if (!matchObject(val1, val2))
							return false;
					} else if (!Base.equals(val1, val2)) {
						return false;
					}
				}
			}
			return true;
		}
		var type = typeof name;
		if (type === 'object') {
			for (var key in name) {
				if (name.hasOwnProperty(key) && !this.matches(key, name[key]))
					return false;
			}
			return true;
		} else if (type === 'function') {
			return name(this);
		} else if (name === 'match') {
			return compare(this);
		} else {
			var value = /^(empty|editable)$/.test(name)
					? this['is' + Base.capitalize(name)]()
					: name === 'type'
						? Base.hyphenate(this._class)
						: this[name];
			if (name === 'class') {
				if (typeof compare === 'function')
					return this instanceof compare;
				value = this._class;
			}
			if (typeof compare === 'function') {
				return !!compare(value);
			} else if (compare) {
				if (compare.test) {
					return compare.test(value);
				} else if (Base.isPlainObject(compare)) {
					return matchObject(compare, value);
				}
			}
			return Base.equals(value, compare);
		}
	},

	getItems: function(options) {
		return Item._getItems(this, options, this._matrix);
	},

	getItem: function(options) {
		return Item._getItems(this, options, this._matrix, null, true)[0]
				|| null;
	},

	statics: {
		_getItems: function _getItems(item, options, matrix, param, firstOnly) {
			if (!param) {
				var obj = typeof options === 'object' && options,
					overlapping = obj && obj.overlapping,
					inside = obj && obj.inside,
					bounds = overlapping || inside,
					rect = bounds && Rectangle.read([bounds]);
				param = {
					items: [],
					recursive: obj && obj.recursive !== false,
					inside: !!inside,
					overlapping: !!overlapping,
					rect: rect,
					path: overlapping && new Path.Rectangle({
						rectangle: rect,
						insert: false
					})
				};
				if (obj) {
					options = Base.filter({}, options, {
						recursive: true, inside: true, overlapping: true
					});
				}
			}
			var children = item._children,
				items = param.items,
				rect = param.rect;
			matrix = rect && (matrix || new Matrix());
			for (var i = 0, l = children && children.length; i < l; i++) {
				var child = children[i],
					childMatrix = matrix && matrix.appended(child._matrix),
					add = true;
				if (rect) {
					var bounds = child.getBounds(childMatrix);
					if (!rect.intersects(bounds))
						continue;
					if (!(rect.contains(bounds)
							|| param.overlapping && (bounds.contains(rect)
								|| param.path.intersects(child, childMatrix))))
						add = false;
				}
				if (add && child.matches(options)) {
					items.push(child);
					if (firstOnly)
						break;
				}
				if (param.recursive !== false) {
					_getItems(child, options, childMatrix, param, firstOnly);
				}
				if (firstOnly && items.length > 0)
					break;
			}
			return items;
		}
	}
}, {

	importJSON: function(json) {
		var res = Base.importJSON(json, this);
		return res !== this ? this.addChild(res) : res;
	},

	addChild: function(item) {
		return this.insertChild(undefined, item);
	},

	insertChild: function(index, item) {
		var res = item ? this.insertChildren(index, [item]) : null;
		return res && res[0];
	},

	addChildren: function(items) {
		return this.insertChildren(this._children.length, items);
	},

	insertChildren: function(index, items) {
		var children = this._children;
		if (children && items && items.length > 0) {
			items = Base.slice(items);
			var inserted = {};
			for (var i = items.length - 1; i >= 0; i--) {
				var item = items[i],
					id = item && item._id;
				if (!item || inserted[id]) {
					items.splice(i, 1);
				} else {
					item._remove(false, true);
					inserted[id] = true;
				}
			}
			Base.splice(children, items, index, 0);
			var project = this._project,
				notifySelf = project._changes;
			for (var i = 0, l = items.length; i < l; i++) {
				var item = items[i],
					name = item._name;
				item._parent = this;
				item._setProject(project, true);
				if (name)
					item.setName(name);
				if (notifySelf)
					item._changed(5);
			}
			this._changed(11);
		} else {
			items = null;
		}
		return items;
	},

	_insertItem: '#insertChild',

	_insertAt: function(item, offset) {
		var owner = item && item._getOwner(),
			res = item !== this && owner ? this : null;
		if (res) {
			res._remove(false, true);
			owner._insertItem(item._index + offset, res);
		}
		return res;
	},

	insertAbove: function(item) {
		return this._insertAt(item, 1);
	},

	insertBelow: function(item) {
		return this._insertAt(item, 0);
	},

	sendToBack: function() {
		var owner = this._getOwner();
		return owner ? owner._insertItem(0, this) : null;
	},

	bringToFront: function() {
		var owner = this._getOwner();
		return owner ? owner._insertItem(undefined, this) : null;
	},

	appendTop: '#addChild',

	appendBottom: function(item) {
		return this.insertChild(0, item);
	},

	moveAbove: '#insertAbove',

	moveBelow: '#insertBelow',

	addTo: function(owner) {
		return owner._insertItem(undefined, this);
	},

	copyTo: function(owner) {
		return this.clone(false).addTo(owner);
	},

	reduce: function(options) {
		var children = this._children;
		if (children && children.length === 1) {
			var child = children[0].reduce(options);
			if (this._parent) {
				child.insertAbove(this);
				this.remove();
			} else {
				child.remove();
			}
			return child;
		}
		return this;
	},

	_removeNamed: function() {
		var owner = this._getOwner();
		if (owner) {
			var children = owner._children,
				namedChildren = owner._namedChildren,
				name = this._name,
				namedArray = namedChildren[name],
				index = namedArray ? namedArray.indexOf(this) : -1;
			if (index !== -1) {
				if (children[name] == this)
					delete children[name];
				namedArray.splice(index, 1);
				if (namedArray.length) {
					children[name] = namedArray[0];
				} else {
					delete namedChildren[name];
				}
			}
		}
	},

	_remove: function(notifySelf, notifyParent) {
		var owner = this._getOwner(),
			project = this._project,
			index = this._index;
		if (this._style)
			this._style._dispose();
		if (owner) {
			if (this._name)
				this._removeNamed();
			if (index != null) {
				if (project._activeLayer === this)
					project._activeLayer = this.getNextSibling()
							|| this.getPreviousSibling();
				Base.splice(owner._children, null, index, 1);
			}
			this._installEvents(false);
			if (notifySelf && project._changes)
				this._changed(5);
			if (notifyParent)
				owner._changed(11, this);
			this._parent = null;
			return true;
		}
		return false;
	},

	remove: function() {
		return this._remove(true, true);
	},

	replaceWith: function(item) {
		var ok = item && item.insertBelow(this);
		if (ok)
			this.remove();
		return ok;
	},

	removeChildren: function(start, end) {
		if (!this._children)
			return null;
		start = start || 0;
		end = Base.pick(end, this._children.length);
		var removed = Base.splice(this._children, null, start, end - start);
		for (var i = removed.length - 1; i >= 0; i--) {
			removed[i]._remove(true, false);
		}
		if (removed.length > 0)
			this._changed(11);
		return removed;
	},

	clear: '#removeChildren',

	reverseChildren: function() {
		if (this._children) {
			this._children.reverse();
			for (var i = 0, l = this._children.length; i < l; i++)
				this._children[i]._index = i;
			this._changed(11);
		}
	},

	isEmpty: function() {
		var children = this._children;
		return !children || !children.length;
	},

	isEditable: function() {
		var item = this;
		while (item) {
			if (!item._visible || item._locked)
				return false;
			item = item._parent;
		}
		return true;
	},

	hasFill: function() {
		return this.getStyle().hasFill();
	},

	hasStroke: function() {
		return this.getStyle().hasStroke();
	},

	hasShadow: function() {
		return this.getStyle().hasShadow();
	},

	_getOrder: function(item) {
		function getList(item) {
			var list = [];
			do {
				list.unshift(item);
			} while (item = item._parent);
			return list;
		}
		var list1 = getList(this),
			list2 = getList(item);
		for (var i = 0, l = Math.min(list1.length, list2.length); i < l; i++) {
			if (list1[i] != list2[i]) {
				return list1[i]._index < list2[i]._index ? 1 : -1;
			}
		}
		return 0;
	},

	hasChildren: function() {
		return this._children && this._children.length > 0;
	},

	isInserted: function() {
		return this._parent ? this._parent.isInserted() : false;
	},

	isAbove: function(item) {
		return this._getOrder(item) === -1;
	},

	isBelow: function(item) {
		return this._getOrder(item) === 1;
	},

	isParent: function(item) {
		return this._parent === item;
	},

	isChild: function(item) {
		return item && item._parent === this;
	},

	isDescendant: function(item) {
		var parent = this;
		while (parent = parent._parent) {
			if (parent === item)
				return true;
		}
		return false;
	},

	isAncestor: function(item) {
		return item ? item.isDescendant(this) : false;
	},

	isSibling: function(item) {
		return this._parent === item._parent;
	},

	isGroupedWith: function(item) {
		var parent = this._parent;
		while (parent) {
			if (parent._parent
				&& /^(Group|Layer|CompoundPath)$/.test(parent._class)
				&& item.isDescendant(parent))
					return true;
			parent = parent._parent;
		}
		return false;
	},

}, Base.each(['rotate', 'scale', 'shear', 'skew'], function(key) {
	var rotate = key === 'rotate';
	this[key] = function() {
		var value = (rotate ? Base : Point).read(arguments),
			center = Point.read(arguments, 0, { readNull: true });
		return this.transform(new Matrix()[key](value,
				center || this.getPosition(true)));
	};
}, {
	translate: function() {
		var mx = new Matrix();
		return this.transform(mx.translate.apply(mx, arguments));
	},

	transform: function(matrix, _applyMatrix, _applyRecursively,
			_setApplyMatrix) {
		var _matrix = this._matrix,
			transformMatrix = matrix && !matrix.isIdentity(),
			applyMatrix = (_applyMatrix || this._applyMatrix)
					&& ((!_matrix.isIdentity() || transformMatrix)
						|| _applyMatrix && _applyRecursively && this._children);
		if (!transformMatrix && !applyMatrix)
			return this;
		if (transformMatrix) {
			if (!matrix.isInvertible() && _matrix.isInvertible())
				_matrix._backup = _matrix.getValues();
			_matrix.prepend(matrix, true);
			var style = this._style,
				fillColor = style.getFillColor(true),
				strokeColor = style.getStrokeColor(true);
			if (fillColor)
				fillColor.transform(matrix);
			if (strokeColor)
				strokeColor.transform(matrix);
		}
		if (applyMatrix && (applyMatrix = this._transformContent(_matrix,
				_applyRecursively, _setApplyMatrix))) {
			var pivot = this._pivot;
			if (pivot)
				_matrix._transformPoint(pivot, pivot, true);
			_matrix.reset(true);
			if (_setApplyMatrix && this._canApplyMatrix)
				this._applyMatrix = true;
		}
		var bounds = this._bounds,
			position = this._position;
		if (transformMatrix || applyMatrix) {
			this._changed(25);
		}
		var decomp = transformMatrix && bounds && matrix.decompose();
		if (decomp && decomp.skewing.isZero() && decomp.rotation % 90 === 0) {
			for (var key in bounds) {
				var cache = bounds[key];
				if (cache.nonscaling) {
					delete bounds[key];
				} else if (applyMatrix || !cache.internal) {
					var rect = cache.rect;
					matrix._transformBounds(rect, rect);
				}
			}
			this._bounds = bounds;
			var cached = bounds[this._getBoundsCacheKey(
				this._boundsOptions || {})];
			if (cached) {
				this._position = this._getPositionFromBounds(cached.rect);
			}
		} else if (transformMatrix && position && this._pivot) {
			this._position = matrix._transformPoint(position, position);
		}
		return this;
	},

	_transformContent: function(matrix, applyRecursively, setApplyMatrix) {
		var children = this._children;
		if (children) {
			for (var i = 0, l = children.length; i < l; i++)
				children[i].transform(matrix, true, applyRecursively,
						setApplyMatrix);
			return true;
		}
	},

	globalToLocal: function() {
		return this.getGlobalMatrix(true)._inverseTransform(
				Point.read(arguments));
	},

	localToGlobal: function() {
		return this.getGlobalMatrix(true)._transformPoint(
				Point.read(arguments));
	},

	parentToLocal: function() {
		return this._matrix._inverseTransform(Point.read(arguments));
	},

	localToParent: function() {
		return this._matrix._transformPoint(Point.read(arguments));
	},

	fitBounds: function(rectangle, fill) {
		rectangle = Rectangle.read(arguments);
		var bounds = this.getBounds(),
			itemRatio = bounds.height / bounds.width,
			rectRatio = rectangle.height / rectangle.width,
			scale = (fill ? itemRatio > rectRatio : itemRatio < rectRatio)
					? rectangle.width / bounds.width
					: rectangle.height / bounds.height,
			newBounds = new Rectangle(new Point(),
					new Size(bounds.width * scale, bounds.height * scale));
		newBounds.setCenter(rectangle.getCenter());
		this.setBounds(newBounds);
	}
}), {

	_setStyles: function(ctx, param, viewMatrix) {
		var style = this._style,
			matrix = this._matrix;
		if (style.hasFill()) {
			ctx.fillStyle = style.getFillColor().toCanvasStyle(ctx, matrix);
		}
		if (style.hasStroke()) {
			ctx.strokeStyle = style.getStrokeColor().toCanvasStyle(ctx, matrix);
			ctx.lineWidth = style.getStrokeWidth();
			var strokeJoin = style.getStrokeJoin(),
				strokeCap = style.getStrokeCap(),
				miterLimit = style.getMiterLimit();
			if (strokeJoin)
				ctx.lineJoin = strokeJoin;
			if (strokeCap)
				ctx.lineCap = strokeCap;
			if (miterLimit)
				ctx.miterLimit = miterLimit;
			if (paper.support.nativeDash) {
				var dashArray = style.getDashArray(),
					dashOffset = style.getDashOffset();
				if (dashArray && dashArray.length) {
					if ('setLineDash' in ctx) {
						ctx.setLineDash(dashArray);
						ctx.lineDashOffset = dashOffset;
					} else {
						ctx.mozDash = dashArray;
						ctx.mozDashOffset = dashOffset;
					}
				}
			}
		}
		if (style.hasShadow()) {
			var pixelRatio = param.pixelRatio || 1,
				mx = viewMatrix._shiftless().prepend(
					new Matrix().scale(pixelRatio, pixelRatio)),
				blur = mx.transform(new Point(style.getShadowBlur(), 0)),
				offset = mx.transform(this.getShadowOffset());
			ctx.shadowColor = style.getShadowColor().toCanvasStyle(ctx);
			ctx.shadowBlur = blur.getLength();
			ctx.shadowOffsetX = offset.x;
			ctx.shadowOffsetY = offset.y;
		}
	},

	draw: function(ctx, param, parentStrokeMatrix) {
		var updateVersion = this._updateVersion = this._project._updateVersion;
		if (!this._visible || this._opacity === 0)
			return;
		var matrices = param.matrices,
			viewMatrix = param.viewMatrix,
			matrix = this._matrix,
			globalMatrix = matrices[matrices.length - 1].appended(matrix);
		if (!globalMatrix.isInvertible())
			return;

		viewMatrix = viewMatrix ? viewMatrix.appended(globalMatrix)
				: globalMatrix;

		matrices.push(globalMatrix);
		if (param.updateMatrix) {
			this._globalMatrix = globalMatrix;
		}

		var blendMode = this._blendMode,
			opacity = this._opacity,
			normalBlend = blendMode === 'normal',
			nativeBlend = BlendMode.nativeModes[blendMode],
			direct = normalBlend && opacity === 1
					|| param.dontStart
					|| param.clip
					|| (nativeBlend || normalBlend && opacity < 1)
						&& this._canComposite(),
			pixelRatio = param.pixelRatio || 1,
			mainCtx, itemOffset, prevOffset;
		if (!direct) {
			var bounds = this.getStrokeBounds(viewMatrix);
			if (!bounds.width || !bounds.height) {
				matrices.pop();
				return;
			}
			prevOffset = param.offset;
			itemOffset = param.offset = bounds.getTopLeft().floor();
			mainCtx = ctx;
			ctx = CanvasProvider.getContext(bounds.getSize().ceil().add(1)
					.multiply(pixelRatio));
			if (pixelRatio !== 1)
				ctx.scale(pixelRatio, pixelRatio);
		}
		ctx.save();
		var strokeMatrix = parentStrokeMatrix
				? parentStrokeMatrix.appended(matrix)
				: this._canScaleStroke && !this.getStrokeScaling(true)
					&& viewMatrix,
			clip = !direct && param.clipItem,
			transform = !strokeMatrix || clip;
		if (direct) {
			ctx.globalAlpha = opacity;
			if (nativeBlend)
				ctx.globalCompositeOperation = blendMode;
		} else if (transform) {
			ctx.translate(-itemOffset.x, -itemOffset.y);
		}
		if (transform) {
			(direct ? matrix : viewMatrix).applyToContext(ctx);
		}
		if (clip) {
			param.clipItem.draw(ctx, param.extend({ clip: true }));
		}
		if (strokeMatrix) {
			ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
			var offset = param.offset;
			if (offset)
				ctx.translate(-offset.x, -offset.y);
		}
		this._draw(ctx, param, viewMatrix, strokeMatrix);
		ctx.restore();
		matrices.pop();
		if (param.clip && !param.dontFinish)
			ctx.clip();
		if (!direct) {
			BlendMode.process(blendMode, ctx, mainCtx, opacity,
					itemOffset.subtract(prevOffset).multiply(pixelRatio));
			CanvasProvider.release(ctx);
			param.offset = prevOffset;
		}
	},

	_isUpdated: function(updateVersion) {
		var parent = this._parent;
		if (parent instanceof CompoundPath)
			return parent._isUpdated(updateVersion);
		var updated = this._updateVersion === updateVersion;
		if (!updated && parent && parent._visible
				&& parent._isUpdated(updateVersion)) {
			this._updateVersion = updateVersion;
			updated = true;
		}
		return updated;
	},

	_drawSelection: function(ctx, matrix, size, selectionItems, updateVersion) {
		var selection = this._selection,
			itemSelected = selection & 1,
			boundsSelected = selection & 2
					|| itemSelected && this._selectBounds,
			positionSelected = selection & 4;
		if (!this._drawSelected)
			itemSelected = false;
		if ((itemSelected || boundsSelected || positionSelected)
				&& this._isUpdated(updateVersion)) {
			var layer,
				color = this.getSelectedColor(true) || (layer = this.getLayer())
					&& layer.getSelectedColor(true),
				mx = matrix.appended(this.getGlobalMatrix(true)),
				half = size / 2;
			ctx.strokeStyle = ctx.fillStyle = color
					? color.toCanvasStyle(ctx) : '#009dec';
			if (itemSelected)
				this._drawSelected(ctx, mx, selectionItems);
			if (positionSelected) {
				var pos = this.getPosition(true),
					parent = this._parent,
					point = parent ? parent.localToGlobal(pos) : pos,
					x = point.x,
					y = point.y;
				ctx.beginPath();
				ctx.arc(x, y, half, 0, Math.PI * 2, true);
				ctx.stroke();
				var deltas = [[0, -1], [1, 0], [0, 1], [-1, 0]],
					start = half,
					end = size + 1;
				for (var i = 0; i < 4; i++) {
					var delta = deltas[i],
						dx = delta[0],
						dy = delta[1];
					ctx.moveTo(x + dx * start, y + dy * start);
					ctx.lineTo(x + dx * end, y + dy * end);
					ctx.stroke();
				}
			}
			if (boundsSelected) {
				var coords = mx._transformCorners(this.getInternalBounds());
				ctx.beginPath();
				for (var i = 0; i < 8; i++) {
					ctx[!i ? 'moveTo' : 'lineTo'](coords[i], coords[++i]);
				}
				ctx.closePath();
				ctx.stroke();
				for (var i = 0; i < 8; i++) {
					ctx.fillRect(coords[i] - half, coords[++i] - half,
							size, size);
				}
			}
		}
	},

	_canComposite: function() {
		return false;
	}
}, Base.each(['down', 'drag', 'up', 'move'], function(key) {
	this['removeOn' + Base.capitalize(key)] = function() {
		var hash = {};
		hash[key] = true;
		return this.removeOn(hash);
	};
}, {

	removeOn: function(obj) {
		for (var name in obj) {
			if (obj[name]) {
				var key = 'mouse' + name,
					project = this._project,
					sets = project._removeSets = project._removeSets || {};
				sets[key] = sets[key] || {};
				sets[key][this._id] = this;
			}
		}
		return this;
	}
}), {
	tween: function(from, to, options) {
		if (!options) {
			options = to;
			to = from;
			from = null;
			if (!options) {
				options = to;
				to = null;
			}
		}
		var easing = options && options.easing,
			start = options && options.start,
			duration = options != null && (
				typeof options === 'number' ? options : options.duration
			),
			tween = new Tween(this, from, to, duration, easing, start);
		function onFrame(event) {
			tween._handleFrame(event.time * 1000);
			if (!tween.running) {
				this.off('frame', onFrame);
			}
		}
		if (duration) {
			this.on('frame', onFrame);
		}
		return tween;
	},

	tweenTo: function(to, options) {
		return this.tween(null, to, options);
	},

	tweenFrom: function(from, options) {
		return this.tween(from, null, options);
	}
});

var Group = Item.extend({
	_class: 'Group',
	_selectBounds: false,
	_selectChildren: true,
	_serializeFields: {
		children: []
	},

	initialize: function Group(arg) {
		this._children = [];
		this._namedChildren = {};
		if (!this._initialize(arg))
			this.addChildren(Array.isArray(arg) ? arg : arguments);
	},

	_changed: function _changed(flags) {
		_changed.base.call(this, flags);
		if (flags & 2050) {
			this._clipItem = undefined;
		}
	},

	_getClipItem: function() {
		var clipItem = this._clipItem;
		if (clipItem === undefined) {
			clipItem = null;
			var children = this._children;
			for (var i = 0, l = children.length; i < l; i++) {
				if (children[i]._clipMask) {
					clipItem = children[i];
					break;
				}
			}
			this._clipItem = clipItem;
		}
		return clipItem;
	},

	isClipped: function() {
		return !!this._getClipItem();
	},

	setClipped: function(clipped) {
		var child = this.getFirstChild();
		if (child)
			child.setClipMask(clipped);
	},

	_getBounds: function _getBounds(matrix, options) {
		var clipItem = this._getClipItem();
		return clipItem
			? clipItem._getCachedBounds(
				matrix && matrix.appended(clipItem._matrix),
				Base.set({}, options, { stroke: false }))
			: _getBounds.base.call(this, matrix, options);
	},

	_hitTestChildren: function _hitTestChildren(point, options, viewMatrix) {
		var clipItem = this._getClipItem();
		return (!clipItem || clipItem.contains(point))
				&& _hitTestChildren.base.call(this, point, options, viewMatrix,
					clipItem);
	},

	_draw: function(ctx, param) {
		var clip = param.clip,
			clipItem = !clip && this._getClipItem();
		param = param.extend({ clipItem: clipItem, clip: false });
		if (clip) {
			ctx.beginPath();
			param.dontStart = param.dontFinish = true;
		} else if (clipItem) {
			clipItem.draw(ctx, param.extend({ clip: true }));
		}
		var children = this._children;
		for (var i = 0, l = children.length; i < l; i++) {
			var item = children[i];
			if (item !== clipItem)
				item.draw(ctx, param);
		}
	}
});

var Layer = Group.extend({
	_class: 'Layer',

	initialize: function Layer() {
		Group.apply(this, arguments);
	},

	_getOwner: function() {
		return this._parent || this._index != null && this._project;
	},

	isInserted: function isInserted() {
		return this._parent ? isInserted.base.call(this) : this._index != null;
	},

	activate: function() {
		this._project._activeLayer = this;
	},

	_hitTestSelf: function() {
	}
});

var Shape = Item.extend({
	_class: 'Shape',
	_applyMatrix: false,
	_canApplyMatrix: false,
	_canScaleStroke: true,
	_serializeFields: {
		type: null,
		size: null,
		radius: null
	},

	initialize: function Shape(props, point) {
		this._initialize(props, point);
	},

	_equals: function(item) {
		return this._type === item._type
			&& this._size.equals(item._size)
			&& Base.equals(this._radius, item._radius);
	},

	copyContent: function(source) {
		this.setType(source._type);
		this.setSize(source._size);
		this.setRadius(source._radius);
	},

	getType: function() {
		return this._type;
	},

	setType: function(type) {
		this._type = type;
	},

	getShape: '#getType',
	setShape: '#setType',

	getSize: function() {
		var size = this._size;
		return new LinkedSize(size.width, size.height, this, 'setSize');
	},

	setSize: function() {
		var size = Size.read(arguments);
		if (!this._size) {
			this._size = size.clone();
		} else if (!this._size.equals(size)) {
			var type = this._type,
				width = size.width,
				height = size.height;
			if (type === 'rectangle') {
				this._radius.set(Size.min(this._radius, size.divide(2)));
			} else if (type === 'circle') {
				width = height = (width + height) / 2;
				this._radius = width / 2;
			} else if (type === 'ellipse') {
				this._radius._set(width / 2, height / 2);
			}
			this._size._set(width, height);
			this._changed(9);
		}
	},

	getRadius: function() {
		var rad = this._radius;
		return this._type === 'circle'
				? rad
				: new LinkedSize(rad.width, rad.height, this, 'setRadius');
	},

	setRadius: function(radius) {
		var type = this._type;
		if (type === 'circle') {
			if (radius === this._radius)
				return;
			var size = radius * 2;
			this._radius = radius;
			this._size._set(size, size);
		} else {
			radius = Size.read(arguments);
			if (!this._radius) {
				this._radius = radius.clone();
			} else {
				if (this._radius.equals(radius))
					return;
				this._radius.set(radius);
				if (type === 'rectangle') {
					var size = Size.max(this._size, radius.multiply(2));
					this._size.set(size);
				} else if (type === 'ellipse') {
					this._size._set(radius.width * 2, radius.height * 2);
				}
			}
		}
		this._changed(9);
	},

	isEmpty: function() {
		return false;
	},

	toPath: function(insert) {
		var path = new Path[Base.capitalize(this._type)]({
			center: new Point(),
			size: this._size,
			radius: this._radius,
			insert: false
		});
		path.copyAttributes(this);
		if (paper.settings.applyMatrix)
			path.setApplyMatrix(true);
		if (insert === undefined || insert)
			path.insertAbove(this);
		return path;
	},

	toShape: '#clone',

	_asPathItem: function() {
		return this.toPath(false);
	},

	_draw: function(ctx, param, viewMatrix, strokeMatrix) {
		var style = this._style,
			hasFill = style.hasFill(),
			hasStroke = style.hasStroke(),
			dontPaint = param.dontFinish || param.clip,
			untransformed = !strokeMatrix;
		if (hasFill || hasStroke || dontPaint) {
			var type = this._type,
				radius = this._radius,
				isCircle = type === 'circle';
			if (!param.dontStart)
				ctx.beginPath();
			if (untransformed && isCircle) {
				ctx.arc(0, 0, radius, 0, Math.PI * 2, true);
			} else {
				var rx = isCircle ? radius : radius.width,
					ry = isCircle ? radius : radius.height,
					size = this._size,
					width = size.width,
					height = size.height;
				if (untransformed && type === 'rectangle' && rx === 0 && ry === 0) {
					ctx.rect(-width / 2, -height / 2, width, height);
				} else {
					var x = width / 2,
						y = height / 2,
						kappa = 1 - 0.5522847498307936,
						cx = rx * kappa,
						cy = ry * kappa,
						c = [
							-x, -y + ry,
							-x, -y + cy,
							-x + cx, -y,
							-x + rx, -y,
							x - rx, -y,
							x - cx, -y,
							x, -y + cy,
							x, -y + ry,
							x, y - ry,
							x, y - cy,
							x - cx, y,
							x - rx, y,
							-x + rx, y,
							-x + cx, y,
							-x, y - cy,
							-x, y - ry
						];
					if (strokeMatrix)
						strokeMatrix.transform(c, c, 32);
					ctx.moveTo(c[0], c[1]);
					ctx.bezierCurveTo(c[2], c[3], c[4], c[5], c[6], c[7]);
					if (x !== rx)
						ctx.lineTo(c[8], c[9]);
					ctx.bezierCurveTo(c[10], c[11], c[12], c[13], c[14], c[15]);
					if (y !== ry)
						ctx.lineTo(c[16], c[17]);
					ctx.bezierCurveTo(c[18], c[19], c[20], c[21], c[22], c[23]);
					if (x !== rx)
						ctx.lineTo(c[24], c[25]);
					ctx.bezierCurveTo(c[26], c[27], c[28], c[29], c[30], c[31]);
				}
			}
			ctx.closePath();
		}
		if (!dontPaint && (hasFill || hasStroke)) {
			this._setStyles(ctx, param, viewMatrix);
			if (hasFill) {
				ctx.fill(style.getFillRule());
				ctx.shadowColor = 'rgba(0,0,0,0)';
			}
			if (hasStroke)
				ctx.stroke();
		}
	},

	_canComposite: function() {
		return !(this.hasFill() && this.hasStroke());
	},

	_getBounds: function(matrix, options) {
		var rect = new Rectangle(this._size).setCenter(0, 0),
			style = this._style,
			strokeWidth = options.stroke && style.hasStroke()
					&& style.getStrokeWidth();
		if (matrix)
			rect = matrix._transformBounds(rect);
		return strokeWidth
				? rect.expand(Path._getStrokePadding(strokeWidth,
					this._getStrokeMatrix(matrix, options)))
				: rect;
	}
},
new function() {
	function getCornerCenter(that, point, expand) {
		var radius = that._radius;
		if (!radius.isZero()) {
			var halfSize = that._size.divide(2);
			for (var q = 1; q <= 4; q++) {
				var dir = new Point(q > 1 && q < 4 ? -1 : 1, q > 2 ? -1 : 1),
					corner = dir.multiply(halfSize),
					center = corner.subtract(dir.multiply(radius)),
					rect = new Rectangle(
							expand ? corner.add(dir.multiply(expand)) : corner,
							center);
				if (rect.contains(point))
					return { point: center, quadrant: q };
			}
		}
	}

	function isOnEllipseStroke(point, radius, padding, quadrant) {
		var vector = point.divide(radius);
		return (!quadrant || vector.isInQuadrant(quadrant)) &&
				vector.subtract(vector.normalize()).multiply(radius)
					.divide(padding).length <= 1;
	}

	return {
		_contains: function _contains(point) {
			if (this._type === 'rectangle') {
				var center = getCornerCenter(this, point);
				return center
						? point.subtract(center.point).divide(this._radius)
							.getLength() <= 1
						: _contains.base.call(this, point);
			} else {
				return point.divide(this.size).getLength() <= 0.5;
			}
		},

		_hitTestSelf: function _hitTestSelf(point, options, viewMatrix,
				strokeMatrix) {
			var hit = false,
				style = this._style,
				hitStroke = options.stroke && style.hasStroke(),
				hitFill = options.fill && style.hasFill();
			if (hitStroke || hitFill) {
				var type = this._type,
					radius = this._radius,
					strokeRadius = hitStroke ? style.getStrokeWidth() / 2 : 0,
					strokePadding = options._tolerancePadding.add(
						Path._getStrokePadding(strokeRadius,
							!style.getStrokeScaling() && strokeMatrix));
				if (type === 'rectangle') {
					var padding = strokePadding.multiply(2),
						center = getCornerCenter(this, point, padding);
					if (center) {
						hit = isOnEllipseStroke(point.subtract(center.point),
								radius, strokePadding, center.quadrant);
					} else {
						var rect = new Rectangle(this._size).setCenter(0, 0),
							outer = rect.expand(padding),
							inner = rect.expand(padding.negate());
						hit = outer._containsPoint(point)
								&& !inner._containsPoint(point);
					}
				} else {
					hit = isOnEllipseStroke(point, radius, strokePadding);
				}
			}
			return hit ? new HitResult(hitStroke ? 'stroke' : 'fill', this)
					: _hitTestSelf.base.apply(this, arguments);
		}
	};
}, {

statics: new function() {
	function createShape(type, point, size, radius, args) {
		var item = new Shape(Base.getNamed(args), point);
		item._type = type;
		item._size = size;
		item._radius = radius;
		return item;
	}

	return {
		Circle: function() {
			var center = Point.readNamed(arguments, 'center'),
				radius = Base.readNamed(arguments, 'radius');
			return createShape('circle', center, new Size(radius * 2), radius,
					arguments);
		},

		Rectangle: function() {
			var rect = Rectangle.readNamed(arguments, 'rectangle'),
				radius = Size.min(Size.readNamed(arguments, 'radius'),
						rect.getSize(true).divide(2));
			return createShape('rectangle', rect.getCenter(true),
					rect.getSize(true), radius, arguments);
		},

		Ellipse: function() {
			var ellipse = Shape._readEllipse(arguments),
				radius = ellipse.radius;
			return createShape('ellipse', ellipse.center, radius.multiply(2),
					radius, arguments);
		},

		_readEllipse: function(args) {
			var center,
				radius;
			if (Base.hasNamed(args, 'radius')) {
				center = Point.readNamed(args, 'center');
				radius = Size.readNamed(args, 'radius');
			} else {
				var rect = Rectangle.readNamed(args, 'rectangle');
				center = rect.getCenter(true);
				radius = rect.getSize(true).divide(2);
			}
			return { center: center, radius: radius };
		}
	};
}});

var Raster = Item.extend({
	_class: 'Raster',
	_applyMatrix: false,
	_canApplyMatrix: false,
	_boundsOptions: { stroke: false, handle: false },
	_serializeFields: {
		crossOrigin: null,
		source: null
	},
	_prioritize: ['crossOrigin'],
	_smoothing: true,

	initialize: function Raster(object, position) {
		if (!this._initialize(object,
				position !== undefined && Point.read(arguments, 1))) {
			var image = typeof object === 'string'
					? document.getElementById(object) : object;
			if (image) {
				this.setImage(image);
			} else {
				this.setSource(object);
			}
		}
		if (!this._size) {
			this._size = new Size();
			this._loaded = false;
		}
	},

	_equals: function(item) {
		return this.getSource() === item.getSource();
	},

	copyContent: function(source) {
		var image = source._image,
			canvas = source._canvas;
		if (image) {
			this._setImage(image);
		} else if (canvas) {
			var copyCanvas = CanvasProvider.getCanvas(source._size);
			copyCanvas.getContext('2d').drawImage(canvas, 0, 0);
			this._setImage(copyCanvas);
		}
		this._crossOrigin = source._crossOrigin;
	},

	getSize: function() {
		var size = this._size;
		return new LinkedSize(size ? size.width : 0, size ? size.height : 0,
				this, 'setSize');
	},

	setSize: function() {
		var size = Size.read(arguments);
		if (!size.equals(this._size)) {
			if (size.width > 0 && size.height > 0) {
				var element = this.getElement();
				this._setImage(CanvasProvider.getCanvas(size));
				if (element)
					this.getContext(true).drawImage(element, 0, 0,
							size.width, size.height);
			} else {
				if (this._canvas)
					CanvasProvider.release(this._canvas);
				this._size = size.clone();
			}
		}
	},

	getWidth: function() {
		return this._size ? this._size.width : 0;
	},

	setWidth: function(width) {
		this.setSize(width, this.getHeight());
	},

	getHeight: function() {
		return this._size ? this._size.height : 0;
	},

	setHeight: function(height) {
		this.setSize(this.getWidth(), height);
	},

	getLoaded: function() {
		return this._loaded;
	},

	isEmpty: function() {
		var size = this._size;
		return !size || size.width === 0 && size.height === 0;
	},

	getResolution: function() {
		var matrix = this._matrix,
			orig = new Point(0, 0).transform(matrix),
			u = new Point(1, 0).transform(matrix).subtract(orig),
			v = new Point(0, 1).transform(matrix).subtract(orig);
		return new Size(
			72 / u.getLength(),
			72 / v.getLength()
		);
	},

	getPpi: '#getResolution',

	getImage: function() {
		return this._image;
	},

	setImage: function(image) {
		var that = this;

		function emit(event) {
			var view = that.getView(),
				type = event && event.type || 'load';
			if (view && that.responds(type)) {
				paper = view._scope;
				that.emit(type, new Event(event));
			}
		}

		this._setImage(image);
		if (this._loaded) {
			setTimeout(emit, 0);
		} else if (image) {
			DomEvent.add(image, {
				load: function(event) {
					that._setImage(image);
					emit(event);
				},
				error: emit
			});
		}
	},

	_setImage: function(image) {
		if (this._canvas)
			CanvasProvider.release(this._canvas);
		if (image && image.getContext) {
			this._image = null;
			this._canvas = image;
			this._loaded = true;
		} else {
			this._image = image;
			this._canvas = null;
			this._loaded = !!(image && image.src && image.complete);
		}
		this._size = new Size(
				image ? image.naturalWidth || image.width : 0,
				image ? image.naturalHeight || image.height : 0);
		this._context = null;
		this._changed(1033);
	},

	getCanvas: function() {
		if (!this._canvas) {
			var ctx = CanvasProvider.getContext(this._size);
			try {
				if (this._image)
					ctx.drawImage(this._image, 0, 0);
				this._canvas = ctx.canvas;
			} catch (e) {
				CanvasProvider.release(ctx);
			}
		}
		return this._canvas;
	},

	setCanvas: '#setImage',

	getContext: function(modify) {
		if (!this._context)
			this._context = this.getCanvas().getContext('2d');
		if (modify) {
			this._image = null;
			this._changed(1025);
		}
		return this._context;
	},

	setContext: function(context) {
		this._context = context;
	},

	getSource: function() {
		var image = this._image;
		return image && image.src || this.toDataURL();
	},

	setSource: function(src) {
		var image = new self.Image(),
			crossOrigin = this._crossOrigin;
		if (crossOrigin)
			image.crossOrigin = crossOrigin;
		image.src = src;
		this.setImage(image);
	},

	getCrossOrigin: function() {
		var image = this._image;
		return image && image.crossOrigin || this._crossOrigin || '';
	},

	setCrossOrigin: function(crossOrigin) {
		this._crossOrigin = crossOrigin;
		var image = this._image;
		if (image)
			image.crossOrigin = crossOrigin;
	},

	getSmoothing: function() {
		return this._smoothing;
	},

	setSmoothing: function(smoothing) {
		this._smoothing = smoothing;
		this._changed(257);
	},

	getElement: function() {
		return this._canvas || this._loaded && this._image;
	}
}, {
	beans: false,

	getSubCanvas: function() {
		var rect = Rectangle.read(arguments),
			ctx = CanvasProvider.getContext(rect.getSize());
		ctx.drawImage(this.getCanvas(), rect.x, rect.y,
				rect.width, rect.height, 0, 0, rect.width, rect.height);
		return ctx.canvas;
	},

	getSubRaster: function() {
		var rect = Rectangle.read(arguments),
			raster = new Raster(Item.NO_INSERT);
		raster._setImage(this.getSubCanvas(rect));
		raster.translate(rect.getCenter().subtract(this.getSize().divide(2)));
		raster._matrix.prepend(this._matrix);
		raster.insertAbove(this);
		return raster;
	},

	toDataURL: function() {
		var image = this._image,
			src = image && image.src;
		if (/^data:/.test(src))
			return src;
		var canvas = this.getCanvas();
		return canvas ? canvas.toDataURL.apply(canvas, arguments) : null;
	},

	drawImage: function(image ) {
		var point = Point.read(arguments, 1);
		this.getContext(true).drawImage(image, point.x, point.y);
	},

	getAverageColor: function(object) {
		var bounds, path;
		if (!object) {
			bounds = this.getBounds();
		} else if (object instanceof PathItem) {
			path = object;
			bounds = object.getBounds();
		} else if (typeof object === 'object') {
			if ('width' in object) {
				bounds = new Rectangle(object);
			} else if ('x' in object) {
				bounds = new Rectangle(object.x - 0.5, object.y - 0.5, 1, 1);
			}
		}
		if (!bounds)
			return null;
		var sampleSize = 32,
			width = Math.min(bounds.width, sampleSize),
			height = Math.min(bounds.height, sampleSize);
		var ctx = Raster._sampleContext;
		if (!ctx) {
			ctx = Raster._sampleContext = CanvasProvider.getContext(
					new Size(sampleSize));
		} else {
			ctx.clearRect(0, 0, sampleSize + 1, sampleSize + 1);
		}
		ctx.save();
		var matrix = new Matrix()
				.scale(width / bounds.width, height / bounds.height)
				.translate(-bounds.x, -bounds.y);
		matrix.applyToContext(ctx);
		if (path)
			path.draw(ctx, new Base({ clip: true, matrices: [matrix] }));
		this._matrix.applyToContext(ctx);
		var element = this.getElement(),
			size = this._size;
		if (element)
			ctx.drawImage(element, -size.width / 2, -size.height / 2);
		ctx.restore();
		var pixels = ctx.getImageData(0.5, 0.5, Math.ceil(width),
				Math.ceil(height)).data,
			channels = [0, 0, 0],
			total = 0;
		for (var i = 0, l = pixels.length; i < l; i += 4) {
			var alpha = pixels[i + 3];
			total += alpha;
			alpha /= 255;
			channels[0] += pixels[i] * alpha;
			channels[1] += pixels[i + 1] * alpha;
			channels[2] += pixels[i + 2] * alpha;
		}
		for (var i = 0; i < 3; i++)
			channels[i] /= total;
		return total ? Color.read(channels) : null;
	},

	getPixel: function() {
		var point = Point.read(arguments);
		var data = this.getContext().getImageData(point.x, point.y, 1, 1).data;
		return new Color('rgb', [data[0] / 255, data[1] / 255, data[2] / 255],
				data[3] / 255);
	},

	setPixel: function() {
		var point = Point.read(arguments),
			color = Color.read(arguments),
			components = color._convert('rgb'),
			alpha = color._alpha,
			ctx = this.getContext(true),
			imageData = ctx.createImageData(1, 1),
			data = imageData.data;
		data[0] = components[0] * 255;
		data[1] = components[1] * 255;
		data[2] = components[2] * 255;
		data[3] = alpha != null ? alpha * 255 : 255;
		ctx.putImageData(imageData, point.x, point.y);
	},

	createImageData: function() {
		var size = Size.read(arguments);
		return this.getContext().createImageData(size.width, size.height);
	},

	getImageData: function() {
		var rect = Rectangle.read(arguments);
		if (rect.isEmpty())
			rect = new Rectangle(this._size);
		return this.getContext().getImageData(rect.x, rect.y,
				rect.width, rect.height);
	},

	setImageData: function(data ) {
		var point = Point.read(arguments, 1);
		this.getContext(true).putImageData(data, point.x, point.y);
	},

	_getBounds: function(matrix, options) {
		var rect = new Rectangle(this._size).setCenter(0, 0);
		return matrix ? matrix._transformBounds(rect) : rect;
	},

	_hitTestSelf: function(point) {
		if (this._contains(point)) {
			var that = this;
			return new HitResult('pixel', that, {
				offset: point.add(that._size.divide(2)).round(),
				color: {
					get: function() {
						return that.getPixel(this.offset);
					}
				}
			});
		}
	},

	_draw: function(ctx, param, viewMatrix) {
		var element = this.getElement();
		if (element && element.width > 0 && element.height > 0) {
			ctx.globalAlpha = this._opacity;

			this._setStyles(ctx, param, viewMatrix);

			DomElement.setPrefixed(
				ctx, 'imageSmoothingEnabled', this._smoothing
			);

			ctx.drawImage(element,
					-this._size.width / 2, -this._size.height / 2);
		}
	},

	_canComposite: function() {
		return true;
	}
});

var SymbolItem = Item.extend({
	_class: 'SymbolItem',
	_applyMatrix: false,
	_canApplyMatrix: false,
	_boundsOptions: { stroke: true },
	_serializeFields: {
		symbol: null
	},

	initialize: function SymbolItem(arg0, arg1) {
		if (!this._initialize(arg0,
				arg1 !== undefined && Point.read(arguments, 1)))
			this.setDefinition(arg0 instanceof SymbolDefinition ?
					arg0 : new SymbolDefinition(arg0));
	},

	_equals: function(item) {
		return this._definition === item._definition;
	},

	copyContent: function(source) {
		this.setDefinition(source._definition);
	},

	getDefinition: function() {
		return this._definition;
	},

	setDefinition: function(definition) {
		this._definition = definition;
		this._changed(9);
	},

	getSymbol: '#getDefinition',
	setSymbol: '#setDefinition',

	isEmpty: function() {
		return this._definition._item.isEmpty();
	},

	_getBounds: function(matrix, options) {
		var item = this._definition._item;
		return item._getCachedBounds(item._matrix.prepended(matrix), options);
	},

	_hitTestSelf: function(point, options, viewMatrix) {
		var res = this._definition._item._hitTest(point, options, viewMatrix);
		if (res)
			res.item = this;
		return res;
	},

	_draw: function(ctx, param) {
		this._definition._item.draw(ctx, param);
	}

});

var SymbolDefinition = Base.extend({
	_class: 'SymbolDefinition',

	initialize: function SymbolDefinition(item, dontCenter) {
		this._id = UID.get();
		this.project = paper.project;
		if (item)
			this.setItem(item, dontCenter);
	},

	_serialize: function(options, dictionary) {
		return dictionary.add(this, function() {
			return Base.serialize([this._class, this._item],
					options, false, dictionary);
		});
	},

	_changed: function(flags) {
		if (flags & 8)
			Item._clearBoundsCache(this);
		if (flags & 1)
			this.project._changed(flags);
	},

	getItem: function() {
		return this._item;
	},

	setItem: function(item, _dontCenter) {
		if (item._symbol)
			item = item.clone();
		if (this._item)
			this._item._symbol = null;
		this._item = item;
		item.remove();
		item.setSelected(false);
		if (!_dontCenter)
			item.setPosition(new Point());
		item._symbol = this;
		this._changed(9);
	},

	getDefinition: '#getItem',
	setDefinition: '#setItem',

	place: function(position) {
		return new SymbolItem(this, position);
	},

	clone: function() {
		return new SymbolDefinition(this._item.clone(false));
	},

	equals: function(symbol) {
		return symbol === this
				|| symbol && this._item.equals(symbol._item)
				|| false;
	}
});

var HitResult = Base.extend({
	_class: 'HitResult',

	initialize: function HitResult(type, item, values) {
		this.type = type;
		this.item = item;
		if (values)
			this.inject(values);
	},

	statics: {
		getOptions: function(args) {
			var options = args && Base.read(args);
			return Base.set({
				type: null,
				tolerance: paper.settings.hitTolerance,
				fill: !options,
				stroke: !options,
				segments: !options,
				handles: false,
				ends: false,
				position: false,
				center: false,
				bounds: false,
				guides: false,
				selected: false
			}, options);
		}
	}
});

var Segment = Base.extend({
	_class: 'Segment',
	beans: true,
	_selection: 0,

	initialize: function Segment(arg0, arg1, arg2, arg3, arg4, arg5) {
		var count = arguments.length,
			point, handleIn, handleOut, selection;
		if (count > 0) {
			if (arg0 == null || typeof arg0 === 'object') {
				if (count === 1 && arg0 && 'point' in arg0) {
					point = arg0.point;
					handleIn = arg0.handleIn;
					handleOut = arg0.handleOut;
					selection = arg0.selection;
				} else {
					point = arg0;
					handleIn = arg1;
					handleOut = arg2;
					selection = arg3;
				}
			} else {
				point = [ arg0, arg1 ];
				handleIn = arg2 !== undefined ? [ arg2, arg3 ] : null;
				handleOut = arg4 !== undefined ? [ arg4, arg5 ] : null;
			}
		}
		new SegmentPoint(point, this, '_point');
		new SegmentPoint(handleIn, this, '_handleIn');
		new SegmentPoint(handleOut, this, '_handleOut');
		if (selection)
			this.setSelection(selection);
	},

	_serialize: function(options, dictionary) {
		var point = this._point,
			selection = this._selection,
			obj = selection || this.hasHandles()
					? [point, this._handleIn, this._handleOut]
					: point;
		if (selection)
			obj.push(selection);
		return Base.serialize(obj, options, true, dictionary);
	},

	_changed: function(point) {
		var path = this._path;
		if (!path)
			return;
		var curves = path._curves,
			index = this._index,
			curve;
		if (curves) {
			if ((!point || point === this._point || point === this._handleIn)
					&& (curve = index > 0 ? curves[index - 1] : path._closed
						? curves[curves.length - 1] : null))
				curve._changed();
			if ((!point || point === this._point || point === this._handleOut)
					&& (curve = curves[index]))
				curve._changed();
		}
		path._changed(41);
	},

	getPoint: function() {
		return this._point;
	},

	setPoint: function() {
		this._point.set(Point.read(arguments));
	},

	getHandleIn: function() {
		return this._handleIn;
	},

	setHandleIn: function() {
		this._handleIn.set(Point.read(arguments));
	},

	getHandleOut: function() {
		return this._handleOut;
	},

	setHandleOut: function() {
		this._handleOut.set(Point.read(arguments));
	},

	hasHandles: function() {
		return !this._handleIn.isZero() || !this._handleOut.isZero();
	},

	isSmooth: function() {
		var handleIn = this._handleIn,
			handleOut = this._handleOut;
		return !handleIn.isZero() && !handleOut.isZero()
				&& handleIn.isCollinear(handleOut);
	},

	clearHandles: function() {
		this._handleIn._set(0, 0);
		this._handleOut._set(0, 0);
	},

	getSelection: function() {
		return this._selection;
	},

	setSelection: function(selection) {
		var oldSelection = this._selection,
			path = this._path;
		this._selection = selection = selection || 0;
		if (path && selection !== oldSelection) {
			path._updateSelection(this, oldSelection, selection);
			path._changed(257);
		}
	},

	_changeSelection: function(flag, selected) {
		var selection = this._selection;
		this.setSelection(selected ? selection | flag : selection & ~flag);
	},

	isSelected: function() {
		return !!(this._selection & 7);
	},

	setSelected: function(selected) {
		this._changeSelection(7, selected);
	},

	getIndex: function() {
		return this._index !== undefined ? this._index : null;
	},

	getPath: function() {
		return this._path || null;
	},

	getCurve: function() {
		var path = this._path,
			index = this._index;
		if (path) {
			if (index > 0 && !path._closed
					&& index === path._segments.length - 1)
				index--;
			return path.getCurves()[index] || null;
		}
		return null;
	},

	getLocation: function() {
		var curve = this.getCurve();
		return curve
				? new CurveLocation(curve, this === curve._segment1 ? 0 : 1)
				: null;
	},

	getNext: function() {
		var segments = this._path && this._path._segments;
		return segments && (segments[this._index + 1]
				|| this._path._closed && segments[0]) || null;
	},

	smooth: function(options, _first, _last) {
		var opts = options || {},
			type = opts.type,
			factor = opts.factor,
			prev = this.getPrevious(),
			next = this.getNext(),
			p0 = (prev || this)._point,
			p1 = this._point,
			p2 = (next || this)._point,
			d1 = p0.getDistance(p1),
			d2 = p1.getDistance(p2);
		if (!type || type === 'catmull-rom') {
			var a = factor === undefined ? 0.5 : factor,
				d1_a = Math.pow(d1, a),
				d1_2a = d1_a * d1_a,
				d2_a = Math.pow(d2, a),
				d2_2a = d2_a * d2_a;
			if (!_first && prev) {
				var A = 2 * d2_2a + 3 * d2_a * d1_a + d1_2a,
					N = 3 * d2_a * (d2_a + d1_a);
				this.setHandleIn(N !== 0
					? new Point(
						(d2_2a * p0._x + A * p1._x - d1_2a * p2._x) / N - p1._x,
						(d2_2a * p0._y + A * p1._y - d1_2a * p2._y) / N - p1._y)
					: new Point());
			}
			if (!_last && next) {
				var A = 2 * d1_2a + 3 * d1_a * d2_a + d2_2a,
					N = 3 * d1_a * (d1_a + d2_a);
				this.setHandleOut(N !== 0
					? new Point(
						(d1_2a * p2._x + A * p1._x - d2_2a * p0._x) / N - p1._x,
						(d1_2a * p2._y + A * p1._y - d2_2a * p0._y) / N - p1._y)
					: new Point());
			}
		} else if (type === 'geometric') {
			if (prev && next) {
				var vector = p0.subtract(p2),
					t = factor === undefined ? 0.4 : factor,
					k = t * d1 / (d1 + d2);
				if (!_first)
					this.setHandleIn(vector.multiply(k));
				if (!_last)
					this.setHandleOut(vector.multiply(k - t));
			}
		} else {
			throw new Error('Smoothing method \'' + type + '\' not supported.');
		}
	},

	getPrevious: function() {
		var segments = this._path && this._path._segments;
		return segments && (segments[this._index - 1]
				|| this._path._closed && segments[segments.length - 1]) || null;
	},

	isFirst: function() {
		return !this._index;
	},

	isLast: function() {
		var path = this._path;
		return path && this._index === path._segments.length - 1 || false;
	},

	reverse: function() {
		var handleIn = this._handleIn,
			handleOut = this._handleOut,
			tmp = handleIn.clone();
		handleIn.set(handleOut);
		handleOut.set(tmp);
	},

	reversed: function() {
		return new Segment(this._point, this._handleOut, this._handleIn);
	},

	remove: function() {
		return this._path ? !!this._path.removeSegment(this._index) : false;
	},

	clone: function() {
		return new Segment(this._point, this._handleIn, this._handleOut);
	},

	equals: function(segment) {
		return segment === this || segment && this._class === segment._class
				&& this._point.equals(segment._point)
				&& this._handleIn.equals(segment._handleIn)
				&& this._handleOut.equals(segment._handleOut)
				|| false;
	},

	toString: function() {
		var parts = [ 'point: ' + this._point ];
		if (!this._handleIn.isZero())
			parts.push('handleIn: ' + this._handleIn);
		if (!this._handleOut.isZero())
			parts.push('handleOut: ' + this._handleOut);
		return '{ ' + parts.join(', ') + ' }';
	},

	transform: function(matrix) {
		this._transformCoordinates(matrix, new Array(6), true);
		this._changed();
	},

	interpolate: function(from, to, factor) {
		var u = 1 - factor,
			v = factor,
			point1 = from._point,
			point2 = to._point,
			handleIn1 = from._handleIn,
			handleIn2 = to._handleIn,
			handleOut2 = to._handleOut,
			handleOut1 = from._handleOut;
		this._point._set(
				u * point1._x + v * point2._x,
				u * point1._y + v * point2._y, true);
		this._handleIn._set(
				u * handleIn1._x + v * handleIn2._x,
				u * handleIn1._y + v * handleIn2._y, true);
		this._handleOut._set(
				u * handleOut1._x + v * handleOut2._x,
				u * handleOut1._y + v * handleOut2._y, true);
		this._changed();
	},

	_transformCoordinates: function(matrix, coords, change) {
		var point = this._point,
			handleIn = !change || !this._handleIn.isZero()
					? this._handleIn : null,
			handleOut = !change || !this._handleOut.isZero()
					? this._handleOut : null,
			x = point._x,
			y = point._y,
			i = 2;
		coords[0] = x;
		coords[1] = y;
		if (handleIn) {
			coords[i++] = handleIn._x + x;
			coords[i++] = handleIn._y + y;
		}
		if (handleOut) {
			coords[i++] = handleOut._x + x;
			coords[i++] = handleOut._y + y;
		}
		if (matrix) {
			matrix._transformCoordinates(coords, coords, i / 2);
			x = coords[0];
			y = coords[1];
			if (change) {
				point._x = x;
				point._y = y;
				i = 2;
				if (handleIn) {
					handleIn._x = coords[i++] - x;
					handleIn._y = coords[i++] - y;
				}
				if (handleOut) {
					handleOut._x = coords[i++] - x;
					handleOut._y = coords[i++] - y;
				}
			} else {
				if (!handleIn) {
					coords[i++] = x;
					coords[i++] = y;
				}
				if (!handleOut) {
					coords[i++] = x;
					coords[i++] = y;
				}
			}
		}
		return coords;
	}
});

var SegmentPoint = Point.extend({
	initialize: function SegmentPoint(point, owner, key) {
		var x, y,
			selected;
		if (!point) {
			x = y = 0;
		} else if ((x = point[0]) !== undefined) {
			y = point[1];
		} else {
			var pt = point;
			if ((x = pt.x) === undefined) {
				pt = Point.read(arguments);
				x = pt.x;
			}
			y = pt.y;
			selected = pt.selected;
		}
		this._x = x;
		this._y = y;
		this._owner = owner;
		owner[key] = this;
		if (selected)
			this.setSelected(true);
	},

	_set: function(x, y) {
		this._x = x;
		this._y = y;
		this._owner._changed(this);
		return this;
	},

	getX: function() {
		return this._x;
	},

	setX: function(x) {
		this._x = x;
		this._owner._changed(this);
	},

	getY: function() {
		return this._y;
	},

	setY: function(y) {
		this._y = y;
		this._owner._changed(this);
	},

	isZero: function() {
		var isZero = Numerical.isZero;
		return isZero(this._x) && isZero(this._y);
	},

	isSelected: function() {
		return !!(this._owner._selection & this._getSelection());
	},

	setSelected: function(selected) {
		this._owner._changeSelection(this._getSelection(), selected);
	},

	_getSelection: function() {
		var owner = this._owner;
		return this === owner._point ? 1
			: this === owner._handleIn ? 2
			: this === owner._handleOut ? 4
			: 0;
	}
});

var Curve = Base.extend({
	_class: 'Curve',
	beans: true,

	initialize: function Curve(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7) {
		var count = arguments.length,
			seg1, seg2,
			point1, point2,
			handle1, handle2;
		if (count === 3) {
			this._path = arg0;
			seg1 = arg1;
			seg2 = arg2;
		} else if (!count) {
			seg1 = new Segment();
			seg2 = new Segment();
		} else if (count === 1) {
			if ('segment1' in arg0) {
				seg1 = new Segment(arg0.segment1);
				seg2 = new Segment(arg0.segment2);
			} else if ('point1' in arg0) {
				point1 = arg0.point1;
				handle1 = arg0.handle1;
				handle2 = arg0.handle2;
				point2 = arg0.point2;
			} else if (Array.isArray(arg0)) {
				point1 = [arg0[0], arg0[1]];
				point2 = [arg0[6], arg0[7]];
				handle1 = [arg0[2] - arg0[0], arg0[3] - arg0[1]];
				handle2 = [arg0[4] - arg0[6], arg0[5] - arg0[7]];
			}
		} else if (count === 2) {
			seg1 = new Segment(arg0);
			seg2 = new Segment(arg1);
		} else if (count === 4) {
			point1 = arg0;
			handle1 = arg1;
			handle2 = arg2;
			point2 = arg3;
		} else if (count === 8) {
			point1 = [arg0, arg1];
			point2 = [arg6, arg7];
			handle1 = [arg2 - arg0, arg3 - arg1];
			handle2 = [arg4 - arg6, arg5 - arg7];
		}
		this._segment1 = seg1 || new Segment(point1, null, handle1);
		this._segment2 = seg2 || new Segment(point2, handle2, null);
	},

	_serialize: function(options, dictionary) {
		return Base.serialize(this.hasHandles()
				? [this.getPoint1(), this.getHandle1(), this.getHandle2(),
					this.getPoint2()]
				: [this.getPoint1(), this.getPoint2()],
				options, true, dictionary);
	},

	_changed: function() {
		this._length = this._bounds = undefined;
	},

	clone: function() {
		return new Curve(this._segment1, this._segment2);
	},

	toString: function() {
		var parts = [ 'point1: ' + this._segment1._point ];
		if (!this._segment1._handleOut.isZero())
			parts.push('handle1: ' + this._segment1._handleOut);
		if (!this._segment2._handleIn.isZero())
			parts.push('handle2: ' + this._segment2._handleIn);
		parts.push('point2: ' + this._segment2._point);
		return '{ ' + parts.join(', ') + ' }';
	},

	classify: function() {
		return Curve.classify(this.getValues());
	},

	remove: function() {
		var removed = false;
		if (this._path) {
			var segment2 = this._segment2,
				handleOut = segment2._handleOut;
			removed = segment2.remove();
			if (removed)
				this._segment1._handleOut.set(handleOut);
		}
		return removed;
	},

	getPoint1: function() {
		return this._segment1._point;
	},

	setPoint1: function() {
		this._segment1._point.set(Point.read(arguments));
	},

	getPoint2: function() {
		return this._segment2._point;
	},

	setPoint2: function() {
		this._segment2._point.set(Point.read(arguments));
	},

	getHandle1: function() {
		return this._segment1._handleOut;
	},

	setHandle1: function() {
		this._segment1._handleOut.set(Point.read(arguments));
	},

	getHandle2: function() {
		return this._segment2._handleIn;
	},

	setHandle2: function() {
		this._segment2._handleIn.set(Point.read(arguments));
	},

	getSegment1: function() {
		return this._segment1;
	},

	getSegment2: function() {
		return this._segment2;
	},

	getPath: function() {
		return this._path;
	},

	getIndex: function() {
		return this._segment1._index;
	},

	getNext: function() {
		var curves = this._path && this._path._curves;
		return curves && (curves[this._segment1._index + 1]
				|| this._path._closed && curves[0]) || null;
	},

	getPrevious: function() {
		var curves = this._path && this._path._curves;
		return curves && (curves[this._segment1._index - 1]
				|| this._path._closed && curves[curves.length - 1]) || null;
	},

	isFirst: function() {
		return !this._segment1._index;
	},

	isLast: function() {
		var path = this._path;
		return path && this._segment1._index === path._curves.length - 1
				|| false;
	},

	isSelected: function() {
		return this.getPoint1().isSelected()
				&& this.getHandle1().isSelected()
				&& this.getHandle2().isSelected()
				&& this.getPoint2().isSelected();
	},

	setSelected: function(selected) {
		this.getPoint1().setSelected(selected);
		this.getHandle1().setSelected(selected);
		this.getHandle2().setSelected(selected);
		this.getPoint2().setSelected(selected);
	},

	getValues: function(matrix) {
		return Curve.getValues(this._segment1, this._segment2, matrix);
	},

	getPoints: function() {
		var coords = this.getValues(),
			points = [];
		for (var i = 0; i < 8; i += 2)
			points.push(new Point(coords[i], coords[i + 1]));
		return points;
	}
}, {
	getLength: function() {
		if (this._length == null)
			this._length = Curve.getLength(this.getValues(), 0, 1);
		return this._length;
	},

	getArea: function() {
		return Curve.getArea(this.getValues());
	},

	getLine: function() {
		return new Line(this._segment1._point, this._segment2._point);
	},

	getPart: function(from, to) {
		return new Curve(Curve.getPart(this.getValues(), from, to));
	},

	getPartLength: function(from, to) {
		return Curve.getLength(this.getValues(), from, to);
	},

	divideAt: function(location) {
		return this.divideAtTime(location && location.curve === this
				? location.time : this.getTimeAt(location));
	},

	divideAtTime: function(time, _setHandles) {
		var tMin = 1e-8,
			tMax = 1 - tMin,
			res = null;
		if (time >= tMin && time <= tMax) {
			var parts = Curve.subdivide(this.getValues(), time),
				left = parts[0],
				right = parts[1],
				setHandles = _setHandles || this.hasHandles(),
				seg1 = this._segment1,
				seg2 = this._segment2,
				path = this._path;
			if (setHandles) {
				seg1._handleOut._set(left[2] - left[0], left[3] - left[1]);
				seg2._handleIn._set(right[4] - right[6],right[5] - right[7]);
			}
			var x = left[6], y = left[7],
				segment = new Segment(new Point(x, y),
						setHandles && new Point(left[4] - x, left[5] - y),
						setHandles && new Point(right[2] - x, right[3] - y));
			if (path) {
				path.insert(seg1._index + 1, segment);
				res = this.getNext();
			} else {
				this._segment2 = segment;
				this._changed();
				res = new Curve(segment, seg2);
			}
		}
		return res;
	},

	splitAt: function(location) {
		var path = this._path;
		return path ? path.splitAt(location) : null;
	},

	splitAtTime: function(time) {
		return this.splitAt(this.getLocationAtTime(time));
	},

	divide: function(offset, isTime) {
		return this.divideAtTime(offset === undefined ? 0.5 : isTime ? offset
				: this.getTimeAt(offset));
	},

	split: function(offset, isTime) {
		return this.splitAtTime(offset === undefined ? 0.5 : isTime ? offset
				: this.getTimeAt(offset));
	},

	reversed: function() {
		return new Curve(this._segment2.reversed(), this._segment1.reversed());
	},

	clearHandles: function() {
		this._segment1._handleOut._set(0, 0);
		this._segment2._handleIn._set(0, 0);
	},

statics: {
	getValues: function(segment1, segment2, matrix, straight) {
		var p1 = segment1._point,
			h1 = segment1._handleOut,
			h2 = segment2._handleIn,
			p2 = segment2._point,
			x1 = p1.x, y1 = p1.y,
			x2 = p2.x, y2 = p2.y,
			values = straight
				? [ x1, y1, x1, y1, x2, y2, x2, y2 ]
				: [
					x1, y1,
					x1 + h1._x, y1 + h1._y,
					x2 + h2._x, y2 + h2._y,
					x2, y2
				];
		if (matrix)
			matrix._transformCoordinates(values, values, 4);
		return values;
	},

	subdivide: function(v, t) {
		var x0 = v[0], y0 = v[1],
			x1 = v[2], y1 = v[3],
			x2 = v[4], y2 = v[5],
			x3 = v[6], y3 = v[7];
		if (t === undefined)
			t = 0.5;
		var u = 1 - t,
			x4 = u * x0 + t * x1, y4 = u * y0 + t * y1,
			x5 = u * x1 + t * x2, y5 = u * y1 + t * y2,
			x6 = u * x2 + t * x3, y6 = u * y2 + t * y3,
			x7 = u * x4 + t * x5, y7 = u * y4 + t * y5,
			x8 = u * x5 + t * x6, y8 = u * y5 + t * y6,
			x9 = u * x7 + t * x8, y9 = u * y7 + t * y8;
		return [
			[x0, y0, x4, y4, x7, y7, x9, y9],
			[x9, y9, x8, y8, x6, y6, x3, y3]
		];
	},

	getMonoCurves: function(v, dir) {
		var curves = [],
			io = dir ? 0 : 1,
			o0 = v[io + 0],
			o1 = v[io + 2],
			o2 = v[io + 4],
			o3 = v[io + 6];
		if ((o0 >= o1) === (o1 >= o2) && (o1 >= o2) === (o2 >= o3)
				|| Curve.isStraight(v)) {
			curves.push(v);
		} else {
			var a = 3 * (o1 - o2) - o0 + o3,
				b = 2 * (o0 + o2) - 4 * o1,
				c = o1 - o0,
				tMin = 1e-8,
				tMax = 1 - tMin,
				roots = [],
				n = Numerical.solveQuadratic(a, b, c, roots, tMin, tMax);
			if (!n) {
				curves.push(v);
			} else {
				roots.sort();
				var t = roots[0],
					parts = Curve.subdivide(v, t);
				curves.push(parts[0]);
				if (n > 1) {
					t = (roots[1] - t) / (1 - t);
					parts = Curve.subdivide(parts[1], t);
					curves.push(parts[0]);
				}
				curves.push(parts[1]);
			}
		}
		return curves;
	},

	solveCubic: function (v, coord, val, roots, min, max) {
		var v0 = v[coord],
			v1 = v[coord + 2],
			v2 = v[coord + 4],
			v3 = v[coord + 6],
			res = 0;
		if (  !(v0 < val && v3 < val && v1 < val && v2 < val ||
				v0 > val && v3 > val && v1 > val && v2 > val)) {
			var c = 3 * (v1 - v0),
				b = 3 * (v2 - v1) - c,
				a = v3 - v0 - c - b;
			res = Numerical.solveCubic(a, b, c, v0 - val, roots, min, max);
		}
		return res;
	},

	getTimeOf: function(v, point) {
		var p0 = new Point(v[0], v[1]),
			p3 = new Point(v[6], v[7]),
			epsilon = 1e-12,
			geomEpsilon = 1e-7,
			t = point.isClose(p0, epsilon) ? 0
			  : point.isClose(p3, epsilon) ? 1
			  : null;
		if (t === null) {
			var coords = [point.x, point.y],
				roots = [];
			for (var c = 0; c < 2; c++) {
				var count = Curve.solveCubic(v, c, coords[c], roots, 0, 1);
				for (var i = 0; i < count; i++) {
					var u = roots[i];
					if (point.isClose(Curve.getPoint(v, u), geomEpsilon))
						return u;
				}
			}
		}
		return point.isClose(p0, geomEpsilon) ? 0
			 : point.isClose(p3, geomEpsilon) ? 1
			 : null;
	},

	getNearestTime: function(v, point) {
		if (Curve.isStraight(v)) {
			var x0 = v[0], y0 = v[1],
				x3 = v[6], y3 = v[7],
				vx = x3 - x0, vy = y3 - y0,
				det = vx * vx + vy * vy;
			if (det === 0)
				return 0;
			var u = ((point.x - x0) * vx + (point.y - y0) * vy) / det;
			return u < 1e-12 ? 0
				 : u > 0.999999999999 ? 1
				 : Curve.getTimeOf(v,
					new Point(x0 + u * vx, y0 + u * vy));
		}

		var count = 100,
			minDist = Infinity,
			minT = 0;

		function refine(t) {
			if (t >= 0 && t <= 1) {
				var dist = point.getDistance(Curve.getPoint(v, t), true);
				if (dist < minDist) {
					minDist = dist;
					minT = t;
					return true;
				}
			}
		}

		for (var i = 0; i <= count; i++)
			refine(i / count);

		var step = 1 / (count * 2);
		while (step > 1e-8) {
			if (!refine(minT - step) && !refine(minT + step))
				step /= 2;
		}
		return minT;
	},

	getPart: function(v, from, to) {
		var flip = from > to;
		if (flip) {
			var tmp = from;
			from = to;
			to = tmp;
		}
		if (from > 0)
			v = Curve.subdivide(v, from)[1];
		if (to < 1)
			v = Curve.subdivide(v, (to - from) / (1 - from))[0];
		return flip
				? [v[6], v[7], v[4], v[5], v[2], v[3], v[0], v[1]]
				: v;
	},

	isFlatEnough: function(v, flatness) {
		var x0 = v[0], y0 = v[1],
			x1 = v[2], y1 = v[3],
			x2 = v[4], y2 = v[5],
			x3 = v[6], y3 = v[7],
			ux = 3 * x1 - 2 * x0 - x3,
			uy = 3 * y1 - 2 * y0 - y3,
			vx = 3 * x2 - 2 * x3 - x0,
			vy = 3 * y2 - 2 * y3 - y0;
		return Math.max(ux * ux, vx * vx) + Math.max(uy * uy, vy * vy)
				<= 16 * flatness * flatness;
	},

	getArea: function(v) {
		var x0 = v[0], y0 = v[1],
			x1 = v[2], y1 = v[3],
			x2 = v[4], y2 = v[5],
			x3 = v[6], y3 = v[7];
		return 3 * ((y3 - y0) * (x1 + x2) - (x3 - x0) * (y1 + y2)
				+ y1 * (x0 - x2) - x1 * (y0 - y2)
				+ y3 * (x2 + x0 / 3) - x3 * (y2 + y0 / 3)) / 20;
	},

	getBounds: function(v) {
		var min = v.slice(0, 2),
			max = min.slice(),
			roots = [0, 0];
		for (var i = 0; i < 2; i++)
			Curve._addBounds(v[i], v[i + 2], v[i + 4], v[i + 6],
					i, 0, min, max, roots);
		return new Rectangle(min[0], min[1], max[0] - min[0], max[1] - min[1]);
	},

	_addBounds: function(v0, v1, v2, v3, coord, padding, min, max, roots) {
		function add(value, padding) {
			var left = value - padding,
				right = value + padding;
			if (left < min[coord])
				min[coord] = left;
			if (right > max[coord])
				max[coord] = right;
		}

		padding /= 2;
		var minPad = min[coord] - padding,
			maxPad = max[coord] + padding;
		if (    v0 < minPad || v1 < minPad || v2 < minPad || v3 < minPad ||
				v0 > maxPad || v1 > maxPad || v2 > maxPad || v3 > maxPad) {
			if (v1 < v0 != v1 < v3 && v2 < v0 != v2 < v3) {
				add(v0, padding);
				add(v3, padding);
			} else {
				var a = 3 * (v1 - v2) - v0 + v3,
					b = 2 * (v0 + v2) - 4 * v1,
					c = v1 - v0,
					count = Numerical.solveQuadratic(a, b, c, roots),
					tMin = 1e-8,
					tMax = 1 - tMin;
				add(v3, 0);
				for (var i = 0; i < count; i++) {
					var t = roots[i],
						u = 1 - t;
					if (tMin <= t && t <= tMax)
						add(u * u * u * v0
							+ 3 * u * u * t * v1
							+ 3 * u * t * t * v2
							+ t * t * t * v3,
							padding);
				}
			}
		}
	}
}}, Base.each(
	['getBounds', 'getStrokeBounds', 'getHandleBounds'],
	function(name) {
		this[name] = function() {
			if (!this._bounds)
				this._bounds = {};
			var bounds = this._bounds[name];
			if (!bounds) {
				bounds = this._bounds[name] = Path[name](
						[this._segment1, this._segment2], false, this._path);
			}
			return bounds.clone();
		};
	},
{

}), Base.each({
	isStraight: function(p1, h1, h2, p2) {
		if (h1.isZero() && h2.isZero()) {
			return true;
		} else {
			var v = p2.subtract(p1);
			if (v.isZero()) {
				return false;
			} else if (v.isCollinear(h1) && v.isCollinear(h2)) {
				var l = new Line(p1, p2),
					epsilon = 1e-7;
				if (l.getDistance(p1.add(h1)) < epsilon &&
					l.getDistance(p2.add(h2)) < epsilon) {
					var div = v.dot(v),
						s1 = v.dot(h1) / div,
						s2 = v.dot(h2) / div;
					return s1 >= 0 && s1 <= 1 && s2 <= 0 && s2 >= -1;
				}
			}
		}
		return false;
	},

	isLinear: function(p1, h1, h2, p2) {
		var third = p2.subtract(p1).divide(3);
		return h1.equals(third) && h2.negate().equals(third);
	}
}, function(test, name) {
	this[name] = function(epsilon) {
		var seg1 = this._segment1,
			seg2 = this._segment2;
		return test(seg1._point, seg1._handleOut, seg2._handleIn, seg2._point,
				epsilon);
	};

	this.statics[name] = function(v, epsilon) {
		var x0 = v[0], y0 = v[1],
			x3 = v[6], y3 = v[7];
		return test(
				new Point(x0, y0),
				new Point(v[2] - x0, v[3] - y0),
				new Point(v[4] - x3, v[5] - y3),
				new Point(x3, y3), epsilon);
	};
}, {
	statics: {},

	hasHandles: function() {
		return !this._segment1._handleOut.isZero()
				|| !this._segment2._handleIn.isZero();
	},

	hasLength: function(epsilon) {
		return (!this.getPoint1().equals(this.getPoint2()) || this.hasHandles())
				&& this.getLength() > (epsilon || 0);
	},

	isCollinear: function(curve) {
		return curve && this.isStraight() && curve.isStraight()
				&& this.getLine().isCollinear(curve.getLine());
	},

	isHorizontal: function() {
		return this.isStraight() && Math.abs(this.getTangentAtTime(0.5).y)
				< 1e-8;
	},

	isVertical: function() {
		return this.isStraight() && Math.abs(this.getTangentAtTime(0.5).x)
				< 1e-8;
	}
}), {
	beans: false,

	getLocationAt: function(offset, _isTime) {
		return this.getLocationAtTime(
				_isTime ? offset : this.getTimeAt(offset));
	},

	getLocationAtTime: function(t) {
		return t != null && t >= 0 && t <= 1
				? new CurveLocation(this, t)
				: null;
	},

	getTimeAt: function(offset, start) {
		return Curve.getTimeAt(this.getValues(), offset, start);
	},

	getParameterAt: '#getTimeAt',

	getTimesWithTangent: function () {
		var tangent = Point.read(arguments);
		return tangent.isZero()
				? []
				: Curve.getTimesWithTangent(this.getValues(), tangent);
	},

	getOffsetAtTime: function(t) {
		return this.getPartLength(0, t);
	},

	getLocationOf: function() {
		return this.getLocationAtTime(this.getTimeOf(Point.read(arguments)));
	},

	getOffsetOf: function() {
		var loc = this.getLocationOf.apply(this, arguments);
		return loc ? loc.getOffset() : null;
	},

	getTimeOf: function() {
		return Curve.getTimeOf(this.getValues(), Point.read(arguments));
	},

	getParameterOf: '#getTimeOf',

	getNearestLocation: function() {
		var point = Point.read(arguments),
			values = this.getValues(),
			t = Curve.getNearestTime(values, point),
			pt = Curve.getPoint(values, t);
		return new CurveLocation(this, t, pt, null, point.getDistance(pt));
	},

	getNearestPoint: function() {
		var loc = this.getNearestLocation.apply(this, arguments);
		return loc ? loc.getPoint() : loc;
	}

},
new function() {
	var methods = ['getPoint', 'getTangent', 'getNormal', 'getWeightedTangent',
		'getWeightedNormal', 'getCurvature'];
	return Base.each(methods,
		function(name) {
			this[name + 'At'] = function(location, _isTime) {
				var values = this.getValues();
				return Curve[name](values, _isTime ? location
						: Curve.getTimeAt(values, location));
			};

			this[name + 'AtTime'] = function(time) {
				return Curve[name](this.getValues(), time);
			};
		}, {
			statics: {
				_evaluateMethods: methods
			}
		}
	);
},
new function() {

	function getLengthIntegrand(v) {
		var x0 = v[0], y0 = v[1],
			x1 = v[2], y1 = v[3],
			x2 = v[4], y2 = v[5],
			x3 = v[6], y3 = v[7],

			ax = 9 * (x1 - x2) + 3 * (x3 - x0),
			bx = 6 * (x0 + x2) - 12 * x1,
			cx = 3 * (x1 - x0),

			ay = 9 * (y1 - y2) + 3 * (y3 - y0),
			by = 6 * (y0 + y2) - 12 * y1,
			cy = 3 * (y1 - y0);

		return function(t) {
			var dx = (ax * t + bx) * t + cx,
				dy = (ay * t + by) * t + cy;
			return Math.sqrt(dx * dx + dy * dy);
		};
	}

	function getIterations(a, b) {
		return Math.max(2, Math.min(16, Math.ceil(Math.abs(b - a) * 32)));
	}

	function evaluate(v, t, type, normalized) {
		if (t == null || t < 0 || t > 1)
			return null;
		var x0 = v[0], y0 = v[1],
			x1 = v[2], y1 = v[3],
			x2 = v[4], y2 = v[5],
			x3 = v[6], y3 = v[7],
			isZero = Numerical.isZero;
		if (isZero(x1 - x0) && isZero(y1 - y0)) {
			x1 = x0;
			y1 = y0;
		}
		if (isZero(x2 - x3) && isZero(y2 - y3)) {
			x2 = x3;
			y2 = y3;
		}
		var cx = 3 * (x1 - x0),
			bx = 3 * (x2 - x1) - cx,
			ax = x3 - x0 - cx - bx,
			cy = 3 * (y1 - y0),
			by = 3 * (y2 - y1) - cy,
			ay = y3 - y0 - cy - by,
			x, y;
		if (type === 0) {
			x = t === 0 ? x0 : t === 1 ? x3
					: ((ax * t + bx) * t + cx) * t + x0;
			y = t === 0 ? y0 : t === 1 ? y3
					: ((ay * t + by) * t + cy) * t + y0;
		} else {
			var tMin = 1e-8,
				tMax = 1 - tMin;
			if (t < tMin) {
				x = cx;
				y = cy;
			} else if (t > tMax) {
				x = 3 * (x3 - x2);
				y = 3 * (y3 - y2);
			} else {
				x = (3 * ax * t + 2 * bx) * t + cx;
				y = (3 * ay * t + 2 * by) * t + cy;
			}
			if (normalized) {
				if (x === 0 && y === 0 && (t < tMin || t > tMax)) {
					x = x2 - x1;
					y = y2 - y1;
				}
				var len = Math.sqrt(x * x + y * y);
				if (len) {
					x /= len;
					y /= len;
				}
			}
			if (type === 3) {
				var x2 = 6 * ax * t + 2 * bx,
					y2 = 6 * ay * t + 2 * by,
					d = Math.pow(x * x + y * y, 3 / 2);
				x = d !== 0 ? (x * y2 - y * x2) / d : 0;
				y = 0;
			}
		}
		return type === 2 ? new Point(y, -x) : new Point(x, y);
	}

	return { statics: {

		classify: function(v) {

			var x0 = v[0], y0 = v[1],
				x1 = v[2], y1 = v[3],
				x2 = v[4], y2 = v[5],
				x3 = v[6], y3 = v[7],
				a1 = x0 * (y3 - y2) + y0 * (x2 - x3) + x3 * y2 - y3 * x2,
				a2 = x1 * (y0 - y3) + y1 * (x3 - x0) + x0 * y3 - y0 * x3,
				a3 = x2 * (y1 - y0) + y2 * (x0 - x1) + x1 * y0 - y1 * x0,
				d3 = 3 * a3,
				d2 = d3 - a2,
				d1 = d2 - a2 + a1,
				l = Math.sqrt(d1 * d1 + d2 * d2 + d3 * d3),
				s = l !== 0 ? 1 / l : 0,
				isZero = Numerical.isZero,
				serpentine = 'serpentine';
			d1 *= s;
			d2 *= s;
			d3 *= s;

			function type(type, t1, t2) {
				var hasRoots = t1 !== undefined,
					t1Ok = hasRoots && t1 > 0 && t1 < 1,
					t2Ok = hasRoots && t2 > 0 && t2 < 1;
				if (hasRoots && (!(t1Ok || t2Ok)
						|| type === 'loop' && !(t1Ok && t2Ok))) {
					type = 'arch';
					t1Ok = t2Ok = false;
				}
				return {
					type: type,
					roots: t1Ok || t2Ok
							? t1Ok && t2Ok
								? t1 < t2 ? [t1, t2] : [t2, t1]
								: [t1Ok ? t1 : t2]
							: null
				};
			}

			if (isZero(d1)) {
				return isZero(d2)
						? type(isZero(d3) ? 'line' : 'quadratic')
						: type(serpentine, d3 / (3 * d2));
			}
			var d = 3 * d2 * d2 - 4 * d1 * d3;
			if (isZero(d)) {
				return type('cusp', d2 / (2 * d1));
			}
			var f1 = d > 0 ? Math.sqrt(d / 3) : Math.sqrt(-d),
				f2 = 2 * d1;
			return type(d > 0 ? serpentine : 'loop',
					(d2 + f1) / f2,
					(d2 - f1) / f2);
		},

		getLength: function(v, a, b, ds) {
			if (a === undefined)
				a = 0;
			if (b === undefined)
				b = 1;
			if (Curve.isStraight(v)) {
				var c = v;
				if (b < 1) {
					c = Curve.subdivide(c, b)[0];
					a /= b;
				}
				if (a > 0) {
					c = Curve.subdivide(c, a)[1];
				}
				var dx = c[6] - c[0],
					dy = c[7] - c[1];
				return Math.sqrt(dx * dx + dy * dy);
			}
			return Numerical.integrate(ds || getLengthIntegrand(v), a, b,
					getIterations(a, b));
		},

		getTimeAt: function(v, offset, start) {
			if (start === undefined)
				start = offset < 0 ? 1 : 0;
			if (offset === 0)
				return start;
			var abs = Math.abs,
				epsilon = 1e-12,
				forward = offset > 0,
				a = forward ? start : 0,
				b = forward ? 1 : start,
				ds = getLengthIntegrand(v),
				rangeLength = Curve.getLength(v, a, b, ds),
				diff = abs(offset) - rangeLength;
			if (abs(diff) < epsilon) {
				return forward ? b : a;
			} else if (diff > epsilon) {
				return null;
			}
			var guess = offset / rangeLength,
				length = 0;
			function f(t) {
				length += Numerical.integrate(ds, start, t,
						getIterations(start, t));
				start = t;
				return length - offset;
			}
			return Numerical.findRoot(f, ds, start + guess, a, b, 32,
					1e-12);
		},

		getPoint: function(v, t) {
			return evaluate(v, t, 0, false);
		},

		getTangent: function(v, t) {
			return evaluate(v, t, 1, true);
		},

		getWeightedTangent: function(v, t) {
			return evaluate(v, t, 1, false);
		},

		getNormal: function(v, t) {
			return evaluate(v, t, 2, true);
		},

		getWeightedNormal: function(v, t) {
			return evaluate(v, t, 2, false);
		},

		getCurvature: function(v, t) {
			return evaluate(v, t, 3, false).x;
		},

		getPeaks: function(v) {
			var x0 = v[0], y0 = v[1],
				x1 = v[2], y1 = v[3],
				x2 = v[4], y2 = v[5],
				x3 = v[6], y3 = v[7],
				ax =     -x0 + 3 * x1 - 3 * x2 + x3,
				bx =  3 * x0 - 6 * x1 + 3 * x2,
				cx = -3 * x0 + 3 * x1,
				ay =     -y0 + 3 * y1 - 3 * y2 + y3,
				by =  3 * y0 - 6 * y1 + 3 * y2,
				cy = -3 * y0 + 3 * y1,
				tMin = 1e-8,
				tMax = 1 - tMin,
				roots = [];
			Numerical.solveCubic(
					9 * (ax * ax + ay * ay),
					9 * (ax * bx + by * ay),
					2 * (bx * bx + by * by) + 3 * (cx * ax + cy * ay),
					(cx * bx + by * cy),
					roots, tMin, tMax);
			return roots.sort();
		}
	}};
},
new function() {

	function addLocation(locations, include, c1, t1, c2, t2, overlap) {
		var excludeStart = !overlap && c1.getPrevious() === c2,
			excludeEnd = !overlap && c1 !== c2 && c1.getNext() === c2,
			tMin = 1e-8,
			tMax = 1 - tMin;
		if (t1 !== null && t1 >= (excludeStart ? tMin : 0) &&
			t1 <= (excludeEnd ? tMax : 1)) {
			if (t2 !== null && t2 >= (excludeEnd ? tMin : 0) &&
				t2 <= (excludeStart ? tMax : 1)) {
				var loc1 = new CurveLocation(c1, t1, null, overlap),
					loc2 = new CurveLocation(c2, t2, null, overlap);
				loc1._intersection = loc2;
				loc2._intersection = loc1;
				if (!include || include(loc1)) {
					CurveLocation.insert(locations, loc1, true);
				}
			}
		}
	}

	function addCurveIntersections(v1, v2, c1, c2, locations, include, flip,
			recursion, calls, tMin, tMax, uMin, uMax) {
		if (++calls >= 4096 || ++recursion >= 40)
			return calls;
		var fatLineEpsilon = 1e-9,
			q0x = v2[0], q0y = v2[1], q3x = v2[6], q3y = v2[7],
			getSignedDistance = Line.getSignedDistance,
			d1 = getSignedDistance(q0x, q0y, q3x, q3y, v2[2], v2[3]),
			d2 = getSignedDistance(q0x, q0y, q3x, q3y, v2[4], v2[5]),
			factor = d1 * d2 > 0 ? 3 / 4 : 4 / 9,
			dMin = factor * Math.min(0, d1, d2),
			dMax = factor * Math.max(0, d1, d2),
			dp0 = getSignedDistance(q0x, q0y, q3x, q3y, v1[0], v1[1]),
			dp1 = getSignedDistance(q0x, q0y, q3x, q3y, v1[2], v1[3]),
			dp2 = getSignedDistance(q0x, q0y, q3x, q3y, v1[4], v1[5]),
			dp3 = getSignedDistance(q0x, q0y, q3x, q3y, v1[6], v1[7]),
			hull = getConvexHull(dp0, dp1, dp2, dp3),
			top = hull[0],
			bottom = hull[1],
			tMinClip,
			tMaxClip;
		if (d1 === 0 && d2 === 0
				&& dp0 === 0 && dp1 === 0 && dp2 === 0 && dp3 === 0
			|| (tMinClip = clipConvexHull(top, bottom, dMin, dMax)) == null
			|| (tMaxClip = clipConvexHull(top.reverse(), bottom.reverse(),
				dMin, dMax)) == null)
			return calls;
		var tMinNew = tMin + (tMax - tMin) * tMinClip,
			tMaxNew = tMin + (tMax - tMin) * tMaxClip;
		if (Math.max(uMax - uMin, tMaxNew - tMinNew) < fatLineEpsilon) {
			var t = (tMinNew + tMaxNew) / 2,
				u = (uMin + uMax) / 2;
			addLocation(locations, include,
					flip ? c2 : c1, flip ? u : t,
					flip ? c1 : c2, flip ? t : u);
		} else {
			v1 = Curve.getPart(v1, tMinClip, tMaxClip);
			if (tMaxClip - tMinClip > 0.8) {
				if (tMaxNew - tMinNew > uMax - uMin) {
					var parts = Curve.subdivide(v1, 0.5),
						t = (tMinNew + tMaxNew) / 2;
					calls = addCurveIntersections(
							v2, parts[0], c2, c1, locations, include, !flip,
							recursion, calls, uMin, uMax, tMinNew, t);
					calls = addCurveIntersections(
							v2, parts[1], c2, c1, locations, include, !flip,
							recursion, calls, uMin, uMax, t, tMaxNew);
				} else {
					var parts = Curve.subdivide(v2, 0.5),
						u = (uMin + uMax) / 2;
					calls = addCurveIntersections(
							parts[0], v1, c2, c1, locations, include, !flip,
							recursion, calls, uMin, u, tMinNew, tMaxNew);
					calls = addCurveIntersections(
							parts[1], v1, c2, c1, locations, include, !flip,
							recursion, calls, u, uMax, tMinNew, tMaxNew);
				}
			} else {
				if (uMax - uMin >= fatLineEpsilon) {
					calls = addCurveIntersections(
							v2, v1, c2, c1, locations, include, !flip,
							recursion, calls, uMin, uMax, tMinNew, tMaxNew);
				} else {
					calls = addCurveIntersections(
							v1, v2, c1, c2, locations, include, flip,
							recursion, calls, tMinNew, tMaxNew, uMin, uMax);
				}
			}
		}
		return calls;
	}

	function getConvexHull(dq0, dq1, dq2, dq3) {
		var p0 = [ 0, dq0 ],
			p1 = [ 1 / 3, dq1 ],
			p2 = [ 2 / 3, dq2 ],
			p3 = [ 1, dq3 ],
			dist1 = dq1 - (2 * dq0 + dq3) / 3,
			dist2 = dq2 - (dq0 + 2 * dq3) / 3,
			hull;
		if (dist1 * dist2 < 0) {
			hull = [[p0, p1, p3], [p0, p2, p3]];
		} else {
			var distRatio = dist1 / dist2;
			hull = [
				distRatio >= 2 ? [p0, p1, p3]
				: distRatio <= 0.5 ? [p0, p2, p3]
				: [p0, p1, p2, p3],
				[p0, p3]
			];
		}
		return (dist1 || dist2) < 0 ? hull.reverse() : hull;
	}

	function clipConvexHull(hullTop, hullBottom, dMin, dMax) {
		if (hullTop[0][1] < dMin) {
			return clipConvexHullPart(hullTop, true, dMin);
		} else if (hullBottom[0][1] > dMax) {
			return clipConvexHullPart(hullBottom, false, dMax);
		} else {
			return hullTop[0][0];
		}
	}

	function clipConvexHullPart(part, top, threshold) {
		var px = part[0][0],
			py = part[0][1];
		for (var i = 1, l = part.length; i < l; i++) {
			var qx = part[i][0],
				qy = part[i][1];
			if (top ? qy >= threshold : qy <= threshold) {
				return qy === threshold ? qx
						: px + (threshold - py) * (qx - px) / (qy - py);
			}
			px = qx;
			py = qy;
		}
		return null;
	}

	function getCurveLineIntersections(v, px, py, vx, vy) {
		var isZero = Numerical.isZero;
		if (isZero(vx) && isZero(vy)) {
			var t = Curve.getTimeOf(v, new Point(px, py));
			return t === null ? [] : [t];
		}
		var angle = Math.atan2(-vy, vx),
			sin = Math.sin(angle),
			cos = Math.cos(angle),
			rv = [],
			roots = [];
		for (var i = 0; i < 8; i += 2) {
			var x = v[i] - px,
				y = v[i + 1] - py;
			rv.push(
				x * cos - y * sin,
				x * sin + y * cos);
		}
		Curve.solveCubic(rv, 1, 0, roots, 0, 1);
		return roots;
	}

	function addCurveLineIntersections(v1, v2, c1, c2, locations, include,
			flip) {
		var x1 = v2[0], y1 = v2[1],
			x2 = v2[6], y2 = v2[7],
			roots = getCurveLineIntersections(v1, x1, y1, x2 - x1, y2 - y1);
		for (var i = 0, l = roots.length; i < l; i++) {
			var t1 = roots[i],
				p1 = Curve.getPoint(v1, t1),
				t2 = Curve.getTimeOf(v2, p1);
			if (t2 !== null) {
				addLocation(locations, include,
						flip ? c2 : c1, flip ? t2 : t1,
						flip ? c1 : c2, flip ? t1 : t2);
			}
		}
	}

	function addLineIntersection(v1, v2, c1, c2, locations, include) {
		var pt = Line.intersect(
				v1[0], v1[1], v1[6], v1[7],
				v2[0], v2[1], v2[6], v2[7]);
		if (pt) {
			addLocation(locations, include,
					c1, Curve.getTimeOf(v1, pt),
					c2, Curve.getTimeOf(v2, pt));
		}
	}

	function getCurveIntersections(v1, v2, c1, c2, locations, include) {
		var epsilon = 1e-12,
			min = Math.min,
			max = Math.max;

		if (max(v1[0], v1[2], v1[4], v1[6]) + epsilon >
			min(v2[0], v2[2], v2[4], v2[6]) &&
			min(v1[0], v1[2], v1[4], v1[6]) - epsilon <
			max(v2[0], v2[2], v2[4], v2[6]) &&
			max(v1[1], v1[3], v1[5], v1[7]) + epsilon >
			min(v2[1], v2[3], v2[5], v2[7]) &&
			min(v1[1], v1[3], v1[5], v1[7]) - epsilon <
			max(v2[1], v2[3], v2[5], v2[7])) {
			var overlaps = getOverlaps(v1, v2);
			if (overlaps) {
				for (var i = 0; i < 2; i++) {
					var overlap = overlaps[i];
					addLocation(locations, include,
							c1, overlap[0],
							c2, overlap[1], true);
				}
			} else {
				var straight1 = Curve.isStraight(v1),
					straight2 = Curve.isStraight(v2),
					straight = straight1 && straight2,
					flip = straight1 && !straight2,
					before = locations.length;
				(straight
					? addLineIntersection
					: straight1 || straight2
						? addCurveLineIntersections
						: addCurveIntersections)(
							flip ? v2 : v1, flip ? v1 : v2,
							flip ? c2 : c1, flip ? c1 : c2,
							locations, include, flip,
							0, 0, 0, 1, 0, 1);
				if (!straight || locations.length === before) {
					for (var i = 0; i < 4; i++) {
						var t1 = i >> 1,
							t2 = i & 1,
							i1 = t1 * 6,
							i2 = t2 * 6,
							p1 = new Point(v1[i1], v1[i1 + 1]),
							p2 = new Point(v2[i2], v2[i2 + 1]);
						if (p1.isClose(p2, epsilon)) {
							addLocation(locations, include,
									c1, t1,
									c2, t2);
						}
					}
				}
			}
		}
		return locations;
	}

	function getLoopIntersection(v1, c1, locations, include) {
		var info = Curve.classify(v1);
		if (info.type === 'loop') {
			var roots = info.roots;
			addLocation(locations, include,
					c1, roots[0],
					c1, roots[1]);
		}
	  return locations;
	}

	function getIntersections(curves1, curves2, include, matrix1, matrix2,
			_returnFirst) {
		var self = !curves2;
		if (self)
			curves2 = curves1;
		var length1 = curves1.length,
			length2 = curves2.length,
			values2 = [],
			arrays = [],
			locations,
			current;
		for (var i = 0; i < length2; i++)
			values2[i] = curves2[i].getValues(matrix2);
		for (var i = 0; i < length1; i++) {
			var curve1 = curves1[i],
				values1 = self ? values2[i] : curve1.getValues(matrix1),
				path1 = curve1.getPath();
			if (path1 !== current) {
				current = path1;
				locations = [];
				arrays.push(locations);
			}
			if (self) {
				getLoopIntersection(values1, curve1, locations, include);
			}
			for (var j = self ? i + 1 : 0; j < length2; j++) {
				if (_returnFirst && locations.length)
					return locations;
				getCurveIntersections(values1, values2[j], curve1, curves2[j],
						locations, include);
			}
		}
		locations = [];
		for (var i = 0, l = arrays.length; i < l; i++) {
			Base.push(locations, arrays[i]);
		}
		return locations;
	}

	function getOverlaps(v1, v2) {

		function getSquaredLineLength(v) {
			var x = v[6] - v[0],
				y = v[7] - v[1];
			return x * x + y * y;
		}

		var abs = Math.abs,
			getDistance = Line.getDistance,
			timeEpsilon = 1e-8,
			geomEpsilon = 1e-7,
			straight1 = Curve.isStraight(v1),
			straight2 = Curve.isStraight(v2),
			straightBoth = straight1 && straight2,
			flip = getSquaredLineLength(v1) < getSquaredLineLength(v2),
			l1 = flip ? v2 : v1,
			l2 = flip ? v1 : v2,
			px = l1[0], py = l1[1],
			vx = l1[6] - px, vy = l1[7] - py;
		if (getDistance(px, py, vx, vy, l2[0], l2[1], true) < geomEpsilon &&
			getDistance(px, py, vx, vy, l2[6], l2[7], true) < geomEpsilon) {
			if (!straightBoth &&
				getDistance(px, py, vx, vy, l1[2], l1[3], true) < geomEpsilon &&
				getDistance(px, py, vx, vy, l1[4], l1[5], true) < geomEpsilon &&
				getDistance(px, py, vx, vy, l2[2], l2[3], true) < geomEpsilon &&
				getDistance(px, py, vx, vy, l2[4], l2[5], true) < geomEpsilon) {
				straight1 = straight2 = straightBoth = true;
			}
		} else if (straightBoth) {
			return null;
		}
		if (straight1 ^ straight2) {
			return null;
		}

		var v = [v1, v2],
			pairs = [];
		for (var i = 0; i < 4 && pairs.length < 2; i++) {
			var i1 = i & 1,
				i2 = i1 ^ 1,
				t1 = i >> 1,
				t2 = Curve.getTimeOf(v[i1], new Point(
					v[i2][t1 ? 6 : 0],
					v[i2][t1 ? 7 : 1]));
			if (t2 != null) {
				var pair = i1 ? [t1, t2] : [t2, t1];
				if (!pairs.length ||
					abs(pair[0] - pairs[0][0]) > timeEpsilon &&
					abs(pair[1] - pairs[0][1]) > timeEpsilon) {
					pairs.push(pair);
				}
			}
			if (i > 2 && !pairs.length)
				break;
		}
		if (pairs.length !== 2) {
			pairs = null;
		} else if (!straightBoth) {
			var o1 = Curve.getPart(v1, pairs[0][0], pairs[1][0]),
				o2 = Curve.getPart(v2, pairs[0][1], pairs[1][1]);
			if (abs(o2[2] - o1[2]) > geomEpsilon ||
				abs(o2[3] - o1[3]) > geomEpsilon ||
				abs(o2[4] - o1[4]) > geomEpsilon ||
				abs(o2[5] - o1[5]) > geomEpsilon)
				pairs = null;
		}
		return pairs;
	}

	function getTimesWithTangent(v, tangent) {
		var x0 = v[0], y0 = v[1],
			x1 = v[2], y1 = v[3],
			x2 = v[4], y2 = v[5],
			x3 = v[6], y3 = v[7],
			normalized = tangent.normalize(),
			tx = normalized.x,
			ty = normalized.y,
			ax = 3 * x3 - 9 * x2 + 9 * x1 - 3 * x0,
			ay = 3 * y3 - 9 * y2 + 9 * y1 - 3 * y0,
			bx = 6 * x2 - 12 * x1 + 6 * x0,
			by = 6 * y2 - 12 * y1 + 6 * y0,
			cx = 3 * x1 - 3 * x0,
			cy = 3 * y1 - 3 * y0,
			den = 2 * ax * ty - 2 * ay * tx,
			times = [];
		if (Math.abs(den) < Numerical.CURVETIME_EPSILON) {
			var num = ax * cy - ay * cx,
				den = ax * by - ay * bx;
			if (den != 0) {
				var t = -num / den;
				if (t >= 0 && t <= 1) times.push(t);
			}
		} else {
			var delta = (bx * bx - 4 * ax * cx) * ty * ty +
				(-2 * bx * by + 4 * ay * cx + 4 * ax * cy) * tx * ty +
				(by * by - 4 * ay * cy) * tx * tx,
				k = bx * ty - by * tx;
			if (delta >= 0 && den != 0) {
				var d = Math.sqrt(delta),
					t0 = -(k + d) / den,
					t1 = (-k + d) / den;
				if (t0 >= 0 && t0 <= 1) times.push(t0);
				if (t1 >= 0 && t1 <= 1) times.push(t1);
			}
		}
		return times;
	}

	return {
		getIntersections: function(curve) {
			var v1 = this.getValues(),
				v2 = curve && curve !== this && curve.getValues();
			return v2 ? getCurveIntersections(v1, v2, this, curve, [])
					  : getLoopIntersection(v1, this, []);
		},

		statics: {
			getOverlaps: getOverlaps,
			getIntersections: getIntersections,
			getCurveLineIntersections: getCurveLineIntersections,
			getTimesWithTangent: getTimesWithTangent
		}
	};
});

var CurveLocation = Base.extend({
	_class: 'CurveLocation',

	initialize: function CurveLocation(curve, time, point, _overlap, _distance) {
		if (time >= 0.99999999) {
			var next = curve.getNext();
			if (next) {
				time = 0;
				curve = next;
			}
		}
		this._setCurve(curve);
		this._time = time;
		this._point = point || curve.getPointAtTime(time);
		this._overlap = _overlap;
		this._distance = _distance;
		this._intersection = this._next = this._previous = null;
	},

	_setCurve: function(curve) {
		var path = curve._path;
		this._path = path;
		this._version = path ? path._version : 0;
		this._curve = curve;
		this._segment = null;
		this._segment1 = curve._segment1;
		this._segment2 = curve._segment2;
	},

	_setSegment: function(segment) {
		this._setCurve(segment.getCurve());
		this._segment = segment;
		this._time = segment === this._segment1 ? 0 : 1;
		this._point = segment._point.clone();
	},

	getSegment: function() {
		var segment = this._segment;
		if (!segment) {
			var curve = this.getCurve(),
				time = this.getTime();
			if (time === 0) {
				segment = curve._segment1;
			} else if (time === 1) {
				segment = curve._segment2;
			} else if (time != null) {
				segment = curve.getPartLength(0, time)
					< curve.getPartLength(time, 1)
						? curve._segment1
						: curve._segment2;
			}
			this._segment = segment;
		}
		return segment;
	},

	getCurve: function() {
		var path = this._path,
			that = this;
		if (path && path._version !== this._version) {
			this._time = this._offset = this._curveOffset = this._curve = null;
		}

		function trySegment(segment) {
			var curve = segment && segment.getCurve();
			if (curve && (that._time = curve.getTimeOf(that._point)) != null) {
				that._setCurve(curve);
				return curve;
			}
		}

		return this._curve
			|| trySegment(this._segment)
			|| trySegment(this._segment1)
			|| trySegment(this._segment2.getPrevious());
	},

	getPath: function() {
		var curve = this.getCurve();
		return curve && curve._path;
	},

	getIndex: function() {
		var curve = this.getCurve();
		return curve && curve.getIndex();
	},

	getTime: function() {
		var curve = this.getCurve(),
			time = this._time;
		return curve && time == null
			? this._time = curve.getTimeOf(this._point)
			: time;
	},

	getParameter: '#getTime',

	getPoint: function() {
		return this._point;
	},

	getOffset: function() {
		var offset = this._offset;
		if (offset == null) {
			offset = 0;
			var path = this.getPath(),
				index = this.getIndex();
			if (path && index != null) {
				var curves = path.getCurves();
				for (var i = 0; i < index; i++)
					offset += curves[i].getLength();
			}
			this._offset = offset += this.getCurveOffset();
		}
		return offset;
	},

	getCurveOffset: function() {
		var offset = this._curveOffset;
		if (offset == null) {
			var curve = this.getCurve(),
				time = this.getTime();
			this._curveOffset = offset = time != null && curve
					&& curve.getPartLength(0, time);
		}
		return offset;
	},

	getIntersection: function() {
		return this._intersection;
	},

	getDistance: function() {
		return this._distance;
	},

	divide: function() {
		var curve = this.getCurve(),
			res = curve && curve.divideAtTime(this.getTime());
		if (res) {
			this._setSegment(res._segment1);
		}
		return res;
	},

	split: function() {
		var curve = this.getCurve(),
			path = curve._path,
			res = curve && curve.splitAtTime(this.getTime());
		if (res) {
			this._setSegment(path.getLastSegment());
		}
		return  res;
	},

	equals: function(loc, _ignoreOther) {
		var res = this === loc;
		if (!res && loc instanceof CurveLocation) {
			var c1 = this.getCurve(),
				c2 = loc.getCurve(),
				p1 = c1._path,
				p2 = c2._path;
			if (p1 === p2) {
				var abs = Math.abs,
					epsilon = 1e-7,
					diff = abs(this.getOffset() - loc.getOffset()),
					i1 = !_ignoreOther && this._intersection,
					i2 = !_ignoreOther && loc._intersection;
				res = (diff < epsilon
						|| p1 && abs(p1.getLength() - diff) < epsilon)
					&& (!i1 && !i2 || i1 && i2 && i1.equals(i2, true));
			}
		}
		return res;
	},

	toString: function() {
		var parts = [],
			point = this.getPoint(),
			f = Formatter.instance;
		if (point)
			parts.push('point: ' + point);
		var index = this.getIndex();
		if (index != null)
			parts.push('index: ' + index);
		var time = this.getTime();
		if (time != null)
			parts.push('time: ' + f.number(time));
		if (this._distance != null)
			parts.push('distance: ' + f.number(this._distance));
		return '{ ' + parts.join(', ') + ' }';
	},

	isTouching: function() {
		var inter = this._intersection;
		if (inter && this.getTangent().isCollinear(inter.getTangent())) {
			var curve1 = this.getCurve(),
				curve2 = inter.getCurve();
			return !(curve1.isStraight() && curve2.isStraight()
					&& curve1.getLine().intersect(curve2.getLine()));
		}
		return false;
	},

	isCrossing: function() {
		var inter = this._intersection;
		if (!inter)
			return false;
		var t1 = this.getTime(),
			t2 = inter.getTime(),
			tMin = 1e-8,
			tMax = 1 - tMin,
			t1Inside = t1 >= tMin && t1 <= tMax,
			t2Inside = t2 >= tMin && t2 <= tMax;
		if (t1Inside && t2Inside)
			return !this.isTouching();
		var c2 = this.getCurve(),
			c1 = t1 < tMin ? c2.getPrevious() : c2,
			c4 = inter.getCurve(),
			c3 = t2 < tMin ? c4.getPrevious() : c4;
		if (t1 > tMax)
			c2 = c2.getNext();
		if (t2 > tMax)
			c4 = c4.getNext();
		if (!c1 || !c2 || !c3 || !c4)
			return false;

		var offsets = [];

		function addOffsets(curve, end) {
			var v = curve.getValues(),
				roots = Curve.classify(v).roots || Curve.getPeaks(v),
				count = roots.length,
				t = end && count > 1 ? roots[count - 1]
						: count > 0 ? roots[0]
						: 0.5;
			offsets.push(Curve.getLength(v, end ? t : 0, end ? 1 : t) / 2);
		}

		function isInRange(angle, min, max) {
			return min < max
					? angle > min && angle < max
					: angle > min || angle < max;
		}

		if (!t1Inside) {
			addOffsets(c1, true);
			addOffsets(c2, false);
		}
		if (!t2Inside) {
			addOffsets(c3, true);
			addOffsets(c4, false);
		}
		var pt = this.getPoint(),
			offset = Math.min.apply(Math, offsets),
			v2 = t1Inside ? c2.getTangentAtTime(t1)
					: c2.getPointAt(offset).subtract(pt),
			v1 = t1Inside ? v2.negate()
					: c1.getPointAt(-offset).subtract(pt),
			v4 = t2Inside ? c4.getTangentAtTime(t2)
					: c4.getPointAt(offset).subtract(pt),
			v3 = t2Inside ? v4.negate()
					: c3.getPointAt(-offset).subtract(pt),
			a1 = v1.getAngle(),
			a2 = v2.getAngle(),
			a3 = v3.getAngle(),
			a4 = v4.getAngle();
		return !!(t1Inside
				? (isInRange(a1, a3, a4) ^ isInRange(a2, a3, a4)) &&
				  (isInRange(a1, a4, a3) ^ isInRange(a2, a4, a3))
				: (isInRange(a3, a1, a2) ^ isInRange(a4, a1, a2)) &&
				  (isInRange(a3, a2, a1) ^ isInRange(a4, a2, a1)));
	},

	hasOverlap: function() {
		return !!this._overlap;
	}
}, Base.each(Curve._evaluateMethods, function(name) {
	var get = name + 'At';
	this[name] = function() {
		var curve = this.getCurve(),
			time = this.getTime();
		return time != null && curve && curve[get](time, true);
	};
}, {
	preserve: true
}),
new function() {

	function insert(locations, loc, merge) {
		var length = locations.length,
			l = 0,
			r = length - 1;

		function search(index, dir) {
			for (var i = index + dir; i >= -1 && i <= length; i += dir) {
				var loc2 = locations[((i % length) + length) % length];
				if (!loc.getPoint().isClose(loc2.getPoint(),
						1e-7))
					break;
				if (loc.equals(loc2))
					return loc2;
			}
			return null;
		}

		while (l <= r) {
			var m = (l + r) >>> 1,
				loc2 = locations[m],
				found;
			if (merge && (found = loc.equals(loc2) ? loc2
					: (search(m, -1) || search(m, 1)))) {
				if (loc._overlap) {
					found._overlap = found._intersection._overlap = true;
				}
				return found;
			}
		var path1 = loc.getPath(),
			path2 = loc2.getPath(),
			diff = path1 !== path2
				? path1._id - path2._id
				: (loc.getIndex() + loc.getTime())
				- (loc2.getIndex() + loc2.getTime());
			if (diff < 0) {
				r = m - 1;
			} else {
				l = m + 1;
			}
		}
		locations.splice(l, 0, loc);
		return loc;
	}

	return { statics: {
		insert: insert,

		expand: function(locations) {
			var expanded = locations.slice();
			for (var i = locations.length - 1; i >= 0; i--) {
				insert(expanded, locations[i]._intersection, false);
			}
			return expanded;
		}
	}};
});

var PathItem = Item.extend({
	_class: 'PathItem',
	_selectBounds: false,
	_canScaleStroke: true,
	beans: true,

	initialize: function PathItem() {
	},

	statics: {
		create: function(arg) {
			var data,
				segments,
				compound;
			if (Base.isPlainObject(arg)) {
				segments = arg.segments;
				data = arg.pathData;
			} else if (Array.isArray(arg)) {
				segments = arg;
			} else if (typeof arg === 'string') {
				data = arg;
			}
			if (segments) {
				var first = segments[0];
				compound = first && Array.isArray(first[0]);
			} else if (data) {
				compound = (data.match(/m/gi) || []).length > 1
						|| /z\s*\S+/i.test(data);
			}
			var ctor = compound ? CompoundPath : Path;
			return new ctor(arg);
		}
	},

	_asPathItem: function() {
		return this;
	},

	isClockwise: function() {
		return this.getArea() >= 0;
	},

	setClockwise: function(clockwise) {
		if (this.isClockwise() != (clockwise = !!clockwise))
			this.reverse();
	},

	setPathData: function(data) {

		var parts = data && data.match(/[mlhvcsqtaz][^mlhvcsqtaz]*/ig),
			coords,
			relative = false,
			previous,
			control,
			current = new Point(),
			start = new Point();

		function getCoord(index, coord) {
			var val = +coords[index];
			if (relative)
				val += current[coord];
			return val;
		}

		function getPoint(index) {
			return new Point(
				getCoord(index, 'x'),
				getCoord(index + 1, 'y')
			);
		}

		this.clear();

		for (var i = 0, l = parts && parts.length; i < l; i++) {
			var part = parts[i],
				command = part[0],
				lower = command.toLowerCase();
			coords = part.match(/[+-]?(?:\d*\.\d+|\d+\.?)(?:[eE][+-]?\d+)?/g);
			var length = coords && coords.length;
			relative = command === lower;
			if (previous === 'z' && !/[mz]/.test(lower))
				this.moveTo(current);
			switch (lower) {
			case 'm':
			case 'l':
				var move = lower === 'm';
				for (var j = 0; j < length; j += 2) {
					this[move ? 'moveTo' : 'lineTo'](current = getPoint(j));
					if (move) {
						start = current;
						move = false;
					}
				}
				control = current;
				break;
			case 'h':
			case 'v':
				var coord = lower === 'h' ? 'x' : 'y';
				current = current.clone();
				for (var j = 0; j < length; j++) {
					current[coord] = getCoord(j, coord);
					this.lineTo(current);
				}
				control = current;
				break;
			case 'c':
				for (var j = 0; j < length; j += 6) {
					this.cubicCurveTo(
							getPoint(j),
							control = getPoint(j + 2),
							current = getPoint(j + 4));
				}
				break;
			case 's':
				for (var j = 0; j < length; j += 4) {
					this.cubicCurveTo(
							/[cs]/.test(previous)
									? current.multiply(2).subtract(control)
									: current,
							control = getPoint(j),
							current = getPoint(j + 2));
					previous = lower;
				}
				break;
			case 'q':
				for (var j = 0; j < length; j += 4) {
					this.quadraticCurveTo(
							control = getPoint(j),
							current = getPoint(j + 2));
				}
				break;
			case 't':
				for (var j = 0; j < length; j += 2) {
					this.quadraticCurveTo(
							control = (/[qt]/.test(previous)
									? current.multiply(2).subtract(control)
									: current),
							current = getPoint(j));
					previous = lower;
				}
				break;
			case 'a':
				for (var j = 0; j < length; j += 7) {
					this.arcTo(current = getPoint(j + 5),
							new Size(+coords[j], +coords[j + 1]),
							+coords[j + 2], +coords[j + 4], +coords[j + 3]);
				}
				break;
			case 'z':
				this.closePath(1e-12);
				current = start;
				break;
			}
			previous = lower;
		}
	},

	_canComposite: function() {
		return !(this.hasFill() && this.hasStroke());
	},

	_contains: function(point) {
		var winding = point.isInside(
				this.getBounds({ internal: true, handle: true }))
					? this._getWinding(point)
					: {};
		return winding.onPath || !!(this.getFillRule() === 'evenodd'
				? winding.windingL & 1 || winding.windingR & 1
				: winding.winding);
	},

	getIntersections: function(path, include, _matrix, _returnFirst) {
		var self = this === path || !path,
			matrix1 = this._matrix._orNullIfIdentity(),
			matrix2 = self ? matrix1
				: (_matrix || path._matrix)._orNullIfIdentity();
		return self || this.getBounds(matrix1).intersects(
				path.getBounds(matrix2), 1e-12)
				? Curve.getIntersections(
						this.getCurves(), !self && path.getCurves(), include,
						matrix1, matrix2, _returnFirst)
				: [];
	},

	getCrossings: function(path) {
		return this.getIntersections(path, function(inter) {
			return inter.hasOverlap() || inter.isCrossing();
		});
	},

	getNearestLocation: function() {
		var point = Point.read(arguments),
			curves = this.getCurves(),
			minDist = Infinity,
			minLoc = null;
		for (var i = 0, l = curves.length; i < l; i++) {
			var loc = curves[i].getNearestLocation(point);
			if (loc._distance < minDist) {
				minDist = loc._distance;
				minLoc = loc;
			}
		}
		return minLoc;
	},

	getNearestPoint: function() {
		var loc = this.getNearestLocation.apply(this, arguments);
		return loc ? loc.getPoint() : loc;
	},

	interpolate: function(from, to, factor) {
		var isPath = !this._children,
			name = isPath ? '_segments' : '_children',
			itemsFrom = from[name],
			itemsTo = to[name],
			items = this[name];
		if (!itemsFrom || !itemsTo || itemsFrom.length !== itemsTo.length) {
			throw new Error('Invalid operands in interpolate() call: ' +
					from + ', ' + to);
		}
		var current = items.length,
			length = itemsTo.length;
		if (current < length) {
			var ctor = isPath ? Segment : Path;
			for (var i = current; i < length; i++) {
				this.add(new ctor());
			}
		} else if (current > length) {
			this[isPath ? 'removeSegments' : 'removeChildren'](length, current);
		}
		for (var i = 0; i < length; i++) {
			items[i].interpolate(itemsFrom[i], itemsTo[i], factor);
		}
		if (isPath) {
			this.setClosed(from._closed);
			this._changed(9);
		}
	},

	compare: function(path) {
		var ok = false;
		if (path) {
			var paths1 = this._children || [this],
				paths2 = path._children ? path._children.slice() : [path],
				length1 = paths1.length,
				length2 = paths2.length,
				matched = [],
				count = 0;
			ok = true;
			for (var i1 = length1 - 1; i1 >= 0 && ok; i1--) {
				var path1 = paths1[i1];
				ok = false;
				for (var i2 = length2 - 1; i2 >= 0 && !ok; i2--) {
					if (path1.compare(paths2[i2])) {
						if (!matched[i2]) {
							matched[i2] = true;
							count++;
						}
						ok = true;
					}
				}
			}
			ok = ok && count === length2;
		}
		return ok;
	},

});

var Path = PathItem.extend({
	_class: 'Path',
	_serializeFields: {
		segments: [],
		closed: false
	},

	initialize: function Path(arg) {
		this._closed = false;
		this._segments = [];
		this._version = 0;
		var segments = Array.isArray(arg)
			? typeof arg[0] === 'object'
				? arg
				: arguments
			: arg && (arg.size === undefined && (arg.x !== undefined
					|| arg.point !== undefined))
				? arguments
				: null;
		if (segments && segments.length > 0) {
			this.setSegments(segments);
		} else {
			this._curves = undefined;
			this._segmentSelection = 0;
			if (!segments && typeof arg === 'string') {
				this.setPathData(arg);
				arg = null;
			}
		}
		this._initialize(!segments && arg);
	},

	_equals: function(item) {
		return this._closed === item._closed
				&& Base.equals(this._segments, item._segments);
	},

	copyContent: function(source) {
		this.setSegments(source._segments);
		this._closed = source._closed;
	},

	_changed: function _changed(flags) {
		_changed.base.call(this, flags);
		if (flags & 8) {
			this._length = this._area = undefined;
			if (flags & 32) {
				this._version++;
			} else if (this._curves) {
			   for (var i = 0, l = this._curves.length; i < l; i++)
					this._curves[i]._changed();
			}
		} else if (flags & 64) {
			this._bounds = undefined;
		}
	},

	getStyle: function() {
		var parent = this._parent;
		return (parent instanceof CompoundPath ? parent : this)._style;
	},

	getSegments: function() {
		return this._segments;
	},

	setSegments: function(segments) {
		var fullySelected = this.isFullySelected(),
			length = segments && segments.length;
		this._segments.length = 0;
		this._segmentSelection = 0;
		this._curves = undefined;
		if (length) {
			var last = segments[length - 1];
			if (typeof last === 'boolean') {
				this.setClosed(last);
				length--;
			}
			this._add(Segment.readList(segments, 0, {}, length));
		}
		if (fullySelected)
			this.setFullySelected(true);
	},

	getFirstSegment: function() {
		return this._segments[0];
	},

	getLastSegment: function() {
		return this._segments[this._segments.length - 1];
	},

	getCurves: function() {
		var curves = this._curves,
			segments = this._segments;
		if (!curves) {
			var length = this._countCurves();
			curves = this._curves = new Array(length);
			for (var i = 0; i < length; i++)
				curves[i] = new Curve(this, segments[i],
					segments[i + 1] || segments[0]);
		}
		return curves;
	},

	getFirstCurve: function() {
		return this.getCurves()[0];
	},

	getLastCurve: function() {
		var curves = this.getCurves();
		return curves[curves.length - 1];
	},

	isClosed: function() {
		return this._closed;
	},

	setClosed: function(closed) {
		if (this._closed != (closed = !!closed)) {
			this._closed = closed;
			if (this._curves) {
				var length = this._curves.length = this._countCurves();
				if (closed)
					this._curves[length - 1] = new Curve(this,
						this._segments[length - 1], this._segments[0]);
			}
			this._changed(41);
		}
	}
}, {
	beans: true,

	getPathData: function(_matrix, _precision) {
		var segments = this._segments,
			length = segments.length,
			f = new Formatter(_precision),
			coords = new Array(6),
			first = true,
			curX, curY,
			prevX, prevY,
			inX, inY,
			outX, outY,
			parts = [];

		function addSegment(segment, skipLine) {
			segment._transformCoordinates(_matrix, coords);
			curX = coords[0];
			curY = coords[1];
			if (first) {
				parts.push('M' + f.pair(curX, curY));
				first = false;
			} else {
				inX = coords[2];
				inY = coords[3];
				if (inX === curX && inY === curY
						&& outX === prevX && outY === prevY) {
					if (!skipLine) {
						var dx = curX - prevX,
							dy = curY - prevY;
						parts.push(
							  dx === 0 ? 'v' + f.number(dy)
							: dy === 0 ? 'h' + f.number(dx)
							: 'l' + f.pair(dx, dy));
					}
				} else {
					parts.push('c' + f.pair(outX - prevX, outY - prevY)
							 + ' ' + f.pair( inX - prevX,  inY - prevY)
							 + ' ' + f.pair(curX - prevX, curY - prevY));
				}
			}
			prevX = curX;
			prevY = curY;
			outX = coords[4];
			outY = coords[5];
		}

		if (!length)
			return '';

		for (var i = 0; i < length; i++)
			addSegment(segments[i]);
		if (this._closed && length > 0) {
			addSegment(segments[0], true);
			parts.push('z');
		}
		return parts.join('');
	},

	isEmpty: function() {
		return !this._segments.length;
	},

	_transformContent: function(matrix) {
		var segments = this._segments,
			coords = new Array(6);
		for (var i = 0, l = segments.length; i < l; i++)
			segments[i]._transformCoordinates(matrix, coords, true);
		return true;
	},

	_add: function(segs, index) {
		var segments = this._segments,
			curves = this._curves,
			amount = segs.length,
			append = index == null,
			index = append ? segments.length : index;
		for (var i = 0; i < amount; i++) {
			var segment = segs[i];
			if (segment._path)
				segment = segs[i] = segment.clone();
			segment._path = this;
			segment._index = index + i;
			if (segment._selection)
				this._updateSelection(segment, 0, segment._selection);
		}
		if (append) {
			Base.push(segments, segs);
		} else {
			segments.splice.apply(segments, [index, 0].concat(segs));
			for (var i = index + amount, l = segments.length; i < l; i++)
				segments[i]._index = i;
		}
		if (curves) {
			var total = this._countCurves(),
				start = index > 0 && index + amount - 1 === total ? index - 1
					: index,
				insert = start,
				end = Math.min(start + amount, total);
			if (segs._curves) {
				curves.splice.apply(curves, [start, 0].concat(segs._curves));
				insert += segs._curves.length;
			}
			for (var i = insert; i < end; i++)
				curves.splice(i, 0, new Curve(this, null, null));
			this._adjustCurves(start, end);
		}
		this._changed(41);
		return segs;
	},

	_adjustCurves: function(start, end) {
		var segments = this._segments,
			curves = this._curves,
			curve;
		for (var i = start; i < end; i++) {
			curve = curves[i];
			curve._path = this;
			curve._segment1 = segments[i];
			curve._segment2 = segments[i + 1] || segments[0];
			curve._changed();
		}
		if (curve = curves[this._closed && !start ? segments.length - 1
				: start - 1]) {
			curve._segment2 = segments[start] || segments[0];
			curve._changed();
		}
		if (curve = curves[end]) {
			curve._segment1 = segments[end];
			curve._changed();
		}
	},

	_countCurves: function() {
		var length = this._segments.length;
		return !this._closed && length > 0 ? length - 1 : length;
	},

	add: function(segment1 ) {
		return arguments.length > 1 && typeof segment1 !== 'number'
			? this._add(Segment.readList(arguments))
			: this._add([ Segment.read(arguments) ])[0];
	},

	insert: function(index, segment1 ) {
		return arguments.length > 2 && typeof segment1 !== 'number'
			? this._add(Segment.readList(arguments, 1), index)
			: this._add([ Segment.read(arguments, 1) ], index)[0];
	},

	addSegment: function() {
		return this._add([ Segment.read(arguments) ])[0];
	},

	insertSegment: function(index ) {
		return this._add([ Segment.read(arguments, 1) ], index)[0];
	},

	addSegments: function(segments) {
		return this._add(Segment.readList(segments));
	},

	insertSegments: function(index, segments) {
		return this._add(Segment.readList(segments), index);
	},

	removeSegment: function(index) {
		return this.removeSegments(index, index + 1)[0] || null;
	},

	removeSegments: function(start, end, _includeCurves) {
		start = start || 0;
		end = Base.pick(end, this._segments.length);
		var segments = this._segments,
			curves = this._curves,
			count = segments.length,
			removed = segments.splice(start, end - start),
			amount = removed.length;
		if (!amount)
			return removed;
		for (var i = 0; i < amount; i++) {
			var segment = removed[i];
			if (segment._selection)
				this._updateSelection(segment, segment._selection, 0);
			segment._index = segment._path = null;
		}
		for (var i = start, l = segments.length; i < l; i++)
			segments[i]._index = i;
		if (curves) {
			var index = start > 0 && end === count + (this._closed ? 1 : 0)
					? start - 1
					: start,
				curves = curves.splice(index, amount);
			for (var i = curves.length - 1; i >= 0; i--)
				curves[i]._path = null;
			if (_includeCurves)
				removed._curves = curves.slice(1);
			this._adjustCurves(index, index);
		}
		this._changed(41);
		return removed;
	},

	clear: '#removeSegments',

	hasHandles: function() {
		var segments = this._segments;
		for (var i = 0, l = segments.length; i < l; i++) {
			if (segments[i].hasHandles())
				return true;
		}
		return false;
	},

	clearHandles: function() {
		var segments = this._segments;
		for (var i = 0, l = segments.length; i < l; i++)
			segments[i].clearHandles();
	},

	getLength: function() {
		if (this._length == null) {
			var curves = this.getCurves(),
				length = 0;
			for (var i = 0, l = curves.length; i < l; i++)
				length += curves[i].getLength();
			this._length = length;
		}
		return this._length;
	},

	getArea: function() {
		var area = this._area;
		if (area == null) {
			var segments = this._segments,
				closed = this._closed;
			area = 0;
			for (var i = 0, l = segments.length; i < l; i++) {
				var last = i + 1 === l;
				area += Curve.getArea(Curve.getValues(
						segments[i], segments[last ? 0 : i + 1],
						null, last && !closed));
			}
			this._area = area;
		}
		return area;
	},

	isFullySelected: function() {
		var length = this._segments.length;
		return this.isSelected() && length > 0 && this._segmentSelection
				=== length * 7;
	},

	setFullySelected: function(selected) {
		if (selected)
			this._selectSegments(true);
		this.setSelected(selected);
	},

	setSelection: function setSelection(selection) {
		if (!(selection & 1))
			this._selectSegments(false);
		setSelection.base.call(this, selection);
	},

	_selectSegments: function(selected) {
		var segments = this._segments,
			length = segments.length,
			selection = selected ? 7 : 0;
		this._segmentSelection = selection * length;
		for (var i = 0; i < length; i++)
			segments[i]._selection = selection;
	},

	_updateSelection: function(segment, oldSelection, newSelection) {
		segment._selection = newSelection;
		var selection = this._segmentSelection += newSelection - oldSelection;
		if (selection > 0)
			this.setSelected(true);
	},

	divideAt: function(location) {
		var loc = this.getLocationAt(location),
			curve;
		return loc && (curve = loc.getCurve().divideAt(loc.getCurveOffset()))
				? curve._segment1
				: null;
	},

	splitAt: function(location) {
		var loc = this.getLocationAt(location),
			index = loc && loc.index,
			time = loc && loc.time,
			tMin = 1e-8,
			tMax = 1 - tMin;
		if (time > tMax) {
			index++;
			time = 0;
		}
		var curves = this.getCurves();
		if (index >= 0 && index < curves.length) {
			if (time >= tMin) {
				curves[index++].divideAtTime(time);
			}
			var segs = this.removeSegments(index, this._segments.length, true),
				path;
			if (this._closed) {
				this.setClosed(false);
				path = this;
			} else {
				path = new Path(Item.NO_INSERT);
				path.insertAbove(this);
				path.copyAttributes(this);
			}
			path._add(segs, 0);
			this.addSegment(segs[0]);
			return path;
		}
		return null;
	},

	split: function(index, time) {
		var curve,
			location = time === undefined ? index
				: (curve = this.getCurves()[index])
					&& curve.getLocationAtTime(time);
		return location != null ? this.splitAt(location) : null;
	},

	join: function(path, tolerance) {
		var epsilon = tolerance || 0;
		if (path && path !== this) {
			var segments = path._segments,
				last1 = this.getLastSegment(),
				last2 = path.getLastSegment();
			if (!last2)
				return this;
			if (last1 && last1._point.isClose(last2._point, epsilon))
				path.reverse();
			var first2 = path.getFirstSegment();
			if (last1 && last1._point.isClose(first2._point, epsilon)) {
				last1.setHandleOut(first2._handleOut);
				this._add(segments.slice(1));
			} else {
				var first1 = this.getFirstSegment();
				if (first1 && first1._point.isClose(first2._point, epsilon))
					path.reverse();
				last2 = path.getLastSegment();
				if (first1 && first1._point.isClose(last2._point, epsilon)) {
					first1.setHandleIn(last2._handleIn);
					this._add(segments.slice(0, segments.length - 1), 0);
				} else {
					this._add(segments.slice());
				}
			}
			if (path._closed)
				this._add([segments[0]]);
			path.remove();
		}
		var first = this.getFirstSegment(),
			last = this.getLastSegment();
		if (first !== last && first._point.isClose(last._point, epsilon)) {
			first.setHandleIn(last._handleIn);
			last.remove();
			this.setClosed(true);
		}
		return this;
	},

	reduce: function(options) {
		var curves = this.getCurves(),
			simplify = options && options.simplify,
			tolerance = simplify ? 1e-7 : 0;
		for (var i = curves.length - 1; i >= 0; i--) {
			var curve = curves[i];
			if (!curve.hasHandles() && (!curve.hasLength(tolerance)
					|| simplify && curve.isCollinear(curve.getNext())))
				curve.remove();
		}
		return this;
	},

	reverse: function() {
		this._segments.reverse();
		for (var i = 0, l = this._segments.length; i < l; i++) {
			var segment = this._segments[i];
			var handleIn = segment._handleIn;
			segment._handleIn = segment._handleOut;
			segment._handleOut = handleIn;
			segment._index = i;
		}
		this._curves = null;
		this._changed(9);
	},

	flatten: function(flatness) {
		var flattener = new PathFlattener(this, flatness || 0.25, 256, true),
			parts = flattener.parts,
			length = parts.length,
			segments = [];
		for (var i = 0; i < length; i++) {
			segments.push(new Segment(parts[i].curve.slice(0, 2)));
		}
		if (!this._closed && length > 0) {
			segments.push(new Segment(parts[length - 1].curve.slice(6)));
		}
		this.setSegments(segments);
	},

	simplify: function(tolerance) {
		var segments = new PathFitter(this).fit(tolerance || 2.5);
		if (segments)
			this.setSegments(segments);
		return !!segments;
	},

	smooth: function(options) {
		var that = this,
			opts = options || {},
			type = opts.type || 'asymmetric',
			segments = this._segments,
			length = segments.length,
			closed = this._closed;

		function getIndex(value, _default) {
			var index = value && value.index;
			if (index != null) {
				var path = value.path;
				if (path && path !== that)
					throw new Error(value._class + ' ' + index + ' of ' + path
							+ ' is not part of ' + that);
				if (_default && value instanceof Curve)
					index++;
			} else {
				index = typeof value === 'number' ? value : _default;
			}
			return Math.min(index < 0 && closed
					? index % length
					: index < 0 ? index + length : index, length - 1);
		}

		var loop = closed && opts.from === undefined && opts.to === undefined,
			from = getIndex(opts.from, 0),
			to = getIndex(opts.to, length - 1);

		if (from > to) {
			if (closed) {
				from -= length;
			} else {
				var tmp = from;
				from = to;
				to = tmp;
			}
		}
		if (/^(?:asymmetric|continuous)$/.test(type)) {
			var asymmetric = type === 'asymmetric',
				min = Math.min,
				amount = to - from + 1,
				n = amount - 1,
				padding = loop ? min(amount, 4) : 1,
				paddingLeft = padding,
				paddingRight = padding,
				knots = [];
			if (!closed) {
				paddingLeft = min(1, from);
				paddingRight = min(1, length - to - 1);
			}
			n += paddingLeft + paddingRight;
			if (n <= 1)
				return;
			for (var i = 0, j = from - paddingLeft; i <= n; i++, j++) {
				knots[i] = segments[(j < 0 ? j + length : j) % length]._point;
			}

			var x = knots[0]._x + 2 * knots[1]._x,
				y = knots[0]._y + 2 * knots[1]._y,
				f = 2,
				n_1 = n - 1,
				rx = [x],
				ry = [y],
				rf = [f],
				px = [],
				py = [];
			for (var i = 1; i < n; i++) {
				var internal = i < n_1,
					a = internal ? 1 : asymmetric ? 1 : 2,
					b = internal ? 4 : asymmetric ? 2 : 7,
					u = internal ? 4 : asymmetric ? 3 : 8,
					v = internal ? 2 : asymmetric ? 0 : 1,
					m = a / f;
				f = rf[i] = b - m;
				x = rx[i] = u * knots[i]._x + v * knots[i + 1]._x - m * x;
				y = ry[i] = u * knots[i]._y + v * knots[i + 1]._y - m * y;
			}

			px[n_1] = rx[n_1] / rf[n_1];
			py[n_1] = ry[n_1] / rf[n_1];
			for (var i = n - 2; i >= 0; i--) {
				px[i] = (rx[i] - px[i + 1]) / rf[i];
				py[i] = (ry[i] - py[i + 1]) / rf[i];
			}
			px[n] = (3 * knots[n]._x - px[n_1]) / 2;
			py[n] = (3 * knots[n]._y - py[n_1]) / 2;

			for (var i = paddingLeft, max = n - paddingRight, j = from;
					i <= max; i++, j++) {
				var segment = segments[j < 0 ? j + length : j],
					pt = segment._point,
					hx = px[i] - pt._x,
					hy = py[i] - pt._y;
				if (loop || i < max)
					segment.setHandleOut(hx, hy);
				if (loop || i > paddingLeft)
					segment.setHandleIn(-hx, -hy);
			}
		} else {
			for (var i = from; i <= to; i++) {
				segments[i < 0 ? i + length : i].smooth(opts,
						!loop && i === from, !loop && i === to);
			}
		}
	},

	toShape: function(insert) {
		if (!this._closed)
			return null;

		var segments = this._segments,
			type,
			size,
			radius,
			topCenter;

		function isCollinear(i, j) {
			var seg1 = segments[i],
				seg2 = seg1.getNext(),
				seg3 = segments[j],
				seg4 = seg3.getNext();
			return seg1._handleOut.isZero() && seg2._handleIn.isZero()
					&& seg3._handleOut.isZero() && seg4._handleIn.isZero()
					&& seg2._point.subtract(seg1._point).isCollinear(
						seg4._point.subtract(seg3._point));
		}

		function isOrthogonal(i) {
			var seg2 = segments[i],
				seg1 = seg2.getPrevious(),
				seg3 = seg2.getNext();
			return seg1._handleOut.isZero() && seg2._handleIn.isZero()
					&& seg2._handleOut.isZero() && seg3._handleIn.isZero()
					&& seg2._point.subtract(seg1._point).isOrthogonal(
						seg3._point.subtract(seg2._point));
		}

		function isArc(i) {
			var seg1 = segments[i],
				seg2 = seg1.getNext(),
				handle1 = seg1._handleOut,
				handle2 = seg2._handleIn,
				kappa = 0.5522847498307936;
			if (handle1.isOrthogonal(handle2)) {
				var pt1 = seg1._point,
					pt2 = seg2._point,
					corner = new Line(pt1, handle1, true).intersect(
							new Line(pt2, handle2, true), true);
				return corner && Numerical.isZero(handle1.getLength() /
						corner.subtract(pt1).getLength() - kappa)
					&& Numerical.isZero(handle2.getLength() /
						corner.subtract(pt2).getLength() - kappa);
			}
			return false;
		}

		function getDistance(i, j) {
			return segments[i]._point.getDistance(segments[j]._point);
		}

		if (!this.hasHandles() && segments.length === 4
				&& isCollinear(0, 2) && isCollinear(1, 3) && isOrthogonal(1)) {
			type = Shape.Rectangle;
			size = new Size(getDistance(0, 3), getDistance(0, 1));
			topCenter = segments[1]._point.add(segments[2]._point).divide(2);
		} else if (segments.length === 8 && isArc(0) && isArc(2) && isArc(4)
				&& isArc(6) && isCollinear(1, 5) && isCollinear(3, 7)) {
			type = Shape.Rectangle;
			size = new Size(getDistance(1, 6), getDistance(0, 3));
			radius = size.subtract(new Size(getDistance(0, 7),
					getDistance(1, 2))).divide(2);
			topCenter = segments[3]._point.add(segments[4]._point).divide(2);
		} else if (segments.length === 4
				&& isArc(0) && isArc(1) && isArc(2) && isArc(3)) {
			if (Numerical.isZero(getDistance(0, 2) - getDistance(1, 3))) {
				type = Shape.Circle;
				radius = getDistance(0, 2) / 2;
			} else {
				type = Shape.Ellipse;
				radius = new Size(getDistance(2, 0) / 2, getDistance(3, 1) / 2);
			}
			topCenter = segments[1]._point;
		}

		if (type) {
			var center = this.getPosition(true),
				shape = new type({
					center: center,
					size: size,
					radius: radius,
					insert: false
				});
			shape.copyAttributes(this, true);
			shape._matrix.prepend(this._matrix);
			shape.rotate(topCenter.subtract(center).getAngle() + 90);
			if (insert === undefined || insert)
				shape.insertAbove(this);
			return shape;
		}
		return null;
	},

	toPath: '#clone',

	compare: function compare(path) {
		if (!path || path instanceof CompoundPath)
			return compare.base.call(this, path);
		var curves1 = this.getCurves(),
			curves2 = path.getCurves(),
			length1 = curves1.length,
			length2 = curves2.length;
		if (!length1 || !length2) {
			return length1 == length2;
		}
		var v1 = curves1[0].getValues(),
			values2 = [],
			pos1 = 0, pos2,
			end1 = 0, end2;
		for (var i = 0; i < length2; i++) {
			var v2 = curves2[i].getValues();
			values2.push(v2);
			var overlaps = Curve.getOverlaps(v1, v2);
			if (overlaps) {
				pos2 = !i && overlaps[0][0] > 0 ? length2 - 1 : i;
				end2 = overlaps[0][1];
				break;
			}
		}
		var abs = Math.abs,
			epsilon = 1e-8,
			v2 = values2[pos2],
			start2;
		while (v1 && v2) {
			var overlaps = Curve.getOverlaps(v1, v2);
			if (overlaps) {
				var t1 = overlaps[0][0];
				if (abs(t1 - end1) < epsilon) {
					end1 = overlaps[1][0];
					if (end1 === 1) {
						v1 = ++pos1 < length1 ? curves1[pos1].getValues() : null;
						end1 = 0;
					}
					var t2 = overlaps[0][1];
					if (abs(t2 - end2) < epsilon) {
						if (!start2)
							start2 = [pos2, t2];
						end2 = overlaps[1][1];
						if (end2 === 1) {
							if (++pos2 >= length2)
								pos2 = 0;
							v2 = values2[pos2] || curves2[pos2].getValues();
							end2 = 0;
						}
						if (!v1) {
							return start2[0] === pos2 && start2[1] === end2;
						}
						continue;
					}
				}
			}
			break;
		}
		return false;
	},

	_hitTestSelf: function(point, options, viewMatrix, strokeMatrix) {
		var that = this,
			style = this.getStyle(),
			segments = this._segments,
			numSegments = segments.length,
			closed = this._closed,
			tolerancePadding = options._tolerancePadding,
			strokePadding = tolerancePadding,
			join, cap, miterLimit,
			area, loc, res,
			hitStroke = options.stroke && style.hasStroke(),
			hitFill = options.fill && style.hasFill(),
			hitCurves = options.curves,
			strokeRadius = hitStroke
					? style.getStrokeWidth() / 2
					: hitFill && options.tolerance > 0 || hitCurves
						? 0 : null;
		if (strokeRadius !== null) {
			if (strokeRadius > 0) {
				join = style.getStrokeJoin();
				cap = style.getStrokeCap();
				miterLimit = style.getMiterLimit();
				strokePadding = strokePadding.add(
					Path._getStrokePadding(strokeRadius, strokeMatrix));
			} else {
				join = cap = 'round';
			}
		}

		function isCloseEnough(pt, padding) {
			return point.subtract(pt).divide(padding).length <= 1;
		}

		function checkSegmentPoint(seg, pt, name) {
			if (!options.selected || pt.isSelected()) {
				var anchor = seg._point;
				if (pt !== anchor)
					pt = pt.add(anchor);
				if (isCloseEnough(pt, strokePadding)) {
					return new HitResult(name, that, {
						segment: seg,
						point: pt
					});
				}
			}
		}

		function checkSegmentPoints(seg, ends) {
			return (ends || options.segments)
				&& checkSegmentPoint(seg, seg._point, 'segment')
				|| (!ends && options.handles) && (
					checkSegmentPoint(seg, seg._handleIn, 'handle-in') ||
					checkSegmentPoint(seg, seg._handleOut, 'handle-out'));
		}

		function addToArea(point) {
			area.add(point);
		}

		function checkSegmentStroke(segment) {
			var isJoin = closed || segment._index > 0
					&& segment._index < numSegments - 1;
			if ((isJoin ? join : cap) === 'round') {
				return isCloseEnough(segment._point, strokePadding);
			} else {
				area = new Path({ internal: true, closed: true });
				if (isJoin) {
					if (!segment.isSmooth()) {
						Path._addBevelJoin(segment, join, strokeRadius,
							   miterLimit, null, strokeMatrix, addToArea, true);
					}
				} else if (cap === 'square') {
					Path._addSquareCap(segment, cap, strokeRadius, null,
							strokeMatrix, addToArea, true);
				}
				if (!area.isEmpty()) {
					var loc;
					return area.contains(point)
						|| (loc = area.getNearestLocation(point))
							&& isCloseEnough(loc.getPoint(), tolerancePadding);
				}
			}
		}

		if (options.ends && !options.segments && !closed) {
			if (res = checkSegmentPoints(segments[0], true)
					|| checkSegmentPoints(segments[numSegments - 1], true))
				return res;
		} else if (options.segments || options.handles) {
			for (var i = 0; i < numSegments; i++)
				if (res = checkSegmentPoints(segments[i]))
					return res;
		}
		if (strokeRadius !== null) {
			loc = this.getNearestLocation(point);
			if (loc) {
				var time = loc.getTime();
				if (time === 0 || time === 1 && numSegments > 1) {
					if (!checkSegmentStroke(loc.getSegment()))
						loc = null;
				} else if (!isCloseEnough(loc.getPoint(), strokePadding)) {
					loc = null;
				}
			}
			if (!loc && join === 'miter' && numSegments > 1) {
				for (var i = 0; i < numSegments; i++) {
					var segment = segments[i];
					if (point.getDistance(segment._point)
							<= miterLimit * strokeRadius
							&& checkSegmentStroke(segment)) {
						loc = segment.getLocation();
						break;
					}
				}
			}
		}
		return !loc && hitFill && this._contains(point)
				|| loc && !hitStroke && !hitCurves
					? new HitResult('fill', this)
					: loc
						? new HitResult(hitStroke ? 'stroke' : 'curve', this, {
							location: loc,
							point: loc.getPoint()
						})
						: null;
	}

}, Base.each(Curve._evaluateMethods,
	function(name) {
		this[name + 'At'] = function(offset) {
			var loc = this.getLocationAt(offset);
			return loc && loc[name]();
		};
	},
{
	beans: false,

	getLocationOf: function() {
		var point = Point.read(arguments),
			curves = this.getCurves();
		for (var i = 0, l = curves.length; i < l; i++) {
			var loc = curves[i].getLocationOf(point);
			if (loc)
				return loc;
		}
		return null;
	},

	getOffsetOf: function() {
		var loc = this.getLocationOf.apply(this, arguments);
		return loc ? loc.getOffset() : null;
	},

	getLocationAt: function(offset) {
		if (typeof offset === 'number') {
			var curves = this.getCurves(),
				length = 0;
			for (var i = 0, l = curves.length; i < l; i++) {
				var start = length,
					curve = curves[i];
				length += curve.getLength();
				if (length > offset) {
					return curve.getLocationAt(offset - start);
				}
			}
			if (curves.length > 0 && offset <= this.getLength()) {
				return new CurveLocation(curves[curves.length - 1], 1);
			}
		} else if (offset && offset.getPath && offset.getPath() === this) {
			return offset;
		}
		return null;
	},

	getOffsetsWithTangent: function() {
		var tangent = Point.read(arguments);
		if (tangent.isZero()) {
			return [];
		}

		var offsets = [];
		var curveStart = 0;
		var curves = this.getCurves();
		for (var i = 0, l = curves.length; i < l; i++) {
			var curve = curves[i];
			var curveTimes = curve.getTimesWithTangent(tangent);
			for (var j = 0, m = curveTimes.length; j < m; j++) {
				var offset = curveStart + curve.getOffsetAtTime(curveTimes[j]);
				if (offsets.indexOf(offset) < 0) {
					offsets.push(offset);
				}
			}
			curveStart += curve.length;
		}
		return offsets;
	}
}),
new function() {

	function drawHandles(ctx, segments, matrix, size) {
		var half = size / 2,
			coords = new Array(6),
			pX, pY;

		function drawHandle(index) {
			var hX = coords[index],
				hY = coords[index + 1];
			if (pX != hX || pY != hY) {
				ctx.beginPath();
				ctx.moveTo(pX, pY);
				ctx.lineTo(hX, hY);
				ctx.stroke();
				ctx.beginPath();
				ctx.arc(hX, hY, half, 0, Math.PI * 2, true);
				ctx.fill();
			}
		}

		for (var i = 0, l = segments.length; i < l; i++) {
			var segment = segments[i],
				selection = segment._selection;
			segment._transformCoordinates(matrix, coords);
			pX = coords[0];
			pY = coords[1];
			if (selection & 2)
				drawHandle(2);
			if (selection & 4)
				drawHandle(4);
			ctx.fillRect(pX - half, pY - half, size, size);
			if (!(selection & 1)) {
				var fillStyle = ctx.fillStyle;
				ctx.fillStyle = '#ffffff';
				ctx.fillRect(pX - half + 1, pY - half + 1, size - 2, size - 2);
				ctx.fillStyle = fillStyle;
			}
		}
	}

	function drawSegments(ctx, path, matrix) {
		var segments = path._segments,
			length = segments.length,
			coords = new Array(6),
			first = true,
			curX, curY,
			prevX, prevY,
			inX, inY,
			outX, outY;

		function drawSegment(segment) {
			if (matrix) {
				segment._transformCoordinates(matrix, coords);
				curX = coords[0];
				curY = coords[1];
			} else {
				var point = segment._point;
				curX = point._x;
				curY = point._y;
			}
			if (first) {
				ctx.moveTo(curX, curY);
				first = false;
			} else {
				if (matrix) {
					inX = coords[2];
					inY = coords[3];
				} else {
					var handle = segment._handleIn;
					inX = curX + handle._x;
					inY = curY + handle._y;
				}
				if (inX === curX && inY === curY
						&& outX === prevX && outY === prevY) {
					ctx.lineTo(curX, curY);
				} else {
					ctx.bezierCurveTo(outX, outY, inX, inY, curX, curY);
				}
			}
			prevX = curX;
			prevY = curY;
			if (matrix) {
				outX = coords[4];
				outY = coords[5];
			} else {
				var handle = segment._handleOut;
				outX = prevX + handle._x;
				outY = prevY + handle._y;
			}
		}

		for (var i = 0; i < length; i++)
			drawSegment(segments[i]);
		if (path._closed && length > 0)
			drawSegment(segments[0]);
	}

	return {
		_draw: function(ctx, param, viewMatrix, strokeMatrix) {
			var dontStart = param.dontStart,
				dontPaint = param.dontFinish || param.clip,
				style = this.getStyle(),
				hasFill = style.hasFill(),
				hasStroke = style.hasStroke(),
				dashArray = style.getDashArray(),
				dashLength = !paper.support.nativeDash && hasStroke
						&& dashArray && dashArray.length;

			if (!dontStart)
				ctx.beginPath();

			if (hasFill || hasStroke && !dashLength || dontPaint) {
				drawSegments(ctx, this, strokeMatrix);
				if (this._closed)
					ctx.closePath();
			}

			function getOffset(i) {
				return dashArray[((i % dashLength) + dashLength) % dashLength];
			}

			if (!dontPaint && (hasFill || hasStroke)) {
				this._setStyles(ctx, param, viewMatrix);
				if (hasFill) {
					ctx.fill(style.getFillRule());
					ctx.shadowColor = 'rgba(0,0,0,0)';
				}
				if (hasStroke) {
					if (dashLength) {
						if (!dontStart)
							ctx.beginPath();
						var flattener = new PathFlattener(this, 0.25, 32, false,
								strokeMatrix),
							length = flattener.length,
							from = -style.getDashOffset(), to,
							i = 0;
						from = from % length;
						while (from > 0) {
							from -= getOffset(i--) + getOffset(i--);
						}
						while (from < length) {
							to = from + getOffset(i++);
							if (from > 0 || to > 0)
								flattener.drawPart(ctx,
										Math.max(from, 0), Math.max(to, 0));
							from = to + getOffset(i++);
						}
					}
					ctx.stroke();
				}
			}
		},

		_drawSelected: function(ctx, matrix) {
			ctx.beginPath();
			drawSegments(ctx, this, matrix);
			ctx.stroke();
			drawHandles(ctx, this._segments, matrix, paper.settings.handleSize);
		}
	};
},
new function() {
	function getCurrentSegment(that) {
		var segments = that._segments;
		if (!segments.length)
			throw new Error('Use a moveTo() command first');
		return segments[segments.length - 1];
	}

	return {
		moveTo: function() {
			var segments = this._segments;
			if (segments.length === 1)
				this.removeSegment(0);
			if (!segments.length)
				this._add([ new Segment(Point.read(arguments)) ]);
		},

		moveBy: function() {
			throw new Error('moveBy() is unsupported on Path items.');
		},

		lineTo: function() {
			this._add([ new Segment(Point.read(arguments)) ]);
		},

		cubicCurveTo: function() {
			var handle1 = Point.read(arguments),
				handle2 = Point.read(arguments),
				to = Point.read(arguments),
				current = getCurrentSegment(this);
			current.setHandleOut(handle1.subtract(current._point));
			this._add([ new Segment(to, handle2.subtract(to)) ]);
		},

		quadraticCurveTo: function() {
			var handle = Point.read(arguments),
				to = Point.read(arguments),
				current = getCurrentSegment(this)._point;
			this.cubicCurveTo(
				handle.add(current.subtract(handle).multiply(1 / 3)),
				handle.add(to.subtract(handle).multiply(1 / 3)),
				to
			);
		},

		curveTo: function() {
			var through = Point.read(arguments),
				to = Point.read(arguments),
				t = Base.pick(Base.read(arguments), 0.5),
				t1 = 1 - t,
				current = getCurrentSegment(this)._point,
				handle = through.subtract(current.multiply(t1 * t1))
					.subtract(to.multiply(t * t)).divide(2 * t * t1);
			if (handle.isNaN())
				throw new Error(
					'Cannot put a curve through points with parameter = ' + t);
			this.quadraticCurveTo(handle, to);
		},

		arcTo: function() {
			var abs = Math.abs,
				sqrt = Math.sqrt,
				current = getCurrentSegment(this),
				from = current._point,
				to = Point.read(arguments),
				through,
				peek = Base.peek(arguments),
				clockwise = Base.pick(peek, true),
				center, extent, vector, matrix;
			if (typeof clockwise === 'boolean') {
				var middle = from.add(to).divide(2),
				through = middle.add(middle.subtract(from).rotate(
						clockwise ? -90 : 90));
			} else if (Base.remain(arguments) <= 2) {
				through = to;
				to = Point.read(arguments);
			} else {
				var radius = Size.read(arguments),
					isZero = Numerical.isZero;
				if (isZero(radius.width) || isZero(radius.height))
					return this.lineTo(to);
				var rotation = Base.read(arguments),
					clockwise = !!Base.read(arguments),
					large = !!Base.read(arguments),
					middle = from.add(to).divide(2),
					pt = from.subtract(middle).rotate(-rotation),
					x = pt.x,
					y = pt.y,
					rx = abs(radius.width),
					ry = abs(radius.height),
					rxSq = rx * rx,
					rySq = ry * ry,
					xSq = x * x,
					ySq = y * y;
				var factor = sqrt(xSq / rxSq + ySq / rySq);
				if (factor > 1) {
					rx *= factor;
					ry *= factor;
					rxSq = rx * rx;
					rySq = ry * ry;
				}
				factor = (rxSq * rySq - rxSq * ySq - rySq * xSq) /
						(rxSq * ySq + rySq * xSq);
				if (abs(factor) < 1e-12)
					factor = 0;
				if (factor < 0)
					throw new Error(
							'Cannot create an arc with the given arguments');
				center = new Point(rx * y / ry, -ry * x / rx)
						.multiply((large === clockwise ? -1 : 1) * sqrt(factor))
						.rotate(rotation).add(middle);
				matrix = new Matrix().translate(center).rotate(rotation)
						.scale(rx, ry);
				vector = matrix._inverseTransform(from);
				extent = vector.getDirectedAngle(matrix._inverseTransform(to));
				if (!clockwise && extent > 0)
					extent -= 360;
				else if (clockwise && extent < 0)
					extent += 360;
			}
			if (through) {
				var l1 = new Line(from.add(through).divide(2),
							through.subtract(from).rotate(90), true),
					l2 = new Line(through.add(to).divide(2),
							to.subtract(through).rotate(90), true),
					line = new Line(from, to),
					throughSide = line.getSide(through);
				center = l1.intersect(l2, true);
				if (!center) {
					if (!throughSide)
						return this.lineTo(to);
					throw new Error(
							'Cannot create an arc with the given arguments');
				}
				vector = from.subtract(center);
				extent = vector.getDirectedAngle(to.subtract(center));
				var centerSide = line.getSide(center, true);
				if (centerSide === 0) {
					extent = throughSide * abs(extent);
				} else if (throughSide === centerSide) {
					extent += extent < 0 ? 360 : -360;
				}
			}
			var epsilon = 1e-7,
				ext = abs(extent),
				count = ext >= 360 ? 4 : Math.ceil((ext - epsilon) / 90),
				inc = extent / count,
				half = inc * Math.PI / 360,
				z = 4 / 3 * Math.sin(half) / (1 + Math.cos(half)),
				segments = [];
			for (var i = 0; i <= count; i++) {
				var pt = to,
					out = null;
				if (i < count) {
					out = vector.rotate(90).multiply(z);
					if (matrix) {
						pt = matrix._transformPoint(vector);
						out = matrix._transformPoint(vector.add(out))
								.subtract(pt);
					} else {
						pt = center.add(vector);
					}
				}
				if (!i) {
					current.setHandleOut(out);
				} else {
					var _in = vector.rotate(-90).multiply(z);
					if (matrix) {
						_in = matrix._transformPoint(vector.add(_in))
								.subtract(pt);
					}
					segments.push(new Segment(pt, _in, out));
				}
				vector = vector.rotate(inc);
			}
			this._add(segments);
		},

		lineBy: function() {
			var to = Point.read(arguments),
				current = getCurrentSegment(this)._point;
			this.lineTo(current.add(to));
		},

		curveBy: function() {
			var through = Point.read(arguments),
				to = Point.read(arguments),
				parameter = Base.read(arguments),
				current = getCurrentSegment(this)._point;
			this.curveTo(current.add(through), current.add(to), parameter);
		},

		cubicCurveBy: function() {
			var handle1 = Point.read(arguments),
				handle2 = Point.read(arguments),
				to = Point.read(arguments),
				current = getCurrentSegment(this)._point;
			this.cubicCurveTo(current.add(handle1), current.add(handle2),
					current.add(to));
		},

		quadraticCurveBy: function() {
			var handle = Point.read(arguments),
				to = Point.read(arguments),
				current = getCurrentSegment(this)._point;
			this.quadraticCurveTo(current.add(handle), current.add(to));
		},

		arcBy: function() {
			var current = getCurrentSegment(this)._point,
				point = current.add(Point.read(arguments)),
				clockwise = Base.pick(Base.peek(arguments), true);
			if (typeof clockwise === 'boolean') {
				this.arcTo(point, clockwise);
			} else {
				this.arcTo(point, current.add(Point.read(arguments)));
			}
		},

		closePath: function(tolerance) {
			this.setClosed(true);
			this.join(this, tolerance);
		}
	};
}, {

	_getBounds: function(matrix, options) {
		var method = options.handle
				? 'getHandleBounds'
				: options.stroke
				? 'getStrokeBounds'
				: 'getBounds';
		return Path[method](this._segments, this._closed, this, matrix, options);
	},

statics: {
	getBounds: function(segments, closed, path, matrix, options, strokePadding) {
		var first = segments[0];
		if (!first)
			return new Rectangle();
		var coords = new Array(6),
			prevCoords = first._transformCoordinates(matrix, new Array(6)),
			min = prevCoords.slice(0, 2),
			max = min.slice(),
			roots = new Array(2);

		function processSegment(segment) {
			segment._transformCoordinates(matrix, coords);
			for (var i = 0; i < 2; i++) {
				Curve._addBounds(
					prevCoords[i],
					prevCoords[i + 4],
					coords[i + 2],
					coords[i],
					i, strokePadding ? strokePadding[i] : 0, min, max, roots);
			}
			var tmp = prevCoords;
			prevCoords = coords;
			coords = tmp;
		}

		for (var i = 1, l = segments.length; i < l; i++)
			processSegment(segments[i]);
		if (closed)
			processSegment(first);
		return new Rectangle(min[0], min[1], max[0] - min[0], max[1] - min[1]);
	},

	getStrokeBounds: function(segments, closed, path, matrix, options) {
		var style = path.getStyle(),
			stroke = style.hasStroke(),
			strokeWidth = style.getStrokeWidth(),
			strokeMatrix = stroke && path._getStrokeMatrix(matrix, options),
			strokePadding = stroke && Path._getStrokePadding(strokeWidth,
				strokeMatrix),
			bounds = Path.getBounds(segments, closed, path, matrix, options,
				strokePadding);
		if (!stroke)
			return bounds;
		var strokeRadius = strokeWidth / 2,
			join = style.getStrokeJoin(),
			cap = style.getStrokeCap(),
			miterLimit = style.getMiterLimit(),
			joinBounds = new Rectangle(new Size(strokePadding));

		function addPoint(point) {
			bounds = bounds.include(point);
		}

		function addRound(segment) {
			bounds = bounds.unite(
					joinBounds.setCenter(segment._point.transform(matrix)));
		}

		function addJoin(segment, join) {
			if (join === 'round' || segment.isSmooth()) {
				addRound(segment);
			} else {
				Path._addBevelJoin(segment, join, strokeRadius, miterLimit,
						matrix, strokeMatrix, addPoint);
			}
		}

		function addCap(segment, cap) {
			if (cap === 'round') {
				addRound(segment);
			} else {
				Path._addSquareCap(segment, cap, strokeRadius, matrix,
						strokeMatrix, addPoint);
			}
		}

		var length = segments.length - (closed ? 0 : 1);
		for (var i = 1; i < length; i++)
			addJoin(segments[i], join);
		if (closed) {
			addJoin(segments[0], join);
		} else if (length > 0) {
			addCap(segments[0], cap);
			addCap(segments[segments.length - 1], cap);
		}
		return bounds;
	},

	_getStrokePadding: function(radius, matrix) {
		if (!matrix)
			return [radius, radius];
		var hor = new Point(radius, 0).transform(matrix),
			ver = new Point(0, radius).transform(matrix),
			phi = hor.getAngleInRadians(),
			a = hor.getLength(),
			b = ver.getLength();
		var sin = Math.sin(phi),
			cos = Math.cos(phi),
			tan = Math.tan(phi),
			tx = Math.atan2(b * tan, a),
			ty = Math.atan2(b, tan * a);
		return [Math.abs(a * Math.cos(tx) * cos + b * Math.sin(tx) * sin),
				Math.abs(b * Math.sin(ty) * cos + a * Math.cos(ty) * sin)];
	},

	_addBevelJoin: function(segment, join, radius, miterLimit, matrix,
			strokeMatrix, addPoint, isArea) {
		var curve2 = segment.getCurve(),
			curve1 = curve2.getPrevious(),
			point = curve2.getPoint1().transform(matrix),
			normal1 = curve1.getNormalAtTime(1).multiply(radius)
				.transform(strokeMatrix),
			normal2 = curve2.getNormalAtTime(0).multiply(radius)
				.transform(strokeMatrix);
		if (normal1.getDirectedAngle(normal2) < 0) {
			normal1 = normal1.negate();
			normal2 = normal2.negate();
		}
		if (isArea)
			addPoint(point);
		addPoint(point.add(normal1));
		if (join === 'miter') {
			var corner = new Line(point.add(normal1),
					new Point(-normal1.y, normal1.x), true
				).intersect(new Line(point.add(normal2),
					new Point(-normal2.y, normal2.x), true
				), true);
			if (corner && point.getDistance(corner) <= miterLimit * radius) {
				addPoint(corner);
			}
		}
		addPoint(point.add(normal2));
	},

	_addSquareCap: function(segment, cap, radius, matrix, strokeMatrix,
			addPoint, isArea) {
		var point = segment._point.transform(matrix),
			loc = segment.getLocation(),
			normal = loc.getNormal()
					.multiply(loc.getTime() === 0 ? radius : -radius)
					.transform(strokeMatrix);
		if (cap === 'square') {
			if (isArea) {
				addPoint(point.subtract(normal));
				addPoint(point.add(normal));
			}
			point = point.add(normal.rotate(-90));
		}
		addPoint(point.add(normal));
		addPoint(point.subtract(normal));
	},

	getHandleBounds: function(segments, closed, path, matrix, options) {
		var style = path.getStyle(),
			stroke = options.stroke && style.hasStroke(),
			strokePadding,
			joinPadding;
		if (stroke) {
			var strokeMatrix = path._getStrokeMatrix(matrix, options),
				strokeRadius = style.getStrokeWidth() / 2,
				joinRadius = strokeRadius;
			if (style.getStrokeJoin() === 'miter')
				joinRadius = strokeRadius * style.getMiterLimit();
			if (style.getStrokeCap() === 'square')
				joinRadius = Math.max(joinRadius, strokeRadius * Math.SQRT2);
			strokePadding = Path._getStrokePadding(strokeRadius, strokeMatrix);
			joinPadding = Path._getStrokePadding(joinRadius, strokeMatrix);
		}
		var coords = new Array(6),
			x1 = Infinity,
			x2 = -x1,
			y1 = x1,
			y2 = x2;
		for (var i = 0, l = segments.length; i < l; i++) {
			var segment = segments[i];
			segment._transformCoordinates(matrix, coords);
			for (var j = 0; j < 6; j += 2) {
				var padding = !j ? joinPadding : strokePadding,
					paddingX = padding ? padding[0] : 0,
					paddingY = padding ? padding[1] : 0,
					x = coords[j],
					y = coords[j + 1],
					xn = x - paddingX,
					xx = x + paddingX,
					yn = y - paddingY,
					yx = y + paddingY;
				if (xn < x1) x1 = xn;
				if (xx > x2) x2 = xx;
				if (yn < y1) y1 = yn;
				if (yx > y2) y2 = yx;
			}
		}
		return new Rectangle(x1, y1, x2 - x1, y2 - y1);
	}
}});

Path.inject({ statics: new function() {

	var kappa = 0.5522847498307936,
		ellipseSegments = [
			new Segment([-1, 0], [0, kappa ], [0, -kappa]),
			new Segment([0, -1], [-kappa, 0], [kappa, 0 ]),
			new Segment([1, 0], [0, -kappa], [0, kappa ]),
			new Segment([0, 1], [kappa, 0 ], [-kappa, 0])
		];

	function createPath(segments, closed, args) {
		var props = Base.getNamed(args),
			path = new Path(props && props.insert == false && Item.NO_INSERT);
		path._add(segments);
		path._closed = closed;
		return path.set(props, { insert: true });
	}

	function createEllipse(center, radius, args) {
		var segments = new Array(4);
		for (var i = 0; i < 4; i++) {
			var segment = ellipseSegments[i];
			segments[i] = new Segment(
				segment._point.multiply(radius).add(center),
				segment._handleIn.multiply(radius),
				segment._handleOut.multiply(radius)
			);
		}
		return createPath(segments, true, args);
	}

	return {
		Line: function() {
			return createPath([
				new Segment(Point.readNamed(arguments, 'from')),
				new Segment(Point.readNamed(arguments, 'to'))
			], false, arguments);
		},

		Circle: function() {
			var center = Point.readNamed(arguments, 'center'),
				radius = Base.readNamed(arguments, 'radius');
			return createEllipse(center, new Size(radius), arguments);
		},

		Rectangle: function() {
			var rect = Rectangle.readNamed(arguments, 'rectangle'),
				radius = Size.readNamed(arguments, 'radius', 0,
						{ readNull: true }),
				bl = rect.getBottomLeft(true),
				tl = rect.getTopLeft(true),
				tr = rect.getTopRight(true),
				br = rect.getBottomRight(true),
				segments;
			if (!radius || radius.isZero()) {
				segments = [
					new Segment(bl),
					new Segment(tl),
					new Segment(tr),
					new Segment(br)
				];
			} else {
				radius = Size.min(radius, rect.getSize(true).divide(2));
				var rx = radius.width,
					ry = radius.height,
					hx = rx * kappa,
					hy = ry * kappa;
				segments = [
					new Segment(bl.add(rx, 0), null, [-hx, 0]),
					new Segment(bl.subtract(0, ry), [0, hy]),
					new Segment(tl.add(0, ry), null, [0, -hy]),
					new Segment(tl.add(rx, 0), [-hx, 0], null),
					new Segment(tr.subtract(rx, 0), null, [hx, 0]),
					new Segment(tr.add(0, ry), [0, -hy], null),
					new Segment(br.subtract(0, ry), null, [0, hy]),
					new Segment(br.subtract(rx, 0), [hx, 0])
				];
			}
			return createPath(segments, true, arguments);
		},

		RoundRectangle: '#Rectangle',

		Ellipse: function() {
			var ellipse = Shape._readEllipse(arguments);
			return createEllipse(ellipse.center, ellipse.radius, arguments);
		},

		Oval: '#Ellipse',

		Arc: function() {
			var from = Point.readNamed(arguments, 'from'),
				through = Point.readNamed(arguments, 'through'),
				to = Point.readNamed(arguments, 'to'),
				props = Base.getNamed(arguments),
				path = new Path(props && props.insert == false
						&& Item.NO_INSERT);
			path.moveTo(from);
			path.arcTo(through, to);
			return path.set(props);
		},

		RegularPolygon: function() {
			var center = Point.readNamed(arguments, 'center'),
				sides = Base.readNamed(arguments, 'sides'),
				radius = Base.readNamed(arguments, 'radius'),
				step = 360 / sides,
				three = sides % 3 === 0,
				vector = new Point(0, three ? -radius : radius),
				offset = three ? -1 : 0.5,
				segments = new Array(sides);
			for (var i = 0; i < sides; i++)
				segments[i] = new Segment(center.add(
					vector.rotate((i + offset) * step)));
			return createPath(segments, true, arguments);
		},

		Star: function() {
			var center = Point.readNamed(arguments, 'center'),
				points = Base.readNamed(arguments, 'points') * 2,
				radius1 = Base.readNamed(arguments, 'radius1'),
				radius2 = Base.readNamed(arguments, 'radius2'),
				step = 360 / points,
				vector = new Point(0, -1),
				segments = new Array(points);
			for (var i = 0; i < points; i++)
				segments[i] = new Segment(center.add(vector.rotate(step * i)
						.multiply(i % 2 ? radius2 : radius1)));
			return createPath(segments, true, arguments);
		}
	};
}});

var CompoundPath = PathItem.extend({
	_class: 'CompoundPath',
	_serializeFields: {
		children: []
	},
	beans: true,

	initialize: function CompoundPath(arg) {
		this._children = [];
		this._namedChildren = {};
		if (!this._initialize(arg)) {
			if (typeof arg === 'string') {
				this.setPathData(arg);
			} else {
				this.addChildren(Array.isArray(arg) ? arg : arguments);
			}
		}
	},

	insertChildren: function insertChildren(index, items) {
		var list = items,
			first = list[0];
		if (first && typeof first[0] === 'number')
			list = [list];
		for (var i = items.length - 1; i >= 0; i--) {
			var item = list[i];
			if (list === items && !(item instanceof Path))
				list = Base.slice(list);
			if (Array.isArray(item)) {
				list[i] = new Path({ segments: item, insert: false });
			} else if (item instanceof CompoundPath) {
				list.splice.apply(list, [i, 1].concat(item.removeChildren()));
				item.remove();
			}
		}
		return insertChildren.base.call(this, index, list);
	},

	reduce: function reduce(options) {
		var children = this._children;
		for (var i = children.length - 1; i >= 0; i--) {
			var path = children[i].reduce(options);
			if (path.isEmpty())
				path.remove();
		}
		if (!children.length) {
			var path = new Path(Item.NO_INSERT);
			path.copyAttributes(this);
			path.insertAbove(this);
			this.remove();
			return path;
		}
		return reduce.base.call(this);
	},

	isClosed: function() {
		var children = this._children;
		for (var i = 0, l = children.length; i < l; i++) {
			if (!children[i]._closed)
				return false;
		}
		return true;
	},

	setClosed: function(closed) {
		var children = this._children;
		for (var i = 0, l = children.length; i < l; i++) {
			children[i].setClosed(closed);
		}
	},

	getFirstSegment: function() {
		var first = this.getFirstChild();
		return first && first.getFirstSegment();
	},

	getLastSegment: function() {
		var last = this.getLastChild();
		return last && last.getLastSegment();
	},

	getCurves: function() {
		var children = this._children,
			curves = [];
		for (var i = 0, l = children.length; i < l; i++) {
			Base.push(curves, children[i].getCurves());
		}
		return curves;
	},

	getFirstCurve: function() {
		var first = this.getFirstChild();
		return first && first.getFirstCurve();
	},

	getLastCurve: function() {
		var last = this.getLastChild();
		return last && last.getLastCurve();
	},

	getArea: function() {
		var children = this._children,
			area = 0;
		for (var i = 0, l = children.length; i < l; i++)
			area += children[i].getArea();
		return area;
	},

	getLength: function() {
		var children = this._children,
			length = 0;
		for (var i = 0, l = children.length; i < l; i++)
			length += children[i].getLength();
		return length;
	},

	getPathData: function(_matrix, _precision) {
		var children = this._children,
			paths = [];
		for (var i = 0, l = children.length; i < l; i++) {
			var child = children[i],
				mx = child._matrix;
			paths.push(child.getPathData(_matrix && !mx.isIdentity()
					? _matrix.appended(mx) : _matrix, _precision));
		}
		return paths.join('');
	},

	_hitTestChildren: function _hitTestChildren(point, options, viewMatrix) {
		return _hitTestChildren.base.call(this, point,
				options.class === Path || options.type === 'path' ? options
					: Base.set({}, options, { fill: false }),
				viewMatrix);
	},

	_draw: function(ctx, param, viewMatrix, strokeMatrix) {
		var children = this._children;
		if (!children.length)
			return;

		param = param.extend({ dontStart: true, dontFinish: true });
		ctx.beginPath();
		for (var i = 0, l = children.length; i < l; i++)
			children[i].draw(ctx, param, strokeMatrix);

		if (!param.clip) {
			this._setStyles(ctx, param, viewMatrix);
			var style = this._style;
			if (style.hasFill()) {
				ctx.fill(style.getFillRule());
				ctx.shadowColor = 'rgba(0,0,0,0)';
			}
			if (style.hasStroke())
				ctx.stroke();
		}
	},

	_drawSelected: function(ctx, matrix, selectionItems) {
		var children = this._children;
		for (var i = 0, l = children.length; i < l; i++) {
			var child = children[i],
				mx = child._matrix;
			if (!selectionItems[child._id]) {
				child._drawSelected(ctx, mx.isIdentity() ? matrix
						: matrix.appended(mx));
			}
		}
	}
},
new function() {
	function getCurrentPath(that, check) {
		var children = that._children;
		if (check && !children.length)
			throw new Error('Use a moveTo() command first');
		return children[children.length - 1];
	}

	return Base.each(['lineTo', 'cubicCurveTo', 'quadraticCurveTo', 'curveTo',
			'arcTo', 'lineBy', 'cubicCurveBy', 'quadraticCurveBy', 'curveBy',
			'arcBy'],
		function(key) {
			this[key] = function() {
				var path = getCurrentPath(this, true);
				path[key].apply(path, arguments);
			};
		}, {
			moveTo: function() {
				var current = getCurrentPath(this),
					path = current && current.isEmpty() ? current
							: new Path(Item.NO_INSERT);
				if (path !== current)
					this.addChild(path);
				path.moveTo.apply(path, arguments);
			},

			moveBy: function() {
				var current = getCurrentPath(this, true),
					last = current && current.getLastSegment(),
					point = Point.read(arguments);
				this.moveTo(last ? point.add(last._point) : point);
			},

			closePath: function(tolerance) {
				getCurrentPath(this, true).closePath(tolerance);
			}
		}
	);
}, Base.each(['reverse', 'flatten', 'simplify', 'smooth'], function(key) {
	this[key] = function(param) {
		var children = this._children,
			res;
		for (var i = 0, l = children.length; i < l; i++) {
			res = children[i][key](param) || res;
		}
		return res;
	};
}, {}));

PathItem.inject(new function() {
	var min = Math.min,
		max = Math.max,
		abs = Math.abs,
		operators = {
			unite:     { '1': true, '2': true },
			intersect: { '2': true },
			subtract:  { '1': true },
			exclude:   { '1': true, '-1': true }
		};

	function preparePath(path, resolve) {
		var res = path.clone(false).reduce({ simplify: true })
				.transform(null, true, true);
		return resolve
				? res.resolveCrossings().reorient(
					res.getFillRule() === 'nonzero', true)
				: res;
	}

	function createResult(paths, simplify, path1, path2, options) {
		var result = new CompoundPath(Item.NO_INSERT);
		result.addChildren(paths, true);
		result = result.reduce({ simplify: simplify });
		if (!(options && options.insert == false)) {
			result.insertAbove(path2 && path1.isSibling(path2)
					&& path1.getIndex() < path2.getIndex() ? path2 : path1);
		}
		result.copyAttributes(path1, true);
		return result;
	}

	function traceBoolean(path1, path2, operation, options) {
		if (options && (options.trace == false || options.stroke) &&
				/^(subtract|intersect)$/.test(operation))
			return splitBoolean(path1, path2, operation);
		var _path1 = preparePath(path1, true),
			_path2 = path2 && path1 !== path2 && preparePath(path2, true),
			operator = operators[operation];
		operator[operation] = true;
		if (_path2 && (operator.subtract || operator.exclude)
				^ (_path2.isClockwise() ^ _path1.isClockwise()))
			_path2.reverse();
		var crossings = divideLocations(
				CurveLocation.expand(_path1.getCrossings(_path2))),
			paths1 = _path1._children || [_path1],
			paths2 = _path2 && (_path2._children || [_path2]),
			segments = [],
			curves = [],
			paths;

		function collect(paths) {
			for (var i = 0, l = paths.length; i < l; i++) {
				var path = paths[i];
				Base.push(segments, path._segments);
				Base.push(curves, path.getCurves());
				path._overlapsOnly = true;
			}
		}

		if (crossings.length) {
			collect(paths1);
			if (paths2)
				collect(paths2);
			for (var i = 0, l = crossings.length; i < l; i++) {
				propagateWinding(crossings[i]._segment, _path1, _path2, curves,
						operator);
			}
			for (var i = 0, l = segments.length; i < l; i++) {
				var segment = segments[i],
					inter = segment._intersection;
				if (!segment._winding) {
					propagateWinding(segment, _path1, _path2, curves, operator);
				}
				if (!(inter && inter._overlap))
					segment._path._overlapsOnly = false;
			}
			paths = tracePaths(segments, operator);
		} else {
			paths = reorientPaths(
					paths2 ? paths1.concat(paths2) : paths1.slice(),
					function(w) {
						return !!operator[w];
					});
		}

		return createResult(paths, true, path1, path2, options);
	}

	function splitBoolean(path1, path2, operation) {
		var _path1 = preparePath(path1),
			_path2 = preparePath(path2),
			crossings = _path1.getCrossings(_path2),
			subtract = operation === 'subtract',
			divide = operation === 'divide',
			added = {},
			paths = [];

		function addPath(path) {
			if (!added[path._id] && (divide ||
					_path2.contains(path.getPointAt(path.getLength() / 2))
						^ subtract)) {
				paths.unshift(path);
				return added[path._id] = true;
			}
		}

		for (var i = crossings.length - 1; i >= 0; i--) {
			var path = crossings[i].split();
			if (path) {
				if (addPath(path))
					path.getFirstSegment().setHandleIn(0, 0);
				_path1.getLastSegment().setHandleOut(0, 0);
			}
		}
		addPath(_path1);
		return createResult(paths, false, path1, path2);
	}

	function linkIntersections(from, to) {
		var prev = from;
		while (prev) {
			if (prev === to)
				return;
			prev = prev._previous;
		}
		while (from._next && from._next !== to)
			from = from._next;
		if (!from._next) {
			while (to._previous)
				to = to._previous;
			from._next = to;
			to._previous = from;
		}
	}

	function clearCurveHandles(curves) {
		for (var i = curves.length - 1; i >= 0; i--)
			curves[i].clearHandles();
	}

	function reorientPaths(paths, isInside, clockwise) {
		var length = paths && paths.length;
		if (length) {
			var lookup = Base.each(paths, function (path, i) {
					this[path._id] = {
						container: null,
						winding: path.isClockwise() ? 1 : -1,
						index: i
					};
				}, {}),
				sorted = paths.slice().sort(function (a, b) {
					return abs(b.getArea()) - abs(a.getArea());
				}),
				first = sorted[0];
			if (clockwise == null)
				clockwise = first.isClockwise();
			for (var i = 0; i < length; i++) {
				var path1 = sorted[i],
					entry1 = lookup[path1._id],
					point = path1.getInteriorPoint(),
					containerWinding = 0;
				for (var j = i - 1; j >= 0; j--) {
					var path2 = sorted[j];
					if (path2.contains(point)) {
						var entry2 = lookup[path2._id];
						containerWinding = entry2.winding;
						entry1.winding += containerWinding;
						entry1.container = entry2.exclude ? entry2.container
								: path2;
						break;
					}
				}
				if (isInside(entry1.winding) === isInside(containerWinding)) {
					entry1.exclude = true;
					paths[entry1.index] = null;
				} else {
					var container = entry1.container;
					path1.setClockwise(container ? !container.isClockwise()
							: clockwise);
				}
			}
		}
		return paths;
	}

	function divideLocations(locations, include, clearLater) {
		var results = include && [],
			tMin = 1e-8,
			tMax = 1 - tMin,
			clearHandles = false,
			clearCurves = clearLater || [],
			clearLookup = clearLater && {},
			renormalizeLocs,
			prevCurve,
			prevTime;

		function getId(curve) {
			return curve._path._id + '.' + curve._segment1._index;
		}

		for (var i = (clearLater && clearLater.length) - 1; i >= 0; i--) {
			var curve = clearLater[i];
			if (curve._path)
				clearLookup[getId(curve)] = true;
		}

		for (var i = locations.length - 1; i >= 0; i--) {
			var loc = locations[i],
				time = loc._time,
				origTime = time,
				exclude = include && !include(loc),
				curve = loc._curve,
				segment;
			if (curve) {
				if (curve !== prevCurve) {
					clearHandles = !curve.hasHandles()
							|| clearLookup && clearLookup[getId(curve)];
					renormalizeLocs = [];
					prevTime = null;
					prevCurve = curve;
				} else if (prevTime >= tMin) {
					time /= prevTime;
				}
			}
			if (exclude) {
				if (renormalizeLocs)
					renormalizeLocs.push(loc);
				continue;
			} else if (include) {
				results.unshift(loc);
			}
			prevTime = origTime;
			if (time < tMin) {
				segment = curve._segment1;
			} else if (time > tMax) {
				segment = curve._segment2;
			} else {
				var newCurve = curve.divideAtTime(time, true);
				if (clearHandles)
					clearCurves.push(curve, newCurve);
				segment = newCurve._segment1;
				for (var j = renormalizeLocs.length - 1; j >= 0; j--) {
					var l = renormalizeLocs[j];
					l._time = (l._time - time) / (1 - time);
				}
			}
			loc._setSegment(segment);
			var inter = segment._intersection,
				dest = loc._intersection;
			if (inter) {
				linkIntersections(inter, dest);
				var other = inter;
				while (other) {
					linkIntersections(other._intersection, inter);
					other = other._next;
				}
			} else {
				segment._intersection = dest;
			}
		}
		if (!clearLater)
			clearCurveHandles(clearCurves);
		return results || locations;
	}

	function getWinding(point, curves, dir, closed, dontFlip) {
		var ia = dir ? 1 : 0,
			io = ia ^ 1,
			pv = [point.x, point.y],
			pa = pv[ia],
			po = pv[io],
			windingEpsilon = 1e-9,
			qualityEpsilon = 1e-6,
			paL = pa - windingEpsilon,
			paR = pa + windingEpsilon,
			windingL = 0,
			windingR = 0,
			pathWindingL = 0,
			pathWindingR = 0,
			onPath = false,
			onAnyPath = false,
			quality = 1,
			roots = [],
			vPrev,
			vClose;

		function addWinding(v) {
			var o0 = v[io + 0],
				o3 = v[io + 6];
			if (po < min(o0, o3) || po > max(o0, o3)) {
				return;
			}
			var a0 = v[ia + 0],
				a1 = v[ia + 2],
				a2 = v[ia + 4],
				a3 = v[ia + 6];
			if (o0 === o3) {
				if (a0 < paR && a3 > paL || a3 < paR && a0 > paL) {
					onPath = true;
				}
				return;
			}
			var t =   po === o0 ? 0
					: po === o3 ? 1
					: paL > max(a0, a1, a2, a3) || paR < min(a0, a1, a2, a3)
					? 1
					: Curve.solveCubic(v, io, po, roots, 0, 1) > 0
						? roots[0]
						: 1,
				a =   t === 0 ? a0
					: t === 1 ? a3
					: Curve.getPoint(v, t)[dir ? 'y' : 'x'],
				winding = o0 > o3 ? 1 : -1,
				windingPrev = vPrev[io] > vPrev[io + 6] ? 1 : -1,
				a3Prev = vPrev[ia + 6];
			if (po !== o0) {
				if (a < paL) {
					pathWindingL += winding;
				} else if (a > paR) {
					pathWindingR += winding;
				} else {
					onPath = true;
				}
				if (a > pa - qualityEpsilon && a < pa + qualityEpsilon)
					quality /= 2;
			} else {
				if (winding !== windingPrev) {
					if (a0 < paL) {
						pathWindingL += winding;
					} else if (a0 > paR) {
						pathWindingR += winding;
					}
				} else if (a0 != a3Prev) {
					if (a3Prev < paR && a > paR) {
						pathWindingR += winding;
						onPath = true;
					} else if (a3Prev > paL && a < paL) {
						pathWindingL += winding;
						onPath = true;
					}
				}
				quality = 0;
			}
			vPrev = v;
			return !dontFlip && a > paL && a < paR
					&& Curve.getTangent(v, t)[dir ? 'x' : 'y'] === 0
					&& getWinding(point, curves, !dir, closed, true);
		}

		function handleCurve(v) {
			var o0 = v[io + 0],
				o1 = v[io + 2],
				o2 = v[io + 4],
				o3 = v[io + 6];
			if (po <= max(o0, o1, o2, o3) && po >= min(o0, o1, o2, o3)) {
				var a0 = v[ia + 0],
					a1 = v[ia + 2],
					a2 = v[ia + 4],
					a3 = v[ia + 6],
					monoCurves = paL > max(a0, a1, a2, a3) ||
								 paR < min(a0, a1, a2, a3)
							? [v] : Curve.getMonoCurves(v, dir),
					res;
				for (var i = 0, l = monoCurves.length; i < l; i++) {
					if (res = addWinding(monoCurves[i]))
						return res;
				}
			}
		}

		for (var i = 0, l = curves.length; i < l; i++) {
			var curve = curves[i],
				path = curve._path,
				v = curve.getValues(),
				res;
			if (!i || curves[i - 1]._path !== path) {
				vPrev = null;
				if (!path._closed) {
					vClose = Curve.getValues(
							path.getLastCurve().getSegment2(),
							curve.getSegment1(),
							null, !closed);
					if (vClose[io] !== vClose[io + 6]) {
						vPrev = vClose;
					}
				}

				if (!vPrev) {
					vPrev = v;
					var prev = path.getLastCurve();
					while (prev && prev !== curve) {
						var v2 = prev.getValues();
						if (v2[io] !== v2[io + 6]) {
							vPrev = v2;
							break;
						}
						prev = prev.getPrevious();
					}
				}
			}

			if (res = handleCurve(v))
				return res;

			if (i + 1 === l || curves[i + 1]._path !== path) {
				if (vClose && (res = handleCurve(vClose)))
					return res;
				if (onPath && !pathWindingL && !pathWindingR) {
					pathWindingL = pathWindingR = path.isClockwise(closed) ^ dir
							? 1 : -1;
				}
				windingL += pathWindingL;
				windingR += pathWindingR;
				pathWindingL = pathWindingR = 0;
				if (onPath) {
					onAnyPath = true;
					onPath = false;
				}
				vClose = null;
			}
		}
		windingL = abs(windingL);
		windingR = abs(windingR);
		return {
			winding: max(windingL, windingR),
			windingL: windingL,
			windingR: windingR,
			quality: quality,
			onPath: onAnyPath
		};
	}

	function propagateWinding(segment, path1, path2, curves, operator) {
		var chain = [],
			start = segment,
			totalLength = 0,
			winding;
		do {
			var curve = segment.getCurve(),
				length = curve.getLength();
			chain.push({ segment: segment, curve: curve, length: length });
			totalLength += length;
			segment = segment.getNext();
		} while (segment && !segment._intersection && segment !== start);
		var offsets = [0.5, 0.25, 0.75],
			winding = { winding: 0, quality: -1 },
			tMin = 1e-8,
			tMax = 1 - tMin;
		for (var i = 0; i < offsets.length && winding.quality < 0.5; i++) {
			var length = totalLength * offsets[i];
			for (var j = 0, l = chain.length; j < l; j++) {
				var entry = chain[j],
					curveLength = entry.length;
				if (length <= curveLength) {
					var curve = entry.curve,
						path = curve._path,
						parent = path._parent,
						operand = parent instanceof CompoundPath ? parent : path,
						t = Numerical.clamp(curve.getTimeAt(length), tMin, tMax),
						pt = curve.getPointAtTime(t),
						dir = abs(curve.getTangentAtTime(t).y) < Math.SQRT1_2;
					var wind = null;
					if (operator.subtract && path2) {
						var pathWinding = operand === path1
										  ? path2._getWinding(pt, dir, true)
										  : path1._getWinding(pt, dir, true);
						if (operand === path1 && pathWinding.winding ||
							operand === path2 && !pathWinding.winding) {
							if (pathWinding.quality < 1) {
								continue;
							} else {
								wind = { winding: 0, quality: 1 };
							}
						}
					}
					wind = wind || getWinding(pt, curves, dir, true);
					if (wind.quality > winding.quality)
						winding = wind;
					break;
				}
				length -= curveLength;
			}
		}
		for (var j = chain.length - 1; j >= 0; j--) {
			chain[j].segment._winding = winding;
		}
	}

	function tracePaths(segments, operator) {
		var paths = [],
			starts;

		function isValid(seg) {
			var winding;
			return !!(seg && !seg._visited && (!operator
					|| operator[(winding = seg._winding || {}).winding]
						&& !(operator.unite && winding.winding === 2
							&& winding.windingL && winding.windingR)));
		}

		function isStart(seg) {
			if (seg) {
				for (var i = 0, l = starts.length; i < l; i++) {
					if (seg === starts[i])
						return true;
				}
			}
			return false;
		}

		function visitPath(path) {
			var segments = path._segments;
			for (var i = 0, l = segments.length; i < l; i++) {
				segments[i]._visited = true;
			}
		}

		function getCrossingSegments(segment, collectStarts) {
			var inter = segment._intersection,
				start = inter,
				crossings = [];
			if (collectStarts)
				starts = [segment];

			function collect(inter, end) {
				while (inter && inter !== end) {
					var other = inter._segment,
						path = other && other._path;
					if (path) {
						var next = other.getNext() || path.getFirstSegment(),
							nextInter = next._intersection;
						if (other !== segment && (isStart(other)
							|| isStart(next)
							|| next && (isValid(other) && (isValid(next)
								|| nextInter && isValid(nextInter._segment))))
						) {
							crossings.push(other);
						}
						if (collectStarts)
							starts.push(other);
					}
					inter = inter._next;
				}
			}

			if (inter) {
				collect(inter);
				while (inter && inter._prev)
					inter = inter._prev;
				collect(inter, start);
			}
			return crossings;
		}

		segments.sort(function(seg1, seg2) {
			var inter1 = seg1._intersection,
				inter2 = seg2._intersection,
				over1 = !!(inter1 && inter1._overlap),
				over2 = !!(inter2 && inter2._overlap),
				path1 = seg1._path,
				path2 = seg2._path;
			return over1 ^ over2
					? over1 ? 1 : -1
					: !inter1 ^ !inter2
						? inter1 ? 1 : -1
						: path1 !== path2
							? path1._id - path2._id
							: seg1._index - seg2._index;
		});

		for (var i = 0, l = segments.length; i < l; i++) {
			var seg = segments[i],
				valid = isValid(seg),
				path = null,
				finished = false,
				closed = true,
				branches = [],
				branch,
				visited,
				handleIn;
			if (valid && seg._path._overlapsOnly) {
				var path1 = seg._path,
					path2 = seg._intersection._segment._path;
				if (path1.compare(path2)) {
					if (path1.getArea())
						paths.push(path1.clone(false));
					visitPath(path1);
					visitPath(path2);
					valid = false;
				}
			}
			while (valid) {
				var first = !path,
					crossings = getCrossingSegments(seg, first),
					other = crossings.shift(),
					finished = !first && (isStart(seg) || isStart(other)),
					cross = !finished && other;
				if (first) {
					path = new Path(Item.NO_INSERT);
					branch = null;
				}
				if (finished) {
					if (seg.isFirst() || seg.isLast())
						closed = seg._path._closed;
					seg._visited = true;
					break;
				}
				if (cross && branch) {
					branches.push(branch);
					branch = null;
				}
				if (!branch) {
					if (cross)
						crossings.push(seg);
					branch = {
						start: path._segments.length,
						crossings: crossings,
						visited: visited = [],
						handleIn: handleIn
					};
				}
				if (cross)
					seg = other;
				if (!isValid(seg)) {
					path.removeSegments(branch.start);
					for (var j = 0, k = visited.length; j < k; j++) {
						visited[j]._visited = false;
					}
					visited.length = 0;
					do {
						seg = branch && branch.crossings.shift();
						if (!seg || !seg._path) {
							seg = null;
							branch = branches.pop();
							if (branch) {
								visited = branch.visited;
								handleIn = branch.handleIn;
							}
						}
					} while (branch && !isValid(seg));
					if (!seg)
						break;
				}
				var next = seg.getNext();
				path.add(new Segment(seg._point, handleIn,
						next && seg._handleOut));
				seg._visited = true;
				visited.push(seg);
				seg = next || seg._path.getFirstSegment();
				handleIn = next && next._handleIn;
			}
			if (finished) {
				if (closed) {
					path.getFirstSegment().setHandleIn(handleIn);
					path.setClosed(closed);
				}
				if (path.getArea() !== 0) {
					paths.push(path);
				}
			}
		}
		return paths;
	}

	return {
		_getWinding: function(point, dir, closed) {
			return getWinding(point, this.getCurves(), dir, closed);
		},

		unite: function(path, options) {
			return traceBoolean(this, path, 'unite', options);
		},

		intersect: function(path, options) {
			return traceBoolean(this, path, 'intersect', options);
		},

		subtract: function(path, options) {
			return traceBoolean(this, path, 'subtract', options);
		},

		exclude: function(path, options) {
			return traceBoolean(this, path, 'exclude', options);
		},

		divide: function(path, options) {
			return options && (options.trace == false || options.stroke)
					? splitBoolean(this, path, 'divide')
					: createResult([
						this.subtract(path, options),
						this.intersect(path, options)
					], true, this, path, options);
		},

		resolveCrossings: function() {
			var children = this._children,
				paths = children || [this];

			function hasOverlap(seg, path) {
				var inter = seg && seg._intersection;
				return inter && inter._overlap && inter._path === path;
			}

			var hasOverlaps = false,
				hasCrossings = false,
				intersections = this.getIntersections(null, function(inter) {
					return inter.hasOverlap() && (hasOverlaps = true) ||
							inter.isCrossing() && (hasCrossings = true);
				}),
				clearCurves = hasOverlaps && hasCrossings && [];
			intersections = CurveLocation.expand(intersections);
			if (hasOverlaps) {
				var overlaps = divideLocations(intersections, function(inter) {
					return inter.hasOverlap();
				}, clearCurves);
				for (var i = overlaps.length - 1; i >= 0; i--) {
					var overlap = overlaps[i],
						path = overlap._path,
						seg = overlap._segment,
						prev = seg.getPrevious(),
						next = seg.getNext();
					if (hasOverlap(prev, path) && hasOverlap(next, path)) {
						seg.remove();
						prev._handleOut._set(0, 0);
						next._handleIn._set(0, 0);
						if (prev !== seg && !prev.getCurve().hasLength()) {
							next._handleIn.set(prev._handleIn);
							prev.remove();
						}
					}
				}
			}
			if (hasCrossings) {
				divideLocations(intersections, hasOverlaps && function(inter) {
					var curve1 = inter.getCurve(),
						seg1 = inter.getSegment(),
						other = inter._intersection,
						curve2 = other._curve,
						seg2 = other._segment;
					if (curve1 && curve2 && curve1._path && curve2._path)
						return true;
					if (seg1)
						seg1._intersection = null;
					if (seg2)
						seg2._intersection = null;
				}, clearCurves);
				if (clearCurves)
					clearCurveHandles(clearCurves);
				paths = tracePaths(Base.each(paths, function(path) {
					Base.push(this, path._segments);
				}, []));
			}
			var length = paths.length,
				item;
			if (length > 1 && children) {
				if (paths !== children)
					this.setChildren(paths);
				item = this;
			} else if (length === 1 && !children) {
				if (paths[0] !== this)
					this.setSegments(paths[0].removeSegments());
				item = this;
			}
			if (!item) {
				item = new CompoundPath(Item.NO_INSERT);
				item.addChildren(paths);
				item = item.reduce();
				item.copyAttributes(this);
				this.replaceWith(item);
			}
			return item;
		},

		reorient: function(nonZero, clockwise) {
			var children = this._children;
			if (children && children.length) {
				this.setChildren(reorientPaths(this.removeChildren(),
						function(w) {
							return !!(nonZero ? w : w & 1);
						},
						clockwise));
			} else if (clockwise !== undefined) {
				this.setClockwise(clockwise);
			}
			return this;
		},

		getInteriorPoint: function() {
			var bounds = this.getBounds(),
				point = bounds.getCenter(true);
			if (!this.contains(point)) {
				var curves = this.getCurves(),
					y = point.y,
					intercepts = [],
					roots = [];
				for (var i = 0, l = curves.length; i < l; i++) {
					var v = curves[i].getValues(),
						o0 = v[1],
						o1 = v[3],
						o2 = v[5],
						o3 = v[7];
					if (y >= min(o0, o1, o2, o3) && y <= max(o0, o1, o2, o3)) {
						var monoCurves = Curve.getMonoCurves(v);
						for (var j = 0, m = monoCurves.length; j < m; j++) {
							var mv = monoCurves[j],
								mo0 = mv[1],
								mo3 = mv[7];
							if ((mo0 !== mo3) &&
								(y >= mo0 && y <= mo3 || y >= mo3 && y <= mo0)){
								var x = y === mo0 ? mv[0]
									: y === mo3 ? mv[6]
									: Curve.solveCubic(mv, 1, y, roots, 0, 1)
										=== 1
										? Curve.getPoint(mv, roots[0]).x
										: (mv[0] + mv[6]) / 2;
								intercepts.push(x);
							}
						}
					}
				}
				if (intercepts.length > 1) {
					intercepts.sort(function(a, b) { return a - b; });
					point.x = (intercepts[0] + intercepts[1]) / 2;
				}
			}
			return point;
		}
	};
});

var PathFlattener = Base.extend({
	_class: 'PathFlattener',

	initialize: function(path, flatness, maxRecursion, ignoreStraight, matrix) {
		var curves = [],
			parts = [],
			length = 0,
			minSpan = 1 / (maxRecursion || 32),
			segments = path._segments,
			segment1 = segments[0],
			segment2;

		function addCurve(segment1, segment2) {
			var curve = Curve.getValues(segment1, segment2, matrix);
			curves.push(curve);
			computeParts(curve, segment1._index, 0, 1);
		}

		function computeParts(curve, index, t1, t2) {
			if ((t2 - t1) > minSpan
					&& !(ignoreStraight && Curve.isStraight(curve))
					&& !Curve.isFlatEnough(curve, flatness || 0.25)) {
				var halves = Curve.subdivide(curve, 0.5),
					tMid = (t1 + t2) / 2;
				computeParts(halves[0], index, t1, tMid);
				computeParts(halves[1], index, tMid, t2);
			} else {
				var dx = curve[6] - curve[0],
					dy = curve[7] - curve[1],
					dist = Math.sqrt(dx * dx + dy * dy);
				if (dist > 0) {
					length += dist;
					parts.push({
						offset: length,
						curve: curve,
						index: index,
						time: t2,
					});
				}
			}
		}

		for (var i = 1, l = segments.length; i < l; i++) {
			segment2 = segments[i];
			addCurve(segment1, segment2);
			segment1 = segment2;
		}
		if (path._closed)
			addCurve(segment2 || segment1, segments[0]);
		this.curves = curves;
		this.parts = parts;
		this.length = length;
		this.index = 0;
	},

	_get: function(offset) {
		var parts = this.parts,
			length = parts.length,
			start,
			i, j = this.index;
		for (;;) {
			i = j;
			if (!j || parts[--j].offset < offset)
				break;
		}
		for (; i < length; i++) {
			var part = parts[i];
			if (part.offset >= offset) {
				this.index = i;
				var prev = parts[i - 1],
					prevTime = prev && prev.index === part.index ? prev.time : 0,
					prevOffset = prev ? prev.offset : 0;
				return {
					index: part.index,
					time: prevTime + (part.time - prevTime)
						* (offset - prevOffset) / (part.offset - prevOffset)
				};
			}
		}
		return {
			index: parts[length - 1].index,
			time: 1
		};
	},

	drawPart: function(ctx, from, to) {
		var start = this._get(from),
			end = this._get(to);
		for (var i = start.index, l = end.index; i <= l; i++) {
			var curve = Curve.getPart(this.curves[i],
					i === start.index ? start.time : 0,
					i === end.index ? end.time : 1);
			if (i === start.index)
				ctx.moveTo(curve[0], curve[1]);
			ctx.bezierCurveTo.apply(ctx, curve.slice(2));
		}
	}
}, Base.each(Curve._evaluateMethods,
	function(name) {
		this[name + 'At'] = function(offset) {
			var param = this._get(offset);
			return Curve[name](this.curves[param.index], param.time);
		};
	}, {})
);

var PathFitter = Base.extend({
	initialize: function(path) {
		var points = this.points = [],
			segments = path._segments,
			closed = path._closed;
		for (var i = 0, prev, l = segments.length; i < l; i++) {
			var point = segments[i].point;
			if (!prev || !prev.equals(point)) {
				points.push(prev = point.clone());
			}
		}
		if (closed) {
			points.unshift(points[points.length - 1]);
			points.push(points[1]);
		}
		this.closed = closed;
	},

	fit: function(error) {
		var points = this.points,
			length = points.length,
			segments = null;
		if (length > 0) {
			segments = [new Segment(points[0])];
			if (length > 1) {
				this.fitCubic(segments, error, 0, length - 1,
						points[1].subtract(points[0]),
						points[length - 2].subtract(points[length - 1]));
				if (this.closed) {
					segments.shift();
					segments.pop();
				}
			}
		}
		return segments;
	},

	fitCubic: function(segments, error, first, last, tan1, tan2) {
		var points = this.points;
		if (last - first === 1) {
			var pt1 = points[first],
				pt2 = points[last],
				dist = pt1.getDistance(pt2) / 3;
			this.addCurve(segments, [pt1, pt1.add(tan1.normalize(dist)),
					pt2.add(tan2.normalize(dist)), pt2]);
			return;
		}
		var uPrime = this.chordLengthParameterize(first, last),
			maxError = Math.max(error, error * error),
			split,
			parametersInOrder = true;
		for (var i = 0; i <= 4; i++) {
			var curve = this.generateBezier(first, last, uPrime, tan1, tan2);
			var max = this.findMaxError(first, last, curve, uPrime);
			if (max.error < error && parametersInOrder) {
				this.addCurve(segments, curve);
				return;
			}
			split = max.index;
			if (max.error >= maxError)
				break;
			parametersInOrder = this.reparameterize(first, last, uPrime, curve);
			maxError = max.error;
		}
		var tanCenter = points[split - 1].subtract(points[split + 1]);
		this.fitCubic(segments, error, first, split, tan1, tanCenter);
		this.fitCubic(segments, error, split, last, tanCenter.negate(), tan2);
	},

	addCurve: function(segments, curve) {
		var prev = segments[segments.length - 1];
		prev.setHandleOut(curve[1].subtract(curve[0]));
		segments.push(new Segment(curve[3], curve[2].subtract(curve[3])));
	},

	generateBezier: function(first, last, uPrime, tan1, tan2) {
		var epsilon = 1e-12,
			abs = Math.abs,
			points = this.points,
			pt1 = points[first],
			pt2 = points[last],
			C = [[0, 0], [0, 0]],
			X = [0, 0];

		for (var i = 0, l = last - first + 1; i < l; i++) {
			var u = uPrime[i],
				t = 1 - u,
				b = 3 * u * t,
				b0 = t * t * t,
				b1 = b * t,
				b2 = b * u,
				b3 = u * u * u,
				a1 = tan1.normalize(b1),
				a2 = tan2.normalize(b2),
				tmp = points[first + i]
					.subtract(pt1.multiply(b0 + b1))
					.subtract(pt2.multiply(b2 + b3));
			C[0][0] += a1.dot(a1);
			C[0][1] += a1.dot(a2);
			C[1][0] = C[0][1];
			C[1][1] += a2.dot(a2);
			X[0] += a1.dot(tmp);
			X[1] += a2.dot(tmp);
		}

		var detC0C1 = C[0][0] * C[1][1] - C[1][0] * C[0][1],
			alpha1,
			alpha2;
		if (abs(detC0C1) > epsilon) {
			var detC0X = C[0][0] * X[1]    - C[1][0] * X[0],
				detXC1 = X[0]    * C[1][1] - X[1]    * C[0][1];
			alpha1 = detXC1 / detC0C1;
			alpha2 = detC0X / detC0C1;
		} else {
			var c0 = C[0][0] + C[0][1],
				c1 = C[1][0] + C[1][1];
			alpha1 = alpha2 = abs(c0) > epsilon ? X[0] / c0
							: abs(c1) > epsilon ? X[1] / c1
							: 0;
		}

		var segLength = pt2.getDistance(pt1),
			eps = epsilon * segLength,
			handle1,
			handle2;
		if (alpha1 < eps || alpha2 < eps) {
			alpha1 = alpha2 = segLength / 3;
		} else {
			var line = pt2.subtract(pt1);
			handle1 = tan1.normalize(alpha1);
			handle2 = tan2.normalize(alpha2);
			if (handle1.dot(line) - handle2.dot(line) > segLength * segLength) {
				alpha1 = alpha2 = segLength / 3;
				handle1 = handle2 = null;
			}
		}

		return [pt1,
				pt1.add(handle1 || tan1.normalize(alpha1)),
				pt2.add(handle2 || tan2.normalize(alpha2)),
				pt2];
	},

	reparameterize: function(first, last, u, curve) {
		for (var i = first; i <= last; i++) {
			u[i - first] = this.findRoot(curve, this.points[i], u[i - first]);
		}
		for (var i = 1, l = u.length; i < l; i++) {
			if (u[i] <= u[i - 1])
				return false;
		}
		return true;
	},

	findRoot: function(curve, point, u) {
		var curve1 = [],
			curve2 = [];
		for (var i = 0; i <= 2; i++) {
			curve1[i] = curve[i + 1].subtract(curve[i]).multiply(3);
		}
		for (var i = 0; i <= 1; i++) {
			curve2[i] = curve1[i + 1].subtract(curve1[i]).multiply(2);
		}
		var pt = this.evaluate(3, curve, u),
			pt1 = this.evaluate(2, curve1, u),
			pt2 = this.evaluate(1, curve2, u),
			diff = pt.subtract(point),
			df = pt1.dot(pt1) + diff.dot(pt2);
		return Numerical.isZero(df) ? u : u - diff.dot(pt1) / df;
	},

	evaluate: function(degree, curve, t) {
		var tmp = curve.slice();
		for (var i = 1; i <= degree; i++) {
			for (var j = 0; j <= degree - i; j++) {
				tmp[j] = tmp[j].multiply(1 - t).add(tmp[j + 1].multiply(t));
			}
		}
		return tmp[0];
	},

	chordLengthParameterize: function(first, last) {
		var u = [0];
		for (var i = first + 1; i <= last; i++) {
			u[i - first] = u[i - first - 1]
					+ this.points[i].getDistance(this.points[i - 1]);
		}
		for (var i = 1, m = last - first; i <= m; i++) {
			u[i] /= u[m];
		}
		return u;
	},

	findMaxError: function(first, last, curve, u) {
		var index = Math.floor((last - first + 1) / 2),
			maxDist = 0;
		for (var i = first + 1; i < last; i++) {
			var P = this.evaluate(3, curve, u[i - first]);
			var v = P.subtract(this.points[i]);
			var dist = v.x * v.x + v.y * v.y;
			if (dist >= maxDist) {
				maxDist = dist;
				index = i;
			}
		}
		return {
			error: maxDist,
			index: index
		};
	}
});

var TextItem = Item.extend({
	_class: 'TextItem',
	_applyMatrix: false,
	_canApplyMatrix: false,
	_serializeFields: {
		content: null
	},
	_boundsOptions: { stroke: false, handle: false },

	initialize: function TextItem(arg) {
		this._content = '';
		this._lines = [];
		var hasProps = arg && Base.isPlainObject(arg)
				&& arg.x === undefined && arg.y === undefined;
		this._initialize(hasProps && arg, !hasProps && Point.read(arguments));
	},

	_equals: function(item) {
		return this._content === item._content;
	},

	copyContent: function(source) {
		this.setContent(source._content);
	},

	getContent: function() {
		return this._content;
	},

	setContent: function(content) {
		this._content = '' + content;
		this._lines = this._content.split(/\r\n|\n|\r/mg);
		this._changed(521);
	},

	isEmpty: function() {
		return !this._content;
	},

	getCharacterStyle: '#getStyle',
	setCharacterStyle: '#setStyle',

	getParagraphStyle: '#getStyle',
	setParagraphStyle: '#setStyle'
});

var PointText = TextItem.extend({
	_class: 'PointText',

	initialize: function PointText() {
		TextItem.apply(this, arguments);
	},

	getPoint: function() {
		var point = this._matrix.getTranslation();
		return new LinkedPoint(point.x, point.y, this, 'setPoint');
	},

	setPoint: function() {
		var point = Point.read(arguments);
		this.translate(point.subtract(this._matrix.getTranslation()));
	},

	_draw: function(ctx, param, viewMatrix) {
		if (!this._content)
			return;
		this._setStyles(ctx, param, viewMatrix);
		var lines = this._lines,
			style = this._style,
			hasFill = style.hasFill(),
			hasStroke = style.hasStroke(),
			leading = style.getLeading(),
			shadowColor = ctx.shadowColor;
		ctx.font = style.getFontStyle();
		ctx.textAlign = style.getJustification();
		for (var i = 0, l = lines.length; i < l; i++) {
			ctx.shadowColor = shadowColor;
			var line = lines[i];
			if (hasFill) {
				ctx.fillText(line, 0, 0);
				ctx.shadowColor = 'rgba(0,0,0,0)';
			}
			if (hasStroke)
				ctx.strokeText(line, 0, 0);
			ctx.translate(0, leading);
		}
	},

	_getBounds: function(matrix, options) {
		var style = this._style,
			lines = this._lines,
			numLines = lines.length,
			justification = style.getJustification(),
			leading = style.getLeading(),
			width = this.getView().getTextWidth(style.getFontStyle(), lines),
			x = 0;
		if (justification !== 'left')
			x -= width / (justification === 'center' ? 2: 1);
		var rect = new Rectangle(x,
					numLines ? - 0.75 * leading : 0,
					width, numLines * leading);
		return matrix ? matrix._transformBounds(rect, rect) : rect;
	}
});

var Color = Base.extend(new function() {
	var types = {
		gray: ['gray'],
		rgb: ['red', 'green', 'blue'],
		hsb: ['hue', 'saturation', 'brightness'],
		hsl: ['hue', 'saturation', 'lightness'],
		gradient: ['gradient', 'origin', 'destination', 'highlight']
	};

	var componentParsers = {},
		namedColors = {
			transparent: [0, 0, 0, 0]
		},
		colorCtx;

	function fromCSS(string) {
		var match = string.match(
				/^#([\da-f]{2})([\da-f]{2})([\da-f]{2})([\da-f]{2})?$/i
			) || string.match(
				/^#([\da-f])([\da-f])([\da-f])([\da-f])?$/i
			),
			type = 'rgb',
			components;
		if (match) {
			var amount = match[4] ? 4 : 3;
			components = new Array(amount);
			for (var i = 0; i < amount; i++) {
				var value = match[i + 1];
				components[i] = parseInt(value.length == 1
						? value + value : value, 16) / 255;
			}
		} else if (match = string.match(/^(rgb|hsl)a?\((.*)\)$/)) {
			type = match[1];
			components = match[2].split(/[,\s]+/g);
			var isHSL = type === 'hsl';
			for (var i = 0, l = Math.min(components.length, 4); i < l; i++) {
				var component = components[i];
				var value = parseFloat(component);
				if (isHSL) {
					if (i === 0) {
						var unit = component.match(/([a-z]*)$/)[1];
						value *= ({
							turn: 360,
							rad: 180 / Math.PI,
							grad: 0.9
						}[unit] || 1);
					} else if (i < 3) {
						value /= 100;
					}
				} else if (i < 3) {
					value /= 255;
				}
				components[i] = value;
			}
		} else {
			var color = namedColors[string];
			if (!color) {
				if (window) {
					if (!colorCtx) {
						colorCtx = CanvasProvider.getContext(1, 1);
						colorCtx.globalCompositeOperation = 'copy';
					}
					colorCtx.fillStyle = 'rgba(0,0,0,0)';
					colorCtx.fillStyle = string;
					colorCtx.fillRect(0, 0, 1, 1);
					var data = colorCtx.getImageData(0, 0, 1, 1).data;
					color = namedColors[string] = [
						data[0] / 255,
						data[1] / 255,
						data[2] / 255
					];
				} else {
					color = [0, 0, 0];
				}
			}
			components = color.slice();
		}
		return [type, components];
	}

	var hsbIndices = [
		[0, 3, 1],
		[2, 0, 1],
		[1, 0, 3],
		[1, 2, 0],
		[3, 1, 0],
		[0, 1, 2]
	];

	var converters = {
		'rgb-hsb': function(r, g, b) {
			var max = Math.max(r, g, b),
				min = Math.min(r, g, b),
				delta = max - min,
				h = delta === 0 ? 0
					:   ( max == r ? (g - b) / delta + (g < b ? 6 : 0)
						: max == g ? (b - r) / delta + 2
						:            (r - g) / delta + 4) * 60;
			return [h, max === 0 ? 0 : delta / max, max];
		},

		'hsb-rgb': function(h, s, b) {
			h = (((h / 60) % 6) + 6) % 6;
			var i = Math.floor(h),
				f = h - i,
				i = hsbIndices[i],
				v = [
					b,
					b * (1 - s),
					b * (1 - s * f),
					b * (1 - s * (1 - f))
				];
			return [v[i[0]], v[i[1]], v[i[2]]];
		},

		'rgb-hsl': function(r, g, b) {
			var max = Math.max(r, g, b),
				min = Math.min(r, g, b),
				delta = max - min,
				achromatic = delta === 0,
				h = achromatic ? 0
					:   ( max == r ? (g - b) / delta + (g < b ? 6 : 0)
						: max == g ? (b - r) / delta + 2
						:            (r - g) / delta + 4) * 60,
				l = (max + min) / 2,
				s = achromatic ? 0 : l < 0.5
						? delta / (max + min)
						: delta / (2 - max - min);
			return [h, s, l];
		},

		'hsl-rgb': function(h, s, l) {
			h = (((h / 360) % 1) + 1) % 1;
			if (s === 0)
				return [l, l, l];
			var t3s = [ h + 1 / 3, h, h - 1 / 3 ],
				t2 = l < 0.5 ? l * (1 + s) : l + s - l * s,
				t1 = 2 * l - t2,
				c = [];
			for (var i = 0; i < 3; i++) {
				var t3 = t3s[i];
				if (t3 < 0) t3 += 1;
				if (t3 > 1) t3 -= 1;
				c[i] = 6 * t3 < 1
					? t1 + (t2 - t1) * 6 * t3
					: 2 * t3 < 1
						? t2
						: 3 * t3 < 2
							? t1 + (t2 - t1) * ((2 / 3) - t3) * 6
							: t1;
			}
			return c;
		},

		'rgb-gray': function(r, g, b) {
			return [r * 0.2989 + g * 0.587 + b * 0.114];
		},

		'gray-rgb': function(g) {
			return [g, g, g];
		},

		'gray-hsb': function(g) {
			return [0, 0, g];
		},

		'gray-hsl': function(g) {
			return [0, 0, g];
		},

		'gradient-rgb': function() {
			return [];
		},

		'rgb-gradient': function() {
			return [];
		}

	};

	return Base.each(types, function(properties, type) {
		componentParsers[type] = [];
		Base.each(properties, function(name, index) {
			var part = Base.capitalize(name),
				hasOverlap = /^(hue|saturation)$/.test(name),
				parser = componentParsers[type][index] = type === 'gradient'
					? name === 'gradient'
						? function(value) {
							var current = this._components[0];
							value = Gradient.read(
								Array.isArray(value)
									? value
									: arguments, 0, { readNull: true }
							);
							if (current !== value) {
								if (current)
									current._removeOwner(this);
								if (value)
									value._addOwner(this);
							}
							return value;
						}
						: function() {
							return Point.read(arguments, 0, {
									readNull: name === 'highlight',
									clone: true
							});
						}
					: function(value) {
						return value == null || isNaN(value) ? 0 : +value;
					};
			this['get' + part] = function() {
				return this._type === type
					|| hasOverlap && /^hs[bl]$/.test(this._type)
						? this._components[index]
						: this._convert(type)[index];
			};

			this['set' + part] = function(value) {
				if (this._type !== type
						&& !(hasOverlap && /^hs[bl]$/.test(this._type))) {
					this._components = this._convert(type);
					this._properties = types[type];
					this._type = type;
				}
				this._components[index] = parser.call(this, value);
				this._changed();
			};
		}, this);
	}, {
		_class: 'Color',
		_readIndex: true,

		initialize: function Color(arg) {
			var args = arguments,
				reading = this.__read,
				read = 0,
				type,
				components,
				alpha,
				values;
			if (Array.isArray(arg)) {
				args = arg;
				arg = args[0];
			}
			var argType = arg != null && typeof arg;
			if (argType === 'string' && arg in types) {
				type = arg;
				arg = args[1];
				if (Array.isArray(arg)) {
					components = arg;
					alpha = args[2];
				} else {
					if (reading)
						read = 1;
					args = Base.slice(args, 1);
					argType = typeof arg;
				}
			}
			if (!components) {
				values = argType === 'number'
						? args
						: argType === 'object' && arg.length != null
							? arg
							: null;
				if (values) {
					if (!type)
						type = values.length >= 3
								? 'rgb'
								: 'gray';
					var length = types[type].length;
					alpha = values[length];
					if (reading) {
						read += values === arguments
							? length + (alpha != null ? 1 : 0)
							: 1;
					}
					if (values.length > length)
						values = Base.slice(values, 0, length);
				} else if (argType === 'string') {
					var converted = fromCSS(arg);
					type = converted[0];
					components = converted[1];
					if (components.length === 4) {
						alpha = components[3];
						components.length--;
					}
				} else if (argType === 'object') {
					if (arg.constructor === Color) {
						type = arg._type;
						components = arg._components.slice();
						alpha = arg._alpha;
						if (type === 'gradient') {
							for (var i = 1, l = components.length; i < l; i++) {
								var point = components[i];
								if (point)
									components[i] = point.clone();
							}
						}
					} else if (arg.constructor === Gradient) {
						type = 'gradient';
						values = args;
					} else {
						type = 'hue' in arg
							? 'lightness' in arg
								? 'hsl'
								: 'hsb'
							: 'gradient' in arg || 'stops' in arg
									|| 'radial' in arg
								? 'gradient'
								: 'gray' in arg
									? 'gray'
									: 'rgb';
						var properties = types[type],
							parsers = componentParsers[type];
						this._components = components = [];
						for (var i = 0, l = properties.length; i < l; i++) {
							var value = arg[properties[i]];
							if (value == null && !i && type === 'gradient'
									&& 'stops' in arg) {
								value = {
									stops: arg.stops,
									radial: arg.radial
								};
							}
							value = parsers[i].call(this, value);
							if (value != null)
								components[i] = value;
						}
						alpha = arg.alpha;
					}
				}
				if (reading && type)
					read = 1;
			}
			this._type = type || 'rgb';
			if (!components) {
				this._components = components = [];
				var parsers = componentParsers[this._type];
				for (var i = 0, l = parsers.length; i < l; i++) {
					var value = parsers[i].call(this, values && values[i]);
					if (value != null)
						components[i] = value;
				}
			}
			this._components = components;
			this._properties = types[this._type];
			this._alpha = alpha;
			if (reading)
				this.__read = read;
			return this;
		},

		set: '#initialize',

		_serialize: function(options, dictionary) {
			var components = this.getComponents();
			return Base.serialize(
					/^(gray|rgb)$/.test(this._type)
						? components
						: [this._type].concat(components),
					options, true, dictionary);
		},

		_changed: function() {
			this._canvasStyle = null;
			if (this._owner)
				this._owner._changed(129);
		},

		_convert: function(type) {
			var converter;
			return this._type === type
					? this._components.slice()
					: (converter = converters[this._type + '-' + type])
						? converter.apply(this, this._components)
						: converters['rgb-' + type].apply(this,
							converters[this._type + '-rgb'].apply(this,
								this._components));
		},

		convert: function(type) {
			return new Color(type, this._convert(type), this._alpha);
		},

		getType: function() {
			return this._type;
		},

		setType: function(type) {
			this._components = this._convert(type);
			this._properties = types[type];
			this._type = type;
		},

		getComponents: function() {
			var components = this._components.slice();
			if (this._alpha != null)
				components.push(this._alpha);
			return components;
		},

		getAlpha: function() {
			return this._alpha != null ? this._alpha : 1;
		},

		setAlpha: function(alpha) {
			this._alpha = alpha == null ? null : Math.min(Math.max(alpha, 0), 1);
			this._changed();
		},

		hasAlpha: function() {
			return this._alpha != null;
		},

		equals: function(color) {
			var col = Base.isPlainValue(color, true)
					? Color.read(arguments)
					: color;
			return col === this || col && this._class === col._class
					&& this._type === col._type
					&& this.getAlpha() === col.getAlpha()
					&& Base.equals(this._components, col._components)
					|| false;
		},

		toString: function() {
			var properties = this._properties,
				parts = [],
				isGradient = this._type === 'gradient',
				f = Formatter.instance;
			for (var i = 0, l = properties.length; i < l; i++) {
				var value = this._components[i];
				if (value != null)
					parts.push(properties[i] + ': '
							+ (isGradient ? value : f.number(value)));
			}
			if (this._alpha != null)
				parts.push('alpha: ' + f.number(this._alpha));
			return '{ ' + parts.join(', ') + ' }';
		},

		toCSS: function(hex) {
			var components = this._convert('rgb'),
				alpha = hex || this._alpha == null ? 1 : this._alpha;
			function convert(val) {
				return Math.round((val < 0 ? 0 : val > 1 ? 1 : val) * 255);
			}
			components = [
				convert(components[0]),
				convert(components[1]),
				convert(components[2])
			];
			if (alpha < 1)
				components.push(alpha < 0 ? 0 : alpha);
			return hex
					? '#' + ((1 << 24) + (components[0] << 16)
						+ (components[1] << 8)
						+ components[2]).toString(16).slice(1)
					: (components.length == 4 ? 'rgba(' : 'rgb(')
						+ components.join(',') + ')';
		},

		toCanvasStyle: function(ctx, matrix) {
			if (this._canvasStyle)
				return this._canvasStyle;
			if (this._type !== 'gradient')
				return this._canvasStyle = this.toCSS();
			var components = this._components,
				gradient = components[0],
				stops = gradient._stops,
				origin = components[1],
				destination = components[2],
				highlight = components[3],
				inverse = matrix && matrix.inverted(),
				canvasGradient;
			if (inverse) {
				origin = inverse._transformPoint(origin);
				destination = inverse._transformPoint(destination);
				if (highlight)
					highlight = inverse._transformPoint(highlight);
			}
			if (gradient._radial) {
				var radius = destination.getDistance(origin);
				if (highlight) {
					var vector = highlight.subtract(origin);
					if (vector.getLength() > radius)
						highlight = origin.add(vector.normalize(radius - 0.1));
				}
				var start = highlight || origin;
				canvasGradient = ctx.createRadialGradient(start.x, start.y,
						0, origin.x, origin.y, radius);
			} else {
				canvasGradient = ctx.createLinearGradient(origin.x, origin.y,
						destination.x, destination.y);
			}
			for (var i = 0, l = stops.length; i < l; i++) {
				var stop = stops[i],
					offset = stop._offset;
				canvasGradient.addColorStop(
						offset == null ? i / (l - 1) : offset,
						stop._color.toCanvasStyle());
			}
			return this._canvasStyle = canvasGradient;
		},

		transform: function(matrix) {
			if (this._type === 'gradient') {
				var components = this._components;
				for (var i = 1, l = components.length; i < l; i++) {
					var point = components[i];
					matrix._transformPoint(point, point, true);
				}
				this._changed();
			}
		},

		statics: {
			_types: types,

			random: function() {
				var random = Math.random;
				return new Color(random(), random(), random());
			}
		}
	});
},
new function() {
	var operators = {
		add: function(a, b) {
			return a + b;
		},

		subtract: function(a, b) {
			return a - b;
		},

		multiply: function(a, b) {
			return a * b;
		},

		divide: function(a, b) {
			return a / b;
		}
	};

	return Base.each(operators, function(operator, name) {
		this[name] = function(color) {
			color = Color.read(arguments);
			var type = this._type,
				components1 = this._components,
				components2 = color._convert(type);
			for (var i = 0, l = components1.length; i < l; i++)
				components2[i] = operator(components1[i], components2[i]);
			return new Color(type, components2,
					this._alpha != null
							? operator(this._alpha, color.getAlpha())
							: null);
		};
	}, {
	});
});

var Gradient = Base.extend({
	_class: 'Gradient',

	initialize: function Gradient(stops, radial) {
		this._id = UID.get();
		if (stops && Base.isPlainObject(stops)) {
			this.set(stops);
			stops = radial = null;
		}
		if (this._stops == null) {
			this.setStops(stops || ['white', 'black']);
		}
		if (this._radial == null) {
			this.setRadial(typeof radial === 'string' && radial === 'radial'
					|| radial || false);
		}
	},

	_serialize: function(options, dictionary) {
		return dictionary.add(this, function() {
			return Base.serialize([this._stops, this._radial],
					options, true, dictionary);
		});
	},

	_changed: function() {
		for (var i = 0, l = this._owners && this._owners.length; i < l; i++) {
			this._owners[i]._changed();
		}
	},

	_addOwner: function(color) {
		if (!this._owners)
			this._owners = [];
		this._owners.push(color);
	},

	_removeOwner: function(color) {
		var index = this._owners ? this._owners.indexOf(color) : -1;
		if (index != -1) {
			this._owners.splice(index, 1);
			if (!this._owners.length)
				this._owners = undefined;
		}
	},

	clone: function() {
		var stops = [];
		for (var i = 0, l = this._stops.length; i < l; i++) {
			stops[i] = this._stops[i].clone();
		}
		return new Gradient(stops, this._radial);
	},

	getStops: function() {
		return this._stops;
	},

	setStops: function(stops) {
		if (stops.length < 2) {
			throw new Error(
					'Gradient stop list needs to contain at least two stops.');
		}
		var _stops = this._stops;
		if (_stops) {
			for (var i = 0, l = _stops.length; i < l; i++)
				_stops[i]._owner = undefined;
		}
		_stops = this._stops = GradientStop.readList(stops, 0, { clone: true });
		for (var i = 0, l = _stops.length; i < l; i++)
			_stops[i]._owner = this;
		this._changed();
	},

	getRadial: function() {
		return this._radial;
	},

	setRadial: function(radial) {
		this._radial = radial;
		this._changed();
	},

	equals: function(gradient) {
		if (gradient === this)
			return true;
		if (gradient && this._class === gradient._class) {
			var stops1 = this._stops,
				stops2 = gradient._stops,
				length = stops1.length;
			if (length === stops2.length) {
				for (var i = 0; i < length; i++) {
					if (!stops1[i].equals(stops2[i]))
						return false;
				}
				return true;
			}
		}
		return false;
	}
});

var GradientStop = Base.extend({
	_class: 'GradientStop',

	initialize: function GradientStop(arg0, arg1) {
		var color = arg0,
			offset = arg1;
		if (typeof arg0 === 'object' && arg1 === undefined) {
			if (Array.isArray(arg0) && typeof arg0[0] !== 'number') {
				color = arg0[0];
				offset = arg0[1];
			} else if ('color' in arg0 || 'offset' in arg0
					|| 'rampPoint' in arg0) {
				color = arg0.color;
				offset = arg0.offset || arg0.rampPoint || 0;
			}
		}
		this.setColor(color);
		this.setOffset(offset);
	},

	clone: function() {
		return new GradientStop(this._color.clone(), this._offset);
	},

	_serialize: function(options, dictionary) {
		var color = this._color,
			offset = this._offset;
		return Base.serialize(offset == null ? [color] : [color, offset],
				options, true, dictionary);
	},

	_changed: function() {
		if (this._owner)
			this._owner._changed(129);
	},

	getOffset: function() {
		return this._offset;
	},

	setOffset: function(offset) {
		this._offset = offset;
		this._changed();
	},

	getRampPoint: '#getOffset',
	setRampPoint: '#setOffset',

	getColor: function() {
		return this._color;
	},

	setColor: function() {
		var color = Color.read(arguments, 0, { clone: true });
		if (color)
			color._owner = this;
		this._color = color;
		this._changed();
	},

	equals: function(stop) {
		return stop === this || stop && this._class === stop._class
				&& this._color.equals(stop._color)
				&& this._offset == stop._offset
				|| false;
	}
});

var Style = Base.extend(new function() {
	var itemDefaults = {
		fillColor: null,
		fillRule: 'nonzero',
		strokeColor: null,
		strokeWidth: 1,
		strokeCap: 'butt',
		strokeJoin: 'miter',
		strokeScaling: true,
		miterLimit: 10,
		dashOffset: 0,
		dashArray: [],
		shadowColor: null,
		shadowBlur: 0,
		shadowOffset: new Point(),
		selectedColor: null
	},
	groupDefaults = Base.set({}, itemDefaults, {
		fontFamily: 'sans-serif',
		fontWeight: 'normal',
		fontSize: 12,
		leading: null,
		justification: 'left'
	}),
	textDefaults = Base.set({}, groupDefaults, {
		fillColor: new Color()
	}),
	flags = {
		strokeWidth: 193,
		strokeCap: 193,
		strokeJoin: 193,
		strokeScaling: 201,
		miterLimit: 193,
		fontFamily: 9,
		fontWeight: 9,
		fontSize: 9,
		font: 9,
		leading: 9,
		justification: 9
	},
	item = {
		beans: true
	},
	fields = {
		_class: 'Style',
		beans: true,

		initialize: function Style(style, _owner, _project) {
			this._values = {};
			this._owner = _owner;
			this._project = _owner && _owner._project || _project
					|| paper.project;
			this._defaults = !_owner || _owner instanceof Group ? groupDefaults
					: _owner instanceof TextItem ? textDefaults
					: itemDefaults;
			if (style)
				this.set(style);
		}
	};

	Base.each(groupDefaults, function(value, key) {
		var isColor = /Color$/.test(key),
			isPoint = key === 'shadowOffset',
			part = Base.capitalize(key),
			flag = flags[key],
			set = 'set' + part,
			get = 'get' + part;

		fields[set] = function(value) {
			var owner = this._owner,
				children = owner && owner._children,
				applyToChildren = children && children.length > 0
					&& !(owner instanceof CompoundPath);
			if (applyToChildren) {
				for (var i = 0, l = children.length; i < l; i++)
					children[i]._style[set](value);
			}
			if ((key === 'selectedColor' || !applyToChildren)
					&& key in this._defaults) {
				var old = this._values[key];
				if (old !== value) {
					if (isColor) {
						if (old && old._owner !== undefined) {
							old._owner = undefined;
							old._canvasStyle = null;
						}
						if (value && value.constructor === Color) {
							if (value._owner)
								value = value.clone();
							value._owner = owner;
						}
					}
					this._values[key] = value;
					if (owner)
						owner._changed(flag || 129);
				}
			}
		};

		fields[get] = function(_dontMerge) {
			var owner = this._owner,
				children = owner && owner._children,
				value;
			if (key in this._defaults && (!children || !children.length
					|| _dontMerge || owner instanceof CompoundPath)) {
				var value = this._values[key];
				if (value === undefined) {
					value = this._defaults[key];
					if (value && value.clone)
						value = value.clone();
				} else {
					var ctor = isColor ? Color : isPoint ? Point : null;
					if (ctor && !(value && value.constructor === ctor)) {
						this._values[key] = value = ctor.read([value], 0,
								{ readNull: true, clone: true });
						if (value && isColor)
							value._owner = owner;
					}
				}
			} else if (children) {
				for (var i = 0, l = children.length; i < l; i++) {
					var childValue = children[i]._style[get]();
					if (!i) {
						value = childValue;
					} else if (!Base.equals(value, childValue)) {
						return undefined;
					}
				}
			}
			return value;
		};

		item[get] = function(_dontMerge) {
			return this._style[get](_dontMerge);
		};

		item[set] = function(value) {
			this._style[set](value);
		};
	});

	Base.each({
		Font: 'FontFamily',
		WindingRule: 'FillRule'
	}, function(value, key) {
		var get = 'get' + key,
			set = 'set' + key;
		fields[get] = item[get] = '#get' + value;
		fields[set] = item[set] = '#set' + value;
	});

	Item.inject(item);
	return fields;
}, {
	set: function(style) {
		var isStyle = style instanceof Style,
			values = isStyle ? style._values : style;
		if (values) {
			for (var key in values) {
				if (key in this._defaults) {
					var value = values[key];
					this[key] = value && isStyle && value.clone
							? value.clone() : value;
				}
			}
		}
	},

	equals: function(style) {
		function compare(style1, style2, secondary) {
			var values1 = style1._values,
				values2 = style2._values,
				defaults2 = style2._defaults;
			for (var key in values1) {
				var value1 = values1[key],
					value2 = values2[key];
				if (!(secondary && key in values2) && !Base.equals(value1,
						value2 === undefined ? defaults2[key] : value2))
					return false;
			}
			return true;
		}

		return style === this || style && this._class === style._class
				&& compare(this, style)
				&& compare(style, this, true)
				|| false;
	},

	_dispose: function() {
		var color;
		color = this.getFillColor();
		if (color) color._canvasStyle = null;
		color = this.getStrokeColor();
		if (color) color._canvasStyle = null;
		color = this.getShadowColor();
		if (color) color._canvasStyle = null;
	},

	hasFill: function() {
		var color = this.getFillColor();
		return !!color && color.alpha > 0;
	},

	hasStroke: function() {
		var color = this.getStrokeColor();
		return !!color && color.alpha > 0 && this.getStrokeWidth() > 0;
	},

	hasShadow: function() {
		var color = this.getShadowColor();
		return !!color && color.alpha > 0 && (this.getShadowBlur() > 0
				|| !this.getShadowOffset().isZero());
	},

	getView: function() {
		return this._project._view;
	},

	getFontStyle: function() {
		var fontSize = this.getFontSize();
		return this.getFontWeight()
				+ ' ' + fontSize + (/[a-z]/i.test(fontSize + '') ? ' ' : 'px ')
				+ this.getFontFamily();
	},

	getFont: '#getFontFamily',
	setFont: '#setFontFamily',

	getLeading: function getLeading() {
		var leading = getLeading.base.call(this),
			fontSize = this.getFontSize();
		if (/pt|em|%|px/.test(fontSize))
			fontSize = this.getView().getPixelSize(fontSize);
		return leading != null ? leading : fontSize * 1.2;
	}

});

var DomElement = new function() {
	function handlePrefix(el, name, set, value) {
		var prefixes = ['', 'webkit', 'moz', 'Moz', 'ms', 'o'],
			suffix = name[0].toUpperCase() + name.substring(1);
		for (var i = 0; i < 6; i++) {
			var prefix = prefixes[i],
				key = prefix ? prefix + suffix : name;
			if (key in el) {
				if (set) {
					el[key] = value;
				} else {
					return el[key];
				}
				break;
			}
		}
	}

	return {
		getStyles: function(el) {
			var doc = el && el.nodeType !== 9 ? el.ownerDocument : el,
				view = doc && doc.defaultView;
			return view && view.getComputedStyle(el, '');
		},

		getBounds: function(el, viewport) {
			var doc = el.ownerDocument,
				body = doc.body,
				html = doc.documentElement,
				rect;
			try {
				rect = el.getBoundingClientRect();
			} catch (e) {
				rect = { left: 0, top: 0, width: 0, height: 0 };
			}
			var x = rect.left - (html.clientLeft || body.clientLeft || 0),
				y = rect.top - (html.clientTop || body.clientTop || 0);
			if (!viewport) {
				var view = doc.defaultView;
				x += view.pageXOffset || html.scrollLeft || body.scrollLeft;
				y += view.pageYOffset || html.scrollTop || body.scrollTop;
			}
			return new Rectangle(x, y, rect.width, rect.height);
		},

		getViewportBounds: function(el) {
			var doc = el.ownerDocument,
				view = doc.defaultView,
				html = doc.documentElement;
			return new Rectangle(0, 0,
				view.innerWidth || html.clientWidth,
				view.innerHeight || html.clientHeight
			);
		},

		getOffset: function(el, viewport) {
			return DomElement.getBounds(el, viewport).getPoint();
		},

		getSize: function(el) {
			return DomElement.getBounds(el, true).getSize();
		},

		isInvisible: function(el) {
			return DomElement.getSize(el).equals(new Size(0, 0));
		},

		isInView: function(el) {
			return !DomElement.isInvisible(el)
					&& DomElement.getViewportBounds(el).intersects(
						DomElement.getBounds(el, true));
		},

		isInserted: function(el) {
			return document.body.contains(el);
		},

		getPrefixed: function(el, name) {
			return el && handlePrefix(el, name);
		},

		setPrefixed: function(el, name, value) {
			if (typeof name === 'object') {
				for (var key in name)
					handlePrefix(el, key, true, name[key]);
			} else {
				handlePrefix(el, name, true, value);
			}
		}
	};
};

var DomEvent = {
	add: function(el, events) {
		if (el) {
			for (var type in events) {
				var func = events[type],
					parts = type.split(/[\s,]+/g);
				for (var i = 0, l = parts.length; i < l; i++) {
					var name = parts[i];
					var options = (
						el === document
						&& (name === 'touchstart' || name === 'touchmove')
					) ? { passive: false } : false;
					el.addEventListener(name, func, options);
				}
			}
		}
	},

	remove: function(el, events) {
		if (el) {
			for (var type in events) {
				var func = events[type],
					parts = type.split(/[\s,]+/g);
				for (var i = 0, l = parts.length; i < l; i++)
					el.removeEventListener(parts[i], func, false);
			}
		}
	},

	getPoint: function(event) {
		var pos = event.targetTouches
				? event.targetTouches.length
					? event.targetTouches[0]
					: event.changedTouches[0]
				: event;
		return new Point(
			pos.pageX || pos.clientX + document.documentElement.scrollLeft,
			pos.pageY || pos.clientY + document.documentElement.scrollTop
		);
	},

	getTarget: function(event) {
		return event.target || event.srcElement;
	},

	getRelatedTarget: function(event) {
		return event.relatedTarget || event.toElement;
	},

	getOffset: function(event, target) {
		return DomEvent.getPoint(event).subtract(DomElement.getOffset(
				target || DomEvent.getTarget(event)));
	}
};

DomEvent.requestAnimationFrame = new function() {
	var nativeRequest = DomElement.getPrefixed(window, 'requestAnimationFrame'),
		requested = false,
		callbacks = [],
		timer;

	function handleCallbacks() {
		var functions = callbacks;
		callbacks = [];
		for (var i = 0, l = functions.length; i < l; i++)
			functions[i]();
		requested = nativeRequest && callbacks.length;
		if (requested)
			nativeRequest(handleCallbacks);
	}

	return function(callback) {
		callbacks.push(callback);
		if (nativeRequest) {
			if (!requested) {
				nativeRequest(handleCallbacks);
				requested = true;
			}
		} else if (!timer) {
			timer = setInterval(handleCallbacks, 1000 / 60);
		}
	};
};

var View = Base.extend(Emitter, {
	_class: 'View',

	initialize: function View(project, element) {

		function getSize(name) {
			return element[name] || parseInt(element.getAttribute(name), 10);
		}

		function getCanvasSize() {
			var size = DomElement.getSize(element);
			return size.isNaN() || size.isZero()
					? new Size(getSize('width'), getSize('height'))
					: size;
		}

		var size;
		if (window && element) {
			this._id = element.getAttribute('id');
			if (this._id == null)
				element.setAttribute('id', this._id = 'view-' + View._id++);
			DomEvent.add(element, this._viewEvents);
			var none = 'none';
			DomElement.setPrefixed(element.style, {
				userDrag: none,
				userSelect: none,
				touchCallout: none,
				contentZooming: none,
				tapHighlightColor: 'rgba(0,0,0,0)'
			});

			if (PaperScope.hasAttribute(element, 'resize')) {
				var that = this;
				DomEvent.add(window, this._windowEvents = {
					resize: function() {
						that.setViewSize(getCanvasSize());
					}
				});
			}

			size = getCanvasSize();

			if (PaperScope.hasAttribute(element, 'stats')
					&& typeof Stats !== 'undefined') {
				this._stats = new Stats();
				var stats = this._stats.domElement,
					style = stats.style,
					offset = DomElement.getOffset(element);
				style.position = 'absolute';
				style.left = offset.x + 'px';
				style.top = offset.y + 'px';
				document.body.appendChild(stats);
			}
		} else {
			size = new Size(element);
			element = null;
		}
		this._project = project;
		this._scope = project._scope;
		this._element = element;
		if (!this._pixelRatio)
			this._pixelRatio = window && window.devicePixelRatio || 1;
		this._setElementSize(size.width, size.height);
		this._viewSize = size;
		View._views.push(this);
		View._viewsById[this._id] = this;
		(this._matrix = new Matrix())._owner = this;
		if (!View._focused)
			View._focused = this;
		this._frameItems = {};
		this._frameItemCount = 0;
		this._itemEvents = { native: {}, virtual: {} };
		this._autoUpdate = !paper.agent.node;
		this._needsUpdate = false;
	},

	remove: function() {
		if (!this._project)
			return false;
		if (View._focused === this)
			View._focused = null;
		View._views.splice(View._views.indexOf(this), 1);
		delete View._viewsById[this._id];
		var project = this._project;
		if (project._view === this)
			project._view = null;
		DomEvent.remove(this._element, this._viewEvents);
		DomEvent.remove(window, this._windowEvents);
		this._element = this._project = null;
		this.off('frame');
		this._animate = false;
		this._frameItems = {};
		return true;
	},

	_events: Base.each(
		Item._itemHandlers.concat(['onResize', 'onKeyDown', 'onKeyUp']),
		function(name) {
			this[name] = {};
		}, {
			onFrame: {
				install: function() {
					this.play();
				},

				uninstall: function() {
					this.pause();
				}
			}
		}
	),

	_animate: false,
	_time: 0,
	_count: 0,

	getAutoUpdate: function() {
		return this._autoUpdate;
	},

	setAutoUpdate: function(autoUpdate) {
		this._autoUpdate = autoUpdate;
		if (autoUpdate)
			this.requestUpdate();
	},

	update: function() {
	},

	draw: function() {
		this.update();
	},

	requestUpdate: function() {
		if (!this._requested) {
			var that = this;
			DomEvent.requestAnimationFrame(function() {
				that._requested = false;
				if (that._animate) {
					that.requestUpdate();
					var element = that._element;
					if ((!DomElement.getPrefixed(document, 'hidden')
							|| PaperScope.getAttribute(element, 'keepalive')
								=== 'true') && DomElement.isInView(element)) {
						that._handleFrame();
					}
				}
				if (that._autoUpdate)
					that.update();
			});
			this._requested = true;
		}
	},

	play: function() {
		this._animate = true;
		this.requestUpdate();
	},

	pause: function() {
		this._animate = false;
	},

	_handleFrame: function() {
		paper = this._scope;
		var now = Date.now() / 1000,
			delta = this._last ? now - this._last : 0;
		this._last = now;
		this.emit('frame', new Base({
			delta: delta,
			time: this._time += delta,
			count: this._count++
		}));
		if (this._stats)
			this._stats.update();
	},

	_animateItem: function(item, animate) {
		var items = this._frameItems;
		if (animate) {
			items[item._id] = {
				item: item,
				time: 0,
				count: 0
			};
			if (++this._frameItemCount === 1)
				this.on('frame', this._handleFrameItems);
		} else {
			delete items[item._id];
			if (--this._frameItemCount === 0) {
				this.off('frame', this._handleFrameItems);
			}
		}
	},

	_handleFrameItems: function(event) {
		for (var i in this._frameItems) {
			var entry = this._frameItems[i];
			entry.item.emit('frame', new Base(event, {
				time: entry.time += event.delta,
				count: entry.count++
			}));
		}
	},

	_changed: function() {
		this._project._changed(4097);
		this._bounds = this._decomposed = undefined;
	},

	getElement: function() {
		return this._element;
	},

	getPixelRatio: function() {
		return this._pixelRatio;
	},

	getResolution: function() {
		return this._pixelRatio * 72;
	},

	getViewSize: function() {
		var size = this._viewSize;
		return new LinkedSize(size.width, size.height, this, 'setViewSize');
	},

	setViewSize: function() {
		var size = Size.read(arguments),
			delta = size.subtract(this._viewSize);
		if (delta.isZero())
			return;
		this._setElementSize(size.width, size.height);
		this._viewSize.set(size);
		this._changed();
		this.emit('resize', { size: size, delta: delta });
		if (this._autoUpdate) {
			this.update();
		}
	},

	_setElementSize: function(width, height) {
		var element = this._element;
		if (element) {
			if (element.width !== width)
				element.width = width;
			if (element.height !== height)
				element.height = height;
		}
	},

	getBounds: function() {
		if (!this._bounds)
			this._bounds = this._matrix.inverted()._transformBounds(
					new Rectangle(new Point(), this._viewSize));
		return this._bounds;
	},

	getSize: function() {
		return this.getBounds().getSize();
	},

	isVisible: function() {
		return DomElement.isInView(this._element);
	},

	isInserted: function() {
		return DomElement.isInserted(this._element);
	},

	getPixelSize: function(size) {
		var element = this._element,
			pixels;
		if (element) {
			var parent = element.parentNode,
				temp = document.createElement('div');
			temp.style.fontSize = size;
			parent.appendChild(temp);
			pixels = parseFloat(DomElement.getStyles(temp).fontSize);
			parent.removeChild(temp);
		} else {
			pixels = parseFloat(pixels);
		}
		return pixels;
	},

	getTextWidth: function(font, lines) {
		return 0;
	}
}, Base.each(['rotate', 'scale', 'shear', 'skew'], function(key) {
	var rotate = key === 'rotate';
	this[key] = function() {
		var value = (rotate ? Base : Point).read(arguments),
			center = Point.read(arguments, 0, { readNull: true });
		return this.transform(new Matrix()[key](value,
				center || this.getCenter(true)));
	};
}, {
	_decompose: function() {
		return this._decomposed || (this._decomposed = this._matrix.decompose());
	},

	translate: function() {
		var mx = new Matrix();
		return this.transform(mx.translate.apply(mx, arguments));
	},

	getCenter: function() {
		return this.getBounds().getCenter();
	},

	setCenter: function() {
		var center = Point.read(arguments);
		this.translate(this.getCenter().subtract(center));
	},

	getZoom: function() {
		var scaling = this._decompose().scaling;
		return (scaling.x + scaling.y) / 2;
	},

	setZoom: function(zoom) {
		this.transform(new Matrix().scale(zoom / this.getZoom(),
			this.getCenter()));
	},

	getRotation: function() {
		return this._decompose().rotation;
	},

	setRotation: function(rotation) {
		var current = this.getRotation();
		if (current != null && rotation != null) {
			this.rotate(rotation - current);
		}
	},

	getScaling: function() {
		var scaling = this._decompose().scaling;
		return new LinkedPoint(scaling.x, scaling.y, this, 'setScaling');
	},

	setScaling: function() {
		var current = this.getScaling(),
			scaling = Point.read(arguments, 0, { clone: true, readNull: true });
		if (current && scaling) {
			this.scale(scaling.x / current.x, scaling.y / current.y);
		}
	},

	getMatrix: function() {
		return this._matrix;
	},

	setMatrix: function() {
		var matrix = this._matrix;
		matrix.initialize.apply(matrix, arguments);
	},

	transform: function(matrix) {
		this._matrix.append(matrix);
	},

	scrollBy: function() {
		this.translate(Point.read(arguments).negate());
	}
}), {

	projectToView: function() {
		return this._matrix._transformPoint(Point.read(arguments));
	},

	viewToProject: function() {
		return this._matrix._inverseTransform(Point.read(arguments));
	},

	getEventPoint: function(event) {
		return this.viewToProject(DomEvent.getOffset(event, this._element));
	},

}, {
	statics: {
		_views: [],
		_viewsById: {},
		_id: 0,

		create: function(project, element) {
			if (document && typeof element === 'string')
				element = document.getElementById(element);
			var ctor = window ? CanvasView : View;
			return new ctor(project, element);
		}
	}
},
new function() {
	if (!window)
		return;
	var prevFocus,
		tempFocus,
		dragging = false,
		mouseDown = false;

	function getView(event) {
		var target = DomEvent.getTarget(event);
		return target.getAttribute && View._viewsById[
				target.getAttribute('id')];
	}

	function updateFocus() {
		var view = View._focused;
		if (!view || !view.isVisible()) {
			for (var i = 0, l = View._views.length; i < l; i++) {
				if ((view = View._views[i]).isVisible()) {
					View._focused = tempFocus = view;
					break;
				}
			}
		}
	}

	function handleMouseMove(view, event, point) {
		view._handleMouseEvent('mousemove', event, point);
	}

	var navigator = window.navigator,
		mousedown, mousemove, mouseup;
	if (navigator.pointerEnabled || navigator.msPointerEnabled) {
		mousedown = 'pointerdown MSPointerDown';
		mousemove = 'pointermove MSPointerMove';
		mouseup = 'pointerup pointercancel MSPointerUp MSPointerCancel';
	} else {
		mousedown = 'touchstart';
		mousemove = 'touchmove';
		mouseup = 'touchend touchcancel';
		if (!('ontouchstart' in window && navigator.userAgent.match(
				/mobile|tablet|ip(ad|hone|od)|android|silk/i))) {
			mousedown += ' mousedown';
			mousemove += ' mousemove';
			mouseup += ' mouseup';
		}
	}

	var viewEvents = {},
		docEvents = {
			mouseout: function(event) {
				var view = View._focused,
					target = DomEvent.getRelatedTarget(event);
				if (view && (!target || target.nodeName === 'HTML')) {
					var offset = DomEvent.getOffset(event, view._element),
						x = offset.x,
						abs = Math.abs,
						ax = abs(x),
						max = 1 << 25,
						diff = ax - max;
					offset.x = abs(diff) < ax ? diff * (x < 0 ? -1 : 1) : x;
					handleMouseMove(view, event, view.viewToProject(offset));
				}
			},

			scroll: updateFocus
		};

	viewEvents[mousedown] = function(event) {
		var view = View._focused = getView(event);
		if (!dragging) {
			dragging = true;
			view._handleMouseEvent('mousedown', event);
		}
	};

	docEvents[mousemove] = function(event) {
		var view = View._focused;
		if (!mouseDown) {
			var target = getView(event);
			if (target) {
				if (view !== target) {
					if (view)
						handleMouseMove(view, event);
					if (!prevFocus)
						prevFocus = view;
					view = View._focused = tempFocus = target;
				}
			} else if (tempFocus && tempFocus === view) {
				if (prevFocus && !prevFocus.isInserted())
					prevFocus = null;
				view = View._focused = prevFocus;
				prevFocus = null;
				updateFocus();
			}
		}
		if (view)
			handleMouseMove(view, event);
	};

	docEvents[mousedown] = function() {
		mouseDown = true;
	};

	docEvents[mouseup] = function(event) {
		var view = View._focused;
		if (view && dragging)
			view._handleMouseEvent('mouseup', event);
		mouseDown = dragging = false;
	};

	DomEvent.add(document, docEvents);

	DomEvent.add(window, {
		load: updateFocus
	});

	var called = false,
		prevented = false,
		fallbacks = {
			doubleclick: 'click',
			mousedrag: 'mousemove'
		},
		wasInView = false,
		overView,
		downPoint,
		lastPoint,
		downItem,
		overItem,
		dragItem,
		clickItem,
		clickTime,
		dblClick;

	function emitMouseEvent(obj, target, type, event, point, prevPoint,
			stopItem) {
		var stopped = false,
			mouseEvent;

		function emit(obj, type) {
			if (obj.responds(type)) {
				if (!mouseEvent) {
					mouseEvent = new MouseEvent(type, event, point,
							target || obj,
							prevPoint ? point.subtract(prevPoint) : null);
				}
				if (obj.emit(type, mouseEvent)) {
					called = true;
					if (mouseEvent.prevented)
						prevented = true;
					if (mouseEvent.stopped)
						return stopped = true;
				}
			} else {
				var fallback = fallbacks[type];
				if (fallback)
					return emit(obj, fallback);
			}
		}

		while (obj && obj !== stopItem) {
			if (emit(obj, type))
				break;
			obj = obj._parent;
		}
		return stopped;
	}

	function emitMouseEvents(view, hitItem, type, event, point, prevPoint) {
		view._project.removeOn(type);
		prevented = called = false;
		return (dragItem && emitMouseEvent(dragItem, null, type, event,
					point, prevPoint)
			|| hitItem && hitItem !== dragItem
				&& !hitItem.isDescendant(dragItem)
				&& emitMouseEvent(hitItem, null, type === 'mousedrag' ?
					'mousemove' : type, event, point, prevPoint, dragItem)
			|| emitMouseEvent(view, dragItem || hitItem || view, type, event,
					point, prevPoint));
	}

	var itemEventsMap = {
		mousedown: {
			mousedown: 1,
			mousedrag: 1,
			click: 1,
			doubleclick: 1
		},
		mouseup: {
			mouseup: 1,
			mousedrag: 1,
			click: 1,
			doubleclick: 1
		},
		mousemove: {
			mousedrag: 1,
			mousemove: 1,
			mouseenter: 1,
			mouseleave: 1
		}
	};

	return {
		_viewEvents: viewEvents,

		_handleMouseEvent: function(type, event, point) {
			var itemEvents = this._itemEvents,
				hitItems = itemEvents.native[type],
				nativeMove = type === 'mousemove',
				tool = this._scope.tool,
				view = this;

			function responds(type) {
				return itemEvents.virtual[type] || view.responds(type)
						|| tool && tool.responds(type);
			}

			if (nativeMove && dragging && responds('mousedrag'))
				type = 'mousedrag';
			if (!point)
				point = this.getEventPoint(event);

			var inView = this.getBounds().contains(point),
				hit = hitItems && inView && view._project.hitTest(point, {
					tolerance: 0,
					fill: true,
					stroke: true
				}),
				hitItem = hit && hit.item || null,
				handle = false,
				mouse = {};
			mouse[type.substr(5)] = true;

			if (hitItems && hitItem !== overItem) {
				if (overItem) {
					emitMouseEvent(overItem, null, 'mouseleave', event, point);
				}
				if (hitItem) {
					emitMouseEvent(hitItem, null, 'mouseenter', event, point);
				}
				overItem = hitItem;
			}
			if (wasInView ^ inView) {
				emitMouseEvent(this, null, inView ? 'mouseenter' : 'mouseleave',
						event, point);
				overView = inView ? this : null;
				handle = true;
			}
			if ((inView || mouse.drag) && !point.equals(lastPoint)) {
				emitMouseEvents(this, hitItem, nativeMove ? type : 'mousemove',
						event, point, lastPoint);
				handle = true;
			}
			wasInView = inView;
			if (mouse.down && inView || mouse.up && downPoint) {
				emitMouseEvents(this, hitItem, type, event, point, downPoint);
				if (mouse.down) {
					dblClick = hitItem === clickItem
						&& (Date.now() - clickTime < 300);
					downItem = clickItem = hitItem;
					if (!prevented && hitItem) {
						var item = hitItem;
						while (item && !item.responds('mousedrag'))
							item = item._parent;
						if (item)
							dragItem = hitItem;
					}
					downPoint = point;
				} else if (mouse.up) {
					if (!prevented && hitItem === downItem) {
						clickTime = Date.now();
						emitMouseEvents(this, hitItem, dblClick ? 'doubleclick'
								: 'click', event, point, downPoint);
						dblClick = false;
					}
					downItem = dragItem = null;
				}
				wasInView = false;
				handle = true;
			}
			lastPoint = point;
			if (handle && tool) {
				called = tool._handleMouseEvent(type, event, point, mouse)
					|| called;
			}

			if (
				event.cancelable !== false
				&& (called && !mouse.move || mouse.down && responds('mouseup'))
			) {
				event.preventDefault();
			}
		},

		_handleKeyEvent: function(type, event, key, character) {
			var scope = this._scope,
				tool = scope.tool,
				keyEvent;

			function emit(obj) {
				if (obj.responds(type)) {
					paper = scope;
					obj.emit(type, keyEvent = keyEvent
							|| new KeyEvent(type, event, key, character));
				}
			}

			if (this.isVisible()) {
				emit(this);
				if (tool && tool.responds(type))
					emit(tool);
			}
		},

		_countItemEvent: function(type, sign) {
			var itemEvents = this._itemEvents,
				native = itemEvents.native,
				virtual = itemEvents.virtual;
			for (var key in itemEventsMap) {
				native[key] = (native[key] || 0)
						+ (itemEventsMap[key][type] || 0) * sign;
			}
			virtual[type] = (virtual[type] || 0) + sign;
		},

		statics: {
			updateFocus: updateFocus,

			_resetState: function() {
				dragging = mouseDown = called = wasInView = false;
				prevFocus = tempFocus = overView = downPoint = lastPoint =
					downItem = overItem = dragItem = clickItem = clickTime =
					dblClick = null;
			}
		}
	};
});

var CanvasView = View.extend({
	_class: 'CanvasView',

	initialize: function CanvasView(project, canvas) {
		if (!(canvas instanceof window.HTMLCanvasElement)) {
			var size = Size.read(arguments, 1);
			if (size.isZero())
				throw new Error(
						'Cannot create CanvasView with the provided argument: '
						+ Base.slice(arguments, 1));
			canvas = CanvasProvider.getCanvas(size);
		}
		var ctx = this._context = canvas.getContext('2d');
		ctx.save();
		this._pixelRatio = 1;
		if (!/^off|false$/.test(PaperScope.getAttribute(canvas, 'hidpi'))) {
			var deviceRatio = window.devicePixelRatio || 1,
				backingStoreRatio = DomElement.getPrefixed(ctx,
						'backingStorePixelRatio') || 1;
			this._pixelRatio = deviceRatio / backingStoreRatio;
		}
		View.call(this, project, canvas);
		this._needsUpdate = true;
	},

	remove: function remove() {
		this._context.restore();
		return remove.base.call(this);
	},

	_setElementSize: function _setElementSize(width, height) {
		var pixelRatio = this._pixelRatio;
		_setElementSize.base.call(this, width * pixelRatio, height * pixelRatio);
		if (pixelRatio !== 1) {
			var element = this._element,
				ctx = this._context;
			if (!PaperScope.hasAttribute(element, 'resize')) {
				var style = element.style;
				style.width = width + 'px';
				style.height = height + 'px';
			}
			ctx.restore();
			ctx.save();
			ctx.scale(pixelRatio, pixelRatio);
		}
	},

	getPixelSize: function getPixelSize(size) {
		var agent = paper.agent,
			pixels;
		if (agent && agent.firefox) {
			pixels = getPixelSize.base.call(this, size);
		} else {
			var ctx = this._context,
				prevFont = ctx.font;
			ctx.font = size + ' serif';
			pixels = parseFloat(ctx.font);
			ctx.font = prevFont;
		}
		return pixels;
	},

	getTextWidth: function(font, lines) {
		var ctx = this._context,
			prevFont = ctx.font,
			width = 0;
		ctx.font = font;
		for (var i = 0, l = lines.length; i < l; i++)
			width = Math.max(width, ctx.measureText(lines[i]).width);
		ctx.font = prevFont;
		return width;
	},

	update: function() {
		if (!this._needsUpdate)
			return false;
		var project = this._project,
			ctx = this._context,
			size = this._viewSize;
		ctx.clearRect(0, 0, size.width + 1, size.height + 1);
		if (project)
			project.draw(ctx, this._matrix, this._pixelRatio);
		this._needsUpdate = false;
		return true;
	}
});

var Event = Base.extend({
	_class: 'Event',

	initialize: function Event(event) {
		this.event = event;
		this.type = event && event.type;
	},

	prevented: false,
	stopped: false,

	preventDefault: function() {
		this.prevented = true;
		this.event.preventDefault();
	},

	stopPropagation: function() {
		this.stopped = true;
		this.event.stopPropagation();
	},

	stop: function() {
		this.stopPropagation();
		this.preventDefault();
	},

	getTimeStamp: function() {
		return this.event.timeStamp;
	},

	getModifiers: function() {
		return Key.modifiers;
	}
});

var KeyEvent = Event.extend({
	_class: 'KeyEvent',

	initialize: function KeyEvent(type, event, key, character) {
		this.type = type;
		this.event = event;
		this.key = key;
		this.character = character;
	},

	toString: function() {
		return "{ type: '" + this.type
				+ "', key: '" + this.key
				+ "', character: '" + this.character
				+ "', modifiers: " + this.getModifiers()
				+ " }";
	}
});

var Key = new function() {
	var keyLookup = {
			'\t': 'tab',
			' ': 'space',
			'\b': 'backspace',
			'\x7f': 'delete',
			'Spacebar': 'space',
			'Del': 'delete',
			'Win': 'meta',
			'Esc': 'escape'
		},

		charLookup = {
			'tab': '\t',
			'space': ' ',
			'enter': '\r'
		},

		keyMap = {},
		charMap = {},
		metaFixMap,
		downKey,

		modifiers = new Base({
			shift: false,
			control: false,
			alt: false,
			meta: false,
			capsLock: false,
			space: false
		}).inject({
			option: {
				get: function() {
					return this.alt;
				}
			},

			command: {
				get: function() {
					var agent = paper && paper.agent;
					return agent && agent.mac ? this.meta : this.control;
				}
			}
		});

	function getKey(event) {
		var key = event.key || event.keyIdentifier;
		key = /^U\+/.test(key)
				? String.fromCharCode(parseInt(key.substr(2), 16))
				: /^Arrow[A-Z]/.test(key) ? key.substr(5)
				: key === 'Unidentified'  || key === undefined
					? String.fromCharCode(event.keyCode)
					: key;
		return keyLookup[key] ||
				(key.length > 1 ? Base.hyphenate(key) : key.toLowerCase());
	}

	function handleKey(down, key, character, event) {
		var type = down ? 'keydown' : 'keyup',
			view = View._focused,
			name;
		keyMap[key] = down;
		if (down) {
			charMap[key] = character;
		} else {
			delete charMap[key];
		}
		if (key.length > 1 && (name = Base.camelize(key)) in modifiers) {
			modifiers[name] = down;
			var agent = paper && paper.agent;
			if (name === 'meta' && agent && agent.mac) {
				if (down) {
					metaFixMap = {};
				} else {
					for (var k in metaFixMap) {
						if (k in charMap)
							handleKey(false, k, metaFixMap[k], event);
					}
					metaFixMap = null;
				}
			}
		} else if (down && metaFixMap) {
			metaFixMap[key] = character;
		}
		if (view) {
			view._handleKeyEvent(down ? 'keydown' : 'keyup', event, key,
					character);
		}
	}

	DomEvent.add(document, {
		keydown: function(event) {
			var key = getKey(event),
				agent = paper && paper.agent;
			if (key.length > 1 || agent && (agent.chrome && (event.altKey
						|| agent.mac && event.metaKey
						|| !agent.mac && event.ctrlKey))) {
				handleKey(true, key,
						charLookup[key] || (key.length > 1 ? '' : key), event);
			} else {
				downKey = key;
			}
		},

		keypress: function(event) {
			if (downKey) {
				var key = getKey(event),
					code = event.charCode,
					character = code >= 32 ? String.fromCharCode(code)
						: key.length > 1 ? '' : key;
				if (key !== downKey) {
					key = character.toLowerCase();
				}
				handleKey(true, key, character, event);
				downKey = null;
			}
		},

		keyup: function(event) {
			var key = getKey(event);
			if (key in charMap)
				handleKey(false, key, charMap[key], event);
		}
	});

	DomEvent.add(window, {
		blur: function(event) {
			for (var key in charMap)
				handleKey(false, key, charMap[key], event);
		}
	});

	return {
		modifiers: modifiers,

		isDown: function(key) {
			return !!keyMap[key];
		}
	};
};

var MouseEvent = Event.extend({
	_class: 'MouseEvent',

	initialize: function MouseEvent(type, event, point, target, delta) {
		this.type = type;
		this.event = event;
		this.point = point;
		this.target = target;
		this.delta = delta;
	},

	toString: function() {
		return "{ type: '" + this.type
				+ "', point: " + this.point
				+ ', target: ' + this.target
				+ (this.delta ? ', delta: ' + this.delta : '')
				+ ', modifiers: ' + this.getModifiers()
				+ ' }';
	}
});

var ToolEvent = Event.extend({
	_class: 'ToolEvent',
	_item: null,

	initialize: function ToolEvent(tool, type, event) {
		this.tool = tool;
		this.type = type;
		this.event = event;
	},

	_choosePoint: function(point, toolPoint) {
		return point ? point : toolPoint ? toolPoint.clone() : null;
	},

	getPoint: function() {
		return this._choosePoint(this._point, this.tool._point);
	},

	setPoint: function(point) {
		this._point = point;
	},

	getLastPoint: function() {
		return this._choosePoint(this._lastPoint, this.tool._lastPoint);
	},

	setLastPoint: function(lastPoint) {
		this._lastPoint = lastPoint;
	},

	getDownPoint: function() {
		return this._choosePoint(this._downPoint, this.tool._downPoint);
	},

	setDownPoint: function(downPoint) {
		this._downPoint = downPoint;
	},

	getMiddlePoint: function() {
		if (!this._middlePoint && this.tool._lastPoint) {
			return this.tool._point.add(this.tool._lastPoint).divide(2);
		}
		return this._middlePoint;
	},

	setMiddlePoint: function(middlePoint) {
		this._middlePoint = middlePoint;
	},

	getDelta: function() {
		return !this._delta && this.tool._lastPoint
				? this.tool._point.subtract(this.tool._lastPoint)
				: this._delta;
	},

	setDelta: function(delta) {
		this._delta = delta;
	},

	getCount: function() {
		return this.tool[/^mouse(down|up)$/.test(this.type)
				? '_downCount' : '_moveCount'];
	},

	setCount: function(count) {
		this.tool[/^mouse(down|up)$/.test(this.type) ? 'downCount' : 'count']
			= count;
	},

	getItem: function() {
		if (!this._item) {
			var result = this.tool._scope.project.hitTest(this.getPoint());
			if (result) {
				var item = result.item,
					parent = item._parent;
				while (/^(Group|CompoundPath)$/.test(parent._class)) {
					item = parent;
					parent = parent._parent;
				}
				this._item = item;
			}
		}
		return this._item;
	},

	setItem: function(item) {
		this._item = item;
	},

	toString: function() {
		return '{ type: ' + this.type
				+ ', point: ' + this.getPoint()
				+ ', count: ' + this.getCount()
				+ ', modifiers: ' + this.getModifiers()
				+ ' }';
	}
});

var Tool = PaperScopeItem.extend({
	_class: 'Tool',
	_list: 'tools',
	_reference: 'tool',
	_events: ['onMouseDown', 'onMouseUp', 'onMouseDrag', 'onMouseMove',
			'onActivate', 'onDeactivate', 'onEditOptions', 'onKeyDown',
			'onKeyUp'],

	initialize: function Tool(props) {
		PaperScopeItem.call(this);
		this._moveCount = -1;
		this._downCount = -1;
		this.set(props);
	},

	getMinDistance: function() {
		return this._minDistance;
	},

	setMinDistance: function(minDistance) {
		this._minDistance = minDistance;
		if (minDistance != null && this._maxDistance != null
				&& minDistance > this._maxDistance) {
			this._maxDistance = minDistance;
		}
	},

	getMaxDistance: function() {
		return this._maxDistance;
	},

	setMaxDistance: function(maxDistance) {
		this._maxDistance = maxDistance;
		if (this._minDistance != null && maxDistance != null
				&& maxDistance < this._minDistance) {
			this._minDistance = maxDistance;
		}
	},

	getFixedDistance: function() {
		return this._minDistance == this._maxDistance
			? this._minDistance : null;
	},

	setFixedDistance: function(distance) {
		this._minDistance = this._maxDistance = distance;
	},

	_handleMouseEvent: function(type, event, point, mouse) {
		paper = this._scope;
		if (mouse.drag && !this.responds(type))
			type = 'mousemove';
		var move = mouse.move || mouse.drag,
			responds = this.responds(type),
			minDistance = this.minDistance,
			maxDistance = this.maxDistance,
			called = false,
			tool = this;
		function update(minDistance, maxDistance) {
			var pt = point,
				toolPoint = move ? tool._point : (tool._downPoint || pt);
			if (move) {
				if (tool._moveCount >= 0 && pt.equals(toolPoint)) {
					return false;
				}
				if (toolPoint && (minDistance != null || maxDistance != null)) {
					var vector = pt.subtract(toolPoint),
						distance = vector.getLength();
					if (distance < (minDistance || 0))
						return false;
					if (maxDistance) {
						pt = toolPoint.add(vector.normalize(
								Math.min(distance, maxDistance)));
					}
				}
				tool._moveCount++;
			}
			tool._point = pt;
			tool._lastPoint = toolPoint || pt;
			if (mouse.down) {
				tool._moveCount = -1;
				tool._downPoint = pt;
				tool._downCount++;
			}
			return true;
		}

		function emit() {
			if (responds) {
				called = tool.emit(type, new ToolEvent(tool, type, event))
						|| called;
			}
		}

		if (mouse.down) {
			update();
			emit();
		} else if (mouse.up) {
			update(null, maxDistance);
			emit();
		} else if (responds) {
			while (update(minDistance, maxDistance))
				emit();
		}
		return called;
	}

});

var Tween = Base.extend(Emitter, {
	_class: 'Tween',

	statics: {
		easings: {
			linear: function(t) {
				return t;
			},

			easeInQuad: function(t) {
				return t * t;
			},

			easeOutQuad: function(t) {
				return t * (2 - t);
			},

			easeInOutQuad: function(t) {
				return t < 0.5
					? 2 * t * t
					: -1 + 2 * (2 - t) * t;
			},

			easeInCubic: function(t) {
				return t * t * t;
			},

			easeOutCubic: function(t) {
				return --t * t * t + 1;
			},

			easeInOutCubic: function(t) {
				return t < 0.5
					? 4 * t * t * t
					: (t - 1) * (2 * t - 2) * (2 * t - 2) + 1;
			},

			easeInQuart: function(t) {
				return t * t * t * t;
			},

			easeOutQuart: function(t) {
				return 1 - (--t) * t * t * t;
			},

			easeInOutQuart: function(t) {
				return t < 0.5
					? 8 * t * t * t * t
					: 1 - 8 * (--t) * t * t * t;
			},

			easeInQuint: function(t) {
				return t * t * t * t * t;
			},

			easeOutQuint: function(t) {
				return 1 + --t * t * t * t * t;
			},

			easeInOutQuint: function(t) {
				return t < 0.5
					? 16 * t * t * t * t * t
					: 1 + 16 * (--t) * t * t * t * t;
			}
		}
	},

	initialize: function Tween(object, from, to, duration, easing, start) {
		this.object = object;
		var type = typeof easing;
		var isFunction = type === 'function';
		this.type = isFunction
			? type
			: type === 'string'
				? easing
				: 'linear';
		this.easing = isFunction ? easing : Tween.easings[this.type];
		this.duration = duration;
		this.running = false;

		this._then = null;
		this._startTime = null;
		var state = from || to;
		this._keys = state ? Object.keys(state) : [];
		this._parsedKeys = this._parseKeys(this._keys);
		this._from = state && this._getState(from);
		this._to = state && this._getState(to);
		if (start !== false) {
			this.start();
		}
	},

	then: function(then) {
		this._then = then;
		return this;
	},

	start: function() {
		this._startTime = null;
		this.running = true;
		return this;
	},

	stop: function() {
		this.running = false;
		return this;
	},

	update: function(progress) {
		if (this.running) {
			if (progress > 1) {
				progress = 1;
				this.running = false;
			}

			var factor = this.easing(progress),
				keys = this._keys,
				getValue = function(value) {
					return typeof value === 'function'
						? value(factor, progress)
						: value;
				};
			for (var i = 0, l = keys && keys.length; i < l; i++) {
				var key = keys[i],
					from = getValue(this._from[key]),
					to = getValue(this._to[key]),
					value = (from && to && from.__add && to.__add)
						? to.__subtract(from).__multiply(factor).__add(from)
						: ((to - from) * factor) + from;
				this._setProperty(this._parsedKeys[key], value);
			}

			if (!this.running && this._then) {
				this._then(this.object);
			}
			if (this.responds('update')) {
				this.emit('update', new Base({
					progress: progress,
					factor: factor
				}));
			}
		}
		return this;
	},

	_events: {
		onUpdate: {}
	},

	_handleFrame: function(time) {
		var startTime = this._startTime,
			progress = startTime
				? (time - startTime) / this.duration
				: 0;
		if (!startTime) {
			this._startTime = time;
		}
		this.update(progress);
	},

	_getState: function(state) {
		var keys = this._keys,
			result = {};
		for (var i = 0, l = keys.length; i < l; i++) {
			var key = keys[i],
				path = this._parsedKeys[key],
				current = this._getProperty(path),
				value;
			if (state) {
				var resolved = this._resolveValue(current, state[key]);
				this._setProperty(path, resolved);
				value = this._getProperty(path);
				value = value && value.clone ? value.clone() : value;
				this._setProperty(path, current);
			} else {
				value = current && current.clone ? current.clone() : current;
			}
			result[key] = value;
		}
		return result;
	},

	_resolveValue: function(current, value) {
		if (value) {
			if (Array.isArray(value) && value.length === 2) {
				var operator = value[0];
				return (
					operator &&
					operator.match &&
					operator.match(/^[+\-*/]=/)
				)
					? this._calculate(current, operator[0], value[1])
					: value;
			} else if (typeof value === 'string') {
				var match = value.match(/^[+\-*/]=(.*)/);
				if (match) {
					var parsed = JSON.parse(match[1].replace(
						/(['"])?([a-zA-Z0-9_]+)(['"])?:/g,
						'"$2": '
					));
					return this._calculate(current, value[0], parsed);
				}
			}
		}
		return value;
	},

	_calculate: function(left, operator, right) {
		return paper.PaperScript.calculateBinary(left, operator, right);
	},

	_parseKeys: function(keys) {
		var parsed = {};
		for (var i = 0, l = keys.length; i < l; i++) {
			var key = keys[i],
				path = key
					.replace(/\.([^.]*)/g, '/$1')
					.replace(/\[['"]?([^'"\]]*)['"]?\]/g, '/$1');
			parsed[key] = path.split('/');
		}
		return parsed;
	},

	_getProperty: function(path, offset) {
		var obj = this.object;
		for (var i = 0, l = path.length - (offset || 0); i < l && obj; i++) {
			obj = obj[path[i]];
		}
		return obj;
	},

	_setProperty: function(path, value) {
		var dest = this._getProperty(path, 1);
		if (dest) {
			dest[path[path.length - 1]] = value;
		}
	}
});

var Http = {
	request: function(options) {
		var xhr = new self.XMLHttpRequest();
		xhr.open((options.method || 'get').toUpperCase(), options.url,
				Base.pick(options.async, true));
		if (options.mimeType)
			xhr.overrideMimeType(options.mimeType);
		xhr.onload = function() {
			var status = xhr.status;
			if (status === 0 || status === 200) {
				if (options.onLoad) {
					options.onLoad.call(xhr, xhr.responseText);
				}
			} else {
				xhr.onerror();
			}
		};
		xhr.onerror = function() {
			var status = xhr.status,
				message = 'Could not load "' + options.url + '" (Status: '
						+ status + ')';
			if (options.onError) {
				options.onError(message, status);
			} else {
				throw new Error(message);
			}
		};
		return xhr.send(null);
	}
};

var CanvasProvider = {
	canvases: [],

	getCanvas: function(width, height) {
		if (!window)
			return null;
		var canvas,
			clear = true;
		if (typeof width === 'object') {
			height = width.height;
			width = width.width;
		}
		if (this.canvases.length) {
			canvas = this.canvases.pop();
		} else {
			canvas = document.createElement('canvas');
			clear = false;
		}
		var ctx = canvas.getContext('2d');
		if (!ctx) {
			throw new Error('Canvas ' + canvas +
					' is unable to provide a 2D context.');
		}
		if (canvas.width === width && canvas.height === height) {
			if (clear)
				ctx.clearRect(0, 0, width + 1, height + 1);
		} else {
			canvas.width = width;
			canvas.height = height;
		}
		ctx.save();
		return canvas;
	},

	getContext: function(width, height) {
		var canvas = this.getCanvas(width, height);
		return canvas ? canvas.getContext('2d') : null;
	},

	release: function(obj) {
		var canvas = obj && obj.canvas ? obj.canvas : obj;
		if (canvas && canvas.getContext) {
			canvas.getContext('2d').restore();
			this.canvases.push(canvas);
		}
	}
};

var BlendMode = new function() {
	var min = Math.min,
		max = Math.max,
		abs = Math.abs,
		sr, sg, sb, sa,
		br, bg, bb, ba,
		dr, dg, db;

	function getLum(r, g, b) {
		return 0.2989 * r + 0.587 * g + 0.114 * b;
	}

	function setLum(r, g, b, l) {
		var d = l - getLum(r, g, b);
		dr = r + d;
		dg = g + d;
		db = b + d;
		var l = getLum(dr, dg, db),
			mn = min(dr, dg, db),
			mx = max(dr, dg, db);
		if (mn < 0) {
			var lmn = l - mn;
			dr = l + (dr - l) * l / lmn;
			dg = l + (dg - l) * l / lmn;
			db = l + (db - l) * l / lmn;
		}
		if (mx > 255) {
			var ln = 255 - l,
				mxl = mx - l;
			dr = l + (dr - l) * ln / mxl;
			dg = l + (dg - l) * ln / mxl;
			db = l + (db - l) * ln / mxl;
		}
	}

	function getSat(r, g, b) {
		return max(r, g, b) - min(r, g, b);
	}

	function setSat(r, g, b, s) {
		var col = [r, g, b],
			mx = max(r, g, b),
			mn = min(r, g, b),
			md;
		mn = mn === r ? 0 : mn === g ? 1 : 2;
		mx = mx === r ? 0 : mx === g ? 1 : 2;
		md = min(mn, mx) === 0 ? max(mn, mx) === 1 ? 2 : 1 : 0;
		if (col[mx] > col[mn]) {
			col[md] = (col[md] - col[mn]) * s / (col[mx] - col[mn]);
			col[mx] = s;
		} else {
			col[md] = col[mx] = 0;
		}
		col[mn] = 0;
		dr = col[0];
		dg = col[1];
		db = col[2];
	}

	var modes = {
		multiply: function() {
			dr = br * sr / 255;
			dg = bg * sg / 255;
			db = bb * sb / 255;
		},

		screen: function() {
			dr = br + sr - (br * sr / 255);
			dg = bg + sg - (bg * sg / 255);
			db = bb + sb - (bb * sb / 255);
		},

		overlay: function() {
			dr = br < 128 ? 2 * br * sr / 255 : 255 - 2 * (255 - br) * (255 - sr) / 255;
			dg = bg < 128 ? 2 * bg * sg / 255 : 255 - 2 * (255 - bg) * (255 - sg) / 255;
			db = bb < 128 ? 2 * bb * sb / 255 : 255 - 2 * (255 - bb) * (255 - sb) / 255;
		},

		'soft-light': function() {
			var t = sr * br / 255;
			dr = t + br * (255 - (255 - br) * (255 - sr) / 255 - t) / 255;
			t = sg * bg / 255;
			dg = t + bg * (255 - (255 - bg) * (255 - sg) / 255 - t) / 255;
			t = sb * bb / 255;
			db = t + bb * (255 - (255 - bb) * (255 - sb) / 255 - t) / 255;
		},

		'hard-light': function() {
			dr = sr < 128 ? 2 * sr * br / 255 : 255 - 2 * (255 - sr) * (255 - br) / 255;
			dg = sg < 128 ? 2 * sg * bg / 255 : 255 - 2 * (255 - sg) * (255 - bg) / 255;
			db = sb < 128 ? 2 * sb * bb / 255 : 255 - 2 * (255 - sb) * (255 - bb) / 255;
		},

		'color-dodge': function() {
			dr = br === 0 ? 0 : sr === 255 ? 255 : min(255, 255 * br / (255 - sr));
			dg = bg === 0 ? 0 : sg === 255 ? 255 : min(255, 255 * bg / (255 - sg));
			db = bb === 0 ? 0 : sb === 255 ? 255 : min(255, 255 * bb / (255 - sb));
		},

		'color-burn': function() {
			dr = br === 255 ? 255 : sr === 0 ? 0 : max(0, 255 - (255 - br) * 255 / sr);
			dg = bg === 255 ? 255 : sg === 0 ? 0 : max(0, 255 - (255 - bg) * 255 / sg);
			db = bb === 255 ? 255 : sb === 0 ? 0 : max(0, 255 - (255 - bb) * 255 / sb);
		},

		darken: function() {
			dr = br < sr ? br : sr;
			dg = bg < sg ? bg : sg;
			db = bb < sb ? bb : sb;
		},

		lighten: function() {
			dr = br > sr ? br : sr;
			dg = bg > sg ? bg : sg;
			db = bb > sb ? bb : sb;
		},

		difference: function() {
			dr = br - sr;
			if (dr < 0)
				dr = -dr;
			dg = bg - sg;
			if (dg < 0)
				dg = -dg;
			db = bb - sb;
			if (db < 0)
				db = -db;
		},

		exclusion: function() {
			dr = br + sr * (255 - br - br) / 255;
			dg = bg + sg * (255 - bg - bg) / 255;
			db = bb + sb * (255 - bb - bb) / 255;
		},

		hue: function() {
			setSat(sr, sg, sb, getSat(br, bg, bb));
			setLum(dr, dg, db, getLum(br, bg, bb));
		},

		saturation: function() {
			setSat(br, bg, bb, getSat(sr, sg, sb));
			setLum(dr, dg, db, getLum(br, bg, bb));
		},

		luminosity: function() {
			setLum(br, bg, bb, getLum(sr, sg, sb));
		},

		color: function() {
			setLum(sr, sg, sb, getLum(br, bg, bb));
		},

		add: function() {
			dr = min(br + sr, 255);
			dg = min(bg + sg, 255);
			db = min(bb + sb, 255);
		},

		subtract: function() {
			dr = max(br - sr, 0);
			dg = max(bg - sg, 0);
			db = max(bb - sb, 0);
		},

		average: function() {
			dr = (br + sr) / 2;
			dg = (bg + sg) / 2;
			db = (bb + sb) / 2;
		},

		negation: function() {
			dr = 255 - abs(255 - sr - br);
			dg = 255 - abs(255 - sg - bg);
			db = 255 - abs(255 - sb - bb);
		}
	};

	var nativeModes = this.nativeModes = Base.each([
		'source-over', 'source-in', 'source-out', 'source-atop',
		'destination-over', 'destination-in', 'destination-out',
		'destination-atop', 'lighter', 'darker', 'copy', 'xor'
	], function(mode) {
		this[mode] = true;
	}, {});

	var ctx = CanvasProvider.getContext(1, 1);
	if (ctx) {
		Base.each(modes, function(func, mode) {
			var darken = mode === 'darken',
				ok = false;
			ctx.save();
			try {
				ctx.fillStyle = darken ? '#300' : '#a00';
				ctx.fillRect(0, 0, 1, 1);
				ctx.globalCompositeOperation = mode;
				if (ctx.globalCompositeOperation === mode) {
					ctx.fillStyle = darken ? '#a00' : '#300';
					ctx.fillRect(0, 0, 1, 1);
					ok = ctx.getImageData(0, 0, 1, 1).data[0] !== darken
							? 170 : 51;
				}
			} catch (e) {}
			ctx.restore();
			nativeModes[mode] = ok;
		});
		CanvasProvider.release(ctx);
	}

	this.process = function(mode, srcContext, dstContext, alpha, offset) {
		var srcCanvas = srcContext.canvas,
			normal = mode === 'normal';
		if (normal || nativeModes[mode]) {
			dstContext.save();
			dstContext.setTransform(1, 0, 0, 1, 0, 0);
			dstContext.globalAlpha = alpha;
			if (!normal)
				dstContext.globalCompositeOperation = mode;
			dstContext.drawImage(srcCanvas, offset.x, offset.y);
			dstContext.restore();
		} else {
			var process = modes[mode];
			if (!process)
				return;
			var dstData = dstContext.getImageData(offset.x, offset.y,
					srcCanvas.width, srcCanvas.height),
				dst = dstData.data,
				src = srcContext.getImageData(0, 0,
					srcCanvas.width, srcCanvas.height).data;
			for (var i = 0, l = dst.length; i < l; i += 4) {
				sr = src[i];
				br = dst[i];
				sg = src[i + 1];
				bg = dst[i + 1];
				sb = src[i + 2];
				bb = dst[i + 2];
				sa = src[i + 3];
				ba = dst[i + 3];
				process();
				var a1 = sa * alpha / 255,
					a2 = 1 - a1;
				dst[i] = a1 * dr + a2 * br;
				dst[i + 1] = a1 * dg + a2 * bg;
				dst[i + 2] = a1 * db + a2 * bb;
				dst[i + 3] = sa * alpha + a2 * ba;
			}
			dstContext.putImageData(dstData, offset.x, offset.y);
		}
	};
};

var SvgElement = new function() {
	var svg = 'http://www.w3.org/2000/svg',
		xmlns = 'http://www.w3.org/2000/xmlns',
		xlink = 'http://www.w3.org/1999/xlink',
		attributeNamespace = {
			href: xlink,
			xlink: xmlns,
			xmlns: xmlns + '/',
			'xmlns:xlink': xmlns + '/'
		};

	function create(tag, attributes, formatter) {
		return set(document.createElementNS(svg, tag), attributes, formatter);
	}

	function get(node, name) {
		var namespace = attributeNamespace[name],
			value = namespace
				? node.getAttributeNS(namespace, name)
				: node.getAttribute(name);
		return value === 'null' ? null : value;
	}

	function set(node, attributes, formatter) {
		for (var name in attributes) {
			var value = attributes[name],
				namespace = attributeNamespace[name];
			if (typeof value === 'number' && formatter)
				value = formatter.number(value);
			if (namespace) {
				node.setAttributeNS(namespace, name, value);
			} else {
				node.setAttribute(name, value);
			}
		}
		return node;
	}

	return {
		svg: svg,
		xmlns: xmlns,
		xlink: xlink,

		create: create,
		get: get,
		set: set
	};
};

var SvgStyles = Base.each({
	fillColor: ['fill', 'color'],
	fillRule: ['fill-rule', 'string'],
	strokeColor: ['stroke', 'color'],
	strokeWidth: ['stroke-width', 'number'],
	strokeCap: ['stroke-linecap', 'string'],
	strokeJoin: ['stroke-linejoin', 'string'],
	strokeScaling: ['vector-effect', 'lookup', {
		true: 'none',
		false: 'non-scaling-stroke'
	}, function(item, value) {
		return !value
				&& (item instanceof PathItem
					|| item instanceof Shape
					|| item instanceof TextItem);
	}],
	miterLimit: ['stroke-miterlimit', 'number'],
	dashArray: ['stroke-dasharray', 'array'],
	dashOffset: ['stroke-dashoffset', 'number'],
	fontFamily: ['font-family', 'string'],
	fontWeight: ['font-weight', 'string'],
	fontSize: ['font-size', 'number'],
	justification: ['text-anchor', 'lookup', {
		left: 'start',
		center: 'middle',
		right: 'end'
	}],
	opacity: ['opacity', 'number'],
	blendMode: ['mix-blend-mode', 'style']
}, function(entry, key) {
	var part = Base.capitalize(key),
		lookup = entry[2];
	this[key] = {
		type: entry[1],
		property: key,
		attribute: entry[0],
		toSVG: lookup,
		fromSVG: lookup && Base.each(lookup, function(value, name) {
			this[value] = name;
		}, {}),
		exportFilter: entry[3],
		get: 'get' + part,
		set: 'set' + part
	};
}, {});

new function() {
	var formatter;

	function getTransform(matrix, coordinates, center) {
		var attrs = new Base(),
			trans = matrix.getTranslation();
		if (coordinates) {
			matrix = matrix._shiftless();
			var point = matrix._inverseTransform(trans);
			attrs[center ? 'cx' : 'x'] = point.x;
			attrs[center ? 'cy' : 'y'] = point.y;
			trans = null;
		}
		if (!matrix.isIdentity()) {
			var decomposed = matrix.decompose();
			if (decomposed) {
				var parts = [],
					angle = decomposed.rotation,
					scale = decomposed.scaling,
					skew = decomposed.skewing;
				if (trans && !trans.isZero())
					parts.push('translate(' + formatter.point(trans) + ')');
				if (angle)
					parts.push('rotate(' + formatter.number(angle) + ')');
				if (!Numerical.isZero(scale.x - 1)
						|| !Numerical.isZero(scale.y - 1))
					parts.push('scale(' + formatter.point(scale) +')');
				if (skew.x)
					parts.push('skewX(' + formatter.number(skew.x) + ')');
				if (skew.y)
					parts.push('skewY(' + formatter.number(skew.y) + ')');
				attrs.transform = parts.join(' ');
			} else {
				attrs.transform = 'matrix(' + matrix.getValues().join(',') + ')';
			}
		}
		return attrs;
	}

	function exportGroup(item, options) {
		var attrs = getTransform(item._matrix),
			children = item._children;
		var node = SvgElement.create('g', attrs, formatter);
		for (var i = 0, l = children.length; i < l; i++) {
			var child = children[i];
			var childNode = exportSVG(child, options);
			if (childNode) {
				if (child.isClipMask()) {
					var clip = SvgElement.create('clipPath');
					clip.appendChild(childNode);
					setDefinition(child, clip, 'clip');
					SvgElement.set(node, {
						'clip-path': 'url(#' + clip.id + ')'
					});
				} else {
					node.appendChild(childNode);
				}
			}
		}
		return node;
	}

	function exportRaster(item, options) {
		var attrs = getTransform(item._matrix, true),
			size = item.getSize(),
			image = item.getImage();
		attrs.x -= size.width / 2;
		attrs.y -= size.height / 2;
		attrs.width = size.width;
		attrs.height = size.height;
		attrs.href = options.embedImages == false && image && image.src
				|| item.toDataURL();
		return SvgElement.create('image', attrs, formatter);
	}

	function exportPath(item, options) {
		var matchShapes = options.matchShapes;
		if (matchShapes) {
			var shape = item.toShape(false);
			if (shape)
				return exportShape(shape, options);
		}
		var segments = item._segments,
			length = segments.length,
			type,
			attrs = getTransform(item._matrix);
		if (matchShapes && length >= 2 && !item.hasHandles()) {
			if (length > 2) {
				type = item._closed ? 'polygon' : 'polyline';
				var parts = [];
				for (var i = 0; i < length; i++) {
					parts.push(formatter.point(segments[i]._point));
				}
				attrs.points = parts.join(' ');
			} else {
				type = 'line';
				var start = segments[0]._point,
					end = segments[1]._point;
				attrs.set({
					x1: start.x,
					y1: start.y,
					x2: end.x,
					y2: end.y
				});
			}
		} else {
			type = 'path';
			attrs.d = item.getPathData(null, options.precision);
		}
		return SvgElement.create(type, attrs, formatter);
	}

	function exportShape(item) {
		var type = item._type,
			radius = item._radius,
			attrs = getTransform(item._matrix, true, type !== 'rectangle');
		if (type === 'rectangle') {
			type = 'rect';
			var size = item._size,
				width = size.width,
				height = size.height;
			attrs.x -= width / 2;
			attrs.y -= height / 2;
			attrs.width = width;
			attrs.height = height;
			if (radius.isZero())
				radius = null;
		}
		if (radius) {
			if (type === 'circle') {
				attrs.r = radius;
			} else {
				attrs.rx = radius.width;
				attrs.ry = radius.height;
			}
		}
		return SvgElement.create(type, attrs, formatter);
	}

	function exportCompoundPath(item, options) {
		var attrs = getTransform(item._matrix);
		var data = item.getPathData(null, options.precision);
		if (data)
			attrs.d = data;
		return SvgElement.create('path', attrs, formatter);
	}

	function exportSymbolItem(item, options) {
		var attrs = getTransform(item._matrix, true),
			definition = item._definition,
			node = getDefinition(definition, 'symbol'),
			definitionItem = definition._item,
			bounds = definitionItem.getBounds();
		if (!node) {
			node = SvgElement.create('symbol', {
				viewBox: formatter.rectangle(bounds)
			});
			node.appendChild(exportSVG(definitionItem, options));
			setDefinition(definition, node, 'symbol');
		}
		attrs.href = '#' + node.id;
		attrs.x += bounds.x;
		attrs.y += bounds.y;
		attrs.width = bounds.width;
		attrs.height = bounds.height;
		attrs.overflow = 'visible';
		return SvgElement.create('use', attrs, formatter);
	}

	function exportGradient(color) {
		var gradientNode = getDefinition(color, 'color');
		if (!gradientNode) {
			var gradient = color.getGradient(),
				radial = gradient._radial,
				origin = color.getOrigin(),
				destination = color.getDestination(),
				attrs;
			if (radial) {
				attrs = {
					cx: origin.x,
					cy: origin.y,
					r: origin.getDistance(destination)
				};
				var highlight = color.getHighlight();
				if (highlight) {
					attrs.fx = highlight.x;
					attrs.fy = highlight.y;
				}
			} else {
				attrs = {
					x1: origin.x,
					y1: origin.y,
					x2: destination.x,
					y2: destination.y
				};
			}
			attrs.gradientUnits = 'userSpaceOnUse';
			gradientNode = SvgElement.create((radial ? 'radial' : 'linear')
					+ 'Gradient', attrs, formatter);
			var stops = gradient._stops;
			for (var i = 0, l = stops.length; i < l; i++) {
				var stop = stops[i],
					stopColor = stop._color,
					alpha = stopColor.getAlpha(),
					offset = stop._offset;
				attrs = {
					offset: offset == null ? i / (l - 1) : offset
				};
				if (stopColor)
					attrs['stop-color'] = stopColor.toCSS(true);
				if (alpha < 1)
					attrs['stop-opacity'] = alpha;
				gradientNode.appendChild(
						SvgElement.create('stop', attrs, formatter));
			}
			setDefinition(color, gradientNode, 'color');
		}
		return 'url(#' + gradientNode.id + ')';
	}

	function exportText(item) {
		var node = SvgElement.create('text', getTransform(item._matrix, true),
				formatter);
		node.textContent = item._content;
		return node;
	}

	var exporters = {
		Group: exportGroup,
		Layer: exportGroup,
		Raster: exportRaster,
		Path: exportPath,
		Shape: exportShape,
		CompoundPath: exportCompoundPath,
		SymbolItem: exportSymbolItem,
		PointText: exportText
	};

	function applyStyle(item, node, isRoot) {
		var attrs = {},
			parent = !isRoot && item.getParent(),
			style = [];

		if (item._name != null)
			attrs.id = item._name;

		Base.each(SvgStyles, function(entry) {
			var get = entry.get,
				type = entry.type,
				value = item[get]();
			if (entry.exportFilter
					? entry.exportFilter(item, value)
					: !parent || !Base.equals(parent[get](), value)) {
				if (type === 'color' && value != null) {
					var alpha = value.getAlpha();
					if (alpha < 1)
						attrs[entry.attribute + '-opacity'] = alpha;
				}
				if (type === 'style') {
					style.push(entry.attribute + ': ' + value);
				} else {
					attrs[entry.attribute] = value == null ? 'none'
							: type === 'color' ? value.gradient
								? exportGradient(value, item)
								: value.toCSS(true)
							: type === 'array' ? value.join(',')
							: type === 'lookup' ? entry.toSVG[value]
							: value;
				}
			}
		});

		if (style.length)
			attrs.style = style.join(';');

		if (attrs.opacity === 1)
			delete attrs.opacity;

		if (!item._visible)
			attrs.visibility = 'hidden';

		return SvgElement.set(node, attrs, formatter);
	}

	var definitions;
	function getDefinition(item, type) {
		if (!definitions)
			definitions = { ids: {}, svgs: {} };
		return item && definitions.svgs[type + '-'
				+ (item._id || item.__id || (item.__id = UID.get('svg')))];
	}

	function setDefinition(item, node, type) {
		if (!definitions)
			getDefinition();
		var typeId = definitions.ids[type] = (definitions.ids[type] || 0) + 1;
		node.id = type + '-' + typeId;
		definitions.svgs[type + '-' + (item._id || item.__id)] = node;
	}

	function exportDefinitions(node, options) {
		var svg = node,
			defs = null;
		if (definitions) {
			svg = node.nodeName.toLowerCase() === 'svg' && node;
			for (var i in definitions.svgs) {
				if (!defs) {
					if (!svg) {
						svg = SvgElement.create('svg');
						svg.appendChild(node);
					}
					defs = svg.insertBefore(SvgElement.create('defs'),
							svg.firstChild);
				}
				defs.appendChild(definitions.svgs[i]);
			}
			definitions = null;
		}
		return options.asString
				? new self.XMLSerializer().serializeToString(svg)
				: svg;
	}

	function exportSVG(item, options, isRoot) {
		var exporter = exporters[item._class],
			node = exporter && exporter(item, options);
		if (node) {
			var onExport = options.onExport;
			if (onExport)
				node = onExport(item, node, options) || node;
			var data = JSON.stringify(item._data);
			if (data && data !== '{}' && data !== 'null')
				node.setAttribute('data-paper-data', data);
		}
		return node && applyStyle(item, node, isRoot);
	}

	function setOptions(options) {
		if (!options)
			options = {};
		formatter = new Formatter(options.precision);
		return options;
	}

	Item.inject({
		exportSVG: function(options) {
			options = setOptions(options);
			return exportDefinitions(exportSVG(this, options, true), options);
		}
	});

	Project.inject({
		exportSVG: function(options) {
			options = setOptions(options);
			var children = this._children,
				view = this.getView(),
				bounds = Base.pick(options.bounds, 'view'),
				mx = options.matrix || bounds === 'view' && view._matrix,
				matrix = mx && Matrix.read([mx]),
				rect = bounds === 'view'
					? new Rectangle([0, 0], view.getViewSize())
					: bounds === 'content'
						? Item._getBounds(children, matrix, { stroke: true })
							.rect
						: Rectangle.read([bounds], 0, { readNull: true }),
				attrs = {
					version: '1.1',
					xmlns: SvgElement.svg,
					'xmlns:xlink': SvgElement.xlink,
				};
			if (rect) {
				attrs.width = rect.width;
				attrs.height = rect.height;
				if (rect.x || rect.y)
					attrs.viewBox = formatter.rectangle(rect);
			}
			var node = SvgElement.create('svg', attrs, formatter),
				parent = node;
			if (matrix && !matrix.isIdentity()) {
				parent = node.appendChild(SvgElement.create('g',
						getTransform(matrix), formatter));
			}
			for (var i = 0, l = children.length; i < l; i++) {
				parent.appendChild(exportSVG(children[i], options, true));
			}
			return exportDefinitions(node, options);
		}
	});
};

new function() {

	var definitions = {},
		rootSize;

	function getValue(node, name, isString, allowNull, allowPercent) {
		var value = SvgElement.get(node, name),
			res = value == null
				? allowNull
					? null
					: isString ? '' : 0
				: isString
					? value
					: parseFloat(value);
		return /%\s*$/.test(value)
			? (res / 100) * (allowPercent ? 1
				: rootSize[/x|^width/.test(name) ? 'width' : 'height'])
			: res;
	}

	function getPoint(node, x, y, allowNull, allowPercent) {
		x = getValue(node, x || 'x', false, allowNull, allowPercent);
		y = getValue(node, y || 'y', false, allowNull, allowPercent);
		return allowNull && (x == null || y == null) ? null
				: new Point(x, y);
	}

	function getSize(node, w, h, allowNull, allowPercent) {
		w = getValue(node, w || 'width', false, allowNull, allowPercent);
		h = getValue(node, h || 'height', false, allowNull, allowPercent);
		return allowNull && (w == null || h == null) ? null
				: new Size(w, h);
	}

	function convertValue(value, type, lookup) {
		return value === 'none' ? null
				: type === 'number' ? parseFloat(value)
				: type === 'array' ?
					value ? value.split(/[\s,]+/g).map(parseFloat) : []
				: type === 'color' ? getDefinition(value) || value
				: type === 'lookup' ? lookup[value]
				: value;
	}

	function importGroup(node, type, options, isRoot) {
		var nodes = node.childNodes,
			isClip = type === 'clippath',
			isDefs = type === 'defs',
			item = new Group(),
			project = item._project,
			currentStyle = project._currentStyle,
			children = [];
		if (!isClip && !isDefs) {
			item = applyAttributes(item, node, isRoot);
			project._currentStyle = item._style.clone();
		}
		if (isRoot) {
			var defs = node.querySelectorAll('defs');
			for (var i = 0, l = defs.length; i < l; i++) {
				importNode(defs[i], options, false);
			}
		}
		for (var i = 0, l = nodes.length; i < l; i++) {
			var childNode = nodes[i],
				child;
			if (childNode.nodeType === 1
					&& !/^defs$/i.test(childNode.nodeName)
					&& (child = importNode(childNode, options, false))
					&& !(child instanceof SymbolDefinition))
				children.push(child);
		}
		item.addChildren(children);
		if (isClip)
			item = applyAttributes(item.reduce(), node, isRoot);
		project._currentStyle = currentStyle;
		if (isClip || isDefs) {
			item.remove();
			item = null;
		}
		return item;
	}

	function importPoly(node, type) {
		var coords = node.getAttribute('points').match(
					/[+-]?(?:\d*\.\d+|\d+\.?)(?:[eE][+-]?\d+)?/g),
			points = [];
		for (var i = 0, l = coords.length; i < l; i += 2)
			points.push(new Point(
					parseFloat(coords[i]),
					parseFloat(coords[i + 1])));
		var path = new Path(points);
		if (type === 'polygon')
			path.closePath();
		return path;
	}

	function importPath(node) {
		return PathItem.create(node.getAttribute('d'));
	}

	function importGradient(node, type) {
		var id = (getValue(node, 'href', true) || '').substring(1),
			radial = type === 'radialgradient',
			gradient;
		if (id) {
			gradient = definitions[id].getGradient();
			if (gradient._radial ^ radial) {
				gradient = gradient.clone();
				gradient._radial = radial;
			}
		} else {
			var nodes = node.childNodes,
				stops = [];
			for (var i = 0, l = nodes.length; i < l; i++) {
				var child = nodes[i];
				if (child.nodeType === 1)
					stops.push(applyAttributes(new GradientStop(), child));
			}
			gradient = new Gradient(stops, radial);
		}
		var origin, destination, highlight,
			scaleToBounds = getValue(node, 'gradientUnits', true) !==
				'userSpaceOnUse';
		if (radial) {
			origin = getPoint(node, 'cx', 'cy', false, scaleToBounds);
			destination = origin.add(
					getValue(node, 'r', false, false, scaleToBounds), 0);
			highlight = getPoint(node, 'fx', 'fy', true, scaleToBounds);
		} else {
			origin = getPoint(node, 'x1', 'y1', false, scaleToBounds);
			destination = getPoint(node, 'x2', 'y2', false, scaleToBounds);
		}
		var color = applyAttributes(
				new Color(gradient, origin, destination, highlight), node);
		color._scaleToBounds = scaleToBounds;
		return null;
	}

	var importers = {
		'#document': function (node, type, options, isRoot) {
			var nodes = node.childNodes;
			for (var i = 0, l = nodes.length; i < l; i++) {
				var child = nodes[i];
				if (child.nodeType === 1)
					return importNode(child, options, isRoot);
			}
		},
		g: importGroup,
		svg: importGroup,
		clippath: importGroup,
		polygon: importPoly,
		polyline: importPoly,
		path: importPath,
		lineargradient: importGradient,
		radialgradient: importGradient,

		image: function (node) {
			var raster = new Raster(getValue(node, 'href', true));
			raster.on('load', function() {
				var size = getSize(node);
				this.setSize(size);
				var center = getPoint(node).add(size.divide(2));
				this._matrix.append(new Matrix().translate(center));
			});
			return raster;
		},

		symbol: function(node, type, options, isRoot) {
			return new SymbolDefinition(
					importGroup(node, type, options, isRoot), true);
		},

		defs: importGroup,

		use: function(node) {
			var id = (getValue(node, 'href', true) || '').substring(1),
				definition = definitions[id],
				point = getPoint(node);
			return definition
					? definition instanceof SymbolDefinition
						? definition.place(point)
						: definition.clone().translate(point)
					: null;
		},

		circle: function(node) {
			return new Shape.Circle(
					getPoint(node, 'cx', 'cy'),
					getValue(node, 'r'));
		},

		ellipse: function(node) {
			return new Shape.Ellipse({
				center: getPoint(node, 'cx', 'cy'),
				radius: getSize(node, 'rx', 'ry')
			});
		},

		rect: function(node) {
			return new Shape.Rectangle(new Rectangle(
						getPoint(node),
						getSize(node)
					), getSize(node, 'rx', 'ry'));
			},

		line: function(node) {
			return new Path.Line(
					getPoint(node, 'x1', 'y1'),
					getPoint(node, 'x2', 'y2'));
		},

		text: function(node) {
			var text = new PointText(getPoint(node).add(
					getPoint(node, 'dx', 'dy')));
			text.setContent(node.textContent.trim() || '');
			return text;
		}
	};

	function applyTransform(item, value, name, node) {
		if (item.transform) {
			var transforms = (node.getAttribute(name) || '').split(/\)\s*/g),
				matrix = new Matrix();
			for (var i = 0, l = transforms.length; i < l; i++) {
				var transform = transforms[i];
				if (!transform)
					break;
				var parts = transform.split(/\(\s*/),
					command = parts[0],
					v = parts[1].split(/[\s,]+/g);
				for (var j = 0, m = v.length; j < m; j++)
					v[j] = parseFloat(v[j]);
				switch (command) {
				case 'matrix':
					matrix.append(
							new Matrix(v[0], v[1], v[2], v[3], v[4], v[5]));
					break;
				case 'rotate':
					matrix.rotate(v[0], v[1] || 0, v[2] || 0);
					break;
				case 'translate':
					matrix.translate(v[0], v[1] || 0);
					break;
				case 'scale':
					matrix.scale(v);
					break;
				case 'skewX':
					matrix.skew(v[0], 0);
					break;
				case 'skewY':
					matrix.skew(0, v[0]);
					break;
				}
			}
			item.transform(matrix);
		}
	}

	function applyOpacity(item, value, name) {
		var key = name === 'fill-opacity' ? 'getFillColor' : 'getStrokeColor',
			color = item[key] && item[key]();
		if (color)
			color.setAlpha(parseFloat(value));
	}

	var attributes = Base.set(Base.each(SvgStyles, function(entry) {
		this[entry.attribute] = function(item, value) {
			if (item[entry.set]) {
				item[entry.set](convertValue(value, entry.type, entry.fromSVG));
				if (entry.type === 'color') {
					var color = item[entry.get]();
					if (color) {
						if (color._scaleToBounds) {
							var bounds = item.getBounds();
							color.transform(new Matrix()
								.translate(bounds.getPoint())
								.scale(bounds.getSize()));
						}
					}
				}
			}
		};
	}, {}), {
		id: function(item, value) {
			definitions[value] = item;
			if (item.setName)
				item.setName(value);
		},

		'clip-path': function(item, value) {
			var clip = getDefinition(value);
			if (clip) {
				clip = clip.clone();
				clip.setClipMask(true);
				if (item instanceof Group) {
					item.insertChild(0, clip);
				} else {
					return new Group(clip, item);
				}
			}
		},

		gradientTransform: applyTransform,
		transform: applyTransform,

		'fill-opacity': applyOpacity,
		'stroke-opacity': applyOpacity,

		visibility: function(item, value) {
			if (item.setVisible)
				item.setVisible(value === 'visible');
		},

		display: function(item, value) {
			if (item.setVisible)
				item.setVisible(value !== null);
		},

		'stop-color': function(item, value) {
			if (item.setColor)
				item.setColor(value);
		},

		'stop-opacity': function(item, value) {
			if (item._color)
				item._color.setAlpha(parseFloat(value));
		},

		offset: function(item, value) {
			if (item.setOffset) {
				var percent = value.match(/(.*)%$/);
				item.setOffset(percent ? percent[1] / 100 : parseFloat(value));
			}
		},

		viewBox: function(item, value, name, node, styles) {
			var rect = new Rectangle(convertValue(value, 'array')),
				size = getSize(node, null, null, true),
				group,
				matrix;
			if (item instanceof Group) {
				var scale = size ? size.divide(rect.getSize()) : 1,
				matrix = new Matrix().scale(scale)
						.translate(rect.getPoint().negate());
				group = item;
			} else if (item instanceof SymbolDefinition) {
				if (size)
					rect.setSize(size);
				group = item._item;
			}
			if (group)  {
				if (getAttribute(node, 'overflow', styles) !== 'visible') {
					var clip = new Shape.Rectangle(rect);
					clip.setClipMask(true);
					group.addChild(clip);
				}
				if (matrix)
					group.transform(matrix);
			}
		}
	});

	function getAttribute(node, name, styles) {
		var attr = node.attributes[name],
			value = attr && attr.value;
		if (!value && node.style) {
			var style = Base.camelize(name);
			value = node.style[style];
			if (!value && styles.node[style] !== styles.parent[style])
				value = styles.node[style];
		}
		return !value ? undefined
				: value === 'none' ? null
				: value;
	}

	function applyAttributes(item, node, isRoot) {
		var parent = node.parentNode,
			styles = {
				node: DomElement.getStyles(node) || {},
				parent: !isRoot && !/^defs$/i.test(parent.tagName)
						&& DomElement.getStyles(parent) || {}
			};
		Base.each(attributes, function(apply, name) {
			var value = getAttribute(node, name, styles);
			item = value !== undefined
					&& apply(item, value, name, node, styles) || item;
		});
		return item;
	}

	function getDefinition(value) {
		var match = value && value.match(/\((?:["'#]*)([^"')]+)/),
			name = match && match[1],
			res = name && definitions[window
					? name.replace(window.location.href.split('#')[0] + '#', '')
					: name];
		if (res && res._scaleToBounds) {
			res = res.clone();
			res._scaleToBounds = true;
		}
		return res;
	}

	function importNode(node, options, isRoot) {
		var type = node.nodeName.toLowerCase(),
			isElement = type !== '#document',
			body = document.body,
			container,
			parent,
			next;
		if (isRoot && isElement) {
			rootSize = paper.getView().getSize();
			rootSize = getSize(node, null, null, true) || rootSize;
			container = SvgElement.create('svg', {
				style: 'stroke-width: 1px; stroke-miterlimit: 10'
			});
			parent = node.parentNode;
			next = node.nextSibling;
			container.appendChild(node);
			body.appendChild(container);
		}
		var settings = paper.settings,
			applyMatrix = settings.applyMatrix,
			insertItems = settings.insertItems;
		settings.applyMatrix = false;
		settings.insertItems = false;
		var importer = importers[type],
			item = importer && importer(node, type, options, isRoot) || null;
		settings.insertItems = insertItems;
		settings.applyMatrix = applyMatrix;
		if (item) {
			if (isElement && !(item instanceof Group))
				item = applyAttributes(item, node, isRoot);
			var onImport = options.onImport,
				data = isElement && node.getAttribute('data-paper-data');
			if (onImport)
				item = onImport(node, item, options) || item;
			if (options.expandShapes && item instanceof Shape) {
				item.remove();
				item = item.toPath();
			}
			if (data)
				item._data = JSON.parse(data);
		}
		if (container) {
			body.removeChild(container);
			if (parent) {
				if (next) {
					parent.insertBefore(node, next);
				} else {
					parent.appendChild(node);
				}
			}
		}
		if (isRoot) {
			definitions = {};
			if (item && Base.pick(options.applyMatrix, applyMatrix))
				item.matrix.apply(true, true);
		}
		return item;
	}

	function importSVG(source, options, owner) {
		if (!source)
			return null;
		options = typeof options === 'function' ? { onLoad: options }
				: options || {};
		var scope = paper,
			item = null;

		function onLoad(svg) {
			try {
				var node = typeof svg === 'object' ? svg : new self.DOMParser()
						.parseFromString(svg, 'image/svg+xml');
				if (!node.nodeName) {
					node = null;
					throw new Error('Unsupported SVG source: ' + source);
				}
				paper = scope;
				item = importNode(node, options, true);
				if (!options || options.insert !== false) {
					owner._insertItem(undefined, item);
				}
				var onLoad = options.onLoad;
				if (onLoad)
					onLoad(item, svg);
			} catch (e) {
				onError(e);
			}
		}

		function onError(message, status) {
			var onError = options.onError;
			if (onError) {
				onError(message, status);
			} else {
				throw new Error(message);
			}
		}

		if (typeof source === 'string' && !/^.*</.test(source)) {
			var node = document.getElementById(source);
			if (node) {
				onLoad(node);
			} else {
				Http.request({
					url: source,
					async: true,
					onLoad: onLoad,
					onError: onError
				});
			}
		} else if (typeof File !== 'undefined' && source instanceof File) {
			var reader = new FileReader();
			reader.onload = function() {
				onLoad(reader.result);
			};
			reader.onerror = function() {
				onError(reader.error);
			};
			return reader.readAsText(source);
		} else {
			onLoad(source);
		}

		return item;
	}

	Item.inject({
		importSVG: function(node, options) {
			return importSVG(node, options, this);
		}
	});

	Project.inject({
		importSVG: function(node, options) {
			this.activate();
			return importSVG(node, options, this);
		}
	});
};

Base.exports.PaperScript = function() {
	var global = this,
		acorn = global.acorn;
	if (!acorn && "function" !== 'undefined') {
		try { acorn = __webpack_require__(/*! acorn */ "./node_modules/acorn/dist/acorn.js"); } catch(e) {}
	}
	if (!acorn) {
		var exports, module;
		acorn = exports = module = {};

(function(root, mod) {
  if (typeof exports == "object" && typeof module == "object") return mod(exports);
  if (true) return !(__WEBPACK_AMD_DEFINE_ARRAY__ = [exports], __WEBPACK_AMD_DEFINE_FACTORY__ = (mod),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  mod(root.acorn || (root.acorn = {}));
})(this, function(exports) {
  "use strict";

  exports.version = "0.5.0";

  var options, input, inputLen, sourceFile;

  exports.parse = function(inpt, opts) {
	input = String(inpt); inputLen = input.length;
	setOptions(opts);
	initTokenState();
	return parseTopLevel(options.program);
  };

  var defaultOptions = exports.defaultOptions = {
	ecmaVersion: 5,
	strictSemicolons: false,
	allowTrailingCommas: true,
	forbidReserved: false,
	allowReturnOutsideFunction: false,
	locations: false,
	onComment: null,
	ranges: false,
	program: null,
	sourceFile: null,
	directSourceFile: null
  };

  function setOptions(opts) {
	options = opts || {};
	for (var opt in defaultOptions) if (!Object.prototype.hasOwnProperty.call(options, opt))
	  options[opt] = defaultOptions[opt];
	sourceFile = options.sourceFile || null;
  }

  var getLineInfo = exports.getLineInfo = function(input, offset) {
	for (var line = 1, cur = 0;;) {
	  lineBreak.lastIndex = cur;
	  var match = lineBreak.exec(input);
	  if (match && match.index < offset) {
		++line;
		cur = match.index + match[0].length;
	  } else break;
	}
	return {line: line, column: offset - cur};
  };

  exports.tokenize = function(inpt, opts) {
	input = String(inpt); inputLen = input.length;
	setOptions(opts);
	initTokenState();

	var t = {};
	function getToken(forceRegexp) {
	  lastEnd = tokEnd;
	  readToken(forceRegexp);
	  t.start = tokStart; t.end = tokEnd;
	  t.startLoc = tokStartLoc; t.endLoc = tokEndLoc;
	  t.type = tokType; t.value = tokVal;
	  return t;
	}
	getToken.jumpTo = function(pos, reAllowed) {
	  tokPos = pos;
	  if (options.locations) {
		tokCurLine = 1;
		tokLineStart = lineBreak.lastIndex = 0;
		var match;
		while ((match = lineBreak.exec(input)) && match.index < pos) {
		  ++tokCurLine;
		  tokLineStart = match.index + match[0].length;
		}
	  }
	  tokRegexpAllowed = reAllowed;
	  skipSpace();
	};
	return getToken;
  };

  var tokPos;

  var tokStart, tokEnd;

  var tokStartLoc, tokEndLoc;

  var tokType, tokVal;

  var tokRegexpAllowed;

  var tokCurLine, tokLineStart;

  var lastStart, lastEnd, lastEndLoc;

  var inFunction, labels, strict;

  function raise(pos, message) {
	var loc = getLineInfo(input, pos);
	message += " (" + loc.line + ":" + loc.column + ")";
	var err = new SyntaxError(message);
	err.pos = pos; err.loc = loc; err.raisedAt = tokPos;
	throw err;
  }

  var empty = [];

  var _num = {type: "num"}, _regexp = {type: "regexp"}, _string = {type: "string"};
  var _name = {type: "name"}, _eof = {type: "eof"};

  var _break = {keyword: "break"}, _case = {keyword: "case", beforeExpr: true}, _catch = {keyword: "catch"};
  var _continue = {keyword: "continue"}, _debugger = {keyword: "debugger"}, _default = {keyword: "default"};
  var _do = {keyword: "do", isLoop: true}, _else = {keyword: "else", beforeExpr: true};
  var _finally = {keyword: "finally"}, _for = {keyword: "for", isLoop: true}, _function = {keyword: "function"};
  var _if = {keyword: "if"}, _return = {keyword: "return", beforeExpr: true}, _switch = {keyword: "switch"};
  var _throw = {keyword: "throw", beforeExpr: true}, _try = {keyword: "try"}, _var = {keyword: "var"};
  var _while = {keyword: "while", isLoop: true}, _with = {keyword: "with"}, _new = {keyword: "new", beforeExpr: true};
  var _this = {keyword: "this"};

  var _null = {keyword: "null", atomValue: null}, _true = {keyword: "true", atomValue: true};
  var _false = {keyword: "false", atomValue: false};

  var _in = {keyword: "in", binop: 7, beforeExpr: true};

  var keywordTypes = {"break": _break, "case": _case, "catch": _catch,
					  "continue": _continue, "debugger": _debugger, "default": _default,
					  "do": _do, "else": _else, "finally": _finally, "for": _for,
					  "function": _function, "if": _if, "return": _return, "switch": _switch,
					  "throw": _throw, "try": _try, "var": _var, "while": _while, "with": _with,
					  "null": _null, "true": _true, "false": _false, "new": _new, "in": _in,
					  "instanceof": {keyword: "instanceof", binop: 7, beforeExpr: true}, "this": _this,
					  "typeof": {keyword: "typeof", prefix: true, beforeExpr: true},
					  "void": {keyword: "void", prefix: true, beforeExpr: true},
					  "delete": {keyword: "delete", prefix: true, beforeExpr: true}};

  var _bracketL = {type: "[", beforeExpr: true}, _bracketR = {type: "]"}, _braceL = {type: "{", beforeExpr: true};
  var _braceR = {type: "}"}, _parenL = {type: "(", beforeExpr: true}, _parenR = {type: ")"};
  var _comma = {type: ",", beforeExpr: true}, _semi = {type: ";", beforeExpr: true};
  var _colon = {type: ":", beforeExpr: true}, _dot = {type: "."}, _question = {type: "?", beforeExpr: true};

  var _slash = {binop: 10, beforeExpr: true}, _eq = {isAssign: true, beforeExpr: true};
  var _assign = {isAssign: true, beforeExpr: true};
  var _incDec = {postfix: true, prefix: true, isUpdate: true}, _prefix = {prefix: true, beforeExpr: true};
  var _logicalOR = {binop: 1, beforeExpr: true};
  var _logicalAND = {binop: 2, beforeExpr: true};
  var _bitwiseOR = {binop: 3, beforeExpr: true};
  var _bitwiseXOR = {binop: 4, beforeExpr: true};
  var _bitwiseAND = {binop: 5, beforeExpr: true};
  var _equality = {binop: 6, beforeExpr: true};
  var _relational = {binop: 7, beforeExpr: true};
  var _bitShift = {binop: 8, beforeExpr: true};
  var _plusMin = {binop: 9, prefix: true, beforeExpr: true};
  var _multiplyModulo = {binop: 10, beforeExpr: true};

  exports.tokTypes = {bracketL: _bracketL, bracketR: _bracketR, braceL: _braceL, braceR: _braceR,
					  parenL: _parenL, parenR: _parenR, comma: _comma, semi: _semi, colon: _colon,
					  dot: _dot, question: _question, slash: _slash, eq: _eq, name: _name, eof: _eof,
					  num: _num, regexp: _regexp, string: _string};
  for (var kw in keywordTypes) exports.tokTypes["_" + kw] = keywordTypes[kw];

  function makePredicate(words) {
	words = words.split(" ");
	var f = "", cats = [];
	out: for (var i = 0; i < words.length; ++i) {
	  for (var j = 0; j < cats.length; ++j)
		if (cats[j][0].length == words[i].length) {
		  cats[j].push(words[i]);
		  continue out;
		}
	  cats.push([words[i]]);
	}
	function compareTo(arr) {
	  if (arr.length == 1) return f += "return str === " + JSON.stringify(arr[0]) + ";";
	  f += "switch(str){";
	  for (var i = 0; i < arr.length; ++i) f += "case " + JSON.stringify(arr[i]) + ":";
	  f += "return true}return false;";
	}

	if (cats.length > 3) {
	  cats.sort(function(a, b) {return b.length - a.length;});
	  f += "switch(str.length){";
	  for (var i = 0; i < cats.length; ++i) {
		var cat = cats[i];
		f += "case " + cat[0].length + ":";
		compareTo(cat);
	  }
	  f += "}";

	} else {
	  compareTo(words);
	}
	return new Function("str", f);
  }

  var isReservedWord3 = makePredicate("abstract boolean byte char class double enum export extends final float goto implements import int interface long native package private protected public short static super synchronized throws transient volatile");

  var isReservedWord5 = makePredicate("class enum extends super const export import");

  var isStrictReservedWord = makePredicate("implements interface let package private protected public static yield");

  var isStrictBadIdWord = makePredicate("eval arguments");

  var isKeyword = makePredicate("break case catch continue debugger default do else finally for function if return switch throw try var while with null true false instanceof typeof void delete new in this");

  var nonASCIIwhitespace = /[\u1680\u180e\u2000-\u200a\u202f\u205f\u3000\ufeff]/;
  var nonASCIIidentifierStartChars = "\xaa\xb5\xba\xc0-\xd6\xd8-\xf6\xf8-\u02c1\u02c6-\u02d1\u02e0-\u02e4\u02ec\u02ee\u0370-\u0374\u0376\u0377\u037a-\u037d\u0386\u0388-\u038a\u038c\u038e-\u03a1\u03a3-\u03f5\u03f7-\u0481\u048a-\u0527\u0531-\u0556\u0559\u0561-\u0587\u05d0-\u05ea\u05f0-\u05f2\u0620-\u064a\u066e\u066f\u0671-\u06d3\u06d5\u06e5\u06e6\u06ee\u06ef\u06fa-\u06fc\u06ff\u0710\u0712-\u072f\u074d-\u07a5\u07b1\u07ca-\u07ea\u07f4\u07f5\u07fa\u0800-\u0815\u081a\u0824\u0828\u0840-\u0858\u08a0\u08a2-\u08ac\u0904-\u0939\u093d\u0950\u0958-\u0961\u0971-\u0977\u0979-\u097f\u0985-\u098c\u098f\u0990\u0993-\u09a8\u09aa-\u09b0\u09b2\u09b6-\u09b9\u09bd\u09ce\u09dc\u09dd\u09df-\u09e1\u09f0\u09f1\u0a05-\u0a0a\u0a0f\u0a10\u0a13-\u0a28\u0a2a-\u0a30\u0a32\u0a33\u0a35\u0a36\u0a38\u0a39\u0a59-\u0a5c\u0a5e\u0a72-\u0a74\u0a85-\u0a8d\u0a8f-\u0a91\u0a93-\u0aa8\u0aaa-\u0ab0\u0ab2\u0ab3\u0ab5-\u0ab9\u0abd\u0ad0\u0ae0\u0ae1\u0b05-\u0b0c\u0b0f\u0b10\u0b13-\u0b28\u0b2a-\u0b30\u0b32\u0b33\u0b35-\u0b39\u0b3d\u0b5c\u0b5d\u0b5f-\u0b61\u0b71\u0b83\u0b85-\u0b8a\u0b8e-\u0b90\u0b92-\u0b95\u0b99\u0b9a\u0b9c\u0b9e\u0b9f\u0ba3\u0ba4\u0ba8-\u0baa\u0bae-\u0bb9\u0bd0\u0c05-\u0c0c\u0c0e-\u0c10\u0c12-\u0c28\u0c2a-\u0c33\u0c35-\u0c39\u0c3d\u0c58\u0c59\u0c60\u0c61\u0c85-\u0c8c\u0c8e-\u0c90\u0c92-\u0ca8\u0caa-\u0cb3\u0cb5-\u0cb9\u0cbd\u0cde\u0ce0\u0ce1\u0cf1\u0cf2\u0d05-\u0d0c\u0d0e-\u0d10\u0d12-\u0d3a\u0d3d\u0d4e\u0d60\u0d61\u0d7a-\u0d7f\u0d85-\u0d96\u0d9a-\u0db1\u0db3-\u0dbb\u0dbd\u0dc0-\u0dc6\u0e01-\u0e30\u0e32\u0e33\u0e40-\u0e46\u0e81\u0e82\u0e84\u0e87\u0e88\u0e8a\u0e8d\u0e94-\u0e97\u0e99-\u0e9f\u0ea1-\u0ea3\u0ea5\u0ea7\u0eaa\u0eab\u0ead-\u0eb0\u0eb2\u0eb3\u0ebd\u0ec0-\u0ec4\u0ec6\u0edc-\u0edf\u0f00\u0f40-\u0f47\u0f49-\u0f6c\u0f88-\u0f8c\u1000-\u102a\u103f\u1050-\u1055\u105a-\u105d\u1061\u1065\u1066\u106e-\u1070\u1075-\u1081\u108e\u10a0-\u10c5\u10c7\u10cd\u10d0-\u10fa\u10fc-\u1248\u124a-\u124d\u1250-\u1256\u1258\u125a-\u125d\u1260-\u1288\u128a-\u128d\u1290-\u12b0\u12b2-\u12b5\u12b8-\u12be\u12c0\u12c2-\u12c5\u12c8-\u12d6\u12d8-\u1310\u1312-\u1315\u1318-\u135a\u1380-\u138f\u13a0-\u13f4\u1401-\u166c\u166f-\u167f\u1681-\u169a\u16a0-\u16ea\u16ee-\u16f0\u1700-\u170c\u170e-\u1711\u1720-\u1731\u1740-\u1751\u1760-\u176c\u176e-\u1770\u1780-\u17b3\u17d7\u17dc\u1820-\u1877\u1880-\u18a8\u18aa\u18b0-\u18f5\u1900-\u191c\u1950-\u196d\u1970-\u1974\u1980-\u19ab\u19c1-\u19c7\u1a00-\u1a16\u1a20-\u1a54\u1aa7\u1b05-\u1b33\u1b45-\u1b4b\u1b83-\u1ba0\u1bae\u1baf\u1bba-\u1be5\u1c00-\u1c23\u1c4d-\u1c4f\u1c5a-\u1c7d\u1ce9-\u1cec\u1cee-\u1cf1\u1cf5\u1cf6\u1d00-\u1dbf\u1e00-\u1f15\u1f18-\u1f1d\u1f20-\u1f45\u1f48-\u1f4d\u1f50-\u1f57\u1f59\u1f5b\u1f5d\u1f5f-\u1f7d\u1f80-\u1fb4\u1fb6-\u1fbc\u1fbe\u1fc2-\u1fc4\u1fc6-\u1fcc\u1fd0-\u1fd3\u1fd6-\u1fdb\u1fe0-\u1fec\u1ff2-\u1ff4\u1ff6-\u1ffc\u2071\u207f\u2090-\u209c\u2102\u2107\u210a-\u2113\u2115\u2119-\u211d\u2124\u2126\u2128\u212a-\u212d\u212f-\u2139\u213c-\u213f\u2145-\u2149\u214e\u2160-\u2188\u2c00-\u2c2e\u2c30-\u2c5e\u2c60-\u2ce4\u2ceb-\u2cee\u2cf2\u2cf3\u2d00-\u2d25\u2d27\u2d2d\u2d30-\u2d67\u2d6f\u2d80-\u2d96\u2da0-\u2da6\u2da8-\u2dae\u2db0-\u2db6\u2db8-\u2dbe\u2dc0-\u2dc6\u2dc8-\u2dce\u2dd0-\u2dd6\u2dd8-\u2dde\u2e2f\u3005-\u3007\u3021-\u3029\u3031-\u3035\u3038-\u303c\u3041-\u3096\u309d-\u309f\u30a1-\u30fa\u30fc-\u30ff\u3105-\u312d\u3131-\u318e\u31a0-\u31ba\u31f0-\u31ff\u3400-\u4db5\u4e00-\u9fcc\ua000-\ua48c\ua4d0-\ua4fd\ua500-\ua60c\ua610-\ua61f\ua62a\ua62b\ua640-\ua66e\ua67f-\ua697\ua6a0-\ua6ef\ua717-\ua71f\ua722-\ua788\ua78b-\ua78e\ua790-\ua793\ua7a0-\ua7aa\ua7f8-\ua801\ua803-\ua805\ua807-\ua80a\ua80c-\ua822\ua840-\ua873\ua882-\ua8b3\ua8f2-\ua8f7\ua8fb\ua90a-\ua925\ua930-\ua946\ua960-\ua97c\ua984-\ua9b2\ua9cf\uaa00-\uaa28\uaa40-\uaa42\uaa44-\uaa4b\uaa60-\uaa76\uaa7a\uaa80-\uaaaf\uaab1\uaab5\uaab6\uaab9-\uaabd\uaac0\uaac2\uaadb-\uaadd\uaae0-\uaaea\uaaf2-\uaaf4\uab01-\uab06\uab09-\uab0e\uab11-\uab16\uab20-\uab26\uab28-\uab2e\uabc0-\uabe2\uac00-\ud7a3\ud7b0-\ud7c6\ud7cb-\ud7fb\uf900-\ufa6d\ufa70-\ufad9\ufb00-\ufb06\ufb13-\ufb17\ufb1d\ufb1f-\ufb28\ufb2a-\ufb36\ufb38-\ufb3c\ufb3e\ufb40\ufb41\ufb43\ufb44\ufb46-\ufbb1\ufbd3-\ufd3d\ufd50-\ufd8f\ufd92-\ufdc7\ufdf0-\ufdfb\ufe70-\ufe74\ufe76-\ufefc\uff21-\uff3a\uff41-\uff5a\uff66-\uffbe\uffc2-\uffc7\uffca-\uffcf\uffd2-\uffd7\uffda-\uffdc";
  var nonASCIIidentifierChars = "\u0300-\u036f\u0483-\u0487\u0591-\u05bd\u05bf\u05c1\u05c2\u05c4\u05c5\u05c7\u0610-\u061a\u0620-\u0649\u0672-\u06d3\u06e7-\u06e8\u06fb-\u06fc\u0730-\u074a\u0800-\u0814\u081b-\u0823\u0825-\u0827\u0829-\u082d\u0840-\u0857\u08e4-\u08fe\u0900-\u0903\u093a-\u093c\u093e-\u094f\u0951-\u0957\u0962-\u0963\u0966-\u096f\u0981-\u0983\u09bc\u09be-\u09c4\u09c7\u09c8\u09d7\u09df-\u09e0\u0a01-\u0a03\u0a3c\u0a3e-\u0a42\u0a47\u0a48\u0a4b-\u0a4d\u0a51\u0a66-\u0a71\u0a75\u0a81-\u0a83\u0abc\u0abe-\u0ac5\u0ac7-\u0ac9\u0acb-\u0acd\u0ae2-\u0ae3\u0ae6-\u0aef\u0b01-\u0b03\u0b3c\u0b3e-\u0b44\u0b47\u0b48\u0b4b-\u0b4d\u0b56\u0b57\u0b5f-\u0b60\u0b66-\u0b6f\u0b82\u0bbe-\u0bc2\u0bc6-\u0bc8\u0bca-\u0bcd\u0bd7\u0be6-\u0bef\u0c01-\u0c03\u0c46-\u0c48\u0c4a-\u0c4d\u0c55\u0c56\u0c62-\u0c63\u0c66-\u0c6f\u0c82\u0c83\u0cbc\u0cbe-\u0cc4\u0cc6-\u0cc8\u0cca-\u0ccd\u0cd5\u0cd6\u0ce2-\u0ce3\u0ce6-\u0cef\u0d02\u0d03\u0d46-\u0d48\u0d57\u0d62-\u0d63\u0d66-\u0d6f\u0d82\u0d83\u0dca\u0dcf-\u0dd4\u0dd6\u0dd8-\u0ddf\u0df2\u0df3\u0e34-\u0e3a\u0e40-\u0e45\u0e50-\u0e59\u0eb4-\u0eb9\u0ec8-\u0ecd\u0ed0-\u0ed9\u0f18\u0f19\u0f20-\u0f29\u0f35\u0f37\u0f39\u0f41-\u0f47\u0f71-\u0f84\u0f86-\u0f87\u0f8d-\u0f97\u0f99-\u0fbc\u0fc6\u1000-\u1029\u1040-\u1049\u1067-\u106d\u1071-\u1074\u1082-\u108d\u108f-\u109d\u135d-\u135f\u170e-\u1710\u1720-\u1730\u1740-\u1750\u1772\u1773\u1780-\u17b2\u17dd\u17e0-\u17e9\u180b-\u180d\u1810-\u1819\u1920-\u192b\u1930-\u193b\u1951-\u196d\u19b0-\u19c0\u19c8-\u19c9\u19d0-\u19d9\u1a00-\u1a15\u1a20-\u1a53\u1a60-\u1a7c\u1a7f-\u1a89\u1a90-\u1a99\u1b46-\u1b4b\u1b50-\u1b59\u1b6b-\u1b73\u1bb0-\u1bb9\u1be6-\u1bf3\u1c00-\u1c22\u1c40-\u1c49\u1c5b-\u1c7d\u1cd0-\u1cd2\u1d00-\u1dbe\u1e01-\u1f15\u200c\u200d\u203f\u2040\u2054\u20d0-\u20dc\u20e1\u20e5-\u20f0\u2d81-\u2d96\u2de0-\u2dff\u3021-\u3028\u3099\u309a\ua640-\ua66d\ua674-\ua67d\ua69f\ua6f0-\ua6f1\ua7f8-\ua800\ua806\ua80b\ua823-\ua827\ua880-\ua881\ua8b4-\ua8c4\ua8d0-\ua8d9\ua8f3-\ua8f7\ua900-\ua909\ua926-\ua92d\ua930-\ua945\ua980-\ua983\ua9b3-\ua9c0\uaa00-\uaa27\uaa40-\uaa41\uaa4c-\uaa4d\uaa50-\uaa59\uaa7b\uaae0-\uaae9\uaaf2-\uaaf3\uabc0-\uabe1\uabec\uabed\uabf0-\uabf9\ufb20-\ufb28\ufe00-\ufe0f\ufe20-\ufe26\ufe33\ufe34\ufe4d-\ufe4f\uff10-\uff19\uff3f";
  var nonASCIIidentifierStart = new RegExp("[" + nonASCIIidentifierStartChars + "]");
  var nonASCIIidentifier = new RegExp("[" + nonASCIIidentifierStartChars + nonASCIIidentifierChars + "]");

  var newline = /[\n\r\u2028\u2029]/;

  var lineBreak = /\r\n|[\n\r\u2028\u2029]/g;

  var isIdentifierStart = exports.isIdentifierStart = function(code) {
	if (code < 65) return code === 36;
	if (code < 91) return true;
	if (code < 97) return code === 95;
	if (code < 123)return true;
	return code >= 0xaa && nonASCIIidentifierStart.test(String.fromCharCode(code));
  };

  var isIdentifierChar = exports.isIdentifierChar = function(code) {
	if (code < 48) return code === 36;
	if (code < 58) return true;
	if (code < 65) return false;
	if (code < 91) return true;
	if (code < 97) return code === 95;
	if (code < 123)return true;
	return code >= 0xaa && nonASCIIidentifier.test(String.fromCharCode(code));
  };

  function line_loc_t() {
	this.line = tokCurLine;
	this.column = tokPos - tokLineStart;
  }

  function initTokenState() {
	tokCurLine = 1;
	tokPos = tokLineStart = 0;
	tokRegexpAllowed = true;
	skipSpace();
  }

  function finishToken(type, val) {
	tokEnd = tokPos;
	if (options.locations) tokEndLoc = new line_loc_t;
	tokType = type;
	skipSpace();
	tokVal = val;
	tokRegexpAllowed = type.beforeExpr;
  }

  function skipBlockComment() {
	var startLoc = options.onComment && options.locations && new line_loc_t;
	var start = tokPos, end = input.indexOf("*/", tokPos += 2);
	if (end === -1) raise(tokPos - 2, "Unterminated comment");
	tokPos = end + 2;
	if (options.locations) {
	  lineBreak.lastIndex = start;
	  var match;
	  while ((match = lineBreak.exec(input)) && match.index < tokPos) {
		++tokCurLine;
		tokLineStart = match.index + match[0].length;
	  }
	}
	if (options.onComment)
	  options.onComment(true, input.slice(start + 2, end), start, tokPos,
						startLoc, options.locations && new line_loc_t);
  }

  function skipLineComment() {
	var start = tokPos;
	var startLoc = options.onComment && options.locations && new line_loc_t;
	var ch = input.charCodeAt(tokPos+=2);
	while (tokPos < inputLen && ch !== 10 && ch !== 13 && ch !== 8232 && ch !== 8233) {
	  ++tokPos;
	  ch = input.charCodeAt(tokPos);
	}
	if (options.onComment)
	  options.onComment(false, input.slice(start + 2, tokPos), start, tokPos,
						startLoc, options.locations && new line_loc_t);
  }

  function skipSpace() {
	while (tokPos < inputLen) {
	  var ch = input.charCodeAt(tokPos);
	  if (ch === 32) {
		++tokPos;
	  } else if (ch === 13) {
		++tokPos;
		var next = input.charCodeAt(tokPos);
		if (next === 10) {
		  ++tokPos;
		}
		if (options.locations) {
		  ++tokCurLine;
		  tokLineStart = tokPos;
		}
	  } else if (ch === 10 || ch === 8232 || ch === 8233) {
		++tokPos;
		if (options.locations) {
		  ++tokCurLine;
		  tokLineStart = tokPos;
		}
	  } else if (ch > 8 && ch < 14) {
		++tokPos;
	  } else if (ch === 47) {
		var next = input.charCodeAt(tokPos + 1);
		if (next === 42) {
		  skipBlockComment();
		} else if (next === 47) {
		  skipLineComment();
		} else break;
	  } else if (ch === 160) {
		++tokPos;
	  } else if (ch >= 5760 && nonASCIIwhitespace.test(String.fromCharCode(ch))) {
		++tokPos;
	  } else {
		break;
	  }
	}
  }

  function readToken_dot() {
	var next = input.charCodeAt(tokPos + 1);
	if (next >= 48 && next <= 57) return readNumber(true);
	++tokPos;
	return finishToken(_dot);
  }

  function readToken_slash() {
	var next = input.charCodeAt(tokPos + 1);
	if (tokRegexpAllowed) {++tokPos; return readRegexp();}
	if (next === 61) return finishOp(_assign, 2);
	return finishOp(_slash, 1);
  }

  function readToken_mult_modulo() {
	var next = input.charCodeAt(tokPos + 1);
	if (next === 61) return finishOp(_assign, 2);
	return finishOp(_multiplyModulo, 1);
  }

  function readToken_pipe_amp(code) {
	var next = input.charCodeAt(tokPos + 1);
	if (next === code) return finishOp(code === 124 ? _logicalOR : _logicalAND, 2);
	if (next === 61) return finishOp(_assign, 2);
	return finishOp(code === 124 ? _bitwiseOR : _bitwiseAND, 1);
  }

  function readToken_caret() {
	var next = input.charCodeAt(tokPos + 1);
	if (next === 61) return finishOp(_assign, 2);
	return finishOp(_bitwiseXOR, 1);
  }

  function readToken_plus_min(code) {
	var next = input.charCodeAt(tokPos + 1);
	if (next === code) {
	  if (next == 45 && input.charCodeAt(tokPos + 2) == 62 &&
		  newline.test(input.slice(lastEnd, tokPos))) {
		tokPos += 3;
		skipLineComment();
		skipSpace();
		return readToken();
	  }
	  return finishOp(_incDec, 2);
	}
	if (next === 61) return finishOp(_assign, 2);
	return finishOp(_plusMin, 1);
  }

  function readToken_lt_gt(code) {
	var next = input.charCodeAt(tokPos + 1);
	var size = 1;
	if (next === code) {
	  size = code === 62 && input.charCodeAt(tokPos + 2) === 62 ? 3 : 2;
	  if (input.charCodeAt(tokPos + size) === 61) return finishOp(_assign, size + 1);
	  return finishOp(_bitShift, size);
	}
	if (next == 33 && code == 60 && input.charCodeAt(tokPos + 2) == 45 &&
		input.charCodeAt(tokPos + 3) == 45) {
	  tokPos += 4;
	  skipLineComment();
	  skipSpace();
	  return readToken();
	}
	if (next === 61)
	  size = input.charCodeAt(tokPos + 2) === 61 ? 3 : 2;
	return finishOp(_relational, size);
  }

  function readToken_eq_excl(code) {
	var next = input.charCodeAt(tokPos + 1);
	if (next === 61) return finishOp(_equality, input.charCodeAt(tokPos + 2) === 61 ? 3 : 2);
	return finishOp(code === 61 ? _eq : _prefix, 1);
  }

  function getTokenFromCode(code) {
	switch(code) {
	case 46:
	  return readToken_dot();

	case 40: ++tokPos; return finishToken(_parenL);
	case 41: ++tokPos; return finishToken(_parenR);
	case 59: ++tokPos; return finishToken(_semi);
	case 44: ++tokPos; return finishToken(_comma);
	case 91: ++tokPos; return finishToken(_bracketL);
	case 93: ++tokPos; return finishToken(_bracketR);
	case 123: ++tokPos; return finishToken(_braceL);
	case 125: ++tokPos; return finishToken(_braceR);
	case 58: ++tokPos; return finishToken(_colon);
	case 63: ++tokPos; return finishToken(_question);

	case 48:
	  var next = input.charCodeAt(tokPos + 1);
	  if (next === 120 || next === 88) return readHexNumber();
	case 49: case 50: case 51: case 52: case 53: case 54: case 55: case 56: case 57:
	  return readNumber(false);

	case 34: case 39:
	  return readString(code);

	case 47:
	  return readToken_slash(code);

	case 37: case 42:
	  return readToken_mult_modulo();

	case 124: case 38:
	  return readToken_pipe_amp(code);

	case 94:
	  return readToken_caret();

	case 43: case 45:
	  return readToken_plus_min(code);

	case 60: case 62:
	  return readToken_lt_gt(code);

	case 61: case 33:
	  return readToken_eq_excl(code);

	case 126:
	  return finishOp(_prefix, 1);
	}

	return false;
  }

  function readToken(forceRegexp) {
	if (!forceRegexp) tokStart = tokPos;
	else tokPos = tokStart + 1;
	if (options.locations) tokStartLoc = new line_loc_t;
	if (forceRegexp) return readRegexp();
	if (tokPos >= inputLen) return finishToken(_eof);

	var code = input.charCodeAt(tokPos);
	if (isIdentifierStart(code) || code === 92 ) return readWord();

	var tok = getTokenFromCode(code);

	if (tok === false) {
	  var ch = String.fromCharCode(code);
	  if (ch === "\\" || nonASCIIidentifierStart.test(ch)) return readWord();
	  raise(tokPos, "Unexpected character '" + ch + "'");
	}
	return tok;
  }

  function finishOp(type, size) {
	var str = input.slice(tokPos, tokPos + size);
	tokPos += size;
	finishToken(type, str);
  }

  function readRegexp() {
	var content = "", escaped, inClass, start = tokPos;
	for (;;) {
	  if (tokPos >= inputLen) raise(start, "Unterminated regular expression");
	  var ch = input.charAt(tokPos);
	  if (newline.test(ch)) raise(start, "Unterminated regular expression");
	  if (!escaped) {
		if (ch === "[") inClass = true;
		else if (ch === "]" && inClass) inClass = false;
		else if (ch === "/" && !inClass) break;
		escaped = ch === "\\";
	  } else escaped = false;
	  ++tokPos;
	}
	var content = input.slice(start, tokPos);
	++tokPos;
	var mods = readWord1();
	if (mods && !/^[gmsiy]*$/.test(mods)) raise(start, "Invalid regexp flag");
	try {
	  var value = new RegExp(content, mods);
	} catch (e) {
	  if (e instanceof SyntaxError) raise(start, e.message);
	  raise(e);
	}
	return finishToken(_regexp, value);
  }

  function readInt(radix, len) {
	var start = tokPos, total = 0;
	for (var i = 0, e = len == null ? Infinity : len; i < e; ++i) {
	  var code = input.charCodeAt(tokPos), val;
	  if (code >= 97) val = code - 97 + 10;
	  else if (code >= 65) val = code - 65 + 10;
	  else if (code >= 48 && code <= 57) val = code - 48;
	  else val = Infinity;
	  if (val >= radix) break;
	  ++tokPos;
	  total = total * radix + val;
	}
	if (tokPos === start || len != null && tokPos - start !== len) return null;

	return total;
  }

  function readHexNumber() {
	tokPos += 2;
	var val = readInt(16);
	if (val == null) raise(tokStart + 2, "Expected hexadecimal number");
	if (isIdentifierStart(input.charCodeAt(tokPos))) raise(tokPos, "Identifier directly after number");
	return finishToken(_num, val);
  }

  function readNumber(startsWithDot) {
	var start = tokPos, isFloat = false, octal = input.charCodeAt(tokPos) === 48;
	if (!startsWithDot && readInt(10) === null) raise(start, "Invalid number");
	if (input.charCodeAt(tokPos) === 46) {
	  ++tokPos;
	  readInt(10);
	  isFloat = true;
	}
	var next = input.charCodeAt(tokPos);
	if (next === 69 || next === 101) {
	  next = input.charCodeAt(++tokPos);
	  if (next === 43 || next === 45) ++tokPos;
	  if (readInt(10) === null) raise(start, "Invalid number");
	  isFloat = true;
	}
	if (isIdentifierStart(input.charCodeAt(tokPos))) raise(tokPos, "Identifier directly after number");

	var str = input.slice(start, tokPos), val;
	if (isFloat) val = parseFloat(str);
	else if (!octal || str.length === 1) val = parseInt(str, 10);
	else if (/[89]/.test(str) || strict) raise(start, "Invalid number");
	else val = parseInt(str, 8);
	return finishToken(_num, val);
  }

  function readString(quote) {
	tokPos++;
	var out = "";
	for (;;) {
	  if (tokPos >= inputLen) raise(tokStart, "Unterminated string constant");
	  var ch = input.charCodeAt(tokPos);
	  if (ch === quote) {
		++tokPos;
		return finishToken(_string, out);
	  }
	  if (ch === 92) {
		ch = input.charCodeAt(++tokPos);
		var octal = /^[0-7]+/.exec(input.slice(tokPos, tokPos + 3));
		if (octal) octal = octal[0];
		while (octal && parseInt(octal, 8) > 255) octal = octal.slice(0, -1);
		if (octal === "0") octal = null;
		++tokPos;
		if (octal) {
		  if (strict) raise(tokPos - 2, "Octal literal in strict mode");
		  out += String.fromCharCode(parseInt(octal, 8));
		  tokPos += octal.length - 1;
		} else {
		  switch (ch) {
		  case 110: out += "\n"; break;
		  case 114: out += "\r"; break;
		  case 120: out += String.fromCharCode(readHexChar(2)); break;
		  case 117: out += String.fromCharCode(readHexChar(4)); break;
		  case 85: out += String.fromCharCode(readHexChar(8)); break;
		  case 116: out += "\t"; break;
		  case 98: out += "\b"; break;
		  case 118: out += "\u000b"; break;
		  case 102: out += "\f"; break;
		  case 48: out += "\0"; break;
		  case 13: if (input.charCodeAt(tokPos) === 10) ++tokPos;
		  case 10:
			if (options.locations) { tokLineStart = tokPos; ++tokCurLine; }
			break;
		  default: out += String.fromCharCode(ch); break;
		  }
		}
	  } else {
		if (ch === 13 || ch === 10 || ch === 8232 || ch === 8233) raise(tokStart, "Unterminated string constant");
		out += String.fromCharCode(ch);
		++tokPos;
	  }
	}
  }

  function readHexChar(len) {
	var n = readInt(16, len);
	if (n === null) raise(tokStart, "Bad character escape sequence");
	return n;
  }

  var containsEsc;

  function readWord1() {
	containsEsc = false;
	var word, first = true, start = tokPos;
	for (;;) {
	  var ch = input.charCodeAt(tokPos);
	  if (isIdentifierChar(ch)) {
		if (containsEsc) word += input.charAt(tokPos);
		++tokPos;
	  } else if (ch === 92) {
		if (!containsEsc) word = input.slice(start, tokPos);
		containsEsc = true;
		if (input.charCodeAt(++tokPos) != 117)
		  raise(tokPos, "Expecting Unicode escape sequence \\uXXXX");
		++tokPos;
		var esc = readHexChar(4);
		var escStr = String.fromCharCode(esc);
		if (!escStr) raise(tokPos - 1, "Invalid Unicode escape");
		if (!(first ? isIdentifierStart(esc) : isIdentifierChar(esc)))
		  raise(tokPos - 4, "Invalid Unicode escape");
		word += escStr;
	  } else {
		break;
	  }
	  first = false;
	}
	return containsEsc ? word : input.slice(start, tokPos);
  }

  function readWord() {
	var word = readWord1();
	var type = _name;
	if (!containsEsc && isKeyword(word))
	  type = keywordTypes[word];
	return finishToken(type, word);
  }

  function next() {
	lastStart = tokStart;
	lastEnd = tokEnd;
	lastEndLoc = tokEndLoc;
	readToken();
  }

  function setStrict(strct) {
	strict = strct;
	tokPos = tokStart;
	if (options.locations) {
	  while (tokPos < tokLineStart) {
		tokLineStart = input.lastIndexOf("\n", tokLineStart - 2) + 1;
		--tokCurLine;
	  }
	}
	skipSpace();
	readToken();
  }

  function node_t() {
	this.type = null;
	this.start = tokStart;
	this.end = null;
  }

  function node_loc_t() {
	this.start = tokStartLoc;
	this.end = null;
	if (sourceFile !== null) this.source = sourceFile;
  }

  function startNode() {
	var node = new node_t();
	if (options.locations)
	  node.loc = new node_loc_t();
	if (options.directSourceFile)
	  node.sourceFile = options.directSourceFile;
	if (options.ranges)
	  node.range = [tokStart, 0];
	return node;
  }

  function startNodeFrom(other) {
	var node = new node_t();
	node.start = other.start;
	if (options.locations) {
	  node.loc = new node_loc_t();
	  node.loc.start = other.loc.start;
	}
	if (options.ranges)
	  node.range = [other.range[0], 0];

	return node;
  }

  function finishNode(node, type) {
	node.type = type;
	node.end = lastEnd;
	if (options.locations)
	  node.loc.end = lastEndLoc;
	if (options.ranges)
	  node.range[1] = lastEnd;
	return node;
  }

  function isUseStrict(stmt) {
	return options.ecmaVersion >= 5 && stmt.type === "ExpressionStatement" &&
	  stmt.expression.type === "Literal" && stmt.expression.value === "use strict";
  }

  function eat(type) {
	if (tokType === type) {
	  next();
	  return true;
	}
  }

  function canInsertSemicolon() {
	return !options.strictSemicolons &&
	  (tokType === _eof || tokType === _braceR || newline.test(input.slice(lastEnd, tokStart)));
  }

  function semicolon() {
	if (!eat(_semi) && !canInsertSemicolon()) unexpected();
  }

  function expect(type) {
	if (tokType === type) next();
	else unexpected();
  }

  function unexpected() {
	raise(tokStart, "Unexpected token");
  }

  function checkLVal(expr) {
	if (expr.type !== "Identifier" && expr.type !== "MemberExpression")
	  raise(expr.start, "Assigning to rvalue");
	if (strict && expr.type === "Identifier" && isStrictBadIdWord(expr.name))
	  raise(expr.start, "Assigning to " + expr.name + " in strict mode");
  }

  function parseTopLevel(program) {
	lastStart = lastEnd = tokPos;
	if (options.locations) lastEndLoc = new line_loc_t;
	inFunction = strict = null;
	labels = [];
	readToken();

	var node = program || startNode(), first = true;
	if (!program) node.body = [];
	while (tokType !== _eof) {
	  var stmt = parseStatement();
	  node.body.push(stmt);
	  if (first && isUseStrict(stmt)) setStrict(true);
	  first = false;
	}
	return finishNode(node, "Program");
  }

  var loopLabel = {kind: "loop"}, switchLabel = {kind: "switch"};

  function parseStatement() {
	if (tokType === _slash || tokType === _assign && tokVal == "/=")
	  readToken(true);

	var starttype = tokType, node = startNode();

	switch (starttype) {
	case _break: case _continue:
	  next();
	  var isBreak = starttype === _break;
	  if (eat(_semi) || canInsertSemicolon()) node.label = null;
	  else if (tokType !== _name) unexpected();
	  else {
		node.label = parseIdent();
		semicolon();
	  }

	  for (var i = 0; i < labels.length; ++i) {
		var lab = labels[i];
		if (node.label == null || lab.name === node.label.name) {
		  if (lab.kind != null && (isBreak || lab.kind === "loop")) break;
		  if (node.label && isBreak) break;
		}
	  }
	  if (i === labels.length) raise(node.start, "Unsyntactic " + starttype.keyword);
	  return finishNode(node, isBreak ? "BreakStatement" : "ContinueStatement");

	case _debugger:
	  next();
	  semicolon();
	  return finishNode(node, "DebuggerStatement");

	case _do:
	  next();
	  labels.push(loopLabel);
	  node.body = parseStatement();
	  labels.pop();
	  expect(_while);
	  node.test = parseParenExpression();
	  semicolon();
	  return finishNode(node, "DoWhileStatement");

	case _for:
	  next();
	  labels.push(loopLabel);
	  expect(_parenL);
	  if (tokType === _semi) return parseFor(node, null);
	  if (tokType === _var) {
		var init = startNode();
		next();
		parseVar(init, true);
		finishNode(init, "VariableDeclaration");
		if (init.declarations.length === 1 && eat(_in))
		  return parseForIn(node, init);
		return parseFor(node, init);
	  }
	  var init = parseExpression(false, true);
	  if (eat(_in)) {checkLVal(init); return parseForIn(node, init);}
	  return parseFor(node, init);

	case _function:
	  next();
	  return parseFunction(node, true);

	case _if:
	  next();
	  node.test = parseParenExpression();
	  node.consequent = parseStatement();
	  node.alternate = eat(_else) ? parseStatement() : null;
	  return finishNode(node, "IfStatement");

	case _return:
	  if (!inFunction && !options.allowReturnOutsideFunction)
		raise(tokStart, "'return' outside of function");
	  next();

	  if (eat(_semi) || canInsertSemicolon()) node.argument = null;
	  else { node.argument = parseExpression(); semicolon(); }
	  return finishNode(node, "ReturnStatement");

	case _switch:
	  next();
	  node.discriminant = parseParenExpression();
	  node.cases = [];
	  expect(_braceL);
	  labels.push(switchLabel);

	  for (var cur, sawDefault; tokType != _braceR;) {
		if (tokType === _case || tokType === _default) {
		  var isCase = tokType === _case;
		  if (cur) finishNode(cur, "SwitchCase");
		  node.cases.push(cur = startNode());
		  cur.consequent = [];
		  next();
		  if (isCase) cur.test = parseExpression();
		  else {
			if (sawDefault) raise(lastStart, "Multiple default clauses"); sawDefault = true;
			cur.test = null;
		  }
		  expect(_colon);
		} else {
		  if (!cur) unexpected();
		  cur.consequent.push(parseStatement());
		}
	  }
	  if (cur) finishNode(cur, "SwitchCase");
	  next();
	  labels.pop();
	  return finishNode(node, "SwitchStatement");

	case _throw:
	  next();
	  if (newline.test(input.slice(lastEnd, tokStart)))
		raise(lastEnd, "Illegal newline after throw");
	  node.argument = parseExpression();
	  semicolon();
	  return finishNode(node, "ThrowStatement");

	case _try:
	  next();
	  node.block = parseBlock();
	  node.handler = null;
	  if (tokType === _catch) {
		var clause = startNode();
		next();
		expect(_parenL);
		clause.param = parseIdent();
		if (strict && isStrictBadIdWord(clause.param.name))
		  raise(clause.param.start, "Binding " + clause.param.name + " in strict mode");
		expect(_parenR);
		clause.guard = null;
		clause.body = parseBlock();
		node.handler = finishNode(clause, "CatchClause");
	  }
	  node.guardedHandlers = empty;
	  node.finalizer = eat(_finally) ? parseBlock() : null;
	  if (!node.handler && !node.finalizer)
		raise(node.start, "Missing catch or finally clause");
	  return finishNode(node, "TryStatement");

	case _var:
	  next();
	  parseVar(node);
	  semicolon();
	  return finishNode(node, "VariableDeclaration");

	case _while:
	  next();
	  node.test = parseParenExpression();
	  labels.push(loopLabel);
	  node.body = parseStatement();
	  labels.pop();
	  return finishNode(node, "WhileStatement");

	case _with:
	  if (strict) raise(tokStart, "'with' in strict mode");
	  next();
	  node.object = parseParenExpression();
	  node.body = parseStatement();
	  return finishNode(node, "WithStatement");

	case _braceL:
	  return parseBlock();

	case _semi:
	  next();
	  return finishNode(node, "EmptyStatement");

	default:
	  var maybeName = tokVal, expr = parseExpression();
	  if (starttype === _name && expr.type === "Identifier" && eat(_colon)) {
		for (var i = 0; i < labels.length; ++i)
		  if (labels[i].name === maybeName) raise(expr.start, "Label '" + maybeName + "' is already declared");
		var kind = tokType.isLoop ? "loop" : tokType === _switch ? "switch" : null;
		labels.push({name: maybeName, kind: kind});
		node.body = parseStatement();
		labels.pop();
		node.label = expr;
		return finishNode(node, "LabeledStatement");
	  } else {
		node.expression = expr;
		semicolon();
		return finishNode(node, "ExpressionStatement");
	  }
	}
  }

  function parseParenExpression() {
	expect(_parenL);
	var val = parseExpression();
	expect(_parenR);
	return val;
  }

  function parseBlock(allowStrict) {
	var node = startNode(), first = true, strict = false, oldStrict;
	node.body = [];
	expect(_braceL);
	while (!eat(_braceR)) {
	  var stmt = parseStatement();
	  node.body.push(stmt);
	  if (first && allowStrict && isUseStrict(stmt)) {
		oldStrict = strict;
		setStrict(strict = true);
	  }
	  first = false;
	}
	if (strict && !oldStrict) setStrict(false);
	return finishNode(node, "BlockStatement");
  }

  function parseFor(node, init) {
	node.init = init;
	expect(_semi);
	node.test = tokType === _semi ? null : parseExpression();
	expect(_semi);
	node.update = tokType === _parenR ? null : parseExpression();
	expect(_parenR);
	node.body = parseStatement();
	labels.pop();
	return finishNode(node, "ForStatement");
  }

  function parseForIn(node, init) {
	node.left = init;
	node.right = parseExpression();
	expect(_parenR);
	node.body = parseStatement();
	labels.pop();
	return finishNode(node, "ForInStatement");
  }

  function parseVar(node, noIn) {
	node.declarations = [];
	node.kind = "var";
	for (;;) {
	  var decl = startNode();
	  decl.id = parseIdent();
	  if (strict && isStrictBadIdWord(decl.id.name))
		raise(decl.id.start, "Binding " + decl.id.name + " in strict mode");
	  decl.init = eat(_eq) ? parseExpression(true, noIn) : null;
	  node.declarations.push(finishNode(decl, "VariableDeclarator"));
	  if (!eat(_comma)) break;
	}
	return node;
  }

  function parseExpression(noComma, noIn) {
	var expr = parseMaybeAssign(noIn);
	if (!noComma && tokType === _comma) {
	  var node = startNodeFrom(expr);
	  node.expressions = [expr];
	  while (eat(_comma)) node.expressions.push(parseMaybeAssign(noIn));
	  return finishNode(node, "SequenceExpression");
	}
	return expr;
  }

  function parseMaybeAssign(noIn) {
	var left = parseMaybeConditional(noIn);
	if (tokType.isAssign) {
	  var node = startNodeFrom(left);
	  node.operator = tokVal;
	  node.left = left;
	  next();
	  node.right = parseMaybeAssign(noIn);
	  checkLVal(left);
	  return finishNode(node, "AssignmentExpression");
	}
	return left;
  }

  function parseMaybeConditional(noIn) {
	var expr = parseExprOps(noIn);
	if (eat(_question)) {
	  var node = startNodeFrom(expr);
	  node.test = expr;
	  node.consequent = parseExpression(true);
	  expect(_colon);
	  node.alternate = parseExpression(true, noIn);
	  return finishNode(node, "ConditionalExpression");
	}
	return expr;
  }

  function parseExprOps(noIn) {
	return parseExprOp(parseMaybeUnary(), -1, noIn);
  }

  function parseExprOp(left, minPrec, noIn) {
	var prec = tokType.binop;
	if (prec != null && (!noIn || tokType !== _in)) {
	  if (prec > minPrec) {
		var node = startNodeFrom(left);
		node.left = left;
		node.operator = tokVal;
		var op = tokType;
		next();
		node.right = parseExprOp(parseMaybeUnary(), prec, noIn);
		var exprNode = finishNode(node, (op === _logicalOR || op === _logicalAND) ? "LogicalExpression" : "BinaryExpression");
		return parseExprOp(exprNode, minPrec, noIn);
	  }
	}
	return left;
  }

  function parseMaybeUnary() {
	if (tokType.prefix) {
	  var node = startNode(), update = tokType.isUpdate;
	  node.operator = tokVal;
	  node.prefix = true;
	  tokRegexpAllowed = true;
	  next();
	  node.argument = parseMaybeUnary();
	  if (update) checkLVal(node.argument);
	  else if (strict && node.operator === "delete" &&
			   node.argument.type === "Identifier")
		raise(node.start, "Deleting local variable in strict mode");
	  return finishNode(node, update ? "UpdateExpression" : "UnaryExpression");
	}
	var expr = parseExprSubscripts();
	while (tokType.postfix && !canInsertSemicolon()) {
	  var node = startNodeFrom(expr);
	  node.operator = tokVal;
	  node.prefix = false;
	  node.argument = expr;
	  checkLVal(expr);
	  next();
	  expr = finishNode(node, "UpdateExpression");
	}
	return expr;
  }

  function parseExprSubscripts() {
	return parseSubscripts(parseExprAtom());
  }

  function parseSubscripts(base, noCalls) {
	if (eat(_dot)) {
	  var node = startNodeFrom(base);
	  node.object = base;
	  node.property = parseIdent(true);
	  node.computed = false;
	  return parseSubscripts(finishNode(node, "MemberExpression"), noCalls);
	} else if (eat(_bracketL)) {
	  var node = startNodeFrom(base);
	  node.object = base;
	  node.property = parseExpression();
	  node.computed = true;
	  expect(_bracketR);
	  return parseSubscripts(finishNode(node, "MemberExpression"), noCalls);
	} else if (!noCalls && eat(_parenL)) {
	  var node = startNodeFrom(base);
	  node.callee = base;
	  node.arguments = parseExprList(_parenR, false);
	  return parseSubscripts(finishNode(node, "CallExpression"), noCalls);
	} else return base;
  }

  function parseExprAtom() {
	switch (tokType) {
	case _this:
	  var node = startNode();
	  next();
	  return finishNode(node, "ThisExpression");
	case _name:
	  return parseIdent();
	case _num: case _string: case _regexp:
	  var node = startNode();
	  node.value = tokVal;
	  node.raw = input.slice(tokStart, tokEnd);
	  next();
	  return finishNode(node, "Literal");

	case _null: case _true: case _false:
	  var node = startNode();
	  node.value = tokType.atomValue;
	  node.raw = tokType.keyword;
	  next();
	  return finishNode(node, "Literal");

	case _parenL:
	  var tokStartLoc1 = tokStartLoc, tokStart1 = tokStart;
	  next();
	  var val = parseExpression();
	  val.start = tokStart1;
	  val.end = tokEnd;
	  if (options.locations) {
		val.loc.start = tokStartLoc1;
		val.loc.end = tokEndLoc;
	  }
	  if (options.ranges)
		val.range = [tokStart1, tokEnd];
	  expect(_parenR);
	  return val;

	case _bracketL:
	  var node = startNode();
	  next();
	  node.elements = parseExprList(_bracketR, true, true);
	  return finishNode(node, "ArrayExpression");

	case _braceL:
	  return parseObj();

	case _function:
	  var node = startNode();
	  next();
	  return parseFunction(node, false);

	case _new:
	  return parseNew();

	default:
	  unexpected();
	}
  }

  function parseNew() {
	var node = startNode();
	next();
	node.callee = parseSubscripts(parseExprAtom(), true);
	if (eat(_parenL)) node.arguments = parseExprList(_parenR, false);
	else node.arguments = empty;
	return finishNode(node, "NewExpression");
  }

  function parseObj() {
	var node = startNode(), first = true, sawGetSet = false;
	node.properties = [];
	next();
	while (!eat(_braceR)) {
	  if (!first) {
		expect(_comma);
		if (options.allowTrailingCommas && eat(_braceR)) break;
	  } else first = false;

	  var prop = {key: parsePropertyName()}, isGetSet = false, kind;
	  if (eat(_colon)) {
		prop.value = parseExpression(true);
		kind = prop.kind = "init";
	  } else if (options.ecmaVersion >= 5 && prop.key.type === "Identifier" &&
				 (prop.key.name === "get" || prop.key.name === "set")) {
		isGetSet = sawGetSet = true;
		kind = prop.kind = prop.key.name;
		prop.key = parsePropertyName();
		if (tokType !== _parenL) unexpected();
		prop.value = parseFunction(startNode(), false);
	  } else unexpected();

	  if (prop.key.type === "Identifier" && (strict || sawGetSet)) {
		for (var i = 0; i < node.properties.length; ++i) {
		  var other = node.properties[i];
		  if (other.key.name === prop.key.name) {
			var conflict = kind == other.kind || isGetSet && other.kind === "init" ||
			  kind === "init" && (other.kind === "get" || other.kind === "set");
			if (conflict && !strict && kind === "init" && other.kind === "init") conflict = false;
			if (conflict) raise(prop.key.start, "Redefinition of property");
		  }
		}
	  }
	  node.properties.push(prop);
	}
	return finishNode(node, "ObjectExpression");
  }

  function parsePropertyName() {
	if (tokType === _num || tokType === _string) return parseExprAtom();
	return parseIdent(true);
  }

  function parseFunction(node, isStatement) {
	if (tokType === _name) node.id = parseIdent();
	else if (isStatement) unexpected();
	else node.id = null;
	node.params = [];
	var first = true;
	expect(_parenL);
	while (!eat(_parenR)) {
	  if (!first) expect(_comma); else first = false;
	  node.params.push(parseIdent());
	}

	var oldInFunc = inFunction, oldLabels = labels;
	inFunction = true; labels = [];
	node.body = parseBlock(true);
	inFunction = oldInFunc; labels = oldLabels;

	if (strict || node.body.body.length && isUseStrict(node.body.body[0])) {
	  for (var i = node.id ? -1 : 0; i < node.params.length; ++i) {
		var id = i < 0 ? node.id : node.params[i];
		if (isStrictReservedWord(id.name) || isStrictBadIdWord(id.name))
		  raise(id.start, "Defining '" + id.name + "' in strict mode");
		if (i >= 0) for (var j = 0; j < i; ++j) if (id.name === node.params[j].name)
		  raise(id.start, "Argument name clash in strict mode");
	  }
	}

	return finishNode(node, isStatement ? "FunctionDeclaration" : "FunctionExpression");
  }

  function parseExprList(close, allowTrailingComma, allowEmpty) {
	var elts = [], first = true;
	while (!eat(close)) {
	  if (!first) {
		expect(_comma);
		if (allowTrailingComma && options.allowTrailingCommas && eat(close)) break;
	  } else first = false;

	  if (allowEmpty && tokType === _comma) elts.push(null);
	  else elts.push(parseExpression(true));
	}
	return elts;
  }

  function parseIdent(liberal) {
	var node = startNode();
	if (liberal && options.forbidReserved == "everywhere") liberal = false;
	if (tokType === _name) {
	  if (!liberal &&
		  (options.forbidReserved &&
		   (options.ecmaVersion === 3 ? isReservedWord3 : isReservedWord5)(tokVal) ||
		   strict && isStrictReservedWord(tokVal)) &&
		  input.slice(tokStart, tokEnd).indexOf("\\") == -1)
		raise(tokStart, "The keyword '" + tokVal + "' is reserved");
	  node.name = tokVal;
	} else if (liberal && tokType.keyword) {
	  node.name = tokType.keyword;
	} else {
	  unexpected();
	}
	tokRegexpAllowed = false;
	next();
	return finishNode(node, "Identifier");
  }

});

		if (!acorn.version)
			acorn = null;
	}

	function parse(code, options) {
		return (global.acorn || acorn).parse(code, options);
	}

	var binaryOperators = {
		'+': '__add',
		'-': '__subtract',
		'*': '__multiply',
		'/': '__divide',
		'%': '__modulo',
		'==': '__equals',
		'!=': '__equals'
	};

	var unaryOperators = {
		'-': '__negate',
		'+': '__self'
	};

	var fields = Base.each(
		['add', 'subtract', 'multiply', 'divide', 'modulo', 'equals', 'negate'],
		function(name) {
			this['__' + name] = '#' + name;
		},
		{
			__self: function() {
				return this;
			}
		}
	);
	Point.inject(fields);
	Size.inject(fields);
	Color.inject(fields);

	function __$__(left, operator, right) {
		var handler = binaryOperators[operator];
		if (left && left[handler]) {
			var res = left[handler](right);
			return operator === '!=' ? !res : res;
		}
		switch (operator) {
		case '+': return left + right;
		case '-': return left - right;
		case '*': return left * right;
		case '/': return left / right;
		case '%': return left % right;
		case '==': return left == right;
		case '!=': return left != right;
		}
	}

	function $__(operator, value) {
		var handler = unaryOperators[operator];
		if (value && value[handler])
			return value[handler]();
		switch (operator) {
		case '+': return +value;
		case '-': return -value;
		}
	}

	function compile(code, options) {
		if (!code)
			return '';
		options = options || {};

		var insertions = [];

		function getOffset(offset) {
			for (var i = 0, l = insertions.length; i < l; i++) {
				var insertion = insertions[i];
				if (insertion[0] >= offset)
					break;
				offset += insertion[1];
			}
			return offset;
		}

		function getCode(node) {
			return code.substring(getOffset(node.range[0]),
					getOffset(node.range[1]));
		}

		function getBetween(left, right) {
			return code.substring(getOffset(left.range[1]),
					getOffset(right.range[0]));
		}

		function replaceCode(node, str) {
			var start = getOffset(node.range[0]),
				end = getOffset(node.range[1]),
				insert = 0;
			for (var i = insertions.length - 1; i >= 0; i--) {
				if (start > insertions[i][0]) {
					insert = i + 1;
					break;
				}
			}
			insertions.splice(insert, 0, [start, str.length - end + start]);
			code = code.substring(0, start) + str + code.substring(end);
		}

		function walkAST(node, parent) {
			if (!node)
				return;
			for (var key in node) {
				if (key === 'range' || key === 'loc')
					continue;
				var value = node[key];
				if (Array.isArray(value)) {
					for (var i = 0, l = value.length; i < l; i++)
						walkAST(value[i], node);
				} else if (value && typeof value === 'object') {
					walkAST(value, node);
				}
			}
			switch (node.type) {
			case 'UnaryExpression':
				if (node.operator in unaryOperators
						&& node.argument.type !== 'Literal') {
					var arg = getCode(node.argument);
					replaceCode(node, '$__("' + node.operator + '", '
							+ arg + ')');
				}
				break;
			case 'BinaryExpression':
				if (node.operator in binaryOperators
						&& node.left.type !== 'Literal') {
					var left = getCode(node.left),
						right = getCode(node.right),
						between = getBetween(node.left, node.right),
						operator = node.operator;
					replaceCode(node, '__$__(' + left + ','
							+ between.replace(new RegExp('\\' + operator),
								'"' + operator + '"')
							+ ', ' + right + ')');
				}
				break;
			case 'UpdateExpression':
			case 'AssignmentExpression':
				var parentType = parent && parent.type;
				if (!(
						parentType === 'ForStatement'
						|| parentType === 'BinaryExpression'
							&& /^[=!<>]/.test(parent.operator)
						|| parentType === 'MemberExpression' && parent.computed
				)) {
					if (node.type === 'UpdateExpression') {
						var arg = getCode(node.argument),
							exp = '__$__(' + arg + ', "' + node.operator[0]
									+ '", 1)',
							str = arg + ' = ' + exp;
						if (!node.prefix
								&& (parentType === 'AssignmentExpression'
									|| parentType === 'VariableDeclarator')) {
							if (getCode(parent.left || parent.id) === arg)
								str = exp;
							str = arg + '; ' + str;
						}
						replaceCode(node, str);
					} else {
						if (/^.=$/.test(node.operator)
								&& node.left.type !== 'Literal') {
							var left = getCode(node.left),
								right = getCode(node.right),
								exp = left + ' = __$__(' + left + ', "'
									+ node.operator[0] + '", ' + right + ')';
							replaceCode(node, /^\(.*\)$/.test(getCode(node))
									? '(' + exp + ')' : exp);
						}
					}
				}
				break;
			case 'ExportDefaultDeclaration':
				replaceCode({
					range: [node.start, node.declaration.start]
				}, 'module.exports = ');
				break;
			case 'ExportNamedDeclaration':
				var declaration = node.declaration;
				var specifiers = node.specifiers;
				if (declaration) {
					var declarations = declaration.declarations;
					if (declarations) {
						declarations.forEach(function(dec) {
							replaceCode(dec, 'module.exports.' + getCode(dec));
						});
						replaceCode({
							range: [
								node.start,
								declaration.start + declaration.kind.length
							]
						}, '');
					}
				} else if (specifiers) {
					var exports = specifiers.map(function(specifier) {
						var name = getCode(specifier);
						return 'module.exports.' + name + ' = ' + name + '; ';
					}).join('');
					if (exports) {
						replaceCode(node, exports);
					}
				}
				break;
			}
		}

		function encodeVLQ(value) {
			var res = '',
				base64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
			value = (Math.abs(value) << 1) + (value < 0 ? 1 : 0);
			while (value || !res) {
				var next = value & (32 - 1);
				value >>= 5;
				if (value)
					next |= 32;
				res += base64[next];
			}
			return res;
		}

		var url = options.url || '',
			agent = paper.agent,
			version = agent.versionNumber,
			offsetCode = false,
			sourceMaps = options.sourceMaps,
			source = options.source || code,
			lineBreaks = /\r\n|\n|\r/mg,
			offset = options.offset || 0,
			map;
		if (sourceMaps && (agent.chrome && version >= 30
				|| agent.webkit && version >= 537.76
				|| agent.firefox && version >= 23
				|| agent.node)) {
			if (agent.node) {
				offset -= 2;
			} else if (window && url && !window.location.href.indexOf(url)) {
				var html = document.getElementsByTagName('html')[0].innerHTML;
				offset = html.substr(0, html.indexOf(code) + 1).match(
						lineBreaks).length + 1;
			}
			offsetCode = offset > 0 && !(
					agent.chrome && version >= 36 ||
					agent.safari && version >= 600 ||
					agent.firefox && version >= 40 ||
					agent.node);
			var mappings = ['AA' + encodeVLQ(offsetCode ? 0 : offset) + 'A'];
			mappings.length = (code.match(lineBreaks) || []).length + 1
					+ (offsetCode ? offset : 0);
			map = {
				version: 3,
				file: url,
				names:[],
				mappings: mappings.join(';AACA'),
				sourceRoot: '',
				sources: [url],
				sourcesContent: [source]
			};
		}
		walkAST(parse(code, {
			ranges: true,
			preserveParens: true,
			sourceType: 'module'
		}));
		if (map) {
			if (offsetCode) {
				code = new Array(offset + 1).join('\n') + code;
			}
			if (/^(inline|both)$/.test(sourceMaps)) {
				code += "\n//# sourceMappingURL=data:application/json;base64,"
						+ self.btoa(unescape(encodeURIComponent(
							JSON.stringify(map))));
			}
			code += "\n//# sourceURL=" + (url || 'paperscript');
		}
		return {
			url: url,
			source: source,
			code: code,
			map: map
		};
	}

	function execute(code, scope, options) {
		paper = scope;
		var view = scope.getView(),
			tool = /\btool\.\w+|\s+on(?:Key|Mouse)(?:Up|Down|Move|Drag)\b/
					.test(code) && !/\bnew\s+Tool\b/.test(code)
						? new Tool() : null,
			toolHandlers = tool ? tool._events : [],
			handlers = ['onFrame', 'onResize'].concat(toolHandlers),
			params = [],
			args = [],
			func,
			compiled = typeof code === 'object' ? code : compile(code, options);
		code = compiled.code;
		function expose(scope, hidden) {
			for (var key in scope) {
				if ((hidden || !/^_/.test(key)) && new RegExp('([\\b\\s\\W]|^)'
						+ key.replace(/\$/g, '\\$') + '\\b').test(code)) {
					params.push(key);
					args.push(scope[key]);
				}
			}
		}
		expose({ __$__: __$__, $__: $__, paper: scope, view: view, tool: tool },
				true);
		expose(scope);
		code = 'var module = { exports: {} }; ' + code;
		var exports = Base.each(handlers, function(key) {
			if (new RegExp('\\s+' + key + '\\b').test(code)) {
				params.push(key);
				this.push('module.exports.' + key + ' = ' + key + ';');
			}
		}, []).join('\n');
		if (exports) {
			code += '\n' + exports;
		}
		code += '\nreturn module.exports;';
		var agent = paper.agent;
		if (document && (agent.chrome
				|| agent.firefox && agent.versionNumber < 40)) {
			var script = document.createElement('script'),
				head = document.head || document.getElementsByTagName('head')[0];
			if (agent.firefox)
				code = '\n' + code;
			script.appendChild(document.createTextNode(
				'document.__paperscript__ = function(' + params + ') {' +
					code +
				'\n}'
			));
			head.appendChild(script);
			func = document.__paperscript__;
			delete document.__paperscript__;
			head.removeChild(script);
		} else {
			func = Function(params, code);
		}
		var exports = func && func.apply(scope, args);
		var obj = exports || {};
		Base.each(toolHandlers, function(key) {
			var value = obj[key];
			if (value)
				tool[key] = value;
		});
		if (view) {
			if (obj.onResize)
				view.setOnResize(obj.onResize);
			view.emit('resize', {
				size: view.size,
				delta: new Point()
			});
			if (obj.onFrame)
				view.setOnFrame(obj.onFrame);
			view.requestUpdate();
		}
		return exports;
	}

	function loadScript(script) {
		if (/^text\/(?:x-|)paperscript$/.test(script.type)
				&& PaperScope.getAttribute(script, 'ignore') !== 'true') {
			var canvasId = PaperScope.getAttribute(script, 'canvas'),
				canvas = document.getElementById(canvasId),
				src = script.src || script.getAttribute('data-src'),
				async = PaperScope.hasAttribute(script, 'async'),
				scopeAttribute = 'data-paper-scope';
			if (!canvas)
				throw new Error('Unable to find canvas with id "'
						+ canvasId + '"');
			var scope = PaperScope.get(canvas.getAttribute(scopeAttribute))
						|| new PaperScope().setup(canvas);
			canvas.setAttribute(scopeAttribute, scope._id);
			if (src) {
				Http.request({
					url: src,
					async: async,
					mimeType: 'text/plain',
					onLoad: function(code) {
						execute(code, scope, src);
					}
				});
			} else {
				execute(script.innerHTML, scope, script.baseURI);
			}
			script.setAttribute('data-paper-ignore', 'true');
			return scope;
		}
	}

	function loadAll() {
		Base.each(document && document.getElementsByTagName('script'),
				loadScript);
	}

	function load(script) {
		return script ? loadScript(script) : loadAll();
	}

	if (window) {
		if (document.readyState === 'complete') {
			setTimeout(loadAll);
		} else {
			DomEvent.add(window, { load: loadAll });
		}
	}

	return {
		compile: compile,
		execute: execute,
		load: load,
		parse: parse,
		calculateBinary: __$__,
		calculateUnary: $__
	};

}.call(this);

var paper = new (PaperScope.inject(Base.exports, {
	Base: Base,
	Numerical: Numerical,
	Key: Key,
	DomEvent: DomEvent,
	DomElement: DomElement,
	document: document,
	window: window,
	Symbol: SymbolDefinition,
	PlacedSymbol: SymbolItem
}))();

if (paper.agent.node) {
	__webpack_require__(/*! ./node/extend.js */ 1)(paper);
}

if (true) {
	!(__WEBPACK_AMD_DEFINE_FACTORY__ = (paper),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
} else {}

return paper;
}.call(this, typeof self === 'object' ? self : null);


/***/ }),

/***/ "./src/EasterEgg.js":
/*!**************************!*\
  !*** ./src/EasterEgg.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _paper = __webpack_require__(/*! paper */ "./node_modules/paper/dist/paper-full.js");

var _paper2 = _interopRequireDefault(_paper);

var _world = __webpack_require__(/*! ./world */ "./src/world.js");

var _world2 = _interopRequireDefault(_world);

var _bird = __webpack_require__(/*! ./bird */ "./src/bird.js");

var _bird2 = _interopRequireDefault(_bird);

var _starSystem = __webpack_require__(/*! ./starSystem */ "./src/starSystem.js");

var _starSystem2 = _interopRequireDefault(_starSystem);

var _promiseLoader = __webpack_require__(/*! ./promiseLoader */ "./src/promiseLoader.js");

var _promiseLoader2 = _interopRequireDefault(_promiseLoader);

var _inTouch = __webpack_require__(/*! ./inTouch */ "./src/inTouch.js");

var _inTouch2 = _interopRequireDefault(_inTouch);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var settings = {
    bird: {
        init: {
            position: {
                x: 20 // % of the screen
            }
        }
    },
    world: {
        speed: (0, _inTouch2.default)() ? 0.015 * window.innerWidth : 0.008 * window.innerWidth
    },
    stars: {
        speed: (0, _inTouch2.default)() ? 0.013 * window.innerWidth : 0.008 * window.innerWidth
    }
};

var App = function () {
    function App(options) {
        var _this = this;

        _classCallCheck(this, App);

        this.images = options.images;
        this.init(options);

        // this.debug()

        window.addEventListener('resize', function () {
            _this.resize();
        });

        window.addEventListener('orientationchange', function () {
            setTimeout(function () {
                _this.resize();
            }, 400);
        });
    }

    _createClass(App, [{
        key: 'init',
        value: function init(_ref) {
            var canvas = _ref.canvas,
                images = _ref.images;

            // set canvas
            this.canvas = document.querySelector('' + canvas);

            // init paper project
            _paper2.default.setup(this.canvas);
            this.setSizeCanvas();
            this.project = _paper2.default.project;
            this.viewBounds = _paper2.default.view.bounds;

            // load all assets
            // this.load()
        }
    }, {
        key: 'load',
        value: function load(callback) {
            var _this2 = this;

            var worldPromise = new Promise(function (resolve) {
                var img = new _paper.Raster(_this2.images.world);
                img.onLoad = function () {
                    resolve(img);
                };
            });

            Promise.all([(0, _promiseLoader2.default)(this.images.bird), worldPromise, (0, _promiseLoader2.default)(this.images.star)]).then(function (value) {
                var _value = _slicedToArray(value, 3);

                _this2.birdItem = _value[0];
                _this2.worldItem = _value[1];
                _this2.starItem = _value[2];

                _this2.ready();
                _this2.resize();
                callback();
            });
        }
    }, {
        key: 'ready',
        value: function ready() {
            this.birdItem.insertBelow(this.worldItem);
            this.world = new _world2.default({ item: this.worldItem, bounds: this.viewBounds });
            this.bird = new _bird2.default({ item: this.birdItem, bounds: this.viewBounds, xOffset: settings.bird.init.position.x });
            this.starSystem = new _starSystem2.default({ item: this.starItem, bounds: this.viewBounds, layer: this.project.activeLayer, speed: settings.stars.speed });

            this.onMouseMove();
            // this.animate()
        }
    }, {
        key: 'play',
        value: function play() {
            this.animate();
        }
    }, {
        key: 'pause',
        value: function pause() {
            _paper2.default.view.pause();
        }
    }, {
        key: 'animate',
        value: function animate() {
            var _this3 = this;

            _paper2.default.view.onFrame = function (event) {
                _this3.world.animate(settings.world.speed);
                _this3.bird.animate(event.time);
                _this3.starSystem.animate({ bounds: _this3.viewBounds, time: event.time, speed: settings.stars.speed });
            };
        }
    }, {
        key: 'resize',
        value: function resize() {
            this.setSizeCanvas();
            this.viewBounds = _paper2.default.view.bounds;
            this.world.resize(this.viewBounds);
        }
    }, {
        key: 'setSizeCanvas',
        value: function setSizeCanvas() {
            _paper2.default.view.viewSize = new _paper2.default.Size(window.innerWidth, window.innerHeight);
        }
    }, {
        key: 'debug',
        value: function debug() {
            var _this4 = this;

            _paper2.default.view.onKeyDown = function (event) {
                if (event.key == 'space') _this4.project.activeLayer.selected = !_this4.project.activeLayer.selected;
            };
        }
    }, {
        key: 'onMouseMove',
        value: function onMouseMove() {
            var _this5 = this;

            _paper2.default.view.onMouseMove = function (evt) {
                _this5.bird.onMouseMove(evt);
            };
        }
    }]);

    return App;
}();

exports.default = App;

/***/ }),

/***/ "./src/bird.js":
/*!*********************!*\
  !*** ./src/bird.js ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _paper = __webpack_require__(/*! paper */ "./node_modules/paper/dist/paper-full.js");

var _paper2 = _interopRequireDefault(_paper);

var _tail = __webpack_require__(/*! ./tail */ "./src/tail.js");

var _tail2 = _interopRequireDefault(_tail);

var _inTouch = __webpack_require__(/*! ./inTouch */ "./src/inTouch.js");

var _inTouch2 = _interopRequireDefault(_inTouch);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var tempPoint = new _paper2.default.Point();
var limitDelta = 10;
var deltaMovementX = void 0;

var Bird = function () {
    function Bird(_ref) {
        var item = _ref.item,
            bounds = _ref.bounds,
            xOffset = _ref.xOffset;

        _classCallCheck(this, Bird);

        this.item = item;
        (0, _inTouch2.default)() ? this.item.scale(0.78) : '';
        this.item.position.x = xOffset * bounds.size.width * 0.01;
        this.currentSide = 'right';
        this.mousePos = new _paper2.default.Point();
        this.sinFactor = (0, _inTouch2.default)() ? 3.5 : 2;
        this.easeFactor = (0, _inTouch2.default)() ? 0.6 : 0.07;
        this.tail = new _tail2.default();
        this.tail.group.insertBelow(this.item);
    }

    _createClass(Bird, [{
        key: 'animate',
        value: function animate(time) {
            deltaMovementX = (this.mousePos.x - this.item.position.x) * this.easeFactor;
            this.item.position.y += (this.mousePos.y - this.item.position.y) * this.easeFactor + Math.sin(time * 4) * this.sinFactor;
            this.item.position.x += deltaMovementX;
            this.tail.animate(this.item, time);

            this.handleFlipSide();
        }
    }, {
        key: 'handleFlipSide',
        value: function handleFlipSide() {
            if (Math.abs(deltaMovementX) < 2 && this.currentSide === 'left') {
                this.rotate('right');
            }
        }
    }, {
        key: 'rotate',
        value: function rotate(side) {
            this.currentSide = side;
            this.item.scale(-1, 1);
        }
    }, {
        key: 'onMouseMove',
        value: function onMouseMove(evt) {
            this.mousePos = evt.point.clone();
            var delta = this.mousePos.x - tempPoint.x;

            if (delta > limitDelta || delta < -limitDelta) {
                if (delta > 0) {
                    // move mouse right
                    if (this.currentSide != 'right') {
                        this.rotate('right');
                    }
                } else {
                    // move mouse left
                    if (this.currentSide != 'left') {
                        this.rotate('left');
                    }
                }
            }

            tempPoint = this.mousePos.clone();
        }
    }]);

    return Bird;
}();

exports.default = Bird;

/***/ }),

/***/ "./src/images/bird.svg":
/*!*****************************!*\
  !*** ./src/images/bird.svg ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PHN2ZyBpZD0iTGl2ZWxsb18xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNzEuNyAyMzguNSI+PHN0eWxlPi5zdDB7ZmlsbDojZmZmfS5zdDIsLnN0NXtmaWxsOiMwYjdhZjQ7c3Ryb2tlOiMwMDA7c3Ryb2tlLXdpZHRoOjI7c3Ryb2tlLWxpbmVqb2luOnJvdW5kfS5zdDV7ZmlsbDpub25lfTwvc3R5bGU+PGcgaWQ9Im1vdXNlIj48cGF0aCBjbGFzcz0ic3QwIiBkPSJNOTAuNSA2YzMuOCAwIDI0LjMgMTggNDMuNiAzNy44bDMuNiAzLjdjMTguMSAxOC42IDMzLjEgMzQuNCAxNyA1MS41bC0xLjEgMS4xLS45LjljLjQgMCAuOC4xIDEuMi4yIDEuNS4zIDMgLjUgNC41LjUgNC4zIDAgNi4zLTEuNyA4LjMtMi44IDIuNC0xLjMgOS40LTQuNiAxMy43LTQuNmgxYTEzLjE5IDEzLjE5IDAgMCAxIDcuMiAzLjVoLjljMyAwIDguOC0uNyA4LjgtLjdsLTcuOSA3LjRjMCAuMyAwIC43LS4xIDEtLjMgMy0yLjIgMTIuNi0xNC40IDIyLjUtMSAuOC0yLjIgMS43LTMuNCAyLjYtMS42IDEuMS0zLjQgMi4zLTUuMyAzLjQgMi42IDMuNyAzLjggOC4yIDMuNCAxMi43LS41IDcuMy00LjMgMjAuNS05LjQgMzQuMy04LjQgMjIuNy0yMC43IDQ3LjItMjguOCA1MC4yLTIuMy0xLjUtLjktMTEgOS42LTMyLjMtOCAxNC45LTEyLjUgMjIuMi0xMy44IDIyLjItLjggMC0uNS0yLjUuNy03LjQgMi4yLTguOSA1LjItMTcuNiA4LjktMjYuMS00LjYgNi45LTguMSAxMC43LTkuNiAxMC43LTEuMiAwLTEtMi44IDEuMi04LjggNC03LjkgNy4xLTEzLjIgMTAuMS0xNi45LTQuOSA0LjYtOSA3LjgtMTAuNSA3LjgtMS4xIDAtLjgtMS44IDEuNi02LjMgMy4yLTQuMyA2LjktOC4yIDEwLjktMTEuOC01LjQgNC42LTEwIDcuMS0xMS45IDcuMXMtMS4yLTIuMyAzLjYtNy4yYzcuNS02LjYgOC41LTguMyA4LjYtOC40LS4yLjItMTEgMTAuMS0xNC4yIDEwLjEtMSAwLTEuMy0xLS4yLTMuOCA3LjUtOCA5LjctMTAuNCA5LjktMTAuNi0uNC40LTguOCA3LjktMTAuNyA3LjktMS4xIDAgLjEtMi42IDYuNC0xMC42LTMuMiAzLjQtNi42IDUuNC04LjUgNS40cy0yLjEtMi4zIDEuNy03LjZhOC4yOCA4LjI4IDAgMCAxLTUuNSAyLjVjLTEuNyAwLTIuOS0xLTIuNS0zLjVoLTMuOGMtLjQtLjItLjctLjItMS4xLS4yLTEuOCAwLTYuMS44LTE1LjkgNS43LTMuMyAxLjctNy4zIDMuOC0xMiA2LjYtMjUuMSAxNC44LTY2IDI0LjUtNzMuNSAyNS4zLTEuMi0xLjggMi4yLTUuNSAxMy4xLTguOCA3LjctMi4zIDI2LjQtNy44IDM3LjMtMTEuMWwuNC0uMWMyLjMtLjUgNC42LTEuMiA2LjgtMi4xbC44LS4zYy0xIC40LTQgMS4zLTggMi42LTEuNC4zLTIuNy41LTQuMS42LTEuNiAwLTIuMS0uNC0uOS0xLjMgMy4xLTIuMiAxMi41LTYuMiAxNS45LTctNC41IDEtOS42IDIuNi0xMS4yIDIuNi0uNiAwLS43LS4yLS4xLS44IDIuMS0xLjkgNS4yLTQuOCA3LjQtNS40LS4yIDAtLjQuMS0uNi4xLTMuNyAwLTYuNy01LjktMS4zLTYuMy02LjItLjItMTYtMi43LS41LTIuOS05LjYtLjEtMjcuNC00LjMtMy44LTQuMyAyLjkgMCA2LjQuMSAxMC43LjItMzUuOC0yLjItNzcuNS0xMi02NS45LTE2LjNoLjRjMTAuNyAwIDMwLjEgOS4zIDQ1LjIgOS45IDcgLjMgMTguNCAyLjMgMzAuMiAyLjMgNC40IDAgOC43LS4zIDEzLTEgNC40LS43IDguNi0yLjEgMTIuNS00IC42LS4zIDEuMS0uNiAxLjctLjktMi4yLTIuNS01LjQtNy42IDUuNy04LjctOS40LS4zLTE1LjUtMi45LjItMy01LjUtMi4yLTkuOS01LjctNi01LjcgMi4xLjEgNC4yLjUgNi4yIDEuMi0yLjgtMi4xLTUuOC00LjctMy41LTQuNyAxIDAgMyAuNSA2LjYgMS44LTQuOS0yLjYtNy4zLTUuNi00LjYtNS42IDEuMiAwIDMuMi41IDYuNSAxLjktNS4xLTIuMi03LjctNS40LTQuNC01LjQgMS43LjEgMy40LjUgNSAxLjEtNC42LTIuNi02LjUtNS4xLTQtNS4xIDEuMiAwIDMuMi41IDYuNCAxLjgtNS4xLTMtOC4xLTYuOS01LjItNi45IDIgLjIgMy45LjggNS42IDEuOC00LTIuNi03LjItNi42LTQuNi02LjYgMS4zIDAgNC4xIDEgOC44IDMuNS03LjctNC44LTEzLjgtMTAuNS0xMS4zLTEwLjUgMS4zIDAgNC44IDEuNiAxMS43IDUuNi0xMy4yLTguOC0yMy43LTE3LjgtMjEuNi0xNy44IDEuMyAwIDcuMiAzLjQgMjAgMTIuMi0yMC4zLTE3LjItMjYtMjEuMS0yMy4yLTI0LjZoLjFjMS4xIDAgNSAzIDkuNCA2LjYgMiAxLjcgMy45IDMuMyA1LjggNC44IDIuNSAyLjIgNS4yIDQuMiA4IDYuMS0yLjEtMS42LTQuOS0zLjctOC02LjEtMS45LTEuNi0zLjktMy4yLTUuOC00LjgtNy45LTYuOC0xNS4zLTE0LjMtMTMuOS0xOC43aC4yYzMuMiAwIDE3LjQgMTYuOSAyOC4xIDIzLjUtMjAuNS0xOC44LTM2LjMtMzYuNS0zNi4zLTQxLjggMy45IDEuNiAyOC45IDMwLjIgMzQuNCAzMy43LTE5LjktMTkuNy0zOC42LTM4LTM4LjItNDQuNmguMWMxLjEgMCAzLjYgMS45IDcgNSA1LjggNi42IDE1LjUgMTUuOCAyNy45IDI4LjYtMS44LTEuOS0zLjYtMy43LTUuNC01LjYtOC4zLTguNy0xNi4yLTE3LTIxLjktMjIuM2wtLjYtLjZDOTIgMTIgODkuOSA4LjYgOTAuMiA2aC4zbTAtM2MtLjMgMC0uNyAwLTEgLjEtMS4yLjMtMi4xIDEuMy0yLjIgMi41LS4xIDEuMiAwIDIuMy4zIDMuNC0uOS41LTEuNSAxLjQtMS42IDIuNS0uMiAyLjUgMS4zIDUuNyA0LjIgOS44LS4yLjQtLjMuOS0uNCAxLjQgMCAxLjMgMCA0LjYgOS41IDE1LjgtLjYuNC0xLjEgMS0xLjMgMS43LS43IDIuMy0uNiA1LjcgNC40IDExLjctMi41IDMuOSAxIDcuNyAyLjEgOSAuNi43IDEuMyAxLjMgMi4xIDIuMS0uNy40LTEuMSAxLTEuNCAxLjgtLjYgMi4xLjQgMy44IDcuMiA5LjQuOS43IDEuOCAxLjUgMi44IDIuMi0uMS4yLS4yLjUtLjMuNy0uNiAxLjkuNiAzLjcgMi44IDUuOC0uNC40LS42LjktLjggMS40LS4zIDEtLjIgMiAuMyAyLjktLjcuNS0xLjMgMS4yLTEuNSAyLjEtLjIgMS0uMSAyIC40IDIuOS0uOC41LTEuMyAxLjItMS41IDIuMS0uMi43LS4yIDEuNC4xIDIuMS0uOC41LTEuMyAxLjMtMS41IDIuMi0uMS4zLS4xLjctLjEgMS0uOC41LTEuNCAxLjMtMS42IDIuMi0uMS41LS4xIDEgMCAxLjQtLjcuNC0xLjIgMS4xLTEuNSAxLjgtLjEuNC0uMi44LS4yIDEuMy0xLjYuMS0yLjkgMS4zLTMuMyAyLjlhNCA0IDAgMCAwIDEgMy4zYy0yLjEuNi0zLjMgMS42LTMuMyAzLjQgMCAxLjUgMSAyLjggMi4zIDMuMy0uMS4zLS4zLjUtLjQuOC0uNiAxLjgtLjUgMy43LjQgNS4zLTMuMyAxLjQtNi44IDIuNS0xMC4zIDMuMS00LjEuNy04LjMgMS0xMi41IDEtNi43LS4xLTEzLjQtLjYtMjAuMS0xLjQtMy45LS40LTcuMi0uOC0xMC0uOS03LjYtLjMtMTYuMy0yLjktMjQuOC01LjQtNy43LTIuMy0xNS00LjUtMjAuNS00LjVoLS41Yy0uMyAwLS43LjEtMSAuMi00IDEuNS00LjEgNC4xLTMuOSA1LjEuNCAyLjUgMi44IDQuMyA4LjQgNi40IDUuMyAxLjggMTAuOCAzLjMgMTYuMyA0LjQgNi40IDEuNCAxMy41IDIuNiAyMC43IDMuNnYuNmMwIDMgMy40IDQgNy4zIDQuOC41IDIuNCAzLjYgMyA1LjMgMy40bC41LjF2LjJjLjEgMS41LjcgMi45IDEuNyA0LjEtLjkuOC0xLjkgMS42LTIuOSAyLjZsLS4zLjNjLS45LjctMS41IDEuOS0xLjQgMy4xLTEuMS42LTIuMSAxLjItMy4xIDEuOS0uOS42LTEuNiAxLjYtMS44IDIuNy00LjYgMS40LTkuNSAyLjktMTQuMSA0LjItNi4xIDEuOC0xMS45IDMuNS0xNS40IDQuNi02LjEgMS45LTEzLjYgNC45LTE1LjEgOS4zLS41IDEuNC0uNCAyLjkuNCA0LjEuNi44IDEuNSAxLjMgMi41IDEuM2guM2M4LjktMSA0OS42LTEwLjkgNzQuNy0yNS43IDQuMy0yLjYgOC4zLTQuNyAxMS44LTYuNSA5LjktNSAxMy42LTUuNCAxNC41LTUuNGguMWMuMy4xLjYuMi45LjJoMS4yYy4yLjYuNSAxLjEuOSAxLjYgMSAxLjEgMi4zIDEuOCAzLjggMS45LS4yIDEgMCAyIC40IDIuOS42IDEuMiAxLjggMiAzLjEgMi4yLS42IDEtLjYgMi4zLS4xIDMuNC4yLjQuNC43LjggMS0uNyAxLjgtMS4yIDQgMCA1LjcuNC42IDEgMS4xIDEuNyAxLjQtLjUgMS4xLS42IDIuMy0uMSAzLjQuNCAxIDEuMyAxLjggMi4zIDIuMWwtLjEuMWMtLjEuMS0uMi4yLS4yLjQtMi42IDQuOC0zLjIgNy4xLTIuMSA5IC43IDEuMSAxLjggMS44IDMuMSAxLjdoLjJjLS44IDEuNS0xLjYgMy0yLjQgNC43IDAgLjEtLjEuMi0uMS4zLTIuNyA3LjItMi41IDkuNi0xLjQgMTEuMy43IDEgMS44IDEuNiAzIDEuNi40IDAgLjggMCAxLjEtLjEtMS4zIDMuOS0yLjQgNy45LTMuNCAxMS43LTEuNSA2LjEtMS42IDguMy0uNCA5LjkuNi44IDEuNiAxLjMgMi42IDEuMy4zIDAgLjUgMCAuOC0uMS0uOCAzLjctMSA3LjkgMS44IDkuNy44LjUgMS44LjYgMi42LjMgMTIuNC00LjUgMjcuNy00NC4xIDMwLjYtNTIgNS41LTE0LjcgOS4xLTI3LjkgOS42LTM1LjEuNC00LjEtLjQtOC4yLTIuMi0xMS45IDEtLjYgMi0xLjMgMi45LTIgMS4yLS45IDIuNC0xLjggMy41LTIuNyAxMi43LTEwLjMgMTUtMjAuNSAxNS40LTI0LjRsNy02LjZjMS4yLTEuMSAxLjMtMyAuMS00LjItLjYtLjYtMS4zLS45LTIuMS0uOWgtLjFjLS4xIDAtNS40LjYtOC4yLjdsLS4yLS4yYy0yLjItMS44LTQuOS0yLjktNy43LTMuM2wtLjUtLjFoLS45Yy01LjkgMC0xNC44IDQuOC0xNS4yIDVsLTEuMS42YTEwLjIgMTAuMiAwIDAgMS01LjMgMS44YzE0LjktMTguNi0yLjMtMzYuMy0xOS01My4zbC0uMS0uMS0zLjUtMy41Yy05LjQtOS42LTE5LjUtMTkuMi0yNy45LTI2LjRDOTQuNCAzIDkxLjcgMyA5MC41IDN6Ii8+PHBhdGggZD0iTTE5OC4zIDk3LjFjLTMuMi40LTYuNS42LTkuNy42LS4zLS4zLS42LS41LS45LS44LTEuOC0xLjQtNC0yLjQtNi4zLTIuN2gtLjNjLTQuMi0uNC0xMS44IDMuMi0xNC40IDQuNnMtNS4zIDMuOS0xMi45IDIuM2MtLjQtLjEtLjgtLjItMS4yLS4ybC45LS45IDEuMS0xLjFjMTYuMS0xNy4xIDEuMi0zMi45LTE3LTUxLjVsLTMuNi0zLjdDMTE0LjQgMjMuNSA5My4zIDUuMiA5MC4yIDZjLS4zIDIuNiAxLjggNiA1LjkgMTAuN2wuNi42YzUuNyA1LjMgMTMuNiAxMy42IDIxLjkgMjIuMyAxLjggMS45IDMuNiAzLjggNS40IDUuNi0xMi40LTEyLjctMjIuMS0yMS44LTI3LjktMjguNS0zLjUtMy4yLTYtNS4xLTcuMS01LS40IDYuNSAxOC4yIDI0LjkgMzguMyA0NC43LTUuNS0zLjQtMzAuNS0zMi0zNC40LTMzLjYgMCA1LjMgMTUuOCAyMy4xIDM2LjMgNDEuOC0xMC44LTYuNy0yNS40LTI0LjEtMjguMy0yMy41LTEuNCA0LjQgNiAxMS45IDEzLjkgMTguNiAxLjkgMS42IDQgMy4yIDUuOCA0LjggMyAyLjQgNS44IDQuNSA4IDYuMS0yLjgtMS45LTUuNS0zLjktOC02LjEtMS45LTEuNS0zLjktMy4xLTUuOC00LjgtNC42LTMuNy04LjctNi45LTkuNS02LjYtMi44IDMuNSAyLjkgNy4zIDIzLjIgMjQuNi0zMy44LTIzLjMtMTkuNi04LjQgMS43IDUuNi0yMC4yLTExLjctMTIuMi0yLjMtLjYgNS0xNC41LTcuNy0xMC4yLS44LTQuMyAzLjEtMTAuNC00LjctNy4zIDEtLjQgNS4xLTEwLTQuMS05LjItLjYtMi40IDMuMy05LjYtMy4yLTcuNCAxLjQtLjYgNC4zLTEwLjgtNC41LTguOCAwLTEuOCAzLjctMTEuNC00LjMtNy4yLS4yLTMuMSAyLjktMTIuNy0zLjQtNy4zIDEuNy0uMiA0LjUtMTUuNyAwLTkuNiAyLjctLjIgMy0xMS4xIDEtNy45IDYuMi01LjcgOC43LS42LjMtMS4xLjYtMS43LjktNCAxLjktOC4yIDMuMy0xMi41IDQtMTUuOCAyLjgtMzMuNi0uOS00My4yLTEuMy0xNS4zLS42LTM1LTEwLjEtNDUuNi05LjktMTEuNiA0LjMgMzAuMSAxNCA2NS44IDE2LjItMzguNy0xLjMtMTcuNiA0LTYuOCA0LjEtMTUuNS4xLTUuNyAyLjcuNSAyLjktNS44LjQtMi4xIDcgMS45IDYuMi0yLjIuNi01LjMgMy41LTcuNCA1LjRzNS4xLS40IDExLjItMS45Yy0zLjUuOC0xMi45IDQuNy0xNS45IDctMiAxLjQuNyAxLjcgNSAuOCA0LjEtMS4yIDcuMS0yLjIgOC0yLjZsLS44LjNjLTIuMi45LTQuNSAxLjYtNi44IDIuMWwtLjQuMWMtMTAuOSAzLjMtMjkuNiA4LjgtMzcuMyAxMS4xLTEwLjggMy4zLTE0LjMgNy0xMy4xIDguOCA3LjYtLjggNDguNC0xMC41IDczLjUtMjUuMyA0LjctMi44IDguNy00LjkgMTItNi42IDE0LjQtNy4zIDE3LTUuNSAxNy01LjVIMTE4LjJjLS45IDQuOSA0LjcgNC4xIDggLjktNy41IDEwLjQuMiA5LjIgNi44IDIuMi0xNy4xIDIxLjggMy43IDMuMiA0LjMgMi43LS4yLjItMi40IDIuNi05LjkgMTAuNi00LjYgMTEuMiAxNC4yLTYuMSAxNC40LTYuNC0uMS4xLTEuMSAxLjktOC42IDguNC05LjggOS45LTIuMyA5LjEgOC4zLjEtNCAzLjYtNy43IDcuNS0xMC45IDExLjgtNS43IDEwLjQuMyA2LjUgOC45LTEuNS0yLjkgMy43LTYuMSA4LjktMTAuMSAxNi45LTQuOSAxMy4yLS4xIDEwLjcgOC40LTEuOS0zLjcgOC40LTYuNyAxNy4yLTguOSAyNi4xLTMuMyAxMy4zLjQgOSAxMy4xLTE0LjctMTAuNSAyMS4zLTExLjkgMzAuOC05LjYgMzIuMyA4LjEtMi45IDIwLjQtMjcuNCAyOC44LTUwLjIgNS4xLTEzLjggOC45LTI3IDkuNC0zNC4zLjUtNC41LS43LTktMy40LTEyLjcgMS45LTEuMSAzLjctMi4zIDUuMy0zLjQgMS4yLS44IDIuMy0xLjcgMy40LTIuNSAxMi4xLTkuOSAxNC0xOS40IDE0LjQtMjIuNSAwLS4zLjEtLjcuMS0xbDcuOS03LjV6bS03NC4xLTUxLjdsMi4zIDIuNC0yLjMtMi40em0tNTYuNSA5NS4zem0xLjYgOS4xbC40LS4xYy0uMS4xLS4yLjEtLjQuMXoiIGZpbGw9IiNmZmYiIHN0cm9rZT0iIzAwMCIgc3Ryb2tlLXdpZHRoPSIyIiBzdHJva2UtbGluZWpvaW49InJvdW5kIi8+PHBhdGggY2xhc3M9InN0MCIgZD0iTTcwLjQgMTU4LjZjLTIuNiAxLjEtNS4zIDItOCAyLjYgNC4xLTEuMiA3LjEtMi4yIDgtMi42eiIvPjxwYXRoIGQ9Ik03MC40IDE1OC42Yy0xIC40LTQgMS4zLTggMi42IDIuOC0uNiA1LjUtMS41IDgtMi42eiIvPjxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik02Ny45IDE0MC43eiIvPjxwYXRoIGQ9Ik02Ny45IDE0MC43eiIvPjxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik02OS44IDE0OS43Yy0uMSAwLS4yLjEtLjQuMS4yIDAgLjMgMCAuNC0uMXoiLz48cGF0aCBkPSJNNjkuOCAxNDkuN2MtLjEgMC0uMy4xLS40LjEuMiAwIC4zIDAgLjQtLjF6Ii8+PHBhdGggY2xhc3M9InN0MCIgZD0iTTEyNC4yIDQ1LjNjLTEyLjQtMTIuOC0yMi4yLTIxLjktMjgtMjguNiA2LjkgNi4zIDE3LjUgMTcuNyAyOCAyOC42eiIvPjxwYXRoIGQ9Ik05Ni4yIDE2LjdjNS44IDYuNiAxNS41IDE1LjggMjcuOSAyOC42LTEwLjQtMTAuOS0yMS0yMi4zLTI3LjktMjguNnoiLz48cGF0aCBjbGFzcz0ic3QwIiBkPSJNMTIwLjggNjQuNmMtMS45LTEuNS0zLjktMy4xLTUuOC00LjggMS45IDEuNiAzLjkgMy4yIDUuOCA0Ljh6Ii8+PHBhdGggZD0iTTExNC45IDU5LjhjMiAxLjcgMy45IDMuMyA1LjggNC44LTEuOC0xLjYtMy44LTMuMi01LjgtNC44eiIvPjxwYXRoIGNsYXNzPSJzdDIiIGQ9Ik05Ny40IDE0OS4yYy0zLjMgMS43LTcuMyAzLjgtMTIgNi42LTI1LjEgMTQuOC02NiAyNC41LTczLjUgMjUuMy0xLjItMS44IDIuMi01LjUgMTMuMS04LjggNy43LTIuMyAyNi40LTcuOCAzNy4zLTExLjEtNC4zLjktNyAuNy01LS44IDMuMS0yLjIgMTIuNS02LjIgMTUuOS03LTYuMSAxLjQtMTMuNSAzLjktMTEuMiAxLjkgMi4yLTIuMSA1LjItNC44IDcuNC01LjQtNCAuNy03LjYtNS44LTEuOS02LjItNi4yLS4yLTE2LTIuNy0uNS0yLjktMTAuOC0uMS0zMS45LTUuNCA2LjgtNC4xLTM1LjgtMi4yLTc3LjUtMTItNjUuOS0xNi4zIDEwLjYtLjIgMzAuMyA5LjMgNDUuNiA5LjkgOS43LjQgMjcuNCA0IDQzLjIgMS4zLS4yLjItLjMuNC0uNS42LTUgNS41LTEyLjIgOC41LTE3LjkgMTMuMi0uOC42LTEuNiAxLjgtMSAyLjYuNC40LjkuNiAxLjQuNiAzLjguNCA3LjYuNCAxMS4zIDAgMi42LS40IDUuNS0uNyA3LjQuNnpNMTYxLjMgMTgxYy04LjQgMjIuNy0yMC43IDQ3LjItMjguOCA1MC4yLTIuMy0xLjUtLjktMTEgOS42LTMyLjMtMTIuOCAyMy43LTE2LjUgMjgtMTMuMSAxNC43IDIuMi04LjkgNS4yLTE3LjYgOC45LTI2LjEtOC41IDEyLjYtMTMuMiAxNS4xLTguNCAxLjkgNC03LjkgNy4xLTEzLjIgMTAuMS0xNi45LTguNiA4LTE0LjYgMTItOC45IDEuNSAzLjItNC4zIDYuOS04LjIgMTAuOS0xMS44LTEwLjYgOS0xOC4xIDkuOC04LjMtLjEgNy41LTYuNiA4LjUtOC4zIDguNi04LjQtLjIuMi0xOSAxNy42LTE0LjQgNi40IDcuNS04IDkuNy0xMC40IDkuOS0xMC42LS42LjYtMjEuNCAxOS4xLTQuMy0yLjctNi41IDctMTQuMyA4LjItNi44LTIuMi0zLjMgMy4yLTguOSAzLjktOC0uOUgxMTdjLjktLjIgMS45LS4yIDIuOC0uMWw2LjUuNWMzLjQuMiA2LjguNSA5LjkgMS45IDEuNi44IDMuMiAxLjggNC42IDIuOWwyLjUgMS44YzEuNCAxIDMgMi4yIDMuNSAzLjkuMyAxIC4zIDIuMSAxIDIuN3MxLjYuNSAyLjQuOWMxLjEuNyAxLjcgMiAxLjQgMy4zLS4zIDEuMi0uOSAyLjQtMS43IDMuNCAxLjctLjEgMi4xIDIuNCAxLjUgMy45cy0xLjUgMy40LS40IDQuN2MuOCAxIDIuNSAxLjEgMi45IDIuMy4zIDEtLjYgMS45LS40IDIuOS4yIDEuMSAxLjUgMS43IDIuNyAxLjkgMS41LjMgMy4zLjMgNS4xLjR6TTEzMSA1Mi4xYzEuMSAxLjMgMS45IDIuOCAyLjQgNC40LjUgMS43IDAgMy41LTEuMyA0LjYgMS4zIDEuOCAxLjEgNC4zLS40IDUuOS45LjYgMS42IDEuNSAyIDIuNS4zIDEtLjIgMi4xLTEuMiAyLjRoLS4xYzEuNC45IDIuNSAyLjIgMy4xIDMuOC41IDEuNi43IDguOS4yIDEwLjRzLTQuMSAxMi01LjkgMTQuMmMtMSAxLjItOS41IDE4LjYtOC41IDE4LjgtMi4xIDIuNC03LjIgNy4xLTExLjkgOC41LjYtLjMgMS4xLS42IDEuNy0uOS0yLjItMi41LTUuNC03LjYgNS43LTguNy05LjQtLjMtMTUuNS0yLjkuMi0zLTcuMi0yLjgtMTIuNS04IC4yLTQuNS00LjEtMy4xLTguNC03LjEgMy4xLTIuOS03LTMuNy05LTguMiAxLjgtMy43LTYuOC0yLjgtOS03LjYuNi00LjMtNi44LTMuOS03LjYtNy4zIDIuNC0zLjMtNy00LjEtMTAtOS44LjQtNS4xLTUuOS0zLjktMTAuMi0xMC44IDQuMy0zLjEtMTEuNy03LjMtMTkuNy0xNi43LjQtNC45LTIxLjMtMTQuMS0zNS40LTI5LTEuNi01LjYtMjAuMy0xNy4yLTI2LTIxLjEtMjMuMi0yNC42LjgtLjMgNSAyLjkgOS41IDYuNkMxMDcgNTIuOSA5OS42IDQ1LjMgMTAxIDQxYzIuOS0uNiAxNy40IDE2LjcgMjguMyAyMy41QzEwOC44IDQ1LjcgOTMgMjggOTMgMjIuN2MzLjkgMS42IDI4LjkgMzAuMiAzNC40IDMzLjdDMTA3LjMgMzYuNiA4OC42IDE4LjMgODkgMTEuN2MxLS4xIDMuNiAxLjggNy4xIDVDOTIgMTIgODkuOSA4LjYgOTAuMiA2YzMuMS0uOCAyNC4yIDE3LjUgNDMuOCAzNy43bDMuNiAzLjdjMCAxLjEtLjQgMi4yLS45IDMuMi0xLjEgMi00IDMtNS43IDEuNXoiLz48cGF0aCBkPSJNMTkwLjQgMTA0LjVjMCAuMyAwIC43LS4xIDEtLjMgMy0yLjIgMTIuNi0xNC40IDIyLjUtMSAuOC0yLjIgMS43LTMuNCAyLjYtMS4xLS44LTIuMi0xLjctMy0yLjMtMy42LTIuOS0xMC4xLTkuNS0xMC4xLTE0LjctLjUtMS44LTEuMS0zLjYtMS45LTUuNC0xLjEtMi4xLTMtMy43LTQuNi01LjUtLjMtLjMtLjQtLjYtLjYtLjktLjItLjguNC0xLjQgMS4xLTEuOWwtLjkuOWMuNCAwIC44LjEgMS4yLjIgNy42IDEuNiAxMC4yLS44IDEyLjktMi4zczEwLjItNSAxNC40LTQuNmguM2ExMy4xOSAxMy4xOSAwIDAgMSA3LjIgMy41YzMuMyAwIDYuNS0uMiA5LjctLjZsLTcuOCA3LjV6Ii8+PHBhdGggY2xhc3M9InN0MCIgZD0iTTE3OS44IDEwMy4xYzAtLjctLjUtMS40LTEuMi0xLjRzLTEuNC41LTEuNCAxLjIuNSAxLjMgMS4yIDEuNGMuNCAwIC44LS4xIDEtLjQiLz48cGF0aCBkPSJNMTg3LjUgOTcuM2MtLjEuNy0uMSAxLS4zIDEuNyAwIC4yLS4xLjQtLjIuNi0uNC44LTEuNi42LTIuNS41LTEuMiAwLTIuNS0uMi0zLjctLjUtMS4yLS40LTIuMi0xLjctMS44LTIuOC41LS44IDEuMS0xLjYgMS45LTIuMiAzLjMuMSA2LjYgMi43IDYuNiAyLjd6IiBmaWxsPSIjZWQxMTMwIi8+PHBhdGggZD0iTTE4OS43IDEwNi42Yy0uNCAyLjgtMi41IDExLjYtMTMuOSAyMC4zLTIuMy0uNS00LjQtMS43LTYuMi0zLjItMi40LTIuMy0zLjYtNi4xLTIuMi05LjEuMy0uNi43LTEuMiAxLjItMS42LjUtLjMgMS4xLS41IDEuNi0uNSAzLjEtLjYgNi40LTEgOC42LTMuMiAxLjItMS4yIDEuOS0yLjggMy4yLTQgMi4xLTEuOCA1LjctMS43IDcuMi42LjIuMi40LjUuNS43eiIgZmlsbD0iIzBiN2FmNCIvPjxwYXRoIGNsYXNzPSJzdDUiIGQ9Ik0xNDAuMyAxNDQuN2M5LjgtLjkgMTkuMi00LjYgMjctMTAuN00xNTIuOSAxMDFjLTYuMS0xLjItMTIuNC4yLTE3LjQgMy44Ii8+PC9nPjwvc3ZnPg=="

/***/ }),

/***/ "./src/images/star.svg":
/*!*****************************!*\
  !*** ./src/images/star.svg ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyOC43MyAyOC43MyI+PHBhdGggZmlsbD0iI2ZmZiIgZD0iTTE3LjM5IDBsMS4xMiA4LjMxIDguMTUtMS45Ny01LjA4IDYuNjcgNy4xNSA0LjM4LTguMzEgMS4xMiAxLjk2IDguMTUtNi42Ny01LjA4LTQuMzcgNy4xNS0xLjEyLTguMzEtOC4xNSAxLjk2IDUuMDgtNi42N0wwIDExLjM0bDguMzEtMS4xMi0xLjk3LTguMTUgNi42NyA1LjA4TDE3LjM5IDB6Ii8+PC9zdmc+"

/***/ }),

/***/ "./src/images/world.png":
/*!******************************!*\
  !*** ./src/images/world.png ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAADtkAAAFECAYAAAGPQIDPAAAABGdBTUEAALGPC/xhBQAAQABJREFUeAHsXQd8FMX3f9dyd8mlkg6BhN6bUhQUEBQEBRVFVIoCCgoWFCt2ReyK4B8QfyCIXbBiQVGqFOndQOgkpLfL5S5X9j+zl93bvdu927vbJBuY5RN2yps3b74zO2/etAMgD0EAIA2BcDkBgiBAECAIEATqDgFV3bEmnBWIQBZ1qOWJYOS6avz58s27bXHBpCG0BAGCAEGAIOCLgGIVLoUeLK4KPb5iN44QpZQBKVkay3BRi+tzcl25mRoSLh+SniBAECAIXIoIKFKZMYqKqZDGqHSVUAa5FC1TD8w7tu/J3yoqqesZP3kTBAgCdYdAZFKEw1JYoxXIoScK2y0QToIUioBaaXJhRfXoww8pTayg5DGZolzcBLffdAN4K2BuvNzu2ChVVV0pWyxr+basYXXJX248CD+CQGNAQG3QV6qiddlcWVNyxlL2jGiK+73dcm0k/X0Xx3ffFQXqYi59flw3WWazMM/kpCRXffZb3HJcrG5FKdzqh+Ip68MJ8O68D2i8UWXDte2a0Mrq+eee4ykxpVYIbqCVlWbVq6+8jKfDaTFj4hMAlwuXr67lxh9m2Y6syLrOB/PHeUUZ4Z36yIvkQRC4yBHQjLbkmpL3jm6DlazhpkwLfuMyx75/JW3d4u8N/616P5X9vs/Ed034v8jmTqR8Kfw3tPKYi+qxQJZ+Jr+ggO7AiNKVr+XV95Qy0xB88uUqI8O8EujaNAZ23Oo7i2L8oNQnrXxwhMeJWwaGk2vuOYiMjKQVLhMmVIbz589TTZs2jUY0ZoYu2Df+GINNIwf9CwtKal5eWKaXg1dd8sAdR4wp6lBllaVzXeZDeBMEAiGgMqU6b604pP4utkWeo9KcfpurmP52N56cLpj0gnWHT3hJ/xifsITMKWyYas8M1aqsKdQtcd1h+6m7oW+pSXLfKaRkG+PSHguGQhz1YuHiyqt9sLXqU3RvRbV8mElQ2WJF7E3rw6yBArBcWD7vR/10M7i7RwIvWKAM16Snp2NsKjFO+/bt8wWJx8HX01DKFkvy0oyEiE0r0oOW2bcUdReCccXcK8xVneouF8K5FgE1xvuHH35QdJtoqNqKesZMRT50nO57by4/ncYoWyyPeeh2p1xyYUsXK9tgH+Zb4aarhxm6RThftVp9mJvvxeaWXeG+++671KMzH2Gnf9VqmAqHW9G4rV2CjnsiNzM1gt+WB+Poj7LGSQGjsG5vFyGK80/9FzNKV/JoTZSZDBFGLZjFlC3DftHVbhdTPuyLNapcHBzWrXwjGXbu3EkTdtXe7E5Q+/+vv/5Kea8LcwlCUbY9rz8PeIQs9PdDn+Ch7X+ZAbq01YVsnXPLI7fbuwPx9sud30XKb7iEcl2HsUUPrTRGjhwJDz/8MNsXSEh/UZJEDHmDArVunpTCDc/eqZFCFwwNtm7xsy3ezA6A9BBZDhHRvLrJnxpH4b4MK1fmr3x6PJ025eNq+i1gLNDhYf73P9RmpmIeTqezA25AMTExPNnC5K+Y5MH3rH5Ex0Ax0d/PT4WbBkcxXoCOObSy9QQAmHpfALO5iq5cRhnhiuY+iR/XwHM9nfBwTyM3mHYLTc36ENVtgAk1wErvLEwfVoB5uu90Dy7ji8/NhqcqFroHF7UDESb9yXN2yLruDA+nbRWfQd++fRkSn2NSwSjb8/kOMI7m48syFnHkumqg8z9WkVjfYFWnEy1QKCqEMp6PhkRS4zvq2cEcI1V+fj6kpqbK2v4Z3hfjm/m2c3NzoU2bNtSjl69jsXtlg6d9epe9oKAA5t92Al7deAVL701zkfu1SNHZB98vXMrhr9p8IoSmlUOZUmYYMwqX8TNvLFPVa+w0s75wWpw1fkER2B9NYkjo9y2/OWFcKyfc0sZjCMnV987ovZpasOMWwZlPRojnBmxjnPBDyVTq9rFjVLNnz6bDGts0t2wWrsVicStbrETQH0/ZYmi8lMsrH5awyhZHb926lbfOicOmrDWDucoMT26qpjvM8meO4GD2qaPRFss/kMNb2WKFiv8cDodPB4954cHEi6/Modl2bhrrHoRwMslqpvPBqW/MXXSY80BLmtJkirQwSabfGeMedjIBft7Ykg1W2WJ26eoI2goe3rfID3dPFBoAnPb4GtaF2wdWtjfuaU8LYn4wnh25x77WgZ4pidSCz4BJgtRfYhqshGKiTYUS6BstSXFxMS4mO5DGSx9VVVWqt7YNYsvUp+VNrNvbgZUtfp69eivVO/3Wi9Jq8S4z14+VLddfl+6/Yi/zYZ+y/0WHTyAK4AwABuPvBP1ZExeW+ihbnHb1MA2tbHHfdt9694w3TiPEV2qYQR1ZiNtEnCENtw1Jyf45twL2H9jHKluciG6cnPYpiVEDEsmicO8fG139/YJMH2UhVq4NLxbC41oVO6pptswKPb4cwSO/7LOusPJIDS8sJSWFVmTuip8Dc/b+Qn/IiMjdoyIHroBoE3+qhMckBA/muX37draBGTRRFbix4PyvXN+KlcmbNZbzpu9Z/UhHY6X736S/Yeet7pkj6+JSSOx7yjupj1+jQQYCGrQUbEg2miLVSzDBgtmJBh9Cr4CrbsulFaZXcNDeldqWkvkEY3UHLYjEBNwOIb+wCDqmRYNWzTeycP0UPxBvksiSIdOi5nA7bhM44Oc1vyQyERfTG5cPPwkJwjMiVptnrHddxlO8ordr1ov1bz23knVf1/oxFf5uxnR6k/2W2MiLx9EMFWUo+ns0CkyfM8Vat5Bx8d/f6DN8sCgd93fAddx5n5bzGSHfrfE9fMJ+jDnlu/OUQ4W+kz8Z79N9A38KK/a588Xfztq1a31kZ3iJvbXqCBduA7P6rxP8bhJiknlJx3Z+m/WXmnaybm+Hu7V6Bobe8Urx83ug4KSKRh1rhdQkd8zKhy9+MUPXtlmw43rfxsLlgxUZ84hNV3GnGcRo5JhuwBXJyMJ9M/lvcs2G9ZvWcaNYN0ODA2Z3978EhhswWr+AM+uygLZ0WS6hO9D0LqCjAqEzEEmZsDlwtaO8w2lbIjlLC2aU7Wu7nJDXeiTMj10jmhDjjgdBz/9TXfPWTitvp3VhYSGVmOjuF56YHGdfurrip6ItWbfQzASWSJhMGrLsjAyhvodkTXf1zRinEvumMF/crt/7dyjM7PU7mw1DP7TNI9A7fSwbjqaSRS2YmQfuhyalexusnbBCyujAG5XEpnD7VqVRUbPyfMorZVrZe0pZ0/kEVRjXnceLu0OZKZKQLIx16/qgGZRM4rFgkom+X9GNgw8XLYELk420oRGlU1ksDuCsHQonnWVIqfm/mlLdo/03CBN4hTLt6Y4x46B1/gw6trTT5xB/6E5g4ryS0F6mz1XqEkZwaHNKKMWKoTocZ8+iMkm912iZcPzmKlrs35W7Cn459hZ2+jyZyV1gfAfa0PNbATgh2i0JN910k+SyiilZRohYUwI8dJl7UPDy+j4+ZWTomMpn/Pgtpnh/zKmBMT/z9xxNmXQ3fDxrEze5ZHdq39NwWNtFMn2whEpVuniD2cCBAwGeTGen9f21uVv2d4DVXd1LFV/8Z3NN+t1CTz0MHHi16++/N3jajNeSiNCeBG8Mj6A67dDKs+710TcV1NQXi2SZVfLOSw4/tjyE+OAxJxq80lE/5DwL+8/9SStdhva5P3pCREQEDGs7E3ql3c4EB3xjhYufGsoFaWX7PVgHTOkhMBqNTrScpZZjcO3hGrqLOQMrpOgwV0bZcXMIReFO7FEG70U157IBb4W75eRdoFWhZSrOc0dVP0fBrC1ax5HVUJbxMP2N+Ps+OElZZ9ZyG5ycqId78gbDsrR18F+JE7qvrBCsvxSV1nw4rjOtkN/rImLms5z5DkaxcvvRVvccBXNlFeR/654+P1jwO3ROxhMKvo8Sla4gSL6i80MCKdtt+6zQ947zbKKZvWNh7hXuKVQ2sNax5IANCpzCFiIG/OYOL7GAMhWAk3IrAYfjDkHEGKVz4tLXZi364ubjTWQ0REG1tconf0y3bNkncOIT9+z2vF3DYO4bc+DC1+6GUVVTAlER7um59Sc+hD9uEbe63k18BJ557nlP1t6dvSdG1CV0Rk+UOMSIQEq3vi29CDVU2JxUNO5A+vyeAA8+9iSMO/hkUKVDm0Hw7jwrPegKAfdHXi+H/62qhMp/8cyi71PfmPhKIBwipmyFqfmhRxLegw4lM/mBEnyl1efh5eOvspTI2sXTCbybk9hIEYf34LihFS+jcK0uO+w7c6+P1OWuErhlursf4EZ6K13vjVPeFq73951edpCydn+f158LKX2s8NOXpML+O3S0st2yZQv069fPZ/8MVzap7uUd58K0adNYGfBFHEzaJdZiMPf6mvFKfn/2331wV7uPJNN7E0pVurgd1UfbYcHxFlTM70/Z7j5shZ63ehQt5iE2evr8qA1u+rctnQ0z8sGKLpBiZJThbZ1eh/aJA+n0+D8cjjCD5wduBxflgA/3joIZ3X+GuJFboPyn/iydFEdh1UkoSF8Nncofo/kyMuE8/v33X/hxlmeJ5b6VTSEjI4PN2x//F9ZdDi8Ndq9DMKP7yPGnRZPg6U7mWbJkCUy5ci5raTDhQm/vj1GIRo6wO1tmw28rUv2yqi8F8/bVRsf07gbN9NJh8GH8b9D6SxUcd1/U41c+buShCX9ApxXXgmFqPDc4ODeaau59eWfYsaJaNF19YSIqgFeEVGW7aPuNMK3PT16pw/MW/3sLvGpIYZlstFfCzeYc0X5JtP/hTPFjfCN0UGWroSIxY3TxjKu6ulp4xM/mHJqjv6lV6aY2MwV/TSt23xOutbEFPjMaUqzcYBWut3V7y4klrifVW3h5DxjwLZQ1dw+McN+SlRQNR+7kW8AMCsxSC+OX8mb6q0F9roBVx6y8JEwfzwusJ08gpVufgzbRhi2EhVhjxxt/gnksn7Zgya/XZ8B1bZ+hFduJEyegZcuWAZXuvF3Xwz2T7oGYfWNYPvXl+Pzo/XBne8/UCFbC03t/AwnGDFERbI4qeHPLYDp+4IBBcBXMhbH7pkCa2tPY/SleIcZiSiENTSUfqsOpZG9ZGtrKjYuNdeVN9NoN5S2kBD/TWWBSPEj8p5sBrumLjV3pT+/xOtjxKdqUyun8hVLH9D71aWWVa4JQXH2HSVW2WK47Ku+GN86XQPP2P4Yl5g8HHoErWz4MSVFZNJ+vtl4P20yZPjzX2sth7HYVxPU+aQl4XakQ5jis9kHWywPI6flwmYgw39iK81Z2XJZCViaO91a61Nc9HSOyt7IbnE4uXQlnB3l27/ac+AP8shj/bLX78R5Ue8vgnW+u8zRcf7fb8MBtvfD+eIiO4Hf/3G8A5yJmLNWKwHulfVgOOTFdeWHYs7qmFE5fRm/qp+OwoRGu8j1RugNma/8Pvoj+hOYp5T8vpdsW6bL/fNKh9oKWpOD6FjOAGRS3adPKlZ19XGUwRPxts9mv8UkTZAAfcT+J5VC2a0/Zof+m1rxcwgWfx6yReRgrlxFbDqW76o8qGPSS58NkeNfVu5/+MBxZJzx9ivNkGq5c+cfFxNhKy8s9C6OIcTAdA5YDdyxiadZcvQRGjx5Nx5dYXZD+cBO5ROfxQbjgtYbdvMB69tzQdrare+oNkvsArHC5z6OniqFXl+At3j+y58IvtjNcVn7dHaoPQv4u/nql3wRekWkD8yGvwIxnhySX1YuFqJeZNo1vMVl09slb+WFmg+48C+pY/iDd37RyfvuvKWpvJit/MAq3813NQBNgTOqtbLGMYt8IjmOeomoXRH6bxXglvUPp8z/fezf81IrPfki2HSZf9hk/0I8PKd3bkR77SpQkwKANXT5kr6qy8PoeUV4iEWwFisTTweEq2+lrq+GtfPfaJjcfLvAudDmNWuWe9cFWY7O0TLinrWdkxE0n5v6vaCO8qF8a1MhHjJd3OO5sghlRMem116yFF154nrXaUaXzdm16K12cbvXl2TCuA2/DLMPO5y1k6SZedhqyjXW3YcpbCH9W7uKvKmDay0WS2pk3Xy//s2jq5xUmrAs6x/xv7dEqJkzqm9u54GUI2yO+SpWrlIUw9pfXXU9WwGc/FbpJAqwDyz0g8SeXd1wg69ZbwXqnZ/zBfhdf7JkMO7TB91v+2hkjS6C3vseJZ2tqYE4gOqnxjMLF9AeaDIAB0W0EkwopXW8r15/Crf7xFJRfV8Dy5irc+dX58EKH2Wzcx8c7QBdtH9rfbTxfqbNEHAfzPUhRsJxkgPY7BH0KYgNaMtjdcyWXTUC31HZ4A1qJuKv7MlF+f5VfA1s+D80YQd8pvWwY7qAtYEf4yIQY63tPJvr0/lKmkblTx6Io1EZwlS9WuCOuHwk9Lc8ESkbH4woZcugc/NnJbWkF2wFIyYSp9CkFneBs0Vq4u88fUpKxO6iZdeDv9z0AG9TsXoKAPAJZvYsPWOHhBb6N6Pe+GuilDbhbP2D+UggCdYThKpXevS5zbd+xk22rwXYM3mVgOhgmHJ0nhKvXeI6yMOHcd9sV5XBmSyY3yL9baLTsJ0W4GPlhLRgll7LFzAccLkBru56jfIIZCgSO2TcZmqKLVaQ+m1FnPdJzHF5qMlG6cDA/HdeFMtUaCNwMhlqLnf+2f9JtOXAiSh0WyD6HZ7U9T7+qZi7DrHPsOitVXQIj3vF8s/7WcbkKV2g6+Ul9Ovw2xkcMT+ZhutadscMVG/izlYFYcvv4QLTceKbv5YaJuQP1/a8svEssqeTwcNoN24kJ5WaKUu2v3JHlYyp1H3kWto0wCSWhw5ouLIdjJt/5fNEEKOKXmgr4D00P/H1mAZh0SZKOF0ipiL/KKiE/Y5W/rAXjpPC+NfsCjL7sN8H03EDnFWtAs3UEHSRk0XJphdzU2FOAzrsJRdFhYhYY96MUTSxDRPWqEmiaohXlFEoDZc7Snqt0Qf/VNXBqogHaf1YDR++S3kGLCoQiHC6KvQgDK2C8fyB93uX+ktBxYlhzEzrRveD0RSXcQAlu0+UnXq+qhqclkIZFEkjZMsxHl0+ACHQZ+m6zBXqaItkZHubbCNS5MXzE3hQ6DvToweli0YLhgQZ3gon8BIbQNlsdie10PJmz/8KbffPyw5S527s+H2zS/uedP8ec4WlBf1bualMzKvHAKJYPd6cy99vmKtwNlcegz/AV3iLJ7g/GmMKZP1V9AVJ6fxeWHKPLJ6L2qIKBh87C1L7r6D0FZQ4nxGndkEptj3IoXaYgwbYfFfObiwyDcN5a1Ik5AqwVSOWvrdGCI8IhldwvncrsAsrEDiT90gaKjKl0QkU075sJlEQ0vgk6v1acIA+vVKoGLqjkUUZpF+yQl+rZ0CVaAAkRmZQVTqkMEigDk2SVWeBkHL3xNDBxAIrWJVVwPMFjTQQg9xvdnqqGo6rgNliJMexIWeCwSp4yohXLPWjGvIdYXsGEJ55KhqJMz5RmMGm9aZNPpkBBVr53cEj+nq4q2K2Wpx6RAP+gvytDEsQrUfSeMqqyRxyrLL2iG8zbO7cUdqSHsQufI/nluy2ws6c8bTXtaFPIa3+ewz10p5y8rnRVwD/qmNCF4aRUqeFFebQQh6kSnVqNPB2+EstGZCIIEASUh0BrtV5xylZ5KF16EsmrcF2XHoBKKXFZiXzg56XpTstVLmTdes5myMWU8CEIEAQIAo0QAe3VWR/KJrYGzac7a+fTw2WqsqFpYL084wG1xQWuSHl4adCUslOmKeVoUylUJskzvROXUABlscnhwu5Oj09PoLlIOR58EgPvBJbjSS08BRlJmXKwgrTIHEhPaSULrwxbDiTr5eHVwnYMEvXCO12DFVZ/zNbD1sZnv2OwbGh6g90G1ix5eGWWHoVTWb6nFkIRrDU6MmQydg4lqU8adNUQqGRqq60KDkJ0ljxylVtzINYgT/tqS+0EQ1bgvQo+4AgEtMnbD5FZwe3VEWBDBxkrLNAmS57paWN5NeIlzxJPJ8su0EbiE3zhPyo0pyyPFgpfFsIhTAQqauRRamGKcUkmp9BaPHkaPwLtLPtlWb9t/EiQEtQVAtoLv62TjbcajQ5d2DqS4VG5kGUUxPEZf1lqnBpwauTpFLV2DTh08vAyW2vAbJBno5NKqy2iHAfxXbRhP4iXgUK/6SvHo9JpjZRdHl5ORwUUanPkEAtciFeB9pQsvMBeBhd0Z2XhpTFWwIXqM7LwSi+uhAs50bLwii4qg8oTcbLw0hWhMhbIs0HGEFUKF6rk2YBlpHnJszHMUIDaRKk8ctljrFBdcUoW7CNrCuFCdrksvIz5qIwVhbLwii43QWWeWRZeLbFcF+Rpq2fz80GXUiaLXLqoqHjt7s3DZWGGmWhdTrRLWSMLP63LhXjJY4BHoKnuGpmmuo1IEVVrxY/ABFP4xOpSKDLKM6WM7nSyoXngYLIXp1WprDJOKVfLNaXcynUCctQtxeUOIqat6zhkq4M7RyjGvr3rPziqbicWHVS4PS4bDpS1DSqNGHFNUTYcTJSHV5fC/+BAkjxldBYcgX3JHcTEDipck3gAdhf5nFwMigdDrEvaB7sLuzHesN76pD2wO7tHWDyYxJqUY+DMl2mZwbUD7eruzbAO663L3Qu707uHxYNJ3Ky8Cs7FyrPbPCIXYZ8uD/ZyyoUWLErl0WgMajK+8c1Tcj16F/8ibbn4Ej4EAYIAQUAIgW2FPbcIhYcS5sxvtymUdHWdRpNC+tVgMVbNM/0gk1lEZ423ysqixNEkMPonj7WsQ78eZFfJY5UCtrqR9S3Howcb+ifPRhQNaIoQYrJMKatBk+8Cp+cnXMIoLJLrPJKraRgs2KTRUeVQWRXL+sNxxJjKoMIsz7RTQnoFlOTKc1YvoWkZlJyXR654qIBSkEkuFSojJRMvQGUEecqYnFkCBad8f+4ulLaR0rIY8k/4XvMZCq8IMByoAasspjfitR/xkmV3kg70++1gk4VXakI+XCiRpZuAZBWqR0qmekS88mXilYh+LbII5GkTejDO0/bRDQ6lPQmmQSYzumOHkkXhqpEqcsmkjNRQhXjJM11h1zhA55RHeWtQd+gEBU4pA5pSlml6GrUJK2oTgu0l2EC9/gTYaloGm0yQXm84DjabPFPKkdHHwaKTi1c24iXPNDDaTbEHIS/L3Brabb4TLQ3Isr3V1OEgmI/Is4PXFHsAzDpZ9BqYYvdBpk6eKWW0xFMh3zekLke7YwTbcbCBKlCXycVLXane0ELnGhCsDEL0qK1uyEK3hArFBRsW02MnVOyRpamCnLzQN6TcKeVgQa4veoPTUl9ZkXwaCQLVJ1oflEvU6px2++TiRfgQBBoLAtHdd8qibJVeXm2fzE/kk1HG85vot64saOOOLAezKl02Klqmm19slMOsV2lNMoGGtzHKMycDeHc4smeU9sgrVjY02SiT+ac6AvGb5dm5I2e7l7P+lCpX2gWA6p3ylDQ9DwCdlZTlScsFMO+RhZWsTOSsRzl5ydnnpJ4DyJRp3KpUXgh7WaZ/ZW1cdcAsWmNA0zvyPHq1Fu0GJg9BgCBw0SFwrOk/F12ZSIEUhcAloXAVhTgRhiBwqSNwInWHbBDkpG+TjRdh1HAIHG26oeEyr7+ctSWnPpYtN7RBBk1qyjStKePUh5xyyclLzhkZOWdu5eUlY5vATQsLJ8sjZyllEYhmIqdUcvLCPwKGfgxMlgffZ4PutZHlkfN7lLHLkfPTRrzk+4bUiBe6NFcm7OVbxJKzrTqj7KA5pZOnjKdU65FOGygHM/wNEQtXDiQJD4IAQYAgQBBQBAK6Q7r1ihBEQAiicAVAIUEEAYIAQYAgQBCQGwFtwmbZ9hMh2eScGJCxqHKKpdx5J2XO78hYjfI2LxkbhYys6DkneY5cyvs9yllGWXkhZjL9wg86GSEfL6W2+0sBeznrUU5eyLwlFq6cHwbhRRAgCBAECAIEAREEiMIVAYYEEwQIAgQBggBBQE4E5LmsWE6JCK+GQAD/wjIefFU2ROYkT4IAQYAgcLEjgGfzyXOJINClja5s//cZQd38r+p0oieCR4FX71wilUaKSRAgCFwUCChyKnn200+70AXpVIsWzWXbNlLftYXlxw/Kd0R9583NLzpKvYI61JLCf8EqWswHpdvNpOfyJW6CAEGAIEAQkI6A4ixbnVZ7uMZuZ++1Rb+qoDgZJcB7HdKzvzN0DVEGU6T6/yr/zbyfkUHON7J2G2OdyAkB4UUQaFAEOrTSVR7JsUc3qBAk86AQUJRlGxUVaVv2ySesosUlqbUOgypUQxNzFW1DlAFbonWlaOnyIP5ZTbVkfbehGxrJ/6JCICVnrPeVT91xGP6eS7e2YOOw//+eTTQVx3dnw2qBGPdX64e8w0LGCPe9+AmZAUnIQ0Axyvbm1jpL0WR9xOgdD/IEnD9/PqNwU3kRCvTE6eEv3Dj7du/Ek876cAJUPxSPG+0oXoTMHo0GJuIPUWa2guxOrG1uqq+8BAUggQSBiwiB21zFVMrqji6sXGmlG6kpRm92r0RcjAYv6dCKFxf7zscL6CU2rHD1oKrCb/T36aDottBEG/VbuNBwlWxkZGSjXc4LFwc509frdCCuQKEp1aEttObvR0XTvwRvmFdClw8rKO9n6KrKqo3nHXL9BJ43+7D8tcrUhwcuz++//w4DfrmDjjN+UNoNOfZ7E4ph400n5h95TWTVD/NTZfnZQrE8xMIbw7Qy03kItT+xcpFwgkBdIKC/aTk1cvUNYK+shO9jM1U3XjhKGZKT6Kw2npzuk+UFq+9vOvTometcF9ne5zRJQuYUOr1qzwwV+unRdyq6vfPot6V74LZT/wumr0fmNPUFV5CE+HiqtKxMMcYZV7bG4q4X8HBHhx8MCvPmADSGUbQ4bP2YGBBStNPLhsMXI6KwQlbcOoVRC1WtvtLAo5sdnGK5nbgsjKLFIUgp+/yIOIOJy0XvCwvaMl34fKKjoRQtLpPSLVzuyJzBGstNnrpBoHXrNnRDrhvujZ+rtuNo2Dl1JuiiowFbtIyiDaZkQor2G5vbUMF8qB4LKKxosTujfB5sizdL6lfQt+JE3witaCsqKnBy+PoGE+ROUKn+GRstiQedKMj/8HeZl5dXZ/yDFKdOyIMZ7UgRIN1qtZ6Pi4v+12q198YJ6I64Yw6ddve3zaBnRz2Pj3VxKe3HFmB0pB4K76UNXB4N9nT+0gHH8ytYRYwsRLll98lTSgBj0WL5hQYJDA8cf+zYMchY0IcOMkyNZ6I8b4QT3g+GMAPGWoyIiHDl5+er4uPjBcsbH6OuKtmaGbRF+0VvCoZGxHry5rh6aw7B8Q0ZnBBpTkZmadT1QxUVFeUym8087Ih1WzfYCw1kLnWsIyHqY4vGPh6cNfiDt0Q941F6w1/1/elsqZZtSf8Yn0o0Nb8bItRan/Dtp+6mwwaUJi+0geUB5Ln35ohJH3xXs9TIJWb6Mm4Y7rfww/Rt5TYKUheX8b4nLn0o7kul3chm2UZF6g8h0M7r9Xqorq7pxV1fOP5rc7oOvBUtIOXyWNVItn6WLl/JuhkHU9kHx2rZCsdxFdPjGnwUxG2cTGPEsjEyYzf3adOmDe2l42sHINx47MaKtvZNr8/YbDZVXFyc0IwAJjMEo2g39dUB/kjxn5iixUx3ODuxdPv/8+0QMI3QQw+shCIaKOyTYVHOoskRqvt68gcVtR83r6NpIBEbRbZIYW7EmOGnQ4eOrrlzX6eevXor++eOEd5Ig+n0av2xRlHQOhDyr/j8yVFPlkQgJVvFVbRiWRX3XOOzPjqn1iARS8OECynaaUcKnEz8hviC+7GFi/4++m3qUwbQRpYzcbgvE+q3uP0apo3Vq6BplMrMpAv3jduHEA+mTXVuOohtZyPaPkFFR0e7p/8QwR9//OmDlRAvpYTJpmzNVdaOXTu3Bjjcyv3HKWHrFug3B3E455k/4wLteyfqR1o54Uq9cfNUDoVHaeFG4N0QdBoVROngfV6CevTgjpybXdNldlpGRk71W+7yMTS4fAXT4mHfnb+ygwbGqmdovDGiwzl44gbI0iIHUm7VXL+Yu1vvXFp5dtIGr1+aTU6i04rx9g7/7M1kRXwAp6bEUre3jaDb90e73X0KrgPmD3UuFu5gybscAfxf4ikv7/oIkKbRRRsjjMW4jGh54ypG+MOHD6l69aInrZgg0ffixYvpuMf7r28t1qmKJr6IItYtFC7Mj3ds8Im4ufSUT5/8/NJK3nfvk8hPwD3GJ3zWdTG5Ojodop4owOZxpJiiZdjibwY/TN92fHKc8PQjk0DCmxmsYdLnBmwTTXFTq9fYuNR+pYCmtlmresiQwSrcPhMSEhTR57CCijh8KlaEzm8wbdEgpbD/a2ltAiuZvwuNdMeXvtTKKh9uJsvaz+F6aTejdPF7zt5fYOYVWx+e2XcNm6nRaKyTtSJcoUiA3YxAuKEct/ypxjIwMhVX8E/CoOlfeHn3j0wS+h2DRoXdPr+edi+/HrdzND0jcdSKFfGPC1Ld0/IonVQrEluxf0e0p/MK5z/MJ/PqMwFZ3DnCxH4MAYnriCAdjbxTIvlNm+kwmCwX77fCphu+gvIgZ0hwW0DP7amp7s3xi19KrGF4Xixvo8FAf0cWm8Xdy3oVbPDgQbyQPi1G8/z9+w6k/VOn8gfPnA62CS/BxeXpiYozXg3qN7jrpPa9K9h+iimuttV1jNPv2zC2lU9avwlqI52UsA4afL8nNVK0VdjHDEI9MXzXN9nuZj5lg/vzxh8Bn0Kaj9MG2ASvbOhLuz///HPod8UANhzNqLBu7Ph46Uc8P+MpLi6mlW55eXlIMjF86vrN75GCyA139syflGS3PYc24aKpU0a5fH6NuyHkTjLwkj+0eR6tSHO/7MELF/MMe0lLT7HiyrdYLHTtYLfJaOJrPzEGfsI7d+7CKm/Esgfmi5/3d4zwk8od1bNbb9Cg9ROskGMWWHzob2/rWVuhMUHYRHQ/60PHDRh5jXtAySjaZav9FxErSDmf3a7OML5XWUCWjHwBCeuIIKd25M2MxL0VLc52alcDXHvttfBewgzmWJaPNO7adv/fJE7zb/GWFhQz+zBqSAo9W3PfrTE65jvA7yijKs+HUSMKwJ3hqNZz+L2cl/zYEnn8p45s6PbTq1g3jhukf522Vp7ot44N5zpQHkX4qAr68n1H1FzCRui+t8mV25GSXfFPfMUTXPE3bp0giGnFf4Fn2KOf7hGwn77VbrFz88PunacneQdBqauQDSsyZ7DuQI7b2kbQCnnl3mKa9MiRI2JLW4KscH3jtiUU2T9rHN1ejixuCddEvMGSoC+PdeeUbId7eixh/UKOM2fOQG0ejwjFN3SYYAMIJJSUznT0IwWwaq2vMhDq+HB+N/x0B1yRMZ6XNTPi4QXWerhTD/7oMLkhIqLQZrcnC/HxDovQRbhsNTa/uEjNm0uH83lz80AonyG8l4lRDDx58BRygOf2R/Phq3dRx+/1yK1ouezbWg5A0e4W3CAfd0NtlsKqEbex7j8a4ejJ84KzJoywT2yxQ79UCka1iqCDuJvuMB+GjlGwjL+86W6IPY8NGPHHUu2CSCO/j2woTMSl9MTo1AbHk/3/Fpxy9FAB4G/Nu10z3593ODedt3vjyUXwnXkfHdykdC+2dEu8aaT4cT2hy3CcFku1Z/QqJWEd0BxqP5vqaEyDd7NToV/EMJ8c7qka4jgz608fOb03Sv2xcjTo+/GvFfA+/uP9fTNHfriZMhujuGGMVYsVbcXso1D2VFtoHs1vp1x6ITdOFzOnPT2rh7817nfjTY+VLA5DW5zhg66LvKNF/Y+sbgNNmjQRbGtS2lmFrQA+2D7Kbz8umnkdRQQtTO765lRakk974YvntflHTMHiRFUrmsP7tZVwpPBv6JA0iOXl/WEzH/WIYaOgZ/XTNB0O27RpE1x1FbusxKZnHDd3eBk6J0ubsrlzUSx06NCBSSr4jo6Mg0d6/UbHMTJhD7cReMvucNlAq3bvxK759yZ46V7x2Ueu4t29ezf0NNwmKAcTOGRSLvy5NJ3xBrXGyiYK0pGwuSJgivpWLrjjxSPurEX92I4goJBeBLjjwHzoYAmDHa/k9OyNt3Lm0tQ3Jty8xdx6TZTr8X5/Bt0XiPGTGj7zAGc+EyVCSjcoGd59MsE58/VinqZoyN3P+LgNU3YhRYfjGGXH0OG3t7J1mM3wT+GTXBIIVtm22veh8/PYf3mDp76WplTUY+dVWNFuHPElXHfddbB8+XK4ffdMXl6heHCfha7a3eRwOq/G6SdHJFa/GdWMnbZ8r4vIwnWAzE6U7oCW8dL2CAixenXjFQHb1FNPPek6efIkfPXV17y2JMQvnLCgmDdP15r9KVpT7wvuzqZWoqxmqaKWheXTFoD/xiW6lSDu37qM4SsgrvLCLK9p7V4DWvPbD2yZMc3aZ9EGLPQ82Oc7VuHh8OT4ZtClY0/Jihbz+HxaObz66hy4puU0mtdzz7xIv5PimuJo2s0oWjqg9j9vWb39czcNYMlv1SXQZV/5P+FpXjw4sdvds0I9eyILCg1eOo8qYNN7O7CirbK4p+Wfe9d9lMqbRm6/98habv7B8mM2O2FFe3LaFtF2J8a3xklBGto/QPPBSjYERdtymGeKTiwfJYZLUbSo05JddO8OmLGCxDIyGGAOd8p+5oQ4Na6nhQtrO3LkNupVeDotgTmzzg6cxJiGGV7V7V20wLSA/uOy6pP5CdfLuoU2S33X/SHeAX2tKfx7e7wVLRYgqtvttKL9uPWLtKLF/YyYov26do2WFTyAo33L5mB3OK7CeON6lEPR4izFFG3Gef8GCCOuTq13d4xMgMAb7a5XffnlV/Qguy7bS0Ctz8gWZYS3zTtbPsb4mfeu14qgUxPeAIqJEnxnLCqH/6K6snHMBydmIbKEHIe31VhYdRKSorI4FPXnnP17d5gzdK/fDLG8R48ehfbt27ODAe6ovvTmk9DUJH3c02J5OeT/kymYZ30qwUDTyUPvvVCx9h8L/9yNoNQhB2rRx2H3N3MilTPePY43tbVJjoYDd+hA8Bx0AGZv/q8cnpgcC5tLlkP/hImi1Aqybpvg9VNRQTkRfQrvgJYGPXwR/QknNDTnzXsnwXfdl7KJud8CG4gcW+dcgC17rM7jZ2qqv343VVQDVZhd8PGmB+DR6z3TlG2GF8KxU57ZlzqyeF9EiuUFoSlcphxCFq4U69b7vK0/y7ZvRbYju+ubvOlG73y7jXevz3ZaFQGHRtf4zPygGR28BsuIDXP7R8LMy1jDlA0Xc2DDSehh+ncct/Sfa2HSlX8IkQUVdkfl3TS91LZYO1Bkdd2ahanU8Kv5y3l7jtigx+hz9CDb0ONEjq0GWqNMOiBMDuPM5Gg/rAC09H7+E1qnZTY7+UnGi/KuEG5F8AgvAc+evNWwosjT8LKdVuh+d77kkvdfVQk71zb3oa9PZYszDzSdLLdicTgclAZdAs088SYj5E02Mt6A757fuGD3beIDGzwdNrRNLPwwXBOSwg0oQC2B3LhIzZdLJ7ZhhUvDuJkOjvHjt9TOjpsGu8WUqzcd4w/Uxhg60TeaGdq8eTNeapLc34ny4kR01hhLN8S0i8NBYgp32PEFzpe0Oz0Ntja9t8L1nkr2p2zRJwAVAz1jWO+8xRQtR3QfJ3fpCkdKHcB69+k+jL0CQu3zR5ZNgCgN/7sNpv3h6eStn6dTfbv5GUDg5U/OjNbh4zXQceRZtgThKlxJjU9I0fYeegY23hL45kQ8PffB0kh4wMDfn8QF3WIvg0gd3WbpDRi4dN7TsGyJ/Thwh/D0mVLo2skzzeyHXHIU5mt2OuGnuE8lp8GEv5x8FXad+Rmu6jcIBmrn+qTFIy40MuaFX151CA5PEx3E82itd8QAvqDc+6lPhRuoI5RLqXhP70jtDLyxyVhhh8JSz8Y9bz6400EfFTzd2wjP9zXA27us8OxHad5s/Ptr9yyMHZEMX74l/o0gbIYhRr/7Z1Y3sYEUrZByFZJkSkEXGNzKZ8JLiJQNC1bZ4oSB2hnLPIBDrvaIsymM60apOcdTvJUeI8rK45dDG21nxku/+0Vf7zSM+5X38XIV7p99hjgivmzHWqtcy/b2x/IdC7e3YeO882WUbdGAVBjc3L3Exsvcy4Pb/PSeUfDOVe49JV7Rot5gFS1mxO33RRl7RUhti4GU7ysL7/LiHIS39psOR+EGVLZCihaLGMiqLV2RAXoVfyTir2hMJTDTyVKVrdVRCW//cx0c6tGOZv+68yFoEed/p6g/OYTiOly4HbqjeXT82NFUy7cxy4XIBMNweeJjEmFGj5/p+GA6mq7mg3D8fvHOGjMUm+5UisKVo3PjKtq5c16BmQXvCWItNTCUUbwYzkJ5drnVBQcOn+SNkoXomLCjJ2qgw43nAn6LDL0cb7mULZYlUCcnJm8w3wLmIZfCxbxQu+yGXvuxO9gnXqXZfDyuSz+hdN6Kj6FhFCDjx+9grFuusk3qe9r5n7YLq6i5eRr2PE5tiC9UMdPG3PzkdAeraJ9t9RQ0iRSeavYnl1RFi3kEaoc77dfCrx/zjT5/eQvFhfOt+tWGoSpaXBHBKFrvQklVtLgi7ql+kFW0mM9Tmg8AhzN/3ryl+Ncem8umx3wYRYvT6tBINhjeuCylFe5lMaeLtw8ioCj7TZ3pjVT+CMUGPXJ2TP7ylxA3WQINj2Rchwj6die8WWnbtm30UZ6kuGh6aitcRYszwtYs8zf9vkn0+hVPAAEPxrnnTWcFYnyD2rZDAz/OdJQvBT+kfcsIyZeU8FOG5OsRSNFirredjqeZM+t4p9EiFrczWxG5iPZzw0KSJohEcg4gUd+2L4isWVLcJ4opWkxUcupjlpbrENow9dD7n3BJ4JvI1p5FU14M3/OGsxmraPkxAHE6Y50r2r+WNfHONqA/FEWLmeL2xfwxmWD/u/AU7T1ebaPf951w97EMjdD7ct0fENXdfU5YKF5KGPOt4naAVrPGSUnD0Kjo305kfORNECAIEAQIAgQBgoDsCPi1bGXPjTAkCBAECAIEAYLAJYgAUbaXYKWTIhMECAIEAYJA/SKgwj9eXL9ZktwIAgQBggBBgCBwaSFALNtLq75JaQkCBAGCAEGgARDQXvhtXQNkS7IkCBAECAIEAYLApYOAKuoZM5lGvnTqm5SUIEAQIAgQBBoAATKN3ACgkywJAgQBggBB4NJCQDXP9AOxbC+tOielJQgQBAgCBIF6RkC1LZ5MI9cz5iQ7ggBBgCBAELjEECDTyJdYhZPiEgQIAgQBgkD9I6DCP3xc/9mSHAkCBAGCAEGAIHDpIEAs20unrklJCQIEAYIAQaCBECDKtoGAJ9kSBAgCBAGCwKWDgAr9eDmZRr506puUlCBAECAIEAQaAAFi2TYA6CRLggBBgCBAELi0ECDK9tKqb1JaggBBgCBAEGgABFT4F+cbIF+SJUGAIEAQIAgQBC4ZBIhle8lUNSkoQYAgQBAgCDQUAkTZNhTyJF+CAEGAIEAQuGQQUF0yJSUFJQhc2ggM0mpg7S+LUrXXXhkZEhJbdlth8OS8GlsNdSti8FNITEgiggBBgCBAECAIEAQIAgQBgkAdIUCM2yCBpdCDk8TExFCVlZVkJj5I/OQit9lsVEREBDw7+xlqzmtzST24gdVMGGWqWP5acmjWa5iVM+vN4pp3lpcnIDZVYbIiyQkCBAGCAEGAIEAQIAgQBAgCQSNAjFuJkMVEmyrKKyqjvclVKtXdKGy5dzjx1w0Cb7/1puuxWY/7tFtUDz5hdSOBcrhGR6pOVPyblaUciXwliex5sqzaRsX7xpAQggBBgCBAECAIEAQIAgQBgoC8CFxyBkGw8EVFGvebqyxd/KWLiop0WSzVGn80JC58BJhVc3+cLnIjV2Xbk+WKiGi8n62q0wkjqj+rvzokcQQBggBBgCBAEGgECKhTcsY689t+RVH7s1QxvU9mV1ZR7UTk7oZuk9uL40r6x0Bm6YHlleC8W4RW8cHRJlN1RWWlwWQ0HKmy2joqXmAi4CWFANnOKVLdep3WcfjwYapoiqGL9eEESIjGY3Lhp6rKopZieAmnJqGBEIgxmayB8MV1hP8euCwak+Kt43h77EXxIIOWwlesor9GbdjiykBlqK4tC7ky9qJonaQQBAGCAEHgIkJArZ52m6uYimiVWIMMVwr/qUw6/lEbk2ZJbZwTlzz6uZ70jHPFjqy2jH6LMqpKUdStjB+9acP2odeK6DSn4rtMxL/A2UVjqOSiZwLNnzic+aN6LKBa6BL4+XMT1LM7MjLSicdY2LDFWZurrR1mP/O0q57FINkRBPwi0HiXgPwWK/TI2b0Ntmf7GiO4HL7o9jbcM2kSG5Q3NQ7iDWo4b3ZBU5Pw/EB2qRO6fVpB8GVRC8rRrPqh+LNiKWwOCvRaFQzf1Rb+2ryNJdswJgb6pGlZP3bELCh90O6CBbzARuAx6lUVlt1ZPtvgG4HoQYsY2/ukpaKKigo6IUkgiIApKso2bOh1um9Xf4f7MYcgEQkkCBAECAKXGAItJt5hOb38i9Go2L8KFR0btdzwjSenc71+3ResO/zGM5FxvU+5TkR0FR44MkS17xc1sfYPMm7TMcGqPTOaIncu4+e8tU00kaXlTse7Dqh5gRMuq3PBgvmu6dNniI5royIjKUt1taSyySoYYUYQ8EJAtJF60Sneq1ar39+1a9fDPXr0wLL2RhNLbE+DtqregsK+81cIgwYqS6fHm4RoDPNK6GB8rBMZXUIkbNjgHxzQqvdg+ChtAx1m/KA0FjkqWALi8IvA1U215t9HR0ctOlADjuEvwoyzr/mlx5FM/Wzbtg26fzFckB7VQ8C2vnz5cteECRNoOrz4i9qUatmyZdSB/Xupd9+bVy8d9puzEuyP3xPHt9AFS3TxBf62yQLXT7sQsJ4uvpLLV6JFCxe6pk6bxsMQ9VsxKAfe6oB8ORJOBIH6RQCvGl3kx0/qF9BLI7erop4xb8RFHf6qTXKJf3l0AJge7CyJXqpxO+cyK9xvTJbEMyFziiQ6TPR6djwMihgFI8vamguoXNknxvF3508YvHOOft7IhfvunUx9uvLzehkz+ZMplDij0UivQlcTIz0U+BSThjcIUoxUQQry4ovPuV544WXRsuRvbAE/b7S4pjxXJHguFhmsvI82r8oFWR+X0dtcgxSFJWcMLuaDn7XBYv1wn018bzOb8pJ0dEN1QG/ZabuyBs4Um2kQGOyCRaTNV2r4avWP0POrG3hJv8muoSb8ViXU4fZHW4Y2tb/JDkezz/DScD1oQNUK+U9ww+RyPzUltnruzCb0Nh+5eErlU1bhhJ83WCCv0Aku1K03TdHAsP6RkBgv+LlIZRsy3Ze/ml13zCpomMxDlrphExoNBheaMRftA+NiY8rRhXhxDSslyZ0gEBwCGRnNXWfOnBZs10MGD6HW/bVOqD8PLhNCfXEjoIs0Rz1ewO4MchUfgxsWNpdcZqmrt1KNW3zeVsqTWX6Equj2jmDb905v2vs0tS4uj6Utcl2AG8pbs35vein+R3sabHP6GyNi/68CZvY0wIt93Rsau31eDUNvmwhv6r5mFxYYft5jtsoaCpIXlYUlB8O7Ht79kf2+SSifY8eOQdu2bVugOPEBolBCEtZgCCi20RmNBie6pMlHcX3//fdwU9vHYO0WCwzoZQQ953KdoVPN8PumfDeYh7Ed4v957vOxELPxPXiwh8emYIxSJqX3x8qEc9/xH1VD6X0eu7XQ4oKMJWU0CfqpGnjWvIhLDlJWEXkJLlKPSQe7C++Pp5famSIy+AutwrZaboOciXqGVPTN8MAEfbPiYf1IdzN/dXs1fJmjhkenGeGBO/CCuviTPigfcvPdRjZ4taU/t1qgS5sISElEC6wdcwSZSF1ZQDce/4xuPB4hyESmwGa9zsB+vbTZZ6lZDnAchQPb0qWSh0RnuvzkyapqqmVIiS+RRIMGDnD99fd61fejomFYps5nsMGF4fTp05CZmanYPp8rK3E3TgTmz5/vmjFjhsqgM1ywOWxpQqUw6RJcj1yxhtcOs4s2wersZyin0wnr1q1TDRo0SCipYFifpmNhR+5XPH6ChCTwokRgW7yZuoLq4op84IDPeFGswFVvJlK31ZyX1GYayriVump7uqYELuQ+KlZUwfDB96Pf63vNJFj+aB1sLrg/vp9gQoHAEWvQN3u8nI7ZuHEj9F51kw/VllwHDPm2UjA/H+J6DtCq9S670xqUbK1atqJy83LBarWqnr16K09ixAve2DIID47yeBFuj2r8+InOFSs+wUPEoPIU4EWC/CCgSHDFtj9c1jkedn1du/VBpFBFyduhRfMMqNopPvDuNDIfDh2vNVwQH8aAXdjiWZj5qLuTWHNzNAxuzh51EMztrjVmWHW8hheHDttDyb0eY5kXyfF0WVFedbzMJbgNmkN20Tq9V8uFCnrLj5XQa/wz8NLLL/GimfriBXp50pfZoKTCfQfDiclxkI7ORs/a4oAFOys8lF5Gqyei1oUM19YtYuD4r0k+UdyAF74ci2Scww2i3V9//TXcfvvtot8YvmjCJ1GYAXjn0OF+kZCm9t92w8zGJ3kV5YSkvyrBoJc8vvDhIRaAblgWxVAszcUefnxSLOV93p87qYPLj49QoIk0iDNq4MJ9sfDavzb4L2UQDL3hZrhz/+PQ5MPSjy1OuLchsFq8eDH12GOP5prNVfgMGXkaOQIjbxzl+uHH7wN+p7h/en7gdp/SRgz5E5577lmfcH8BNTU18Mq1u3kk1fZyeGfrsEgUWM2LIJ6LEoGJhsdq7je+RCu7oZaODsdjhyUf6Wnf8n/QctK4gLgUbtoKR5qtDEgXN+I7x9FVTQPmL2Xltn3Ff86Crm9J2r20/dTdAWVjCF6omuTaPGspq6S5Bi5zNK/KTkGT/8N3YXnGxkx6qe+BP1IwqbUdJnR0r/Yy6RL+r/Stagc8wfgb6q3XRLoe77eO11/tylsFv2S/FbRIzw3w3PvCJF6aPQbO551hvH7fyMb9CxEM9ktEIoNGgFe5QaeWOcGIEde7fv75F1qm5MRoKNgo7VyCVDGyrjkNR26PgZwyJ3RaXg4ZKU3g2Fi3fbGq93wwV1bAxCOzA7Jr9okDiso5RpKfFAmxcZB7N9uX0JSzt1TBgj0qcCKDwEk5JqLAFX5Y4Kgv0d9i9Pc39ijxGThgoGv9hvX8ggoIGhehc1TaQROv18OhiRqI8TKGVnZ5E6ZMmSKQ0jdIipHrcFFgml8K39xgghtbuTtaxghg0uumxIFGI/OnwFnRRZ0X7rhwB8Y81yPD9hfGE+4bDxhLr/K/Eh1uHsGmN/xRBpHGgM1BMlvjZadetVpdz0lOcJES/nqziRqY4TtxwbRpXOzx3eJgyUDp2Nf1TpLJkye5Pv74f/QHdv/906iFCxfxPjYyg92oGmuvX3/9dcewYcNCEnpgy8lwVYbvfMorG/qGxE9oYMlltP7kQmrz2RXSPwZuYuJWNAJGiCr8Oz4/kSvkAfsOeOSh3twgv26p52+lrN4W9f/B5fgzzW9byzljh/g7m/iVCUdKXbUNxrDta06m0PZsXt/rLM6GUr3Hriq1uiBtsXvXIZaDGSNhdzAPVx+VPBAPkTpPtnYnBTEf1u9WZb3GWPV4v7/wpJfoM2/n9VBR5TbqRYkEIsT6oFD7NJyFXq+n0OSd37YkIAoJ4iDgaXGcwHpyqk2RqvwVc5MTbx7CHoeQNeuaGheMnt0RvmnN3zYQSiYL9lihnJ5gcadekXMHnD53MmhWH3wwHwpX9fJJt3DvLchgzvUJ5waYTCaqqkrwzCiXrN7dBw4coDp39r/tVe4OYETbp6Bnmnv7y67cb2D18GWylDt5aQ1UbE+RhZcQk2H35VHd2kWo3ngssIITSs8Na3LZaThm9PsTzFzyBgWOCvoAAEAASURBVHF3tx6CMzszZMn7stvOmXcfrpH9ogxZhKtjJlE6eL/o/viHudngSbrMGDVo1KF148wABA9g8CRQ9IKyYC+eGhodbVqDlPB+m62mJ1c2xq2P0FBTbkuEecvPowkkDR2MfmIN5r8yEA6eTodNH3vuuVq11gyr11nM5/Lsji17bSa0QxWPujYyvMi7wRAYgrbe/fHiX73YOgxHktSETLi3C56v5T9dpp+BMWPG8AORz1t3nCz9F1buf5Cm27x5M/w+O+BiGctzx/mvYW3Oe0NRwFo2kDgUi0BN93nU7tOTg5YPb7uV+kgxcHfcM91hfR78NrSanYVQ0tn/mLDXmHPO33M7ujtCEQHvNZ+CVZ1fFYn1BF978C/Hq6YVfmViqK/q+JIzYsALvHy/OZIKgwQmSpk04byv/9kFf+f4Gsm/X7MMRo0aRbPu1KkjdfjwkTox4Nqp9RW39V8veaxgdVTCW1uupeVKSkiBaV1+4BW/stvX8P4H7/LCsMe7b2IIwjFsGR74PfvqLUiuIZTdZa0TnLh5XWzu0EZFoaOgQStWsv40RfsbimD2Swtg/PjxglK9OyASHuhuEIwLFHjN6qEwuCVvPMlLEkoDntBtIbSI4x0zZXkGyw9t7YNp06a9jBi8wDKpY0dMTKyzvLwsqA9txgMPQfyhOwUl6/7QeRg9erRgnFjgPRMmQ7PTvrP/DP3bW66B0umh1blr7jnAW8uFnm5dO8O46/Lg8UlxQtEhhW3ZbYV+6LKGYJ5ml5+F/YZOwSRpcNrh9mzYtj01bDk+RVvVJzxdWN/9Vthyh8PAewt/uY2ClEWeGeZQZtdr8Oz5AmEegS4BMZvNVFSU74RkaWkpxOddLl5UZjdDoOMAIhyie5/6zFzlEtpHeF2r5tqvlr2aFHfynAOmvlg4x1oDwe1xFcnzEg7ujQxan/3Dr268AvBOEebp2rEnGIxG2LFrCx3UM3MYjGjxImw5txR+P/oh6HQ6OvzO28dBqwszmGQ+73m7hkGF2TMYxgSZSZ1hfMePfWjDDXhg/1TQqzwqrGnp3tetAE+HyzeM9G/HxprGlpebm4XB46JLGq02nK3o9jaNSfS+Wa4/Y4s8lSahtFINXPuOBa5Ra+8NyFvK6m2gS6Va9j3r2Knt5NcglbJq2+XwG46PI4/45cNAdM29NlBp9YwXqPXPQvHly1l/XTs6f2+E46fP81aBuROrLZaUWQqqw/spwCcNKTVPGNPcnQ0q0HtdFtZJsYTG6GIGLhYA95W4z2Se+NhEuK/LN1BafQ5mLOsGXbt2hTVr1sCY28bCik8/4Y2Fhfh+sH0UVWErCNhWmfwu5Xe9DRLlOl9obXkIDAZhYwBfQjRtzHAorrJDVnwErB3hUcKBKnnzOTusOrMEEiMzA5HS8S/93Rv/VAztxg24d+ZN6EIXt37kxnGZCTVWHL9o361QWHaOSxqS22KxwNzr97Npd+auhqMFa11V9jKIiUhUJZnaQIqpnSrWkAYRGiOYa4qhpPos5Fccooqrz1JOyo7okqBP84nq5rHdWD5CHzQbGYRDrPxfZ8+E//J8V9ejI+PhkV7un6P7cN+NUFJWyObWJ/NWuK7FLNbvzzF/+0i4MNXhj4QXl4t+v7jjp2bolIa2sBdXQ5lZ/PhWWmoq5P3lO9DnMZTgSep3Cgq3ZPql3P+fDZpN9n/+1y8DBUSufSYXxg4P76j5nI9KHc/OK2UVmQKKVSciNG+e4Tp9+gzdRx8/fhyazXdvt2MGBjjT1CaxcGocb0JeNlmSFpbmmO3QmmGI+rvF6MKf+xh/r66JsOOLGFDqvRjoQrJqdCGZ8EwVUwjy5iEwsMVkV/8WU2QfF1w/1wW/Pi0+JmN0DL4wctd74U+C8Qol4Jl5QHhpb0TlsaptjqrwOiiB/MSC0NiBN0hBkwF77HY7Pfut1Wq/QN+b8KywGMOLKJzqsYCHDS5aMNtwMf01U6pBpTNip9/HOr+F85bybL8d6WpTMyrxwCi/30Yg43bPFXpoofEYmt5C5bpqoHPLB7yDffxSceAa+KrPB0PhyGyaF1eH4ICtd8RAj2RJtrKPLKEGNF1mhfP38Mfy6ExuJTqTK+U66ZHF8d35y6u1gjzT8nFIimoZqliS0n184A7IKzlJ09572XJINbUTTDfzu7YwtveL0Kep/J8xMpq7okwPCGZMAsHvhyoHPu8+2cQ+c0JsWF/NoLuL4O8d5aLihLJy8czGanj2dHua5x6HBdb3WM7jHz9qKzz66EyY3vsbSDAGv63ySOHf8O1h38lgIQPvQP5v0CVlGJ3/sZQP4MuvP+fJIsWDtivD68Prpp270NngORslX57HExffJMcMfvHW6/u7r+bFr855Ag6d28gLwx4hnHyIBALiR6J6e2wm3NNjCTSL4W/ZZQY0v9WUw82TSlm5BNgIBg3+0QlbTgq3Q4fDAdps4Q5OkJlI4I9/VcHIa3yN5bV9NXC51jdchI2ig4tcDmj7jyUsGfXdT1hr7BB41BJWLg2WmMKXQTEDEG7/hsMGX3UlrOl5tN6Ew2dy8eTk5XfpABncsHruiXrLG99Gjm/Q1fzXNqQ8b33kQtWqPyz1ZqyEJKQyEhlRXx3eR6mMckiWYv7ma+BErHifnVG674IFqDTJDP0T3hNphF6GCE2W1eYaaN6ZacB60YWOA6g7n4DEhEgoKuHDP+6Om+Crb9dQSLeIzwoA4DOnRf6zblSxpiaayHU3x3XvvKT5nYITU2NOLnU8ptooeUzZv/XjTv21gS9nGvZ8JagjIvyCFWj1NpBxG+gyKXP6LdA8IsGvDMEatieKm6G7TdxD/YzldrSQUknzP3NvHCRH+mtavmIkflQFRffV7TiE0Xvc3F964QXq/fffLczRtE7mhnu762rF1jsfpfi/PPAIdbx0e3CVKCB8TLSpBP1UYDwThSbW1i1atGgwvv/GZIqyVlVZGt1Yq86M20iDqrBqVxbvsD8DnNB76eoKuA4dtk/kXELDNPK8vDz45s7WsAZ6wu9/b4LKykqonJUBCYbg6vTU8nRIFrhF1vuDePT7dhAf767nHTt2wHuTt0DrBM/WAiH5xcK4e/kxTWx0AjzU8xdB8kX7RqOO5zzEmprAQ5etEaRpTIHjPoqHdu3a+TVUmRl7XK777r0fUrInhlzElUenwMn8g2x6PHjAxjXz5FUehTdPzWO87Lt31SE4OC248W/rT21wrqSK5jGzTxx8dcwBuSVm2t+hdSbsGVHB8p+5yQILV4Z+KWwghchm1MgcCZs9GIUiOrpFGXe4aEdh43+i9fBxwdT4ydySTC28FhYn/UEHLTtog1GtdUH3eVx+Utynyp3Q/hPPBM67b78FD5ydSyc1TGV1nxRWYdHc+Hgm/LRmnS+PELY0r/y5khr/ZGFwysI354s1JBr1keF9iF7I3FF5Nx0y7Mh5mNjb3X69SOrda3WYwaAV7uNPb7sB3o/yP4HdteyQ+TxlbxuhgxykV5y2GgovxfwkUpDeaDJou0icaHDHUSVw+FjtUQF/7ZzZ3u/FKTLSSFVXN6qzec33xnQ43a3VdK+SSPN+dKwddNNJG5f1rUSXKT3Bv0xJKJdA529/yGjriN94rahhveS3v+HGgeLGXyBdHmhL8poTwyBR7X9nwx77ZjjR8VZ4ua9ncxMzlsa/HPFtmyfhoby3hIrPC/viqA3eOxwB5yvs0KNzR/jut3UQHR0Nd/eIh0VX15npQMvwcesXYcaDD8Gcl16C++d9z5NLyPNWp3mgVfufmBBKV59h2Ba4p9p9V8Bz1vHQMWmwbNm/889QqtpREYqO64r6qn20ICL9Co4L9MsfshVERkZ10ULbILDcex8kCGpd7Dn3JYE8aJKzy5tCE7VwX3TUWQ2/dv+Ex/NE6Q5oGd+bDqMo9Hu1dx2A/3tlFYxs9TKPLlTPoYI/oFPytaEmJ+n8ILDJ+RzMeuph2P6WZ07lTPk+4G6xxsmZFVwhVm0q98P5B2KFomQJu/qbStjxp7QfkK+scoF9qHzne2UpgMxMZDBw66IPk7mULLvMiIiIE7t371Z16tSJDcQO7uosL0Imz4azdhgg4fKQvQUO6F67PS31f54t+QcnxkLrOPeuvYxl5VC4LVMmyeqPDZoMwR/e2frLUdk5IaPWZ9unHBIv2zEU1nbwLHjuq6qGw6lfweub+sGtnd4OeaI4HNmwXp9/bhnLYoG1AOZai0CDzt4+qW8C0w2hH/cwf10CzdOFxxhshhId768oh0cmSNM/3/5uhleXOGDZZ39Cjx492ByQ8d0o+sRIUBedje/ahBE8kGHH0HHfZqcNDp2dyg0Sdb9R9ZBr7awP/BoAeMw3Yo5dlAeO8Ld6W9FntdOyoZm7oxTg4s+4XWho5pydOkw0rWnP09S6+DzRuh1lSYZTU/U+ueI7FPItLrr/xkbuwJax8NuNwtn8hRaYhn/nXtn1YVQbUDYd7SrSioohlsxv+I0f2eErY2u/NGKReGd/0/JD0CyyBRjQEbyNnImqcrRLbGm3JWJJ6zT8hrLx8HPcp/D94cfhq4xCXl5fRH/C88vleX3zQMrhsnHbeMT5v5vb0oPdcl5r6C5+IQmmol+YEXp+OTUfhg8f7hOVnJxEFRYGdz7eh4kMAbK2ULx1TapMFz4ohjivn4GRmjYQ3S0fOWClsZVfMu/VWr/EKBId4kZbO/zuiAjEIuj4qppSmGKbCX0OnIVHrhRYxQiaY90mwDP2OyotkJP+dd1m5MUdd27B6vNl/45BlzKxetWLo9v7VNoR+GCI4M4oQfpgAgf+WQXbvpF2f8hVt+XCD3nuLfTB5NFYaMMxcJtcefKPknLqOqWVdeq9U1yLPloSsH/dv38/tP3fwDoV/4q18bDnSA46nxsH97SqQWer1DAE/YY39+cZxAT47ZQdFmbrYVeuFd3mboa1a9fC1WvGsuSzLqDfjkYKsD6e1IFFcKHAvar8yZxkuBv9Fnkoj7HnSbPVRoWWOJQMFZZGr9Jvevyq9f3DEWvZrjHwZlMVdI9y71YrdzghSqMGbZB21YyS/tCvxZRwRJGU9n50iZSBc4mUpERBEh1yVMNV2/wbRkGyDIp82ssVsOjLQli1ahVs+mGSbd6nFYagGNQz8QeRGdRdaGLB+3E2vR2SdMF9ngn7nnH+GpsrbLF5ZcA9h+oVRXurl17tHJ37hyivnyf0gZiXLhdKCmVTNzqs8wyCMx2r/6iCgS95Jn28Gfgz7l+98Btca/3SOwlMq74Stt531ic8lABmdRenvaplHPxxo9tGGrI1Ezbv2E2zlNOwvesnCywp6xCKqEGlmd3qScl36QTF2It4dPkEiKi9h8crStR7xlYDzfWeFeeHT5XA5Z2+DWsVGt+j8+EnT4nmKSVCg279dqFxNbLp/JL/inYlHq95Bh580L0izSVGY3L8HTi5YfXpDjj4kiLM4R+bUR1qf0M0EP3bS8tgBvqR6HCfl7ZUw6ycdkEbNZNTboLOyUN52a87/hZ8azlOKz9vo3eXfi78svYHHn2o50F5TPx4fjjyJHzZLJ+leMF2N7RPHMj6leq4pvguSEH7tpinrmanGP6H49+DVd9/xXjpt3fdLNw6DLJNWTCpOg86Xv4N+tkUj3w4wfJ/roW90cHNGC61FcGMKe4tybzMQ/AEs9VzTJ8SWKTLDCEXZSfpUH0A8ne1CFlItCInSz8WsgC1CZPio7cWlFT0FeODz9EGOwkjxiuY8BG/qmBddrHfJP5Wjj/t9DpkNG8OA3+9E9D5Wyh9IE5w5v7M0Chom+lR1H4zDCXSe9uUv62bQfCPQhdPWS6di6daohXbnCDg4ZHuOHAjvJfpa5DwiELwfGb6H6hVovZECByFk7y3eSCcia3bAbUNrf6lbXEfURGWomFC531a7nzk9WK8tNdgA06m5LGgWXsivovfLWzv6dOcr6SNCKpRBHselZHH+x29+2bXVb98yV0B45GIrd660A18Bdo9PFrGgy64o84Yugnqqu7m064znV8RzY9bro01P8ODk+9n2MryZgzbtaOj4epm/DGSLBlwmFy5shL+pDpzQuR34ntV9nT7SPQogpw5Xll0J7TgGKhy8Q5n/Lwn70f42WtsLJdcgfj0H18Cm3e5d+R+9tln1Lhx40TbdSBe4cQLfmhSGSbGq82FmzPFDxh4MTr7XjEkBXmAncti3M8W+Ki0bhWTt3GL83/+z8vonzRIaoJ+/6oz39Dlyhesu/L4zXBfirQtSG9Rj/lckBRsfnVJ/8HWIbC1s7SVyDvPNYUbO8yRTRzmduruza+FG7Ne4fE9uuMmWGwUny3lEYfgcaLZregJZ0JI6UkyLacGPnlT+m/rFvWLRgPBsD5dT+YKcYWzelvfxm2EGl7vnKS579UrjfHXr/a/heudgSaY3q0ODT6J9YeNUuZi1rfeeB0ezH2TTsn93UGDXg9l0yR356I54xnfyGn+L0URTewvAhm3Hy/5CKb0e8MfVdhxqD3hCYrtYTNSGIPZV/1DhTu5smDbMNjSKTVgyZ63ToQOSYNgbMVEwQmdX0or4Hh1DXSJMsCr5ytgSp+1AXnKTeDveIoceXVyHIC8baFP2skhgxgP1Mbra1Xlnty4rku5P70kJpNYeMvyo1RZt7clK7z+2e8634nYH9AoDrSC6+/87foD94HaJGwEil0qNbBbAbU6urVgOfyt2mLD1k7VwOUTWolB1GjCLZ/W7fcwxpIHV/QJfEZXTsC2nf0UrnN9h+qIgv+sDniyqfDkX5c9x+AZ9Lu1I8sm0LtcAskQjnHL8N5quRb+XF6/O06ZvIXej8wtqvnom4qF1TZ4RChezjBVcvaY8JdR5ZSolpcWSeUQ7AbqILMgWGrtOnDoGm7rkZioOqse7AabWHSDhcegn9WpMDXIxI3fMjcpdUFxvPLkSgU7XABhpem3QHUcmZbvgLwUPB5S1pMJNjgFvueMGlrKrPJqOBlrbGgxfPJvXYq2EcXXzVZ7n8yCCGiP7gU7CgrcRalS7UOzAt2CKEr9kKpUe5BcnoOW9ZNrwFzsh0pB1yk+IF19EySfSoWCzAv1nW3A/HpSFtitUt73iGZP/0H7Eq8MWIB6JuiVHQv/ti2v51wlZIcnm5Fxo7SnV14Z/JumvHs7Lt9bDTu7K08/pmY3gwttzymtGiENyZWnQLmugErYCsEdIagXcDXwolaFzsko8VGhK/JVauVZtyqnCpSImZOyIrkCTlbWe1Wr1KgeFdjGFCsXhdpXHZ8NC6UR4G9RkfVI8AqqOkk9BgUXdKQs3Q6rlWd8uMfSytOP6TEdoEhTEBzI9UCt2HbvQv1qkOf06gEudxYa5bUvZ00B0kPKM4rcgCkPL7VS9TbqwJQ4nsA74pQoF95xo0S51Artv/AQWpmWLeopXK5668Ivioy0iu3wLwp4660QVdXKm/2tt8KTjAgCBAGCAEGAIEAQIAgQBAgCYSCgvTrrwzCS111SDbp90alV4EqkzQVUHd3yHA6aanTVuyuM88zh5O0vraYS1WO08uox2lQKlUnK2z4XV14AZbHKOSPB1m1L1ek2FNWC9SvEgWY0c5pTlOIOI6UWnoKMpEyFoOQRIy0yB9JTFAcXZNhyIFmvPLla2I5Bor6NB0CFuOyHc7vrOqYrRBqPGAa7DaxZyjsmoLGUQvss5fX3rasPgslYt5freGpHugsdzgaVArfZtio4CNFZysOr3JoDsQbl9V9tqZ1gyBK+2Vl6a5Cfsk3efojM6io/4zA5Giss0CZLeTt1jOiYU5ss5e1Y6GTZBdrIy8JEXf7kePejYldu5S8u4UgQIAgQBAgCSkTAVdLgF7gqERZRmUy6JsrbAykqbcNHZNjyG16IRiRBO8t+xZ23bUTwEVEJAgSBBkaAGLcNXAEke4IAFwFVFBmzcvEg7ksDAdLqg6tnreVkcAkINUGAIEAQIAg0SgSaVWU3SrkbUmjtL88qbwsRBkTrcoJDrWlIbATz1qLDwA4FXgARgbZx1yhwG7fR4YBqrfJu2U2s1kORUYltP+M8unaxqWDja8hAtE1NibdB4osWmJ+4aUh4vPNu5dJBjlp57autKwKyFShXeyTXUQXK1SUuAg6UKa8eOxcZ4WCi8uTqgrbjH0hq5/05NLi/W0EE7EtWHl69EiPg3yLlyVWcpIPthcqT64okNWz9SXlyaVJ04MxXnlylSA9tVWC/2icXta9flIdXs3IHnItVnlx9EV7bflWeXPvLuykSL3SogmxLbnCtK5MAepdVJk6EDUGAIEAQIAgQBAgCBAGCAEFAfgQ0KWS8Kj+qhCMXAbItmYuGBLfWpbzfuJUgNiEhCBAECAIEAYIAQYAg0GgR2FbYc4sShXfmt9ukRLm2afpuVKJcSpWpKdn+q9SqCVou7bqFQaeplwQq0NopoHT1klkQmeDZACX+SpFS5bKjneU6Bd4Vgze8K1CsIFoiIcUI4LOaSvzxJD36XXNbpfLqSB+L5CpXnlyRLTVgOaFAuVoBWHKUJxfeoGZTnlhgQDIpcU3G1MEJ5lXKA8zUEcB8WIFyoQuJzQeVJ5dSJVKqHlKqXCb0gxBm5f0cNlRSWXuiVdBDae0M35NcrTShkDwVrkMQo+6tOMnwKTqycqu4arm4BDI4LRdXgUhpCAIEgUsGgQqzEqcyAWqg7R4lVoJN1X6nEuUiMhEECALKQcBScOUG5UjjkaRpj+OKM2yxdLoeylyKaXaZck1I5UrmaW/ERRCQHYFyCsj+ctlRJQyVjoCtvPURJcpYfaK1IteJdPkd9ikRLyITQYAgQBAgCBAECALCCGj7ZH4iHNPQoQq9nRVUKgu6nlV5v/KsUpUjudCmQ4U9KihG+0abKEwqLE4J+ktRnlxK3UikPKRoiZQLVzY02dhWcagpt19V5G3ciqs/RiCl1iMjn9LeaRfQvj4FLiqn56F977uUhhbaM6r6BzL3kN+6lVozSv0elSqXUg8UpZ4DyFTgPCuRS+qX6KZD7Z6s3AYHGaEmCBAECAIEAYIAQYAgQBAgCBAELiIEjp1Gv6lJnosCAa1Vobf/KvX3K1UqNVogVd45LKXKhVa6Fboio9QlP2XKpUyp0IVSqH0p8Xdulfo9KhcvlUqZ9ahYuZTZ7tE9Hkq84E2dk/ivi7L3UtyoLbvJDgC78m5kATTQUeJVfe4OTHHViNaJVJQCrxpVrFwqhJcCx9GZlozNVso+QGkNTO10gkuBtlpBfg0kK1AuNWpfWsuZ5UqrR1oe/CO86LZk5cmmUGNNqXgpVS6l7opRrBGJvkQFfo2oGhXaT2CwcGUq7lFqC1Nk88JVSCmz3Svze1Sr0BhMgYAhi5tyKfB7RP0Xgkt5gKFhjkuJDV+53yOy1RRYj2pQIRNSge2L9KtBjQzUZ2A9WlIbGFSieiCOPqNaj85pKk4upIcosi25HhoAyYIgQBAgCBAECAIEAYIAQYAgQBAgCBAE6hYB7UsDT1bUbRahcUebYpxoygn/HKmiHp1GZbc7KYeihELC6LRgtztAcXWp16mcNjulOLmMehVU25QnV6RepbYoUK4oo1pXVe1SXD2aotR6c5Xy5IoxqY3oZ2QUh1dctDqqrFJ5ciVEa6JLKp2KwysxVhNTVK5AueLUsUVlyqvH5DhtfEGZQ3H1mJKgTcgvUZ5caYnaJnlFypMrPVGbmKtAuZomaxPPFygPr2bJuuRzBXbFtfuMFF3K2XzlydUcyXVGgXK1SNMmn85TXvtqkaZLOZ2nvHrMTNOmnFIgXlnpunTtvFebxCjNUHPLo0I/1ULpFCgb/uFWBd6WDOi2ZFBeXapQPVKUAuVCv4mtSLygUqFylSpSLoBC9D0qsX1dQHhlKK//Up1H/WpzxcmlgjMIrxaKk0sNJ9FRuizFyQWqHFSPLRUnlwqyUT22UqBcR5FcrZUnl+ow0o9tFCjXQSSX8m57BziAsGqnPLxgH2pfSpRrrzLlUu1B7au98uqRlquDAuXajfAickmtGDXkkm3JUsEidAQBggBBgCBAECAIEAQIAgQBggBBgCCgWAQUt+1XsUgRwQgCFy8CJlS0TPTXGf0Z0B/aNQE29EceggBBgCBAECAIEAQIAgQBgkCjQECBdwc2CtyIkASBRoVAQqzqj+uujLzii7dTokIV/N4XCqtX/2HZW1LuvDJUHiQdQYAgQBAgCBAECAIEAYIAQaAuECCGbV2gSngSBBoYgfhY1frfFqUN6N0VL8DWzfPfyRq44o7z+0orqe51kwPhShAgCBAECAIEAYIAQYAgQBCQhgAxbKXhRFNFR0e7KioqaMzQ74fjM8rK+9GwIMrTWEmjIiMd5qoqehs9qgfShmsrMi5atb5oS+YAjaZhIInve/JwWSXVqbG2KyI3QYAgQBAgCBAECAIEAYJA40WAXCAVRN0xRi1OQlHoh4nIU+8IGI1GJ2PU4szPnz9/yU8u3HOLyUIdakmVbstqMKMW1wXKvyOWY/bUuBrsJw9BgCBAEPh/9s4DvqmqC+An3Uma7kUHbdlQ9t57iIKAMpQNosgGBRURF7hRQRQBkSkgICIyBP3Ye4+yV6GMlpaurKZtkvfdm5I0473kJU1Kx3m/X5v37j333HP/b91z10MCSAAJIAEkgARKisCz6dopqdI5MR/iyFo4UGlpaRAeHo4MncjZhip3chosvifs7+/PkEaHCtdIs3tpBNO9Ten7ApX+HCbeyIf6/R7QsdC4EJUeCv4iASSABJAAEkACSAAJuIRAhXMGHKGoUCgsnFqqJywsDERC7wuO6MQ09hN4/PixhVNLteTk5NDGBdZzZH8upT/FvOlBatozWpqdWkqxXg0vIHaqNn4XVmHOTem/etBCJIAEkAAScBoBoXuGn1hwnKe+uEbuQhztxxMWiiEBRwhgb6MNallZWUxAQIBVKZznaRWPUyI9PTz25hcUdLKmTCQSaXNzc8vtJ6x8vAWy3LPx9NM8ZXLzbZqUq8hlSm8Xc5mkikYjASSABJDAsyDgXsVPFvLf876pqpPwkLS7R3dO5qxTt23sLTu0Jsr3emsheIAAquVc4pR9FmWxN881a9YwQ4cOBaz/2ksO5V1NAHtsrRDWarU6p3bkkEFWpAD6v/wStsBZJVT8SGtO7VvTpsDChQtBqVS6Bfj7l8dz4U57aMuyU0uvAPnpeCEtB9n1L/4VgRqQABJAAkgACTifQLtdmxi/eS3ou8qDS7vkk6Zq6tTq46PCPegIJcZXJEjRhz397UjDqVNLj0PdPCHQzQMyAhtaHckU5+aVHeEuot+Uj36qp7T8RNKpedSppRtdzLO0GIZ2IAFKoEy3GLnyFFKntnYlPzg/yFOXjc+CTKvZSSS+jFyuwIYCq5Qci6TnwtrixyMGD4Qlof/TKV9d53MY++Y4ul8urm2RUJChOB0f5Bi50ptK1CRJkatiDJWC0mspWoYEkAASQAIVhcCLaTcY75BgOJg0QVdkbXYepDfZ0pYcHNEzCDneh3EPFeoPgfbYGm+qPC0IG98VTHhVovrxg1Bv47jMtn7GhxCcdZ46zxp94Hs+EfkzhBG6iudOv/owNKg5CM5NLDX1GerU6m3V/5L6Ga6loYeBv8+cADpilqdAkjs5kMmbGmxwaiNXmK59o3eyyNxOiI8Kh4KCApDJ5AJ6w/t5QbKlSgxxgAB9sFOkOqd2w4YN0LByIKuaVes2GsKHX3kfyPmD3S/5Wjx8DUJlZGfDd2Ha8ujUUvzKM/HiUxuiyvw5KiOXEpqJBJAAEkACNgjEDh2YS51a480twBvCb79ymPwxIPJcTH+NnVpjWf2+j7ebrvfW3KnVxxv/kp5b2uPZ+7hfLYb24uqdWipDnVq6MY1+LBXvyp49urOOiCPVNJXOUPyHBEoBgVLTClQKWFAT/IhTlGNsC7lhQfhDliHojz/+gF6H3oACDQOeVr4XGvBj1gzSaDfPkBB3eBOY21qoebupD2ujS5ZKC4FPo4x70VUq8lx9N9IkjzvZGkhYXfjdYZOIMnDwdMhuGbC0+CYKEu7gc6j4GA0atmzZwgwfNkwlk8uLuhQMsbiDBJAAEqiABDxFGg8ft3S1TB7BVno3H2/py8pHEn3c1k51tYHLO7DWQ/Qy+t8FW/bCoJ78BiCZ99jqdbD9BsWNMQT/T3oNut3+kfVd6evm/b1QIJqcrsly6RojtKPBYBDLDs63ZYGCQSVOgNdNW+JWOSFDegPSIax2qBKYO7U0ba8z1U1UUKeWbtacWhqfPTHwG/qLm30EfNxByuXUUk16p5buL+pa9CLx8aEjYUy3KgHu8E8//j23np6e+fS6adO6FWurpKl2lx2JK5JTSyk+LW+sy4hWIMVCoY+2b9++IJXJfMicc3uefxWIEhYVCSCBCkbAXTwjza1fzr3wAdoMhv7FjRysIAx66Di4u79u7NTSsLCw5rzrAb9skurUOPPfFIXp4L+ufrUgxjOQ2kw3jyB3kZz25NI/WYNvp/4luevmAyLz+b2F0k74n5KSYvN9QutPTsgKVSCBYhEoN45tz+e6a3fv3q27qa5evar7pUOG6arGfAgRp5b1IRYUVDS9UTWlaJ9L5w+R78LwPYXTJbb35e9UcemraOFZEwJ1LaZt94RCzErro1tGJ3jRFfmsIuoY4wmtKrnLrQoVRgry8/N181oOHzkq8Pby2EWCmTNnzjBhof5HeaR3hkg8cfL42OqMvEqVDlLuu8SgTqXKqLJnTIBSmWu4IYRCIfz++++sz7WyVzS0GAkAVI6prPUV++JiNXgx2EVANCNNd81sllQzzGVttnyhiDi4u3SObkHaUnOFrTYs51w4ylz28C2tQa95nKPHtz386cJRJlty3Tmip85sQUb9r8XGke4Cd9gfmMbaG20s5+A+ExHBrbp5fDDQ+nFciEQ3f4zkEe9gPs88GTrnz/wUFNuAcuHYTp48Qbvzn92C7t27626qWrVqGcDQT/UIfXyOGQJYdlLG+nM6v2uqnISB9YN0Ny1LUsugHZ/Cxks5MFH2InSp7AktIng5VZZ6KmAIaVzQnQc6xHjxyrVwZ5jJmgusREgaODPUH5Z2M3nGm8juHeAn9vUEq98bJt/INXEAVHkFPWjjY+PGjeFxWnYrojDaRKnzD6KJc3fH+WrLjkZS/r3E2jZlx+LSZSm5XovmTDw1bdCgQQJPT4//SpelaA0SsI+A0EeYTr8nfy/5nkAml9Hhli3s04DSFZaAt99WgWfhV+aEU+67ZKhuvlLDqy6tUJpUM6yekqM139Y1tFsVehpZ6+Iyg2M9UTjHwiHmo4NDJvrIkSM2fb3jt9N1ya8N8dTVlUm97I6nG7zJobPUBj948EBXB6UF9vMrl1/YKLXsnWkYr5vRmRm6QteCBezzDmhe70yfShaqiWzp6QlFkxWMjEgIdpMFkIn+xttURR+YFzzFELTajn6kyY28ISMjA5YtXwkN/hbD/oF+YuqwkZt8kkEh7pgQqBvsLjV2amnktdkdbQ731itJCHaH4XWKnGDqGG+6ka+P1v2mjwusT3ZYPzPzUjeRIuwJ9V25t6SkpPvcscWOERGnzpX6rRp44qIKFm+Qwkc/ZsKi9Tlw5Kz1nnKryooZSTgcJipii6mmwiW3VvPIzy/oWuGAYIHLA4GR2dnZ9NJmlLnKEPKdckOZSNBxcsC7R82QEHcqHAHx249eNC60mq7HwXMruGrRVsia0rtTpMGxZBV4Grhtv9JatMNxq/wOGxz2oT7T6H1Rx2FlJCGpr06gdbJbo/3v9+7RBR6+EaBzWGmv7KpVq+C7jmKoElo0FczNzQ2qrDZdZFU6MfBn0qGwtjh2lGTa+d/P10ZFRRmyzMnJ1i0IS58/YpGYf4uEQQPuPCsChmFrz8oAa/l6e3triUMhSE5Ohg4dOjB5eXl0mKp+joEuaeqBykx4iAe0HvIEjp4zWvfpSlVYulEKbww0XVrdOL8uIx7BDvKxbOPtK/9x8NGnn+mC+Aw9Nk6r3zdZ1Mho+HLHjVLFiVRN0dNAn6AC/+odWoqg56lqsO/oSR2NVo0SYF97+6eLjD2ghVXns3U69g3wg1aRpnUfb/KAtjZ8+e+9Cni+vQg86ht1npJriW6S5kmJcgVDHWTjbeOwoYP7L17yi+Cvv/6CIUOGDCWRdj3MiTOnayU0VuqK/V2HlPDpu1LY7lbDbvWDtLfg9Q9FMPC5krl8cUEp/qeIvnhtSYvFYoZ+59mWHMYjgdJAoG7dutrExESb9RPyLH+e2PtPabAZbXg2BNyr99IwBXKpNuVsFuRJJxMrtustca/eU+4zYJPJcK68XVPV/U5/ZVox0Ccw+90simRCL/ezeR2q/rkP2Z1s11f6TkpVLz9Xw2beOVo1xFfh1+H5/O2ftR+5n7B4trfM8jW2u6qfIGgBmYPbbproK7+ZiiHGcWalhi6kXvY/80C24yXxs2HK1GmGKLY6c9CirK9y1fCeQagU7mzbto3p1auXTcvI84ZeS85onehJ9OBzyyZxxwSsXdyOaXRSKs7KGnEysqUa2H9KBX27mDyvSBvVbV3uz3VrDbsWPLZqSW6uFkRN78KUJmL4so2XTva3q3kw5t8iv3lzb194oUphHJeyDdfzYFDNot5CKjfiYSfY8MdmXZLsCYHg41GEOXJJtjIrjzEznEt7+Q43dmppSenq0/o6+lry3fOXqxexb7VeCsde5W6k0JN6OPkUVK1a6IjSMOMH7XzSE/nSr+ehhpo+U2xsT6+l8FB/eHwgxCBM1yPbcUAJvTuJoefUSvDPv4cNcfqd8PAwJi0t3eJFo483/nW1U7vsDynEzAuEZh7Ou+RuaVSwd2QavDc2wLgoTt9H59Ym0kByv2TGRITAzUFaGHK7GWzevpszEXkpf0AiC1vtOKUwAgk4ToC+t1u2bMWcOHGc6/n3wgftjxkcD31Ocw+2AjInnKldqzacOXum6IWpF7Dy6+Muys3T5hZ151qRxahyR6DD8UD5/i7j7CvX83NNexe5UudnZsHxHPrYtL4xSjU8djtrXYjE+jRKYh6JG9i8vqOyLzO5Db+3KUcz3H2nFwS4FdVRbBnRod1GJv/Il3Im4wZrhcq8XmZLH1dHjnE6UrerSY5vGIeVlv0qEfWY2ykXeZuzcuVKGDNmDEMXLO1f7TtBjH99k7RfHu7IqLV5rM8/X5GvUqaQCbt27cbs2fM/VhkTZXjgEAFeN45DmouRiL4c2ZLTYMHVamxRhWFPnZGZr4fAF9NYR50WpX0qSwP0zk+9v4Rw895Dg0zm+EDy2TLriOhN3bp1G9jb7Kohna0dvx+zphVoYb4tufIc/9tzYu3LNbysw30KILdAA4GLcqB69RqQ+PwTq1h6nq0N+w4dMcjoz23gYhnk5hVOPalTuxZc2WxjGsrT6+ODscEwdwq7A5eXz4B3Q6OeXUOuAGKxSEsW8jEMDzKKMux++05wwVsj/G223hoS2LET0Pgu3BGZPnDtSM5btJryImSejeMtb4/ggVO50HFkCq9rxB695US2DamAmLSqGFcwuMro5ydRk29ue3LFYzgScJTAkydPmOBg02+Aznp/FvPDwh+gXt36UDvvNUGkxHKE5FdHO0B+AT9Hw9w2OvqGOMpwK/MY/H7pLXxWmAMq58drJMeY6h71oKU8jCGrHvM+/5rf6ql73z3N6917MGkCL4qpqsLRZtaEI1re01zxqGe1XkDTp0f0hpo+4dZUGeJO3B1p2Le1k1hwAqZObqETU3xu0qOrC3u9npfsh05i35EP2sPvm//SjW4j7xlbauH0gL9BJpNBp11DOGWJc8v7/HAqcWKEl5tQ8U7bvSIto4bZ/zYFti9r8Mludgc6K8JyC3npBMyaNYs01tWBvfv2CCQS3bqoBkHy7OpHDv4yBOCO0wiUqguNlop+aoWuSmtewtWrV8Pwpp+YB5scT/1SCvNXpwM8HTpqEml0oKl5A9zdC58tCZESODOgsJ6nrxg2SqgJx7oSPTY22QfXITQ01CBFbfxs6mtwYZjpBWwQMNopbTe5kWku3xV7QtKTcYFxtjLyX6wAMvzcRMzfVwKPX7NeL38k10LEwkdA52RlZmaC6JPCxpDsmVdAv7KfmMQpTlcy0W18QHoLC3uPbVxLU76QwoI17NcKeXDRi4PVE5eIBZekJ+MTjPN0xn5kq3twyb2eM1TZpaNaLnFwz8TZlYaPsLBxklSVx9hopeKjqfzI+HjAzKzxgZ8blyhqRR5kSItGm9QMl0DNxq1g687dkDc1GKR5DNRYkwub/toGldf1h4Xn8vJ+upDnY6yjpPZ9xSLVuPHjvL755ltssS4p6C7Mx9Pd816+Or8ynyz8fAPJKCnLEXhzDrTkk9xEZmLzzRAoLJoTRyO/PNyB9Jbk43VlQqr8HpDeWkMniD29trST5IXPTNfh4KLkTMe2e4N0+F1SlSsrQ7jx92sNgSw7fuc/YP4LeGBRX2YR1QUZM1LMi/wN8qXDnsr6PBkXkCsmHTkra82FN8eNh5Vk2tErNYtGzXHpZAtfWeszGHltlkmUljAXL8zmbatJYuce1JzV7sg1gcD0MeHIM6hV83bQVfiNiXUFZEQb6bU1CeM6qFGjJnPz5g1TQ7iEMZw3gVIFlExAX2ns1KYeiC10UolzYcuppSWe/x4ZWWHDEbn3fYbBqY0MDTI4tWPTu+mgbe8r4eXUXhyyy8SppYmHDx8O1zMLgDrIQb9YX6Agc3yA4YGsy7ji/Iu25dTue26tjqG5U0sR5chlELnc+gsp0tcN3GZGw+TxbwL9XBN9oNIt4Iui3oL6lURQvVuyLpztHxkibPNaoukWzHx6zdHrjvyFhfga1JGXJ7vHSySc7dTeTi4A+uH3Z+HU0gLfEtbX5e/shadyz8YTwCCkeeAGQNa5e8vcqaVcjJ3aj2fPgguveMLGGqd1Ti2N9/MWQOoYEbTbPghi/dxhXgeRt71DzqgeBzeP4CD/1Kdpn5fJFd5ffz1PtzCHg/ow2bMnIKhTO4E8WhmGr1M7ZvRY6Bw9xWmWmzu1VPF7bQ8ISA8u4yXwOuu0jFBRqSQQKqgkMzas67yJvBf4oT39fLeszv+zMbyLryaAUd78hwzz0WqPU9tVUV1trFM8/dFQelzF301O3gU6p7b/bo3OqaXhyXL63/5tZ1I+0TEOBF+bzjl2I8z7V/d0xvxU+416mqJ3jQ+05Plg4dTS6Pv379utN1rxgkUaT3cfCAoIBbLCskWcecCNG9d178Fr164xxNG+Yh6Px44RKFWOrUajGWEoBnESIkJ5jRQxJLG1o1qSBeGioiLfGVyYQkaGlK5at0H32ZiusdZ7A2kK7RcPoHnz5lazIwu1wKMppy1kNGSOZsLKfAhalA2RIjEv5zY8PJz3A9siwxIK8PH0sT6pudAOTw83r/uiH3JgxC7Lp2bDtQqdQ9uzp/U5sJkyuc2GA5rd154boVObFqD6uHDuNQ2rF1XY+dc5RAGJ/SVQpd1dGuy0Le1gODSqUzR0eceOHRbnWHkmziKsOAaENbkHgYNNhwAWR19x0taeHAY1mtl+oNuTB2lkeKYvQ3tsdbFs9eyJgd+a57HRbAXw96Q/m4twHs9u4WO9lYgzJf8I4vsUPMnIDr948SL1g3YYpyTDv0r9s83Y3oq+T+bB6pxZch61l69c4u8dEHCLFi+EeuHPWSCkw4gd2f6+M5sz2TvtDjSiDq6Pm889TiGMKNMEtgXcLGpFJiWZKf7RTSt7xLtMmyv30vARTpjxju1KIR9FRKaHl+3BR22lN3jZNevh3zxzLRRjpt+0qFAPreWlvDzCX6xXtP1a0QKsB5na+mC7fpM7zdHJk8VfYVubxSZp1/T0FZIROyX+Pu9T8yOGPg8aRLzA+cyKiYkxsZXPwcZL77KKBXpHQ3R0NGscW2DNmjVBq9XUpi/IFStW0Pqh4ZywyWOYdQKcJ9l6MqfHDiSV1w1U6y+bpPA6Wc3WmdvzYx7Bn82EBpW0R7VygDfcGFF47dCWJeYd7mGp+oS7SEvUmZz/gSNDFqiOaVPeBt/zA/TqDL8ck80X/vTTTxPGjx+vO0ekhbG0nCuD3fodP4l/fo40W/fwFwpFjEqVW9R6UCjkOaDOV/k1Q9rrk+h+yQR7+OJQB5Mwew5eqfstrOzymc0k7bdooH7HF+DHwF062SczLkLN6lUh443C869SM+A3LhDc3JyLeO02GQzpLYFDZ3Kh/fDCuaJikSBZfire/icoRymvk1W9Q92c9t7lyMX+4DxGC5WOWDZe2K+pMIWocZIyt4Ivumath5U+0+gIBz7PMe/5Gbq5Ux+09oMPmnmAaGHWfjKooZOj58ZaOvoNxNatW3OK7F5aCZ4bm1qDCNzkFMKI0kDAndS5THp8HDEqQBIMkxqbtG3o1FxO+w923f0SlLlFw+n1+qMqVYasrEzYtmMrdOnSRR+s++Wa32YiRA6+OtxJW6BVuZuH43GpJ0AXHQojf6H6X2/wIePP3doeCExLMLeeXKPQtbDKZB7Fesx3ESk+w5H5zLGlo6psbd94ham/iHzRwgk1T7fqVkOo5dHQPJj1uJWgoVb05nmTepnm+wjIet2y7rDtdj6QBU5NPqHIqpQjUD+lTx+tX+ckamUBZOQUdrKT6iztGTqll3HRb9M3m64/FSKKs6l+w81JcOORqTkNEpqAivj51x+cYU3fLLYfPBfH7tg66iMYZ0S+ZKAhHWQ2rwPjNLhfSMC5NXn7qE5rkuD14emN0UXdW/altykd1/0JXHvZOe+yWitjYFjDJbo8H0ovw/Jzr9nMn02A60X8yb7mxLEyee6YJFer1eDpaWMlK5MUJXMg8hGlKXIV9KXDuUVHxsKo6rp2CxOZjQ9fg+u3LpuE8TkYPfJ1iEoq4r/7Wkc4+ErxF8WsusEDtv3oBY2NvonLxx6+MqcSVdB5dAo82BsL/hLuc81XH5+XJF9drpILOix1muqKvEqyYlIAQ4dyGW/dT1aDf5vfMg7iva9fgXxmp2j4qL4Sghdl5ZKFPe26iQICfB8wjCA8J0c2gWS8lCXzlr06hR87mSiHx+lFjRwL3w+Bx4IxMLjFWqhTrWgO18zvMzWJN/IV1+/lazOytfeycrT8am0sGWOQ8wj4e4drBybMEyw9oxu5WGzFbO9A6pDQlZHNtzo168HLEb+YBG/NGA8XLxWONGbTZSJsdrDoUFtFJoOf3DPDUioPJW4+D6PdaoWvkBR9o5WPoW0Ceqt9Xt3GyyEQHu2p6bT3L5uVxJJ0bPnOr7Vn0SjjubWUIaPMgAwXPl5/q/c1XTlYd7pCA3zh/ggvuJmlgXqri3qEXdhZ89rUljuW+XoF8blcdDKf7m+ha+zVarUwueUWi7n7bI7q4EFDoWrqRIs8frrQGzKz0y3C7Q04d+4c/DFFCZ8fakt73fRTeexVUyHli1+7thNb9zY+UvqJE/L3nbOdWo1GC34tUiEzgrS8kFVt7z4ouonsNNMgHrxIBZ+d32lwamlElF8CRIdVM8jw3dm8ebOFaKriuq4H2JpTSxN5eHjQxYyYIYOHloqhe8TeMwUFBYwtp5ba/uDRPZixrWh+Kw2j28CoXwt37Pxv7NTSpD1q7YdPzv4Fd3J4jeDhzC3SRw2N+z/QXTvvrXoZ2gxx7rOkWT0fkJ2KNzi1tCfXkY1WAsuCU0vLRu2kq0c7Y5v/XnCxe4ycYUdJ66DDxcydWrpw3cFjJx02JSKg0If9Yh+53smWMT5Q+LRHuJ0tpRKx8C59FmVlyaKys6UeZHcJPY6qFMx4eYLKywPmBvi5Kcgz/ti2H8Xkc1nhcOxY0XDTSUP9Ye6QTSZOLc3zi2lB7tt/jvC7ubNyQObRuAb0PXHkt0jnXDy2CoXxFgTcBR5/0+F7k1r8JQj3rQb5+Y6NWifzx4A6oAMTvoLP5n4OGq3llMV6E5It8lcoFBZOLRXqE7xIp69pbG+LNLYC2nmHiDMCGzKDvIIsu4ZtJcb4EiVwoub0yCv1xriryTdd7dmOZPNzaqlOZePfbTq1VE721Xn6Y3Wj61yU1OZzbgbv52LHPpZT4lzp1FIGQxPfgfuvF/ZZpWcXNmpWD3SHQ4OKeqwb1q7Guwx8uPqC2+/03iZ/djm1VDetf89qfwQ+6nTSwqml8bUqWTa6rdvwG42y2NIzUiEiKFb3jBrX7HeLeL4Bv01IAzeBO131PaVuSOdSUe/na/uzlitRx/bjCYHy3UvJMsRO2o5fUEFYR9IjRD/NQv7c6yWBVKbQLRhU3Cwari50aN9qvZdVVXr2Q9Zwa4Fsc0cjxDV1N0BYcIS1pIa439au0U02pxVJMoehpC/2FlOnTNXNsSLzoRtTZ5vP9l7b/fBN7ysWonSZdWdtZO4urE/aDaP2fgAq8i0lR7Z/ehY9Z7/86ms4co7UfZ5eW3tSlkKljhmOqOVM066JEC5cy+OM54rIamd7ng5X2mcRrujsHHunDPPnVQl5FmV0VZ4SkfDJL93FRfMoSEZ/tvjRsHBd6y1F16w9NtxPL2r0+0z8hiEpcW4PijzgU0OA2Y6Pj+deqVwZaxasO7zylwTyzlfxzrtQZVbWsTiT3t+X+z2vk3n8+DFbUs6w1o18gDq400f7c67GF+zvdva9MQHqHT9HMPHRHtmcyjCCN4EaQa21M9sdMvEc53Qr7CXVK/n111/B20sIV64UPtvJegIwrdUOeLv1LqhepbZeDGrVqqXbrxnSAVT/dQZ3lqkTgwYNMsjTHfJ6gy+fTzQJMz/oGTfLPMjmcXS9RTqZReLKIloJfsUrSGkzkYsFyOr9znsRutjWklRf26ewTtSmCrsDYc0W1bwoXpUAgQ+/d5PHMbDZav7lsuI/epar+PX0HQhMNx2+wwFjg2oRuEc2NYm9lxVtcuyqg1Cyno1+GHLVNYWNYs0iPOD7777TZflhtVR42pjKqyxcdkpAsIney/cC6+seInsL7B8lRpxHnRPJlceAGt+Dv6/lOiarro60SPJx51Pwer3CEYod3i6KTk9PhxdqzNTV96tFNjRE1K3VEFo2bWs4pjvu7mSKkGfRtdm3zme6RfFIlNhEEA9YCRTrgmLVyBFIPm+iJCvBmlTQOERtBjce4gtnz12wKdezhh9s6cnP+dIrq7osD0Y33aM/5PxlG5rAKWwUwTZ06n/3vodjdwtvBCNRu3b9/f0ZqVRKC8vrgW6H8rMLFixoNHnyZDuSWIqylfvUwz9g1615lsI2QmJCa8LIOqusSj2SXYXp9adCvJ2+UC5ZaCowMNCqbhpZLVYCt/4JsynHR4BW4vhOoS4rPbVs5XbGsORt+xTw4sTHJfbcYitHCYY1JdcGGX4CsLWPBHrEFc6HMp7DdOjQIWj2Rx+HTKq1yR16RBbAgjaW7ZtkqDJtbi/yfp/mQOxh9aTp9UscUE477j4sgDiyCnlQgBgyj/JrxONSdpXMAatVxdPqPUOGrdO5eTe4dGA4N4GZbQ8x7m72vTdvZByCGsGWnf3d5xZAu3btoN+LA6BujlEtzyh7DemRI8PtDCHHjx+HHexT1wwyxdmZljjOInl01sXcXNCaNMZYCLkgwPh+MhqaSZ9vrPeZC0wotSqZRj8aGByR3waPJ3PsstV8+C1XYuX8OKa//LrNd4qt4ciajlsK0ndFWU5aNcrY1vu7tuyW9nG9Ly0fyEY6duVchsCsb4xCuHfNGZxLiYIYMg2q8koVpOUoYdgr/eGX8L3cCpwUQ99Zf5N3WHejd9iwhoHwS4dC7KGLs/6V50MPO7OrRRzaq+Zp3ot/G+gIE1dsf92eBYkPivyD7du3w4lvQjiz0vsJnaq+Dm2jXzORI+vvkXeY1VNtIq8/+OZwpyd5WpXV6X962Yr6a/NmdgYYX5HgIhmCWa+4unybp4Jczj2CKCzID6r+CCBjAABAAElEQVQGeMKFh1L4p48QWlTi/3KW/KignwrgZeJj+S0wnm/UuEFzeN5/vu4i3Xb7Ezj/4B8LPf6SIJjceKdFOA3QX/yskXYETmu1E/TzClLl1+F48momM/cBWUZcAIE+UYIIvwRBsLAySLzDgC7clK1KgXTFLXgsu8bk5KUxIk8/qBbcwa151CDSql7ILnbYJcNcCTtMsRBt3qQV9PD93iKcBnCVX+8MH76/Ajaf/sKkJ14fx6rQKJAOe8vI6QoLOvFv6Or/nwBOPVJBbKA3DJ32MVhz6qUn48DPt2Q6Ek+08oLqZCn5srrJGA3EHuG+f/mWq6LMtTWu9F4b6Q9xpJEmfi0DKU+ydKjIVADQTA/ni81uOfNvbefm5jL6j9jTb3ZXdZsJbRqX3uuxolwndp9Y7gQTSM/Fj9zRjsVsv/MJzPhqGFxdwt7wcejeCth/d4lOed3ojtCv6peOZcQz1ZF7y+AP6RlW6eCs8wkk4gprpJMD69evp71w4aKhDrZ37x6mc+cuumOVSgVkBWpDnJOzLvXq/N18krMbzIsxNjTg4gxmtx+/nkqa7k/VMvhp2hhjFZz7fBaRsuXYPq65gWEuxFs9Z7Yc24DY0aTn0LqzM+eGCLp7DeQsiz6itSJSK5z+yKDsfnY0CD0EELcqD1Kzi97De/bsgTZ/D9Anc9nv7rsFhsbZu2TaWO1VUtpja8gvU6WFqKU5cSTgniGQY2eMd0juV6Joi5ePnNQxfqm/lCOV84Jf/dkP6tSpo1PIVReldc/oVy9ByoZGzsv4qaZ1Fycyd7LPGM6t0zMo4wqt3oTOKJu7OwxVX6yypri63l8zAD7/gv2Fd+bMGUhY3c2hLDouVcH5vAKY0e6QRXq6SBSdT2u+bXxAFj26fRka1G0CLwb/ZB4N/yreghOnj1qEs90AR++vhj13FulkI8Nj4NHj+xbp+Aaw6eeblkuuOCsXjxz2GnTs0g5GjhypU89m3/zTz4FMYTmEhw5fpt8DM94yc+/Dp/90huDgYN1wDuM4/f65lK3QqJJlDxZtpX/N7wqs78PfwdXrPPpIDZ3Jat1c2+AX/GDdN6Fc0U4JDyWf9LkuLHbbkFNsKY6S1nlX4dqpqOKogJenpsr//E8pKZaSUp6YzGdkyGJxOiu/6yCC8Q0L7wV9b+3GXr7wYtWihZdcUZxliXmaSfsKV2WsWzdBm5hY+HmX0JBASD/If2GO4timrnFdt76Are+Ts+XRbNAD+elL+eX6OmErt6NhdD6to2mLk25v8kKQqVOhT5XPiqPGrrQvnR8Nse7erGm6S2/Iz2hc/nyJGDd2dMqixb+y2kADAwL85GRhtgp5/SbWep+pK4y0YNM8caNmoWSnu0UER0DHXsfAPcZyfqS5eKRiOjT83vr1t//Gm+DmyZ11Zp/d6vwNgZy9KUfPqaDWpDDzrE2O+SwcxXfRKH1vLZMvh4z82rp8MnKp81hU3xrx6gBYtX4TJI0JgEriZ+8nNdgqhOt3HwKZZseQdyB9AWpMAAE0JL2058zCDIff1V1kdSSPQdAJOwUaFeQ13Qa+51zfKMBm7leHO5KV3vO4L0i2RNxhNUlD+jUaTTrC6DVszp07ZSmMcfWVHOAMp/aGxz+cTm2wn8ghp3bCL26gXBMLO4U1LZxaLWn1ob2I3+17mfWUDYz+VedYsTm1NEF38XeQnJxskfaHsz0twvYm/azTRZ2+12ptsojnGzDy6YrNfOX5ynm4ecOmTY7Z9dobI+H2ilq68o1pvJI1Szantk5kOwunliYOEsbA/Jducjq1VGb7jS9g0qYqdNdke1X5CH6V1tGd82arZCZxtg5aR3ro5oo8efKEVXTdDikEtUljjbMnkA7XZNsysjXlwqmlZTvqXRvOXLZ/XrExl83zI3yNj8vbPp13JBYJdcV65aU+Bqc2ZnXh9SGfFOhyp5ZmPqaet7uvJygiQtwVXRo9Fsz+4AMY/2poiTm1fSZJdU4tmT/r0Ck+tSG6XF8nDkHhSPR89XecPYWFIyfL4M6VJ5WoU0st+LPhcktDnob861fDNyWgvjOdfDosfra/n9v6AD9B0twpgRoybD9l0eT90LhhLfIpoyx4840RJvZ8OHsWzJw5y9b128QkUfk4eE0o8FSxObW0eCfrDbSrIr9/u22nlup9oBhFf6xumU13Wr0mvPvEcTq1VPHWvUqr6a1m/jTS7/wsXvdpm9BXdPO2Bb/UNzi1VIXeqX191AhdnWZJ2B7dLx+n9kK6GraSaSCu3Or65urUk0/X0bVk1HTUEpliR0cKaYUgyLXm1NZRppaYU0uNpB0vz8qppfm/23a/0/w3vVNL9ZKRWfTaoY4uc//+/WJfs1RnSW8u7bGli344WqD2w1OhcZvXYP6CHzhVPH4zEPy97SvC4F+0sMwn3kTn9/V+Njlu/V4W0IWe2HoYTQRtHLy5Lgaiokx7p6zpTK68FFatKXzhBviR7/012mHI4dfrA+BR6n3dym10pbQLqTsgK+gwvDZ2BGz8/Ao0qfSSQdYVO79c6Q8zP5gBM9+dBZObbtfd1KuujILk9KswqcUWCPCppMuWNgpM+aM6hIUVtky+T3rC2RYL0dtoPgw5JoTMn01YpY+2+3dxYh9Iz3wMIxsuhRj/+ob0dB7X9MuTDMd0ZzTpwf3dzh7cGDI3JZ3MTeHavL294IeZfjB2oD+XiNXwOT9nwWzyTV3jzdbwJWPZsrJf3Pm2fs3v/iZTaIeVlfLysZPU2EbKJweuoLIRy1WQLVMaFt/YRYZx9d0qMxzz0eeoTOwaNRw+cQb8vm2ue77qPg1kZf6so/nYTEcXbnu6/besEnRrbf8UyL6TUxVb9yhtOQj6bCrkb5BPlHZ88z/se5FaIRX9sD88iPrDikTpiFJr82HG5SlWjamcdXGFArSjrQpxRI7o66tc+VlYYQsVh4xJ8NPrvUfXtrD7hxRdFKlbglvdJOrA0pW73nr//ZnzPvvsc5NzlZaWBuHh4SZhJnpL6YGnwD3/l5hXPUcEt7TbQr69llRxK20drWjCFZtOgK3hyJmnzsKlkF85bVUnyeBJpauc8bFd7mnP5dXjtKNGznXNkwbfWHXc+ZSbfrKm2wQ3+PNaBLSPpp2eAIsuFsBb+4oa9PWLOnEayxLRYX8EnLhwxeXvIP2oJGMTzvnX4RxhQeVay5NgQKtdxklK5f4PR7uAVK2kKx07zT7ymbTi3vvUh+W0Z8WKFczo0aM5r1vOhM8worhAOE2316lt2TMZ9vctGnVjPA8gUCKG6Q0YmHW40KEICZDAgxGFNyynAWYRsYtz4Kq4yNHRR3cQCKFv3e/0h7Dm4ni4m0XfIYWbnzgQpjT9R39o9++GB6Pgxu2ih501x5Y6efSTCuarT9qdaSlIsOH6ZLiRehLatOgInX3Yh5BvvTsTLt7bZ7B2WIOfIC7AsUboFNk1qDdCClOmTDboM2c9/uJY8GaZv5LZNwmi7fyu7O03DkFCQgJ8PPsDqHTse6g282/o0qWLLm/zl8aoq/mwfn64wS5bO1W6J8OdfyvrxOp0ewCHcwvncthKV5bia6gS4cnpWIdNTsvQQHj7ey57fjlsmIMJF3cVaUbU8Ta8PO6NPwbX32lqWGyj+WYtnHzZEO1gLraTzQuZCh/M/tAgmE4aWSReAohdkQOPj8cZwl25I1dowbdZkkUW7ds0gIO/yC3CbQWQuba0ssirp8OWrvIWH+ffSDu0wSKn3Uc7r82GNVH3dZjWS1aWelxPlHfhs9tfWbUzjyzyEpl9cUygn6BvlpR5nvzuJL+9uRKFh7jLUw/E2j/n5alj+/27wTBtBPsoBf8WDyFHpmLN2mjxKdb40hZIV7Idz7hrfo8fZdWZY7P7X+lV8M+0ft6M0+mH5RqHme8rvg5mBuQ/snov2Jpnm6o6aa7WcOzXLEl717sB50P8WGBLeMG/rkHefCclPweSH1lviKFp2rdZCzlV3jMk13+zvGObFrCr6U34Pnw6THs8zxDPtfPqHoB/b8khProS9OjdF779foFO1Lx+w5Xe0fA9yWp4YUvh1K8OgRGwRVDYacKlLyTrPOnwYXcUlQU5JqsLc+koifDTib3g27jChaac/Wx01LkV+giUyjPxwqgu2fAwJYMTA3m20OuW2/vlTPlsIqzexI6apDwTxwh9OO9fE7UvjU+FdQ28TcKcfZCzujJ4kgWU2Dbj3toFZ3uQzwXl6MROnjwJzZs31+13qTIBWsc43jlEW4ZfWxEJ1apVs9oLTD9Q78yWHLbylnTY0ZQV0LrSKNZsvz3ZFZS5coiJioeR1dazyvAN1H9gm8ofPXoUWrdurUtq3GNM5+jOufU5q8pOyqtwYqz9PUKsylgC1VoGfMfxn5dYtUcy3N5ducx8r5alyDaDittrW9YXB/L0cD92/cbNlu/3bQIrOtrE5XKBGYfzYOEZhUU++orMN2dUMHup9UqGRWIHAsh5BaGPJyjPxOhSj/iiCRnJshHIJ4Qg0M5VzvXZl/VrRV8OZ/7WD+upfbHWh+wvxmJk9KpspCH1Eq+vwY8sVvisN7rGQuWcqzCNrN1gvuWSz4O8f+1d82Crx3GKiyA9FwdXbuWTVdpTczKytJ6bF4SLOrfk30FrngHv+eRGIxmMddDpT7GxsU4/n8Z5OHOfOrZUX0P5PW1y3Tn8KoxGBkguvM38zz+DV3l7KOuo1W9fsTpUmKq21WtbHMe2fpNH6v3CWpw22JpfO/pqsmas8EOrjQBTvYOZfQNFJkxoD+jgAS/B8sj9cGXE/2DSgG6wpxe3j8J8+ZAuXGZE2nRX/z4wDS3+0ZoreVDpeCS09uQ/wCY+5zpMbrvXIvOr6ftgfsrvMDmiPySEdbOIL+mAyAf9oYN/YblWPM4En2p/O90Ea86tt5dAoToXz6uCy9S+ZRjSnXE0DoIDCi8583doQIAkfePGzSHduhXynTnzXebLL7+2+z52Ogii0OQGcEYGOxdHMD3b8eIHtAJjvCqaM/I31tFtgxy25icYB5nsf1LjE5OXLu31CxXHA51XSrcz4rngDp7QUGHfS88kEzzgJHAn6yRUCSxsPOAU4hlxK/MYNB2phaQNpPIrLBz+zab/5sl+sEgYwalVNOweZ5wzItxGBYAX6QHjs4W1vAfXPOrxES2TMlWUFyH7bJzDtvs0vjMvLw9mOKyghBN6uLsf6vti7zab/txicgEcGOhn1wru9ppNV5sM4tnQqCxgSAt3oXn6IWEHDx6E5pv7GrL1GWs6VN4QUYp31u2Ua4fMSLNaKSzF5jvdNIlXsHZKy+0m16GzMjF2bKnOqWRV+hbRQ6Fx2itwNux3Z2Vjlx7jT/yQD7FDC8U9UHv4AfkaANzxr2WXLmPhVvlX4fpJ0+lGxvH27meR1WL5NN4olFp4f4Ecdh/3hms3HxiyEYtFjFKZWyoqlwajOHb0ji2Njsm5wigafGf39djx0m7tV77reZWXT69t/cZ/QPRLnJ3xsLVpHU3gpk6czxFrPbazmyhhmpW6hy3H1tow5AImH5oOr8pKuukfWjjd3w0iyfSWTKPpLWzCeZ8mAfl0JFuULkytVoP6bec2VB0ia4s02VuNM09bEbNzU2FtgRwUGiUsEEXDK95FHQjGHVe29Dgzfuf1T+AKca6nk4a0nlnDIMCj6JJpfyUNxrXY6czsDLrMnVuRtyBHcTbezyDAY2fABzVg05//FEpeYb+mdJEcDWylYeSI3Q8Sa1w6t/CR71keyWsITkSbu3B3OPcNZC0fPnEfL/OCd7y5exfqqZ7A6GaOLYrEJ3+UKb0ERl54HQKtfKdxbOBVWNOLX+OMI6W0xzG42NoHot1cu/qtI2VwVpri9Nqu2y6HIe+mOfUZ5qxyPdXTftiQwftX/7bWqo2uagHXl4U6qK80CIaVHblb6fWy5r965/bKlStQZUlbQ3TLHXI4/3dhj6ohsJTvLN0k1Yz9+Alnj0kpN9+Z5gnIyCCXDM2mo44uN6pp1dYnBWqYmhYPL9SaY1XOmZHGjq0z9ep1+e3PIQudWb3N9aKu+TWqZJKKZU+SSamfcGjs2FIoVXOuarMafMvLSTWGaM3hM5a7oj4DkyZZn+qkvr4NXtzU3TiZyf65ie+oZW8rOJ8h1hxba2tlDJPfgR112UeTUQMqXfxE/ZdfEmu+z8sqwcPxrFEmtts6sLV+CE1fPVwCia/YNw3QWr4tlihhn6i2NZFixU2JGePw9DZHMr57rTfMjAq2mdTZQ5H1GW689BZzI/OYm4+XQJV7Lr6wh04fyfN3wlwp/LQuHU6cOAEtJIM5U0mjz4GfH7vP/KydW6c9iYXeAoXyLL+u7o275PDivcIVPjmpORixMykfOh6ubjV1LekNGNtmj1UZ88h80hrk5e46Z8c8P3p88+qL8GF0ELjqJmDL09GwPbe/g2VhF+HjvFFQM6SDo2ocSpehvAfBoli70tqq6PyRnwXDXyuc52GXYp7C9ji31l6IPLMrtWLLRyXD9NfY55PxMdp8eAyfNK6W8fH0PJSbn1/kBVrJcEk3CYyo47yKAltW1Dmli0MMqB8MbYNV0CbKA+qF8KsI0bR0nm0oWdSMbjdGB0Dlp/PRaUt7t0+d23rPZj8NU6sZ8Kh/RxedUDsets3XQJUY+7lpNFRPktPee1z2lubwmW0PMtYW9LNlO30X3rw2COZWLqrAPcovgEgv+85HCkmzP3itreyKHX/y/jpYn32o2HpsKVg5+j68Ndp1jfW28l9ycAaMffNNaJwQCOeuZNNWpwe20jzLeHPHltrSXnpTfan+V/weTkbG83Vu27darvVsaH0hnOIMR3bUsR2uURdsr/om5w3EVb7QflEQ6Wt3W4ARucLdqBUqyJAqdQdyuRw8ZlXW7d+TaqAmWV+BbrWrxcG5F5xXJ0pZFQX+VjoYdJkW899FslDTnkariqnFdvKNF8fBlvhc24JGEiqyyBdZ9hl8yTdR6XZYqoAFWZXgpbo/GEnZvxvQ/AV4m4wMLM522/tfiJF1szm6MLRdGhw7cU43zdI4v88/n8vMmjW7+BemsVI79p3ygvcVC/6TnYzvyjdf1ZLCShJfeVty9MPPQ3co4J5fA1ui0FB2C0a0/o9V7tvDHeDttgdM4n483wuycp7owrZs2QIvvfRSicyDjX80AFqSRbPoVhYc23mE3ZkG8Tp7X7kfDn3qfKXbd+W/JZf7QdqTFEMWs9ofIR83L3xI6AMfy29BuG81/aHJry3nlgrvK5DBYsl9WPScEGLsXGDKJDOzg0038mHYt+FmodyH5dW5PU9ePJ2Pq7kLbiOmtDm2Cxf+oJ04cRLrc/Xx48fg/7nrWqe5UOl7XdniScuq1ekgn0nehJ9/mA8PRvmAXo95D/PWyp4wqCf/eVFsdtgMM+qRMshaGyZlEGLfqdP7vvzqnQIJe2z5DfVxF+dPb/M/zgq0rZJ/fbg9nGtQxZYY73iFRgt/B6zmLe+o4AgySifIxZVoaltTz0twZ1+hU+Corc5KV/25ZMWt+2oX35gOW+tDHFtWT2AO+TTf93U+tEvxluzzZIGv+bzS2BqSrJgXzgxQJbM+w2kG1ubZOurYWhuGTBslT94bZVK2H/NmwC9jnDOsv/YfnpD08LFOf0pKCgR+yT19z8SIYhzQT22WxDZA0gRax41xaVYrzw6G3dWdO6quuHX+OT8PcWmZOZUbvaf9/MRpMpmSfyWXU6n9Ec7wqLvY49TKf86030qWFKOXMbpvktIbpN2haryc2rdzU1id2m3HewF1ctp4Ws4d0zu11IR+/frBoUOuafmln6PRb72zhxmcWn1Yaf+dbtQg8HvMY1h4rIfLTTZ2amlmbWdatiZ+mfSt7tz+eXGihT0fVbf98uzkKYENqjoQ/Fe84XprvlpmocvegAE1vKBq13u8kwUcLGw15Z2gjAg29Cj2KIi3SktRyfcoGS6ntlII+TTZM3BqbbGhlabolfmcYlMyfoYnpCU/haxUTB3aZo0tGw/7JBfA833uc+pwRkStqqY9YbFRxfNJr2yL8aUr9+9dXomOz37TGTaWBR1jmqxx2Kml5XOmU0v1id3dwCOpH9116baqwS8wq+q7cFPDvqKwszI/XVAXJnxY2BDuLJ2O6rm5q7KYXuP6P7FIwP+F42im/NJJWnv4FnpSLPKzRZFkNd4FLDHcQf0CGkLnnCB6L9vcFN9EWJUTT3/M6dRS5bm7XPusMy9Ah8s7iyqHJHJr1QinObU0r7uP0nRZjh72arlxamvKk4HOr3W1U5uZ+8DpTi09GQuOdtKdkzL3jzQ2K5WFPf9SqYIO52rzLMpg9QbmYZA3eWjyflPU7XwPTg9iH5PNIy+dSJ1fpXDayzUL6tSX3YFRrXebmLItaTacTy7q4aWrFtNeDmdu8492hhP1KsN0WW+YJ9lmobq4rTcWCl0UYL5oyLS7TyDVsymsj7oL38F7UEni+CIdbCYvvtQX0jNSDVHmn/ehEXx6ZQ0K7Nxxe/Uu+BRzXpU7Gdrp+XSxHlvZn7uaB7Gvh9oSK3PxR+akQO9OvKbmW5Qtrmuy9F6K2tTrsZByecAs4iDO5cplax8J9Igrlj/BpZpX+J7ua+CFF16wKuvIN8HZFDIj/MmKxs5oL2XR/rQ1ODxUAo8POH8ItG/TpMeKXIZ7ZTkWk8pSkLebj2xG230O9+Ap8jNhTJ5r2pFK8h138v5aMjT5sEtPXf6WTIgI9XBpHo4o//M/Bbw81brj5oheHmmCunv43V0vqcK7RYoO1Yys8gYP1UUi/S6f0b4nXmjzAWSr17bD67dAHMu+hsAW/1gm+Hwv1krgO0v+hbc4PtXENerqhloFLatZNrzrS6UfhvxIcw96jmyrDy6Tv8lk+HGIC0dOrMjLguymzunJ5gN49cnn4J/azn9lXFKqIDHc8XJotAXw+ZKRfIrgchlJs6T7ciVTosNYbD4ArJXaHqf2xEVVsZ3a0yvDXObU0nKuF1ouNlUvsKg1+YP2R53m1CY+3gXUEaR/1KmlG5tTS8N/O8m9mAGNf9absiBbVw5zO74n3+yiTi3d3oIvdTI1Ugbqjp3x7826f0GIf6QzVDmkQ7s+DiqT7yMXZ9Msz+advFFtb3i33W3e8mVF8ItZjveA71oaUbyWMvshBYk8IPO9Zj6ats0bE39Wt7E6tQt/WKDr5XyWTi0tXpd/h5mUcmEXX51dr9QLMISHL84iK8Ra7cgwyFrbEazKgeAWd62JOBT3+rctdel2LankEqeWKpefjg9f+XmoxiEDS3ki4tRKi+PU0uKJvYJ4lfIaqZRRRzXh3HVWeboq8dcPM2BHphTWpGXBF5pJrHKuCmweMwQ2kzUUXLl59ePHypU2sOl+qZsYXn1BXNilwibg5LBm7qI8OpeW/GXY49RSM3zc3CDz7jK7LNqS0IRXnTZuXjer9/l/o7dyxr+YdoPVqaWGrtqpVNtlMBHuIL/J+eBtcvUbnR19lGFl3qmNXSR1qVNLuZekU0vzG958F/S6lgr7cuTw3aMncJD8sm1ZZDVp+kwcllKdLdoi7POsKhZh9gTQNRSmj/nZniQuk5Wdio+ho0aubY9mPD2ADg9zuHGVr5GcN6gtBdRQWzLG8cWdV1tSY/Lp8NQAnyIHd86BlpCYmAgbJ1p+39G4fPbsLyA9tMefOrP2pGOT7XJNBmOabWaLcnqY9FYfGBse6BS9zmyhf6K8Cz+fegX8fYNhcpMdBvtunuoPi3xc38M5IPcW7HjD8R65qsQZeHg0zmC3rZ2Qpvfgho9rRi3YyttV8cVZHbkk5tl6usFE6cTAheblD12mgo8/nQMjbs0FX/IZp1Opahh3JhB2dsqBMBGvOpa5Spcdp5NPhISa2aQiizIF/FRUyf+b9C53d0LvMs0rZlqw08qy4vj7MKzpZyWy8uzDx2qI7sw9x85phSohRTGSutoRjX5x+F1vbub1Ky/CxzFFjttl4shW9fHSOSJUdp3vctIAXHjtn73UG76JLboOitsTYW6Lo8ftzg2Hph6OjRLhm+fXBanw5YliT7Xgm51dcmR0Qi4ZneBy42g9MajRPc0tcT3TxS/sshbA2hxUNlX6Xk62OH1Y59cUIPDivgasLSLFNc/2ScPNWvXxGNYHP1ePbX7UQIjwZG+fPZT0KjQcUhU83Z12++qLX6K/CvIZOcHvcS7P81l93se4YA+kl2CGYJ5xkMk6OcYjG8/KldDYt+g2zFZrYGZ2a2gfP84kfXEOZn77KojM3vvF0eeMtPtO5ELn0Skuu6gFYTcG2uWgOqNQqAMJIAEkgASQABJAAkgACSABJIAEkICzCHgIyOINuCEBJIAEkAASQAJIAAkgASSABJAAEiirBNCrLatnDu1GAkgACSABJIAEkAASQAJIAAkgAR0BdGzxQkACSAAJIAEkgASQABJAAkgACSCBMk0AHdsyffrQeCSABJAAEkACSAAJIAEkgASQABJAxxavASSABJAAEkACSAAJIAEkgASQABIo0wQEA7QZuCpymT6FaDwSQAJIAAkgASSABJAAEkACSKBiE8Ae24p9/rH0SAAJIAEkgASQABJAAkgACSCBMk8AHdsyfwqxAEgACSABJIAEkAASQAJIAAkggYpNAB3bin3+sfRIAAkgASSABJAAEkACSAAJIIEyTwAd2zJ/CrEASAAJIAEkgASQABJAAkgACSCBik3AI3XXnopNAEuPBJAAEkACSAAJIAEkgASQABJAAmWagED8vhxXRS7TpxCNRwJIAAkgASSABJAAEkACSAAJVGwCOBS5Yp9/LD0SQAJIAAkgASSABJAAEkACSKDME0DHtsyfQiwAEkACSAAJIAEkgASQABJAAkigYhNAx7Zin38sPRJAAkgACSABJIAEkAASQAJIoMwTQMe2zJ9CLAASQAJIAAkgASSABJAAEkACSKBiExAs8N2Ki0dV7GsAS48EkAASQAJIAAkgASSABJAAEijTBATHA3FV5DJ9BtF4JIAEkAASQAJIAAkgASSABJBABSeAQ5Er+AWAxUcCSAAJIAEkgASQABJAAkgACZR1AujYlvUziPYjASSABJAAEkACSAAJIAEkgAQqOAF0bCv4BYDFRwJIAAkgASSABJAAEkACSAAJlHUC6NiW9TOI9iMBJIAEkAASQAJIAAkgASSABCo4AQHT6EdcFbmCXwRYfCSABJAAEkACSAAJIAEkgASQQFkmgD22Zfnsoe1IAAkgASSABJAAEkACSAAJIAEkAOjY4kWABJAAEkACSAAJIAEkgASQABJAAmWaADq2Zfr0ofFIAAkgASSABJAAEkACSAAJIAEkgI4tXgNIAAkgASSABJAAEkACSAAJIAEkUKYJeKi0BWW6AGg8EkACSAAJIAEkgASQABJAAkgACVRsAoKMwIa4KnLFvgaw9EgACSABJIAEkAASQAJIAAkggTJNAIcil+nTh8YjASSABJAAEkACSAAJIAEkgASQADq2eA0gASSABJAAEkACSAAJIAEkgASQQJkmgI5tmT59aDwSQAJIAAkgASSABJAAEkACSAAJoGOL1wASQAJIAAkgASSABJAAEkACSAAJlGkCgsn9/HPKdAnQeCSABJAAEkACSAAJIAEkgASQABKo0AQEzOUquCpyhb4EsPBIAAkgASSABJAAEkACSAAJIIGyTQCHIpft84fWIwEkgASQABJAAkgACSABJIAEKjwBdGwr/CWAAJAAEkACSAAJIAEkgASQABJAAmWbADq2Zfv8ofVIAAkgASSABJAAEkACSAAJIIEKTwAd2wp/CSAAJIAEkAASQAJIAAkgASSABJBA2SYgKNvmo/VIAAkgASSABJCAFQIRJK5HgETQS62BTj7eAu+2jYRMw1pevjXjPQXkD2rEeoJYZLudOz+fget3C+B6Ur7u98KNAtnhM7mQI9Uy3t5wMEvKbCN57SV/t6zYg1FIAAkgASSABJAAEkACSAAJIAEkgASQABJAAkgACSABJMBCADttWaBgEBJAAkgACSCBMkDgdV+R4PNWDby9Pp4Q5Ne6kU+pNfni9Tz49Ocs+fb9SrcCDXyk1cK8UmssGoYEkAASQAJIAAkgASSABJAAEkACSAAJIAEkgASQABJ4BgSw0/YZQK9AWbZ98uTJoZSUFBg2ZDBz8dLlP7Ra7cAKVH4sKhIwJ/BGaGjI4nXr1gtatmwJEeGhlxRKVT1zITxGAkYEYv0lbv8OfE5ceenHoaW3V9bIYEd2Z87PKPhxrTRTrmSeI+nPO6ID0yABJIAEkAASQAJIAAkgASSABJAAEkACSAAJIAEkgATKMgHstC3LZ68U2+7m5rY0Pz//dXd3dwsrBQLdZUfX4mQsIjEACZRPAgzZWEsm8fXNlysU3qyRGFjRCLwqFgp+Pb0xSlirildFK7tFeVPT1dCw/0N5eqbmAzIzd4GFAAYgASSABJAAEkACSAAJIAEkgASQABJAAkgACSABJIAEyhEB7LQtRyeztBTF19dXK5PJbF5bpLOqgHRWYc9EaTlxaIfTCbgLBFvyCgr6sg1eMM5s1IgRzMrVq21/VNQ4Ee6XdQJufr6Cq/NmBFd/vb+fzedlWS+ss+zftk8Br85Iy1DkMpWJTqWz9KIeJIAEkAASQAJIAAkgASSABJAAEkACSAAJIAEkgASQwLMmgA3Fz/oMlKP8RULvi1nZ0npeXvz7Ydu1bcMcPnIUO6vK0XWARSkkIBQKtUql0q5nrFgk0ipzcy2npyPUckGAdNJeWfV5WO2+XcTlojyloRCnElXQcVRKujKXiSD2aEuDTWgDEkACSAAJIAEkgASQABJAAkgACdgkEE8kkmxKOSZA/cNUx5JiKiSABJAAEkACz5YAdpY9W/7lIneRj8+xtLQ0hn6b054OW1r4Q4ePCObOmYMN7eXiSsBC6AmEhoTY3WFL0yqUSje6jrJIJFTrdeFvmSbQrXVDbxlzuQpD/3JOxGOHrZNPZ7N6PqA4HR9K+Goo4yG9xAqyKv8YJ2eD6pAAEkACSAAJIAEkgASQABJAAkjACQQEvh6K8NuvMO5+nreoD0d9ZqK2dnFV+4kF565tj9b53otFlVP2S2owngCjiqsX0zuNwDtE0+9O04aKkAASQALlmIBds8DKMQcsmgMEJGLRo0epjyuR5ZAdSG2a5OHDhxAdHd2FhO41jcEjJFC2CHww633tnLmfOeXZKhKJmNzcXBxcU4YuAR8vt4++ejtw9uSh/jhj+hmftx0HlPDKjLR1coV2yDM2BbNHAkgACSABJIAEkAASQAJIAAmURQKMQOLxJyNTv1xM44O8e1d+EDC/tVCvJ73ZFq3mUJRJe0ffyamKrXuUXYnMcb2ctV+xSJB0c2dMXKVQD4PYozQ1+LwUZDjWMgxUyU5MlIG2viHQsZ1GpKHnlLe710iVJv83x1RUqFQxbVq3ukdWVzRpH5v57jvMl19/Y3LeKxQVLCwSQAJIgAcBkwcnD3kUQQIgFPqQWYS5umvnpd49oU/BIRhUs3BJ5Lkn82DuMYXDlAQCnVq8Lh0miAmfIQE6Sdbh7MU+XpAxtnAAxKUnGui92w1SnmTp9NWrW5e5dPkyGSQKGoczwIQuI0Bmdo7+dnrw0inDsaPWZZCLqfifQ0p4eVrqvNxcmFFMVZgcCSABJIAEkAASQAJIAAkgASRQrgl4+Poe7Ce9144WcveiF0HYM0ZX3oJLmZA5cI8c8jTVSMBjXaC1f+4wPPD3rqu8GodYSEk/PK1Rvq/lHOw89pN01dKNMtpZvNM4sUgoSH+0LzbEX8Le77d0oxT6/xBtnMSwXy07UZHFaHjNPPEG+Karp//Y1b7xEr2CRvJ72nt157iNv78h/+cnh3xIuOONQHql5fD3g3ena+Z8yd0xm5qaCpUqVRpDiv5rOSw+FgkJIAEkUGwC2DlWbIQVRoFbjUA36YVh/uJkmRaifQXgVtjBagBAR6+dfGkLdOzY0RDm6M70t6Yxi35adCY3P7+ZozowHRIoAQKL69Su9cblK1ed8izd0MsX+lS1/Cb0rWwNVAtwh5/Oq9TTD+Y2JuVKLIGyYRbWCQQ0SfC6f3pjNC+Hz7oqjC1JAkPfS8tdu03egOR5syTzxbyQABJAAkgACSABJIAEkAASQAKlnEC1ttvW36z0QneDmVsC4pjgcy+wtnkwWgYyeu2WaW5mzwAtLNEnEog9zoWeeamhwJO9Y5XK5R1OhaymyfokVn/nLsnKnzEqwMvbi9UMk7Qdhj9Sb7lTq2jqrUls4cE0xX3V6vwM2rOb8TRaKAK3Wz+LK0f28gpgSQFwS62C5tUmmsT5X5ieJdWqiqb1msRWyIMPZTLZJ3xXZIyKimQePUrhvkgqJEIsNBJAAkgAwPbbDilVdALB4+p7P/yuo4gMMuPefjifD9P3ScHNjf1d26ZZI9jT+h68vEsDX/95FBISEnSyyklFlSF5PgM/XciDHxIZ6NypA1SuUhNS96yEXzu7g7KAgYRVOYpUJUM7ca9yW4IxSMBlBDx83EF6bqifMI5MqNx/vwA2+fYGOrt2/cY/YHRdIUyuL4BYv6KBosIfsnTxmZmZkFAlBpJGeEPMyjxIz+GejV4l1BeuDLbsuDUu1eUMDbRYKx1Bpt2uNg7HfdcT8BULzpNRvQ0kYvZnnestwBycRUBLGhgkze+mK3OZMGfpRD1IAAkgASSABJAAEkACSAAJIIGySMA7PEzxYspVkbntmrw8OHRzEriJ6eJfzt2uPT4GAU5esMq97h1ID2joXEOJtkrZiUxewwUW7ehZaiVEJc4ckguadU7PtAwplIiEN6UKJZ2BbfeGnwazGxkmQAJIoJwTsHjZlPPylovieXp6HPD1FTfJysqhyzz+7KJCiZuHu+ccGGTUA8WR0ai9WlifmM0aq1AoYHWfcBiVYL0TijUxR+AdMuswYbWULtNymEMEg5GAMwl4fNzKR/ZuMyFd+sYpW0auFk49vxL69u3Lqi/AVwSpr9nO7uCDAujxp7wPUfI3q6LiB7YICvLfUVBQoJLJlOzrCxU/jzKhISLEXZFyINbCgS0TxqORNgm0eOWh4mRiXigRzLUpjAJIoOQIeO7bty/feAWT1atXM6NGjVqv1eK3mkvuNGBOSAAJIAEkgASQABIoxQS8fNeJp6e+6n9pgLbNX7/pRhc//GsHnH59qkKTl/+bRi6fRqzn9HPIUsj7yVLIHayV8E9JDBNy8UWntyG/8vFOmP+e5dLJ1myxFRfTMll9waOu1Zm2tnSwxaui+kOkZ9HEE3OZ15PX5S/LOGpryeT2ge6iVS/61wtbGD1AJHEvbPdplbhVXcejiXa56kurE2bM8ywtx0KhkHxGT1ns60MsFjNED46QLy0nFu1AAkjgmREo9gP1mVleATOWSHxlUqnMYilOsVikId+YdVqFpHqAm+zicH+LfLiQL4yeCTNm0P5j0y02MgyuD1CbBjrxKOznrAOyAujoRJWoCgmYE+iUMS5gr8jTNY/KlpvVcP6B1DxPUKvVUPBWKDz9xrNFvHnAist5mvF7lNTTYR89YZ7AxrFY7HM1J0dey518rNV4q4AV6NAJr0ru//hBaJl0nIzPHe7zI7DyL5l21Kx0ugT5BX4pUAoJuIYAWblkUV5e3jgPD/bqXVZWFoSHhv5eoNG86hoLUCsSQAJIAAkUg8DMatWqfXbr1i1seC4GREyKBJAADwIeQpn4nXRd+x0jfwyd38kFYWQlqwkLO3SnKDS5eb+1WLtkbFSf563K08icy9fggmihTTl7BTy6bil4sD3KqVN4BzTMhCW+cfaaYlW+as41bVaDebye6XTJZHeBW9YL/gnhP0UPFPu5C63qFp57h9kfmKZrdErRJEM/aZ3qJMEtq4lKT6To49mz5R99+qlDjWa0zSs8QAyBgYHwy+r10LZtW13J6teryyReuiwmB5yDDUoPgvJnyZjXxmiX/bqMzr5yXaN++cOGJUICTiXg0EPVqRagMmMCbz969Gge+Ri7cRjtuDm/auWyBsNHvMZ5vmrXrsVcu3adVwXCRLnRgcQTjqaNC2xlFMRrd97ZApi0/QH4+/vr5OOiwuFa/wJeaW0JNdzhD9duJRnEGtapAYe7pIOHWyGKRr/lqK5laq3XgAypcQcJ2EWAyZ0caEgQvUIFT6RKw/FPP/0Er92YbTguzk6ttflw/WEGeHsX9g1WJUskX7axRDJbfsGLsnKVaijWbFCyLI2GzJDnfJYkJydDtWpVL2RlZTcgnbgmZnh7e+Xl5xfYniJskqpUHtT8/r3gK1OH+XNyKJVWo1FOI3DkrAraDntEv3t70WlKURES4EmgerWq2hs3b3HW+YzVBPj7MzlSKT6rjKHgPhJAAkjg2REYWL9+/Q0XLhSO/UpLS4Pw8PBfiDlvPDuTMGckgATKKwG3yu1kwqH/mEy4yD/8uabv/rdNR187CcDWyOrqwCPd2UcUOphHWp2NjPZsHK96L98s9rX0gAYexWoWscjqfHA76CypaRFe3IAW177V/OCTaHG+2mcFz8uHPMvZMcXN0Hnpa9apXevq5StXi3XunuvUFv6qf8XEql13C+AXRWMYPW4yfDdlMHP0YT7tyT1qIoQHLiEgFomfyBXyYL3ygIAAyMnJqUKOixrm9ZH4iwSQgEsJFOvh6lLLKphyP4koKUeqiHO02EsWL4KUS7O0n/yUHU10pNipR7Ksuyh7SC1vq41+avL9vyq/aSCcfMvx1MtWRe3M3rb4kUdq6LKpaEbi7Pffhek5S8Hbo/ASpvEvbZP9J82D7ra1oQQSYCfg5QYzpzfz+WR2C6FhpGfDbb5w7U6yIUGnKn7wT2+n+ikG3Vw7vyTmwQ5oCvNizkO1AIv6vEkyaR4DkUuyh5Pv3a4xieBx0LaRl9zTr5F47/7jPKTZRciyOIxKpSrZBwS7KY6EBn86MTBl9rhAw/l3RAmmKT8EdhxQQq/xqdh5W35OaWkvCflMOmO3jZMnTWQW/vhTWX3u2l1eTIAEkAASKAUEmgb4BRxr1ryZ+8pVKwWRkZFWTerffwCzefMftBJv/0PeqmaMRAJIoIIS8PQesCnfo3pP1uIrvq3EDMi96/T23uTf/4S7Lfaw5uloYMZzOzUFf4VYb+SwQ/m5q3kQ+zr96o3ztqbSG+o79b92eiPQScVdyE17B3wE7B3MHbNCM1SQ69y1ox3E4kHaykJFgver1Wvme/DoCYMW6rssX74cvvviUwgCGUyomQ8x7++GNm3a6GSePHkCX/WqCp8fegKenuzNLJPHvwlfe2406GTbSVdqodaqnJ+UBTCRLR7Dik2g6dq1a08OHjzY6nPj/PnzMGzYcObenbsKmVIWTnItmtlSbBNQARJAAsYErN6MxoK47zoCZHlj1erVa7wTExMhNjYWRo4cyZnZmlXLYfjI1yA3Nxd8fHxg8Ct9YfFbF8DPt6itjr403/k2s2DBmhwS7n4hL585r8rXZpIVV3OI4gzy95D8HSZ/2UJ3yM6cEFg4RZYEsG2P5Fr4rfJU+PCjjw3Ry3uIYXCtklsx9EaWBuqvpuabbi8nBMDarkVl18cGLcpS5qrBdBqgPhJ/kYARAV9PuHBjlH/9QB/T6+jA/QJouyYVzGeTTmzqB/PaOL2+bmSR6W6BhoGc9xIhOpqOxyBr5Ny6BXvHNYThdehKJdxby/VSxYV0DR11KyF/tMZclfyFkMm8gd5ebkGeboIaMqWmwcvdxMy6b8ItZqtX7i6F5AfpJAlAfGwEqPI0kJJaeKwLNPu3adMmuEhmFdSoWRPGjBnD5OfnmwI1ky9thy3qe8uPr48qt8+M60n5sG6bHH77QwHe5NS0YyTQmvGF1h6+EOzG/3rO0qrhlEYBhxk5HHGXwWMogCF9fHV/DWuX3DuhpK+fiXPT835aL6P3UkFJ5435lX8CzvgG1OHDh6Fdu3ZhhBb3g7r8o8QSIgEkgARcRkAkEmvv3LktIDNnHc6jQ/sOzMFDB2l9E5d7dJgiJkQCFZ5Ad9E7GbsFHtZ9r7wl1dT90i/zd/R4Yt1av6Y6cGtXp+mVL7oM8tEKnrnbFvt0URZMXBdrW9AOiVWiqtppYZ2c3r4x8zowfb1HWm2Xfzmnvvyh9g71Q0t8E3vCqkdvBAz3crdqIm+7Gm6TkAkR91jlSfsRXBsTCjUCrfffq9QMVF6WvVWWD31ZFWGgvQR6f/31N1tnzJju8Ek+ceIEdOncRaVQKizaFe01BuWRABIoIuDwTVmkouLt+YpFin37D4iaNm1qtfCZmZnwxhtjmN3/bHkoV8I0IvyHUQKxv0Rw7tG+2OoioeW7//lp0bBz9wEjcQDaKTIg4T2TMEcPdpLZQ6lbpDDYRiN7rT884e7DxxbZrFmzBgacnmIR7qqAlrv84fx19tUYVvf0hYE12Duw2m6Q5p55rGEftuYqY1FvmSDg4w7SrAmBnJVf+ewbEBJiOahxyZIlMOLKzBIrY8cDleD4+csW+b034y34MG8FuJFvgFjbWv5bAOc303b84m/dJobAf3uLRlVSjdaeS+ImSUqliqlLxIxv3g5B/u5fh1Wq1mzTpj8EdevSaO5NJpNBWFiYy2bvCr0F2cqz8VYHrnBbV7piPpyfCSvWKmCuIAp6ewU8c+P2F8hgtuABdOrhDQs/Dub9jeZnbrgVAyTNkpRyJVNuO/etFB2jXECAfA5DSz6LYXiIHzp0CK5fvw4tWrSAevXqOZSjRCy6L1fmVnYoMSZCAkgACZQCAnUT6mrpssJp6WmbiDmDimnSZDeBx/z6oV0hTFJboFLLmKTMY8wj+XVagz6rZtRvE/2mTrdRhhJfiYZ0sro1bNjQKLT4u//++y/06NFjKdE0tvjaUAMSQAJljUBbj+dkGjc34cXpG1l7qBhGC9rkI6C5e0CrublDqk2/7As+AedArfIQz0hrxLe8sZ5zIOGjd/iK85K7+O7Hmuw301nt5qXATKjgejZkxN4wC3X8sObz97XHpAmWjawOqnxZdhv21fvCwdTcySpd+Er7l/9VXnZuVC3WLMn9dC1ZrKEbCNz8m3t01LT27CFp4tEBIt3ZO6h7yxuolTMueCi+DJgBWvU8bkvYY3rEesj/6iNxut9Llz5us+wOhIZazoa2p60tWaaFOityBpMV5tazlwBDrREQegvv3rh1I1Y/OcSarD1xK1euZCZNmKSSK+XW2uEHeLv5fJ6vLahaJaARRPs3Eni4ecOjnPPaW9mnoECb14nkedCefM1lBQK3xCaNmyScPXcGtFotr/vMXAceI4FnTcDQUPSsDSkL+YtFogK5QmHXiLLQYAmkH7K/w+Tn36Uw7tN0EyxhwWJIOxRhEubIQdzzarh7955J0gDyLk4dbTpSb9vtfBiwXW4iZ3wwtpEEFrRnX97CWM7Wfr8bLaFjxw4w+NbnECrirvtpyPLMXbczcF+qAbEHA/970YPI83v2km99PiHf+rSsFdgyDuPLHQGyrMvbmeMC5nnyGC0oy2fgo1MM/H49D+KDhLC+ixYqS6xfczW2+MKqVavAc/GL0DSiePfHxXQ1NF9XtCy4+cnoUScctnYznfjX8k8tnL+fbSI6d86nMLHbcgjw476/TBJwHMz+IRvmLM4wiW1WPwRO/W5/n2fPcTL450CaiS5bB/9n7yrAozja8HtxFyIQLAmE4K4J7u7QUhxaKFCgRSp4+VugSLECLcUpFCi0uBZ3dwgEiRH35C6X8382NHI5yd7dXnIXZvvQ7M58+u7e7tj3jYuLi4JM4mq/AUUJya/vGHWu8tmK5XR6pedzl+CZRKJAs74x6JtYBlNt9Y/2KCkXdomSscImFk9OVYRrEb+nkrJRm16SvQIuzcO+I4uBl2qjo3UUAQ0IVOjaqUPUqX/P8X4ni4BCtnyNJUHK76EjLX/DRx99pIG96OKTJ0+iR48eXxHKNUVTUwqKAEWAImA6CDjaOwrJQJ9dYYv27t2LUaNGMZlc7pC6YPKPjNOqHEwb8Xsy8Df3qxZHeXZWGtdm5jGKZVkQNjyKtb+syisrrpMuVb9C84pDsPJ6N0WWNJ1p0Cl3/ovLEKqHIkARKFYEbGEnvOSelPOeuyg6gv9NaAOenXEW3Mpi7qH3Vu2LpPVx/uigxnBd3kIfVrU8f125hDZNuQnSc2wUpohyqM/ZWPckhYVkr/9YwwZ2Cnnt/nCu/JTbO67GNQpJB4L45RQOX8flYKCQCJG13IuZ2Y1UIVRf4H9xsPPb5j75/ZNqu8RYt3U3OnbsCGdnZwinuqvn1KHUfm0qCm8LM2zwAGwpf1EHKcC0i1nZvz0WOREmde0CnWR9AMTtfV0bnBtR/1fegdfTEBJ9o9hdbtygObq7rGa1oD+OH4o/Hk2QiWTCIcTQQ+SfVI3BntaW1nfsHe19T5w4wctNy51LJ5FIYGtre4U8a21yy+hfioC5IMDZh8xcHNbTzuY3ycFEPuhyeHuSSdbL+k+yrjs3FZOn5Eezvj1dGVUq6d9W8GgZj+RU9ZOwPau74u9u+RM6n1+SYcfDdLXu1gmsggPByfBzzadXS1hEYeXtYiSkK9sTFxeHgIAAdCwrwr5eRXe0i1ChVH3wlVgx9KSAmV3KVKqgFx8MAr4uFvwXo105XS24/qEQc2+TMPoDB9CzZ08lLB0cHJAyTmXcSYmmqAs5SXdeYZsIYVExcHNT7cx5ujjg3Zh8HQdCxeiy4y0Tnaoi2oU0sDNuqZarEGopOBmxDt27d8+hSE5OhuWrxnpPBveYmIbjF5NYNdhyTWIaXU5ODsfFYmmv3DJd/1apaJX55nRlpmFvFkd8khTVu7zDLbua8LLQ/xtgqs4KyUryBsJnuPq3D6r7q8+aYIq2D/gyLuvg2SxO3yem6Ce1iRsErC0w4fJHzr828M4fACksmVmc9lfDnzFm7NjCVTnXzPtv3JiR2L4rf0E5j8SKBVTywbTv5uLzzz+HpaVy2+zx48cgEWKkn6roQoScVSuYFlIEKAIUgRJGwMnRWfr4ySNLf39/gyx58eIFPuvxA9pXmsJKzpnwFbgVcYAVLRdEtWrWQTe35XC0UT/gvfZmH0WGOJHpUJzkQh+VQRGgCJgWAs4811v/ukU3K2iVSJ6NLp3PwipQ7+5tQXEq59l/D5UOCNmmuQGqwlF0wYUOfaSWWypxJrPlhKP4ezU3i5FrNHsnvW5TixPbFmfFYkWteUUDogPF6PA/MBHndODQjTRI5K9w+CpMaaxf+uoERPs/UipTI7XDjMZ2J39saZ/TIX+ZIsOdNivw2bhxOaReHmVwva8clThedM1M3JJtvVDbwxLqAivEZLswNumZaaCOmjv6vmiBv2ujBcPqr1d7/yPTH6HtFBuM/XSMRgFcVwypswLVPFqxErv7xQQ8fnOVjAEaNnzHZDjp37d/bFZ2VnlWiikRRcAEEFD7ozUBu0zGBLLfrJTPFyiPgP1nHZP+eMzIwThz9jLaNS+D1g0l6NnGAfU53Ov1LdlXc82uLCyd7gw7W/0WYv1vXQq2H7XD26hYFVxredvj/ifKK9qmXJFh0/38Cdu3b99iVp9G2NFRP/2FlU45L8DEHTfQqFGjwlUarx88eIDg4GCsbWmBEbWVI4I1Mmmo8NyQGiOQooKGalpcChEgP+BR8RPdtjta6//KyyR7uvr9IcWWLVswZAiz0IvdISWbSXu5uyF2LDeTUcffiiEYtB4jR43KM6CMsz1ixir/jlfcE2HK8Ri4uipHwf60ZDF+Xzsfb8+pT6OTJ1TLyfgFqeja0hoDuxjWcCqo4l2cFP+cJXulPrTCpduZqFy5EjZs/ANNmzYtSJZ37uNTThEXF6/rS6lN/GXfS96kQ2Dqx7AvE9DuhgcGahjUM3X7DbHvppSPJZXe4cp+029Py0gn0rFp2NciI7WzhgAAQABJREFUEVYY4jPlLb0I2FthGVks9LV3EZlBlt8TY95V5YVsBVHxr1gOIQPFeUXbn4lQxo6HPlW1f1ukZCLYykL528esbCeHcmGe5NJ1QtrxItKOVwKJrLY2u33XS9ddod5QBFDb1cXt0cyZMyzmzptrtHcRs6hlbpuio0gOhX2DJ5GXjX5b/vnnHzxYw75t8yblJv55PjtTJBe6GN04qoAiQBEoDgTG33Tnb9SkqFX5sVLbvls5mWwsrEOwzFMxWBzN6fv29PLusB9UpbAqva4FwQelmecrcOL7tEZ8zHNg/67VZnCLrGhZaK0FnA0eZMqEOBPeFRUtucGtsO2tKk+S2vbcoBbH7IMjBbKQf5QGcFysceNwP+cWLQpE1TIyO16rjGt3HyqJDxntCn8DA3eUBLK4WGj3KZYsXY7U1FRMbeuHTe2134o7ZEypzV+Z1Yjo1yzEl1oSaws76fjGuy3d7dn/DuacbgA7u/xgEGOB0zVgOppVKDqj1L7o0Qh9/cJYZmDkiFGKg/8cJGmcM5lVdCKjKaKCKQIGIMDpR9sAO0yRtcKWLZujxo79VAUjb09nEkFrWLRacTg88Is47KhjkzNYF5oqQ72d+ROxAoEAEZPKo4qb8kfvh9sSLLrxPhD12LFjsN8xFC3Lq/3m6+xCi71ZOP04AmXLGr6CrkvnLhiouIpRtfX7qETz5ai+NX0UyZ+xU2dHKIM5IeAwrZFdyuJW9nrN9GeJZfD/k4f09DROfHYmq8MSP1UaO9ZbbhZJkTspvi32/s1kCQEqeTjh1XBV2YXTbM5q7ogFLd7DUWl7OhJv+OXwm/L/Gg1Oxv1nqvcgPj4e5cqV+5HYXuTyVwdbXrrgvr9JD3r1HReP/z31RSVL1ftoyvfHmLZlKGTo5xqKhydMe52NS7MwYaZAoW3fFmPCRGWbJgLdHgx3OVmjjHI7S52pyUI5fH5LgYWFhUp1WFgYwr+pjyCO2mIFFTivS50hlWNlwTIzPG+zdevWi2PGjMlrr9vb2+fsg969ezf5iRMn88oL+nbq1CkmawTTiKSd9ILA0HOKAMcIkInTUEdHxwDSr+S1bduWY+nK4sieZahfuTP6VV2kXFHEVdlB90h2qy+KoDK8WigU4n9d7pC+uX7tvDOvf1Y8ijsaKpKLahhuDZVAEaAIFDMC3v+6vot3tnDTqjZI5Cd3+CpctUGolYtdpeXRYGnXRxe4GdwjKg97B8rdb3XmxNb4gL1QPK3CzpEiqCKCHeBsYbibNyWZ6FFtWhHadKv++Pkz6XSH5YYbV0htNonW7tr1EqwCuhaqUb4ULPcWQpLl3j/AOuPPHk5qP0aHgjaoDVII9C2Px/2ylQUa8arc5iykCZT1paen49DHFfBxdbWm51nj+WtqlkCCDyYjlp2lQ3qHKlOdG/n0VdvvyQNGw8mr5KvY+3Smhlpui7t07I7m0gU6Cd34aBAS0t7pxKMPcYGtOJhQ+M76yKA8FAEuEdDrB82lASYoy6Z5PduU2CRrx3+OXETjxo1zTLx8+TJmT++HqzuZRRime8SSPTD7D4/BxQHKqYX/fiXGsBPvIziYlKZvpvijnld+W4FJyfezy3jMX7QUQ8k+Aotcz8PHkZP2F1zWZWH1LysxadIkowC3a9cufPrpp/AhgYZ1y8iRJlLgbpIlxBIyUe3VBU3IXkHlnALV6mb2MVp7s68iW8ZnRhGuqCXSUkgGWJ8GtQiqdfXa1ZzfEhmYOEDIB2thoVWaEbAICQmR1ahRAwvmL1CsWrVKnMnPZMJHFZpZ1NZYWVvYikbW/83Cx1n9mIZAnIJ7MX/jVvReWNuS1JKOwpyIpbAMBZJkjhg6bBh+++1XtcK5KCznWBWnBySiepn836Ahcqvts0BUXBJu3LgBq3VdUdczXy7z297faCVGjxmTo6JXLQ8c6KwM6fpH2fAiUbMj+ym/NwyxyRi8367MQOUG8/DF5Kk54hMTE1G7ZlX8vsBe1n9qQhNS+FCd3pF9nbJ2LCZpBUzwOH9TiBNfyjDbmv0qSBN0o1hM2k9+t7wZWRj/sWnOvW/8K0M+YWGSNQFDXiyAUCWmioDFZ3VsBL90cGS9quxKtASdD6ju3GBvY4XUiYY/7/47RahYsSIEyXG43t8Cdlb5zX+vDamxfCnM8gVEomglJIo2/4On4xPBTIhXqVKlP2F7v/pJR35KThGgCKgi4GBr++jMuXP1Cu8ppkppvJING35FzN76ZPuNovuyTFrGRVeCVfbVM5Z1kyZMhkfIcE7EX4/cobgRuTNdKM8y7QEKTrylQigC5o3AAodNiu62n7ByouB+pKwYWBIpZGI07XoF3u3YpSUtSqxCJsPF++Ng6Wl4Nzt1xHmpaJOT3m26XFtPXslC81nlci8N+ls+7akiu8Hq/EazQdIA+wffKC66J3AmL9ecC6LD+GFCW1b7Inv8Vg4vhzPdVc2H47o07Nv3F/r06QMbm/eTo0zmCv5kN1gWyt6jWYphNffipWi5N0OjkN413LC/q/I3PluqgM9mPtwdbSG3tMXrsAhme7E9MplsqEZB5lnhYGNhn963xgKr6p7MMLbhR8tZaejWrZvhglhIYJsFhRGVnh2HL3bWRPXq1VlINg7JmtWrFV9Nm8Ysci6eWW3juEGlmikCnH8wzBSHng72vP0Zt/zsLS3ND5Jt/2Ti7N50bOmoeSFRebJ/7IwZM/Hsr5+xtaPqR9ptQzruDXVC1UKRt/reT+9fszG64Z9wtXvfYFp3vzeS0+PVRpHoq4MN3/z586G41IMNaQ6NVC7C1kcjFJmSJNhY2QgkMonEytLaUq6QOXt4eOCnpT/xikqNy6SUEIlE5vcgsUbJKIT7MjMzP9K2T8Ht27cx67vZistXLsHJwUlKGl/Z5INPspPKXRmwR9bayitjX4m1ccwADbMiv7iPgo0UZqBo2bX2SJpoR/bpUG506mOXgETfjrsErA5WoHAqzl0vxDhuFYyKfgFYbrVXo3hmT9yzJDBs+zLDI+I1KjFiRf3+UfznbyWLSFbqn4ga75fHK8YH+mlfiWlEczSKrtgyEo95dTTW0wrtCFQTPkHyPV/tRCVQK8yWw6FxeBuiWudFQCVgLlXJMQIOVkhOnuReRl+xH/0LPE+WYms7oFk5g8et8szYHSLCtzeBpAxBThmzN26bah640Pe9jogMGZruzliVKcH0PCYTPyETtjIyYavXh/P8+fNwThyBpnXtcrx8ShY2Bg2NjudnKZqTgggTd52aRxEwOQSsedYXJQpJW6ZdaypHREQENgx7S9rX7CcTDr+Zg8fvmOAKdoettT2+Cb6gQnw7+i/E2V3Cwyd3ycSxapcwsGotfFxxqwqfoQUpwihsvDtMIVNI2hFZlw2VR/kpAhQBFQQ+ceDZ7j7vlqz6w1YhNaxgYFYzSdqM26oDd4aJhfTJHvQ5PMBAKfnsfztWUHg97WcwHsL9b5HeOylfsJ5nn81LlC67UNXgRjQzTmRZeTjcrbhJpOT39AfpPqc3BttVGJZJ/F6yl18fKzKtj+LSAiQ12qL2m1RYpqlcB+yzxLu4RLXm7NyxAwPufqW0BUzgXh4i45NV6EeNHKHY+ccupjFgrtl1ZthbuSyf0vwgz8aSm+exIEiCRgewctWKgkU553w+H/6Vq6JNuSmo5dVJpf7Hy0GsF7yRsVv0qzUfDcr1VpGjqSAs9Q52PZ6iqbpEykmbjmnoLiX/ZpWIAVTpB4eAwR9Xc0HMxZF3tHd7h/ZrZnk6enA0MVncvmeR9HntxybiZYQlbt26nbPaZNzo4fjF9URxm6KiLzpTjpo7sjEj+EyRKZ/4Dfdj1eqfVWQYoyDQpyk+DvylSNFCSQaOJ01HSOjTImnZEnTv3kNx6tTJM4S+G1ueD4xuopeX1/qEBO5WG86ZPRevj7gi0KN1kVA+TTiDgyHzi6TjgiA0NBSbx0TB1krzwgpGz5Z7IzC5QTSmN34/kMyFbn1lOK9Ph0QqA5NKPbhFc2SmRWHfz05oWqfkbdPHJylZebmKpIjffigTj/6pCKsCkWb6yNOXp0rTd7hrW0tfdspXCIF+klc4c9lL7z3fC4nj7NKxafivWVnySZwJpIJMHYHq14c4v2jorX0sZtNTMdqsvIb69eujTy13/NW5eJvhfc5Y4kxI/uBHWloaDg6piE8KpBh7TDK2dPo7MzxTDH+OQe/u7+93/M2bt6S/q+r3iRMnMGXyF4qk5MR3GRmCRkS3ppG7bjuWeB0f2cdZacK2Rq8EvHirGq1cwacMos9xG4S2Ylua7IdfUyMzBAomjYuUY5yoOIqAWSBAstrIJjf7x8LRJn+dyl/Rn+Ll62fFav/Ro0exftpFXI7eiI2//4oRI0bk6f9y6jS4PPo471rbydZnwxGd9FobSV7dwYMHcWOFS5Ht+lyGx/En8YZ3GLfvXYeDw/sB1xnBp+Fg7ZpLwulfZhHyF0+nwoFEGnfIeJn5RCacTZapruNUCRVGEfgAEejlUif1aNUJbrcF4bgQ3QjtbPoYFYWNWT9g37TZ4FkUOSenkx1ZGxvKByXeVGpH6SSgADH/TRjuW6hO+BQgYXUqT8pGgtNjVrTaiDyCwxWvLOqpNjS1Mamp80sPkWfU/5kTjDYkXkZjAfcLddoIfGXWM8ngcBFH4HYfXB+kvY9ShIgSr+50RIZF24+g4DYLTKrkuK8qw9clH4KzERLwxu1Fz549VWx2dXVVZGRkcHJPVYRzWGAPi+WdrF0mbnfyc/xbnIrIxns5lK4s6kn8KRx68X1OYWxsLOrUqo/evj+ikms9ZUINV1Hpj7HoeE9UqMBuK6tW/sPRvvJkDdKUi9vO46NTp/zJ4qVLl+GHhT+is+9M7H34LWxt9doBT1mJDlfOjm74qsmpPI6YzOfY/Xhylkgm1D7Im8dBTygCuiNg8MdMd5XFyuE+ZZhLwtrZBXKFFqt63ZSdu5mFmcv4ePZWxAxcYdbsOfD09NQqxMneBknjnbTSGKuyxZ8kparzp2hRaZhOKva+mohXMQ904tGHeOSwMfB99zkr1mvvtkLsHYrlK5egdu3acHExPB1hYcXXr19H586dFVlZWddJXavC9aX8+mfyUZ22ZcsW3jCSepjrg2mwMalyf1q8HLfOvCJ7WS1mpaK4om0D/Gvgk8rbWdmUSxTPf00aAeORMMGGpKEp/rbltCsS/HpfdQA81z7mr5SEs65duxY//PA/uJGmypShlpg+Wvs+PQX5S/L8IYk8C4+Rop+WDAVc2teoWSzO2pRcWhcufTFFWRMl4dh9zb3EJuPVYeLaPOx6Bl/RUl0dLSs9CDhZQ5A40V3rsuc/QsTo/2e4UpuOWcH/Yow7qhTjQsKfXCbh+x9+VAG/po8LHnykOpgTzSf7Q+5MXyWQ6h+BS9KqiUm7x9rSMn9ARcWAQgWDu3lj/0rnQqVFX45Z2gTbduxTIjy8rhz6djBuX/qPo5mKyT8m/Ut+712VlNMLikApQ4DsxSr/tuVFtQsvcl395X5PpGUm514a9Jdp35NB1iJltKkyBm0rqfb5mAE1D3vfIidXb0bvwr+v2c1p1qnQDv0DfirSppIm+OPOENy3U12wMlEQKTwiTllDdgmkUSIlfZOofnND4GNFw3VKsydlHn+jOOnC3QJ0dYC8k73BiCESWHjWUFetd1nWqgryQYK3nAwyHCobICtzsyv7hp4Gq3/44yzGDTZsHM6jebj8lXU9g/166dUBQY5VNFiqW/HaV5XQ3LqjbkxFULfgl1U4fh2vdTyft78vErrdM6vo2iLczqlufdEHdx69XyB2//59BGztSLJqKEPR7ZgcF9+kKYmb0cwZi4KsUWZDapZQajr73TrBYt8kO6/+39r7qETWD3ZtjuDKo5X8MMWLDQ/6ITkjrkjTmIwsE5vuhZejv1baGP4zOFp55GXuLEi8M2QMIhJCChapnDORvaT/CWdn3fuTKsJIQWXPmhhVe5u6qpyybCmfbLvYWyqWZ6vcQ41MtIIiwAIB5TcbCwZzIakdYM1/eriScUdp9ADjyj0hBk4ToHef/ti0eatB6YLXrlmD8W8X6mGFfiw1t2WjsvsQtPEbr5+A/7ikQUex5KdFBslgy1zJxx+jA/ewJcfa+z2QnpnCmp4Lwu++naVYvWYVk1KZCZnuxYXMEpTxPxdnl7ldu3bF9h3bebmryYvDni6duqOZeD7rRumel1/gddy94jAN4eHh2DgigkSh2+qtLyLtPvY9m4HIz2zhamtwP4SVHY1PuOLZqzBWtJqIli5diu+//x5zx9tjzueqg0ea+Iqr/Jdd6Rje2wnurgb3M9Wa7NE4Aq/s66qto4XcI9A4+xnC7lbiXrCeEsnE7UsykcPtKIuetlA2bhEgs5C7b96+PfTS9FZoUtYKMpIsqYWP6sRnRbI9RVI6X63yCZ+OxmqnI2rrjFFYfZcIEcnvUyQXlM9Evgqnan4/v0iRoeGujDaE50pBviLOm1y9evWOrvtaepD57+RrPkWIVl/delQGrtxJzKtcvOhHzO6vuYOdR8jxyab9GZi8KClLLAETbb+b/JP+p4Lpl/Ryd7GYV97bssLPX3u4dW2ldb4/h63b57H801eFzMhhvnP/CaR/KALGRsCKZ3X2k7prOvq6NdJJ1ZWoTbj57k+yENYNGZnpaODTE83LjYCLrbeSnCxJGjJqHMSmrRvRvk1nBCkWkMWKqu9ShuldxhOEWG3HzTvXlGQwFy39h6JD5akq5WwKyn10D1988YVGUibrTIOq7TCketHZmzQKKYGK4zd74ayj9jYRicTlP5IJ+xDzLpSAiSWpkowdkw/3f8d/GSBK7dhYrp/0r/4I9HCpnXa86kSVVSRiuRTNnu2RbXQ+Z5zO5H8mt663Qm7TciangwDur4crgvZuMfi5jz50HG/qG579z3/gQcWN3RUMsqd5wzj5ScdAg3CqnxEqjaq3TP2HSMdHqPGTvdINzqc4kZWrOtiuncx+zEWNz5vk8W488foGlQskpUklW/jMuwNUcxTjy0Z2uaLM+q//bgVik1KRnZ2NZZ3L4ptGypAMPC3H8Rf5E7e/d3YkW6rlj8cNOc4XHn4jaUBACC1OIIiV4/vZuK/63dFXayegcvoLTGtlXp/m9Q/6IiUjXiucHdt2RTAWaqXRVLnt2Qi8S3qlUu3tUQ5Dq22Cs62XSh1TkCFKQJmuj7Bg4TzUql4XA8puJOPGyq+JeP4r3Evei6cx51HGzYNkHMxCh0pTUbdsN7UyNRXeiNqhOBf220xSv1ITDS2nCLBFwKAPIlslxUlnb8tLy7rvr9KYKk4bCuoiKdRw6n49nD3H3cuW6WBYkMg7bQNsBW3Q5zyNfNSrbBWjR7V5qOnVXh8RannOZX+D67cuq63LLWRWVTNpp478cxx+VSpj2YqlJHqKXTuH2aesf99BEGYLUMenE3r6z8sVq/Uvk0aqxcw09O3bVytdcVa6O3ujtntXxa3o/bDhWaVnyQRM7vzN5F9ScdpRQBezamiovYXdQpFcUrmedxdyX+x5d6L/LkBSsqdisRhD6i9Dbe9OrAy5H3sQ/4atgZuLO+rUq4XzF86x4mOI5s2bh4f3nqJD5zYYPHgwKlasqJV37969eParn1YaXSrTs+Px692PsbWLJQYH5jc+dZHBhvbb6yTE6sAT+Pr6siFnRbNg/jz8e2QVrv9ZlhV9cRDV6h2F50e1D2zpYseIaQlYcydAFxZKyyECHV1D8OB4BQ4l6i/KvUXY5bRMRVv9JVBOU0Ng1jczZYuXLlfu6REjxzQqg19b51ubRfYZv9hpCwYMGJBfWODMs4wb3o1QEVOAwrROq29PF0RmyItM7+LgYC8jHV2tjpGFarh8+TJuXL+KXTs3oXdrMX7+xvDm+3er+PAMnEaAs8DQputRvoiU1aaFsHZrlmxKFc9enepCqETaKWktRcBgBOydbTwEX7Y4ZrJjBczWNvsixiMqOjzHWSba5uA0sc6OhyRewIHn6oNO69VuiPbOC1UmmnVWUoIMr2/3w3p7dgthQmXZ6JwRGsKHvFYJmmxs1f0zMzP/cXJS/ZQlJSWBbN0zkRjwG/mnePHiBcqXL4/Fi39UrFmz9pZQmB1kbOOofJNFYDCJsv1Lm3XDw3dIv5Cfs2LG6Yx1BNu0ktl/elV5ZsoAZfLMGHRZwINNmTIGSHnPerhmdan7iU7sBu00aEtp/LdcfK2SQQCubirGSFvt2Qo1qM8rPuBUQzHes5XB3z+HB98qLrhrj4bNU8rypE3TX+XWTSaqxUhxfhbu19ihlC647FYRCUpRXbC5b98+1Dw+HoHunD1OLD3gnmz9IzFSgyYhKoIESnidVVIQkSFD3Oj3aZWdHWyROI5ZP6l61NmZzn+TJmfe8U9Vaw0vsQcv7YlrbVd3DYvSCmuolvESk1qeL1xsNtdR6Y8gr3MDO3dtV7GZmc+Y1foSrC11WzzAFydj1Y2eOfIYGW7OZTCl0QnWQTsqhhRDwdKr7RQSuUjt77UY1FMVpQABgz9EpoKBo4PF1tTrvmOsrUvepdR0ssn2NxVx6cpVneFhXj7ebk54PdyGpHIqPl/OR0rwyVEpJrQ4TlJLaF3wo7NPuQw7XowiG8O/zL3M+1vVLxDNnb9A1TLN88oKn/z6cACJVIkpXKz2mkk5+FGdn8hkcwe19eoK634RiY8++khdVYmU+VUOwAj/XSWiW1elW58OQ3TyG13ZjEbPpMIY33QnyjvXZKVDQgYo+i63RVAQu3545Yr+GFV1j0bZzCqubU9HIiIqDO7uypFL9Ws3Rh/P9Rp5Da348XIQZja0ww+t7Q0VpRN/m0NyHLn1AuXKldOJjyFetXIl7NIWY+IQwwfrdVauhqFShwhEnTdsgvrT5ulYbs3dBLAaM2kRCwR2kYb9Z5cs4WBf8u1khyZvVwuFYGaS6GHGCDg6Osr5fL5K4+zly5c4NqEJvqjHrG16fxwIFWP4SeUIWyalvI+nO0705KGueezcketO3t9MMWmn/pb2CSnYm1eYf+K2adOmlM8++0wFIzdXZ8Rd9DS5/afzTdf9bN3uTOy74IOyZcvh814h6ExSrhn7kMsVsKwbxqwwLL4QbWM7ReWbEgL1JzTZ89DTwc+UbCrSlixJOizsRbCTKkfxFslICM5GrsSNsPdzMadPn8bwj8difL2/YMEz/4HsXP//vtEdV538ci9Z/Y2WixGc/uI5mcCtzYrBDIjs7e1uZGUJW+hr6i+/rFVMnfplyTcq9XWA8umNQHeX2qknqk5ktQeQy6Ov5f+6JhrtOWnB9yZpcblLySy+MF/W79ocg194zxYukyePjDDI74SGfyvkNyqptCHZ3rikVBkseiuPv7DlzaXrmfFKcaPeUr1tyJXTJnS1bLnNQ4NxzZUXIQ3F6KE8WHhUyy3K/7vOD0mjZfnX5GzBLSmW3sxQKlN3MfubmZgv4n6/XXW6zKVs9LvWuHrzriIuNvaMTKHQLcyygJNkojb1lVtdN/tCEZ0FSNSe+mWE4suW59TWmWOhkLTRNj76GPce3EZgYGCOC0Pq/IxqHrrvIvVv9FJ0Kv+1SpSsqeNyI2oXib5db9D70cg+WtjZ2UlGjhjO2/fXfpBAOmZlW5aRdVLxLBAw+GPEQoexSepd2Vn+UavGuq3SMIZRm/anwynwF3zyCTOWpdvRv1c3LPG8AT8jpegsbM3cK0JEv7DDeju/vKoh7q3RvOLQvGt1J+Fp99B4tBRfTfsyZz/LofV/RnVPdgFEvz8kqZU7BuH65dvoWGEGdE2zlSlKQt8ljmjfvr0601TKBtVazGridsWtjjmRuQcOHMCYUZ8iqNxogsP7e7jhIcnNnx6nItuYBUxqsFa8H4ypgnPZa++RtNL8FM7lahPYtlVHtLFclEMSmnwFJ8OXYObXX2H+gvkYPWIsKkUWncZbJpdg6K8kyrZOHW2qcuqYtCsD6s1HYx/1EVOaBAjEKTgdtRjlqjrh/OXTmNvmhiZSlfJPfnVBrVq18Pz5c4zqNgvd/Gep0BQsYPwZ82QSPP5bwbdflIINNjG4OcKpWFagycmik4A9PMQk6v4sxMbGol+XQNzap/uAW0EMuDgXkkwDP/yWhsVf6bbquO+4eGwLUdOR4sIoKkNvBJrbPMOr8yU/ic6r/TaYOMH+BaC3x5SRawQcrJCaPMndrdvtqrh4446S+C+auuHnYOU+2Os0GQI3JYLs55pD+/GAPlhf9gqcbEpDs/u9+2qibsm6Q4USNsxF8ya1cWsn2T2xlB2br32Hz8aNy/Hq0KFDaOz8JSqpSY9tLLet67/9jqwBWGos+VTuh4eABSw2fdf68melabLyw7uLmj3+/VZ/hDjovriSkXhanI4xgogZIsjNMtWftTW+3Pqj16rhvZx5YrLwqP8sPxw/qV8E0x9//IGRI0eWno+55keG1uQjMJBE2R7Ivyz67EJmKJ7HdUUTa3bjZEVLVKZoK/CTWc0M52xCULC8rGKwKNLg5/pIz/pwW9dK2VgdrtK/vS0VLoTe0brbD2aiz8+GZVqaybMTb/UlETQGHI+zopEUPxWOPGcDpOSzrsz6Bsem/6Q0USV99hfWZ03BsJqqpnYl/ZVLhforFy9exM5p/bChzfs+C7MI88uUzthDJmiMmcEx3wvzOfP8nQ++UDVzR1hYGGZM+0px8vQZJiXzKeLRIPJPaWKL7E+795xL4McBOkaRMugw/aj+TnXRtsoXzCU9ShkCKcIobLjzETNTfd2UXFswf47s+4U/Kg9mEAPJVocKoVCYW8783U7+Md+d8eSfavg+KaQH9wgY/GHm3iT2EqcOd81YM8uDmy8he7VKlD9vT8OSLWSTKZJO57+9UJTqtV0wH859M/pgVSu92yXaxCvVzbokRGaoI5bZqR+4bpwZhuHBzHdH87GFRFTG/BdRyXxQbK3t8W3Li5oZjFRzI3YH9lz7H5O+SKsGspcaTs/RH9sHcYdx7OUStTpSU1PRqX03CJMt4WfXCiKZAHfi92DHri3o37+/Ek9oaCiOHz8ONzc39OzZE97e6ielJBIJmlbphd5VFirxm8vFm5RbmLmjNRo1aqTWZCYVIpP2OiQkBMyet8HBzPxF/hEZGYnAatURVGEkbCwckCgNQUT6fZz+9wSaNGmST1jgzL9sXQyvsalAiW6noV6/YN+B3UUyMb9tXSZbixTIkuB54jn8cnZMTmouhqWijy/GBO7Tyr3vwae4aWWjluYMWeX2o2UU7ow0/iTuvxES3KwxEYt+WqbWFm2FPbp3JdESV/HPGv0Gl7TJ1qXu9NUsVK1kjQBf6yLZ/JtG4Z5tqQlGKNJfcyP4RPoGp29q/2YY26eLt4VoPybWrNtdxsbIxORb+rlapIeMcnXMtavitmwkZeT3zWcHOWN+M+X3w5J7Esy7mJqzrUODWoG40SlR5/Zhrj5z+Ft2U5oi/a7/B/Vcb7r6LcaNZ/qr7w+mjaB4ViX3stj+2jZ8O4fsCrG42BRSRaUWgYrONeWjG279oH7HpfZmFuHYyRs9cMZJ/4wybTNeCJ7Ksv2JmsQiVBmzerRXGYu1v8z2dP64u2qaY7aKz98UomZVa/h4WeHFWzGqdQ+HpaXqPFjnYFf8u9kzT6yITHa4B0fsEgrlI/IK1Z80LuPmcsLFzd1r9uw5vIEDB6LMf2loT5w4gSFDPlZkZvJ7EdYT6tlpqZERYG52/zKWDuMyZNnte7jWFo4tE+TS27UOibTPHac2zAKXRzMV/7omGeXdOlMwRPZg5l7VB1ZPkyWbA6V9457oP3hG9F7p+bFUsU7/VDKiCzEkze07PT0AOn8WK933orrePswlk60bai3QW38u44wX2fKP7CZw8hD1FNSTZs98nOOTIisJlfbUxcOPVV2UkUwsVXbLEZ+SnmtGzl9m+7iKuwYo7W+rRMDhRfNTbnj08i127dqFQXemcii5eEW9yyRbBW5OydmSkI3mBw8eYFW7nlhtqf/WX/7pLzG1lX6LidjYSGlMB4Hl1zoqRDLt2wgVk7WOZE6K37at5sVFGRkZcHV1zVlQUNgmMqlLto3QP3tJYXn0Wj0CRmlAqFfFXamDHS9LcM/fqPk/s4RyfPVjEnadEKBHgDW6VbBCa/Lv8FsZvrmknO5OF89u3bqF1Z91wbYOnLWv1KoPJymaO/+ZhYeOtYscLFyRnQDLptr3Jd30+GPEpUYo6SrnVR5jauyBlYWtUnlxXbxOuYEu090xZsxojSqrVWiMIQHrNdZrqrgXcxAnXikHMMybuwD3/soikZYDNbHllR96PZvgbom+VX/IK6MnqghcjdqKcMENDKv+e5HPaTz/NZ5abSR7Il9RElTZqyZG1dqmVMbm4kn8KRx68b1G0qioKAxoObnI6FaNAjioWPegD1IzEpQk+ZPU2cOLSJ299mp7hLnWUOJTdzEtOxKNmgnxeX3jZir4I0SMuOAZmPf9QnVmsCqr5eOC6wMscTVaiosxUmx7LkLjmrZY9p0HGpC/xjx6fB6LExt9NKqY3ywLX9mU7ASzRuNoRR4CFySZ6HVFDtsSjHZsOPBd+sMXYlbp1fIMpyfFiQCPRNUmx4x3c1e3RcVX/D74bct2PHz4EFar2iLALb8tx2Qa2FVrEYJbtcHQrkG4PUh5Mrc4neBKV4ZIgZ+dRmPpilVKImfOnIER8ZtRvUy+/3470xF3zU+JrrRd+HVJRPg77anm5s75DmvXrELGbe373HOFjXOz8N18gXw4V/KonA8LgbKOVeXjGu8yyfGA1ynXMc/697wbEvwkElOC6YBmHiAGnDBbuVR/MhVDbA1LI9ow/Tk/Ui7uTEy5aYA5bFgHdwyy23p2c3n9Z2hZaGkyzAp3H7xUouwYXBbnNmtX+/y1GMwWXdUKLPLcfH0WyFYBSrK0XZDtFxRZWSYxiKvNTLOv2+roq3jnUkc2r1z3/AaMkb3KIqnGuzw/KfvZ6SDnOq+IT2DB+Bbg2euWHUqTy1VcV6DG11M0VbMqPzm/ExzHVGdFW5hIQSYeo/i3SKYa/eY7reu9VcS7NND7m9ZBmCB9WHO26oxoYUO1XHs+/EF+3O2Nfg4UkhvEL6tw+DqeJ1/piZTx6oe/mQWlCWn8vAnG169fo3nDekjhC0G2dIHs24pk+zu9ISlkkebLi1ES+C66jZo1a+YQTWvqjCXB5tsXepgghXzKKZVAk1wE9uzZg20Tv8Jei/K5RXr9HSqIRtMWR/TipUzKCMgVMhy83xMHAt+Py6VJZTjs8jt5/tX/dpS5i/9qGZm8FZfQ5K2NNR5mP/CvX2uADCEvI/R23s3VRZCekam9kaS3dMrIIGD8tzeHODs78G5k3PFvwaHIPFFh7yQYMCYWNwaxC9ydeEGMtAotULGyL/bv24O7H9nAs9C+eXOui7E1RIq2fo5Y3UKGco6cfLvzbC58sueFCM+vOmGWLfsPx7LsRFg3PVBYVN71b48GIjw6FKQjkVem6aSF32B09p2hqdqo5ftfTcevuxeppE7u2JZEdGKhzrq3PPsEMUlhmP3dHIQctkUd7y46y6AMxkMgLTsW10VLcO/hbTRr3BJdnX7WWdn6B32RkhGvxJeYmIjAKrUwudFRpfLiukgUhOG3u+9Tcxel09vTB+NqHdC459a6ax3wxoV9hylEJsRC5zAcGVj0b70o27TVS0lnbOJlBQ6/EqJngAO2tVf+DDFR/J2PAeXrtICHV1lcP3kAtwarjxwurGfODSGqdnHEpKHG2R/Xq2U4EgtMSkgkCpxtbYnmVrSdUvhemOp1slwKmwPp8C1fcp1It6CwU+kZiu6mitEHZperm4tTTFoG34FNarAUkjq9jJ1x23Kmgv8yt8mYv/B/Ws0JbtIA51tGKtEEH8jE7ZOVSKSx8rtdiciMLmQykqpsdg0cOZofCLVh/XrMn/c1WjV2AV/qj7MXbih55O5qh9QbhqXnUxJYxIVjkzCyTaPCuB/vImyg1WaFQJX63t1f964x36R/pHeiD2ClC2kQFjo+e5OANnX/gbUeqQcLiSo1l4svB8PBwg6Tgo7rNDiZnBWJis9nYoKtBydYXJfwsdIqDo/FQrRtYIeOHezQu51jTur4R2Sc4pufUzNuPRGmpmcqviYK92tSamWFeRW8rb5+fbKSc3F+S1ZsS8PM5clKZoUcrUSicdn1QwoyPggRoXr3UCa9YMHiIs9dXJwyMjMFxunIFKm91BP0TXZvcIjxMksuxyBJuuxm9a85n0jVhGKftxtlcyxucK5PTCaFO3c8CasafTWpZl0ui7qO3jsas6ZXR3i0Uh256+W2ejeWxyw+iSXT9XsnVWgRKX1iVUevSdczJA38kMAZ6lxiXebxaJ7shGsUJ/e4Pd9FEfGpC89Zw2Ljr65I8dv9/IWE5TzLIHzYe1Ob7JfiaUwGAir54OkAEWv7CxMeeSPGnCeuOHnuEgICAgpXa70e39gNa1vp/RholV0clcxWN6fDJbgQJsPFaAmaWDngf7YVUceKmwnAfoIotGp2CJb/bWvGxifmm73vwWiUd6mDfnXXsmH5IGgYXGxjpmJcWdV18b9aL4GbnebgC1MAaMPtwYqU7Hdc/FjalXG1/DY7Wx6UJVK4kEAFMQl4lDrZ82R1qtni4+6OLiP7qs+4WLZNHEmVfhSrf/4Rn/cKwYxV1ngRqtzHz8Xqn3/+wdjRQ+HjbY+IaAHZalLCVOnap2hHeGa4ujrW5vOzY2Qy2VJyfZT8o0chBHQFthB7sV32eXu60mH/itwPsvq0CkfYCPNtF58iH5IrZx0wR4eJ2ty71ijzDUYEn8m9VPq798WXeBV/K6eMrPpEm5Yd4JReF60rfZpT9uPlIKUQeWbPzR3j43XqKCop5PBCTCaftpJUzgt+nI0nj57BO3QUh9KpqNKCwJKrrXDy1EkM6DcIw2tthKeDn0m4dt1qLs6dO5tnC5N28btWF3Mi2p8mnCFRpmtx49ZV1KjxPpJWW+rmrbcH4Ym97ilhn5Pf0DL3MPzV13zHgBvvy8CTs5XJqlPuP3P9Jsdh38qyuNfWBjVMdOVe3gNET1QQyFLIkbU7BYF+ug/CqQjToyA2UYry7SKZAQWZHuyURT8EVrm7uk5q3Lih9dbtO3mVKilvFbGyrQMmkQFmerxHgElv321/Clb+vAJtHy2Bux0Pn122QJOew7Bq7TolmEhaJPzS1RNTGypnPIgVyPHFczFObDbtjrKSM3pehJPBnK+W8vFRF2v0aucAFycu+t26GfMyTIwavd51JFw0FFE36D4oaj/XhvLh9Tdw3zAyEorXHvfBOn/1EWSHUzLwyv0HVHCpnaddrpBCJBXA3tp8+/Z5zuhw8iD2MHYmncrjkJEFkJ8J4/DGwRedq88n+6lrngB5EHsQNpHbsdmB/aLvPEV6nMyQRaLZVAtM/MRFD27js/Bqv80Z43j58iUObGiNOZ+zW9CvybIRszJQqdZo/O+HJTnbJmiiK1g+7rOxis1bthX/h6SgEaXw3BWW59661+1Q2LUqGS8VafWWF9t70e3Rd/LTrnGc399W5UZKbfvv1GvCsiAm2fsGSAe82qO3HGYB9oXLo2BVWb/fjm2Xg9KIIyTFoR5HnwbJ2O7krwcn2YYq7Zkiq8EqvZ+DSVH7MEp2Ui/duUzzsgdjUf+rqMsiw3SF7WIkp/PBpFqv/vdQVPiv7dnrQQ2cvXw9J8o2ekoF+LroPofc/pAUNyLyJ4Rz7dPl796eTugXUDJ9bV3sZGi/uSiE9xt3TLLRP72xLjqbZb5Fq9rLUcm1nka2BMFrnHw+B/0UEnxvr2zX4hpLSTvHNL+hGh0ysCI9Ox6udso43I3ei4nWhxFgr9wHzVXV+NFbzGx1OffS5P+uuN5FkS3NZPttqORgz3sZf8nX3slIwYGnItejW7duSrj5VXRF+BlPpbLci9YjozMev5SczODL/yRlHs6OFm2yRfKhg7o4Wuw8nGZFjlxSrX/d3dwUaenpbHHQKqs0VOr9USom5x3nT3RLWTi5DOdve6/m4Ygaa54dOiYFn++vmXjpXFev2yAl/AMda6Bd1a/04qdMFAGKgPkgwGx43y1kNlpa6xcNOjE7HLMHypXSYJqP98AnpwX4a1+FnLRlXNrN7H31/BNbtLPWr0PKpS1Ulu4IRMnEqHI2C6QxqTszBxyOjcOys7IV3CzV5cAeMxcR6GRne0yqQMCSn37iTZ48mfXAKOM3s1q0x6XPzBYCPtlb72bPP9CrVy8sa+OoMnlqDMcqkoGiJDJQVPDYsnkzWl2boXZwaC7JgtDtczd0CtIt4qigfHrODoGfNqdKZq1KDSDUkew4KNWHgICdhd3rma0uVDU3X7Nf98WYstpT+DITlMNfJ+JNdjau1K4IWwsLvBOJMfhVIrpUm4Manu3MzW2d7X2VfBUbYnbrzFfSDE+kWdjUIga7VnuXtCklol9KGi4VOyUjLkF5D8pcY5wc7Z4KskT6DfjkCqF/8xCwBk8c515fYxRI5bTnCn6DlcUyPno8/SkSE4eiplWjPPu4OAnKrqxwmBZpsA+CZZ6KweJoveX87VBe4fWsv178CXX2K+R3ffXiPdGchxZ69M2zSeS1q+8oOFqqn/wp6t5kk37l4fC28LOsXhSpSv1+0QYEBC3E6NqGD3kzkbELH1hiYg0pPqujny/fyYZg9boNKnbqUvC/hd/jmxTTjgQN/iMTe+U14KlDpKsuGBiLVkwWn6+vt9FY4k1GLpPq+HH8SVx4swKX61SGHWnbvckWYfSbVDR1ssZKX82L0XKdYNqGPRsez700i7+M34uvtFpMjJ1TyGBLZ0fe62Mbyvm1aVI8Q0hOTaPAF4iVzHh9sjICCmwFoVSp5cKjZTySU5XHDrSQ51S5uLgoMjNZT2IXJc5s6/X6GBaHt+U8LQWxl3w5H91pMzQaZ0hEhTkebXbzsUdaHe4GfFiqZIRiSstz5ui+ydmsIB/MDbd6IE2cgTltrpucfeZs0OLLLeFoZYsxjffCxfbD7Mhzff8OPZ2JSwqBQWKrZT5GxAQXEvVrsp8Ojf7570hH7HU/jfX6VsxYnIyZx31hyyuZyT997aZ8wEZRImbd0a9DywV+JJqjKZFzlwtZH4oMe1ubBy1aNK//77kLPEtL3VeOF8bp7du3kPzQUO1EY2FaU72u+qcC0YmpOeYxmRfYpHnmwpeggwo8iEzN2Y/+xYsXqF49f6CqRhVfnGifjvJqIk6DSPrkf/+qAE93w+8fF34YQ8aR8wIM+ToFFXw80KuVBKu+K/5Fos/IHovNh7xbKxDiS2P4SGWaDwJWFrZykrHFZBpuZP8uZIqSIFOIYWflAgdrN9KufD9YLZNLcD/2b4RGb8WxmtxHfo4lqZXrVF0JH+f32WrM5y5qt3Tb9S547Gx2c/JKTm0RJyLoVwVJO/9hZr3waReP2ATVAc05s79TLF6ylOkMJykBRi90RmCMjadwhWPFIh+w2ukvZGF1l1jaWWic39VZtyYGp0czFOdckzl9Pwfxy5F9UOMMlml9pq2s8+0zejXWhNExuCNepMltreXJHY/JJMe9ddbLtHt8RquPANOqkFQaOmHf79kd6SzH9ezCyIi+59J7OOfTG5u76exmUa7oXe+0Lg1SmVwt/8zp07Bn+yZ4OFojU26Dfy9d05guec6sbzGb/ztJ+2vwI6jWFkMKNzwQodqD8noHMxiim0ve6vxITCDbIZSm41nCWZRJ/QXTy2tfqKePzy2fRqJDwGzU8uqUxy6RZSNLkg6hNB22lo5wtvXMyWyYR1DCJ6tvdcDrM14oyyLq3limhoaLEdgjKk98+XLuiDmvPvtNHpGWk56T0nDwTCzZs5z9AhVmX25nZ+eVRKxhueu12GXqVSb3JiUh3tGCu/6c99IySZq2v+YmYFiNkhug1fVh2PtSjHnnRXjgWJvsW2nYrdKWCllXu9TRX323BYEdrbHx9w0QCATwKVsRUxod07jfpjoZ5lLGpKrwTvgan3jmD8KtjEmBT/Uj5uKCSdu58lpb3Knnn2djJFktv0zQAS19zTcSKs8ZNSfbno3EPyd2oVmzZtiyZSuWz9uMwdWY75Jxjg03euCVk6/BwqdkR8DJPwtrOpnPIpgMkQJn/KwxvA/30bEVgiPxxKKOwbhSAcWLQO3sp4i9W7l4lf6nzatVuCApVa5fCHyJWFwiSl28PD3TEhIT9WoEnT59GpPHj0V0fCJcHO1Rvqw3bKyt8DY8Auf6WqNaKZg4bHvBG7cev2B1c169ekUyD+zF4h9/wKFetmhdwbABycgvboKR2fbU8JzFPP1PyXH0WVJepPOc777GPOEWjbZV3JqGhBt+Rkljr1GpESt6TCeT1ae0ZydePtMLX48t3pRmc9ekiJduTZstleJnI7pPRZsYAhY8S/63LS866rJfGtcubLzdDwcCnOBJ3rumdGTKZJgU548eNf5nSmbpZcvJF9/jtDguZwGNXgJMjCmDRJlMq/MWBzcqp0E0MTONas6lO0KQfejQtG7+/OLu43z55wsS1pCFONONqrwUC7/oHKioS/alZHuM5IfLfg6YYlm9UEpOtvxs6dLJtkQfh9yRL3TaytkK4EFZzSWpM24Z1MhTSLPRov99eDRvzNYVJbpDXlVkZW5313lWkr/6iYI/Qahzu/+nTWkYv0O/Pl2Ed1c0dFDeOkXJGS0Xdg++VlxyL7qfkiFPxWRFTTwabdBt0WKJ7lUSmQIVdkhBNtJWYY6NjcX2IdUxrYHm7/fIc3JYBASTrbvu4NOqQnzTmP1kjIpCIxbU3ZqBm1Z1Ss13MprsY72YbFsYUCbYiKgVj2hm28VnDfMX/haP1qK1PM/KxkZhK7Twm1w0sZEo0rJjYRE4Gmtm6bcYxUhmcSo2Ol6KS3cI1n/LUa9Jf/yy/ne18j/kqFudP4ZqEeSg0M7GYl7C1cr/M0aqQlPct/Z5sgw9D/DRxcIVQ6w80NyK+3FbZj+JKpmvMFWHyNqkrHD8cX80rpPVulclfMQ22af17hbe21YT8azWl0xq5YomO7WVX4/YgpmO5+Brq9oYafk0FpODTmtjp3UsEbgWsRXrylxWoWbSgnd/JcboxntU6sytoMroEIwZM0ar2dr2qs1lPHSzF045VECDzNfoUON7BHq0zq0q8u+f98fiuKUFvI2wiviNTITD4jT8Jo3H4nZ2GFnLtBbLtP0nE7dO69epKwrY2s2iccWmZlFktN6EEFjcIwwrZhedYscYJpNoW6YVnmwM2eYs087a+sL9R4/a1ayp+29p4fy5aP9yLZqV0zzIYM7YFLbdYyMfgmxx4WLW180a1MXlttGs6QsSlt8qREqmMGex3tY+ZTGuzvv2UYVtIiRnvM/sMLC2G3Z30j4OKSQpIuvvz0TkRcMXFBW0rzjPD5zmY9C0eK0qY2Ji4BjVEq7O2vHQKoSDym6fx/Iv3xWuF2bjOw7EUREmioCnfSX5hKZ/lUhf/1rkFqxzv2KiyOSblSKRYoVkKOqW7ZFfaKZnMZnPcfzxVDwkfXibUpL9ZWjVUJzaUc5M74hxzWb2MV+2LV2481Cmtb2dxR4SoDCXaIw0rlazlh6U7N7guj4e7BWlwMmrnXxkmWZG/Xh3eLVGtsSa5Lbl6NgsXIw/v/oGPAOy9Eke7lD0PTZEr+9I3OnzCA38W2dvJE9SkFzttc58dftGyS4l19YZvzoZodKYesv06jRUe7pIusvplVbebpleiJ2UvwBDZ8eMyPDwkxNo0aKFkoaQkBDcmB6EoYFGfdyVdBrrIuD3DDy2r2ss8cUqN45kImme+QafNz9E9rPNDx4qViM4VHb4+bfYU9H0F5wxi/w2Wc4psQwtS660gfSpfgtKOLxdxSqq4aAkPHiuum1EREQE/Pz8FhJjvi9Wg0pYmV4fYI5trnxgtXf4wM5OnNtCNkDGyf8lom9V1Uk2jn0oUtypcAmWnyERCPaBRdJyTTAqKwbPLJ1QhXRI3ex8yAojC0SnPUB06g1UlYuwwMYN6lYdNuSHYWTQKa3mXIraiLMvNmoNcXd39sLkRke1yjGVyh33h2FimSxk+R3MM+mPByNwrKqFxmhnZnJ8r8uOPHp6wgECb/viEy/NqTFaPInAl8EX8hQdu98VYSIppgSdyysz5ZNzEWtwPVz75LO3W2V8Xv8vrW4cfDgBly1VX50JpFG3SJSMg+J01PbqgHJujWFv5QxmtVYs+d3zycDOabKatIwBnTithmmo3CBKQJRfsklE5zKTBMcrGSfqNiVNhi1dZfjUuvSuitNwi82y+DhZYDDidsl0TN1bhIWnZSr8zRI4Ixjt6OAgTUtPt7Sy0jr+oaI5KSkJfZpUwfm+uvGpCDLDgr9CxRh5km+w5V81c8FPQbrj9zJFhglPK6NBk6Zokngaw3yFBtkSmirD7LcSHPvdxyA5JcXcdaoPTp+9qqKeWYileFZFpdxUCnYezsSkH5KSBUJFNWJTqqnYRe3QDwE7Syf5zJb/qjYQ9ROnExfTL+qdPhLOHKSw10mxAcRDIsugb23jZbkxwDS9WaWkj//L9W546lINLjyd5zH01ss1Y5JcitMT4jBlZPFmKODaj+KU9+NvqaJ5v6Q2IDpfFKdeE9Ll6gbLg/WtHBpvd/Jz4er5Z6Lb/sezle33/9SoPyiPR3NlJ1zfcaIjRhaOYR/xYeFdR+/bk7W+lnxQ6gO9OkqHA2tI3E+TdHw6HsfuXkETHfdktWsYpohxrK/zd++Yc12M9Giuo4XAruTbCMhYrzZ6c7AgAPfHZsPJRmdzdLbDUIbpV8R4lmmLkf7ZGFbTtBba6+vb4MMCbEuvqXH8Vl+5xcn3kOz5PpwEUX3aTPeFD8Vppz66Vl/vgFt1jRNAoY89RfEcT8lAhu8/RZEZrX4FwUv4yDz7xYaAcuUe2eNnhRPW/7YDZG9bjBk1BMfWOSAyVop+U+KZMPMbhsg3F94S/Yo0q2vLv7W3gqMxwKrXKwq3e3MfvaqLreHpZCBrnwz/2DPjH+Z3jMqKQ4Pm+ZOX2jxovyALHTp0UCLp12cgaqdNN9kUyWde/4SYlAsY7mGHwSTVMbO5ub5Hh2fvMK7FWX3ZKV8BBLhIkXGMfFj3pIiRbeWLAXXWkGdQ94HoAiYZ7XTT8wGIS4xRkT+x6V54OviplBcuWHe9M944BxQuNovrv8iq5fSGifiyhPeuqv5HOiKu+hkFs20kojdohbdRopmNYvAHLLSbewhuH61Q7Ags25Km+HZliv4fn2K3mDOFzOjras8y7kMGDhpo99vGTTq3R+VyObzcnBA9xjRXr3OGFAtB9faKERqvfeL2zz//RGQk2dOHtNWYdPzqjnbBzXCqqe7RDepkcVG2n0xIx5IBpG8+c+NCnNFlnLyShU5jI2Ft/X58koms/XxYXRxdbx72FwSo+/jYrFPXhEwHRrWRUpCQnpsUAjYWtu+mB5+uQPaxLVG7AmI+QlNn9mlI2Ri7m+zdXdnGGq1due3fvxaKsN9mJvzIAsfSehx99h3Oy1WjFszJ376SV7hy68NNmazPvdryT4bss3lJTKhykj78ZsDj6c6zPBZs5VR7o6Ovk30xRpjX54fLo+r8aLT2+77Ue1AkfwF/qxqc3IbWdX+S2bT6Tu+JYOHqSrKB/Nc6879csU4ePzBEZ5w6TDmKP5fr9nsPbP5OctO61vsGGEvUOmW8kt+vt1Rn+xjxK0K90domP0vD3OxBWNL/Oup46gwTS2spWVEIMJn5fthii69tzXeCS6SQ48vyw1Dds01R7ppt/cW3a7HB8x4syYJWLo/TqZlIIBlURnhrDv7RR9/ymGRUrF6yQWgpwigIK3yKrYu89HGh1PIs3ZImX7QxNT5ToPiMOHmiNDrK7a+EJUIOtrx0wX1/oy2X/DBzqugAAEAASURBVGjQO+zsbJS5YFYe1tvG5MwvHWkY6mbFY2xz7atKHjktxZHj7yd3hUIhavo2w4iam1hhVdxEG271xPmanrDm+ANR0I99SWm4QaK8xQrAm8wVlrWSw8XKEo5kUpj5iISJLXAyJRkNy/VAl2qzC7KWuvO7MX/jctg61HdyQkN7BbzInlYEFqRJZUiU8pAgBeSkoLY9MNzL1air8tuTifXxJjqxfjV6C5bv/RxNmjTJeQbOnj2LcwuttaYUfxx3FJsSjsPKiM9ycT2Q9flP8Wqic3GpU9FzJVqCJjM84O5qnE5Wgx7vcD6jlopeWmA6CJyTZGDwrZKxh6RI9iWaI0tGu3G0Olpjc30vy8GH+zi7qFtlfiNGCt4XR9CuXTslA968eYOmDeuhk589zoULER4dB2fn/HdDeno6qlT0QdQYW7NePa3kNIcXP96RIDagN37f9gcYrAL9KyFqlI3aKIAkoRyLMQAbft+qYsGRI0eQ+utQfBxY8plqco2bRiZEh04rg9aNSYPBBA++QA6npmE5ljVp3ACnf0mDh5txvinF6f6tx9loOTxmJMkO9kdx6qW6dEPAysJG/l2rSyXSr9dkKReLMBnZfd5a4pP6W5TUbLzdBxdrllEqU3cx9k08alVZgQoutdVVf1BlCbf7Y4k9M39nvgczIL9pRBRmTTC/hTAliXr1HpGC0Agpt6sdStIhoM3lHT6XZn6aLjvpHFiiH9pKac8VggYrjfbudXw4XXHeLYUT+cFWLWT2427qjZdXzDg03bpO5zt/uFNdufumNjpNjGa3OihNO1tBp1X3kxpl4keybZQuxwJLF8kvlT7SaaKXkV/38Q7pZpcLVgdEvyIg6HuMqm067WVd/C9NtD3283Eg2/y/9dUzXmFCSxoM9Dj+JIKyt6BPmaI/XZ/F10DHgO+UHudV19rjdj1miMWwo/aDl5jbxnQCOiWybKy/2wOZ9813YYJhd4Q9dyLJBla9V5QgNV3OvBgi2HOaFiUnDQC2LjnYY13qdf8vbIyUJiIuUYr4zWmoXkbvtghbV9TS2a9NBdkrQ22dORYeISkjrwTOR3nnmhrNPxW5CHfCjmLv3r3Y+/0LsidQN420JVmxhqQUuGnCKRhM7WNgyL3iapDGEBu08fYKeYdhzUyzISSWZeGZ62ocO34EDvZOmKHFTqEkHe/uj8AGHTsn2rAp6bqlolh8M0oER+ti/TTlub2e6J0x1ngDQb5NovDAzvw7E3mAlbKT9UMjMG8Stysz2UBUJijsWGqGojcbWlOmsbNEeuJENxcri5L5/ZoyNqZsW7pIgcFPauDyNdUOqbODHRLHcRstxwUWQQcycXpvBXh7lEx7nwsfzE2GRKKAW/Pw9CyRwngfSXMDpYTttbOwDR9af4NveWfTXxTGpOoNSbyABP4LkEThcLQth7JOgajoUidvgaJcIcO518tRW34DM8q//xb3ehGHYU1PqUX6beptfMVbB0dL9XMBTR+HY3rLi2p5P7TC32/2RohjxVLjdpDtM7w8V6nU+FNcjjg0DksRZis8ikufEfXYku0GsnPl7yNbRPw5SyTb5uRfYo2Cqukh8pR6KyyYrRC4PpKkfHwRGqGY7rDcYOEtMr0Ujt8k6iVHnhaBLosdYOOiW+zNjY/GSiRL7XWaGI0P2KtQPK2ik52hwfbwtGCvZpogEjtq/0/n22X/4BtF03KWvDMf6TSnrLMeysAeAZ8N6ST7XD32DCZO2cXaG91rLDRxK4vHvPOPemFTFU+1yi6m8/G27BaNe/xGv+yDmeXfL/Ab+yYBzh6foEWlEXmysqWZeJfxFAmC18gWJ8GCZKnxdqqOmp7tc7aUzCM00ZP7sQfh5L8B+1Z5m6iFpmNWyBsxavV5159YdMh0rGJnCa/smyEkzo0ebBGwImGBUjogyRYuWImtILUh4ZT0YIWAdZY1JA4SVrSUCHDJlCHDucT6Z2Z3CzzIaqPkElrUYnZgEYPLKcSI49GVs2zvnU8cifIrx76zzFZuaaXzU2QjnEdT+7K9v/5pWQhzM70JRLb2FzddQIoAr8s4Frdas9VXQyHEC55pRvGaIqhuiZnyNC9n9bNlpmhwCdtExvEfkCDBhiVshtmo9wz3RpJfgtnYW9KGeoeVRYJ/fEmbYTb6G8kFuG9Bv4863LDrhJbZP44eLBBoEOKIhzUFLCgpCUVAdwSaxaTi9n8Lm3Tn/vA4mtzPwt1GtP/I9s77vKiA2BrRbMk/eDqKl26PQLA8A9ctdFv4o5uG0kVNdn34nna2S9c9pd6YOQJMpCU9KAIUAYoARYAiQBGgCFAEKAKmikB5Tw3hjaZqMLWLIkARoAhQBCgCxYAAj0Rs0YMiQBGgCFAEKAIUAYqAoQjQSVtDEaT8FAEOEbCxpKvAOISTiqIIUAQoAhQBigBFgCJAEaAIUAQoAhQBigBFgCJAEaAIUAQoAhQBigBFgCJgFgjQSVsdbxPJjkwPioDxEDDCPijGM5ZKpghQBCgCFAGKAEWAIkARoAhQBCgCFAGKAEWAIkARoAhQBCgCFAGKAEWAIkAR4AIBOmmrI4oWdNJWR8QoOUWAIkARME8E4hJl5ml4CVmdoJDQL6QO2L9LlNMHTAe8KClFgCJAEaAIUAQoAhQBigBFgCJAEaAIUAQoAhQBigBFoLQjYMXvdzODKycVcrmTi6NFsU0EZwnlsLficWW+Vjl8sQJOZBdgGeOeXK6V1pwrBQoFrCztOXNBYmMDuVjMmTxjC2L2lLUm0a7MUyUmWBR3umKZnT2k2UJju1lq5MfbKsATZoN56YhK4H6ZEpBSWTYci3gd8p1I+m2+ee+bnE3us51N8SBv5eIAmbD4fo/8LLlCwbPILB7vuNdi5emSLU3KsONecumUaOXtKpQmpHP3wTUOTDx7C7mzpUURLxfj6FaSmuzjBsSmKZXRC80IJFZwB6JTNRN8wDXkM5LTxrOzze+yJFTxBO8t3YeO7WMRX80LvFeJbMk5pxMI5Qo5LPhEsFksFrKr4uMtfB0tt+LZ8CwsiqnzyjnqxSeQX8UfordhxafQzDVlBARAEPoMjubxczAK2nzI4eyQ/07XpiS6bgXwnjzRRvJB18lIajmBCAIyIpKzuNCxtn9FwbMwzsYM9QVXoVDYWlvBxt7GOKnJMgVyhQPPgoz4GXbEBPoB8x8VKSRTIVe4WNhx1sDmy0UKe56T3vIECj4Z/NKRX5KlsHJyYK1Tys9U8ByVv4FOjauCf++NCl4KgVTh4sCuAyISENNZPhZynhUsc0aP8lWKeNlk3I+1G/mMJXAWVdcXePKiBDSbhsrccXm21sTUrAosfMyW3KTpmAZvNs8aFjxLo9nJryeD6DH9PrIFmF9fDtGj0vF8yeRk3oYnhaO9oV9BzehFBQeAd/2mZgITqPmvj2kSY7IuzWsFWvV4dM/FBHAxGxMspTLIrIz3kjQbIFgayhPJoSgwKMaS7YMls8iSQ86yw/nBglTAcctM8nt0pr/HApBoPXVOTEWmFxnIpwcrBNzSE5Dm6s2KliMiprdovt9kHi8VCkWxAsYR7iUihsfjJZIBqLIlotwMlZZLDEecl58ZWl4yJvvEv0FsWTJQQQ9WCFQSvUGULcWLFViEyFf0ChG21diSG4OO+V46G0OwMWTavhK5iKrZGkN0qZRpFypCdiDFi+3Ntb73FJLGddiSf/B0AcKneG1P8SriQXDMrVfweE95CkXl3OtS/JeTWbuqt57iTXNWzxcn+grcD0Pl6cOvK48KfXr2G7jaqW1/qdAW8PWDPQ18exehVZp8sP7r6ni164/xKriermwfLL39oywI65NAD3qwQsD+oRDCBqa+Bp+VK8VCVDvrHp45NC4WXQYoYb49JjEmy+NZhBpvCt0AhCgrRYAiQBGgCFAEKAIUAYoARYAiQBEoDgTeREqKQw3VQRGgCFAEOEegAokcowdFwFgIZNy529xYsqlcigBFgCJAEaAIUAQoAhQB9QjQSVv1uNBSigBFgCJAEaAIUAQoAhQBigBF4ANAoIoT7RJ9ALeZukgRoAhQBCgCOiLQtLYNTWulI2aUnCJAEaAIUAQoAhQBioChCNARCkMRpPwUAYoARYAiUCoRENHAhVJ5X6lTFAGKAEWAIkARoAhQBCgCFAGKAEWAIkARoAhQBCgCFAF9EBBK0/RhozwUAdYI0Elb1lBRQooARYAiQBH4kBDIzIL8Q/KX+koRoAhQBCgCFAGKAPcIeGVHcS+USqQIUAQoAhQBigBFgCJAEaAIUARKBIFqooQS0UuVfjgIWJ2Ya/vheMuBp1ZyGaQWNEMMWyit5HKCF10bwBYvG6kMYiv6fLHFy14qhdCKhkOyxctTaIske/rOZ4sXUDkWUFRgT/+BU/J4BC7FBw4Ce/d5BC8FxYs1YFXl1nhjQd9fbAELlNsglOLFFi7UIHi9oHixxquumw2epNHfI1vA6iRZ46knxYstXnUTw/HEqzpb8g+ern6CDR550+eL7YPQ1NMGd5IoXmzxSvayxq1EihdbvCzfSRWyo7akU0QPNghYlrWGLJ4+X2ywYmhSSX/oBm2vsoULzWPI++sEfb7YAlYxXYp3rhQvtni1IM/XzZMUL7Z4PU6vT58vtmAROh75j86m6QAYJaUIUAQoAhQBigBFgCJAEaAIUAQoAhQBigBFgCJAEaAIUAQoAgURaNbQhk7YFgSEnlMEKAIUAYoARYAioBcCdNJWL9goE0XAOAjIFVLjCKZSKQIUAYoARYAiQBGgCFAEKAIUAYoARYAiQBGgCFAEKAIUAYpAKUfgniyrlHtI3aMIUARKMwJ00rY0313qm9khYE8nbc3unlGDKQIUAYoARYAiQBGgCFAEKAIUAYoARYAiQBH4sBEIzajy7MNGQDfv5Rnln+rGQakpAuwRaFaeTnmwR4tSUgQoAv9n7zzgoyi6AP727nL9Lrk0QgiQBKQTeq8qXXqR3kSpAopi+yyAFQQLRYoiKiBFFFABkY703kFaEkogIfVakmv7zR6mXK7tXTbhknvLL9zuzJs3b/67t7c7b+aNrxHAO5ivnZFyZo/AYixnLcLmIAEkgASQABJAAkgACSABJIAEkAASQAJIAAkggQIC6blBmQVHuOeOAJ2tyHAng/kFBB7qLAUHuIcEOCagNaRyrBHVIQEkUBwC6LQtDj0siwSQABJAAkgACSABJIAEkECZJpAjwVeiMn0C0XgkgASQABJAAkgACZRzAgnK1gfLeROxeU+QQC1j+hOsHasu7wTMFkN5byLn7RPsWcq5znKtkAKBkQY6oFw3ksPGMV1gOBaMPVDkxZ4VI2nkAwSYPSvjz9IEFyAuf74CsO2+RIAixtC+ZJCP2yJSAORqfNxIHzJPFEh4ZfmQQT5uijSWAv1tHzfSh8yTVgPQ3/Ihg3zcFBGxL9fHbfQl88TEmBxfMsjHbUkjnWAhOO6E9VmS1wHQXmEt7veC8nqEFwaw9fvroKQA4PuQZ2SRl2e85OHk/pXiWRl/lpaQxmf7MwAP2468PAMmgQRyfak8K+TH0hS54ePjvR9fANh0JIAEkAASQAJIAAkgASSABJAAEig5AncsYow35wHe+3TgXQ/E/V60al1mWCZuSAAJIAEkgASQQGECFx/KcOpoYSBu9nOg0QE3IphdiEBaZZw5WgiH292ARjiFyC2kIgLotC0CBA+RABIoOwQMZn3ZMRYtRQJIAAkgASSABJAAEvA7AhG8KuiE9OCsh/IqJnsgjqJIAAkgASSABJAAErAjUI3X4KJdIiY4JZAoPdbWaSZm2BGICMVBc3ZQMIFTAui05RQnKkMCSKA0CShLszKsCwkgASSABJAAEiiXBOJvCzH4tgdnNvtWzfMeiKMoEkACSAAJIAEkgASQABJAAj5MoF4NAXohffj8oGn+RwCdtv53zrHFSAAJIAEkgASQABJAAuWYQFpm2P1y3DzOmxYBVRI5V4oKkQASQAJIAAkgASSABJAAEkACSAAJIAEk4CEBqjq/ZpaHZfxanEfxaAttYdZ/96eNaW/eH9CP90kSncch7zOPSd4xFUAJaBNtKjw4IC/PKksX6Cha9r98a715eTafeYqIPY42smYzBTwixFTODBdijvPKMJ95+44KP6k0KSUCPZ3LSfUMF+bPQv5//Gn9YNpNMUzyeBQ+OZxUXIpKFDwpaCyuQyTnXR95nwwPJpI+80fTj9kQJjQwq3wz239pzG7eNZJXlkkjG5NsTbKWsybZ/JcvThjn7/8nkX9MdvL28z4ZEQf7j3UUqouRyZPL+/xPvesPBU9u1Fi0Aa6lMDePQBAv0JBpyRLmHeOnawIqXnBuhiVd5FoKc/MIhPJCclItaeK8Yz/+ZO6pPHI35pPbMJ/5JLdhyvr7TXL++6QqC0MhyZhKbtUFv+UlzYzYQW625J/18/GN1/J4n/n5sElnjpmNmMfs0RRNMaIWsk/+yNF/xZk88lfiW0VBhP6B6aG0xCsqJxVUEkTq7puSZOWkOSXejCqCyto7prvyEq+onFQQHVBFk2C8oygnzSnxZsQGxGhuG+NLihdfLKZlfD7z01M+tnpBVeBS5p3y0ZhSaEVcUFW4kJlYCjWVjyoaqWLgbEY8q8ZYyJOPibxkm8w0bSaf5JmIvHpTRvLkZiIKmGeicr/VEdbKvGK4FlTuG8pRA+sJa2deMlxFXix51hPVzbyUexl5seTVQFg/87zhIvJywIu84QrlMsqmL6JVSC04mnbNgTQmOSLQOrQWHElFXo7YOEprHVqb8LrqKMsv07R62kh6ibKdNb6ZuNE2im60uFQ6j5wZUebSKcpIeunQ6cH2xFGUnvDCTkP2vLIIr0C24n4vR0Ea6f4O8XsO7AEwa4RVYC/u75IUmalGV/J3Ch60n+kBq+qBvH+LUnCL3L+q+TcEj1p/nUjX8KiEPwtT1FXyPFHbnxF41HaKukR41fOojH8LM+GRG/g3Ag9aT1FnyfXVyIMS/i56igBo6u8QWLc/9iHA7QjW4n4vWP0BwM2Kfo+BLYArxgeZdQIqotODLTCAw0S0DXtxP5ekqH/I72M7P6fAvvkUdZDwas++gL9LUgdIf04Hf6fAuv217gFci2It7veCtQmvq8iL9XWAvFijsgpS1KyyPMnOs8aiNBJAAkgACSABJIAEkAASQAJIAAkgASSABJAAEkACbgnUCYi84lYIBZAAEkACSAAJIAEkgAQ4JYBOW05xojIkgASQABJAAkgACSABJIAEkAASQAJIAAkgASSABJAAEkACSAAJIAEkgASQgGcE0GnrGS+URgJIAAkgASSABJAAEkACSAAJIAEkgASQwBMncOmqOeeJG1GGDDh10eAXa6uWoVOCpiIBJIAEkIAvELhWiYSTxg0JIAFfIYBOW185E2gHEkACSAAJIAEkgASQABJAAkgACSABJIAEWBKoJ6p0gaUoihECTaXRJxAEEkACSAAJIAEkgASQABLwZQKC9ITvfNk+n7ONAgpo8g83lgQoiqz7jrxY0iJXF15fbFkxcsjLE1pWYOT76GEZPxYndy/E5cH5R14ewCKieP/yjJf1y8hcZLixJIDfSJagUMwLAnh1eQYNeXnGi0eAWfB5lTU0HmFlYS4y3FgRwOcvVpjyhbA7Jx8Fqx2837PClC+E38d8FKx2eOQN0oI9FKxYMUL4fWSNyiqIvDzjFaAygTFB4FkhP5amEqj9xJ/W0Y8ReNR05n0IZ9p6hAyFkQASQAJIAAkgASSABJAAEkACSAAJIAEkgASQABJAAkgACSABJIAE/I2A+axgv7+1GdtbugTQaVu6vLE2JIAEkAASQAJIAAkgASSABJAAEkACSAAJIAEkgASQABJAAkgACSABJIAEkIANAXTa2uDAAySABJAAEkACSAAJIAEkgASQABJAAkgACSABJIAEkAASQAJIAAkgASSABJBA6RJAp23p8sbakAASQAJIAAkgASSABJAAEkACSAAJIAEkgASQABJAAkgACSABJIAEkAASQAI2BNBpa4MDD5AAEkACSAAJIAEkgASQABJAAkgACSABJIAEkAASQAJIAAkgASSABJAAEkACpUtAEHxIXbo1lvnaKNICusy3AhvgowTw8vLsxFAEGI3fR9bQ8PpijcoqiLyQl2cEULokCeD30UO6CMwjYIjLI1zADPu1eFbEv6XxAvPo/CMuj3AB8vKQFwGG74/smeH7NntWKOk5Abx/ecYMeXnIC+/3HgHD+71HuAB5IS/PCHgmTd63caatZ8hQGgkgASSABJAAEkACSAAJIAEkgASQABJAAkgACSABJIAEkAASQAJIAAkgASTAKQF02nKKE5UhASSABJAAEkACSAAJIAEkgASQABJAAkgACSABJIAEkAASQAJIAAkgASSABDwjgE5bz3ihNBJAAkgACSABJIAEkAASQAJIAAkgASSABJAAEkACSAAJIAEkgASQABJAAkiAUwLotOUUJypDAkgACSABJIAEkAASQAJIAAkgASSABJAAEkACSAAJIAEkgASQABJAAkgACXhGQDBjXlqWZ0X8W5qsM22maeD7NwX2rRfwwGSygJF9Cf+WFPDBYDIDfidZXgZCAZgMJuTFEheIhZQlx0Dj9cUSmEREQXYu8mKJCyRiip+dg7zY8pJJeQKd3oLfR5bAlFJKqNbj9cUSFyjklFijRV5seQUpKEmmBnmx5aVSULIM5MUWF6iUfHmG2oz3e5bEQoJ4irRM/H1kiQtCg3jKVOTFFheEBfODHqXj95EtsHDCKwV5scUFFUL5quRUvL7YAqsQwg9OTkNebHlVDOWHPMDriy0uiAzjhyQ9wuuLLTDkxZbUY7nIcH5oUgpeX2ypRVXgh9xLRl5seVWuIAin2AqjHBJAAkgACSABJFAuCQSQVinJn5z8Kf77YyJxaIr85ZJj3JAAEkACSAAJIAEkgASQABJAAkgACSABJIAEkAASQAJIgGMC6LDlGCiqQwJIAAkgASTgQwQ68HjwTJCS1ztTbanfqLZQ06yeSFy3mlBcMyYAakYLoUqkgJW5SSkm+DfeCNcTjHD5tiH3zGWD/tj5HGWgnHddrbf8YTLBHqJoF/mjWSlEISSABJAAEkACSAAJIAEkgASQABJAAkgACSABJIAEkAASsBJAhy1eCEgACSABJIAEyj4BmUAAM0go5ykRoXzxxOeVgcN7ykloLnbOWK6ar9ZaYO2fWli2QZ15864RzCZ6Za4RPiX607iqA/UgASSABJAAEkACSAAJIAEkgASQABJAAkgACSABJIAEyhsBdNiWtzOK7UECSAAJIAF/IBClUlKLDUbo8sFklXjaiEBKJPTNn3SLhYblGzXwwZJ0rclMncrIMk8hJ+iKP5wkbCMSQAJIAAkgASSABJAAEkACSAAJIAEkgASQABJAAkiADQHf7N1lYznKIAEkgATKNgFm6qOpbDcBrS9lAlMCBLDgnQkq/geTggQUVXZ/wr9enUXPnJ9mMltglsUCn5QyR6wOCSABJIAEkAASQAJIAAkgASSABJAAEkACSAAJIAEk4FMEeD5lDRpTrghIpRLLvXv36E8++cQikUjMpHFh5aqB2Bgk4CGBQKVcM/7FcZbExER62tSXDR4WR3E/JEDWn31TIaP0+3+oSNOXYxcbzseKZk1WlWlnLXMap48MpEhbAswXYz8+/1slumIYXyMSwed+eIqxyUgACSABJIAEkAASQAJIAAkgASSABJAAEkACSAAJIAEou9Nz8OT5OoEeGo1mm1wuz7dz9+7d0Lvnc79l5xoG5CfiDhLwDwJjRg4f9v1Pa9ba3HMD5bJ4tU4f6x8IsJUeEOgjFVPrtyyqIO7cWupBsbIvevpyLnQd/0CXlml5hbTmu7LfImwBEkACSAAJIAEkgASQABJAAkgACSABJIAEkAASQAJIwD0BG+eBe3GUQALsCNSuVdNy5eo1u+vLYDBAaLDqgUanj2SnCaWQQJkn0Hsr2Xr37m3XkHXr1tHDhg2TkIxcu0xM8DcCwiAldWhUH0Xc12+Fivyt8Y7a++mKTPNn32XcUevopiQ/3ZEMpiEBJIAEkAASQAJIAAkgASSABJAAEkACSAAJIAEkgATKAwE7h1p5aBS24ckSUMplKVkardPwx1lZWRAeGvq7wWTq82QtxdqRQIkTaLlx48ajgwYNcloRmYVO63Q6DE/vlFC5z6glFlJnL26JElevGlDuG+tNA9MzzVC1851srZ7uRsof9EYHlkECSAAJIAEkgASQABJAAkgACSABJIAEkAASQAJIAAn4MgF0Evjy2Smbto09d+GiU2ct06TAwEBYuWpVL7JbEC+5bLYVrUYCLgnUq1vniCtnLVNYq9VScpk0yaUizCyPBIZWjuBrss9EX80+G4POWhdnODiID5qTMRLLpZgDzeoJ1QIBvO5CHLOQABJAAkgACSABJIAEkAASQAJIAAkgASSABJAAEkACZY4AzrAtc6fMpw0OeGPm67lz533O6rpSKBQ0cVbhoAGfPqVonLcEFDKpXq3VMeGO3W7x8fEQGxv7MhFc4lYYBco6gX51nwr48dKWyoqy3pAnaX/3iQ+0fx/OfsdigUVP0g6sGwkgASSABJAAEkACSAAJIAEkgASQABJAAkgACSABJMAFAVaONS4qQh3ln0CL5s0tx44f9+iaEvB4m8003b/808EW+hmBahcvXrxZr1491s3esmUL9OvXrz0p8A/rQihYlgh0qhYl2HxzZxWMLMDhWeswKkl78HTOdKLyew7VoiokgASQABJAAkgACSABJIAEkAASKDkCTGS+RyWkPpbovV1CulEtEkACSAAJIIESJeCRc61ELUHlZZ0ATTaP2zB29Cj6h59W4yxbj8lhAV8mIJVILDq93uP767crltPjJ0xsStp2xpfbh7Z5RCBIJqGSso5HS/h8jy8JjyryZ+Hwdgm6R+mWOMIAX8z9+ULAtiMBJIAEkAASQAJIAAkgASTg6wQaijpFnq59L1V/7pqhATGWs3c4hZw6HFMxoMWtG6YsHVhCfB0E2ocEkAASQAJIoCgBdJQVJYLHHhOoFhtr8cZZy1S06sefKKVclupxpVgACfgugRlp6eleeeZeGj+B+vKLBadI01r6bvPQMrYEFFIq/sLmqAztqRh01rKF5qVcyj/Rsgf7q9ySiKhMosKr75+XVWMxJIAEkAASQAJIAAkgASSABJAAEmBDgA8jVRufPSt/vQHv7K9R8ts7K98KlFMX2BR1I1OzUR2hRn08prUux0LdUcUFB1N8jZsymF16BKSkqoXkr0XpVYk1IQEkgATKJgF02JbN8+YzVteuXcty89atYnWO/7p5CzPqLchnGoWGIIFiEOjYof18sVjstYZXXp1BfbtixRGioIPXSrDgEyXA58P4t18KylWfjImuX0P4RG3xp8ojwgSgPxMT+P3HYSaJBOb7U9uxrUgACSABJIAEkAASQAJIAAkgAZ8mIOd/G3q4z0/CJmHA/2+loJioAMg8HlO/U0uxltje0Bv7FTLqWvzfla+d+SXKuvzQnYcmax/ljaD68vp8MeO05XujF8twQmB65ahKlpSUFB2Z6DN18qSJRznRikqQABJAAuWYQLEcbeWYCzbNPQFZs6ZNNCdOnuLkGpJKpXR2djYOIHDPHSV8mIBCLktTa7TBXJi4ft06euiwYROJrhVc6EMdpUIgQCqmsjQnoyU8Hie3xlIxurxWEtY2QZ+aYYki7csor23EdiEBJIAEkAASQAJIAAkgASSABEqEAI83GWjLQqChCdF/vjh1UDLB2bDT/RtSAQXdfmv37YdnWzETLx9vj9LNENv1zh2tnq6al+bms0XXtpI9fy2vKCssJ28ST9+RNMh/IX9Vdzf7J0NaZSKTVljO0305UL9pge5HyuXr9lSHP8nLZVJdQuIdaUgIM0enYAuUSRPV+uzoghTcQwJIAAkggcIECn4pC6fiPhJwTaDh5IkT8521mZmZ8NFHH8FLY0bAP//847qkk1w9We+T/Jjfc5KNyUigLBCYdvbcea+ctRaLBcaPHQnTX54EJ0+etLZ1yNCh1JkzZ5bz+fy1ZaHx/m4jmVT90fr54Qbd6Rh01vrIxfDoULT0+LrIdLKO0W8+YhKagQSQABJAAkgACSABJIAEkAASKAsEnmr7x7olworBdPi158+RGbFqYvRQrwyX8tPCLwy0cdYyeg6dybFRFxbMB83JmCrDe8p0JONpm8wiBzIJlfToUNVjRZ21jJgigGcuLP6lrLLkG2mVFJLWuHA6y32BHHg3lkurWBJVDfrtrDYFlDwRDgh2D4/WaHV2zlqm2Op16xmHvFd9Z+6rRQkkgASQQNkngA7bsn8OS7sF49euWXNmydKl1MOHD6FyiBxMb8fA64++gEWB22Hv3r1e23P5ytVKpPBHXivAgkjgyREI/ujDOV9Vq1bNKwvIgAVYqNwGc/kboP6arjCiQRD8/PPP0KhRI0hOTh6mkEpue6UYC5UKAYmY0mafjv3f4O7WCEylUidWwo5A8zgxkHWM+gXJecxLfwC7UiiFBJAAEkACSAAJIAEkgASQABLwWwL8qqMHX6zYvRMIhDIeMys2dF9PRYVbQ34Wda6kBYlgLlsylFygq3BxkEPn3NmrBodq1sytINWciN4rk1KPHAj0GNVHrteeiqkYqnIc6VhlFtgVGywK5u1T1DhNXgjH2mU6TqgoAkpDyhgTVXHVB4qCqd9zM6CLsjZkNVgQFMyX7XRcDFPlEvEtZlKCs613794QqFSmOsvHdCSABJCAvxNAh62/XwEetJ+syziuauWo5cOGD6e6dGwL16fXgBsjhCAXPo4GkmuiYcKECR5otBWtUqUKM0P3fyT1S9scPEICPk2g7QtjRqf+7933vA6LI5fLYen53PxGfteRB88dngJhQQpQKpWw4KuvY2QCuJQvgDu+QqDztBHKHP3pGJsQTL5iHNpRQCDjeLT0m/dCc3g8mFyQintIAAkgASSABJAAEkACSAAJIAEkUJiAOKKCuvmqb0RMWr0579j0GwctayercGngG7Lp9YyUMmB34XJF9nn8WKU2/PzAgpjHRQSu3DcbiyTlH8plPNCejAmdMSaQmYY7kMmQSqgM7cnobT9+Ei7JF3SwU9UgsvfYErk4gRSuBtb7XgH8HxwUy0tqpwCe9lZgvaQkVQM5UyZvm5Obkj9z91adD7qIgT88Lw8/8wlE/bb191iKct09lpmVxURZZAZV44YEkAASQAJFCLi+gxYRxkP/JSALgJWXRwe+UEHKg0S1Gaoq7Ueyxa03wfVkJkpK8TZmtmFYaCitz85uSjSdKZ42LI0ESoyAVKmQZxw9dlxYp06dYlfSLK4O/PP0Qzs96lwaROTrJhJQELwkIyPbjKFj7CA9gQSFjNp3668qHZmwTbiVHQJGIw2q1gnxOj0dW3asRkuRABJAAkgACSABJIAEkAASQAIlT4AnFqkH6JMUhWvau3skCKopCyfl7+f8dRfUbxxPonWmaiQxL8ZxsKhXlXtBX7V26VjVttps0u6r5NC5ml8B2WHe4T5cnpE75+VgqxO5cJ6j/ZmNdfC2tKKjLGuahaYhNvPiRQ1Y4goJTazLk3y2X1kjkOfE2Vg56wqta/BFfj/6Hs2/0Onmoiii434hPX69Gx4eZklOTsln5ApGamoqREZErDOazcNcyWEeEkACSMDfCNiMlPK3xmN72RFQBMCpB+ODrM5apoQjZ+2NDDPsOXXZqcITJ05An+e62uQzI64CZSIYNrBv/rqdjIBUKgUdWdOWrI17mhzSAh68ZVMQD5DAkyXQn1yThrNnz+qy1BobZ+2dO3fglamTQSIKgKIjCpcvXw6DB/R1avnJC1dg2j/5Azbz5ZQiyuqsZRLSp6hU5Puozc/EnSdCQCqmdOoTMeisfSL0i1dpQADFjNSOCVNZQySjt714OLE0EkACSAAJIAEkgASQABJAAuWEgEAuP1DUWcs0TT3zpH1HxX9tFnerDGR92siQnd2zyUhzZsZkD8WcpinunLVM8WydmdX7GPMOx9ZZy+htIXAdAItxyCao4uqTtWnTxMBbMVgYrE1TNVx6MLCmU2cto3dWRA+bOL/PKmrChJA2zPJVrByUjI7yvAUIBH+zddYyHEJDQ+HIsWPMusivl2cu2DYkgASQgKcE8EfFU2J+Ji8PgKuPJqlquWt252Mx8M9xxr9qv5lMJvioowrebCyAdvsrwinimCLOLrg/uz10qlqwpOD+u0ZYdE0I58lKBuMmToZ//t4G25rHWx1fP1/LhVf368+oDdDEvgZMQQIlT0AigHlj64qmLuggFTO1VV9rhhnvfADrVi0H0DyCqbUN8HwNYb4hRjMNH0vHwWfz5sO7b70BI5OXg4k83uum7ILmzZvnyxXe+eOPP6D1jlEgIy8krraKyzL1mQba9VuIKwWY5y2BsB7tJAnbllUsiIvkrSYs98QJvPVFmmHuyiwmksPFJ24MGoAEkAASQAJIAAkgASSABJAAEnhSBHi8Cb0fXF0mCgu1s+BXaSQddrmf604Ku1LuE5Krr6fpS7Gc673XWg5SshYOl5vOYobQ6LEg4RX0+eTpr3rpff0dY7rf989MnTLZsnDxEo/P56lTp6BZs2bM+nor8pjiJxJAAkjAnwl4fCP1Z1j+1nZpANxJm6Sq7K7dnX43w6H4LKdi0aFyuDa84KHmYqoJqgfxQUJCvHq6hS3NMGqNUKDMUwUojwS8ICAVQGraZFWIF0Xh+AMTtKhYEOVn1nETfHw4A/hkUWhHW5WIULg+2GbgpiMxiFqRqU/LQaetQzglk9jxh0/C9ozuo+D2za9kbEWtLAkcPZcDHcYkTTYaYSnLIiiGBEqTAPX+++9bqlWrBpMnT6Z1Ot3/SOWflqYBWBcSQAJIAAkgASSABJCAjxPg8SbzFapFfAGdbTYY1pk12mnE4mwPrK7WZuvam5G9ujkscn7G/0xZUzMLOjUcSnmemNr+d4vp7whO36+vJxggdIS909lz62xLDNbcgl31nT+GB55/PUNtyQm2LeX2qIOKL/mpT1CD0A1pF8XZoHPcSeRWzZMXUCpk2iy11mun9a5du6BLly6vkZZ88eRbgxYgASSABJ4sAU5/GJ9sU/ym9tbBqsAssVh0qSRbTGbWXmTjrGVs6D3xbaem1H0qxsZZywjWDxV45axlypLZvgEYEpYhgVtpESDrN5/w1lnL2FjYWcscz2ohgJjwQGbX4Xb5Rjzc1bh32N4bHyQNFFJMyKES20hY5+0hISotqWBBiVVSBhSTOdWfXP0jah86a8vAyfLQxFYNxZB8oOo3ZE3i7R4WRXEkUOIEZDKpYfbs2TBq1CjQarWU2Wz+RCKRuP+BKHHLsAIkgASQABJAAkgACSABXyEgaDzxC6jU0dLn0Q1Z/6zEF/tmJeijxwzVCxTyB8RGx17YAuP5VYYNvOTMWcuI1XzrFc6dtYxefqyS8+fa6wlGRjXn2xFzDu1KaVaD+apgvszdO6VAwRN9EcyXan6LeZGmGy3enx73eRWjxSTdp0rmSUG+y1UdPpwX9feuPV47a5l2de7cGQ4dOsT0OzEDVHFDAkgACfg1AXTYlqHTz+Pxluzbt+9wWnqmMjs7p+6I4cOYh5uOXDdBLoTNDycE1WOj9y/yMPTGm46XmF28aCEc7pLJRo1HMimTVDKhAH/EPYKGwt4SCN87UNHM28LOyl0fIYQ61WMcZisUCnjxILuBlQ8nBklJqGaNQ0XFS2RC/tIWi6V7amq6jIQ1n0EGieQWT2XZLK2QU4fTjkS/XSsWJ/aXzTPo3mpVIB/ImsTdZRLqvntplEACpUagyenTZ2w6x8hzIOj1ekoqlXLeuVVqrcKKkAASQAJIAAkgASSABLgjIFRcF3WZJxL3/VFwa/kPVr0BpE+h2feLJf2yEiMGWdJ2tP7tJ1pIBmLz5dJlRMC6xFOeAaIKYZoWa5bbpOXl5X2Kw8Mg6+0TeYecfQrqBHHeJ11SDttuyjomdw2/WeeD7hLgM2uyFt7aqfjS2yODm2uz4j43qhsseDUtbp68X1DDfJk/Mm9ZncEfyn/oRBIl+RllZIf0Yd1p0aJFsa1t06YNnDlz5iOhgL+l2MpQARJAAkigDBPg/MexDLPwddOpBQvmT+rYsWO+navXrKU2bFi/lyTUzE8s5o6IB68njAvqy+exC1d8KMlxn2FqaipU2PkOiL0Ie8ymCe+3kMxiI4cySKA4BMLE1O24MJv+8uKosyl7uGsmLPzqS5u0vINjic5DjOfJ5H2mT1bJySzgO3nHXHxOnTJeQ9MFA0iZ8M03btxkPJb9uNBfVnTIZdS/6uMxrcXkxohb+SegPRUTKRVR3I8yKv/osIUlQKBmzRona9Z0/HhHQiNTUVGVHD+AlYAtqBIJIAEkgAQ8IxAUFGRhItV4VgqlkQASQAIeE6ggGbP/qbxSZ6a+U/ASn5dIPiv1fQ76PLop66++O4HMvs2uOmaYjsy+vc8Ti9S9H1xj5SA070rl/NlTUC2Q8xft8/8a3DpWC6FhvTunYs8Ad8IqgRS2VJv4s4wnmk9CHWs3xYxjZtEeTI+bF/NT1VEyJd8edaZJDz8oDlk7YNsEdCPrv4nT3NXjS/lymVSvVqvZdSAXMXzt2rUgEQWQPyG8MnUyMH1QjRo1gqSHyX3kUsm9IuJ4WEoEZFKZXirBAcKlhBurQQIOCXD+4+iwFkxkTYDMmjAvXbqUPnv2LP3K9GnMA9EkprBUIjG/8sqrdj+Czz8/mCKjma6yrsC1YI3t/RWfywLsqnFaqo6DFRoMBgO8/mws9KpWvBlpZgsNnY9UhlnicRD+rR6StAXPh681EQtkAnjk1DDMQALFJEBmmn+Y+FJgflgXo5mGOuuM8KZpMExXPweXUs3FqoFZw7n58Q9g9+7ddnpqR8jt0lwlJE8IqkzWnF7kSoZtnkIh0y9cvNzutyEqKgpatmz+6396KoSEhFguXbpEb968mf4vRKdXa/yytau05aQS6qHmREyN0q4X63uyBHRnYgIlYooJA44bEnhiBEgo5Nxr1/51+TB29+49inSQlEzMtyfWcqwYCSABJFD2CZBoCP/Gx8dTixYtZsKQlszIz7KPCVuABJAAFwQkobd4oQUD/MQDN7p8fmSqZGbfNv9+kZTMvo0coE9SsDVDWad2QYcc20Ju5PgxrKt3o6kg++J1g1sGBdLs9j7Pfgh1JRVZCXdR1gZtgwWvkVDHsgFBjdyWeeryXHMkv2q+3J6gJMar2z8/wYd3yMD+vckpj+y90Cxtjo2NhYyJCvInh8946+HH7goYO2IokL4myFRrKpF3Hb+M8sYSX0mJ9Tl1+pRk957dzPfo7ZKqBPUiASTgmgDnP2Suq8NcNwTIgCLbAXG9ez1H//HndqpoemE9TB55MfycpL1RON3Dff6UBiLd/A5SkYfloN7vcrgZf8da7OjRo/DHjK7wfvPiv5t+fSYHZuzNgoCAxwPZbty4AZ8OagrfdHgcLtZC2q36JnOdwQzDPLUZ5ZGAGwL1Dj6vuNgs4vF1zIT+zuq3CEaPGZNfrHqoBC4N9/rZNF/PlTQz/B49BWZ99Ik1bcH8+TD61scgF3p2e76RYYa41eq2RMnhfOWe7wSfPHkyrWnTpk5LktkC1ryi96Sez/Wgt23fYefodarIhzMEfMgxXoj1+F7ow01C0zwkQNW9zZTw7EvoYR0ojgQcESAdHz+RUeojyQA+R9k2aUw0k7CwsPEk8VubDDxAAkgACSCBJ0JALpObb9y8wYuIiLDWHxoaSqelpZWL5+MnAhQrRQJIwDkBnmim7K20eUUFqC0tTN0vHSx+h1wRxSl7D8K1mA1FUot3aMnIhRTJ+eIpKVI6qGUCfVsQx+l7XB31ddPDuHmcM2VMH3rlX9Mr0k9tdL+uGaQ7ZNrh2Sj+IhxK+lAhkz68FZ9QgbyLFKuqH7vJYXBN28k+ceuN8MEXy8is25dBq87U5JhBWaxKsDBrAt2797Bs377N+v3ZsWMHDOw/MFWfoy/eSWZdOwoiASSQR4DTH7E8pfjpOQGZRHxVq8+u5XnJxyWGDB5Eb9i4yeuXwQpSSpfwYpDb3kGNgQaFA0fS7OMGSMrmwaJ2fBDyubusrqWbYeJhARy7/TgqCOMkahAVCCcGPn6eycqlocaqrN/VBrqPt+ywHBIoQqDv1t7yzV2iHw8U+PS0CcauuQDR0dFWsQrBSlj9NA3to9xGxCmi1vkhc11/ftoANzR8WEoGJAiKhCS/p7FAlML913v+qRzje0eymQd7g/PanOdIpRKLTqd3+wUma9qCQGDzTmFVSmaF0Xo9uRGU4Y2ZWas7FVOhDDcBTeeIgLRJvC47h/bpF2WOmopqfIfACyTCysqGDQvWtHJn2nPdu9Hb/9pZpu+77tqI+UgACSABHyfwqUQifXPJksXU2LFj7UwlA3Do7Oyy/Xxs1yhMQAJI4IkTCGjzhkHY4X27TonsNd3MAxK2Pp7lwLGVO796DiR9ojnVuu/yP1C7GndjpYUNbtMPFQ3d9ml40ohmurvmW3Vnc8505r3NMNC01aEpz2REHNGDto3DzCeYyCfh/uvWq9vt/IWLnDCOq1UdTnRNd9miiGWZ+iwDnR/9zqUwZnpNgFnOISMjw+68/u+dd+nFSxbTanXWAKIc1xf2mjAWRALsCWAHD3tWJSo5c+bMgjgmXtR06OBO5qZqOz2XpR4SWvguG2ftx6dMcKjL9w61ftBCCMs7Cjh11jIV1Qrmw/5eNFQMD7XWy8zuu3BfDTNPi63HgSIKkicG9a6s4DEhLDF8qZUK/uclAZFEAJq0SUH5ztqVl3Lh1T/v5DtrSbhySBwp4NRZy9jKXNdvNBXBt08L7Jy1TP6bj1pBhy3uQzC/3lQcECmjXD/tMgodbMT/erJdEyml0Wgc5NomOXLWMhKXLl1m7kPVbKXLzpFMQt1GZ23ZOV8lban+dIxMIqLUJV0P6kcC/xEYffjQoe88cdYy5bbt+IsiazzdQIpIAAkgASRQqgSOCIVCy4oVK8i4S/otvV7n0FnLWMSsOx4eEv6gVK3DypAAEijfBITyREfOWqbR4qF/8JP3HCiR9ucsusn5chz7TuZwaqtSzOM8dPPHkb04d9YyjV786KjTPtzP5RtaExFfGTxciSwZdrd5hMBssli6F3bWXr58GcaNGg4ysRBea66AmBDbeUCxlStCqyYNnJ7jC9duwkv7Xfd1PZwYJK0dzHPfUeW0FsxwR4AMLnPorGXKffzJR1RWViaPPO9snjPnQ4tIJGK+Y+vc6cR8JIAEvCdgN3LCe1VY0lsCSoU0IUutK1i0wENFDWsHwblfHy8fKW0cn52dSzMry7J66hEK4K1HE4I+dTcrtsUmE5wnjlJme6lxoHUmrfWglP6L/D4b0jXZ+bVptVrYMCAChtcqCJ2RkWOB2JVZWhIug2k/5w+S+ZXjTrkjoBDCua29FQ1aRRbMGk3NtsDZXquhV69e+e0d1LsbrI45kX9cGjsXU03QZHUmE/YcwoIUcHe03SBaOzNCvsl4oDdBpF2GkwSFlLqV/E/VWImYBx8s1sCEd89BZCTr4jZayXq2dE5OTpkbDCSXUefImrXO3yRsWll+DpjZ3bfumOB6ohEekmtNo6PJnwVIMihkPJBLKQgnA2dqkBnnT1UNAD6HERTKCkVZ0/g0fTb9eNRQWTEa7SxrBAb9+uuvG/r37+/Vc/nCr7+mp7/ySpm775a1k4T2IgEkgATkMoXxo48/5E+fPt2j+7XRaARVoEqny9b5Suc7nkwkgATKLoEoyeTLd3lBzrsQdZ+H0YNy73l0n2KDY0twLB18ujunenu89id8/3E4m+pZyVRrcdd4MqCu+04TVtoALpv00K76NJbS7MVMtBnW3moCtQTOI+t0yAjLzoVsWw8o+yo4kSSTGrQJ44JkSjJZhtla/GKAh6S7p25oALxcKxd6xBT0yeZV2GxnMCxa9h0M6N0D7o8VAxMZcV/HpTBkyJA8EZvP/fv3Q631fSFI5Pp1ptV6te5cillJCnPulLcxyM8OGGctM7jM02a/PGUq/f3KlSnZudmP14HwVAHKIwEk4JSAx19Ip5oww1sCvCVLlpgMBgP1IOk+jBw1GurVq+dUFzMTb9rLE+DrRcuArHEG3TpWhSNrGP9kwRZ/zwh9pz7MuvPQnMWj6JMkpGMS+ZVnZt1lkb9H5O/Cf3+R2/vJ7z9d2fWzTOhyDWhzCvyfvbo8Db/U5nadCWKPy63OrwFw+16yjcw7b86E93NW2qQxBxuvGywv/KUbR8Zo/WCXiQlIwJZATLcYwYXNvRR2nSfV1/PgXnKqjfS8efNg2v3PbNJK+iBqVQ6kqvX51TD3gOxpqvxjRztGMw3hyzM/yDHBHJJfh/w1I3/hxOerkop5IcIAUFnMdItABT9o5Udhymdb2q7Fu3CNFtoM2A5NmjQB4kSAgQMHAjNIQiZzHIWGPNzB/M/ngVAkhubNm0OnTp1GkPrWkr8ysUklsDjjSMwUoYNw72WiAW6MZJyyv/6tgzUbdfD3KT20l8uhba4C2gjkUI8vARLWyI2Gx9mMnuuWXDho1MARkQYO6LXQorYYRgyRwWCy9kx55ce0Xtks/rhGT7dkBQqFkIBnBF4g6wOt7Natm2elikgzL9ok5GaJjP4vUhUeIgEkgAT8kcCMwc8Pmb9+wzp2D00OCOXm5kJgYKCZfBaMEHUgh0lIAAkgAVcEKEWkVjr1uuMX8/8Kmi7/Ar0393alxqu8GwuX0w96XfD6Puio0qr9NtPH11XiTGfrhsmWP+VPufb8OTLESVpH9XXLhbh5nOnLq6byxTmmTYrbLn8PTLQJ2mYGPbG+leYRfN2B55WcOIxXXzXA0N8egpz0RTjawlVKuDPKJQ5rsY4b1frjD80ur39H+jHNMQESPY9mBpUVZ6tRowZ948YNJmrojeLowbJIAAkUEODsR7FApX/sSaWic82aNYtbsOArqlGjRkyoI9i0aROsXr3KcvbMeVNmlvp/hMR8dzTCQ3i65IPR+T+AWWStyklfNoGf12+yK9q4QU04s85kTf/ih0zo10kGMV6uo/lvvIE4fh/SZFahy2vgma0mOJLweGZtnkFMyNTsmVUgb4RVXnpJfl4ftx/i4uLsqmhRNQgO9LV/dtIbaYhemblVY4C+doUwAQk8JlCXDFi45GjAwpqrufAicXA52nb0V4CjMo5kuUhbWvVdeHXGDBtVFcjDbKKbh9l/7htB215mvU/YFGZ5oCWzLBf8kAXvTgyyzqqs1j0VbiUyYz5st/Pnz8PD492ga9v82xi8uSDNMO/7LGYkiWOIBSq6qVTKn8PCwgMnTpzMe/75560ze2/dugULFsyjf17zc7Zaqyvph/E+Cbsqb6ka6XrgSoHJvr+Xk2uBt+ZmwIpf1TBBFAZTBeEQxHP/8lOcluXQFvjGkAKLTSnQn/w2ffFeMAQpy4/vKJtEcAhtk/CpPgfeKQ4nLIsEihB4n4won92hQ4ciyZ4fpqSkQIUKFZiQEH96XhpLIAEkgASQgDMCcqnceP7ieUFsbKwzEY/SyQBIWq/X27/AeqQFhZEAEijLBAQBgWZ+nxU8c/yeHPPNv3JpzQMRCGWHICfzd9KuPeTvisP2CcQfyN5IneUwr0ii+GBnyzMH/+T0XmMhjp2DVyYBTykqUpv3h8YOm41pOytx9jL+QqMsmC+r7L1BRUpWzLxI5zb82mW/aZEirA67XT5omi373u1L+nvaMfpdxk3O+kQYbh3I4nSdpZSst57WVqsvaKFvG9A9cLtxg+UWL2EDGNTDWBlURChUTGnujg9y7F0tIsv2sOWvJjh3z7Z/Oa+415caAABAAElEQVRsWloa6N6qBuFS95ds4zVZ2qvpFkVeWfz0joC3M2sd1fbnn3/CoAGD7uYYcqo4yn9CaczFhLOxnxB8rLZ4BDj/0SmeOWWidHTdurVvX7p0xS27devWwbBhw0hISd63Wr3lTdK6jEItHNausWjFwdWV7H54f9isgTH/Sykk+nh3QNcI+PVLO3E7OTYJVN3bbmfp/XbDAN3X3gWVyn42X6/aKvili1sEbExxK3M4yQTP/uL4R/35nl3gp2qnHOpgZhlGrsj8VmuE8Q4FMNGfCTz1Wy/59e4xjt8LIlfqIV3rOKr4sLpy+L6TfdiXkoC5m4Sp7bnF8VIdz1YPhG3PuXaISRZmAH05lhPT/j6shy4v2S+/1aF5KBz4IdCujsVrs8xvfpWxQq+3TC6UKSRr5b7NowLenz1nDvXWW2+5vYlMm/YyvWjREmYAzKeF9HC1G7NtacTtHu0LnM1cKS5tPZdvGqD/+GRoqVfAbH4lUFKur42Sti+XOHA/Nj2ATSS4wy/fhEO7JrazuEu6/pLQf4N8H2v0uNuF6N5VEvpRp38REItE+69cvdohJibG2vAHDx7AX3/9ZY1kwAxe8WYb2L8f/evmLe57ObxRjmWQABJAAqVDYOHixYtffvutd2iNVs10ZJNFGrzeKAlf+lAkUIbVDe8MIoGCStfH0zfSjwJtMev1Zs1yovk1F9o3VatWvf/NmzfcPq+60OEwi6x/y8xowfu1QzqYiATKPYHJx1TaJW2CepvEQ3936LCzZN0Fc+IBMN/aqTHf3scDi0kHFO/vgDavDxG2muGwTFFq+hVNLQNTDnN+n9narp5F9VMHzvSmNf7VYjxSmTN9S5qZYKiIGTvOzfZxQLBxQaX+jjuOvKxiYco+aKH/kXVpEhr5XzElzdDSmU2iebV07YQ9FM0EHfkNBW1JtCzH7/3tan9gtmQm5pqvOHX2Oq1fLICPMyarSmSgcjUSqOJ+SprDul9oGAjfdHDcnqIFKi3P1Kfn0tx0kBdVXv6PA6pUqZqbmJjA+fONUqmkySSvqQThEicYmc7UFWK+bKg0ICjgqeDWIBdVoLSGZPrcw+1gsRhOGWljcydl2SbHEGf0rY0bN1I9e/bkvI1sjUA5JFAcAnjhekav2VdffnF8+iuvsubWvUMF2LFUTtYDpOHKLSOcvJhrXQewTWOxy5q3JyyCHj162Mj8+U0E9OxY/N+jCu1T4PS5q/B2txrAuItDyOC4qY3s7Rmd2Ao2bNlmY0PeQZf2reD3Rv/mHRbrU75EDZmT5CAg8VqdbdV/yoZ7GQVr2JpMJuuanmKyCK96SpCzYnCXzFiusSqrHxHY4lQIM/yNgPDVxmL1J20lToeFfnPBCP1XXYTo6GgbNmzCEdf9QwWXexUem2GjwqOD6msMcC9N67AMs85Hy8397fKYNW9/ukau+0ASvrb2SFj14xrQnqhgJ+dNwh35PqhSxXbAXNL+qhAZ7vydMYHM9D12Pte6BmqDmkLrbN0te3RM2HbWJly/fh0aNKi3NyfH+CzrQiwER/WR63/8JLzMehKZWdDPDn0ATZIV8FFAFIsWPzmRBYaHsF6SBoc3VYSIMOfXy5OzkF3NyzaozZPmpDIjFNzNHmenEKX8koBCLnv0MDkllLxIwpo1a2Db59Pgu/Y08HkUbCGDL4Zsc3zfZwOL+Z0iG+vnVDY6UQYJIAEkUFoEFFJFtlqntr6Y7tq1C/r26Uvrs/WjSP1rPLFBzJOmd6/xtqpueCeXxXSGdNh560v6VtY/wNw/BQEC63v75MlTYN68uSV2L509ezb8+OXvdHzWmdbEwGMujcRMJIAEyhWBClSUemvQNcUy/WzYOOM9cu9x3g9WnIbTuRpoO+Y2BNatVRw1dmV/k0fRoRf7cHZ/TK6xgaYvxHCm72JrMVTicTfA3lxpMIQFcDuZU3x2Jn1A9YizNhc9SR100WbB6wlWz6fuE3k3kr+zqIyr48/aSkzTG4vZeU5dKXKQZ7bQ8AF/BMz/apFdbvvqIfD3c+zHaamWZGhzzMDtybGzqtwlvPFMzMTPPtvwAkWihpZI48iSjzBxwkR67c9rybJZImCODYZcql2VF6B15ZEQwLc+5jmte+/tRfTJ+7+cJI7bFk6FHGd8LRaLp164cIF66qmnrBLk2W4v2eG0H9Fx1ZiKBLglUDJPBtza6Cvaqrz88hSPnLVmsxmWvffYJ8S8ANatLoQx/ci6gW6ctUyDt27eYNPupKQkTpy1Y95JhZvxSRAVFQWrL+nhs+N6mLFPA+dSHodaLlypVlewbmbhdGa/75CRRZO8Oq67JgcY52uV1a6fVW6OksBP3eVQNVQBIuKkPTYiGNRTQ1w6axmDKit4sKW3fDPZDfHKQCxU7gjUDeGnuXLWMg2eHBcAYQsawb9jA0FIOm4igmQwpanS7az0gdv0cOHiBQj+1vHsXE9hhoSEOi3ChGF3tN0f9BMsOq2FqXt1sGjJMuvasyFtkh2Jepy28rsVNmWOHz/u0lnLCEeT6EZDesihcR2R1VnLpPV9VgZktBuzy2oja2LAlSv/PkOEP2ZVgIVQoJzSlVVn7erfNVC9+T2411kGO9Jr+7yzljkdrwkj4KS5Lhj7BkKLVg9h/spMFmfJ90QmDlby61QPeOh7lqFFZYWAiAcpao3W6qytGBoE/Y5PhVUdweqsZdrw422nY4lYNZEZICiRSDD0EytaKIQEkICvERg2cnj+TbBz586g0+uonJyc1V27dLMEKgOZF9ZXXNgsE/Mk91tEDrK83naPW2cto2f52aFw/PZmKic3h8rOyabIrBBKq9VSJemsZepNupsMwxssocY0/PaoiCe6xqThhgSQgF8QiPg58ITVwTRR+gHoF0Sx9055iIcSKWBXs+6cPxNW6tvDvvPQQ9sKi/MU3E1eTSaD17l01k7RJXLurGXavkD+i+sO0MKAPNzPsqQBNXRjgbM1QMb0h7LeiJtWU9hZu5kMJu11tibcmXIMjvTZBBuuG1jrciTIDFCdkb0a5s39zC67f1TBJB27TAcJGVNUcnL5HHKQhUkOCIh4Eu2MVn/NbV15NJWamupAgpskEkUEvl/1PZWbm0ueq9TkM4di3lG/2/Um7L270G0lz8ROpd5sd7B5ZUVdi4ASHHdT4EuFQmkZ98KLFlLHtOzs7HxnLVOuevXqT7spj9lIwCcJoMOW5WmZNHFc/KJFiz36UW3csCZ4uy7irr//srFs5Xff2hx7c3DgZDYMnbQRFArbAUh8Ph+YsMOFt99vGeCXrY5n1zJyU6ZMKSzu1f4V8jC1ZO1v1rLpmRkQ8l2uSz3P1xDCv8MDIGuSEppWEIBYwO50dI0OgNrBvASXyjHTLwhIBZB0ariS1ToczINkVbIGp3qyEhJGi2BBG9ezAp/6MRuW7rthDWeZnp4OT//i2KHqCegY6pF1QIOjMs899xz02WG2ydKRtZv79Oljk8Yc3LmXDJ3Hpdile5rw/cqlNkX+3rnD5tiTg9emj/NEHJiwodu3b2fC8tTwqKADYbmMupZ5PKbMxUGeOisVhrXMgOfmVYITwjoQzuPu5dYBphJJkpGQTTv4NeCFVVVgegsNDJpc/OuyRAx1ofTy1spysYiyX9DZRRnMQgIMAQkJCpL5sipsY085nBkRCPHDefmO2jxCcc8xE8mKt5G1EalBA/ozHXRMuE/ckAASQAJlhQC9bNlSuxc8kUgEf+3cQWVmZfJJZ9yXZNkhuk/vvhalMtBC1oO1hIeFWyaMn2AJUoRpX2+7N7Jz9Rl2OpwBePjoHsjlrF4NnKnwOH327Dlwc/9jH0qUsh7MbLu/pkSgYBIqeKwMCyABJFCmCEhAellGKfNt/kX8D2VJ5SZyXb7SQjsB7d8vdMTNbp33ZnL6EsqvKrft1CiGmdfJEjZcbttN+sc3aw6V1rnymalpQEcONdqq6qqvQ/MrFUSUlYw7wkQUYzXlmIRCPs44QfM0tv7NBIO2ZsKug0eBmbHI9B0nOF6tLq8Iq89gMQ/GJsyF1k3irFEtmELMJKeJDVzPvHSk/MYLQW2IWS86ysO0fALPRCnqWGa23SuTBjDBwgCGDBgF69evzxcojR1mIsb0Twawrmp0o++ot9r90zxQoaKHDhlqURLHLPPcFxoSahk+bISFLCdEHgvpV9TqLOq7ld86fPa7ccO6rEWJDYxh3RgURAIeEkCHLQtgZEq95Zul33nE6vfff4efZns/y65VnO1zwbcr7MNFsDA9XyQlzQT7br0IXbt2zU/L2wkMDIQpDW1/GA9GDQUmVF/RbefOndC0svuZhkXLFT0mN1X4zNANunXrlp/FzBh8X/QSVP7JAg+0nD2zWfWTjlE546zLrwx3/I4AGWI49t74oIpcN7zearK269n6cDczGypXrmxVT+4ZsGDTPlhJQqAXZ1vfTQQzWqrgg/eYJVztt3m/HISUQu8QsgDKGiq8qCR5qIElP56G2UvSi2Z5dFwrpmCgJlPwqy/neVS+sHD8zjDo2aNz4SS3+927d4d69eoWaxYCeZgf9eifqjXdVuZDAq9+lAZTiHNz9u5YWCyo6kOWFc+UDwIqwfIL1eHD5jnwfBlz3GafiVEGKnjefwGKhw5Ll0ECQUJKlz5ZZR0x17uaEOqE2N5PmSa1+tUEc+d/6bR1NWKjbfKYZymmc2Pk4AFw9uxZm7yNm35lRjKPHzSgn0UhgD9JpsOXWJtCeIAEkAASeDIE+jRt0oyZGcGq9iFDhsCWrZuprKxM62zY5JRkatnyZVSGOgXWxI8AC207ENmV0i+/+MpVNqd5FosFokMbgGlvF2hdaYyN7tda/00Nq//1QxFPmmGTgQdIAAmUKwJvyxbbLK5amV8NDD88Y9v5x2GLA5pO5B0ZMJrdzZVlvYqaT4H2iwsspd2LCWoFcfaMej2BW4dtA0kUtx2TBIfcXN09FC8lNuUsB+mriTY8ecHVAERBp9yplAvhfvqkoHxPb+xPuXDmrhoEgscTF8Y1VkGzX/vCm01Z+X7dVQcKIQV7296DG+NU1sh22dNUdmV+vOy+Py1QRMH67jJmhpPz8HR2mv0nQcgTG6c237xnTKOVNtfFtGZ/wOWl0SDgc3M+2RL98nP2vg2tIQ3OSD+BTHU6/LzuZyqLOGaZKCiPUh9Ra9auphz5NxzZwTxfhoSEMPfZcEf5mIYEfJGAR05IX2xASdskFgccYGYqsK2HTL+Hrp1aQw3eFGhQKz+iE9vi+XJr55JZpI3r5R+/2I+1CfllCu8Mn10NZs2ZUzjJuv/6q9PgwvO2uofttsDiZczvXcF25MgR6FhdBe23DYFD/V3PNCwo5Xyv6voA2PjLRjsBEn4KHmVkgmpBIgy40Roif6SBzY+0nSIHCUnEWRfAg8kOsjCp/BOQLu0s/U7Ccla2KxwJWWao8qMZGv4VDv++sA9upufAwYMH7Iq0bt0abjeaBGqyqEZxtnltAuBt9VJ4vYUSPvvkYxtVDRo0gLh1tg+xzINuXC37lwBmNFuDzqtg3wnPwswUrnD5e2KIj4+3Js2b+ylc+8P75x0BORebP70FkRFBcPPmzcLVuNy/ePESJZNJ2ffE2WoTLHg95HsxiUlaFraftmrg+Zbp8N5fMfAhcW6W122KMByWEcftqy208NGSstNPue2biJnknOBsmPJ6YXLYrggppXswMUjqSuW/6Wb47Z/zTkV+WLUKDnfNguqhEsgLiT91yiRrqP5vI/ZB7R+ehW1k2Y2utUKgasUwWLRoETBh63eTWWkpk1XPnRymtJAQZ3pSgU1HodMKy09GnXbt2ljWrl1LBwcHMy/q5WfUS/k5R9gS/yXQngwQtiQmJm45eeqE7Qupl0zi79yEiIHn4HwyM07F/bZl5TH3QhxJhAZXgJF1lzvVFqtqTmbb7glqEtHbQgF/q1NBzEACSKBMEhCDdFkX4fN2th+W3ecZji4oMadt0t8n7OosboLhlwfevo/bVS2oEWQ/itFOil3C5VvG4nW+FKnm48henM4m/j3zAnwp31L8DtUiduYdfhG41kwJbCfjMHkBrV6tkydT9FPIg3emNBDlPJqoimQGgjJb441GSMooiBg3uG9P+KY9D3j/5RfVUZzjKmQZOyaynaOt0isbIPJ79xOhesYKoUVFfoIjHf6aRpZbuN7jqTcsb7TdJwgURzjF8Hbbg9C/7yCn+VxnROUUTNpypfvvxPkwaIEKtu343ZUY6zwSApo6ePBgMhM+mRQKZF0QBZHAEyLAyYvRE7K9NKoN37x5c3Lfvn0d1rVg/nz49JPZEB4SAB2bCqB1Q4BBXeVkjVXusM4iM+KEZNbcO+PtRxs5NMpBYkzbBIgnHYFFt+mko28WrCch+mztnad6Gd6fVeDcbVK/Nuxq9xCkxA4utrCVRtBoNR6pGjN6LPyxeRPcH1u856VfrhvoUX/pmJuzZwZ4ZC0K+xqBKkqe9t8xgbLi2NVkrRZ4FWvB0aNHPAqd1rVLV1gbewzkIscPoZ7aVGlVDqSpmT73x9uDBw9AMKeOdYRiXhrzWXUtDcmp9s6v1lUV8MsPwVCJhBX3Ztu8Wwfn/zXArCne35Mc1fvnfh0cPG2C/acouHAtE159dTp8/Mk8hzOGyag6JrT7R0TPe450OUsLlPN0mcejXTpNnJUtzfTUDDM07pIE50R1S7Nan6nrGeM1WL8uFGqRFy9f3xr2v6cj34f8sFG+bi/aV/oEYgJ52iuj3f/+PH+tEfy+c49DAzMyMmD78Mow4KnH3wlm4NBOEvJtQpx9h4xDBYUSd5Fy/bZqR5CnwrWFksvrrnLZsmWZEyZMyH+AJaGzgERrGEoavL68NhrbhQTKAIFjQYFBzW/eukmRGQ8lYu62bdvg0GcKEPDcP0s0nHYfBgwYUCJ2FFbaoOqz0DvadvBl4fyi+4uO96WzcpMHkvTHawgVFcBjJIAEyhSBloJOmq8UWxy+N4zVdTLdeX23dy/obihYtA+h0zsmEIeHuZFkn/1X7RZm6famnHRw5B54ABkt7rKv3IXkM2OTTJtu1OKE49rcNJha800XtXmeJT37Br1PlZL/XOq5Buclhmvbmh7OPOS07brPlK+AxfJ1IQ11YwN5xy6PDrS5JhedM8CMvVnW8Md5sjGVwuHqQM589HlqXX6+dcQEX51UW2WaNagHBzvct0YWclUo+JsMbbYJbNcAdFWgfOZ9+pSq1ZuD63/B+jqbe6QDGIy2E0FKAs24sS9B5O1xblUfvL8M1h38BCIjI93KeiNw5coVaNq0KU0m3P1Eyo/xRgeWQQIlTYD1F7ikDfFF/bExMZZbt2/bMUpJSYHZr9SHJe/a/K75YhOgRodEuDBMCeMfdYafft6QbyPj8Lj4UiQ0Crf9PZ9/xgjv/lPgywwLUsDd0cVzkuZXSnZCVxqAqdvbbfDzg0FyaTMs6+w9++Y/q3UXU83eK/DWeCz3RAhI+JCWPkXl9Yyiy2St5Z57ZcA4Rr3dnn3mWdhY8xTIhJy800BLEjbz7N2s/AfWauFKuDzU9rvMhP34+9kfbNa03bVrF7T7czB8cSYHRs8J89pp6y0HT8v9slMLGuXH8MK4F+2Kzp37Gf3WW28z67GwerJUyKht6hMxPewU+VjCs8MewHu3K0OcwOf9yiVKLsligIHSG3Btd+USrYcL5fKm8Xd12XQVLnShjnJHgHYU3qtoK1dfzYWX/i4YxV40v11MIOzqzc3vB6P7OJkU0fEXDRlmCOeL1lVGj3sGBiqWMotgZam1HUgbrKEgyLqUFo1GY/cc//nn8+g33niTAUqX0fai2UigLBJ4XyKRzPr888+pKVOmcG6/0WiEV6bNgJPHT0P6g2wYVP0r4qxlF+0qbMAJmDZtGuc2FVXYKnYAdKo8s2iyy+Mckxa+OtaTNlly6xPByy6FMRMJIAFfJtDmaJDmUN4MRkeGtha1N0teOMjdA1+hSrIXVTEPyLrBme57m7bC7SZ/F6rB+13THS2khl/xXkGhklFPJ1ouGOtzEk6rkfq6KTFunm0nS6G6vNn95LoSnhX296ao2zLtm35jCWg22Wnb9csaqun0m9aZhST88ZZTw5R9HM1snRs4BT6Y86FNfdNfngxz+aU71rHb8RjYf+x0vh0VwkMhYYg5vw8sP6PITviyjE0aAwwqkuwPh8+LBYr1r7XaSW4zdq8/Ltu/7Hx/eJSZ5FKGi8xnOnSGNmB7bTnTq85NgbVXJ0DzFs0g9qloWLjoK4eTOZyVZ5v+5htv0mSAL6g16q6kzC625VAOCZQ0Ac++xSVtjQ/pl8tlRo1Ga/fjvGHDBhA/mg59ninWZL0Sb2lOrgVaPXcXjg5SWuuqv1kEN+4UOJycOWInpnaBH9Y+/iEOVUrh3ljPZ284a1zln/nw6NEjZ9ms09PT0yG6ajSkvGB3eljriFqRqU/LoX37JLJuDQo6IyAPgIuPJqnqOct3l95gbTYsXLvF4drP7soWzR85YiQM0W6GZ6u6H+lftKyj4+5/mmDPjUzrQwsT9rL26m4g5Nve0jv9QcOh2wWzbGfPmgVvZiy0qvuLrO+i6q+ADs0Yn6fvbhlkJlnfNyvBgYOH7YwMDQ2h09LSnb6UFCoQcmJ9ZGqz+tzdzwrp5mSXaecznVNgj7AmJ/rKi5IXTfEwd4MSnqrK3cAhrtmkZ5ohpE1iC6L3BNe6UV/ZJUB+f3Tk94fVyIvw77JBrXMcrj5cpYQ7o7x/3nFGsMNGtfbEQ3OZH4EukYgtarWGyltf6+LFixAXFzeAx+OtMZvNTn/gmOd8nU7PzQ+yM8iYjgSQwNNKZeDuAf0HUCu//87jDkRP8fXp2Q/qq2eSzlw2j4aPta+/PhVuPDjpaVVeyYuEYnij1X6vyjLruC0+0Z84bg0tiQJ83vCKIhZCAk+OgBBE2QdVaS5fRjflrIBvpo4CRyFti2u54cAcc99/rIPViqsqv/zujYNA2Cw8/9jbHWag+a10EpFMVnx/sqD+bUgJZMYkFn+rob5hSY2by/4HxU2Vbf790vyF6HzxG+mgnta6ShbJ6/dd2kob9aD/PJwZDX3X2YDSntstsJv0MTnaTo8IhLohJWK+XXVGMw3a/12BihUr2uRVqxIJl/u5DpEcT/pV6vyobkUKHrMpXH4PXpIGBC2f3uIPis/z7p3xgPEdOHhkb6kQ6lx9KrSsNJx1XczzT81R92DipAmsy3gjSJbChN69+tInTxw3qrVqZjB+sjd6sAwS4IqAyxs6V5WUNT0ymSSJdP7Y3ekuX74MIdpXfN5Ze/5aLnwyJTnfWcvwt1gKJhHk5ubCnz1sHTuMTL11hnxnbaVgGWfO2gc6M/S+0pQTZy1jJ1mDjBn9Aq0ORsPHxwpCwzJ5jrZzj0zQa4saKi43gnRhFsgWqUFnFEt5FBgdyXuRRinkSnNAQEABZC+U+HsREhpNLxKJLIRDFy5YkEjfVymQ1ZMszATJwgwIXpILzdfqYPFZx53ihev8N404YL7LhX9TyZrUXZmBVsXfVq9ZDfrhq2D6PuezqDypZUdPAQyJUwHzYNGiRQuo96vdLQt29QRgwj/mbYUv0G7RARC8RwcffJ2el+2Tn6pAPvz1VRK89upUO/tSU9MoMluDuWZcbhVC+Ym+7KyduzwT/urMQ2etg7P4nSAGkodIYezM4g/2caCek6RgsuxS51aS0nnD4cRiVFLSBKQCyGLrrGVs6d75GYcmjR42uESctUxlB55XypVC2Omw4jKSqFDIUvT67HxnLWN2/frMJDT41WAwOHXWMgLnz19gRoHUYPZxQwJIgFsCMqlUN2zwYAuZ9bo3KyuT9/2qlSXurGVasPXPzbA7+w2PGhNVPcgj+eIIfz7/c6+Ly4Uh8FbbAxSZOXOczBxmnn0d/3B4XQMWRAJIoAQJ8Fco9rh01jJ1DxSPB/2XVQq/snNmkrDD+/wzL3t2f3RXuW72BU5i5DKzAf8+4r6Pxp09TL5MzHPbN8BGDyMzu+JzbEVZyaXkshrHyUpXYaG7ppsgGrvPbd8+FSAFSQDvtjNnLaPz1ANDYdU2+yRKoc1xSR5MOUTbOWuZ+m7dSYK49a67cWNI/xHZjpK/kgHOaPeNbaZSGGZ5p92hFTNa7fDaWUsGgsGq1d+WWot231rMui6zxQhhz10scWctY5BUKoXde/6msjRZQrIc0cN2rdtYZBLJBdbGoiAS4JiAvdeO4wrKmjoSNvOPHcsieq491BW+WbYq3/xmjevA4jeyoIUX64XlKymFnU6jk2BJPaHdou1DrzeGzTt2Wy2oEKyExJG2zp1+O0yw47oacnJyoGX1cDg2wDbfW9OnEefU5vgK8Ehzx1sVLsuR2RMQG1sNdGo1RAmyQCXmQbyWAq2FTBoxSqFl1EhoENEDeJTj9pxJ2gx7by96mGPJth265bLW/MwWxFl09I8//qCeffZZaNOmLX3kyGG3D0r5pXHHhkBe6EJmrb4qVarQJHT2MiIw2UaI3cGq2iHtRw+oO9fp/e1m+lE4fvdHuKu+CkFkJGcNhcEaFzExRw4ZOQa4fv06REVFsavNQynmhSRULIG7492+s7HSPPuECQYs2suswQDhQTK4M1pkU671byY4c1dtTfvtt9+gx4EXbfItZERr1PdZkH4ixibd1w60OgtU7ZIC9x+kgVj8mN2K5cvh1sl3YfGatEx9Lu1wUV2ZlPed9mT0OF9rT549FVuSsPX8esD3MGxNXnl/+nwq+yKkna7qs01WNo+/rNHRXs/o99mGoWEeESAzaw8QZ217Two9fSACjp67YlNk8aJF0PP8exAhK95jxabrBljyoAokP0qFsdE6mNm04DeCibTQ73ct49h0PVTdxjKfOeCTMP+mTp06eW0Q83tMNqfPCl4rxoJIwH8JCEOCg3NS09Ke6PcqpkIDGFFrOauz8OftOXDg8npQKh9HpWJVqBhCHWLHQvvKE4qh4XFRg1kPXx/rTeeada+QlMfhc4qtFRUgASRQEgTkoDy8W5XUmo3uLEsa9Ol7C/iVmrMR90hGNz+CHpSTyNn9+TdZJTr0Ul9O9I34cAfMn1n8Nc2jWt0xkXdrx51/HtBKtZggNuYlEFBW558HJR2LntHfBXXyDJBQMscCxUhtqY+kZa8luT0PlhMLIaHaPFCKnIuefGiCg/XfgNZt2sDTTz9tterevXuwcGhjmNOYE/88q5Y23xEIF67HO5R11KfFCHbZaoTj97NBHMCHuw8fQYO4ODohMbF4L1IOLXjiid+GSaPHjW/yMyeD4X68OhbupFwtlUadO3cOZg3ZAvUrdGNV39enupEoVI5nfLNSwIEQ6fOniZ+kPF5HHNBBFSVJAC+6QnSlEiot81h0zzaNJfDNtINw+pco+OHjcDj/WxScXJPr087am4lG63q1f7aW2DlrmSYOHTve2tLhQwbZOWsTSMiIdSfuwCtkXYLlPUI5c9aGLc0BlXIHVAtpVogyt7t8Ph8SExMgNSMdzj0yw767RkjIMEByegqYeNnQqGJvp85axpLGkf3g9bZ7IxpVeM7Cp/ib2FgnFUkzqsVWs2RlZR0jsxutzlqm3J9//sE8+bzARgfK2BGgPv30U+uTo0qlAmbNORIaZ9KI4SMtgcpAZijf46dFu2I2CdYRZu+2PzrGlbOWKVE9uBXwRHy4cftfMphAD4eTTHCE/N1Pz7TOWC0pZy1TN4/Hh4nN90Lgkhwwmos/APSD5gIQLnwWmsbVgZRMMmP2qG2/+5H+AlDKH78Y9OnTB/RG20G7PNJhnTQuCF4eeR+Wrnvs2GXs9LVNTpwWaYcjQH2yFnz/URhs+rICjG83D+bOUILuTExQoJzHTF0uOk1CuGF+uE86azPVZni6RQpcFtRHZy3Li+2GpD682CIL/o13PvKXpaoSEbu4JaouUez7i+6WSOtRKUOAPJKMvT420CNnLVOup+KudcAcs89s7775GnQ+826xnbWMrokHLVZn8O37KfDeYR3U+qWgD4uJtFAvhJ/KyJW1jczgyy2OszY8VAH05VjymwzchL0oawDRXiTAMQEBn791wYIFT9xZyzTrn9PbIDPnAasW9ox9H9545k/oGPMiNKs0kHkHYVWOEWLWPAtWVoBqkXEQqgqHyZNeBpPJdYd2peau89lWLuRLYWab3dT/2h35uqLsKfJCQf3CtizKIQEk4DUBZv3PWPLHDNBsQf6eIX+9yN9g8sf0AzEhod4if3MCIOArKchWySHw1z6isU1JGqstkBcC9LrBJTKVUTJmP2UxcPceVWPGZNuOBVYtdCx0kQww5GILNRQ85xZH3zhdAs2Vs5axo8P1pZaScNZ+rX8LpDPuWfvRXLXXonkA24I+demsZco3ixDAa4++gFZbBkCPGgp4t4UEjLPqlaqzlrEjJiaa/O94S0lJscu4QiLk7b+dBblGE2Tpc62DsOITElhFY7NT5qMJQp7obP2wzhbS3/nihKbrOHHWMk2dv3i2yxaryeSoYUNGglQsg9iK9UAhC4KjR4+6LFM489ixY9CqyjDoFDsV1kxOYe2sPf+QPMtpnnw0wOzsbGpg/wFMx+3LhduF+0igpAm4vbGXtAE+oF8UqKS2zZkS3HHaiMexE3zAJtYmmEls/6pP34FDfeVOO/ZukvX1ssZvh57dOkPCKBGISKzYwtu4PUaoo7LAa40LZl0Uzvd0f9ttA7x6MAZGN3wcVkGT+wgEbQ/A/PnzPVVVbPm6UW2hfzX29V5O2Q1/3/2MDhAI6dzsnEwen8cTCoUKEsOeN3rUGJjz4WyqUqVKTu3q0eM5eseO7UzoDVuvmdMSmPEfAeKfdf68z3R+fP311/SsWbOsDk+w0BqatoBAECAl54bfMLwXdH/qTdsL2wXaq4/2waYrb7uQKJmsq1evwvReS6FFpWHWCk7e3wAWw1L4vR830Vo23zTAsO06cBTmptrqXFi8cg0c+2wEzG7p/Lte/acs2LMmEmrGCEsGQglqPXs1F9qPTrqn1dHMS/H/2bsOuCiOLv6/ox4Hd/QOUlVEQLGLvffYezf2Fv0sMZpETTGxxN5r7C2KJmrsvXdFUREEFWlSjzvalW8Ww8FxheuA3vx+sLMzr83bvd3Z92beSzI3o2XnPPDW/jJWDcfwnFynI4MEmGDipCGlLxP9fEEWLBbz0Lm5dn432tSiQ5NY3sd0YYW757Q5RgMtuRpwPNrNMolygqpTjkTl49s7NLTxoGNdM+2s5i+SY8gFEQK6jcPCXxYVNrVr3RJ7qz6Cpemn12bV7RncdxwRCU1SGGyiCK0iHy2JoYDTsGFDpWSk5hf/7aYthHe0ZyL5irMYt3rnd9yY+ILvCwqwXNxoqBg0YNCAshogH9Am6fGJ72hOThVnXkMZFL+p+6+yYyiE4zf6G4t++0UpnMOH/8Lt5SyYG1OPzuKSlZeMv6JnYM3mxejevXtxR4na0JB1qGIdWqJFO9UTL38WPU85F50nzPPXDkUDFYMGDBooqQEnY6uCNYzrxh7GlM9Wt6Wp/yyBaZvF2p0QEpHzN/nzuydHaMWrWZCZhesfZxP7DF1jZZi2PVrw9m839SbRJbj3qJWGrZZeJVrUq7pmRIhya61Q2r5UFpdZL/miXmZfa41eEb8w+7588z4Hy7yegbtccbmH1m+nIjG0ftwQIcCg/a/h4OAgk3aIOwu3S0SFpGzeAdvSyOJZ6TGyrCwzONlcmdHYZBKvYI3mRszMJlVGWDV0H6T1++fSu3W4GrNT5oiXLF6KXWtOoqPnPFCLxEqWFx8vYdmpgVRkxJLNcushNeugm91auf2yOva/G0E22ETK6iqXtl27dmHU8NHCAmGe9E1WLhIZmH7uGtD6D74SKKyOLZv2KzcHLSb0Z+HHCTambCvNJxj6Hvc8knfyj125aOlpir/aV5znhcP6PIyovRdsc+kP9vDoObj46BDs7e31pq66VYgjz2uuUvySua9h0/o5fvv9V6Xg5QH16tlbdOToX+NI/yZ5MIZ2sQaMnBydChKTEjV6Fj19+hQTvlqMFh4TxYQVVd5lPsGOR592nSuC02aflYUNJtUJB8k7JUGWMiQvvt4KL4abwtWyYvyWr74vwNgHdvB1TsHZrcVGbQnBK/AJlbN78fZM4cb9WXRPF2PMHMlGlxYVw4cWTvIGC7+zRAsTSQNfBVZnhRQtWpCH04MSMW9Sxfv2Mg6OGUui9Rue/xXyztGdUE3djLPP9LIq80HT9CifhOPnI3aYdsLiKzuiF2kC1NyeShY6fbLpTBk/Bj/RD4kX8W17lieccZm3JIdfuDNEWbKqwlHv+r22tuzuBQUCE1MT4+zUtIwjpG0a+ctUgZioZlVbhNYwRsJHEi2l0SD8tvgPmehODmw8PWKDxkNz8fWYqdi/83c8+kt2yL23JNLGcZLK49HLAl56pkCQyREKUzMEoqQ0AT09S0jjC0QryPoxapKYK5OZodGggS9MA2Z0i4w+NRezvazrYOOzHkj+mKB3DZRekFEkwLVr1/Dvd0YSizWK+mQddzwfjncpL2R1SbWRHNnoFTwftZy6SfWVbPjz+QjsPLQeLVq0KNkMX5cQDKyqXMhmCUQlT/692VH0is+5lyUSaD+mqpIyGMAMGvgcNRAZME80JG6nYI3Zc51/tP+P21fwaMZBrfPJOzmZ3+PB0jIdfMpev+PdasN6ZWNlweXCZdY/Isi54q7xeBfUycFkhrQtUi5jOR3fwLRgp9dQjR3IFPlu0RuFc41uat3o3JUbwufNeFzmtRSt8kDqSDkDrcDNV4hNaupDGzx49hIkLK2EpPHx8UibUwP+NsW3DGNVOmRtAiFREUkUDJsbOfn5YRJEKvaJKRP0GD7NyG1m0ys6k3TZnTbg5WRL0F+3bj02LTqKrj4LJdpLn4RHf4en7y+UbpZ73oykhGiuZEqIVN5b/HG1mzgVWkmiVFpEWY75kjC6qNtbu2J8yBHcjd8vuhiz/km+KL+WLvgYaBo0UKQBjZwkRUQqwdHHzJT2bP9SR/Purcu0pVXY4SzbkYGfN+Vj+LCRWL5yZaGca1ox8XWQpAOoPAYQti8HbOZQhHmOUMj+dOxi3ImjbHO6L3v27MGLTb5KMToWPQ/7Tq9EYGCgUvBlAUVHRyM4KFjEy+FRIVorbpzZsgaiw34Wk8XduGWjRf/+/bXChXpx21k7YXLoP0rRO8ubgVt3rykFqykQi2mDqXVPKSTzgROJ/U8nInWCfo34soQSEIen2XKyM54Y9rdu3YoJEyagbztT7PrdURZ4pWi7/SQXA2cm47fptujT3rJcZP5heTp6HHRGFaPyf2aXiwK0zJQjEmBazRgc2aj5R7k2RRv5fXLO9iPZkstQtcnAQKvCaYBhjOy0CTZlTjC9dguRmJpR+Fwd9GSm3sdxpfN+tGvXTszX19MVz3pI+h5bHsrKvpUg6EqALokBtVBhsSyzjhwJt2rdurUUNS6XiznfzhJt2rwVeXl51C7X/0kBFTeIqFDGJQsVbWbdxW8wecqUks2F9denPOFXRSv2tkJ6xImLgK7vuAkpgqmkYasUQ0ODQQNfhgaWBTm0mfZVwE9iW8LrtJvY95Rae6Hf4u8SCiO6CXyDnLF73w5QqVWKSiufcQjzGF50qvDY/PtsKBtm3cHaHeNCDiukV9QpJHOVHc+HYe2WpeIdtzYsO0yqfaIIROvHZdea4z27Bs6SiCQjs9+k8CCqQpjkaJ2RgaBBA1+WBvqLaq/ZRw3Z4tE00UXrdPHzT1dqaCSqKbSYEEHXNn1v5m8ImKOd5/VR6ypCu4ddNJYxOfCQSHi/isY6PdvQCHWMy5ySl6nSdJev4GvmUCacMgAOj34R/mMdpbGOSvKiHJMtmh8WGQf2Uagz4bVfkRK8jrwnFYKVJF3h6lS441EPnfGQOG5LFlsWEx9GSNpV3Hfk4WMmtyRYYZ2yz/r5+c0nJwsKGyruPycz0KLPWlVlBhoz0AIMfBUke1GqNobwwnYlDh0tfKzhj2V/YOvycBKhcrHSpNc+7oq0jBSl4KdM+gbsp8rZfvdHTUDUhwdiutT169NzAJBhh4gPF5BfkCfu00clNjYW6wZFk4XOxc+WjfcGiFJ4sb0I/6P6kMHA48vTQOV9ait5rayYtIvEWNPC0a545Y2SqOUOduRsNuauzENCqgjrSY6cAQPIA6pUuT2QhRCHMhdVlcLS3unSeznY+NQHw2srb7Pqt9YSNWvW1J4QcihVc6uHvn6r5fQWNy+61gR8gXbyCRVT/VSjdn7Wq1dPRAyQ35OWX0r3f4HndSwYFne3bd9G69evn06G361LD4RwZpdJOyL5DI5G/lAmnDYAhtXaCE92iFKkrr/djlTOn7g9qHz9PY8GnETpcJPUboVRI4eDk/kRo3vTsGCirVJjqkhA1O5b52ZxSL7mpVexhnyTgl/uesGSVvneRXpVlIrMqI9Vp6zH4JP3UEUq1g3enMvMFrWtSDIZZNGNBqxMcTx5nA3l4FRYSq/6/rUpE9O1lIpCIeMSnYnTHsDLy6tEC8mfW52Nw6UiteTwRXDflJHG40P2VlQJCmWemIXWrpVz/8FDpb85Nv7ogLX7MjPfJvCzaTTcyueLEkm+9b7fjra2mTvGRuakt3afTGLIIdttS5S//voLvQJmlWjRbrXxwHjOzcd5IwlV5Tw32mVvoGbQQHlogG5CN+fPDDtLo9Okf4q7YgYh9l20XuVytHHH2OBPP8ECQS7OvlsKvlUCDv61FyHBtTGzYdm7Pw5GTcXLD7eVkrtGtSD0cv6U9kcphBJAkSkXcDd1J9q6fQsXq+olerRbvf/hKHannhETjSJ6aZ71kpsHkTtpzBB3GCoGDRg0oLQGrOjm77NClopzYzk9ni84zo7V6Yfd5by/sWBsE9AYxQtRlBZYASB3sb2oT3680vMyBaRwuV1PPm0jCWmlYUmqcUgkeqCZw7agQAROS7aGkgDzefFYVeNHjelQBN7np+PJ+4Gwo2t3gXGjbCeRxcwkhddQxE3BgdggtNHiwkWtKEVNImFH+Lj6MhEWFp/sZAkJCaAvqAG2maQa5DltFze1wOxrOdTK0bInBmrKqAGaLwO0iMfsQHM7+qefUwGxc/xefRGYptr9/ZeU8cb7PxGdfRnNnCeplaqBWpSWXmMf1m1YU5Ks3Hpbv8lo6DZIbn9Rx2nOdCxZvgj9+wyCL7MliaQ4QRwxZdvLvohPfFsEqpcjy9IWU+uclOLFJb/vNXd6GMIkS2nG0KANDdC1QaQC0xBl3fGuNM7asfNT4ND0I2oPYOJY1B/oOTUJkTEZyMjMlOmspXYk1CwnR/TDZD5Ya3LAo59VyVlL3SuZZDz6KP7MNkqxmdX4EmxYjlixYgU+fvwIoVCoFJ4yQEFBQcjNzaURp8LPI0eMFLJZbAHB090XujJC6R+GZsmwzGrcOExI7tl7XB5X687aDx8+4M8//4QVkw2P5LInAJQKajq2A7UrVx/F3NhSaTbULvVugZdQdbsLZl/lKY2nbcCbN25IkWzSpAlevnqND0kZWLA2HXey96HzdF9Y1E1Au68TkJyqm4UPUoJo0EAnq0spZy0tMEYDKqqhzl2aZnDWqqYypaGpvJRJrBB0HpGoNI4+AH+fYddKH3wMPMpdA03uDmSV6aytedRcKkTXymca27ZUGvzsG3x4eXlJ4UxdfVhKNoYxjUR7sLH1ZdM5UgiqNRi1a9dWJWftzp07MbYfC0/CPdgZt73d0m959+Le85nIuevtIM9ZS4k0ZYD0J822zStUk1ZF6Bt73ayEEd6HHGzpVCyxaiqiG8ANGqhUGqCDvrmt92T+7CYXZTprqcF0cPoJL14oF1a4rMGTxa5ITCz73d6jTzcxKRMjc3TymleYJ233mHTUc+kr7lNUadw2SFG3uK93z77o6bRRfK5qJcChFYZW36FTZy0lUx3XHrheUBzi0J/o5YNNCPMBKyCd7Nyhth7pzvqrqlIM8AYNVBINNLP0K95aRWR+VGOa0WreXJ1K39ysK3irq4m0zcR84AlJD5cGDGoumKOVCa2xm4XGRrjImHwNRlKMuq8gW2tGouDIPwTadtY+LrgJ83GPFF5DkVCA1qeCPxtnLXV1rvc0xrQmzkhLSyu8WC4uLqh7qKD4wv1Xez/cDJs2Sr6rFy9ejCmh5jjd0/I8AesihVR+DX4M0HlvrINev7cJETtrKXHm5Cbp1FlL8WjsPgxDqm9Ty1lL4dPJZgTTR+2xY8cO6rTM8jhDuTWu1pn1cO4HM3wdcJikgZwodtZSDDZtXV8mn+TkZMoOXyacMgDr1q7HxNqyN9FSzvTZTS7Ra9i1oJ5fyk06lWFqgDFogGhA2rrxmaiF7Ky9XDpkWkUb2rA5ycTR8QGDfqqHBJtr2HgwCympZIfA4yf46quvyhSXyj+m79AW1+MLYLWGh0OxB8iD6SLhr9r8jFr5XLdu3TLHpg2A+5m7lCJDjWFS7eNIPdoQY5ttQFxcnFJ4qgJt3baVlpGZQU9PT48MCQ4RMswZn2YaqhKqJPDEkfLA2tpaSHYZCzk8jtX169doRavhtD2EprW74PW2avim7mnYMqjF42WX7c+G6C33QVxGcTiPsiX7BDGM7FpnWV2C1xZrjD8nHdZFWTrqwn0/d06ZqPXr18eJf8+Aygty5gYP1g2iMGdnH5jWeoumgxPIJEnj764yZVAXgHo/ODaJVRddabyd4Rz0P+Jq2FmrtMZUB6Sctptf+WHqwlTVkXWEMbYPi862pD3UEXkD2YqhAZNe/iYXq7AUb64I3JuP128/SEm8/69wqTZdNmx8nCOTfNu2bfHLHdkftBHD2JbuljTZiDKpSTZWr16t4PTpMwoNSpIYwNTJY0s3KXXetrFkbisK6dbte0rhagJEPX+Sr3ox029WeWFuVqgrfxXoWZiaYpGJMfK6tWJmbv3JXnR2i7PoyEon0dh+VjkWDBr1ULNSgZ4B1KABnWjAjM7ImRF27usGHgMV/p4dmD6Y2X0Xnjx5IlOOyMhIDB8yEq5OHrC3dcKEcZOk4NxcPNG12lz83O4h1vaLxaDgVXCwdsOmTbJTw2/ctA5X3kkaZouItqgyrqgq98jJ+4htOzbL7S/qGDN6PKp//IYYDSuH+WQw732R6OIjlZKDOG4tHrIC0v5z3FqKO7+8ihGLZZVkaWlJWfsnf3nDN4xYVQ0scetuXRLHxYSNpo7JwkThu5LNWq9fZr6j8V/+rVW6Ru4N8I9vfa2stLZrVA+83VEay2fkz9LYMf0ylvo5a16cTNkay1IkRS/T8UVVrR3HiQaJ6FYuCul5bnPDwQ6q2WoVEqwgnSubGmNltyqgNmxQJfzMZfDIzurS5eTK2RJNV86cKDxv5m6CJ0NYf5N0Nr9JAOj/xNPik6M26r1NMIMlIwrbeZqZ/qVSg6MVCR3+eKMjqMhGZZXj/8h2fJbGa+A+oHRT4fnOyFHo3LmzzL7p02bAz6U2RhI76uo+Mfil/SO08Z2EpmEtJeCpDVo9uvUC08IKXh5++N/0GUhKSpKAKTpZsWIlnmy3hjFd8bXoGbiINq7uvgNmdLNXRbiGo0EDmmqgcnxxqDhKc3P8nHCpSjMV0fQC3nViElgNknEhcTP+PMYhjo4c7Nm3H9TqIFVL4p1PLx1V8dSB//dNfuGO2n8TjuLbJpfUXulz7cMWmJkpftipI58snISk9/jIi5XVJbPtfVYEZmxtDW9vb5n92mokTkw8evyIRvLb2ty6dUvk4+0jtLSwpDxyVbXFo5zo0I2NjN8xmUzhwYMHReRFWJs4p2n6CH/9Mv4OTsQsVGnYfx7YoBK8JsC3k3eqjT667l64218u3HE76GTxanm1CSqJuJvkrKXCzapSTCnL82+/IT+/AFcf8JBoexGBvUxQs1syuLyK57yldtpW76y7j+yr93Pg/psN3OimqqjRAKuGBsyJAXX8v+7YsK/ipAzfstChlhpDMaBUEg14enrk/RVVYPwyTYBTb2QbiN6SfKcvPqTLHFHLli2x+qFsR6lMBA0bIwYzMWvmDCkq1ArkRgoi2UWNtDa3MIbK4R4YDHNhZOQLhc6d0sK8efMGUSedSjcrde7ubIzz56lF88VlVE+JzTDFHTqoWRPHfc4Db/Psu16v6tc0zWRZ0bcTNkGlWNUmU+ClJD183vRh7DzuPS9u3kOfb/Mf+5geW+3EGtmThTaNLNCjDRMbfnAw597zto045p5lyaTdL0XHcGrQgL40QDM3shTObHLB3NRIelGELCFqOXXF9rHv4edaG94e/rBksuDrEoLu1edj77hMeLwdg1HVD2F80DHYRQ6Gv0vdwvkmFeUo0K0FRlY9iFrOXcWkfWzqk3yxf+HdnmD4OteWuYO3Xg8HMbyqlesJmyV2bsjC7/FVb9hG9qs0zlpqDO2JvuUVz/8ct+etqnIsQX8tD+4zbu/93XdzCjIzsxw5HI4xMdSuMjU1if6Mx2sYmuYa6Btg7ixFZax9GP3H/DZa240pxYA0EAcAzP+ZrRXnakn6+cbVSp5qVM/bGid7IqwCVeMAG8UrIJWg9UpLDtufXbpqxdM5Om4vvmZ8p/G4Sg59bvZwocXUGIXza9NVjnjY36QkGpbey8WEK0IkcCueTUZCUCVO5tQxwT/DqoJaBFanTh10vOkhhbW/LR1rVhenx7N3LP6+8Ce3WsIY69kkrc1zKUTdN9BJ6OOMCHZg3Ds5jlpKhA/CfPQPpT4lKkexNLXF2UX5WEby4CoqVMq1yJSLikAU9u04sEaqPzw8HA29e4P5oDcGVF0PN1agGKaR+2C0MF4ER2uPwuiK1O7sFv7DUTNzJqbXO4shPrthcb831vR9g/Z+0+Hu4Ae2lQ1cHD1Q3a0hkv8KhQNTOf+AvYUXZja55G9MN638PzKxBg2V8tSAwgd9eQqmAe+OUSc9TvpVoDj9WRwB3FqnYOu27ejbVzu75Gd+MxkLhLtJCALdXsLNT/Iw8yoNUxv+DWU/1BVdu6esJQj/u+yVN4poKNu3atVqJBwKLnM1TBG9N67rsHuf+o61IjrqHqldimNHjxUdO34MnGwONbuYqi4tPeL1YLOsDwYEBBjtP7CPVqVKFT2ylmRF5QveOjoOVmb2kh1yzg4njELkq2dyerXbTBmRLy3UjuF43xOSX8LyKU7owRAdcNgEb+JlrzZTVUOvXr1CSEgwDi2zRpcW2tGFqjLIgqec0n2nJ+PQ8uJJvCw4Vdui3xbgem8jdDGVWIytKhkDvIoaiBSQUP3LMtG2cfnmgC4Sm93gTWRWtqhG0bnh+HlogMFgCMk7W2IC5upkj5j+kt9n0RkC1NiRIdcZEOZrh/NdRHpTSna+CN47eMSJ4oqsbC6+C+SivZcJnCzKXr9pty49nuS0VSqEBYNheovHy2ugysCeP3+O83taYfIg9d8P9k2S8DHt08Im6tmedMULzg5asbmpMhQJ2Gev88lcHbAkOvZQ4BiXQJJxYhoSk1vAh3IeMxn4hiaDBtTQQPc6zl8d6Vj1W4lnnRp0ykShItGwzV1gTf7KKpy8FITHfyOVK7dbte8R4ty5LHSp/t9vNEd+QZ5Ue1GDvY0jxgcfLzqtVMf3d3pgGUPayVR6EGtykwW/5yRu4UFY9pbk0siV77zmuXPnnrZu3VpC8pMnT1K7dmxJo+xVVhLQhpMvTQNsuvm7jJClcudAFo+miS5ap+v0WdnEdTjf7KsdWpvUUPOkmgG7UWWw5vbJ464BQuvrrcqeTCq4cXL/iUNGG83sDr2/SeJvuuevkY4uF3DQw3+aAkmV77J89J3ovPUHrd4XTapM5pt1Wi13jLxFVuBNLrY/XHpXgDvB0/DD/IWFgmdkZKBpoCfu9pZLQvkBljPkVRL98U7wDMz94Ud4ONkhC6NwTgAAQABJREFUqr/kN9Xgc0IcfpZRKOWWLVsw+OksKYmDd2ZyozKErqRD56u+rWB0cS3To0VnJexDAVlRGBN2Tkreit5ARdQ8mz0L9x/dkStqoEcT9PRZKrdfXsc/b+bj4dt/xd3UM4xtZVsYLdOILrlAQQxUohKX8bDQVmzLkHbwlwDTSnXzvUGiJF4MxSheKwQNRL5IDWj0Uq2AGvM+vsapQjlr2Q2TEGMWThxwXK05aydPHI+Zubp11o44zUWVLWwkCc5iJnlRaMVZm/SvUs7au3fvFoYlaN+mMy5cuKDSbUaFpvP1qk5CZ7ni7f6qSjtrNz/pX67OWmqQVLjgXXt20bI4WbQrV65M6V1jkcjTKlBgQjcX0kCjFNFBJWVoH7guIbnX3IiZb2vuJuzk/62oT6/+RzIy041v3rpRrs5aaqhUvuCTKTOVHnVvl61ws/OFuwvJZ0ruGz5f+YWr79+/R6/u/TB08AicOXOmTJ6UUWDfq4llwikDMCB4DRr7XEbzw60QsD1XGRS1YSJ7F6CGv4/a+CURq1atipycXDQb/ApW9ZNI+Hfl9V2Sjrbr1LUnOyGx/M9Pk3lt0f++b7bBWastZapAJ4DsAHo4zQh5xDFVEcqyWXbVK4IcBhm0pwErpkV8aWctRf3nRYulmPQ+I5LrrM3JycGEavKdBFLEtNBgaUpDyhgm7nTMxIs+fAytYaaUs5Zi/X6MtRvZIjBcCTHcDx06Uqaz9uXLl9i6dSt5l7YjDmQWbFM6auSspeT6eM2pUN/79+8HnU4vd2ctJVOgnykCfE01ctZSdMguXHNLC9oTqm4oBg3oWgNkR9ezMXV2HdWHs5YaSxXrUKWctRQsFXpviM8ehHp2wvXr16mmwvI4/XBRVaVju9YdZcJTuXgDXJtUWmctNagY+7Yyx1a6cZK5oxHZ6TO2ibEllbc8sHT/53RuzWY/Ke2spcbXqVMnWFuzU6k6eX/MX716tZDsvhURW4SIWqRFtRvKl6uBMEtfS0Wj59VaTuue5avTnbZdolhatd1S38B3Rk7TygdTzfnfaiybkQ9LkYqV6nv+Ol9j5+icnAStXEeuIA/brS5rLE/JgbfhVlXorLVe5yThrA0niwa9F90TO2spWlS0v6fxWWCsqvxrU5q6mWBK8grYWVng2p0HWPNI8rtqdxs67FmfFnEPGDAABQLp2/3JUDbzTC/LTJYJ7pbUtZbrX40ytc+JtQlSylmbKxKiW8haLYugH3ImRuboxF4FRxuPwrRpsri+SXkoq7nMtris4oBDa9esQ/uqUzG1zimSprFsZy1FvIp1bZI+T/fOWorX6Lp7aF9V++Edqa6kzg3FoAF1NKDVF4g6AmgRhzF7FDvjt+l2FSL25DKyoyKFNgq//S5twFNmzF8PG4zLp/+BJ9sYCdl8JGblEacpHR2qGGFDC92thqq/Jwc0ejN0C1igjJgqwWyO7IXEZNkLTI4fP47BA4ahs8/38LNtLKYrIi+r1Y86ITNLOWdKdHQ0lva/CyocgbKF4tFtqTGofJwVpbAsbQpfPqXl4eanI54TgfisCFES5znyBDmifLKjDBCBOFJhbmxFY5rawdLcmcY0sYUFya1iZmxJtRcmhKfo5fI5//1lIzv/I7h5SaJskr8ph88R5QqyIRQJYEoCdZgZMWi2RI/u7Fo0N1ZNmYaU42/m4fHbirXyq3eNXxHg0Kq06hSeG7U4jfkLflQIU9S5a+du3FvHAsvMsaipMPT2ybcLMWxMHyz8SfZvZ8GCheBfaCfG0VblYcJxXI5diXdfm8DcRONvJZliLXuQj12vjfEhMwdOLHP42JgiNqMAHn7VcPL8VVChkFUtZHU7Zk3ugQd/FetRVRrahH/6Kh/pWQI0q6v5Bia7OnGIYgRpUzwDLRU1UDfvOWLuuquIpRtwdv3Yg1lcYT/dUDdQ1acGTI2NjyelpHSljB2lC2X4ypliI26+8YGP0M1vCw0j4sb/Kn8sW4qUwwvxY33dzedK89TG+bZnecKJ53lMQkvuaiFvby9hTMwbmd8XQTV80KN5Fsb1Y8LVsXKNXRv605TGuwQ+PNu8DSF0DI5bTZVpwJerARO6mWBW2EUSxEnmz1guXnl05BRk4tC78Yh7F4Mg91bo7vurymIIwv7Gr7/+IoFH5dHt4bEcNgw3ifbKeBJx+ytstaA2DylX4kkYxsaZLyLJF2EN5TAqDxRxxC4WCAQKV/dS932rVi1E589flPgB/Pd7kGirPCM3SKqpBiKqzxUFMhRHAHhP7DQ/vuZijMU8TdnJxW+UW0VkMS1Oa/chP+oUuh1QzW4iSziSCwsXb4+EsTM1RVSviHL4SKI9UA/5PyyLOm9E7xkhGunHLSNClFNrhUY0KHGcn8wXHGPFkrWO2im5why073ANxr7SC3FEeRw0PFIVJ0pEco4jdo3oPvsKF6PIk8CBzcS74fpJVydPBm2184Ui7HtZgCEB0nap2Tf4eMGogV5mTwsXq8rjOe48N+dAZP7yXCHmyoNRtZ2EP06Psg6yZpD0TcoW36yXmBSm2sYlZWnrE+512k0k2JzApSuS9uIA/yD0dt2ssih/3GmHbF4mbFh2ZFftP2SRrvI6VZmZlhBy+dlYdasrL1+Yq/7DUUuyGMhUPg1o/CKqKENuGmqWfWWXW4X4EXi1S8bdhzFwcHBQWT2rVq5A7pF5mFJL+kWjMjEVEDodzEaOUS+SlHuqCliqgfZaaY5atWpJIFHx5mdPWIS+/vIXnlAPuWbf5qBr164SuPJOAqqRF4Cz8i+A/a8mISrhnjxyem+nQjsMr72JOOspm1zFL60X5KJFixYVRlBXJ4/C3FjKCrTuYXekZiUqBZ6dnY3BdZciyEn+ZucLb1ehQXdX/LFcOszHiNqb4c7SjSOPysF85OkYPBnBUnrHlFKDLgPoJnFKLEmvg5PnLpYBKbubybQA967iD2DZmNpv3XciG83rmWvkSOg9PgmbnvprXzgDRZU10NTiOZ6dKX+n7eRfPuat2ZtlrvIADAgVTQOTHj9+vDo4OFhKriqujnhJdqyWLK2ueeDG/cclm7B61Urc2vIjtras+B+YEoKXOPHZmsFN4Ipk7jSxZFrkk4gyJiXAC6tcLheWlpYQPfMp3VWpzx2aJmDEiFHYs3sX4i/Y6WUsfu3fZke/51vphZmByZeoAdG8Zjcr3bgPvJiGftWXqyW3a/+HGD9+fCFu4wbNYJPeEKEuPdWiVRGRniSewJbkv2GkogO+E+cV9zafR61mfl4Rx6WOTMOGDRXu2PGn2vYva7ZVWmZWtn4e9uoM0ICjKw30FtVec0gZ4mtSrgh9OIvodnQnZcBVhnlWcA+ThruAztLeYhLjU00E7e6f19ixeKxBDaHNfs0muEdvXUajWuovnmaEvhHFW2jmsF1h6sJf6NpZ41WFvZ495s9iLteYTtFN0pDjKGLOSpZ6fgne38asqK8wp57k9LtHRDBOnb9UhC73OLKWFdY1l8SVC/wFdOx6noeMdj+Kflq4UEi+aSiD7DM1h931a1P7Q78z3VXyiL8kIYX3B/wusTlETf4VBu3825Xwb2KOP3dtL5SpbctOaCj8QWX5BEI+rsdvRzOP0SrjljfCz1caUSJI/X7LW64i/mQDzqHg4KCeVf39cPzvf15nZ3OrFfUZjuWngcprMSqhM3sbOreiOGvZDRMR+56jlrPWy80JX0cv0JuzNpcvQsCWLBzZao3duQFo5TOxhFblVzNyE/Aq9ap8ABk9SdlREs5aylHraueDe384KXTWUqTMyQ7RldNPFSYJl0Faqiny5VPc/XBQql1eQ/uexTt6KRghWSE4edJUONl6yg3jII+WNtob1W9aaZy11Hi7d+mtjWGrTMPdrio6tO2M1NRUCdy7D25JnCs6oVbmP4m8rwhEoq+qZ4hCZy0F3MpzSmHC+9oeHTDn2+8k8J+nnZY4L+skluT0Ssx+WRZYYb872QU9wcQF+Yc84LYuE/eTJJ0HShFRA6iRqzGOBD6GszUT1GIDVQuXy4NtWJqqaDqBH9DZEr9szEBBgerjoAS6+zQX3z3y1IlsBqKqa+A0tzq2HdZ5OpoyBVs9196MRGedVCagAaAiayDQ3oa9Spazltp1U9pZezauQMpZS4XkGk3meJXZWUtdoJhR1kxLE0SVvljm5mZXk1M+Sll8MjMzMWes12fnrP3fxuYktD8Pi5euRlTMB72FYX992pNylo8orX/DuUEDWtDAoFlh57VARv8k1HXWUpJSaU4sLVgYEPQHWpsv/qyctdT4gkleX9+sV1RVpXLSqirzCbvGM0sYqfbRrxIXvQKLnj/8m6bOt0qRlKvXrCsOo1HUaDh+9hpg0xnLlB3kJIdm9Bl5YToLoR1oUhe5G+uo96EqZxCcJKmpmxxIxc38pxkaOyOu3pcbwEUx8/96WaZ0jXTPJ3bAGU6tNHay/pjwD7TprD2fdxQWU6Kk9Es70geP6D2knLVVd+fLddZOmzoFvbt1Fuvzr9f6sRmJGVbwSj1nY8ycOYukqcs2Ivl+I9q0aiG0ZrOoG1PpubcF6AnvrIOPq+qspVQzmNzBJSP5VXB1KSVea8+p8Hw7Fh38ZsDB1gkvoyKVwisNZEQ3rpTOWmocQ4LXUQc29a+iFTMzM0Fubm7vu3fv0ffs3U/ncLKrurs53Klocn6J8lR6h625GS075ZqXRUW4eJb1EkjoXq7KolDGrNruLLwg+SJJ/CmV8VVF+JgjhCtx6ETudsZ9syB0MGWTnLjJhHfZc5PVDzpj9e0eOBAxE8vutlKatSPTD/VCG8HOxhFNvIfg/nJnjKq5V25ut9KEG7kOhb2N8rvwPlreKE1C5jm1SufnXxaC2v3Rp2d/+LmEYmqjY7B+OgBjgg4iJSVFJp4uG20zJR3IuuSlDdpfB+/B69evtUFKJRpNXcehXv73WNo9AnWrdENg9RA8e/YMbm5uuBOvnMN+69PBhfDKMK5RPRijgvYoA1oI08VnPoxvdkNH/5lgmDMxYsjXqGlXPDkuixC1CmvX4wnYfH8YTmdPKwu8sH9lfirsyEQmyioY/qd9ELqRi+PR+UrhagoUO8wM3zZi4+pV1e06aenp8O0iJI5Sjb6xNB1CIf7a7+3RbOgHtWjtG82Hl5FKiyjV4mNAUk4DFiRMjvFiS/DIO6+8i7O90aLylsHAX20N1NjRgflkfRhfaoL29fAhEmGQiziMulwMSuVH71DNGu9HfD6brF+PtPYzo2Ny0XjJ8X+bN29pQvL8lWgCyI5kLPzGD6u+0zwvmQThcj5pPzYTy1ZuE0vBZDJhRvID66t0asZYrS9eBj5fjgbsGJ67TI0qxCe1XpVOv9YV0+qdkUjJo1cB9MBsUuMzWEfS36ha3OimiLMJasIEnacqbgWCZ1kwaFwqwsOdfTbo08EB+fnqfRsNGTKExmZbUXl+DeUL0kBDS2+VJjHPA+bRW2bYaNWpWlLdNy0TaQUPd2iNvvngf40+Xr9dkoVada8hfTXO/Xr/mXq/zSKBrYXGGn30jebFgWUkOZctoq3KcXHiFa1dH4rvPLMFQpq5pK9H+IcDUtrdQhWW5Oborsd5eJOcKSWuu5MdLvZhYRF2Y7f3zUI77MoVy7G/Xdk2YClin3FDdVsjZGV9WvDNZrNxloTHT8/INCOLfbatW7dOZG9nK7Rhs9KJCsbKUINFFbppNskJ70zZIVQtrbLfYGDodlXRKg18PbfeGBd0DMN891UambUlKJU/l6Qcoe6bClUYDLNI4qyVSoPyz4mz9YigPiWEpdJ7UUb2viXaDFUda0D1p4iOBVKFPJNBi8t54M1UBUcXsJxsAdza5CCb7BRTtVDh8U4OdMPNXrp/UVKOWud1GUg/6IbXxKFTjSQELyoXldgZt+XJQGRwinczmhorP5mhdqB0sFqOCcHH0dJzYhFblY6Tah+Hk71yTtsbt68gKy+5TPrR6TfRLKgX5re7ieqp32BA1XXiXK33Eg6hSpUqZdLQJoCzoxvqVLIwXAySJ7dFQ+UdkdrS1/WkjYWkjOlm6Oj1HXo6bcT+CRw09xmFS3EblGKzZsMKpeCGDh6BHo7rlYItDVTXtRdmNDgP97dfw9UqoHS33POqTg3FfXfu38R/YTTEbbIqbf1niZuNyW/umkUNtLnhj4YbcxD+WrMPIDFhBZUFDYzhtqMr2rVoqgBKdld0zBuMX9kCR85mywbQY+vNfW5wbhanEkfnBnGYZ+yqEo4BWPca6GJqDa8m73TPqAwOT466U7viJL+0y8AxdFcIDfS93Mcqol9VU3pnbxPk5eVJCOUTc1TinDp5kSZAzLtPiz54PB76h9ghvEOlnm5LjZFtRsOtgaxVpKMDk0Gf/fqUx9Lu1b6Hi6MlvpszE4tIPkhrFgOevK+wbKZKdk4pXhWxofuABWKxQoL89b57+MR6FyZJHT9PLIShYtCAhhowpTPyxtc7oL9VBxrKa0BXTQPUrpQo/7mIFki+w5Sl8tYmmDHc1D6HwFMGvIpUqJdra/I3nTwTl1gx6dttWLRwexujq8RJm9GyPiMj8bJnJveet3glwuHlbIzu64rnz58XjqNn9w6FjgtFO2/Pnj2LXgRuwriRWLTod2o+Z1+IrPo/P4LSjPwZfmuq606fGA0YdJPVFjST9BCGW/qeKkOtVWXOq7Wc1jOrqsYOTHl8HS+s09q2SJqxGS627q2xgzFg7gyNDZrP3pFVjhoU9zxTjbYLnxfkaKwHoUiItVYntfYbH8ftLLAY/0j8ISFMiUSTgy5IG1Nsyy1SGbVQf/3ZCBgZSTpxqzkw8Lq/CFRktKKSM8UGY9/8hJYeGqmsiFzhkbEqHUH7CyTaKuPJ33//LVNsKn1CysdUWlpGpjV5Z2w4c+aMqGlYIyFJ+yIgF/zucgsPzgN2DbX8Ezvy0tCu3l8y+RoaPw8NzG5yiWZKM71fkUazZMmyarLkCQkJgZ+vb+GuLBIuWUju9/3krw/5OzByxDBqYYxyjhlZxA1tSmtAay8SpTlqCdCKSTuedce7q5bIqUVGSBKbB/VIw+IVe9C5s+oOK283ZzzukQdTI91ehmSeEME7OLjGrA5XslJWVvlflYkkt2ZNWV2FbeSHiWbzOGjXrl3heecOXRGaM1cuvC47fr/RDPkFZTufvJxqYEj14h0Qqsp0VTRPKkF6EQ2yugrzvp2PALu2sKDb4SP/FVIFL/Em7rXEBInS24kTJxAREQEqlGKnTp2KSEgdKaf23KY3lN51LEWgnBsohyI1Xnnl5s2buH37NhwdHdGnTx+YmEhODjt37Iaox/HwMG9Y+BX7inseX/Vuj9VrV8okeeTIETxcqb6D7P6HozgZ9btM2iUbe3TvCf+UiTAzVmvuVZKUyvWSOhUIBBhZZ0uZ4bL97w8A5aSSVVrzXmBWayP08JP9HJCFo26b1595eJucDvKCV4lEQkICggKr48ZuFqp6qYarEiMlgF2axyHhctmLNk5e4SF4jiPUWUmphBgGEC1oYG3/OHw/qXwj2bHrv4nN4oq8tTAcAwk9aIBlgj/uDmZP87QS20jAmfdSnPJi3NfDsYJ5XEqSNje9cO3Og8IIHQs6+mJpWLFxRAq4kjeciS2Ax1AWagdIG40q+dDkit92dCbOXi/eqbZn958ItpyHoKr6fV/1mJLIDT/PoxwHhmLQgEYaMKGZvJoRdt7fiC45L9eIqAG5QmrgUvQqHOQ+g5kaO3+oAb0X5qNJ5surHAgop2N5FQtrK9qd7m2YPr9Ps2M42kk6JpQVKidXiFNXc9Cz7afvO5eWaUhIkt780qxJQ5xdkywRRaHhgHju7Sd5yjx//zQzMx0yZsxo9O3bn9akSZNC8TgcDn74YZ5o27ZtnKysbMNiPmUvmvbhqtNBH8Ckm46yNbKw/No+zGqATSjd18xBK5xi81KxOEaEYYwZWqFXmkgYq7PAfNAJ9X4ApYgVPNmNr473KdWq+umJrxvBam6o6oj/YfDCjvKzzrupPXGeHMrBAgs3tfkP4OcVnPabqNHL0CfiZ/4+y9dqj6G08E0DFwpMm/1QeJ1pBzvjVuOH8LWWvuxLHhSg6+qrEmnoKFputlaIHqLRkEqLJPP8xgc+Wh36tDPVx84CzwdX3m8D9z8L8DFD+WAKfm7uuMyzA0PNd+sLQQ42ek8r084nU/GGxkqlgbScd1h3ty9lhP62vAW3sLDgk0ij0g+TEoLVqRMqWrToN1qRH6ioy8/PD9HR0bp1ZBUx+4KPlVLBZmaYnnipyjLrUuEfdH0d+STn67fL0/DXORrSMgW4QRxQgYGBKrOlQoZumtgFm1sq/G2oTLc0AuU8892chTNm8h21FM5g3gfUaXCsNLrE+Y7nQ/Eu5ZW4rb5nb7T31s3kU8xEQWXf22F4/ealAgiSS7R5O4RhoUIYeZ1X3m3C5RhpZ+/GjZuw9ddTaO81Wybq7Q97sf7fqfD19cW5c+ewavJFhDgWrytIJWFWbifvwuu06xg/YRy8fbxw++Y9HD/6D8bXkt6pI5NJBW48HDUdPkH26DOgJyjH28Z1W8EQOaCB/Qh4sIPFklM5jbN8T2H/wb2FTt4A94bo4yfbMfsg4SgYNWNx+MgBMX5RxZ7torbeVt7rhCxuWhEpmceli/9AQngNWJjIdoDKRNJiY3rOB6y501NM0cPVC8P994vPZVVWXm+DWJa/rC5xW3B2BM4OtIA3W7fPoH0v8hBb/xss+OkXMW9lK1QomqZhjZGY+B7NQvkkpKYdXBy09u2jrBgI7fUeD/5yVwjfp2EaNhp7KYQxdJavBtbnJ2PunfL9cFy2I0MwY0ma/m/i8lV9peRuboTs1AnWzJJpKjLySJSSDRni8cxqYImFDSWddF67BUhMzcT169dxex7ZjROsewOJWCAdVaZfK4B9x8mYRXKyf//dbESf2YWD7Ytv49WPcjFogQNcHYvbdCRKuZNNSOHjdvpKdO/eXUKWFy9eoF+PRnh4yAZ0uv4+rUxCYuaQvSi/SQhjODFoQAUNUCv9p4edDTWWs6BXBVJaB/3AeQ7nlO9wncOHr88SlaLUaF2Yz4jgngfDcc+EodGI/DKe8tJFAmp3xSfrvEbUlEZmm5vSPkSd8rBwJzkGtV1G/8jB5kPS0bl2/eaIId2spNh9+0dq/pq9mRu4OZhGOqndJlQJsWHTlqdnilpSi5Tr16//qVXB/4ED+4v27TvQg4AoNsYooGHoUk4DpjSj65Z0s5Bhdg1NBtnUNa1j4akcogZQS5PPC4Oyl9Ot6epuypbPfBPvZ+yfNgc0una+55l3ugqbnzlcvEpRPmu5PeGOvgLb2x3UFuhjrcNC/i1PtWXYVV+IznIWrssVukRHtENb1GOWvVi7BIpUtWXEGf5vlnu18pBqyvUUmM54ayQSFMBsrRs+jDST4kfZfD3+zEdKRrZ44wfVRm0GuXHjBkL2dYaRHuamdxP5aHrg0yuhT7eO2OV9W0rWytJwLq4AnY5kkjm94luROKzQr3YDnDH2UHtoWSIBRls3RkPP4WrTMCAWa+BhQjis0rcimV+A0BqHYG4s/f4uhi6f2kdeLDbcGzCPcFfdSKo9kUNjYmLue3urv5eApIjIJIvOysdQrj09VGhK+rMqaE8NTR4fcbsaXE36ZaU9FsDef7KxfGsGMtIFGOxngkYuxggjISR6Pw8mCdwvq83Kr4obrnbgwsZc8cNfbQb/IQ48zkXHFBf0MrUpk1QHU1e0J6Hs5BUheYm0np+HVq1aiUHi4uIwpNUPaO4+Xtym7wqVT7dkiObS/C9duoTzP6pnoI9gL8XR44fFJKkwiA52zpgSelI8ERJ3lqokZb/GmfhF6OK5EDYM9Vf4lSL72Z1S99X2Z0MRatcXtV2+KnN8uyNH4/S1v1C1alUx7MWLF3FhvnpGh2fsZThy/JCYVulKdf9A9HBeByrkcnmUuIyH8OuRih/mU+/yT0UoFGJIrTXwsZFvAKCcvHOjFpS5ej5FWIAwXiTixrJ0Pol3356LlExumb+donGWPh47dgxtzg1HdKYQ1+L5+ItMop+mkB3Hvazw2wy70uBaPRcIRBjz40ds/Vn2auvuo5OwLdJfqzwNxHSjgXrGzxB9Sf0PKm1IRQuM6ULonNAGLQMNnWhgyJgg000rWzKlJg+bnuRhykVuIVPKCEKFEitZWh3j40ZsFpo3qocN1V7pfEFMSd66qgfuzUd0SrYU+dLj3/YsD/XH2KBuzfJ5X0oJqIOGszd4uJMwFnO/ny+XOhVic8rE4fhpdDwa1ZK6heTiqdsxddHHvFW7s3TPSF0BDXgVWgNmdLPXUxqe8C2PCDLKKCb8QScc8HcsBI3k5WIXhiHIqaMyqAaYMjSw4VZnvGRq5qy6xc/GAE7MwywIQ8tgp2k3jaTBSrx/yM2xmrfkIilNCZfEv/U4F2TnbMkmkHCXaFeGrSM1Q4DnJAxp3UAzMP6z7wz7Lht/hidJ0FJ0Qi3ybtu2LWVUUS6njyJihj65GjAFLduUbsLghPyhW0NcKQn8ny8Q7rZ4oxOeDbnOIuaMRK3YdHnragp7p93XSM6zdVoIzA4Hqu2wTfIjUTcjfNQeT3RjC9iQEPDqlI25KZhTfY46qGKcbak3USNrg9o2DzEhUkkVJqFXT/JMir+FFcIFGFZD+vm383k+Ult8izlzi+1Fo4YOwoebx3DyFQcs8jmTMvpTFIGStHVRp8IhF0XbW7JkCSa/X6QLNnqj6UbsV6lZPLn8vJyccSzXDh5G0tdFLlKpDkpfXc080Lpa+USuLCVOpT89+fJHbHB6A6bRp8dY3Sdv8L8w9X03ulRIYvZLbHkwfAnhMUuXfOTQNm1RzzyzbRjbfMj0u/DwUM8+dujQIRI9pK/az2s5shmaS2igsinXZdfvDu8HdykRn67EYDStUob5Ki3fYkZNU4wLkW3/eJYqwKvOGzBgwAAJdjOmTUHk6T0Y7pOHWbdpOHnlDgICAsQwa9eswdPtc7GiidrzFzEtRZW9ZEfbpatmWGGu3MqwNbkfUVBPvtOK4lUyLGtp3k0bt4QgwQmNXUaCYaL/PGXh0d9h1z9/SIX+KJJzWqOTsDS1LTpV+jhsqz2obf5UmT1rDqL+ZiLQsY3S+AZA3WiAykt8NecnPHh8V8xgQNBy+Nk2Ep8rW6k1JR69evWSAqdyDnq6+Kq9c1eKoAoN1KTtfsJfuJ+2G3Hv34DBYEhhz5pBomfcbKswRPP6663xilXs2JYiUqLhTEEmtrHjcbyXbifz1G7by3adsWPvQTF3aiGEi4MtVoXRcDXZCG8sa+LM5evifqqSlJSEtT398V1d2R9fN0n4nf6nubh+wA1+VUwkcLV1kkJyUh4njppRvSSfcVRY/MONaGhTDs8+bY3tS6ITT0L5OR7PJrnQZd9L+tCFWa2YnPwCiHOp6YOngYdSGnBimSImcay1BeWMlFWq7y3A5XsRqBsUgHcjpOeIlKHih0YWmFPv83Ba+u3Kw/u0Tw5qWfoIdLXC/T7Fz1xqZf0rYrAe1VvyOSkLt7K1tRiRjvDTMbC2Vm4R8bVdbmhSR/oe0cW4WfXfRHK4ohq6oG2g+flqwJzOSJna6KS9iZF+7lN1NGkW2wM97YqjxSaRl+f3mfXR0meqOuQMOKU0sPpGO8RY+ZZqVf10c26K8JechHccCBsS7C5sS9oP3ByRM5ki060t6a+zeMIjJBLAUtKnOKyRDNZsK9qFjT86tOzXUZnowzIIqNj0hH+0MIVREdrUyeOxcvyZolOljxyuEMKqj8BmF9+/ZSHv27cPAwcO7ETgTpUFa+hXTwOWoHPjbIIt/DMjBcnBS4yM1Axfqg53i0fTRBet02VPMNUh+B9OvCAGg/vlgu6g+TRAlJuB5uMTYenrrbZECSfOIKqG+pvFPzY+JuRfcFHLaRwbXwBWP/UXcgdlveLHBy/W6CPR/OFM0WWbFK1c54bZTiKGCZ2WPNKIbCCQJnkkKh/NN7+Em9unDSKpqanoHOqDS92N0P+sEOHPMzClrhUWhxXP1dW9sBEfBTgYxceddHNEp+UjmZMDZzsbuDg6wN3TA0NHT0S3bt3E5Lds2YLBT8vDDyUWQSsV9vosZHG4MCMhPqny6tUrNKvfAIv51nJTkCnLmCQFRZiIhr4h65VFMcAp0MCeRyMQ7iOEcYnv+Elv0hAWfFwBVvl2pfLeYtP9geECkaCHviRhMen7ls6y7Tu6N6vwOftneDaaD7wNLy8vtUSwZlnlZHKyDbYttbRXNpL0k79snPKCMBnT1yqLTNp18mUZ1i8ec3xN0EYJg38uCY3suT0H48aPw9mT/6ADOwk/NpB8EaaQvLEdToogJA/hPl4F+E4PBjyP9ZmIYAbBpMRDqqyL1drIBl1q/CoT7G3mIzAaPMWGjWtl9pdsXLVqFVL+ql+ySW91yol3OnUOXkQ9k+LZv+Yy+NuFSbWX1TB4kw2o8AD2ts6YWudkWeCGfj1rYP/LSTh5dR/8/f0xNGQdqlirvrh73F4P8QS3SPwRw75G9j0/VLdvUdSk1+PuN0Pw5m1UmTzDw8Ox/H/H0cx9jEzYZG405kX/DiZN+QUirUh+28MDzeCum/UwYjmPvs7HqkgzJHIKsL6pCC08JJ+d4aR/2m1j9BswEOf/PYFNddNRW4lQm+kkF1XwPg5SbnmJeWmzcvtJbuFO5JI7yKo0eYuHqKlNNgZaOtaAH+8J0h546ZiLfPLbjmRh1PcfK9PcS/5gPo+edWQY4+O+ZsPRQrF9qIAs6qOKidHnf/m6nhTgbFRm4Xjl/aPyqi9pZY1vahU/w6m5728kEsLa+Q7y0Cpde8dx6Th1pdjPQIXrb9e6MWyZH5FIDFhnL7+AnV2xgTAtLQ2Wb+uS/O36uU/evC+AT/t3tYhiH1c65RoELhcNmNLNc2aGnTen6dFZoc5Anz7ril89i39bFI1cEmlm0Ftr9A5apQ7JzxKHTxajvbvXD+8dOqCp91iVxrj2Zke8tvRSCUcesIAYn38pSMAefio8ScjiNs3N0bIxAx2aWoBa4LjxIAf/W5yaR6OLtvJy8BOhkyiHlheLSbuyZp69h6xQxHJwtNL8zYb2WLGKmhZ8KtX83PDyuHqmJ3k5cYtoyzrOmvk/0ZKlf1AOo6IQy7LADG1qaqC+ETP9FMu/cOVVaNZL/tPAn4yt9LhoxfPJEv4h1jONHIKyhh7GdReaz3iveBIrC1FGG2+Fu7B3drRGtM5u6wGzlq4yqJfdlD7oPD9vq5VaOjpznYe6s53LZiIHoiYnRvgh6FeNxr48yhWNTdrJ4aB88wrebPB9dmJXW/mq+NVqHBb+/MmOm5OTg+XtHTG9tgmxP4vwb4ut6NmzJx4NYaO6rfL2oNISNjkqxIjZv2DixImluxSer1u3DiNfFu/6VQhcATupTRSnYgtwKU6Ac2/4SMsBvjZ2wHSGk1akzRMJ0YbGQM+gFSrRC4/4H9rzYsCof1QlvM8deO2tTrgW6Cg1zLCIt5jU6IJUe0Vq4OanY+3t7hH5ovwgDeUyMzbG9ywLei8OT+hNaNHNTGl8CwaN72RrJGgSas74ZijbrKqXqRSbo+e4uPamF5q3bI8F3w0DTZSLU5fewsFB9ve8p7sjXEiU/9j3XHxMzyVzPCG1auSDFGH5DdSDbS6TyehvZGRkSsIqXyXnI8jfJ2OLfLwvrkc/1gQtqDWkmmn2oyPuWt8ClskRIuyrd7jXr/LuCNj/Mh8RV5n4zky1idH0nCR41D8i8+o8TD6K1SdJjtUSMc2pcKznz59H69atpeL5T50yDazH/WTS0lfjzfhd8GySj207NotZtvOdigbukruhxZ0KKhue9ECY01gSequDAihDV3lqID0nHmvv9sa8ZjdVFiNfkIPfr7UU461duw7rfj2Ann6/i9vKo7LkVkvk5pEZYalyk+TLrlWrlsSOW2oiSeXVkDf+1ddbIYZVrRQlxaeRRC8Lrd7gmI532yqWQrPen+7koN5ANrq31vrrApsOZqFXOybsrI3AyxHibktTBBkbFpRpdsX0i03licnbnQ5dhtUra0QkLPJgArOnLDhDv840MM6azVo5btw4k0W//U7btWsX+twz7Ngqqe3Vbt9i5qxZhU2UY5Z8TJXsFtcPHDiANpfHgWlS/DlBGYq8/sxC8k0vMVxlrvyyiYsdx2lgsZjI46Xi3GYWnEvkU5+xhIOl25PFQ6R2aIue+YjP9VEZMDMpZ/9JruFlpA9lV3IeNNCEc5vdKP7BVuDxrL3VgRjgpI3v1Py39UsORteT/Q1bgYekM9G87/dHd5IGaWZOIh5Z1USnAMofqlwJj5gOX04ktmnJcVuaazJJwTJL9A6MUCH2rHSAsTF5RpJruPNYNg6d4WZdvZdTuCHGhmXEnzqEzZo0kFUIU5qOPs7ZDT4gk+wcKypDuztg56/q2YiSSVS2FSf74ddFqn1bWllZibKzszVyGhXJbzhKamCKuWPBjwyS4+y/0oHzmn+k2mxjdyVSiBXhaHKMzkvB6hgGBjAmaUJGJm7TkMUC08azZE/WZGLIbsy/tVzY/dwEje6/Y9WqF9j827p4NZ9sVjJbOT89EHBn8tUax5o9mRi4Xr3QnpQw4VaBGGmnetS2ooHUilzM38h4Lr6/itpVPY7LaUwW0cfBzVL+ZYgmodgD/yxeWNnJ3xJHOn1yxHjvzENCOhdU6rBG4dLR5JSRh1qA6bczB1Q0NHXK/76Zil9Eu9RBLTec7HwReh/lwptjhR9N3cBSYeODKkIvyk3GY+eeaOAxSCk06n2598FQjBXmYrS5PQbyElCvQbhSuF8C0IobrXA7SHaKh2EJfuhQteIvHKBs0ytudHhEnLa1Vb1mlhb0jfWDTQeEr3K2smLKf2aoSjegewEiX72VQpP1nTvq++TcbUeyvQmwvIV4FJ3NZKf6qIkTx+PXX3+jFe1aL2JA7V6vVStElJOTS026pPMxFQF+YUftXVEdKo6E0+Hpwll7LyIPW79NqtTOWs8Nmehw21dlZy21qoenIKRUu/FuYmctFdLC3toV3zT6G1d+tkLfmr8VTgBKXvJVq1VbHVQSV1v1Rm5D4PZmFNr7f4NaNeuACgtCp6k3ZxoXfNTgrNXWhdERHSo/sDxnZVksjWhk9SFZgNCvz0DUrdIViQdDy91ZS8kc6vqVhOhcLhe13brg5Lc0/NTuHgI9mmDG/z4Z0amXJTWJTst5J4FTdDKh0RkcJSu2VCkBRgzs49VA/Y08ZOZVzgVO39dnIITkoZry00dVhq4U7Ji+LHyzKBVU+HzvFu8MzlqltFaxgKiPrya9EspVKLLacVO5CvDlMQ+1MDN9Rj4MhBs2bCDfvaL16RmZppSzllLFrWuXvzyNlDHiRb/8VAhBvWe4U+2xo4MlHG1YUoabfv36ocV5ybQTdILzdjgbjTq8RXqmoAxOFb977hgmov6xwP29IkSE20o4aynpl860KsxV9stUWzw47K53Zy0lw74lTgxLCxqPqhuKQQPyNGBGM4ue0/RKpXDWUmNo5v2NzKFQz6UL1VlYeaONVP+z5LNSbV9Cw2nBJ6P6EoYzzvI/IujBICy72hzPks8pHH6BIBfvsp5iFVN9J4dCBqTTkW6CHUY+WP/YD4+bmsOxfhyZRwPDulvhn3XOrMw73hYZt70t3pz1ZJHdH+XmrKXGsW6eFZHt03urioeL2s5aipajnRFmfXUA3p6OiIiIoJqUKhwOh2ZlaaHI6KkUHQOQtAZCjBgShqF/rfyMv41aLnzIk/0tLU1BsxZfMwcyl7ouyBJmaEZIBrb/jbNa+XA3bTiNfnvIOBkclG8SfOBK6Fl5TMDIn22kCnxJ2KdR+WpPOuPIM3SELRXVXYNSQG0yU79wyH0xVOCMm2PeKXTWUhzMSkT7cba2EDtr597i4/3HrEIh+nTvqpYwOSSa5B+MQVJzflWIxb2OVAW8XGEz8oRwXZeJjH3uOJIfiGVmnjpz1lID7WZshdfJpxWOmXLS3o0/gA0kzVnnxyNxz9Sy0FlLIV0rUM2+p5BRJeqMkKGz9TflO2upBcTNvSdXihGaEhvs2HoHahFhlV7hRUIbH1g42aaAc9drzPmtrlp11lJKc3PIldLd8+fPIXhK+WUly9afHM2v73ZNIN+ilAF2OPlrRf7m2VvT79f0M6FeeJT95evc3FzasmXLpZy1pB9Vq1YFj5dDO3fuHIdsStpCtRkKUOE/2izMaJncB97qLW1UcIVPX+OBdjqbhBRVa/GXAsr66UokuVGG7+HjCMNfLYY+WVGYHCb/I05AVsPGZjwozP/qZCnNIzx6LlbunIsL5y9i+9adGFF1f6HBSi1hdISUxyf5Boy1v8tOR+IayOpZAx95sbC38NIz17LZrb7fBQMHD0CrNs0xavgYTJERkpsKA06FPXZi+sPKjMSjkFPOvvodJ/Ni5fQqbj6Wn4HcBskYUdNMMWAF7eWT8Gsdr+bg6l7NPp5kDS+k53ss+uCJhsaWsroNbRVcA2lCPiyPZUk5XvQl9tyVafxfN2VUzsmHvpSkGZ95ttbs2SZmZswFCxbSxowZo3B+0iC4Oi63LN4hqRnr8sE+F1eAlh7GhWHbtSXBrsg8DAmQfP7visyH25T96NSliwQbc1NjZIyXnqofIyHu00IZGD9Auk+CgOFEKxogu/cpOhX+204rgzUQUVUDq2Y0PjPZnBgKK0uh5umTBfMVitv46VtMbvwp3F181jPMoC1BZQiBp3BQanSevd0NJy1kz3cf83lYSOb0dwsywDaxRQEJofyxIA2tzByxxpw4kIhDVd/lXEEW1nsn4OJ+F32zLpPfkbNc5JA0K4O6ave3cossJr35KBeX7hvj2n0OevXqg/Ubt8mMYPHw4UOEhoY2JcJeK1NgA4DSGrjDChD5GknOayjkNSTqXKhrN5KAOUhpWpoAej/7UbifGaf1jTONTRoLGF/fUNvhWTQm3nJPUW9ulNpzichFy4Up/V+rNb7828lIC4ktEkWlY8MB8fyT7wLUcha3z4oS3g3+XS2ZKSHPZb2AeepCkppOOtyoMoNYmzcT/drtQmtP5Z/HU64K8G9MLl4N+2TvvEJSZDj9cA0hISGYNmUSfhbtUeu7wHcfDfHJqcqILRfGjHwXZMr4LpCLUE4dAVuzsMfIH9X1GBq9aKiPyLv5f3mpiCDvZnvybuaL+GRDSR5am7DwvZkt3Oiy7yWPzEhMb3KpiMwXcTxyvwOWV7HFZbtdZEOWEfhET+EPuuGvavLnEO/z8nHVfm+l0s+7zMf48/G4ZkToq/IENzHBtH4dmL/s+s2JIQ9GG+37TmSDRHCSIDV29DBsnKb6tKTVqCxcuJkiQUvRSWJiIkl96H05Ozu3hSK4L6FP7RexPpRjyaRFcu54V9c2Lyq/AYPE6a5H8qtUxrLkTi4CI1zRwkS9D4lO3Ldo2/BEZRy6QWaDBgwaUFEDq663wRuW9KILZcjwyE78jqLnuDtMvWeNMjx0CUM5bQc+zsPxDfInc+ryd6gXh5dm+vmwV1dGA558DfjmPEH6fS/5ADrsoXK5GQW9cScs4nXI5ksi/QfLympK3Tqh9N+XLKXVrVtXpbG3r2aLYx1UQqlQwIxV6YXhJTu2bYWjNR7pXLar8QXw/OUeqlWrJsHL3ZaJ10OkDaFU7t9qe7Pw4ZqXBLzhRDcaYNZ9k8DLEamWI0U3ohioViAN1HfpKWznP7NCf/eXVtepl/Ox0zW2dLPU+cGPGVj8IR1upqY4Vv2T03JMTDJElm0Q6jYA1ubanwNKCVHODddvd8dhi8o3zoZ5z3HvoitYCkJ/lrNqdcp+40EuGvY4XehgKc2odeuWogsXLqntQCpNz3AOu1SbWtTuH5nlVH4mUm3qCqc4NNeLzpmPposuWKdp9Zl8NG8rVk8cAJqJZhkShJnv0O4nU5haW8vUVVmNApJP9WrsN6Cbq25rFSTnIIX1tCwWMvsdwmJFL2nBaunUJeOpKK/WSrVwKWGYD2eLLtgkqYXfgeOAhAnmMsekamPfs4AxuYP3tBIpXKwqj+6IC0Lse0ptiNOsTK1nhd8bK+981oyb6thR6QLMOCTCPoav6sjljOGc8RSzm6ruNCtnsVVmn5T9GrfebkCYMdlo5mJTiB/6+DVZlUrHKm8nhJFUNWWVH/OGo7p9i7LAKlT/mts9RBl5ibLeQ4Nr+pusfxruoZcdI5S9KtvzEUkJVLzo2qeKE2JOqc4+6SMfto1ew4R4m5UtUVFR1K7b7wj8ImVxPkc4WTdChRinJZO+N+Oml9adtbHE0JT9N6fSOmurkBDIE15UVdtZuzr3I5rVPVghrrFBCIMGDBrQvQamkJ30tTiv1WJkQaPjMr0mQjfwkFUJQyQb02n4M8gM81elqTV+RUgpd6ugRo7yIc4U0TL06V8Du4x99M/0P450cl/asmk7y02Az4AxCYl5islkCv/9918qxM60zKwso/MXL6nsrI2Pj8fmZsJKqxEq3FNRqMVho8boZRxN3UwwpF0DKV7v07gIu+AEKgJMyWJCwrbFDCF5xUlkghcx+SW7DHUdaIB7z9uFySgMSaUD6gaSlVEDZnQGr7I5a9Ny3mMQQzmjfV97a9wL9hY7a6lrtMnHEZsdn2B8wRw4vOuF5ddbFEamqYzXTxmZW9M13lSnDButw9wyq4GTrWn4/o8vM8Tj2L5MGMV2wdQpU6R0e/78RZoFgyH5QpWCMjSooAGFuQE7mrJRN/MJfeK7A3wVaKoNmh2yjDYwq5ZWefUwGwXeci+NQyPT2R74x7uh2uGFjRgMZE69qZYcRo4MvP1QoJZeUzPV/7mQcMgaXYuZFitUdtZeyf8Hx3ydteaspZR2sC2wtzUJtUJSB6ha3nOEmLXzoqpoUvBXrlzBokaqO+ulCOmogcpTyzvmWimdtZRKAk3UW0ihI3VqleyLj5ew+VZbVE3oi29EP2O/x0exs5Zi9CDED/dDfJRy1lLwGR8Wk924levbc1KDozQTmsklSn5SqtiwjK72amuRLXrms0tfzlqKMWWv2r59O1UVF7aleo9JJ3tjuLnYiekoU/H39wdJZfULgS32GCuD+JnBVEiHrZERRkWf8hhgVCI2vzb0zifx+NfOS0Enb9nhBbTBQ1c0XqQJ0H0TH5HMYLVewJRc90iI4PcBPxvCBGvpIl2L24KddzvietxmLVE0kKE0QIX73XK7HQ49nWRQiJY0MKzxWfxMwqGpW64xA/Dvbmssuyudy0BdmvrCYxjT0DdHiIu3c7TOMvG+JzoXvNI6XQNB3WugsYklBk4pvzC4LKaRattAda+SysChjrm5ufDMmTMikoO8Q3Z2Nq19+/YayR0S4A9Hiwo5FVZqXIlcEQIDAwthvby8kEfmufooV7rTcfbsWcTGxmJg315ilvefRiJ94kUMPidt59vfnomCg1noNjZBDP+5VlqOzESTkQw4Nk1EJjGA6btk3/O2c3Ew4hK+qlvs9C2sgZ+uNdBwaK2NOg2bpsoARCRyCyfvIxI4L5CW8w65fI4EOhXWeOOdrmiVNQPtbbQT3aWNtRXuBHthSN6P2H6/vwS/z+GE0tlYkhuzspZ2JmxMO1IFTiS37ZdYavqbYnLHcBw/Fi41fC6PR2MyGZXv40tqJBWiQaHDlpKwujED0wrSjdu9Xi09idHyECiH2smqA4yPkF2x2iyn/8/efYBHUbQBHH+vJLncpdB7C703RUCxAIIKKqICitjrJypgF3vvir0X7AUFFQULiogC0nvvhB5CkivJtf1mURBIvcvV5L/PE+5ud2fmnd8ed3s7OzO21Qbflj/LnaWx9YXlysM3O/ibMH4N8jd7FZsx6JPgu+ucXvauX0fJ9F7zou+MpMC8hjiay+VqruD7YqQX6j41DPzHDW6QLl0K/zfJysqSjq2aSlc1JG3bBtVk7J23HyVw5Mu7Lj1TDVsbm6efzV7Lkw/sbaVFFIZAPlIp+FfPJlUXvxo+uaItL/x1qtyf+J782q6+HJNSvlECDtq8kFFLUrdcIJ8tvlL0aTYOX5xqKOos52bZZV8rDnfwn1eH5xmq5yc3uebkb1+upalG2k37ZjXuNWFcndK7E4eq8MPyee6ZRw698ng88uFjwR+X7b/WCLgd69prrzXUq1c3+IvYh6KP3yex+El6zOxP683r3jE0w0IcfmgyTtwkK0ekH74qLp53fS9PvjC2lHrFjGNflkpkqjtL7q87VNrVKt/FzZLK0n+E291Zak7N+P3RWFL9Dt/2/vzh8kOLBDGpk5Gtanz836q+Kwlx/MV/eN2i/XzNijPl/ob/zMvaY+lmGXV8+e/0i3adSivfpeZysqj5UA2qR2u4Fn1OZ/PCy+RWyz+2wZbTxr5U/r40RWrHWSPHswvy5baX6khiYmi/9txuTUafmCePJDQIlpR0URIY5dsiH86Kzl2qn0y2y0V37A7tmzFKjhEoNjU5OTln/fr1hrp1Qzfko36xzHVT1QiEH74iluzxynFquOGDy3PPPiPjX3hMTqntlZqJXqljM0ijVKM0TTdJA/UYymXgvGZy3ejb5ayZ18qT8z3S4vo3ZPjw4YeKKMm3xQc5svbXRmJJCm1MhwqP0pOx43Kk2XGPy5VXXX0ogj/++EOefeh8uf+6BOnYMlHNVxi5//ZqvmyPmi/7KhXMB4cC4kmlEqib0tJ/ZdfxkXvTFaE7f/tEWbbtdbmshkXOrV5FEtVd+4cvHjVSwF6PV5KNRqliDn9P0d7Lt8o1PaYdHkJcP/9q1ukyMyUjrutwMPgO+cskc16jgy8r1eO2nV459oIc2bRlp6ib06SgoECGnHuGPHndWuk2LHOTw6VVjIMcpaOaZjBN2lilw6CyFK9uCpQT83f5V7a9N+wnKffu+N7Xx/WeyWoMfJjJ4upyiiPDZ7p1Y7k/TOsX3CGdnn6ouGJKXP/n4BFe33PpQXWzHDz2e3ntvsCvJzbpscWzwNw+4IbX253b5O22D5RYn5I2Nln8gu/z9IVl8t7u2yzjrD3kx6FB0ZQURtDbPllVIPlnPynXjyzcWeKcgafJ3VVmSQfVQ+7wZcD3fvlk5iqpU6fOodX6EKZ3nnOsfHxqmSgOpYvEE/2G1tPfKZDvra0iUVzYy2iSu1pGnfBr2MuJRAFun0vmLh8iL2eU7xplWWLNUuea+t04NdS55tE3Fez3+mRC1n55c1eenNXmcWlerWdZsgzbPk//2Vfyl/z3/ytsBZWQ8Zwl+eJvPEHNreyXt58bKu89XP42um4XZEvnbmfIiEuukeOPP75MwySnp6Xac/PsobmLs4T6xuKmI38xRT/C6q/dV2PXdcPSQv4p37L/FllyXvwcY32Yu96fOGSEq45cpO6iKc+y1eeWW6r2kp6NLy9PNsWm1Sf9fnXhOTJv4d9qcugW8uijj8mM93fJcfUCu9Os2AJibMNLs06Tv9ofedH4huw+ckKjS2Is0vgLR29UvMw18ojA+y7fJlf1+OWIdRXlhX5XfFb9H+SbyV/J3r17pVvXnnJarYekpi08v8n35++QjGWj5bKkauUi3KZuADnJsUqmX5AirauF/OO6XLGVlLiDathY+3vjknYJapt+keWnsw1yTmJ8N/4EVfk4TqSG0pXdH2RJm2bRGXXD0G6D/quRLtolvIdSki1rP/r0s+aDBpXpGtsROek/Lm66/jqZ8OXnsj/PKU0b1JGqaj6uxSvXyoi2iTKu15EXH45IHCcv5u3ySq/P/muwLSls/eLvDz/8IM899qCk5W1Sw6YZxHRUw0lJ6Y/e1uYTr2zckyut66TJomFmcamLIVds7i4TJ089tOuZrdNlwmlFf0d8sdotu9olyW1XRuemiUNBhujJ2Bfc8tgbW0vMbcDpfeWH52C6QgIAAEAASURBVDaVuE+oN3o8mqT32JTnytf0KyHuUOdPfrErYDYm+e/sNT1qv/X1ngur1l2vLsIFfuE93Kpnrdwmw4+rGL8tps0aIJNTQn9uG+5jUFz+53jWyo+/15RkS9jbyooLIarr89S0AotXuaVFY7PowwgeXFK7bXTanVot9dpxcB2PZRdIEePWzVU7BnR3beu8ddruDk+E/TO04bJ7/BNStoX0Dd+r2a3epP7P/PcGKjvVoT0dT9fShhRsDar+2QuXyNIqbxzKK5AnLYZO0v74oF7A5R7XdadvqrVl0SedJQTQImeVL6vTMwGn07Ncoa6t7NwxUmyG0kftvDd/iDw2+E9pXyOookqoQfCbWn3gkOVbsyQlpfANA80b15dl5xQ/QtmyvT45a6pBzjitn0z7aYr8fo5Z3Sga0rdx8BU7LOXmXJ+8/EWSPJj0zxz3h22K26ctc9fI/06I/xvPPL58yVwzRO5qUL72jnAcyNNWZMqIbpPVnNBJ4ci+THk+MqOnqF62Zdo3Xnf6U3WqmT7XLU+/Z5c5cxdJq1aFb6rYunWrNGrUaISq48fxWs9g446lT1TjsDNsW8PRWPvU2/tl3jmFv4SCRQtHutVqyOM3lxTIiR/Z5bS3CmTp+Doyxd+u3I21W3wFMrZG/4Aba39b97zoHxClLfo4811u3CU59uwDjbX6/nffPVZ+XPuCrKv9sjw/r/+BoQZKyydetr/wV+9CjbV67BZz/NwMEMvWZtWL3Ok7cvjAae0ayOQFZ4jPH9x8JrFWX7/mk8dn9pKfnbfKuN+HHGis1WOsUaOGbNyyVu779mT5Zv3dpYb9w+qHZOLS0aXud/gOVSx1ZXunN+UGR8kXlQ9PU9TzBuo4bUjtKHUnZ8jot0zS7M1cufsPl8zY5lFDtAQ9GlFRRYV03dLhadLs1M0hzVPPrEEds2S85JZV6qSTJX4E9B6AZ16xK2oBp6cYH4la4XFQsNVq9ec5XUE11p512qkycVCaPJM0QTaNMMn+/6XKgrMcMu3ETNl7jbVCNNbqh1DvPVvWJSkpSQYPHix/qB9D36/aLz/1eVcenRf89+pFzfR7lEW+/nW2ZKuh1PTh5z9oMkfuHXvnoZDan3nFoedHPxnaKlFGevxSo8emozfF5euWPR8uNe6LL72q1H1CvUNCgkGc8zNSp79ftyAtxbBM5R/wRdBQx0R+kRG4rPMbUTvW+hy0TfeOicnGWl3/uzYN5Ov5p0fmQIS5lJO7T5IzHFvCXErksp+U0EIu7bVfsnP++Y6JXMmxUVKqavDodYzliMZaPbK8uRlW1YhlT7EadqqXha9oxkb4MRuFQ/z1Ag1uVWpzQ+riW8L+w3Zr+0eMffZXC2k5PZZsK/fnf/Il0wx+rzdQtgP7V+3SUexvrgwq7Zo9wY332kJLCqolNCOpRtD2PVe97C+tsdajuWWQs5Z8e/XsmGqsnb3DKxv2OotsrNV/I5fUWKsfWL3heeMIo7xac5qsvigxJhtr56sbW6d9UaVCNda2UL1rK0Jjrd5RZ8+6YTHZWKu/v39sW1+WrxisrkEH9xmo51He5eTG15Q3i5hPf0JXi9x9bZrsn11PDOtPlVtvvrFQzA0bNpTrrr36w0IbKsGKsl/pCTNGy8bm3M+eqR3yOXb2qIbQjpvckhjBIcjKQnX3DJcc/5ZLXn7bJmvH15WG3zeVEYtbyo9aO5loaVnucfX14aU+KciWh+tfJF0bDCtLSAf2+XH1w7Lx78HyrWuNdKl+Uonp9LHzhz3YXIYOHVrkfp9+8ZHYHbnyxoKLitwebyvfmn2qzO7QuFDYW9SQyMfUG1xoPSsCFzAZE+SFnYWHqf+4RW1Zs+rcmJtfIPAaqi//zU+I1+eV2XNnSlpa4bsx69WrJ3PX/yAbs+eWmH2HOoNkhhRI4rwh8tnCK0XvLViWJVnNFVXvmE/kfGemZJfzBCRBncw/ZmkgS5M7yN2bWsuxvzWX3A8byeR3q8qwt31y2mcOyVNDBsfSsliNtPD8e4XfY+WNsXf3ZPnj8t2SqxrkWeJHYKirfL3Ny1NTt0c7uzzpK3LamjVr+h0OR8AXm/RetfWr2eTL1gtkYNPo9JyO5HHR599Vc/kGVaTeeHvXz7vk7tnB/RAd3SVRduzYIW3btpVTf/1ndIEEda595qZXZfHixQdi6npcyTf+6cNRbbs8Xa69KFNmzo/vG15efenpUo/D9dcV34BdauJy7nByt2TJmZPRTjXc+pMSDfqbhgv+5TSN8eQ310mJ3iFevvZaGVC18DluLJl92bKO6L/t4n3Rb3bt3+N76eU3ymcF++K9Ogfif93SREb2zxV9hACW/wT0hlzVcFtb9bhZ9fZDNbTWTRP2p6cYNqs99J4nLCUINDEmHjlhdgn7Hr5pc3obg23RmLC/EfM6PWMYkXtMcCdkhwf87/NnbJ+ZnC+3OfIu+CL2K2mVsXYH+a5R8DF5PtwaVH2y87SgrlF3MQU3v+Kj9c4OuifyFZa7SiKULwtelWXHNJRN1yaVuF80Nt7+t1FN01G4jbukKU2iEWewZW7LU6MVfFdVLijnyHLBlh/qdPfk75ZB1tZyfQUYClm/brlo5TC5pV5sj073RONa8trs/qE+lGXOr0vdsyXXXq6P8TKXFQs7tmySKA8OnyxnqqHYj15ee/1NQ3p6WuXB+BcgqC/Do/HK+1rdKehc/UOjsEykfPbwTDmlYcBTGZS3SkWmt6uGi3qv5sjUd6vJ3Ztbyy+W1nJFUg2pr35ohXrRG1Lu9+RI25p9S816R94qefHPflJj3lCZ6t4uLybXOZCmee0BJaZdkzVTzj///BL30TfWq9as1H1ieQf9rprvVA/PX1VPz6KWRzKDu1haVF6sE/k2u+ihVx5qWF1smVccmBg+np1qJbYpNXy9J9TC7M9L3K9heifR56YeqT5D5pgT5fhFl8hzM0+W5bt/KTGdvjE5IU1y0jqGpXExUc3D20fl/15SU5noaSu5nzaQk9/Kl9+3Bt+Tq9QKBbCDWQ0BWmd5gbqZJPTf93dcU0VGtV1f5sbzAMJm1zAJ3JZYV9Zvic5787IYH/kjTOSlZWvp0L6df/fuwOf3nTFjhtx9QhVZf3HsXRQprdLl2a7PkRrsonoxy81fr5RFuwO/ppai5gPv3aXlgaKXr90oGeMLDjzvVNMsP405We664zYZNqxsNwy+cJJVGs9wyDGDtwVblainm/qSQ1579eVi4/jpp59k88//nF8Xu1MENugNt/kLM2y+pRmrLj47xVk1zbBAFVsjAkVTRAQFEoxJz0SwuCOKmpv5hbwag8MgHxHkvy/033bvz/9v7u2i9omXded1ekV2HPu5nJ5YX25w7YiXsIuN8+WExnJKr13Fbq/sG648L01Wftcwff+cjEaqAffD1d830BrUNumNkmdWdpuj6n9hohhcL9sapR+1vswvt1ZpZ0hbfHPofzgeFoFR/X6e2OI88/cFHx22tnxPn/E9Z9ScWeXKxFfjhKDTmxPUXYVBLB67N6h0JwYx4t137v1yWlrbIKIUGbrxHe0Cyw3Fxjogr648eMVjclm70F/rDSrgoxI93NUnmZmZh9Zu375dmtVMEddNsd2IdijgEp7o191f+zwp7htr9ZHr+qsRNC6oeoJU7/aVnNJsVAm1jp9N4/7qI+OaxMdPj+ntG4o+akw0lpTE6mp0wL3RKDpqZdrU18anD6yRUTdeUyiG/ftz9Jv5w34DVaGCo7gi4N4LoY7VajHsc8zPCMu3QkafzbJyWGzc3dv1/Tx5RWsqnczB3fkVrPvPnly5wrlNutY7T9KTG0vV5Iay27FWdmX/Lev2z5M7VWPP9RZ9OpTCy73Nx0o1tX9Jyx++u2X6zGnF7rJ06VL58LrdkmQOS3t8seUGsyHfa5dlq4bLbLtHbugx5UAWdneWbFp7mTzaqPhx9dstXC33nDQrmCJJU4TAT2sflffqrC1iyz+r5qi5CH9MuklaVO91YMXf2z6VjgWfy8a0UdKmZu9i08XShj2tPpDX33y12JA+/eQzWfBK9VKH2u6oeq/3VY2jRy9fu7PlIdXDPj2ludSo0k0apXeWnPydkluwWxZt+1hGJqTKGEtk5xRza35pZ18miy9PlRrJxf62OboqYXvd+dNcWTW9cVjyb9Fnq8xxtwtL3mQaeoFhDVfLz5/WDX3GpeQ4X904cOzQzKifh5USZiQ39x35v+t+fvnV1wI2Ob3PyaqBYLHUS4n+Z0skwfSyLljdRSZNLf48rCzxDB18lnzQ6M+y7Fpon+QXs+XO226R777+QuYNLP8NbD0n5MlPn9eXmnE0P/pBlA++yZNL7tp98OWhx7Vr18qiySfJkNMi+xvgUAClPHGpIa27DcvMXbvF87zbLQ+UsjubY19g+C09f/xYvzkvGsvTM0+SBZ2aRqPooMqcsHe/eDImBZU2lhPNVb+P2u/4WsZa4uOiaHGWxxWskHVzi75purg0lXm9z6dJWvdNO50uLfIntrEDr27dM7w5ztrQMjSEvetaqblOd3R80mQ2FO6VGKqq3545yTew4DOTxWgJSZY98mppttsDvwny8MJb1HxRWoy69vBVZXq+6YPPZMuJgd9UuLvTV5p/TsOAfgvoQ6hrAwO/pNw9d413bcenzGWq0FE7VVv0oH9KlY2Ffnis8M6XaXXPkrdPD9/75KhQgn65UbmNnm2WZKNP3uttODC9SdCZxUhCvZHzorf98o4lfs5DiqIboKYwa9n6IWlcpWtRm+N2nT7N3anZl0mdxNjoVFcWyAu2VJNB7Z4ry64h32eDard55c1npUpaUB9TIY8nUhnq5zKdz98nV1//oLRr316mfP+N/PrjB7Lgy+pi7brR4SrQUiIVSzTLCeiLMNSBqp61C9SwLl1Cna+e3zsTcuU8NWa9Rc2pFc1l7k6vvPmdWV6whKdhIJx1u7f53arBtuQfSPp8nOaTf5EHH76/yFA6Nugng5o9XOS2WFq5cMe30tT+jlxXp6o8sm2vuGvcIrM2vyJP1DfKMSklX2A7a9VOGd5taixVJ65jeXvuYJnWuuSbYHO8Pjl1xTYZ0Op+mbDiLlnepZVszHfLg9mt5LSWRb8XYwll/b7Zcs/n/Q8MJXl0XIsWLZK3rllb6s0Serr2qsG2X5QuyB0dd1lf/+Wxyw9NtslzfZLLmiRs+x0/xS4LJpV8U0qwhVc/ZpOsTe4YbHLSRVBgqLZOfvmz6BuXwh2God0G/RxoUbjLifH8E9JSU/Lmzpuf1LLlPz02yxqvz+eTetXTZPOllatX7eE+vb/1y6yN5R/mvWG1FFl7cWz0ApiZ6ZGJajSEVx6I7I1Fh7sG81y/CeOYIUfehf3lF1+IIfNGOf+0+PhdeepV2+3TZuVfpur/VTAGpIm6QMIJDUcU9M4YGbUfwC/+1VtmFTGFTNRligkgs8AjM2p8XMzW+F/9yIyeklW1c1xXpK1nqeycE3/XUqKJrs4v9eKj9jkQhbpfrhppX33F1sgyODHwhruyxttNNfAtbPegOc0Uvt+xdZeO9U9K3V6oIbCsMR6+n0dvHOn7vZhbn3P46oCeO56qrg1xbw/qvfTrTyPE3KLk6zpHB7PvzKk+94TA7tqbsyRfWlwf+G+5RjkrNHun5wKu2x5Pnszeep7UMh45NfK1zp7y9UVbKuUNpEcfx2i9zngtV5andIhW8SEpt3r2ogrbIWiPY6Pc5H8wJE6RyuSMNS655JiSRz8MZyxvzx8hO/7WwllE3OXd98rt9l9n5w9Sgf8ad8EHEHBITgQCKO/Qrlar8dWsP5uEpbG2QA2B4J/pjHpj7Smf2KX6j43isrFWP1A78lYeOl7FPTGqOww/f++HQpv1i6hNa3WK2cbafa6t8srsM2TBsrOl5Y6h8qTtqwONtXpF7mlQQx6yjJefW6WU2lir7/9h85ry19YP9acs5RRwqWG8x9Qq/Zw53WySuR0by4NJ7x9orNWLzbAkyvt1N0q33RfKppVnyzNqeOBVe6eXM6LwJG9WrYfcP3SirFu3rlAB55x1fpkaa/WEHcP4Y7FQYCFacXxCijyS2Vrqq+HhyzrvboiKLpTNlD42+elPZ6H1oViRNb+JdMlfHoqsyCPMAqO90RumNDlJrgxz9WI1++GWxITlNpvNP3/+fHdObl7AjbXPPPWkvNovvVI31uoHd+oAg0yePLnE4+x0OuWZZ56Rhx56qNjP3XU7smTkDF+J+URqY6/6CfJsXbPU6L5R/P74+YF60d1H/qzq1bOr9Gt0e9w01urH95e366XY5zaZkGoz8AUWqTd86MoxNErrENXGWr0qVS2h79g33+6UT/dkS4GapzzUy/lrtoc6y5jKTx8F6t78+B5aeJm5vTykRnNgKbuAGiZZbMmG/8Y7LXvSeNrzaosa7vh9WxNN3ZTw7vaqncLaWKvDzE1raT5jxUO+Le59YXPa0eExY9+c6iE5+UkwJkjy5LHe8gRrGTax9IszxRSQd8f8gMs2ta4acN3XbApuepv76gwI6kulzYqnfYc31ub6s+VSXx2Zfe02GmuLeS9EYnW/z+1x31jbJW99hW2s1d8DNW0Z0n/F5pC/HfQOPe/t2if6TXihXjKq9wt1lgHld9UxH0lyx8IjOAWUSQXbedo79VJcC5pMy6hvzrVajK+r6oXvTq0o2gX95VvOmM/b9mujCfVrh6dbd+0em2Tz5YHdyVXO+hyRvMCrSY+3nfKnre0R6+PtRRs1DMM1PUq+CKjX6f11F8jWzE2Hqjdz5ky599Lx0qtBbF6HHvdXX5nerr4a+uPIC2uHKhDEE7135z17G8mZbZ4IIjVJdIFZW8bLsd6JcknNKiEF6at64o44ZmKpwwuHtNAyZrYtd6lU77NWnnn2qUMpHn/8CXFOPeXQ6+KezNv+tXyc9XNxm+Ni/TMFO+Wk0+xRnWd8zB9Oee2j+mHzGnNKntzrDV/+YQu8kmU8/6md0u94a8RrnXbcxh15Du3I27MjHkVYCxxWzWK4wenRug1smuDp1yghZVW2X56fX/Rc5WWNpHqaTbZdpmYlM0TrNLaskUZmv+nbfLKq1/0yeszNRxToV40bNdJTZKmawa36v0PR68Of9Z+aIFt37j1iX/2Fx+ORs9tVk4lnxM4wVc8uyJdmg1LlggGx3UP1lqdz5dn39hwyrVXDJrtnRO9mkEOBlOOJrevGXGeBFr0fVOWIvRImrd68avc9F3QYF/UPRY8vXzrvuVQ62kLTA+3wqWf2OjfJ7k03ycMNq5X5EG9Wv9E+z8oVo+psWDvBKE2SEkRvQZiU7ZY1nhS5pOsnZc4rXnf8dukY+U3y4zX8A3H/rnq0nfCDV2pVN8V1PSIdfOpxG3+xO7ToXm0OcaXVh5z/I1uG4fTE6H093erY6h+ZcaXxGGujENfun+x8aiqh9kvf8r6fNiskF0xPqHOx1zL4w+DzmnCsd8CqPwNO/1VyPa3misEBfS853lopeRfr0zGXfRk7bp9264QmAZVj96vPkyZXikU1age6XLpyg+/65IcOfBi9XHCbXND/Q+nbKPB8Ai2X/YsX+H2rRxr/2lj1eo7v43B1nfOlbc2+xVe0gmx5aVZ/OTXdIqekJYp+xNaqc7Us1Y5iU5foz1WjZwUyZLJ+nnjr8T8duN6rn4O+8/fp8nv7JiGRemlHttRo+U1I8ipvJuNmDZS8RaG9Vl7emGIlvd4R6NPvHfLs+P37N27z7szO9d+mYiu9MStWKlBMHAF9qRWTR6CrW/z4Zp01/U8Iz4XRWx7fK48GNoJGoPGXuP/mXJ+8/EWS6vkX/xfov3fvl5VdPhBTCV96M7e9I7+tf+uQyZBzh0ndzAsl3RJ7F6n0XrW+rTfITXXDd/PFAnUH+DfZLtnuMUgVdQpX06xJdXVqW8VkEpfqJbLb65dfcz1SYLCpxt2npJat2SG7ivakwOuQr5eNlGzlfrYaarGGSZMaCSb1ReyT/WpM+l3KaI/6Uq6uhi0fmJ4oJ6mLyuFa5ul35vvPlWPrXxCuIoLOV1M/yN5eNUR27s48lEftqg3lmo5fHnpd1JPJs86QaSlNitoUV+tWq5Oqn9pslju6h2aunmAq3/TDHNk+MzyWdodfnupTIKMTagcTGmkiJDCs4Ro1j23kv7eq9dzkUCeU4fvwi5DfUcXUSTTJhlf7WJMvalP0UMUN3y+QPTmOo5KJjL7hepn9wxeSpL4vThxytTzy2JE3QY25aaTUWTReRnWJjaF7C1Ugiiu86hyjkXL9ctJ30rdvX7nj1pvFNvstuf2Yoi9c9PjKKwu27BdjETev6Q3imWpwwVhZXOpcodOXebIlTPOOh6Kead0zJTfvnwYR/UYCvXdTRVhSu2102p2arSLUpQLX4c4zmt/y2DH1zo/G7/oiWf/e9rGMTJwszdUwEuVZvsnKEWeTiUdk4fY5pU7mxXJa1dQj1hf14tTVDrny2JLPp4tKV9HWzdr6sXyxf2bcV+tc7zqZPjvwYU/jvuLlqID+O6Rmry335bv9D5cjm5hK2qV1Ys62zT7btMRWpvrG6J0PvpG/R9rWGSiDqoRnCpxFzm0yeWtr6Zd0frn9n3SM0n68ZZw6PQnuayL/s7O95677MuAG24VjxvrybspRV8XKvuT/kin7e/13XaQsKc+4dof34+WtAorvwrwN8mOHx8qS/RH73LT1S7nI9/2BdQPstSTzf+X7njsic14EJaA31jzyTpLckhT6ET6CCqgciR5ocX9MXksvR5WCSqpP56BPe1facsfmvdK5/ZHtck7Pfjkzd6TULuc8ud/ty5XVVR+W+mntSgsjYtsnrhgr77y4Wrp3it6104hVtpwFPf3uft/Dr2VPynNq5f8SLWcswSYP7hs72NJELHddXSXnsdHVwnJmpU9M/NFtu2VYq7BkX2qtp6qhOBzTasoZUbzbr9QgA9yhrXOnXN39yB/Kh2dhOGmKPPTwgwdW1ahaS/7X8dvDN8fM8132tdJp/13SO4yNgoFW1qdOLK7dmCUtmjwqjdI7BZo8ZvfXG2rfmzdIvm5ZR6onBHTeHNY67VcNxTfubqbmvX0grOUEm/mbS4bJruzNB5Jv27ZNXhu+WfQhx4taJq28V6Z79lSY3mV2NRf2tSmrZeK50bkmbFfD6K9WNxGd0DU8Jz7L17ll00UW6amGg2aJTYHWrqWye37jiAd34a277Z9NsZd+1TnikQVXoMUkecsuTU+pn1L6CBbJanjDt956S1q3bi1333KT9E9cI7d0PfL8rfc3PjFUbSDzl6+Wu7pZ5M5jj9weXJQVP1Wm3S9lOQbHT3DLgkx7kSCN69aU1UNjY4jkgwGep+Ydf+752tIqI/beBydfli0Tp66TCwe1kB/fqFh3Pzfpt8WxebtX/5wKeJjCg8eOx/AIJBmT864/bkKKLbHsPU7DE0nhXHPUMLzvzR8mbzWrXWxv2yyPVzYVuFUPJ6O0VY27Rzcm9FyWKTf1/LlQ5tPWPyNv11pWaP3hK3ot3ykje0w9fFWlff72nEGy3FoxBvO4psNamfAaN0EG8mZetcEtbc7a1lelqRBzvdWqbszbNaNJyhQ1StLTNzh8E1KbFf2DORCkIPf9xZ0jmVW6+MbU6hOWGG7aNsE3xP21KTEEDdM9XA01281bg7r+q/l90qXnd1LvrDMCksrftVv+dv5zrbCsCb3rc2Vv/VVl3f3Afhn9t/jnO9uX/uPjsFwb7F+uOTs/H7CHbeGd2nMpXxlWNDtfnjw5dq51HVa1Sve02Zu5sjS5Q4Wo97m29nJi05EVoi7lrcQbs/WRMRsWm43Tp0bt0m6QplWPK7TPO3P6yS9tC3ei0ztY6T9mWidbxGYq+iNjeo5dbtucpc4/fxRzCD57CwVXzhXbcpfJKs8YmfUFN7CVhdJ27Ean0xWfNx8H/AVVFpDi9uneMck++9P6YbsiX13NdZV5RXQukrw0r0BOWtZIWpjCc8G/ONNwri/LZOctrlwjl1xyyYEf2PocObG46B9orbPvk7OqpcVieKIP1TXecJW0qNEnJuMLJCiHO1vWrr5QnmwS+Z5qZYlT/1K/YU9b6dfizrLsHvF9Hp95onh9ngNzDN514u+SUMLnybg/+8n61GZiDvJO2YhXrgwFdshfIuuvjs7wVmd/b5efvi3+hLAM4Ze4y/hJedLjqVpSs4QRC0rMgI1hFXi4YLs8PzfyDeoff5cnI+7cE9FzsTBBnn9nN8vH9/dMjr2WtDBVuKJk2+9bn/y2NkvM5sIXnXZfV1XSkmLr7bk22yd3rvfI92/F3l302Wq46arpYblmG/W3230v7XM//Pr+QSoQWsCifjQOBFA7NbH6jlE9JsfWf9BibLbmLBb995jTvVP16k9Sc5i1lHqpbaRa8n/nXVnOLfLhossOXGBLVSMT6cuT/tHqptbOReb6urqQ93sxF/L08/2Prc9LSmL1ItNWppX5Xrt0XnxNhbmhfIpqIBv2l6jvrLh468fMW23CT3ZtyJjd+vi922ImqCADUdfYvd6lTQ992aZ32uRfaGtrrGIsfB4TZBEBJVvrVUNwJlbzvtloeFgCqL30Tu3b1J3lfsOv9C6Q6y+pIcb04IZxdjxVQxvizgw4ju8u7CbpjxVuUCkOWVMj0mXm/63+jxfdmFJUusROG7SdqZ0Diu0qv+b5uunVRQ9DU1Qhap1bDaPcddXrMnnEDqmjj93KEnWB9ft9YvymodSuINdYrnRul/bdY2MI3mgf3CW7fpDHrV8UG8YJy3bIDapRtahl+oaX5Y2a8w5s2qpuDLxofY6M6Py+pCbVPLT7bsd62Z63UrIc69Q6n6RaGkpGlWPVOWrTQ/vE8pMnZ54i7qWFG6VjOeZoxZbcZaMn363F3XUqQ601Q7lbOlrvGspFAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAIFKLWA2FNMNvFKrUHkEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAgAgKM4xABZIpAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEihKgwbYoFdYhgAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACERCgwTYCyBSBAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIFCVAg21RKqxDAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEIiBAg20EkCkCAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQKEqABtuiVFiHAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIRECABtsIIFMEAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAgggUJQADbZFqbAOAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQiIAADbYRQKYIBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAoCgBg61Dk5yiNgSzzpqkpZmMhmCSBpzG49XErAWcLKgEml6O1yCRqVlQIYYsUb4hSQyGylDTosk8Pqckqvp7D2w2i8mYUPSOrI26gKb5xOd3i1m9XX0qGuOB927lvQfF7MsXUyX4r1tg8EuSftAjsDjV94wtOXLvqVyX5lTfNP98/ESgfhSBQGkCmt+fmqp/KbIgUIEEHB712W41Vorz+gp02A5Vxa9+mNldmtdgMDoPrYzxJ5poJvFr1kSTlc/TGD9WcRuezyVJlfTrWv8daLbG7ZGLucBzHX7NYDTmxVhgBnVOmpJiVZ/8YXif+/yauFyaZlO5R6Le6iem+A1GLckQmh+16jtGnH6vZjEE9x3j0TziTjSrqgdWfaOWL0aLpWxkfnXtxuAu274+TVKNZbvYa3f5NZtWtuNm1/xaqtFyRCUdmlusCWUrq2zBs1e4BNR/UzGq6/KVdXFofjGb+LKrrMc/3PV2q7YQ9R2rrqtX3v9j+jdBnlNzqzax/HB7lyV/wxB/Ft9OZZFiHwQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQCDEApHrOhTiwMkOAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQiHcBGmzj/QgSPwIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIxK0ADbZxe+gIHAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEE4l2ABtt4P4LEjwACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACcStAg23cHjoCRwABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQACBeBegwTbejyDxI4AAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIBA3ArQYBu3h47AEUAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAg3gVosI33I0j8CCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCAQtwI02MbtoSNwBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBCIdwHzzqnT4r0OxI8AAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAgjEpYDBNtauxWXkBI0AAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAgjEuQBDIsf5ASR8BBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBCIXwEabOP32BE5AggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAgjEuQANtnF+AAkfAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQTiV4AG2/g9dkSOAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAJxLkCDbZwfQMJHAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAIH4FaDBNn6PHZEjgAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggECcC9BgG+cHkPARQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQCB+BWiwjd9jR+QIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIBDnAjTYxvkBJHwEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEIhfARps4/fYETkCCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCMS5gOGFlG+0OK8D4SOAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAJxKWCYXdVOg21cHjqCRgABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQACBeBdgSOR4P4LEjwACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACcStAg23cHjoCRwABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQACBeBegwTbejyDxI4AAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIBA3ArQYBu3h47AEUAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAg3gVosI33I0j8CCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCAQtwI02MbtoSNwBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBCIdwEabOP9CBI/AggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAgjErQANtnF76AgcAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQTiXYAG23g/gsSPAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAJxK2BobmqVE7fRE3gkBAz/FqI/GjT191+h2sHnBx/1TUc/Nxy24rCnIpocSn8wy6O2H1h9cJ3+qIr/Zzm48tCKgxv+225QixjVDka1s0mFdTDN0Y9HJa0QL3WXg39+Xfqf1wfAVf2VjLJR63SLgx7qaYVb9Hrry+GPPvXKr9b59PXagS2a/l45sPzz8tD++tqDaf/Z4ch/1fajNx/xWjkf8fpgdv+uPLjt6Ee9kIPr1NN/8jisLH3bwT99XxYEEEAgngXUV7VmUhUwqo87s/pYNuofyfp3lP79rf4M+uM/69QL9TwSy4EPWfWP/g2qf1X89/qfrw79e+Sfr5D/tulxqfj8+me/2qbvcmA39frA47+v9d1YEEAAAQSiIOA3+FNTrfq3CgsCCIRSQD9f8qkf2F6vpnn1H9rq9Eid//g0zeBR/+G8oSyLvBBAAAEEyidgMvtTLUmcD5VPkdQIBCfg9miax23IKym1Qevysn4NigUBBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAIMICDIkcYXCKQwABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBA4K0GB7UIJHBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAIMICNNhGGJziEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAgYMCNNgelOARAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQiLAADbYRBqc4BBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBA4KAADbYHJXhEAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEIixAg22EwSkOAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQOChAg+1BCR4RQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQACBCAvQYBthcIpDAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEDgqY8/2eg895RAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBCIoIAhq2pnLYLlURQCCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAwL8CDInMWwEBBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBCIkgANtlGCp1gEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEECABlveAwgggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggECUBGiwjRI8xSKAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAI02PIeQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBKIkQINtlOApFgEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEKDBlvcAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAgggECUBGmyjBE+xCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAA22vAcQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQACBKAnQYBsleIpFAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEDDcNTs+BAQEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAg8gIGbXlTLfLFUiICCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAEMi8x5AAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEoiRAg22U4CkWAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQoMGW9wACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCAQJQEabKMET7EIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAADba8BxBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAIEoCdBgGyV4ikUAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQRosOU9gAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCERJgAbbKMFTLAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIECDLe8BBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAIEoChjGXpu+PUtkUiwACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCFRqAUOlrj2VRwABBBBAAAEEEEAAAQQQQKBiCZhVdRr9+1dPPdYwGqWGLdlYL8FsqGswSE2fX6vu9WpV8gu0FE3EqLb7E0wG9Sd+c4J6VH+JCaIlJhi0JLNBLBaD5nZrhnyPJh6PGArUc5Xe6PGJwe3RjD6fZvKp535NjJYkQ16iSXJNZkOWpml7vH7DLqfLn+n1yh4VS5b626X+Nv37l68eWRBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEIiaAJ1so0ZPwQgggAACCCCAAAIIIIAAAgiUKGBVWzvof1aL8djkJDnO7tJaJiWIIaNBgrt54wRTm4wEW0YDs7FJvQRRj9KwjlnMqmNsrC+qA67s2OOTjdu8sinTIxu3e/2rN3pdaza7PZu2eUz7cvxJqTbjBp9f5ufa/X+r+iz5929frNeN+BBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEE4kcg9u+8jR9LIkUAAQQQQAABBBBAAAEEEECgrAL11Y4nWC3SOzHR2Nfh9Ddu0zTB1f8Ea3K39kmJHVsmSovGCWIy8bP9cNAt272yZE2BLFzp9k3902mfv9xtUbPu7jUYDDNUZ9xf1L5/qr/Vh6fhOQIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAsUJcLducTKsRwABBBBAAAEEEEAAAQQQQCB4gUSV9CQ1++xZiQmGczxeqXX6icnes0+xpfTsnCQtm+ibWcIhsHOPV2YuyJefZ7lcE39x+J35WoHRaPghz+GfqMr7Wf3lhaNc8kQAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQiD8BOtnG3zEjYgQQUALNG9R+ad22XTdYrVa/2WT0+3zeNQ5n/kdq08vqj5vmeZcggAACCOgCF5kMhkvS0tNOdHu8Sfn5+WqiQ4N4vd7aatseiBBAAIEQCTQ1m+VCW7LhctGk7oCTrNqgPjZb/+OTpWq6KURFkE2oBPIL/DL973z5brqj4Jtfne79ef58v1/72FUg+m+J+aEqh3wQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBOJDgE628XGciBIBBA4TSLPZNt98y80N73/woUKfYUuWLJExo27SZs/52+90uf6nkr11WFKeIoAAAghUbIF6KbbkuRaLte4jjzwi11x77YFOtYdXOSsrSxo2bKi5XK7uav3cw7fxHAEEEChFoK7RKBel2gxXeL2SMfQMmzZiYGryyd0sYjIVOi0tJSs2x6LAwpUF8ukPdvf4b/LcTpeW5/FqHxa4ZbyKdUUsxktMCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAALlF+BO4PIbkgMCCERQID0tNevee++resutt5b6+ZWZmSlnn3mmtmb1qo12V35zFaYWwVApCgEEEEAgcgJj1Mzmz7777ruGYcOGlVrq/v37pXHjRlpubt4gtfN3pSZgBwQQqIwCTZOS5DqjGK5plZFgGH1xetrQ022SbDFWRotKXWc1y61M+cMlL32ck/fb3y5zUqLxqzyH/yWF8nelhqHyCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAQAURKLWTWgWpJ9VAAIH4F2iXnJy8dPbs2YaOHTsGVJv8/Hzp2KGDtn37tlUOZ37bgBKzMwIIIIBALAtclJSU9OG3335r6N+/f8Bx9ju1rzZz5p/T8wsK+gScmAQIIFCRBGpaEmWUGAw3HdMmUW66OD31nD42SUzk53JFOsihrstvc1zyyqc5ju9/dxkTzIbP85z+x1UZa0JdDvkhgAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCIRXgLuGw+tL7gggEAIBk8n0cYvmzS5ctHiJQXWmCjrHHTt2SKtWLbW8PPtlKpMPgs6IhAgggAAC0RYwptqsuXeNHWu9a+zd5Tqf/fCDD7Trr7/ea3c46qhK7Yt2xSgfAQQiInBKeqrhPs0v3Udfmp5w4/D0hBpVTREpmEIqrkB+gV/GT7JrD7+R7VAz3W7LtWsPqtp+qf58FbfW1AwBBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBCIf4FydUqI/+pTAwQQiHGBVmr22hUTJkwwDhgwIGShnn5aP+2PGTPmOfPdx4UsUzJCAAEEEIiUQF/13fDzggULDK1btw5JmQUFBdK1S2dtzdp1k7xe77khyZRMEEAgVgT037xDUqyGZ1pnJKQ/MLJa2oCTktWktfwUjpUDVJHjmL+8QB59M9s+9Q+Xz+PTHvZ65RVV3/yKXGfqhgACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCMSbAHcWx9sRI14EKoeAMcVmzex36qm1v5o4SfWBCP1H1cSJE+Xii0d4HQ5nFUXqqBys1BIBBBCIbwGTyfDdMV26Dpz999ywfDf89ddf0q/fqZrT6RqmpPTZB1kQQCA+Bc5MtRmeq1fLVPfZ22qkDDzZGp+1IOoKJ7BoZYHc+kyWfdbigoL8Au0ev1/eUZX0VLiKUiEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAgjgRC33MtjipPqAggEHsCZrN5buNGjY6Zr2YoTE9PD2uAy5Ytk+OOO05zuVxNVUGbwloYmSOAAAIIlEvAZklaPmTo0Dbvjf8g7Oevr77yijZq1Cjx+nzHqKAXlitwEiOAQCQEWqWnGF5PsRmPff6O6inn97cxU20k1Cmj3AKzFuXLmCez8lasd+/Mc2hXqwx/L3emZIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAo0NVtMCgyZpBrdvpM8nb8YDickkVxkN8pp4DXaPaF1VzBvjIW5iRAABBBBAAAEEEEAAAQQqgoCxIlSCOiCAQPwLWJKSpqvOtf7t27cfu279+rB3sNXF2rdvL5s2bTIkJydvUC/7xL8iNUAAAQQqpkBaii3rzrF3R6SDrS54/ciRBo/Xa3ji8cfmW61Wr1qld7ZlQQCB2BEwGY0yMjnJkHvFeanOPTMbr9o/J+OUbb82ThlyWgodbGPnOBFJKQI9O1tk9qf1U3P/zmiRvzBj+iOjqnqSLQaHLVmeU0lTS0nOZgQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQOFLgVEk25aW/1mtTlXdOrpZoMxncizLeUIM1e23JBntSktysdo+l+6aNCQkySrV9Ox4dXc1TsDDjrS5tkgx3WupUmZDSbEOqGHNVvP2PrCKvEEAAAQQQQAABBBBAAAEEwiEQ9pnAwhE0eSKAQMURsCZb5tVv0KDrrFmzDdWrV49Kxdxut2Q0aaJt37HjYRXA/VEJgkIRQAABBIoSsNls1pyPPvrYdM455xS1PSLr9Jltb7vtNs3pcp2iCvwjIoVSCAIIHC1QJ81meFfNVnviq/fWSBnUx3b0dl4jUKEEFq4skKvu22Nfu9mzWc1yO1xVbkmFqiCVQQABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQCBUAhbz3cYU011VP+1rMzdNO5Crb2++ZJ38neaf3/iI+6TfmpDrH/NklsvnNzyVn+9/TO2sD74eycWcmCh3mI2GsY+PqZZ040VpJoPhvxBrnLBJ+9Db1NA9IeVATFt8BXKmfZ09x+971i7+ByIZKGUhEIMC91RJSxnjzHdXbd2yhVa/fj1DWlq6fP7lBP0/UTv1tyIGYyYkBBBAAAEEEEAAgTgR+O+XWZwETJgIIFAhBIxV01OmNWjU5OSZM/9UP3L/ubAV7ZqdfdZA7eeffp6d7/YcH+1YKB8BBBBAQI63WCwzly9fbmjatGlMcIwfP167/vr/aU6n60wV0JSYCIogEKjYAq1Ux9qPG9c3t/rkqdop7VskVuzaUjsEihHYneWTax/c4/z5L1eOw6VdpHb7rZhdWY0AAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIBAOATGqkw/Vn+bw5F5kHlaxGaelNC2aq8qb59kM6YkFMpmV5svtI2T6xua1C+8Td/5k+/t2vUP7nF5/dorDpfcq1YVFMokNCsSLRZ5QPWmHTXuruqWq85LK3I2Xb9fE3PHjdreKp0L3dvt0vxymX2TY67XMStHfGersFyhCS0kuTRRuZyr/p4LSW5kgsB/AqnJiQk/Way27i++9JJh+PDhYjQW/u+ze/duaaIm2nG5XL1U0r/+S84zBBBAAAEEEEAAAQTKLlDoh1jZk7InAgggELBA/dQU66rep/S2fTHhK0NSUlLAGYQ7wQvjxmn33nuPO8/uqKXKyg13eeSPAAIIIFBYICHB9EXDBo3OX6Y62CYnJxfeIcprfvnlF1Ez62oOh0Mf0fSeKIdD8QhUNIEeqVbDJ8d1SKr1/mO1bA3qmCta/agPAuUSsDv8cttzWfnjJ9oLXAXaNSqzL8qVIYkRQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQKF6gj8lqnVx3YL+kXev/kqTTGmiO15fni09brOV67lfJfik+adi2NDZYTTOSR7SonXpH5xJvQMw69Xvf65cnma48r/RJQCZNc8iV9+5xuD0y3u7036aid5azBlZrsjyZYDJc8fr9Na0XDPhnZtqS8ly6xi0nX7zdvy6hY+FehIclfNa10/Ni/u5dambbk9TqjYdtitTT/qlieiDBIB3GWGpbvnXvN2Wktvb/nLeyINvn6q2C+DtSgVBOhRWw2mzW7Guuuirh2efHqcmeS+/ukJOTIxmqo232/v03K5VxFVaGiiGAAAIIIIAAAgiETaD0s86wFU3GCCBQiQTOsCYnT77vvvsMd9x55xGfO3/99Zfcf9etMmfuAunfPEUyLC5pmS7i9qvhpLb7ZF+T3jJlSmQnC1SjWUmXzp21DRs2/Orxek+tRMeJqiKAAALRFuiuOtXO+vrrrw2nn356RGPRP/utVqu81NsqVZIMkmn3y2ZXgkzbJpJvSJTLr7pWbr9r7IF9Dga2adMmOeGE47XsfVmzXPnuEw6u5xEBBAIWaJZqM0zq0joxY8K4Oraa1UwBZ0ACBCqjgNutyY2P783/6Dt7ntOlna8MZlRGB+qMAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCAQcoEqqnPt4vYP31W/5ZjrTZ6cXPmuXhut2pKzDYfPIlnw+w6xv7gsz7dmf77m0R4Xj/9NFYkj5NH8k2E/STZ9nf58zxRLvwZlKmL/dX94L62Sa37jgZpl2v/gTj/96ZSL79ztyPfIV7l5/hvV+rJO1pFmsxrHWRJl2HuP1LQIEVCOAABAAElEQVSe1dt2MMsyPX71k11uvS/bM9/cruipd4/K5TdPnlxu35iXJ/4hatOPR20O1ctU1eP3aosY7mxrsibekVw7vU/Cf52W/X6/NMhdoW1v/6ihmtkmr++d6b8r85sN+/2uTiqA8nZUDlUdyCd+BCxqIp895513vu2998cfca9xWasweNDZ2s8//7TU4SrQ34MsCCCAAAIIIIAAAgiUWSCoE9Ay586OCCBQ2QWGqQ5Ln3777beGvn37HrKYPHmyXH7xcLm+ncjoTiZJNhf9UTTkF4Nc+uAbcv75+v3ikV9Wr14tbdq00dQyVpX+ROQjoEQEEECg0gjUVIMxbLnqisuSXnj51aK/FCJA0aReLZncr0CaVym6g9+8XV655neRbr1Pl+dffl2qVat2ICqn0ylnnTlQW7t6xZKt23f3VCtdEQiXIhCId4FqaTbDR7WqmU6a9HIdW7vmifFeH+JHIKoCe/b5ZPhtuxxzlhZszXNo56hgVkc1IApHAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBCISwGT1fJGevu2I3r/8YPVmPBfX8+5l9/g2ZH1Z0KVF44vtl6+bXZxvLkq3/XVRs1oMn7ld3geUTuXr93KYr7HmGK6s+qnfW3mpv917iw2iMM2qA7AWucZa2XmR/WDvhdl5vx8ueC2XXa7w/9Tjl27WmW/77Ai9KfVUq2GN6xW4xkfP1nL1rdH8lGby/7y8TezZdLrBb4p1pZF37hSTFZbfAVypn2dPcfve1bNbvtAMbuVdXUbmxjv9ok2+MLE6oZRllrJDU3Ft+dfbd8kKSktPeObXHLozeLT/NJn7QuuJa7tr6rOtreWtWD2q/QCt1evXv2J9evXG9LT1Uw95VimTp0q5557rl9NutBVZbO4HFmRFAEEEEAAAQQQQKASCQT9w7ESGVFVBBAIUMBqlgdTEwy3fj84xXrRT25ZvbdAMurVlAKXQ27uaJBr2idIgqnkj58v1rhlfF57mTZzdoClh3735cuXy8ABA7R9+7L25NkdXVQJ20NfCjkigAAClU9AjW76UVJi4vCxY++Se+69r+Qvhgjw/Pjjj3Ln1RfIrMGlh7Iiyyd3zRH5c1u+pKfYZPvebFk4Ik3GrygoeHNpwW6nR3qpkLdEIGyKQCCeBAxWi/GhZIuM+fzZ2uVqXIynShMrApEWWL3RLeeO2mXfusv7S55dG6HKD9do4ZGuGuUhgAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAgiET6C3yZr8Q985v1jS27UuVIp9wyb5uevJ/uoLz1QTm5Zt0Qp84vp6o+Z4aZlDc/o2anme+1TKb9Wfv5QcLGIzT0poW7VXlXdOshlth/pvlpLsyM35322WxGcXeHZPrR9cBkdmJ/OWFcgFt+6y7832/enXxF81zXjSZ8/UtvXsbDlqz+BeDhmzy2uZnmR+wdYoqAxcqnPrpfZNjnlex6wc8Z2tMiltkHj9WA5OEeMDKQZj49uT69iGJVYzWgxlPsTSJGeFf1HrscamSTUKxbw2f7cct+Zpx36fq7faOLfQDqxA4F+BVGvypiHDhjZ65933S79pq4xq+izLvU85WZs/b+4yNattxzImYzcEEEAAAQQQQACBSiwQspPRSmxI1RFA4B8Ba0qifN2huqnXt4NSbSmJwX+8PDnfI7OTusqUab+Hxdbj8cibb74pDRs2lJ49e0rNmjXLXM6qVavkxXHPa++N/0B1FDbuzHM4L1GJfylzBuyIAAIIVF6BWglm0ycJCYm927Vra7j3vvsNAwcOFNXRtkwibrdbFixYIDNnzpShQ4dKo0bBNSiUVtjvv/8uw88ZIIsuSJTUcnyXTVzr1q74yeHM98lZqszfSiuX7QhUcIFO1mTD1CvOTa3y/O3VLWZz8OeJFdyJ6iEQcoE3v8z1j348y+UqONDZdlLICyBDBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQACBeBdIN9msS9o/cEf9lrfcUOIMqr+fdp7X3mSnOfXOzkHX2T13tzheXm53z9/rE5/2vLj9L6nMDs4O29hgNc1IHtGyTuodnYqfPrWMpXuW7ZO8y37zu2c2LNvNKWXMN1y7tT9rq+/KnbVNlxbRYTXQMp917fS8mL97t5rZ9kSVduO/6asnivEmk2ijephTDLcl107rbk4JNOtD+9/vzJQVCVU901rcVGIn5lf3/OG/e+e36/d7Xfobx3koA54gIFLVak3eO2XKVONJJ50UFo8NGzZIly5dtNzc3CdVAXeFpRAyRQABBBBAAAEEEKgQAtzdXCEOI5VAIKoCVVIS5I8LWyc2f+EUq8VgKN/HSr/vfDJ09ENy402jAq7Ujh07pEv7NvLuST4ZO8cvi3c4xefzFerANXfuXOlz4vGy/aoUMavLZ2uy/TJnp1fm7DXL33uMsn6vQ7qo0fh6nNhbevY6UY4//nipV6/eEfF4vV45rmsnGWDdIH0aJsh7ywucE9a4jYlmmZdbIPeonWeoP+2IRLxAAAEEKo9ALTWI6N1ev1zRvrrJf22npLT+jROkyds5MmPGDDnxRP36/X+Ly+WSefPmyaxZs2TW9J9l9t9zxe9xS/cGyXJcVZccV9skx9Y2izXBIA/NzpfFVY6X738sPL7B888/LzfffLP8PTxVLvtNkxvueUL+d/3I/woq4zN1UVVaN2sin/T2So+65jKmKnq3ZapNqPeXuS67Ry5Te3xR9F6sRaBCCiTarMbXqqYZhv34Rl1b2+blbv+skEhUCoFICWTt98k5N+50LF7jXqJmtx2kyt0TqbIpBwEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAIDYFTFbra2ltW17SZ+YUqzGx9DbdrFlzZeY5F/qqzj6txM64gdTWt9slznfXuPO/2ehJfehYm6Vfg0CSl7iv3+GRPV2/1rTFGeW7qbHEUkK3MeXYjdpv5laGZubQzIyrR/abJ09GO7bkDU6qmvi/pJpJtY0l9ocNqDKtclf7vm32P1NPW9NS03k1df/M2hedy13bX8n2u24vNQE7VAaBNhaLZfnSpUsNzZs3D3t9p02bJqeeeqpezsvq78awF0gBCCCAAAIIIIAAAnEnEBc/HONOlYARqBwCtVTn2lnXdLA0eLRXculX2EoxWZOtLqJM8srcRUuladPSL7ocnd3NN90gK6Z+KBNPN8rBjr5+TZMPVnjk8UUi1tR0yc61i+Z1y6snmWRg08BC3qBuSp+/2yerVYdci7pEWDPZIJe2Szo6jAOvfX5NPlzl9v/vF6dR7XuNmsXwrSJ3ZCUCCCBQsQSqqOpkH1PLlP/FmSmWeinGImu3cLdX5u/yydocv7SsYhTVCVc61zRJUgAzW+51+eWa3zX5M9MtLRrXlzUbt8rAZhZ5uJtBGqT+V+73G9wyeq5FFixdGdCs5QcDv3T4MLGsmiovnFj+tqHV+/TOtnnOXK82So3/8PbBMnhEoAIKtFaz1v522xXp1R64vlpgJ1wVEIMqIRCLApN+dWjDb93tVLPbnqfi+zEWYyQmBBBAIMYFmlssSSutFkuO1+e15+Y5pqt4x6k/dQWKBQEEEEAAAQQQQAABBBBAAAEEEEAAAQQQKFXAKInJW8yJpn1ep/Md8ft/UCnWlpoqtDucYrImT+k76ydLeoe2AeX8Y6dePu18q8k6okVA6aK18+4uX2mz36hpOK5j6DquhqMu+9T9ifV7b9EybZ3i4r7ud/P3yPuaz7uszT0BjV6/Jn+XdF/zjGO/z3WKcpwXDssS8tR7cJ+ebkq+qsCnNcqX/Az1uqCE/dkUPoGzUlJSvtmyZYuhatWq4SuliJynTJki5513nqYmhbhJbdY73LIggAACCCCAAAIIIHBAIC5+jHGsEEAgpgRSVDR5N3dN8j7ayxrQBZLiarF4j1cG/2yWLTt2i9kcWJZr1qyR47t1le8GGKVLrcDSFhdPKNdvy/NL909znfvytStVvp+FMm/yQgABBGJEoIqatXbhPd2TG4zuaom5D2J9wIWzfvBLt8FXyhNPPxcw2bPPPC1z3n9UPuj7X+fdgDM5LIE+aEO7D3JVp2I5vcBLx6bDaHga5wImk1yanGR49btX6lhPOS45zmtD+AhUDoENWz1y8mXbHdk5vnccLhmtaq1VjppTSwQQQKBcAs917NBh9OIlS464ru73+0W/KeOGkSO1vXv35tsdDn3W8J/LVRKJEUAAAQQQQAABBBBAAAEEEEAAAQQQQKAiCtQUc/J6ywWTUvM/G6SdsXKmIaVZhuQsWyk7p/zi3zrh27ycxcuSElJS5hfs2/e5Apii/taFECLdZLMubnff7Q1a3XZjUCOOZ37zg8wbNdpT9bd+oZsSNYQVPDqrfWf/6H3sDDHfenlkO/IdHUdpr2ctypdB1+3yrTJ3COq4lJZ/qLd3yl3jfbnRCPOgKh2DyvrlPTP8927/bt1+v6uzysAVVCZFJ6qnVp9Rw5xygcNX0LNdcj3PkCpd0gamtTO2S64rO9050mT5I9oLqd8Ybso7y+WWgnZq/41FZ8XaMAm82rJF8+tWrlptMBpDcz9WMHFOnz5dBgwYoHe2HavSPxFMHqRBAAEEEEAAAQQQqFgCR9wMVLGqRm0QiL6A6jB6b0KC+cHrrr1WBgw805CQkCDz5s3Vxj3/vOTk5uzJy3N0V1Fuin6kZYogQc1c++ugZonHvN3fFrKeE3ucfunwaYHs3JstFktgo8VdeP5gSVz3q7xxcuxfV/pitVsb+atjo90jrZS2t0zi7IQAAgjEuID69L2yZTXjuHkXpaUYDbF9Wjl/l1cGTfHLrHkLpUWLwEZTvfvO26Tg19fkkZ6hm5RTn9H31Al5TqdXTleH+Y8YP9SHwjOZTJ+q85lhw9Usv6efPtBQrVo12bBhgzz77NP+7du3Z6tzm1pqZ/+hBDyp6AJJ6n//u03qmwf98nZdW81qsX9OVtEPCPVDIBgBt1uTi+/a7frhD+cGu0Prq/LYFUw+pEEAAQQquoA6F36v27HHXDpr9pxSf/ysXLlSzjrzTG1HZuYiZ0FB14puQ/0QQAABBBBAAAEEEEAAAQQQQAABBBBAoEwCPcVS7WfryOU2Q1Kq+LbOkoKvhvjPt68vsZeZ3gF3h+qAu23CN3k5S5YnmWy2+Z592fpkD3oH3PVlKlntZPo/e2cBVkX2/vHv3OAmoaIoqGB3dwt2YHfXmruuuu6qa61dq661divW2p1rd6LYjYqC5O2YO/8z7m/9g1zgAhe4sGeeZx5mzpzznvd85jJx5g25fJlL8SJ9fC8dlQslElubWa130LOERTq7pEBSJ7fV445UGD36mrmNPlS07XcPR1Irni4b96kwY2a06apTSYd3Xv7bqMIwQ6jlY5lZif524w3ymwIzx6Les0XaR4ZPSyLNmrHfHE5sNw852CwncaRVs4aaJWV5eEdaZ+JIKywt431sE16y35vETpKvFdYQN4KB06NddClNOPepK2lxMOFW9Ii9CCikkqD2HTqU2Lh5S5LfWuzVZ1Jyrl27hoYNG3JqtXoRqTsyqfr0OCVACVAClAAlQAlQApRA1iXgMA+pWRcxHdl/lEB9uVx+JpBktShUqFCCCA4ePIjOnTpxOr2+BanET3o55CIXYkVJd2GPk+2dFVKRfS8bv9/UQ11zCMkuON/msfMvtS0b++J8WzEKuGYeZ473agtKb4rW6c0oTQb70uYB04qUACVACTggAWcn7BlYRtp8ei1Z6r68pPPYvvvbDLZYAwTs2mtzz6GhoShTtADe9Lb/UPc9N3L9jmuidSx4w3tHjYwpclYqQn39/Nz27NnLEOeCBNnVrVubu3Hjxgm93sg7D9Ml6xJwVSqYs218FcU3z8mVvCgpWZcJHRklkCUIrNgew/70e3ikVsfVIgN6miUGRQdBCVAClIAdCJDn4YixY8e5/Tp+fLImxohBBooWKcLFREcGanQGPho/XSgBSoASoAQoAUqAEqAEKAFKwHYCnFQiNegNejoHaTszWpMSoAQoAUqAEqAEHJWAQDxCkKfSNFnvU8rYKpquLbaIXq+3tHh2VRS73Nbtrw64u/bHRN8PkhIH3JvEAfffDLj/OuDWE8qkx/yunJC6leWTdqZ+eblqI+4vnGnOdqRhivROvQa2S9CseYwC+x+bA3d5OrSuv/4RwV1Zz2K3c6FkzUPbTsJ+NWvEPGVHebZmBrnXSpWT7b8aPdZ/RI0nv2ujLPq6pOzW/8p5D+6vjrQlZLnNvCNtc+JIW1bm9W/TZP0t92iOuRK6MYNkE+MYvoxQtdXcNp9bZIRxfLIE0srJIVBGJpPd3bVrl6BFC95cOn0XrVaLmJgY5M6dcGCA169fo07tWlzE588kgKqxEtGQS18taW+UQIYTyE80eENsA2NYlnXNcG2oApQAJUAJUAKUQAYQcPiXsQxgQrukBL4lICcFi1yc5X6MQJiXIZn6SLY+Q3SM+iF5iOxNjj2J3YA8XAaUK1umy81bt0lV2/7Fhg4ZxG3esvmpWq0rHluWA2w3Jk5U+252d5Xld7bLfEi8Ic2+roOh3nDMmj0n3rFvC/gX3dpVK8Lf7T3GVcr4gG1GlsOx1yZcCrEgu4RDTpkA7YqI4SZJmJWJtCm9OUb9NsbSk4xv37djpPuUACVACWQCAmKFGM/XN1bk9y+UeGbXv4NNuP+ZRYgGyKMA/As6RnCEtyoL6u81Yd7SVejevXuSyMPDw1HY2wsh/flHgrRZhv+t0W9/bLyuMsGP9MCmTS8pktrdzc11c3DwO0apjPOtL0Fha9as4UaNGmEkWW35BrGzt/MPRpuyuSkbsCxyCoQCjjxTaSOjYu5yHMd/qLhMVjpBTSA48OKpkDFXf+zp6jHjx+yJXwAceBBUNUqAEkiawO6Taq7nmDC1zsA1IrWvJd2C1qAEKAFKIMsSqEwC6V2/cOECU7FiyhPShoSEoHjxYlxMjGowIbUqy9KiA6MEKAFKgBKgBCgBSoASoATsREAhU0T06dfXbdiwoUylSpU4nU7XmoimmaXsxJeKoQQoAUqAEqAEKIF0JuDk/Je44gB/J79pVr+x6ne2N3tWVoiqB6y2q2LRQY8RcuSkRZY3D7y7dkjYoC0FvXIWC/a6eHNuxxswIk9iEOLAi+HMe1gmXjdHn/ZyaCfbpgNDzAVvKkXT5V4OTBN4bzGiWswzTlVuPiNk7Pqzwl9Rdy3P9KFcc5eSwnLyvHblMPjtNlyL5swrnE9Y/R2s180xbdIvvKyDur5dO6bCIBaLo/186zsfPXbcZptqe2E7cuQI5o/oicPN/jFHUhs5vCF2Y/testj3VgRpDk8sWLoSderU+dolef+Ef8sW3PXr11UqtYa36Q75epBuUAJZlAD55zxWpHCRxjdu3mAqVKjIvXz5Yj0Zav8sOlw6LEqAEqAEKAFKIEECtnkAJticHqAEsi4BpVK2VSZTdD1z5m+mdGk+8Wj85ejRo+jQoQNHnD/rk6PPnZycgv/4YyEzZMjQZP9vrV61ihv980+amBi1c/ye0r3ETS5C0IL68ty9SybiMWoHtcK0FpTbbsSz18HIkSOHVYmfP39Gi8Z+KGgOxnrfZKO1KjO1hSTrILY9MYI4BcURFRkZiRXLlmDenNn4rYoAA8tYnRvFxEs64/JA/X2NCdWIAEdypoozHrpDCVAClMA3BDrlkDLr7/Z0kbuTwALfLs8iWfQ8w8Elb1EsXrEG5cvHTdS0Z88etG/fHrrh2b5tmiH7jyNY1N1twJ8rV6NHTz72gfVlYN+e8Hi8G+Orpm2w/CiDBZW2xGg+6bnhxAl1nXVt0rV0aenSpYYGBt5P9iR3cHAwyPMTcSKIWUI0/lGplIf7+TXItn//Aas3cj5a5KhRP3Lbt+80aTRaF9LGkK4jpZ0lRYA/J9ELfsnBjuztGieia1IN6XFKgBLI3ATO39Sh2aCPWq2ea0dGcjxzj4ZqTwlQApRA8ggo5bI3/q1a5QvYtt3qM2zypP1Tu5V/S+706VN3tTpDyj12U9IxbUMJUAKUACVACVAClAAlQAlkDgKMQCB4YbFYCnz8+BEeHh5ftfbz8+MuX7r80mA0FP5aSDcoAUqAEqAEKAFKgBJwfAIiODk/lfivLCAq1ipBbTnOAu0fPlyNzQuYvO38E6znaAceTpvHPju4jsm2wy++AYkDKWt+FYOotics5qv5HVpPn4ZvLVNi8gr8ndwciF58VZoTB9smOeuyU/K0sOqsGr9FxpecinmMNi82csfc3jBixro9J6/lNdNpjFF3CdND50N2tXwZXVJOQCaVXHRzy1bz4aNHjJtbxvyueXskT4+c+DhADpHA+uceg5nDiEsWXIxUYtHy1WjevPnXQY8fN5ZbsXIlSRAeVY4UPv56gG5QAlmHgJNSodTOmTNHMJQEOvt3WCQAMVe3bl1+n7f9G/5vOf1LCVAClAAlQAlkdQJfb4ZZfaB0fJRAMgh4y2Syl/v27RM0btw4yWY3rl/l+vTtz0yaNAmdO3dOsn5iFa5evQrygdBCIiG5k3qRidVNq2NKMf6s4iHqdbitUmFrJl576PLDeRPW3FVj7C+jUb5iZURHR+PAzi24d+c25lYH2hZOeHLDHv0nV4aFONc23a/HjY8sunfpBN/GzeJlQ+QjWuUmH38Zkxav+jlDJop7yTVbOBRaG60n0bE2alkMITrE9dhNrlK0PiVACVACaUegupMAp3e3Ukoa5hfHc7Db/ITFwOMxOH36NH8fi6NFUFAQ9u3dgz3bt+B20FPc7+WCwm7xRMRpk947EXoLhpy34Fa4CO06dEKFKtVAstVj85oVOH/tFt5954ocVpyK00rPsyQDcPuD6iitGXyUj/dp1U9icmUy6fW2bdtU3rp1W9ybV2KNrBx7+/YtcZ4dwfEBM86ePZ+kLL1ej3z58pL64Xx2Lz7LF10ylkABuYy5sWhcjmwD2rs49AfHjMVEe6cEsj6BTftVGDrts1aj43gLkNNZf8R0hJQAJfAfJzAlm5vbxNt37jA+Pj52R3Hw4EF07dKF1Wi1fLS5aLt3QAVSApQAJUAJUAKUACVACVACmYcAP2e8xFnpMhAMJ5o+bTp+GP5DgkEfAwMDUb16dT6r7Q+k3bLMM0yqKSVACVAClAAlQAn8Rwl4QiR7IhtwVSnIXihJBJboYOhWVuBahwYxThnkjJakkt9UMKvV2J+rGJfjrj8jiG9K8k3tjNvlzBaEFt/JsYEFSFCXJM0WMkxRp3IvuUBFKSaXQJxhOiTVsZlkMM4XE8SFlZnNuAhlSVV3iOM61gD3wEncWpdzTAEhn5Q08SXU8h4do8vrDNBVIDWfJF6bHv2GgITsz5LLZCP69++HWbPnMApFxme63rt3L2aN7IvzrZM2e+FtegMeG3EyxAnhOgt6F2HRuZgTTr81odMhtZbYcnUlYzzwzbjpLiWQKQmIROKbBQoUqHj37h1GLpdbHQNvyz90yDBu956/IGSEEVq9dgCpuM9qZVpICVAClAAlQAlkAQKO+8aYBeDSIWRKAtWIg+0V4hDCuLvzfq7pv/AvdO3a8UmCUIWsN9NRg5IyEa6f7eisKJvTPkHGLn8wo8FfKhxv74y6XvaRmY48ktUV/3JdfpsBP02bjyFDeH/ZuMuY0SNxfPtaHGkhQHZp3Jd1E8th8Gmtft9zo468hPcnLffGbU33KAFKgBLIEAJ5XMTY6Cxlau71VyrKuMe/js+5ZUbABzdcuHoTuXLliqMk7yxJ7qnY1VKBlgUdK1BCHEXtsKMnEQ09V6vQv4wMs2qKEox8mNyuWu9Xacm9NEBtwnfJbZua+jKpOKx6jVo5jh49xkgk/Px3+i8d2rflDh0+fMFgMNVL/95pj4SAh0LG3J76Q/Zco3q7xv/np4goAUrgP0vg9FUd/Id+1OgMXH0CIT3fV/+zzOnAKQFKIF0JdJBKpTuJEyzTsGHDNO34xYsXKFO6NKfT6/nAOg/TtDMqnBKgBCgBSoASoAQoAUqAEsh4Arwl+1wXhUsDg9kgy5/fGz179kCnTp2YYsWKJVu7WTNncTNmzOA0Wg0fuCYq2QJoA0qAEqAEKAFKgBKgBNKegB+j8DggGxakYERSm3szPz0E88kf2HaRjx0rcnkiI7g1aIT5/bu/RW5LaydSK+MPhVXfZzk41VXQvG7GO/xZo/GW2FmWahXMvZGWc2ib7j7qV3B3LmFc590j0xgC5QmcYh4smSVqKuliDb3VMjNnQoeYsuqPlmDelnOn1Uq0kCdQzUUumUjsppo3a9IY4ydOYipXrgxbkvvwGWavXbuGK5cv4eq50zj690VcvnwZNWrUsEqWD7r004jhOHjkGMi3HKt1EivkEyC1adoAdzqLke0b+93E2n177FU0iwa7VBqSWGeGyoxZ3x6n+5RAJiHQj9h1rjl//vyX/9nk6MwnLiFZbrEtYBu3669dMJnMMJtMn/QG/XYiZyxZDcmRR+tSApQAJUAJUAKORsChX8gcDRbVJ8sTqKtQyM9++BDCuLi4pPtgZ0z7DevWLMDVrdmRM7sQc9dFGX/9I8JInkerEWXS1NjO2QlH2hd28l3eUJH8t89vSKmMHLqdtABepbF5x254kEyuhfLlwebaWlTyyNo+GryTlddaLaLVGohE8cfKkey3/k0bIt/nG1hYO/5xHqWRONwuvWtgp1/TmYQMthDHql9Jcdg3mOkuJUAJUAJpQYB/LuwuF+GPOl4i6e/15IqEMs7eDjXD/zCLS9dvoXjx4lZ1GTZoAAo/CcCQcqm+tViV70iFtfZaMHP1DjRu3BgL5/+O2eSevrWRAHW8Uh9d9GE4i9o7YrQ6M0hed9xPw3FLZBJmC3Ga6qC/UwASJ4ZkLCQT2o/dcfT4uXhO1Gmox1fRvXv14Hbu2nlDrzfxz0J0SR8CrkoZc/37bi4+s0blyDQfxdIHDe2FEqAEYhPYfVLN9RoTFqM1cPz96XHsY3SbEqAEKIFMSKA1+ZC8d/369ejcuXO6zZdHRUUhX758nFqtbkuY7c+E3KjKlAAlQAlQApQAJUAJUAKUgDUC85Vy5WATa5K18m+Nrt26ME2bNv0SlNNa5ZSWmc1mNG/WnLt86bJOo9O4EjnmlMqi7SgBSoASoAQoAUqAErArAZHTr8J8dX6Vdt2fIm9O45nxrNx0AY1vnMwUjrbat+9wvHRNLsc9/3SbW03J+Yrsdto8uqxJNGNE9pQ0T/M2p65o0WtkmPmBqIx1o8I018C2DvJHP+QelpjA5HdyTI7fjqL+08VsNlNtbqxicYq4jlN31142nVhNstqO+Fb2f3C/oLMYE4mJbOf2RZzQr5REVsNTBD7BjPvKGCxZsgQ9e/f98u4XFhaGGzdu/M+B9hSu3roHD2KkXNVDiGo5DKiaW4Qy7sI4SQyW3TNi9DkNeAc+geD/k9g8evQIHfybobdXOH4oJ0LHExYU9OuKxX+uSPYpIN9jULpYISyrqkWD/Kmz6SJOtvDdFaN5FWPZoTFhAFGGS7ZCtAElkP4E6stk8jPz5s3DsGFD7X7fDgkJwb59+7Bp02bu7t274CyWYIPRMIoMc3f6D5X2SAlQApQAJUAJpIyA3W+QKVODtqIEMpxAe1dXl11v3waniYMtbzTn5uZmdZD9+vTAvZuH8Pe6bHBR/v/L4b+Vn70xoe/4UM3NICMnEnIrNDocIcdukFX9b51U/K3v4oTDt7q7yvM6x+87OXK1Jg4FN2gw6pexmPTb1DhNP3z4gJJFCuJVHxlkoqx92cm/0YD7T15+cS6OAyHWzoMHD1CvRhXc7iKBhzxx7o+Ic9XgUxr1wwj2I3G4bUPEBMUSRTcpAUqAEkgtAbFSjD+IkL5z68olfUtJEr8okYrfnWWRvXY3LFq2PNG+27Vogo64hLaFs7af3qiLZuRsPhxTpk2Pw+P+/fuoU7M6ltcXoW3B1H/7+vFvjT7gsfEiuRc0Jh2ldmKWP8/lyVrVRcH84qwU5JzxY3Z579bWHwYWbFRj2U4hrly7a9XZljdm0mq1SIsgJd8N6Mdt37HtiVqtLxEHMN2xNwGBs4I51LSWvP7OhR4yewun8jKGgE5vwdPXJrwmUY9fvTPj9Wvyl6yv35sREmlGuJaFUixADrEQ2UmAmGyMCO4QQsgxEJNVYhF8+etE/krIvpHhYBJw5K8FRoEFJrLPl0WDRSTHIsJCZBpZRJHVhdxOcpMkyN55RChQ4J/Vx0sEH/KRrYg3iQzrmvrrYsZQpb1+S2DhpmjzxMUR7zQ6jr+vRH97nO5TApQAJeDgBLoT59rNO3fuZFq2bJkhqvLP0kWLFOZevX4znyjwc4YoQTulBCgBSoASoAQoAUqAEqAEUkcgr1QsvSkQC3LNmD4DI0aOSNePwUajEU2aNOWuX7tm0Oq0SjIUNnXDoa0pAUqAEqAEKAFKgBJIBQGJy1FxtR8aONUelyrvLd36emzhrlWF5X6Pa4eQCs3StOmFll3M0bneiFwmVkzTflIjXDXtNuv7Mlh4aHme1IhJs7bLAqKxYoHafE5aIkXOoGmmWCzB47Xv8Mopp+lY4WGp+n3HEpmmm9NCjmJd6DN2u+vtVH2cDtAvYdfoZt7SQvVfDBAvUIhwXGNGwyd9XZHfulnRl/NoIN63xPQAYTouSZvYxE78+iADLoRJ8DrGgqaeJnQvLobXN3bVgWFmNDuUeHKIxPr4YcgghF/cjnV+qfppfOmCt5tuvlelCfrMHlOb0YkUkgxFdKEEHI5AQ7lcfmLa1GkY9dOodJ23UalUGDxoCLd33x7odLoThExTh6NDFaIEKAFKgBKgBGIRSNcbZax+6SYlwKeKnaJQSJuIRE6FjEaTWK/Xg2EYkgFUCPIwZyLfv8KiotR8xrY7ZF1N1ldktfsiccJck5kZXbF0DiavhwAFPC1wlpuhJM6P956KcPaGDt2698PsuQviREhKShE++8WC2T9h13wFvEgG1zELNDh6iWQybdWGPChqsWfPfnRpJsO8n5SQy5L0aYrTnYm8mD1+ZcLHz2aysohWWWAgkZGMpNxAVr0BrMFoYaNUFnOMmmMjYyzsy/cm5tNnViIUQi0Ad9ZiQrUxlWW5f64iTdWkR7jOggYHWPQY+hMmTPotjp6xdwICArBl6jDsaZJ1LzszbhgRWbYblq5YFXvoVrd5I8pSRQpgTtkoNPWx7RQ8jmDRar9aE6q1XDOwaEQE0xdyq3RpISVACdhAoJJMiNPjqskUP1eW2jRBz2farrjTjOVb96BRI/4SlPgSFBSExnWq4lkPJwjI/T0rLseJ89rcdwVw6VZggsN7+PAhGtSthd+rmsBHckzN8kFtQcUtMQYNyx1VypkyWj2XN1d2oSF/HpHF21MsJGUiV2eBSCZlRBIxw/DZaJ3ExFGOrFIJA48cQnjmEsKHZNi1FtgjKd32ndZg2Aw1qlWrhlKlK+Hypb8RGvIM66cpUbaoE0b/rkLOQj0xZvxsODnZPtZz586hV4+OKFdUgArFzBAJyXONSUAcA4V4F8rh3LUwSJ2YcL2R8yY6apLSMwXH+RtxH7LWk8sl5WVSmbdOb5CT51OGj5ApFoshkThZREJBRGRUzFVSbxZZr5A1SyxkeD965hTNuLvbS+HmkvoPCFkCioMPQq2x4GqgHpduGnDxsgHXn+iJk6wQJcUylDBKUYqRo4RQisICicNef4NZIx6yOjyy6BHkpMVD8vet3ohy3hLUqk7WqlLULC9FLnLdoovjE7BYOLQc8lF74Y7hMPl98h8O6UIJUAKUgKMT6COXydYdOHiQadCggUPo2qVTR+7ggQN3tQaj41qhOQQpqgQlQAlQApQAJUAJUAKUgKMQEAlEt5ykThXWrFmDrl27ZvhHAIvFAj9fP+7q1asWkiWlKOH00lFYUT0oAUqAEqAEKAFKIN0JlAUjvA2xLAwC4SuwpicwaR8TLXjbQ37lnxPCyWrPxQlOyleSdls9RQVTP+fImfXQLvTmfM/uZtxJIgdHXyJu3sH5Zh3Y7NebOuzHPW3Ac+TYeN/0aj8xmHDAZciUMPO7vQLRRmUBB9TuH5WKRD9mjxf5QVhZzptuOPYSpAtBpccLuCNuLxgFw5sqp265a7qE4eo2kUbofIikmNRJi9daTkr4E+/D/5VAXkzKyEqYOXNBjdCQF6y+ISk/T9Z0XeQizPB2Efx4vpOLQknsjxxxGXqOhbpAXezcezDZ6k2ZPBGv9y/G8nr2uWzpiZNxtj+jQLL9blSZ0CfZCtEGlEDaEOAD/M7duWMn17FTxwz/R+bt5r8f9j23afNm3o9iEtEtc0QTSZtzQ6VSApQAJUAJOCiBDL9hOigXqpb9CfAZsRaS7BADO3fuiJkzZzN58tgeFYx/sCIfxHDixHHuwP593KPHTxi5XBYZFRVziMidRtbnyVVZyOAa8Req+uxIPhQm2ZQSW3gH1mItw/DH0s1o04ZPJpr4wmduLVOqEMIu5CGOuY71bxZwSI2Jc8NxpYMzyTCVOt3GXDbjqUtFHDn19xcH6cSpAP6NfdGOuY6uxSVJVc10xz9pLSi/3YSIGLVNLP4dYK5sLrjcVoDkZBLmHd1cl0WRzGO4QDIa1ieyqLPtv0DpX0qAEkiKQCVS4eaKBnL0LpW8a3Gb48CgWWvRtm3bpPr4evyX0aMgvbwSv1ZJXl9fBTjwhpk4FOXfoEfgo2fImzdvkpru378f3/fthqsdxMiRzOAa3wpfdEePXZ9YXN7hRZxZU3cv/1Z2avfHLoiGxHMwpkxNeg5MrVajZPGC2DhNBN9qSSdP7Ts+VLthn5r/uJCPrO+SqSv/sPeTQi7tyQiExaRSqaB5s6Zo3sL/i2OFu7u7zeJMJhMJlrKHGzhwIDiL6ZVKretCGt+wWYDjVKwilzJnz2/ylFdK5vXAcYaQdTV58sqIoxd0OHpch4tBOpSTy9DA5Iq6QuLULpSD+NFn3cH/b2Qcx+EJcb69bNbgpDgal7RqlCFOuM0aydCsngz879bR3rWy/ElJYoBhJChQ2XbvNOTvcBKjYF0S1elhSoASoATSnYBQKFyvVCp7nz59mqlUiX81cqxl0aJF3KQJEwwxanV2opnOsbSj2lAClAAlQAlQApQAJfCfIdCOjHS3s8LZpNKoFpPt0Q44cn6usytZm5A1l5gR5+bAkaRBZhXZ/0jWh2S9RNYzZLX3sod8728z5ucxJPjyBIY8Y9tbvl3krVyxkhs5aiSfIYU/fwvsIpQKoQQoAUqAEqAEKIHMQsDZCZKPcxU75COM3TjZdzcYgVt8h0BOEwZL9BtwUa9hiXrLcRFPdZbwp0ayL+S0n+UQStQQSd+BY5/DoA4iplkvCIB/nXSDyTYbC4g3xPIg2aA7CoGLV6zi1G2yn+5Dv7kh1179khGIRKkTlg6tT1aqz5pbiISKvsXSobfkd2G88gn6kZfM2rN5HRJmre7vzfUfZxP9JMud/MGlQ4vlulDsAsx3S/zqkPxiI+C/M7vdm2BZoNwrKC2qGvtQqrYjLKFoF11aR1zgqxNBgbGE8R/vPclagF8FEBRUClxLMRxTxMQZ8umhd3VlsuvzCPKb8wuLir0FReSeQm/GU+ADfs0h8Igl6p/N5+Yg9ND7ctLuRxj9lqY6mPX8xSUyXsW0KZCQ7LXP1zdR5PUvZHuA/bRRJWmpb2JY1N5jwoFjp1GjRo2kG8Sq0b1DG9SNOplsG7pYIuJtfiZJi6oHxGhCddx4kwWL4lWgBZRAOhAQicRXnZ2VVflvohUqVEiHHpPfRWhoKLp368FduXqF02jUvYiErcmXkmgL3hjRn6wVyUoeTphsIkakJHNoHMuZQ0gZb3u4mazXyOpoNvjORKcDCoWynsGgB/GhSV4mN9KYLpQAJUAJUAIpJ5D1LXNTzoa2tA+Bqgq5/NLIUT8Kp06dQRLV2vcnFx4eDu/8nji9JgfO3zZYjp7Xqa7c00tJJh1OJmE+i0QC8iBkeQMIcpDnojwxGkuh6uWl+l/6ujq3rK9I1gjNJNKQe+0QvHz9HtmzZ0+07bixP0OuWYeJQ9wSrZeeBx+9NKJyp8/Qktf8VY0U6Fki5S/AEXqSSW+HCacuXkPp0qVtHkZ0dDTy5smFV73lcLToVhMu6/E8isPmJk4Qp+CDsJZkEK6/zwT/XkMxY/Zcm5lUKV0M80qGoHqe5M8/xRg4VNseowlRWSYbLJhvc6e0IiVACfwXCWQnEQYfzK8v9+hTUpKil+5a+4AVfx1HlSq2RUm9ffs2WjTyxbp6Fvjm421+kr8cI05mk69ZcKKNGK5SYfIFpGGLIedZlO4+ET+NTp6NlW/NqmgnfYjvyqT8PkyspZBteTRxLhOgS1MZtszNlYYjTZ7o0HAWxf3DEBGlSbJh21aN0bnuPXRprkyybuwKL96asGJHjHHtHpXZYOBMMrngMXnCDAP5TkKcnz31Bi4neW6TFvMRa5vUlst8q0qdDpw1okD5nzDu1/GxRdll+/79+6hZsyan1WrXkmwJ39lFaNoKUSpkzP0FY3LkG9iRpq5NW9RJS3/43IidRzTYsV+DmGgL2gqzoTXjhkqi5L2rJN1T1qvxgjVgvzkS+4SR+AwzOjZVoLO/ErUrSbPeYDPZiK6RTMsN+oaoNHqO92B7lsnUp+pSApRA1iMgJg8/Qd7eBQqfPXeOiR1cRa/XY9myZdi7bROePHuOwjkkKOpKPrGSB8u3MRZcDNbBaDSmKxHiBIDy5cpxL1++PGhm2dbp2jntjBKgBCgBSoASoAQoAUqAJxBQt069LufOn2VYEkHqr7/+woQJE7mPH0NAggbyjqtNyKrlK6bjMlwilM2ViVydauTriZI5GzIycdKZmLSmKHxQPcJH9SPuXdRdvFc/hpHVQyqQqvWsNogFe52M4QlZt5E1trE2/0GfzwQ7gsxBN1IqlAXz5cuHmbNmMi1atICjOtYSfeMtvXr2xtatWzkRhIFGzshbW9NgNvEo0QJKgBKgBCgBSiBrEVDA5dVc5TafSuJ60FhUaKQrZnFqv1UgLOBrt4FyZgM44qBrifrHSZczaTlxlaEMI0yZXURiipnubeZwczrbOiQw+YZliQlOg2Mhh0/g+pDvTdnONbI/CDvoy37SIbzBIc5y05t/3nW4JVed19waYwGmtpj3q3G8pUzMU/Mq716iFq6224pm1CgKP5hu9heNFHSRDkuRfVRierMci+2GZZwEUsZTyDvJeiMPWSWM/b5RHzZswXTLOE72/SOGcVKC/XAT+oCWITCqeUdbLjH97HCsMLEvuxfU21WeW2F3fHZQL2ERU66bcU1YEifPXbL5vfXgwYNYPqYP9jSyP9YnJEB1vZ0xmmgj+KxOpxLWnB6hBOxGoJxEILtksOgU/G+7ZcuWdhOc1oKioqKwds1abtr06eQ7LWOMio68R/rcQlZ+zurzN/0XI/udyeopEUjqChhxQT2rc3KTepBkVyXh5VqeyaMsDg9lEYgESdtnRuje4eb77dzdj0cggOWt3mJoT2Tf+qbPtN6t6CR0OiAQCzzr1/PF1GlTGN5Gl08E4uzszBkMBj4aQ1haK0HlUwKUACVACfxDwCFfGOnJyRIEypJR3FuzZg3Xv3//NPudDR3cF4VdDmJUH2IBlw7L6HnRyFXiF/zyyy+J9uaR0xW3d7rBy8Mx5rfKtwtFbu8aOHrs+Jcsq48fP8a6NathNhrg5p4LJIsH/ly8ENnN4TjfPukX/hX3iENqsW5YunJNohysHZw5YwYi983AtJpJ92Otvb3LtCYWPpuIh+rCBZgxfQYaOodgaYOUOzWwJLOh/xELVEovzF6wBIULF4aIRBP08vIC/+Iy8vvBiIlRIZxkvK3t7YyNfgxSOyFxJ9SMBn+ptDoz6hE+N+3NiMqjBCiBzE1AKcLuevnETXe1VMhTG+xiwlUWC27EQCmTws1Zifp+fli7cQtUKtWXl/rXr19j17Yt2LBuLU60EqNkjtQ5xnqsJ0ZMu/9Cly5d0Dq3Gn82TPn12Z5nkY+C2OioCG8/hidbbFBQEDo1qY1b7ZN+PDoXbELTvSQD/YTxcM+ZC8FvXiEyIgJNmrdE+/btvzjZHj16FB3I9tl12VClrGPcW1sMU2PgiFVo3Tphn4CwsDAUL5of4Zf4YJ5pv0RGsyjQ5BM+hkaCZLFNkw7fv38Pb29vTiwWrNLrTYPTpJNUCnVywtgi3uLxt3bmVUocLAtyKoeWKZpfJc/Qq7eqsPuUBnUkSvSwuKOhyPnL83mmGEAmUfIWyXy7BeHYb4hCvYoyfNdTiWZ1SMBxYdLX3UwyxEyjZs+xobr9ZzSnVBquVaZRmipKCVACWYmAq1Kh+NCqlb9s46bNDD83wy/8R9q+PbrgTeBV/F7Ngpqe1ufu5tzQQ11jEObNX5ghTB4+fIhSpUrxfQ8k6+oMUYJ2SglQApQAJUAJUAKUwH+PQFGJRPL43bt3cYKzxMbw6NEjLF2yjNu4aSMJ2iuyRMVEXSXHR5DVnt/n8hMjwdMWCAr5+gxAZa9OjICx/twaWzdbtvVmNY68nIFg1W08eHgf+fPnJ3GqOfCBkonB3pd5fg8PD4jFDukXYcsQ49SZPXs2Di1/DF+fwbgSvIk792YtzBbjZFJpWpyKdIcSoAQoAUqAEqAEsgQBBeNycLB0cvOO0kFxPMP8tEVYc/3RjLhS3PLMMmjDwe/MOfKECuoe2xlnXI6o/6H8pS1OkwsLpH68L6DjLZ+K7+TeHfdivDwc63mXfyYXlnnFfXIpywhJsHVHW44bozHaEGF5X2aG4yn3DawOL9dwYVovdq5yh31eor6Rn9a7szQ/cAedr3DSgTcEsW28iMM9jKfHnYE+qkEa6tCuoKtgY2BPF6VQYP3bOp+I5tArEzY8IzZTKg6fYnQomlOGwcXNJOmPJA1Vs000n7Sm/n4zqjdtj3XrNyTZKCAgAAdmf48NvklWTXGF9UEGy+hz2hCtGfxHn+gUC6INKQHrBBgx43RRJJTW6FRqHpPPtSw2P+6H/Se2oVy5ctZbZLJSkvQCfEI23t6PzNvBxeWfwHMb1m/EwIGDUCtfb9TO19duo1IZwnD6xSLuccRFModk2kESvXUjwu3pid9fIBCOIwnsCrq4umLkyBHo3bt3gnORu3fvxoB+A1gyB5kp72t2OzFUECVACVAC6UjA+pNwOipAu8pyBGTOzvJQcsNXLFnyZ5r+vk6ePIlxozrjxna3dDNMP3FJi7kB+XDqbz6wr/Xl0KFDWDq3P46tUFqvkI6lq3ZFY9Q8Pe7du4dChQol2fOoH4Yi550NGJlE9qeB5ziU7jYuSWdjax1Wq1wBHRVP8H35tHFwsdZnQmUTLmqx7pUcz58//5qdmI9ovHnLJoQOcoGzJHXOYfwE2L+THUaWAz/3IEpgAiIhHZNT/vM5rWHDQ8MFtQmNktOO1qUEKIEsS6ACiS546VZ3F5mPa+quZ4kRiiTZzbNJ7TuP3u6ACkdfm8E77RKnxS/djxgxEhvXrkFIv4z/2BFNJmVzr4z64lzMB6pIzvL582cULZgfD7s5wS2RpMJ8H0U26xCj0dskvpV/Czx9cB6PD2d8Vts9JzXYcKIEDhz+O0Hdv0xUbx2J7fOSxy9BgTYcWLUzBifuVcJfe4/ZUDvlVW7duoW6devymW17EymbUy7Jri3zSyXM/WMrc7vUqyKzq2AqzDqBKOKMv2KbCos3RKMYZBhmyQU/GzKMWJdGS1NLIMiswxJ8wklTDHq3ccbI/i7w9sz4+0lqx5UZ2r/7aEbpVsG6aA3Xkuh7JjPoTHWkBCiBTE+gJHkPOvdLZanb77dNIrWRRe/O7XD2/AX4yIyYXdWC8rkS/wb5UWNBxZ0mBIeEQqHI2EA/169fR7OmTUmEYv0DjVZXlZwd214QMv1ppAOgBCgBSoASoAQoAUog3QkE5subr/TTZ0+Z5Abp4439jhw5gp07dnFnz50lwXajIZPIzNGq6I/kW+ErMpI7ZOUnJc1kZf+3FiR/m5K1tFgo9rGwkOdS+KBcntYomqMO4yKx/zzv/hfjUa1pIaxes4p0m/UXknkYBfIXRmuf2cjjXPzrgPVmFY4/m8c9Dj/PmSyG0eRAxkTW+aoR3aAEKAFKgBKgBCgBexBwgtOUJk6dfx6vWG71Y2gXbR3Tu5IVBJJmi9POeMIeA0lAhnZZCUvZyYMERUcMSaCGYxS/Wr8VgbOnmrIda+iQH+I++x6yrBssE/Rq7VjZYh8+N6JWtw+WF05l7Wt8Y6efRbWYJ+yvXu2EfXPUsJPEtBGzOfwafgw+ajni+iqOg2ra9GZ/qX01Ddmnhb04aZuNVj9iGE6OMZjvrFsCs+5nO/fOG/LqFtaTsYPLSeNdI/VmDsMuWHBPnx1rN29HtWrV4nW/d+9etGvXDrrh2eIdy4gC/jtPs0Ms6vt3xPLV66yqEBgYCN9a1XG1oxPyOaftvx6fuKfpHrXmdph5k9aEoVYVooWUgO0ESIx50V8MI2zdsOD3qJSnPTFT/393jQtv1kNZ4hMOHd1nu8RMXNNoNJJEH6VQVtodZTz4qTb7Lh9Uj/Ao7CQX+OkojKyW5RhLlJk1Pye98Nlud5GVn+/jLyL8s09OsjYhaxknkSSvXCHLpdFqGB+fAmjSuAk6d+nE1KpV66tfAaln08IndSlTpgyZZuT4eawFNjWilSgBSoASoARSTOD/76opFkEbUgJfCUzL6e4+/snTp0y2bGn7srR92zYsnDMMlze7pXtGolx1PuL2vSfImzfv14HH3mjXuhG61AtEp6bp5zQSu39+O4hMvNToHoGFfywGyST87WGr+yaTCZ65cuB8awYFEnDGijIQQ8TtRuw9eho1atg+aWKxWLBxw3qMHfnDl8yGxbLHexe3qlNaFUbqWRTbasHPJCPxxEkTrXazfv16/PDDD8gu5jCzOtChaMZH2rKq6DeFr0mmvkpbY7Qk8lUdcuj2N4fpLiVACfxHCDg7YU+bQk5NVzVSWP2A5GgYosh1uesRDW5HK+Dr64s1xJnW3d09npq80y0fZa1dXj2WZ3BWW34CtPspCyz5K2HVxgDkzp07nr4JFbAsi8plS2Ko17tEIzk2PMjh54UbEs0IG7uPT58+oWzZMmhWXY8Ns/g5m4xb3Kq/x/OX76yeR16rRn7VMKLjW7SoJ09XJVfuiMGOswVx5ty1NO935Ijh3Nq166JUak0O0hmX5h0m0IFSIdjRpJa05V8Lc6cv7AT0yarFH8PMmLEsClsOqNFH7I7vhDmRW+CQ346z6ilI1rjUHIttpggsYD+idhUppo7OhlKFnZIlg1ZOHoH5G6LMv/0Z+Vyt4SqQltRBLHn4aG1KgBKwjUAb4ly7dVdLpdwvf8rvwY/CWTQ+ZMHDZy+RM2faPFM/ffr0y3Ny9uzZbRvZ/2odO3YMM6ZN5W7ducuZjMbTZpbtSQ59SpYQWpkSoAQoAUqAEqAEKAFKIDYBoZPY6bnSWel95coVpmjRorGPpev2wYMH0bNbb9T1GoZyHi3SpO9ld9rgzv1rXwNbpkknGSiUn3efNHEylixexjsqo0qubnCX+ySqkcGswYnn87iHn8/yDrfjSeXZiTagBykBSoASoAQoAUrAUQm0Kyost36Ty6V/UpsloOV4TV/LWY8Qi7TnMasObAk0c4hiThsO7dKiXPNn1xiFdz6H0MmaEnxCir0u+Tm3w76MKJ9jObLy+kYNOGfu76EWLZuYNnO/1pjYUrbvtAY/jo8w3RGVSvnkti0dpaDOW9aAmqrnnKbcgjhOXCkQlaZNwkwq5Hswldvleo/JJfBM077SQnhTTQmzpmZ/xqnm6EQNa3Vbmqgsby8NJjoE2EEPpVKMi375xEW2tVDIiWdyHJHPo1iUyu6/QAAAQABJREFU2RRD7ISr4/LlK3GO6fV69OvVHcePH0d+VxEmlWfRrIDD/XxhIdekrY+NWPhAiCijALnccyAsPAIKgQkBDYUo7Z4o7jhjtsfOS8K06rYYrcaE+kTeDXvIpDL+MwSkxLF2G+9Y26jgcFTybBf3H9YKhiefz+Nu5C5Em95h+cplXxzhrVTLEkV//vkn1s0+Qa5DY+0+HtZixqm3CxApfIhjJw/blPDM7kr8T+DOnTu5vn37ggT820CK+qVVP1QuJUAJUAL/dQJJ3mT/64Do+G0iIFYq5dG//TZF+tNPo9P0N7Vq5UoMGjwYgXvzokzRjDGCPn1Vh8HTOTx6GgyRKO6c28uXL1G/dnm8Ou6e7s6//JkyECfYQs0+o1HTdli/YaNNJ4+vNHrUCMxfuAiq790SzLR6g2Qg6nZWjOdvP0AiSdzh9ItT7caN+H3mFAi0kZhckUUTbzHEwjT9edg03ooBOjh5FcO1a9eSHMe/AvlJga1bt2Ld2vW4eesGXOUylFJqMLWmFFVyO97kAK/30DMa3V9PjSdURrT5dxz0LyVACfwnCJQghuXXL3VxURbP4IAG1mjrTSwmXzVgP4ndH24SQ06yQnXt2hXDhg1FkSJFrDWxWrZhwwbSZhhOtBSgkgNch4NVFky7xeHACyN69e6NcRMmI0+ePFZ1j1048dexeHP4T6yqH/d5Inad8ddYbHomxItXr+Hikug3wa/NiEEYGjRogOUTleidQRFYJy2JgtmtH2bOmvdVr383pkwejxd3V2DDDFcI0jDD+7/9ffvXaOSQr8F7VKpSB9t27Iarq+u3Vey2HxwcjJIlS3Ika0IvInSL3QTbJqggyV4beH27lyKjnpttUzNz1tJoLZhDMlov3RKDTqLs+FHoAQ/qVJs5TybRWstZsMYUhj/ZUDT3lWPWmGzIkzPha3OmHWgGKx5N7pdFmr3VhkVaOhFVDmewOrR7SoASyCIEJAKMdJMy0852dFb4JBA0ztahLrprwglLaZy+ENdYxNb2gwf0xfuLuxGiAXKXqYlDR47Fa8rPmXnmzIZ9jbkv82TXQ8y4FiHGdeIq+zpCS4LxlEL1ur6oWbvulwB3Hh4e8WTwBStWrMCQIUMwv67MsiLQoH6vtphYC9aSqcFF5PAHq41oYVYjwE+OTyeZ9pqJxSK5Wq15RIwY+Sxsp7PaQOl4KAFKgBKgBCgBOxPILhFJ7glEAq+xY8diwsQJjECQthlrkqN/dHQ0OnXoisd336Blgd+QXWY/B4pI3XtsfTIQb9+9hrOz4zk8JIfTt3UnTpyEo1tuoWm+CcnOBvKvLAtnxo33O7mzr1eDgeWx0WJsSY69/Pc4/UsJUAKUACVACVACDkuguAIut465vZaLmaRtCTfqfscK8SqLbGiQgBE6ps1XQqTZNxdg3NeNba96nr5eYQkplED5o1kLLU93r+Ky7fJzOD3VCwO5SldfMuc2OpYT5ty1Udi1TM8elxd1OGZNYp5xbXL5WsbnbupwusX+Cbrfm8T+Kl8lrCW2fxbD2P3Ye5u1sKin87EI/f8UiIr5JymeI9+1tX8U1ED3uRapfC/JBvErZHdxwgonAdNisZ9c3tZKIOpnkcRhlmSBnbd0Fbp26xZPwvbt27/YejlK1tp4CtqxYGOQAas+esEc+RFbGjAoks0+/wbjL+oMq+/rb6tMqE3UtdhRZSoqaxFoJRHIN8ucXJ1bFBnHFMhWJcWj05qisfh6K1y4eA7Vq1dPsRxHbHj69Gn07jwYfUpuSvGckLVxPQ2/gONv5uCnn0eQZF4TrFXJsLKIiAj07NGLO3v2b2h1Wj5lcbsMU4Z2TAlQApRAFiSQ8R5vWRDqf2xIfZVK5VqSgYGxxZnEGhs+ghmJqoFXr17hzZs3ePv2Ld6+eYl3b5/j9u3beBMcgoY13NDWD+jZSpkhzhjW9B74WzRW7wrHuLE/IXee/Dh6eC+OnTgL9n6BDNGxTo/3cHKthqPHT8LJKelJQ5774P69cfrALlxoL0E2acIfkJ9EsGh0iMPr9x8hl1tPgmYwGPD94O9wZP9uzKrGoFMGOUFbO1d82ahzWmx9JcGFCxdIlr+yCVVLVrnZbAbvyLQtYDt27NwOk9GEPE56rPBzQg3PpM9BsjpLQeUXJPJVlYAYnc4MPu1wSiZVUtArbUIJUAIZRYBEFwxo5iNus6mZMsOz1xrNLAad1uHkRzlUOh1q1qiJPn37oFmzZsiVK5ddEPFR6Vu3aoPAa5dwtxMLudg+E5n2UO4VySr+81UGj3UK7D5w5Ev23YTkjhr+PcxXNmFuzcSduabfMGHGFTWioqJsdgrdu3cPunfrSoJ/5IaHe+LyE9IvNeVT/lTht2VhWLZ0KT59+ogFC+ZDrdEh7KI33O008Zwa/fi2gU8M2HjAgJ3H9TCZBahcqRwKFCyOfN6FkT9/fvj4+HzJ7MBnKiZhWVPcXdcunbhDhw+9VKt1hVMsJBkNZTLMqVZaOuzvDZ6KZDSjVZMgcOm2HsMnhYMJE2AWkxeVRBRvEsgy7WE+IvMU7gNuCjWYNz47ujRXZtqxOKLiExZFGBcHRF9Xqbm6RL8My/TtiGyoTpQAJWA7ARJcaFp2GTPyfCcXRR5FwnNatkhkLRzq7WfRf+xsDBk61JYmcercvXsXDevVwqlWIvwb7Ehv5jD5uhlbn3Jo2rgRFM4uCLp7CzJVMHY0EpD3F9ueLXljlmsk8N2NzyI8UYmgMXHwlJoxrSqDolaeqUP5QCA39Prl9wwicoHl54NuxlGW7mQFArwVxfWlS5dyJPhUnB8S78Q9ePAgbvXqNXx5E7KeyAoDpmOgBCgBSoASoARSSWCMQqaYxHKsrGPHTvj559FMmTJlUikyfZo/fPgQ7Vp3hFjrgdaFppP5ydQ99/Jaf9a+xvan3+N9yNsEv/mmz+js18vVq1fRtU1/9Ci21n5C/8fq2LM53AfVY4vRop9JiibZtQMqjBKgBCgBSoASoATsQUDhBMknkrlS4SHIa7O866YzGG7sxsmH3GcYRU6b2zlCReOV+RbxuwBLiyeX098AwEYALLER2ZejEJfjtj8jkDiWmrr9ryFddMf06YiXQ3lYdx39ycycdhItVXjbSDl9qhnIfKNPTBAXUWYuoxAmnpQlfTSy3kvFR/PYMuiAobIpjmM4ZF3VOKWhlvdora3ISXqdZIQettuzcpowaJeV0MKs573Fo+MItb7TUSHCn3W8RLL59eSKgm4JY+pygkXO6q2xev0m65JIaYmC+bCtVszX7yEJVszkB3ib6SZHGLwPDScJl4R49OgRWjdrhI6eMZhYJfXXts86C8pvjtGG67kuBNXBTI6Lqm8fAgqSrXYnmf9pVjlPW9Tx/o6R2NEuSW0Mx/qgnoiKibCPtg4g5fLly2jdvBMGlt0BEj0l1RqFap7jwMtJqOtXDavXrbQ5IUqqO06FAN4X5OjRo5g+bQZ3994dmI3m1ybW1IGIvJ0KsbQpJUAJUAL/aQJxjDD+0yTo4JNNwFkpf9eufTvPDRs22/w70pFJlBHDh2Lnzh3o1NQZflWBBtVlDuNokWwIDtBgxvJIbN4RgxOtlWh2mMOygP2oX79+oprt27cPP/bvgasdRIk61/JCIvQWlNiix/PXwciZM/7E4p07d9CsoS9GlGYxorxDzT99YfDj3xpsfSnG2bNnUbly5US52OugSqXChvUbMHbcWLiLORxuJUZhK4aP9uovKTmDT2t0e57RrLZJcaLHKYFMTKCETIQbFzu7KErmSP1kQUo5/HBGg23kelu/vi/JXjrDbgENktLHZDKhebPmuHX9Kp734I3VM46BNV0fhrPoctKCDn0GY+ac+Fld+TZd27dBhbCTGF4h6Y8Swy+Y8dmrFvYcPGqtu69lvHF1wby5EVBPjwvvzVj13IiL27xoZsavhJK3oSPPQ7cfGnGaZGJeu9eAvPkKYfGytahUqZLNgnhjLz8/X06n01cjjW7Y3DB5Fd3kUub57j88cjStYz0wSvLE0dqb9qvw0/QIdBFkxzhRHkjtYMxIqWYuAvyE9AZTOGayIfixjwvGDnIjQY1sfgXOXINNR21fvzehVKt3Oq2eowGB0pE77YoSyAoEckjRB2BW3u3p4uQuS72TgYnlUGSLAScvXENynS34e0RzMidWNOY25iQRNCcj2AeRd5F6O2N0GhP48Pc0u2lGnAQ79ymRiE/WqV2nwclTp5N8GDl8+DA6duxoIfPx/L32up1VoeIoAUqAEqAEKAFHJMBHQ1vrrHBuazAZxL71/UgAlcFMixYtIBKl3vg1PQfMB5jk7+Xz5y7CowdP0MR7HLxdK6YqCGBs/S8Er0H9Xnnx22+/xS7OtNsBAQH4/dfNaOE9Oc3GYOFY3A7Zy5159ScYzvLSYDE0J509TbMOqWBKgBKgBCgBSoASsImAHC7PZyk3F6ombmBT/diVQlni3KZLvnNbbBkZta3f3tqct5a7qOrGPzNKhST7vfPDGPPbJ0eF2VbWSXIeK0lhdqxgCgyHut851nAxr0MZlpRt847t+S6nsL80vm2mHYefbFHd1S/h7VLGtCJ/V8czCv3faH4I3olzkXrzapczmerF77rpNHH2755iZ3/2/XXot7V6D6PaWoQBRiDAQJJzZ/7UmjLp4LISoVCQ+L/ie7UFVXYacOXmXRQrVizR30r18iUxtVAw6uZ12J9FovrbcpD/dlRwkwFX79xHoUKF4jThE0rVqFoZPuw7bGskJI59ibON09jKzur7BvbXi9o3ahNKk8M6K1VoUdYm0FYikG1WStzlLYr+yuR3LZ+mo114ozGC379BtmzZ0rSf9BJeoUxllBcNRl4X/t8n9YuJ1ePOp324ErIRbdu2xZhxo1GyZMnUC05nCXym223btnHLlv6J129egTNzn/Um/Ryixvx0VoV2RwlQApRApiSQuqe7TDlkqrQdCPiTbKb7b968yZQoUcImcYGBgV+yuPVr64K1091takMrJUyAN+LrNSYUp8/rcLmTM3L/L2PHqTcm/PG5BE5dTNhuaub0abi9bT62NEzaEDHawKHkVj2u3g5EkSJF4ih05swZdGzjj41+AjT0drwX5prbonEnzIJbt26hYsWKcXRP7x0+Wo6/vz8UrA6B3cWQitJ/ni6SOAdV367SfNRaphrNmJveDGh/lAAlkCYEssvFuDu5uizP8ArSDJksJpN8WPFIiKFDhmDGzBmQSJJ2Ek0TEkQo72xbv159XL5yGUE9XVAwA4MbWBtjDLmntjrKolT9Vl8iPgrIjHbspWHdGmgvDkTf0kkzPPzSiMlPciHw8XN8K+dfmRs3bsSJBSOxtv6/JcDvJAvnxIs6PNifF6UKZ3zG9f/XLHNu3X1kQIUO7zFl8jhM+o1PZJD0wj/D+fnV565fv35bq9XbO/pHt8L5RCsfHcqnFInoa17SZyPhGks2RWPKoij8KMqFwWQVpvKjSMI90SOZkcBBYxTGkA9mnfwVmPdrdkiow22qTmOX0Z+0B89qNmh1GJYqQbQxJUAJZHkCZCalu5uUWXWhs7O8gKv95lUq7jRjxY5DqFuXT65t+8JHBB7QvQMutxfDQx732d52KelTc9Mjg2X0Oe1TlRGlSI+W9OmV9pIEgdouzsoDUpnMrV27dmjQoCHDO//o9XqcOH6UW79hE7//q9lsnvU/OSKlUqFaumSppHefPjY/7PPvyT4+3tyHDyGjiZwFSehED1MClAAlQAlQApmBAH8f5MjaQygQTlUolT5icg8d8N136NevL1O0aNHMMIZ4OnZs3xnnT11HxVwdkEOWH57OJWHPTCWxO4zSh2BjUD88f/kEuXLlin0o026fP38efTt/j25FV6XbGGIMobj8Zh13N/QoBByCjJyxF+n8TropQDuiBCgBSoASoAQoAcjhvGegbHzrLtLvUzw5x1pIkDpdAVbY7HehqFSnTEOVI3pr//Dmau1Yynj6N3NIvXUfQnC0eDXO/Z6/zXNZ6TEQC5kkDau6j+Pu+DiUXi5VXnHHhEWZ4iJZemCwuY/80Q+5hyUmMPmdstvcJj0rnlM9RfPn67hjbm8YJyZpW5v01C2xvtbpZmO1eL1FNvQBSbyYcrtb091NnPHMuJPQRzf5X38uSjHOtSnsVHSJr1wutdFu5PRbE36664aHL94kaIMUezzE3gXd/BviQZcMMVWLrUqabX93ljgd95+K4cOHJ9gHP5/fxLcO8qkeYU2D1Nlg6cwcqgbEaN7EWMaaLFiaYKf0QFYg0FMmlC8CI3Sr5zMQ5XP7MyJB+ly/tKZorLjbASp1tE3/65kB9p49ezByyAT0LLaGBKdL8SNhgkPl7fxUxjC8j3mAaMMnnHq5GHywt65duybYxlEP8GPhk5WtXLGK239gH7F1krLRMVGXib6DyPrYUfWmelEClAAlkBEEHOplMSMA0D6TRUDh7Cz/9N2A7+TzF/xh02/nypUrqFmzJq4EeKJ6OWmyOqOV4xMIi2DRfEAICpJnwdV+cjgJ456GcJ0F5XexCItUxW9MSub/Pg9XN8zA5gZJP0y+iGJRfz+LyzfuxHGw3bVrF4YN6I29zUSo5OFYL8oGE4sSASxc8+TH33+fQe7cua1yyMjCZUuX4edffkZRZzOudlGmiypmks1QbQTcSHiyp5Esam2P0ZHIVx1I50fSRYHkdzJXJBT1MrPmPKQpbyxBF0ogXQnIJLKfSaT70SQTqB/pOChdO7etMxGZFP3bv6BTpXVNFDKN0UKyjnPILmVIFlfGbtHsE1Jl7X09xl0XokrVagjYthUeHh4JVc2w8hnTZ3xx+v2tMkeywzrWRwgDmRjtcpJF1Tb98Ou0OXEck5s38kVF7R1MqJz0ffrIKyOmPffEnYfWg+WPHP49vO6tw/fl4z9/nQs2oftJDX4f644+bZ0z7DxllY7VGgvcqr8hhny9sWr1epuGde/ePf4ZmSMRLgeQButsapRIJWcls7tfW+cWf4x1T5+Z10R0yayHrtzVo/P3oWhpyobJIk+IqWNtZj2V6ar3euNnkuH2Axb/lgPd/en1NKXwdxxVo/+ksNcaLcdbQ5tSKoe2owQogSxLoLpchFNH2jorquWx7zwUH6xucXhJnCBZbG1deKOJ6hXLopdHCIaWTbnxi6392aveq2gWlbbGaHRmlCAyg+0ll8pJNgFv0uL1gQMHvgTkS6z11KlTuClTpr6QSaU5hCJRttDQ0Djvj4m1/fZYo0YNuLNnz+8mTrsdvz1G9ykBSoASoAQoAQcmwEdWqcAwzCRXZ+fGRrPZqV3b9ujdpxfj6+sLodB+gVccgYHRaERDvybQB7uhic+YNFEpRPUYZ8Jn4uUb63PKadJpOgktVbwcqkiHIp9ruXTqMW43GmMkrr7bxN34sAdCMK9Iplve6fZS3Fp0jxKgBCgBSoASoATsRUAE0YRGTh1/naxYbRdjgBba8uaYKu0Zp7oTM81DpiXqDXSrKnGtPz9inFxc7IXWrnIute1hjnR+JnKZYu/Y06lTM7TcX9yt9R5MhZKO8Wk9WmVBrjqvuRBl+biGoKkbZqpb/6J9h1BJbtOBQoMdciLcaDEjW+AEbrXzaaaQkI8vmTmWUZpOlmt5NJy0+2G7XG8MJ0brzbfXLVUKja26FZcUXORLvqgkY+EztuZZq8H7j2FwdXW1ueWAnl1Q4cNB9LchkYDNQh2k4vHXJsx9VwCXbgXapNH9+/dRv1Y1nGwlRskcqTut+54Z0eeENtrAcmVI58E2KUArOTqBHyRC2UyxUK7w9RmCMh5NGAGTrH9Tu43v/LuVqNkpD2bNmmk3mY4g6MWLF6hSoSaGVdifJurozSoEPB6KISP6YMLE8WnSR0YKValU2LdvH9auXcddIcltZBJJRLRKtYXoNI2s4RmpG+2bEqAEKIGMIuBQL2YZBYH2myQB3tOD5WvxWRqaNm2aaAPilIQ1q1dh4oSfsWmGEk1qyxOtTw8mToCPHvLHhmjMWBGJPwjLDkWdEmxwL8yMIXc9cPvhs3h15s2dg8sbZmFbo8Qdd/TE+cf/iAUVm3fDomXLv8qZP28Ols6ZhqMthfCxY8aQrx2kYuPEayPaHtShffv22L5je6aIssP/nyxZsgS8I5jJYED/ogZMqymzyTkuimSl3fPchKPEwepaiBNUxAzdaDEghzQv8rqUQV7X8sgm8yJOtZ5QiLNDKPjnpUxvVkNrisLb6Lt4G3kdD8PPcWKIonUW7fcE/9ZUnILUNOW9EQ4plco6Xp5emDZ9GsOfR29vb+7du3d8Nqv//xGmphfalhJIBoFsbtlN23dsE/HZ2mfNnEUyyKyHiBFp1Dr1SCJmTTJE2bMqmYYTbhMIxA1zyvKhiHtdpkC2asgm9SKOtW5fI3GZLUZE6t6TyFkf8VH9GO+i7uC9+gmMJJO2XCRBXqUBDbwFaF1IjFqeIpuuOfc/m9DxiBkfdRzatmmLOeR+kj9/fnuOLc1kXbp0CbVr14ZfXgkOtJGS62Hi98A0U8SKYP7+/sMFFg8Ybxw8fgY5c+b8UuvmzZto3tAXOxoxqEHOUWLLgRdGzHyZB3eCnsY7lwsXLsSbbZMxu0bCMvjgHL1OaaEmDtrb//BAgbwO+W0kMQQOdSwqhkWTQdEoVqYh5v6+JMlgH2FhYahRoxr35k0wQ4zdK5DB3E3BgJwVMublgaW53f2q2+UbcgpUyLxNQsNZdP8xFKpnDFYLfeAlSPg5O/OOkmqeHgQ0HPslu+0thRq7V3igdBH6W0ou9w+hZhRuFqzT6Tne0uJhctvT+pQAJZAlCbjIxXgwt47ckxhppM4qIQE8e4ixwl5Rbew6cCyBGnGLp06ehD1r/8DpVkIy35Lx0+r8HN6tUBZi8pqTTcKgSLbEMRmJsUzx9dGaEC3HO1oejTs6upcOBOaWKFF8dFDQQ+IrZNvvh7wngM9wa49lzJifueXL/3ylUmkL2UMelUEJUAKUACVACaQhgSkkyMTEeXPnYuDgwYxY/N+aswwODkbpkuXQrfifcJf72BVzuPYt/nr5I14Hv4BcnrW+3/MGgXk8vDCgbACcJe525ZYSYfw32Rvvt3GXgwN4p9sPeotuCJFzMCWyaBtKgBKgBCgBSoASiEegdSFByU1bXa/b1bN0iMbfHOgjh7TDDvtMxsRT2/4F5sf7wZ4ZYW4b8dghdY66ex9nG7Zms99olvjEpf3RJCoxosUxdm5roXBEL9sdChMVmMqD1wP1aDHwI/tEVNahOBWOfsyeKTJCWF6eN5UjTJvmnoFTzAMk00UtJN3TpoM0kNpeU8UcWs6XcWo0137n+tj3KPMxgDvZTk6yYdo27xx7aFEGC8oEGHD1zgMUKpT01DEfICoXsW+qQ1KnBDQUQPxNkqDYsjPjtoXYcuVbr8eDpy+RJw+fH8b2pUfHtigZchyjKsVPhpCYFN5+7N9vBvPvmLArIi88PPNyly9eeqHW6Yok1pYeczgC/D/hrxKBbIJM7CbxKziMKeHu9/X8ZqS29z4ehjr3NZw5dyIj1UiTvi9cuIDObXoTp/8Au8u/FbIboYrzuH7rSqbwS7AngIiICPTs1p07f/GCUa3RNCKyL9hTPpVFCVAClIAjE0j+U7Ujj4bqZm8CyvyewqXli0s67prvIXdyYqDRWjBtRQx2nzJDKHZGhQoVSAR9KTH2EeDixUvECUmDMf3E6NmKZvFJ7ck4fE6LvmNDMbiUBGMqSYhjUNL/rtOuGSBoOII4Ks742j3/EiYgTkWTaykxtlLiH6IXkpe0fSofXCLZa3kDrocPH6Jn53YowYRgcW3BlwyJXwVn8AbvaFpvpxEftUp0KbMAtz5vx4jpXdCjR48M1izl3X/+/Bl89udHjx7hzZu3eP/+PaTk/0smk6FgoQIoXLgwfho1GnLWE80LTICr1CPlncVqaWL1uPZuK3cxeDMEHHffyBlbk8OvY1Wx1yb/I17srHAeYDQbJa1atcavv45jypcvH0/+9u3bMWjgYHOMKjrxH228lrSAEkg1geWlSpYa9CDoQbyLrk6nw7p167j5v89HKHGOM+i0z0mm6N6kxyup7tW6gMlOZNLFXZ5f2KTwz4yXi30iL7IkmuPNkF04+WIxtmzZgujoaAQ9CCJ/Y8CPkTi9w9vHG/ny5SUOgDVQrFgxZGZDJlfn7GQSZSuuvN2A2x/3Y3YdAYaUS96EpvXTY7/Sg8RZdsh5C+b9sRR9+vb9Mrm2ZvVqLJg0CpfaiSATxfs5fu38CclyX35LDB48eIBSpf7/N/Ls2TM0rlkJj7rZ9j0tiDgb9iLZbStWlGLRRHdkd7PfnP5XZf9DG89IVrRfFmhx7oYONWtUQi4PTxj0OqhionD+4nUShIY8l/WXoEKJf6Lj7j2t4br/HKrVGTg/gum6jahqubkITrw+kV/u6uw4DuQ26p5h1UwmDtOWRmHVFhVWiLxRl7zT0IUSsCeB5+TZuh/7GiWriLB8Rg7koNdTm/Hy767Vu75X335knEx8ihbY3JBWpAQogSxHgDjX/h971wHWRNa130lCQghVARFEFAULKnZx7b2uir271l1XXcvadW1r772sva0de++KBawgdgRBiiAdEtLnv8P/yYJSEggk4MzzzJPJLeee885kcu+5p6yvYc0ddq2HmUgTfVheAWCMJZz3ydOikedkPLJ61UpMmTIVn0aao4Sx/uddUUQ/W25HIpjo6NWqVUsX/86dO9ixeT3uXLuE1T+B6I6yDvow5rpYevS9/HaKHDlHUkynzF7oAIFNHh71Rz944JP94k4Hg2RHwt/fHx3bt0DLehz6yMWYFLkCVUnbsOzas+UsAiwCLAIsAiwCekCgpZlIdL5+/fqCo8ePUyVKlNADC4YzJLM+buTRFGZxdVHfvo/OGbsWuhofkrzRoEEDVK9ZlQS+LYuIiEhwSSaV8RPHaZW9SBvmmAAiERER+PjxY9reJ7PvaWVlpQ2JHNuGhISkOSiPrnkCAp4ox7aFXckESH0RdZG+G7IDMpVYLlOlbiY8TCMnCaXMHiwCLAIsAiwCLAIsAlog4CKEyO+yZaiQT/3/PqsWfXNtuk4ynT5iek1tPOoR96vDU66d9NxAfm2aypTyResHlwxyg/+aRxulvLmSZ/oro44yjCNx4gNlD3Us78AyW4Ng6MDZZMxbmKjwNapqMDZy61KjcIHiKR9VnqaZwUkhI9n6/Ua1UF5PPVu0xSD5ywqOFmJnlarNXMrIfYhONhmU7y/C5OIQPO5tBHvT/JP0DlfgUCCQoBIgWgpEygWYv3h52vpNThLIfHz3EmfOX8BINx4WNDCYRzUrqPNVdjJQjmN0Q3hduKo1nX/++Qe+W6diI7FLy+2oR+yeW/YYhEVLV8DY2BhMkP6UlBS4uLikd2WycxK7WpqUjyGFW9Ir2AtDQsCR6FL+oShO2zJmblQTpxGUk2VtQ+IvjZfAuAd4Jt2JN+8DdMobEyju8+fPYJzvXV1d0xOM6HQQQozRk0VFRWHunPkoV74srK2t4e8XgAC/V/B59BDdXBegglVDXQ+Lo+8mYvCYnzFr9kyd0y5qBJ8+fYpOHTvRKclJb1IkkiaEfzbDbVG7iSy/LAIsAlohoBejDq04ZBsXNgJO5iLqiG1JrtvexbamP9UyLCeUwgajMMeTkGxyf2+Kx7ajSZhKHFxGVxeQDUDtfqJMFKVnr96hTJkyYKKI7N22Aae3r8apDhwY50LrH38ZLnLrw5FkJzzp5YWfy3GwoD4Fa2Hui77CwkmuVKPnOSluEROwthUmoFbpbulDP47wglXtSPx7ZH96WXG+ePLkCerWrYtfav6Tlr1Wl7LGSD7iVtAm+n28D/gcI3GqSnKc0N9JTm8NxmG02MymcE8LM4tKMoXMqKxjWQwZMgS9eveiMioCcqN17Ngxunfv3syPwIGcEbm1Z+tZBPKLAJfLDa5SpaqTv78f2S/R7P0rI4rEq1evYv++A/S582dJgAIjKOTyhFRp6n3Cz1JyavK7YTy8hgko/mgVRbla8G3QoEx/1CzdheJQutsHYZxrl3g3wZjR47Bh0zqDiJKW33uWW3/mXdmlXW8MczuY3jRR+hknXk1DsuwT9rTjoKNz1obn6R0K8YIx8j/0Ro7ZxMWyKjGW79CtF3at/htPe+X+HGz2V2DzB1Mc9jqb9v/AsN2sQS1MKR2I1k7aKbgZh9uJ3hIkkAAfm+fboLGWURYLEbJiNxSTyXHupnjpgbMpcqmMHkcE3JeVkCQWyuQmtY3n3dhtb1jWYlkxayBlYZ+VaDPoMzolW2EG1+6HeAcaCPQ/NBsnFfGYQYfh7I5SaFCDXVtr+jD8tSFOse5A4sXkFJoJ/sMeLAIsAj8WAg1FRrj2sJ+5ScVCDFLQ86IC59+n4ODBg2jSpEla8KGLFy5gxZK/UdtKid0tOQaRufbro6BS06h+UILQJBW2bt2Kli1bwtnZ+Wt12md8fDyqVnaFE1+CG558cL5Z44pJ4BHrLQkgcRMnE3Xbqkyd2S+6RmDLTw09fr13/4FmigYdjn758mUMHtgT/y4zQysPYTrlDQcSVVNXx0kVSnqISgUvUkGnV7IXLAIsAiwCLAIsAoWHQF07G5sbtnalTY+fOK7V/lHhsajfkcb/MRFPzsagRVnGnrbgDokiAbteDMLZiyfRuHFjnQ80+c+pOH7gPFo5/Al7s8zOFUmyaHwRByEmNQiBKbcQKw5D586dMWzEELRq1UprXh4/foyObTzxa41jWvct7A7hSS/hHbKdDk58RmZj6gdKWjmc8PCmsPlgx2MRYBFgEWARYBHIJwJ2PHDCAY7KhDKTc8FT88CjuZQRbQQjmlyThB18GIFPrshJGZFv5DvFp4wgYL6RTz6HDwHHiCInc03xuVzSgmmZ1p/0/v9rUksca5sadYaQKrgt0suyI5in/pMWjn1DUXzTfMJTON1TdzZSufzSnFtjyZzCGVCLUT5fvgGf4b8qre62NRhnSPG216h44a3q2RH73A0xtJA1r03Jvhh9e4eSPmVW0WCMNN2S3in3lPuF18488/w9rzLqst+yz1ewIcpffcIiwGDwykm+ZHUi2koqqwV9T3C4jiQ6Zj4PWq0C50AzTHIMxIx62tkC5XPoH6J7RIoaFXYlIjIyEnZ2dlrJzCTUqV7FFT49jeCYQ6D8x1FKTAiww+OAdxrRX7pkCb148SJ5coq4FOmQqFEntlFBIdDdmCNcp4Laoa69J+o7DKDMBNYFNZZO6MZKQnEkcCyivkSkJd7ShijjPLt//34c3HcED33vo4J1HZQzaYySQifYiMoTnwTG5PW/41nkGdyL2o4r1y6gXr16/1Xo6Gra1Bk4d/A+uldcpiOK2ZPZ/XIQ9hzegtatW2ff6AetOXv2LAYNGkTm+fCJTUxsR2BI+kGhYMVmEWARKMYIFLphRzHGsiiJxoT/rUuSm9a2NKW6JEvo2tVd+LKx/S3MPVubwNLcIPQHRQnPPPH6/LUUfyxJQGCwElUsacwhDiwNSuddn/RZrEbfazRxGOCiuqUCf9bkoCxZrGnqKJaqpHPMlJcnIXXQSa1Wo8tpKe4QlWybCuNRu7RntlQTpJHY6Nsd5qYWmPXXLEyYMB58vuE4T2XLeB4rfHx80L/bSPR3/SePFHLuxmyqnwmajfLVbHDh0tm0iFnv3r3D27dv0wxOmWeLyZLMZD1mMr4wBp3lypUDcVTMmbAWtcz979SpM33vrrc8WZxcgXQlTwJ7sAjoHIE9Zmbmg+/cuZ1lZuX8jMZEmWMiywUFBSE2NjYtshbzXDNn+fLl06J4Mb8bxvC1b+8BaFVmItxs2+RnyGz7Po08BRuPL9izj/GZL74Hk71p8d/LcfnaeTQs2w+tyv2RrbDxqRE4/WY2EqUfcbADF23KGZ4CWkb+n7UJusG03/VSjqtRxgiKl2OhhxE6O2ULQa4VEmJ8v/aZFBv9pTC3IEFA+goxZZglef+zy4hcwdNBg/sE+61Hk1K9roopHhfRChV9WqVE7d4dRfX2LrLlazrP0wErRZbEvadSdPs1CoupMujB111mjCILCMu4XhB4qpRggOIDls4sgaE9Mm806IWhIjDo0UspGPHXl0Cir6hE2FUXAZZZFlkEWATyhwDXzAgPxtQ0dp/bUKgTRY6SOKL2uyjF1AYi1LMu3q8RRqdX85AUY6fPx5SpUzPdCSa686/Df8HLmydxhqx5REaZ5/GJMhrdziSLX8aogpMV6Ec66zaMdSZufrwvJibGPh07dqw3b94CisnYFhMTkxaNntEHVK5c+TvnaE0RCgwMTI9oP3H8GCxasiItG9zX/nt278S0KX/g/GYr1K3GxMPL+lCSZ+fJKxnW7k8UX7wjoRQKKE1EnEcyGR2SLFYHk15fyKkkp4yc8v99MtcScorJmUzOUHKS3AbswSLAIsAiwCLAIqARAguNOIIZ5S1rUx1dZ1P3Iraj8/DamDPnL406/4iNunTyhOJdBdS171kg4ocmPseVyIUIDglMy56jy0GY/cQ6tepjQNXNsBUxW3zaHW9ibuHRlwPgiRRYtnIxevTooRGBDRs2Yu+Kq+hQfoZG7Q2lEbMv6xt2iPb+tI+4D1F+MtAjCG+PDYU/lg8WARYBFgEWARaBrBAw4/D3LnfwHOSfGqE6Hv+KOmn6gcvj5N32Lasx9FEWqAzAwNSWtHCkD8WxKq8PFrQak1ZIIFnrRLd+cJ6yqu2uVd/CaHy+fE01b2pZjrC9Y2EMl+sY0qthoBY8UsZfdTCIh7XT6EhlGR8Rb4lJmVx5L4wGZ+UJmCNPVIdU+9vgnFjfSaNR4/UK+pzFe8qMY1kYcORrjHdKfwyWtibvkkcUxzIfhjv/40IVcgf8kz3g35+PUiYGd3vyhZWhdf7nBVmHvuBh7sKlGDlqlMY22Iwc7Vo0QSPFc0ytk7UNGrN/1fK0Cn3Gz8Wff/6pkegqEi2zbZvW9CMfn5BkSarh/zFpJJXBN2I21LryKN5hEhRL0LXSXGLT2ZoEtTWIvw6NwGP2CLc898Sj5/c12o+SSCRYtXI1Vq9aCyeL2qhvMwilTF00Gitjo5sftyBR5Icnz311br/PJFxp3bw9RrkfAZ9rknFYnV17fZiKifMGY8QIRi3DHlkhULemB5wVnjAT2MDr1Ww6VZGYLFNL+pC2l7Jqz5axCLAIsAgUNQQyW9UUNe5ZfrNDwJr4vQ2yNKd6kswvte1tePI2jYRGrT2EwtpVBahYlqfVpD+7QdhyzRFgHCSmrU7C09dKVKrkivETJoFk6MSypUshv7QCczzYrEYZ0bz1SYHhV9RIlPNJxto/0xYnGes1vX4S6YWL71fg8+fPKFWKCeRUfA7GQc/FuQp+shiPcpa1dSrY889ncTdyK3bs2oaePQtm015bhhl59+8/QI8Z8zsUUkWIXCX3IDSitKXDtmcRyIDAMjNTs8ktWrSkduzcTtnY2GSo0t8lk4Xcs0tPRAfJ0N5pJkz5JXXGDKM42fmyP46c3IumTZvqjK6hEPpt1O/Ytn0Lpvx0jTilah+5NlEahZtB6/A27j56uKjxDwk8YsRlldJf7284idbY+DQQGUMMfXx9sWnDehw5dhylSvDxSzcuphLHWxGrxP8KV6F8xieq0gziHxJzq9uPpHjgJyXzfCM0IYFbBncxTTOm/5GdcLcdTsKcFQk4YuSMGryCUawWyo1mBylWCESrFeit/IDG7fjYOL8kG7Agl7vr90YGj/4RSSSzN7PrnJBLc7aaRYBFoOgi0N1GSO1/PsjcpIRx/uffF4LlGHVLjdnzF2LCxEloWLs6ppQJRsfyOvHdNWiUXQ7IybzQP8uNckav0qlNS1RMeooVP2VtABAtUWPybYnkbJBCKacxnXRhorqpDFrozMwtszA3G8/lGfE7d+5Aoku3oxhnVnt7+7RAV0zEbSYI1uvXr+lnTx8jIOAl/fYdMcYyNREnJqXcI0Yq+wm5Q+TUhVd2FWMB9fDoKlvzn1tkvT5NIQETfx4bhyRFWTx5+iKzJDl8Gz/uN5jIvLBkwn+BO5jAMlfuy5BK3F/rV+Oia0sRjL5xqM6BZL6qGF1DdKwKAYEKXPKWKM6RZyjok0JgLOCcTUpRrybEH+ZrALYziwCLAIsAi0BRR8CaZCK7RFNU7eZOo1CvTG/qW2PEI+/HYcHqP9G3b9+iLmuu/MtkMtjalEKnjj+jT7+eaNu2baZAGdkRqF61NmoZjYSTZa3smuSp3O/zecRY3MJ937t56p9Tp1EjR8Pn4gd0qbAgp2Ya1zFzjldfruM+yYhSwbUc5v09G23aZB2wNDExEaVsS2Nyg+tF0iaCyXB7/+UU3BGVhZc8HkukkSnxalVUMtSMN/pRchalObrG95htyCLAIsAiwCJQJBGwM+cYBya6r0xTfkQpkuD2ehHdhjcUE0yWFnmbVIk6Ba0lLmp+r0McbrnmBn+DVJ+fQ3awvbq7OJjkS8i/nlWXAofsP4LnC+YqrK60ytrjTJeDaUBLGZiIhF7X1MoHjgYBlHPbUPXMeHtOdwGTN0f/R/2kt8q5ZXrxBpWor39mMnDArAks/f5SLzc9yqnJ+ylDjWFenpMdwCL1jP9lxc5aR6wp54zsnEOtMdLmFRY3zFq/rykttp32CNwktsxrX/LgHwvMW7gkzek2t/fsor8X4PWx1djRIvvXjHe4AgNuUDh1/jIaNmyoEWNfvnxBjRo1aPJ5gOxpDNaoE9tIUwTMyK/rdyNQ02vxRNxpxqXMHDjEfj01Er83vKgpDYNqxwRWe6Hehaf+vlnyxeipdu7chaWLlsOEtkNju19R2qxylm21LUwl88LdL4dg3787SJKlTtp2z7E98zuoWN4VQ9z2wMJYt34Bt0K3oEnfMli8ZFGOPDCVjF/C6dOnsWbVejSo3wB7D+zKtU9xaNCv90DEPLWCh8OATOKIiQ7retA6+lXMLVqplm0jlb9nasB+YRFgEWARKEIIFHmFRhHCuiBZ5ZoIsY6mqWHzx1oZ/drLnGdumv3kvCAZ+ZFpP34pxfoDifB+xkN4lASVXF2Ig2JvdP75Z9SqVSvLTbwOLRphlJk/iaRrEDokvd0+uVKNkVelOPmBRlXrFmjh/AdxLNON4ihJFo39b4YjJi4qLeuq3oTU4cCM4qRC+UpoaD4Wzlb5V2jRtBohZEF1IXgB+g/qg5Wrl0MgyD7LhQ5FyTOp27dv489Jk+lXr19CKVd+VKgU/Qkx1mgvz4gW+47Mn+I+czPzvhwuhzP5z8mYPGUyZejP+f3799GrR1+UEdRHK8cJ4Oog4ivze9/+oi+OnT5QrBxtp02dgYfHo9HUcaTOHuZXX67hcuBKmPLk2NaGi3YGmOVWZ8JqSKjCfjke+r2Go+P3EWajo6Nx8eJFnDxxDFeu3YCJsRFcHJUY1EWY5vBpKuJqOArbTFcI3H6UiolLY0nGeQqHV9rC2fHHmG+OmP4FT28ocZhXAdY6eG/q6n6wdFgEMiKgIPP50coQxDhJcWG3HUxF7Po9Iz4Zr2PiVXBqE5oqSaWZyEJvMtax1ywCLAJFHgGRyAgvVzQ1cRzqJsj3i3DFMzX2hZriys27YBwrvx4pKSkoY2cLv37FP7J7r+s8jFi0HV27dv0q/nefTDaxRvVq4X5PPsqa5Qz7uSA5Rl+TSGRq+mayHEMJsS/fEdRzgVDIf0AcdRosXrIYY8aMpbjc/K07wsLCcO7cORw5coi+desOZSqi3pJgD0eVStwkot4jJ5PNNbvDlejk54ol6r4+hx04ddw0060x8/bRi8mP4c3HLPXHGQcLCgpCy6a18PGKdcZig76+8TAV87bEJz99KZOKU+nphNl95GSy47IHiwCLAIsAi0DxRaClgGNy0oRvadat8gLKwdwtV0n3vPoF/+xbgw4dOuTatig3OHLkCJZP342OZeelicEYHL6P80awxBuB0b6oXasu+g7oie7du6cFCvkqKzM/mfzbQvSqsPZrUb4/A+Me4LlsF16/0zzYhyaDhoaGoka1WuhRcQXszapq0iVPbZi9Uga7Z7FH8SnuJTwaNELPPt3QpEkTnDx5EssWr8CE+pd0sq+SJwbz2cnL73f8qYxDN75VJkqPlWIsS41KuadMJghQq+VQMw8FG5gsE0rsFxYBFgEWARaBwkKAZLHdQ7LYDv7Nukkm+9MdMffVE8O9sN/0Cac0N/+ZGwtLnuzGaSlxUSmbTaKM6o7OWZmWHYFCLFc8201TfstVXcKeG5wX3kkLJ9rieBOKV9GiEBHJeiharkK02zFa7V+exKzO9Phm3aGAS/nuQfRLUTWqpAHsbb9XStFKHESnuK/WPzDf4O76crGyPXcMZ4DxeIP/LS4Sj6HPmz+ijUf6cPL7jKlCvWHk5YnnffmwZ23Cv3kq9PP16Ds5pj2kMWn6X5g8dVq2ev3ZM6Yh9soWrGmc8yv5XoQSrY8n4+6dO2hM1rSaHIcOHcLwYUNVqVJZK9L+tiZ92DbfIeAsAucvJei+fYmt+gRjW+Oy3Mz7OgMlkeA6jUBNu87fdS4KBZHJb7Dz2VA8fPgQzP7ZmdNn4XX8ND6GBMHVujHq2Q6ArahCgYpyP3wvJCVewPvB7TQedDUYo39yq1wdo2t55SkZS1Z8KFRSrH3UAfEJcZmC4j179gzHj3nh2OETiP4SjUolm6GCWVOSHKtOut5ptW8bREaFw9zcPCvSxaZs9K9j8PKqGM0cf8tRJkZv9/zzGfrKh7WgaPVLOS1n3lXROXZiK1kEWARYBAwIAYNbDBkQNkWBFYFISL3p00Fkt32+jTGHw97Ogrpp0bFKHDibQjICSPHmIx+fv0hgY2ONFi2ao2WrNmjcuDEqVqyo9fAlzEXw72sEa6HBr/21li23Dlc+kswiV2lIVEK0rzgVlawLLqvi/rfD4HXhAGrX1m3G19xkLKj6X0n05483hajv0FsnQyRII3Hs3SR4NK6NRUsXwM0td4MHnQysQyKxsbHYsnkLvXHjRkgkEuYMUqlVf5IhSN5F9vjBEGBSv+4iDrUd5Ao57+fOXTBt+lSqTp06RQ4GJtr6nt17MGf2PNS3G0CiPw3UiQwyYoSx+Zkn4hJiwOcXj2xO1avUQn3huAIz2hHL40iW2w14GXMDHcoB+zsIwf8Bs9z2uUZh8IJ/0KNHD62exaSkJDx48AB3bt/ClcuX4f8iAKamQjjaqlC/OoXe7UzRqqEwW8WzVoOxjbNE4DoxbO8zKQpe60uhaV1hlm2KeiHjXPv+pjrNuVZA/Xhz66J+/35k/qsmBqBiJR6uHrCDUAcZHIsjljKSTtG+eYgkLlHdnMj3qDjKyMrEIvCjIUBcav8sZ8GZ79PPXCQgAUHyc6x5psDe8BK4fuc+HBwcsiR1hxgmjBvQBT7di+8c4U2cCl2u8REambsfrEKhgHsVF8ytHIOuFTRbE14NUWDghRSJlMYCuRLLsgS68AqNzcxEkfXq1rM4d/4CJRTqfn4bERGB6m4VEHvPPl0qSaoaH8OVCI8mZ5QKYVFKElyIQgkLLjzcBXBxMiIGAnl7nscsTEKFOtMx6U9GlZX9MXvWdPAStmPeGMvsGxl4zZ3HqRg550tKZKwqODmFHkXYZYPmGfg9Y9ljEWARYBHQEIFGAo7wsrVJOZMebkspc4Gtht3+a+YVOA39R7fDrNkz/yssZldqtRolLG0w1G0fCTbMbGVkfahpJUISnuFD0l28jbsNSysLBIe+x2D3LShrUTPrTlqUMgGK95BsIl9io3S2R8DMMVs2awNlRCm0dpqoBTds06wQSJZ9wc5HvRBkXhncHBw/QlQyLJV+Tj0tT1BToNdLgeWEHut0mxWobBmLAIsAiwCLgK4RIFlsBR8S3VeZZEVYQavQ4O0KlUJhix1md7hZtSlKZf3FTRShVd05go4bDV4W6emhSptySZwmZ/81KGXo25Ub1W8ObqKtvFoaBIZf6p9UX1pqxWnzU5aPcKE9nuFEx1ip4yc6VOieN8WijjltlfRe3a9Ua3pqKRKJ3oCOfsG76U9ia9Uq0xM5eysaAM+/pLRSvXctSxt33Z0vXmm1Ctx/W+IX67dY3vjHCKhuALdPKxZkShrj76nxQVAR56/ezNK5bkDv7qj++TIm1MrsvJnVQEfeKbA80AbPXr7Jca3u4+OD/j+3RkBfHhgH3S6nUySpSnQkNG9nRZctS0eggik4i1XAz2OMbXhjjG2NzKnsX3U3FcmYQlMYUGd/OgH2Im8IMLaq/779HWMmDcWMmUwsVt0czP7vwJ4jMaTKXp0QvBK0CgKnGPj7+8NSWArlRU1QqUQz2Iicc6T//PM5GLsF4pjX4RzbFfXKBnV/gnVyE9S289RalJCEpzj+ajqtUstey9XyxoRAvNZE2A4sAiwCLAKFiIBBLM4KUd7iMpSIRNF/0KudaaVdf9toZglVXCQvIDku3ZVg35lkPHrJZKGVgQCMRg090LJ1WzRs2BA1a9aEkZFuF6vHjh3Drjm/4mTbAhLKwMj6Riox+roC7xLUZJL1Mxo7jYSJUeFEpzvy4XfsPLQuLXqxgcGSJ3bWrFmLA+uuoqPTX3nqn1MnZuP+TcxthEueI0r6GvHiz0hMiUXzZi1w9twZ4hRlmlN3g6pjNvTPnz+PrVu20Tdv3YDASCBPFiefJEyOIKfYoJhlmckrAkP5PP4CHp/nUNquNEb/PhoDBgyg7Ozs8kpPL/2uXr2Ktm3bwsK0JIyNTNIcRUvyKqGabTudZfXOKFhooh/uJa5BYPCbjMVF+rpi+cpoVfKvAo+u9hWkd7F3cYM43UoUMRhURYUVTYUw+gGcbplMVlvj3XDN2+crFDr7ZCLMMY64d4kj7s2bN/H+QxCsLExQwUGBTs0EGNXLDDYl8rUHoTNeizIh7ydSdBv3GcfXlkLz+rp3RtAHNmt2JWLLlmRcMHJFCQOI7qsPDNgxiz4CShLFcZAyCDYNaexfY8MGHcjilirJJqVDy1BxdKyqA6m+m0UTtohFgEWgaCBgZ8LDq8OdTK3aEIfE/BwBMSq0O6vEnQe+GgULGz1yGBxeH8HkOrkbMuSHL331re9FY8eJy6hfv77GLDSoUQXzK4ahuaPm9yJBpkbpbYkwN8KWJAV+13gwHTU0Ewk/8vjGZWNiYigOp+DsBDu0bYKh7d+hd/vC0YMx2Wxnb7PB3ft+OSJlb2eFhwfMUda+eKyN5CSQxrr9iSqS6VbO5VBHksXq2QSA8BxB0G0lA2QtcrqSx6mCuYiqTuL1VORQHCUpk5NTqlSp4xOT6Xvk+gE5WYdgAgJ7sAiwCLAIZEDATMARPBMaWTn3rb6GIg62Garydvk+1hvXwpfjvo83XF1d80YkQy/GqVXbOYNUKsWuXbtw/cotXL9xDWJJCtkjFpA9JmMoiENjtSo10OCnehg2/Be4u7tnGE2zy48fP8K9Wh38VvM4CeSoH/3cvjfDsP/YVjRtqpsAyExmJlNjK4yp40X05MaaAcG2yhWBux//QZUvl7DIuFSubb82CE1zuo2SnJLHI4PTLWu4+BUg9pNFgEWARYBFQGcIkCy2u5fZew4ZbZM5i+23AzwQB6HV+w30ApMDVBN+p2+ri9T3v8RD1Tdsw9TGg68avGJGsqGS2n3ROI7L2JEGg7FKJsMpy/J0yUedKY5Ic31kQQkQ3+eacnpdJW/+uBIFNYRGdG/6pKL/+GjlS151vT9XYuLUWTHpFZ1YYyVlzNH/PfoK4OG4x/gt9Cx90SKYInq7r8UG+dlOXFkp+Wkkxf/pz+w99zTh/NxwuCdcxKWfOWSNxZr4awKZvtvMeqjE5XhrXL/7AKVKZV7Dde3QBk1SH2Cse+6m/v5flOh2lYsPoRGZMmlmlG/sbyNR/vVBjKn53/o7KEEFt/5BnDwAAEAASURBVH1JIL+QuWpgQcb2P/B1RcapluDReSxxqh2di1Pttzi5JQeii/vWQrNF/Hb84vj9C5kXbnsyANOnT8eSJUt0IuKUydPg6xWNpo6/6oSetkQ+JjyBT8omvPvwStuuUCrJe4MkKzm47xD8nr8gtrzvSHBfU0jlEqjJf7KdrQM6dGyHNu1aaZ0QRWtmcuiwb99+TBw7BQOr/oO8BFb8ljTxT6BPv5kHhVq+E1AbzmT1W0bZ7ywCLAI/NALsDLyI3X6RkPOvhzu/y+V/SovyGhm/iImsM3bvP5NiyfYE+AbwIFNSaNu6FTp06oyWLVvCyclJZ+NoQkilUqGcvS3OtFGiSsn8rak1GU8fbW6GKjDupgqhyWo0IBlXG5QZAGEhOdVmlJeJgPJCvRvPXhSvhEfM5HpY/7Ek2vVeYoRfMAqkD3E+uBj6N+7eu4Vq1aplhLVIXzMGDHt376W3bd+GlJQUyFJln+VK+UEiFBMeXVGkhSu+zDO7LXMtLSxrSWVSLpOddtSvIynm/a2tgYyhQkQT55p+fQbA3/sTPCsuAYcqGP25b/gR8CoG4fyl04YKRZ742rNnD9bPPYxO5eblqX9+OwXF++JW8EbEpn5C94oqbGhhAhG/YN7N+eU1v/1bnlFj5oYD6Ny5c35JadVfLBbD25sY2V29gosXziMoOATODjz072yEiYMsiHK5eOKtFUhaNPa6KsbYhTF4eMihyBron70pxojJMTjDd4ELa7ynxd1nmxoyAkkksntnxTv0HWyC2WOtDJlVvfCmVtMo1zY05VOkqidh4LJemGAHZRFgEcgzAqZC43cpqVKXRY2EmFTnv43/vBD85boKDq0GYdW6jRp3Z3RxdtZW8O5Gwcm8eOnimGjpjb0UGD9/FUaM0nwTefzY0bD324vxtbW/H1ESNeodTJLEpdK/kqjfBzS+EXlvuNDauuTMN2/eUiVLZp/1Le/k/+s5bswo8FJOYs008/8KC/iK+Y8zbxCG6C8JMDHJOnvGxYsXsXrRL7j6T+E4/hawyFmSj09UYc3+RMXqPYlKvhFuxSfR20nD8+RkHF7zcjAPd10eD63NRJyuKWJ1VffKfEm7n0zMmtQx5jYkRkjmppqvJd99lGPOxnjJ6RsS4ntLTyDx/Zhnn9Ul5uXOsH1YBFgEijoCU3gcwbLmTiPg4TiwQGwNguMf4eCLPzBsyAgM+mUA6tSpAzMzs3TcGF3h06dP8fz5c1y9dBN37t4me5BmJIClG2x4VchnFRLo1wpmAmsSKDEBcURvGyF+gSdRxzFu/Bj8vXB+Oi3mgpkrDiVjXbt4Gy0cJsDZKvfAJYxOPyzJHz5f9iJR9Ql/L5qHYcOGaRQ4Kzw8HJVd3TCs+j6dGKdlEiaXLy+iLkFZ/jnOXTyVS8vcq4cMGoa7l5+ipwvJ9MET5d6BbaE1Asf9fsN4RTx6C/Lm/PFJJcdCaaTkvDxBLgc9hczddxEmiI0xe/wACFQ3MzO9V7VqFdOpU6dT9erVS3vX3bhxA7/+OooWCgU+ycmShj8ADqyILAIsAgWDQCmSxTYouyy2WQ05POSg8nzCB+qkWSC3KNt47EtdjS28zWrhmFccipu7s1RWWBRGGS2JAXG0pTsFP6JMyjgUxpAajfF80ixViN9pympHM80VMhpR1r5R0tzHqjbhEdzTG/UbxH/rkSRsXJmsuCOoonev1r7JH1DJsrZig2MvvfPy9Y7GKcRwCJhPH7Z4QtlxHL8WG9ynUq1E89Ryam6XbRyea94DCqgfbUSJR3/jYS8erFkbGIO7z5owtNFPgY3vTXD1ljdcXFzSu4wfMxofrv+LY205ueoN/BhH2ytcfAz/DIFAkE7j60X3jm3Qi3pA7Au//x+KSVWj06kUcXCi6nqyHMye+o+kvzYTgjOHBj1mvHEp3m/GNjlmqv2KZ1affSXhMCo7HLVLd82qmi3LJwJRKe9x9N1E7Ni9DT179sgXNcZR1cLcKi2YXWEl/frKcED0ZQTxzuC5/+Ncf9dMn+TkZMya+RcJ7rcb7rYdUcumJ6yEuc/TGB3k48ij8Inaj7UbVmPIkMFfWUj7ZOj27N4Hr54HolaJPmACITInTdQ/KfLYtJPRYcao3iAk1h8lS1qjdZuW+KmxB2rXro3q1atnssGOiIjAs2fPMGfWfAS8fIGx9U4SPatlpjF18YXJcHzq9Ww6JPG5TK6WNiE0H+uCLkuDRYBFgEVAFwgUyMaXLhhjaWRGgMvFCJsS3LV+J8qIbIupU2ZmifP+TaVSY/a6eOw7S0MsBYng4YmBg4agWbNmmSYCeR8hfz2ZCMjlHOxwup0a7jYF40SVPw7z1vv0Bzmm3KYRlUqjkeMg1LXvrfcNVblKgk1PPfH2/Ss4OOQ+Gc2b5PrpxUycy9g5oWvZFShpolsncWbyuvvlYPx7bA/atGmjHwH1MOqnT59w7tw5/HvwEO3j+xAmQpE6MSmBCTG0jJwH9cDSjzZkOSLwKpFQ1E6lVpnYlrJF71694dndk/Lw8DCI93dh3BAmsn7N6nXgQvdAVZvWOh/y8Nux2LRvcVrmXJ0T1zPB9m07g/uxukEouMKSXuBW0CZEpLxBWyc1drY1IcZSet8n0tkdanxShWmrdqBXr146o5kfQiEhIThy5Aj27tkN5rpOFQ7WTrdErarfK5vzM05x7DtlRSwe+ElxfZc9BPyisTT0eyNDq4GfsYNbDk2J8SR7sAgURwRCSNaRjvL3WLeoRKFl0CsqODLroMqdPqW8C1EOIjzn3yK4qAjO8skiULQR+NXUVLTl6dNnFGNQwDgKLpg1FXFxcbC0MEdJKys89gtAhzJqbGv5vUHAt6Lf+qTA8ggX3Hj49NuqXL+fPHkSO2cOw/G2RWPek6tA3zRIJc62JTYnoHP7tjhw+CgsLCzSWyiIN+CsWTMRE/EJcV+icfrKTcxpZIoZdfJnL8XcD88zKclSFeqSwd6lD6i7CyNTU5OEGTNmCGfOnF2gNy46OhpNGtXBxP5y/Nan8B1ZZ69PhNSkH1au3pAlej0926FX4+fo06HwecuSoUIqjIhW4tD5FPreM1nKowAp/TlGJSQOs5HEMTndW55sBwilctrM0Y4nqVKeT3m4C0SNaxtzG9QQEP1ewa3Fxy+JUaw/kMT8iBhrzKhCgoQdhkWARYBFQF8IOBhzhH6WQocSfaqtpswENoXKh5oEpWIcZflck7TsrwKuqUbGa1kxqVIrcCZoDiS8cDg7VyDOuo/hVrIDmpUZnWeazDjMevXp5xPwjtiJcX+Mwdx5c8DnZz+/ZQzWXCtWwehax2HMKzwd166XA3Dl9hm4ubllBU+uZYz+tQPRxYukzmhddhKIw3WufdgGeUeAefY3P+iI8yalUVkHmY/PyhMwVRImltL0wySofiecFcQcPu8Csz11gUB3oVB4/OrVq1SjRo2ypZeYmIjy5cvR8fEJY0ijLdk2ZCtYBFgEWASyQEDE4e9cbt9t6O82TbXS1USQ4CdurxfRXXm/U6NN5mdBuWgUPVbcwlhZH9rk9xcUJbI1WKZVH29BfmawqkfS+3Qdir6ZlUZ/wYUKtWlr/y5aPTsFwbfkwHvYHghQBJ5yyJ+CNJ/MjV0Uo/pwDNyDps75pJT/7mUSXtIf3OZRpfWQRCU77q395qqmCTdxm/I7Z9dE7+WfSdClbql1aOHg6xSnVPU88aN8fRKyk4PwZIA5qmZjG06cJrHKDzj2Tob+ffuiRt0G4JHogk8e3MXRY8fxSxUeZtbhwrSI2H3kCagi1OlUoBzjH3Cw79AxtGvXLo3zL1++oFH92hjplIBx7jm/ep5EKdHnphGCPkV+p1uYMG4MHPx2Y1ytnAOobvOXqmZ4p8rkNCaQ2GLbixB82rDaSgTOdleuwGa1iaNpDV7WQUy1IbhEGo07FnXR1nWGNt3YtnlAID41HKeDZqJ5u4bYufuf7551TUkuX74CpzY/QyunPzTtku9272O9EaA6gIDXz3OkxQTaGzV8NJ76vkDbstNR1sI9x/a5VaYqEnE2eC7UwiS4VXHDrTvXUd9uIBrY98+ta7b1SrUcclUqOSVEv6nWyPE3W2J5rGCSqR19OZVWqWXXlLSybR7JsN1YBFgEWAR0hoDeF6w6k6T4EqojFFB3bu4pbdKgRs6T4uILQc6SkQj0GDmXZLG6rUKzpk2xdNly1KhRI+dOeqodSiKIPL9+Cld/LtoLWiVxApv3QIodL7hQ0cZo6jQc1Ut1hJEBZRFjNh3/8e+Nc5e90KBBAz3d8fwPyzjcfY3iyFwzWVh/+3U0rl67gvENzqZF4c7/KP9R+JToh/Ohc/A+6C0sLXUffea/kYrOFWMY8eTJE5w+dZo+TJy4QkNDwFXyxHJ16gsV6FtEkmPkfFZ0JNIbp0w6tt7kbCvkCD24Al4pFZScLj93Ra/ePSlGoWRq+mMZiuZ0J2bNmI0z+++hW4UlOTXTuo55nk9/mAW1+RfsO7AHlStXhkgkSqfDRPdilLBF9ajiWh01BUNRscRPBiVCVEog7ofswNv4B6hagsK65kbwsM9ZYWpQAmTBzLrnCix+SuNj6CdYEccIQzsYA5GtWzZj4cJFKG1NEadbETo2/e9ZNzR+9cmPnGjUWw+PRN1qAqyeVrBZwfIjp5jJltY1Ar8nlkJ/vuHymR8Z2b4sAt8i8FQpQX/VB9w7bg8Xp6L9v/GtbPn5zsxnyrYKTQmLUv1M6NzKDy22L4sAi0CBImBhKjKJmj37L/606dNz1UHv3r0bl1eNx55WOa9HTryXY6+4BnEMuK8180xgloMLRuOo7uMZac1LQXdIJnO8OKkaJFI5wlLUEBBTuhaOBfdfMvKKOPV0oPxCsjItMrquxBttbm6+6f3795StbcEYLPr5+eHo4QPYtm0TFo4z04tzbUawFm5NxqGrprh77zFKlPgvW9nqVStwZN8i+Bxi58EZ8TKU6x0nktQTlsRGi1Npxjoi2lD4YvlgEWARYBHQEQLrjTiCsZ1cZqBaqXa5zul0NGaxIPM+9h4uhy5D/wF9SBCN5SBOb9/JtX79ehxYdQvtyk39rq4gCr6Ig3Ep6i98/BSoFXlm32D+vAXYuH4LOpb7S6NMv1oNwDbOEQEmG/M2H088MHOGA4efY1ttKuNItq2FqZHSI/I4cofpBUpgDelPPtijqCJAAly9GTlyhOvq1Ws1fl/369uHPnvubIBYnPqtgU0DkcjkKLFTsCdB5LkURTH7iZKUFLE/CeI0imAUUFRxYvlmEWARyDcCtqYcQXCy+6o8e7FsifFWTws/jYNmzzm2nKKZMOGLOgJdJLVowaDLFNeuZr5BLSgC8nvLVIKoE3SHV3dzVroWFANZ0L3fe6gylhvAs1hcP4vawiuS3fsM+eT7SvHNMnrFpungCKXHSwvedGHpwhM+i5HGi0MhFjoqvJxHFpwSOYtxcyqq92aVqjLdFWOFCw3GUfxbfh/Ir2KiYhBxug+gKBPrb6tz/a56shXG3nNwkdgS17TN+lE89UGFfueTcOXKlVyTpaxbsxpL5/+FC515qJKNs26uTLENdIpAJLFt73tVDUvnGli3ZUeand69e/fQq3NbPOhphFIm2QeJTJCpUXpbIry8vODp6ZnO18OHDzGmT0fc65ZelOOFXEVj9r1U2a4AmVisxGjS+GiOHQy7soQpOEvVwMAZQjv+SIEN14isVXR17JDGYjffBj3dN+mKJEtHQwTuhe/Gi7hT2LlnO7p06aJhr/9v9uHDB3jUbYrfapzQql9+Gm/16457vrfg6ur6HZng4GAM6DcEUcHJaFd2BkmiVfa7NmxB1gioiK7qSuAK2j/6ilqhlnYnrc5k3ZItZRFgEWARKFgEdDe7KFg+f0TqJU2ElP+qKSVL/dbH3GAXivq6MQHEoK7HhAQkpppg//4DuS4gdc0n4+zITMySk5ORkJAAgUCQ5pDIOCUaGxunOSzFxsbi7du3CHjhjw2rlsNVKMauFhxYCIrez+5DghJjr8txLxJwNHdDo7Ij4GRZS9ew6pTevtdDsfPfDWjdOm+Wk0xW0+PHT+DQgaMIeOkPW4uyKCksDwuqbFqkFiYyM7MZ/SbpEgYM6ot5i2ZlclTThTAqEkKqQd2fUCK5AZwtG8LSuHSBOjI/DDsI8+pROHGqKK+jdYF89jQG9R+Cz74W8HDIHPmHiaoUmfIa4Ukv6LBEPzpBFkVJFEngUjwSxdtIxQUnVU0rYqVqaSgNBJER4slJcm1DluEzhVwzGxEZT8bqgomwwHg8m5K3h5kRZWRO6JpyKA4pp8huOsWjQXMJXea/gkOu018yFNIu1USXQPyBKRUF4n0OWkEiDhFTW1WqUq1IIhvnSaQfM3YiOSXkTM3wKSbXyeQUkJPh4+vJfGd28p35lFF5YmRjR5QXpipaaSRXyyg+iaZtZmSF0qaV4GBRi7I3q4JSpi4EC6bL/x8SElVp67Oe+BwdATOzwovS/nX8ovD57NkztGneEaNqHAWXo3s9NhOMgAIH8dJw8klh38sRuHH7KurWratzeDZv2oIZ02fB3bYzSvIrgkNxIVWmIE4ehHjlR0QlBsFYKECPnt3Ru29PNGzYEEZG2suc5nTj4Iy2dnPAPHeGekiVyXgScRz3Px0kvxUa42ur8Wcd43xlStCXrJv95Vj8hGayTqNj1x5pyiNGgSSRSJCamgqxWJyWpSw+Ph7EQD5tvlS6dOlMWbUKg/eYmBiMGjGMBKm4jnH9jLB4Imuc/i3uYZ+V8OgXnuZo27u9YQU92Hk8CSuXJ+OqUSWYUNlvMnwrE/udRaC4IDBcGYxSrWlsXaj95mxxweBbOZiQu6WahEgSU9SMFcbLb+vZ7ywCLAL6RcBEKPCvWsWtmvf9+xSjM9PkGNK/N6qGncf4Wtm3n+CthLJaF+zYe0ATkultkpKS0LrpT3BRhWJny6wNVdIbsxd5RuB1rAqNjiRJUpWoQ4i8yTMh0tHM1CS8b9++pf/ZvjNdx6EpvWvXruHG9Su4deMigoJDERefAqcyZnAuYwwHWxKowU6BKs5GcK/ER2Xn//QUmtIvjHZvg+VYtVeMxBQj8HkqTB8hhFtFw+S1MPAoCmMw+ogmgyLET14qlknl6r+LAs8sjywCLAIsAjkg4C7gCO+XMXcTelZdQhnzDEtPlAPfBlv1LvYuroQsx8DB/bF85dJ0h9vLly9j0oj56O68ulB4fxzhBYfGCdhBsoPkdsjlcqxZsxaL/l6Cpg6/opadhta7uRFm6/OEALOnsPVhV9wyLYfy3OzXTHki/r9O+2Wx6pmScLJ/iKVSqBeTYrKnyB5FBAHKzMw0afPmLaKBAwdqvYa6ceMGY29DExuYdkReJaF1/cGDh1R2Ga8Z+5gGDerTERFhkSkpqUXTO66I3FiWTRYBQ0RAxBHsWGbfZdgYm2Zav28yyiNTK1D37XIVX1kem82uFEm7SJVaheap5VWc9iu4vGp9MopnUNfSg52UZVuV4dXdsd4g+EoMeI2bzTqqSzzuqNdNX1UEsWNod4FWPXLK17OcX1BLNwuhN6aWo1rw9Ws3VSHxjfqu6yRONaF9fkXSSf9JYSdwOS5Jucv8tsEq9LdJ/sYe/kG1cEwAMeHTnE1apQB1biic467jYmcOsUfN+qfwiNhvdLuowvU791CzpnbO/Fu2bMFf0ybjeHsuPEprzptObh5LJFsEvpDA8vMeq3EqUAFrG1twJHF41id327jn0Up0vUAy2w4YjLl/L0LJkiVRx80VK6t+RkN77e6vmuixdwTI1TO9JTLy8tufosBkwjBjG2rIB3GspbabUtx2u03Li+rzCibBwkZpDI4JHdGtWuHoaAwZcH3yxmRUvflpAyIVT7F1+yZ06tRJI3bsbBzQ23kTLIztNGqfn0aJ0igcDRqDz1/C0sl8/PgRA4ljbfiHeHRwms061qYjk/cLJqnNoRfjabky5aWcljMGzYytPXuwCLAIsAgUCgJ6XSQWioRFbxCOqYi63KmpSaPDK0t9H9K26MmjM44VCjXajIyBfyAPp06dRtOmTXVGOzdCjLPs1Alj8dT3AcZXo9G5PA/lLIqkji83UdPqzwbJ8cslKRRqI9S190SDMgNgWoQyhl0PXYsW/VywaLFm9kyMM+uhQ4cwf84iyFMoeNj+fxZExglM02PhnYb4/bex2LRlg6ZdNG63ffsOLJ29Cf0qbS4wB6y3MbcRyDuBp36+GvP1ozVcv34Ddq44gy7Omj1XPxo+eZE3OP4RfMVb8O7Dq7x0/yH6nDhxAtPGLkQ/1y0FJq/f5/MIM75cIL//p0+fok6dOhhd94hWyoP41Aj4RO3Dm5hbGDZ8KKbPmArGMVOTIywsDJVdq2JcnXOZHLs16auvNowx7vs4b3h/3I5Y6Se0c1JhR1sRzPhFc6rOyMNENc/ukClpXApR4OhHPh7FckmgimUYNmxYds11Xs44/vbq4Qkfn/s4tNwMbRsxsQ3Y4ysCN31S0XtSFG7vtUdVPRvyJ5OonjU7hWOGxB7d+YaXMfkrZuwni0BhIPBSmQpPZSC8j5Y2WIegwsAh4xgJSSrYNw+VpsroiqQ8PGMde80iwCKgNwRWWVlZTvTz86ccHR01YoLZdHRxqYgTnYRom03WbqWaRp2jSqzdcxQdOnTIlS4T4GXb1i3YuHYValqpMIcEtalho52hQa6DsA2yRaDf+RTJ9VDF4WQFhmfbKPuKjiYmwnO+vo+yNej+tisTDPH334bB9/4ZLB0vYOf33wJkQN8Tk9V48lIG3xcyODvyUN2FjyoVipfz8P6zyeoxf8cEJItpxvKNxMRjDxYBFgEWgaKDgBGHtwrgTezltoxyttJvVqmig5r2nDL7cVdDV2LQkAG4dvUGKnN6w822tfaE8tDjRsh69PijLiZNmpRlb19fX0yZNAOBr0PQwmECylvpPiBnlgOzhRohwAQu3U4y2l4QOaAyCXZbkMdeWYx6liRcRjT9i4nD7RIyFutwW5CA5492SZIpO9rb25tTu3btfFFigv+uWbOaHv/HONSpWz/7TZ7/jRIYGIgaNWrQJOBqe1J0JV+Ds51ZBFgEigoCNiSL7cekGitNctoL1kaYO8R4vn3gJnqpyXGqAb+VNl0Npm1nSU1lYp1uFL/5PM2NzAqRe5pkBJOscaIbn/yHKt2+cOaduYl3o3EHpbRBKs90rFtuTQu0PsrlMB1z14kqaamfW8fYVXCrB9MR5jUoPidrR8sCBeB/xFelfsYNjkD5oNIUg1Ci308JQuv32+hLliGUgGJyQRjeMVHcS+1rL6ON+5/R+OGhJTGg/m2FHqWisaU5SdeRjU0Nsx/S4rQKgyf/jXF/jM+X8NevX8egnl1wr4cRSov094zlSwi2cyYEGEfdZ9EqpJLstF1IMNH8/h9/SFBh9HWJ7G64UmAmMolIFkuWkgE3klPfum0rM+JYK6K47XeZlhM1KOAAcCukX3DRpCJ+dmPEZw9DQUBNK/Ek0gsPIvdg0OBBmDl7Guztsw4G0ahBc5RL7Y7C0mmeD/ob9Ts44eXL1/j0PoY41s6CtUk5Q4GuWPHBzJe8Q3bQ3p/2Q0Ur3IlwL4qVgIUvDDO5GkLOeuRkknAdIOcjcrIHiwCLQAYEclWMZmjLXhYwAsZ8zmxHO870J8fLiMzYRU062u8/yvHToHgSPbMT/j18JL28oC8SExPRp3sXJH54jl3NgfLF1KlWRRbmJ0l0pFU+MqRKOBjHtcMakoGzGYnIY8hZALO7/yqiHFz5sBWSU5LSMgxn1+7mzZuYNH4qYiKS0Lj0b6hYomF2TTUuD096idPB0+H/8jkcHHQbtPXWrVvo1W0gRlT/V+dOY5HJb3Apch5Cwj6Ay9VM98NssimVStSrx8yziv/BRBMfNWgCBlfZXfyFLWQJn0ScBFXhDc5fOl3II+tnOGbR5+XllWYYbmKimWPf+nUbsGfVRXQqP0fnTHuH7USJGok4cfKozmn36zMAHx6mol25qfmmnSKPw/3IHXgdexMzZ07H2HFjYGqafQaFQf1/QeyjUqhDAkUUxePYsxEYp0zCc5Jc+pg8Ht1djDCpvgAV9bS5U9AYMpESFz5SYdc7Dg4ePYFWrVoV9JDp9JkI7T//3Blj+vCxfHLJ9HL2Ali+MwEHzqbA+4A9zE0Lf8Nlw75E7NwoxkUjVwjY7LXsI8kikI7A78oQGDdWYs9Km/SyH/kiNEKJSp0+SaRymonGkfQjY8HKziKgZwS6mIpEp86dP081a9ZMI1ZCQkJQv1YNLKqnwsDKOTvZ1TuuwrLth9CxY8dsaT969Ai/kGy4peg4rG+EYjt3zhYAA6vw/6JEs6PJYqkKtQhr7zVhz9zM5GPrNm3LnjhxUqM9CyZ7UtPG9dDQLQ7b5lpqMgTbRg8IMHqQ4XOScfupMc5duIIqVaqkcxEdHY1SpUohybccitN+yIU7EvT9MyokWUJXIMKyDinpd5y9YBFgETBkBIw5xiHupbs4tqkwUaP/YUOWheUtewSYIMV9JjTE+PHjkZSUhGPHjmHtqo2Ij05BXesBac6+HMog7OqzF4KtwZHnozBAEY9xxtaFgsZXh1sVqEXy/89wq29j60KRu4gMwgR2ebZ06RJMmzZdLywzWa8dHOzpmJjYUYSBHXphgh2URYBFoNAQIA6225fadxme3yy2WTHcN3i38k5SBHXC7C2Xo0dnw6x406Tsd/HPSj8nIYx7HTXIyZQ6Pgip2xvQngnvKJ6GdiqayJ3XNtE37uDBwOFKq3tt9YpXTLOz6v1/iDj9OmVve5JXGTXp9zZYjgZ9ItRB/BqFvxmfgcGqSW+V/5YfwWtpVilDqX4uFcTu08p/Nr3V9CrlwquuHyZyGdVTXFcZU7MVxW+9TCMjS9WjjeB5z8eeVhx0cs55L+ROmAJDSPJe/9fvYGOjm73guLg4lCtTGmHDTMDnskv+XG7vD1l9I1SBlVGVcOP+4zT5SQAfbFy/jj515iyEAn5MQlKyF6lYS843BQyQBclYu/OrY61HATvWMrIw+xetxKFwcBqO2vY9Clg8lnx+EUhVJOJJFEkkRc4WLVrgtzEjiF9JmzSb92aNW8M+sZNO/ADyyyfbv2AQoGk1Nvp60omyaOZ9lHUEwYIZujhQLUGEiK1WzY2+cOFipmDpTJK6/v370UePHmMmCWbkTCkOArMysAjkFwF21pxfBHXTv5WpCXXW97CDsLhFbs8PPNfuS9BtfCJWrlyF30aPzg8prfquX7cW82fPxJZmHHQpZpH0GSASZGps85Nj41MZ6nBNMIFXGvV5onSMLssTMZdjjD61dqWX5eciLCkAD6N2ITo1EPXrekChkOPdu3coY1wPrRwn5od0ln0ZR9fbccvxIeRtWj2zEHr8+DHOnD6Lo4dPQJZEoZ7NQFS2bp5l//wWMuMdez8RPX9pjUVLFuaXXKb+Dx48QNeOvTGq+hEShUp3+rUTQZOwZOM0dO3aNdN4zKYY43x05NBxkj36FMyNbeAsagLXEs1gK6qAvW+G4ML1E6hWrVqmfsXty8OHD/Fz+54YVeMIiSKnkX6suEFQ4PLcD9sLu3opOHh4f4GPpe8B6tT0QA1qOIlcVR7vY70RLPFG4Bcf1KheE30H9ISnpyfKli37HZuO9uXR0X5h2m/vu8o8FtwP24dS9ZJJAAvd4s5kf69fxwP9Km+Cjcg5j9zl3I3JdPso+iBefbmOzp06o0fvbmjXrh3MzJh1HtHsrV2H/auuo6PzzJwJ5aE2ikTTZd7zFSu6kIxbLvD19UFsTCz5T5uU9t+S3yiBDEsf4h7i9buFuC76L/uXhCgK9stjsUkRBecSFKZ4CNAmm0xfeRDLYLpEksylg67TEJathn+Pn9LZxkFuAgYHB+Onhg1R3VmMKztsc2v+w9SrSTAWzz+iYE4CAO1fVji4xCeq4E6y1y6WO6Ij3+KHwZoVlEVAGwTeqaToLH+PG//aoUYlgTZdi2VbJgN3t3Gfg5PEdMFMPIolaqxQLAI6Q6CqkIv7/7QRmW9/Z0SNXfoPevXqlSPxlJQU1HSrhKmuCRhcNfd3WNdLNPpOX42hQ4d+R5fJYDpq2BB4XzqFY205cLFi1+zfgaTngj7nUiQ3PymOkay2v2THCjGc3G5dsuTwFwEBlK1t7nNOmUyGZk3qoZZzBLbMYeeL2eFqCOUP/aToMSkVL14GokQJZv8287FuzSqcOboI13daZa4oBt98/aVoMSwyTpJKM5EYpcVAJFYEFgEWgWKMgIAjlLSpOMm4pl1n1m6gGN9nRrTgRF/c+LQe7iU84WbTGkIjdi5VVG/5Q5K9QxJ+BGcz7CEUtCzMHvi81EjZTtmXlFTQncl4Dwt6TJZ+tgi487jwnj/Wij9zlBVfoaDRdlQC6jXui4VL1oLPz9l5I1uqeaxgDDIdHcvQkZGfxxMSG/JIhu3GIsAiYPgIWIs4gpBkHWax/VbkUBJ4u/rrxXQf/mRquFA/wQO+5Umb7+skM3DE9LLaeNQTkqDS8KbWylcnoLo9ReUZ+9oglKgXXOqouGNLc4VdnLSBWadt44feUv7mKOGtm1k4wUu+Zf7sTTF+nxGr9ONV05uz8TFZHJYpxaogt/kG8VyUebFAOYQ/l9dFMORbuAzie3NxeZW67QLKqMagHA031V9egzreFZ4OKdjUlAMBL+d3AhOY/ucLanj0/BWLli7PVVbGcXbrls3Yu2MrKGkSBldUwZrkpTMm4wy5JMb4Mb9h7cYtaXTu3LmDfl3b48NgJnEde7AIfI9AkoyG814JUlJl31f+r4SxI759+zbOnj5Fe508BeYZFPC4EQkp4hukCZPZ4wI51f9rrtUHefmM5INau0lUVtiVb5Xzj0Uryjk3DlfL0Tg5GP1r72IzkOYMlUHXxkpCEBB7CXeD9+CPBqdhLsh9n9GgBWKZyxWBowGT6KD4R7eVtLJFro3ZBpYikUnkrJkzBTNmzsrx/apQKEig5Mr0hw9BKwhs01joWAR+dARy/MH86OAUgvyOJsaU394lNpY925qy9+J/gI+eH4P951U4efJkWpSRQrgPePr0KXp06YgOpSRY2pBbrKI2xaSqseaRDLsC5OhuZIlJ/NIozTHKElZmc6xS8nv0qb0HJYT/OfZk2TiXQjHJvLfmYUfcvXsXjRs3/q51L89+kL2qCHe7Tt/V5beAiVjiF3WeOETy4GheA1ZC3WaV1YS/sKQX8Aqcijvet+Du7q5JF43anDlzBn/+tgB9XTZq1D63Rp8S/fAwZT1m/DUVhw4cJfzeRrmS7ignbIxK1s1gyv/eAI6hGSsJxcmQSYiMDsttiCJdf/jwYUwdNw+DquxkHWwL+E4+jjyGpJKPce/B7QIeSb/kFy5chIs7XqKl0x9ZMqKmVWB+l4FJd/A+/g5MTIXo2csTLpUqYsviY+hSflGW/bQtfBF1CSmlH+LazUvads22PZPduk2r9pB8tEDbclOybVdQFXJVKoLjfZEgjSAR99tl+/7Kz/jM/+RKn1aI/vIZ5ubmmUgxBv6VXavCnm6EJmVGZqrLy5fTLybhV1k4+gmyfg8zNK+SzPOLFeGgjVWY1ViALs5GJAhD8ZlSXgtRYNhNFcZPnoaZs+cUimxMNqyO7dsh9OMr+J0ogZJWetvPystjU2B9YhNUaNgvHBOHWGJ038zPvi4HPXopBXPmJOKWUSXwdRhQRJc8srRYBAwJgRHKYLh0o7Bsevb/FYbEb0HysmJXgmrh1oTjSWJ134Ich6XNIsAikI6AB5l1eu9sZ0L1qyRIMyZhgqV4eNGIiktMb/TtRWAgCcBWqzoC+gtQwjhHG5S0rkNvqOHWayJmz5mbiRTjZNmzSyeoPj7CUeJcy+MUnzlwJkGLyZewZPJsHEqSJEjpX0lKzwNfxTIywu9kv27TzV12GDI7BT937Y3xE6enBTP62ubrZ0xMDJYsmof9+3ZjxgghmZeyDiFfsTHUz0cvpOg+SYagj5EwIjc748EEOXJ2dibOtaXR0kOYsapYXb/7KEfN7uHJqTK6NBFMXKyEY4VhEWARKC4ItOdxBBdG1t5LlTTRn2F7cQGTlYNFoLARSJBGYv/TIdhrbIsmRv8fgLSweAhVydAnJUgcqZbfSgbdn4ybVFhj/4DjMHPJn8xMOe3lMvXAVh5C9YZZ1ibOjpnn2AwuzB5Wv6lJSFJWw8kzlyEQfB/Y6vXr17Czs4OVlW4D3TCOtq4uLnRQcDBrjPkDPqSsyMUeAWYDopqA4s1c6eDZdqxNswJXxK2NvqWeE3keR8xeckpwipajxCXZEcxXTaKFY99QlKBw/581eRJlVyYrzY2eU63unde7Q2XooRN4Nnumwupa6+//1DQRRgdtUlb6qes/Cebc2G2vA2rak1i1OwEHN6SqrplU0tv9qJv0TrmoTG9evxJ1tRdAxz3aBW5Wc2U11HNFOwzOSCNZnYi2kspqQZ9jHG7Z7+1gGShopQycG1Nh+v4wznbiopq1Zrf18Fs5hl4WIz4+HpaWljmiOmPqZOzZvgXbSfKg1jkE5Y+TqrHZX4lwCQXPciA2XHr7meUoj5gEiyFx38En20W5OSLnSIitzDcCNY+q4XX9PqpWrZonWmFhYWAy4N68fo2+dv06wsIjYCoSysRiyRuZXHGNED1IzucZiFc0AedKOyMLu/UiR6FJIdsHTU2Nwj1je/SovqFQbNEyyM1esgiwCOgAgScRXvT1oA0KuVrKrFXY/besMW0tEomuhIaGUlkFQs66CzBjxjR608ZNEckp4jIZ2tQm10/KlStHd+zYnthNW0CamorLV66C7PmSf3IckEqlTOC1hAx92EsWgSKNQIErPoo0OgXHvMBMRN0b6mlWbd0M6+812wU3rsFS3nI4EdPXpqJJ46Y48O+hXBeMuhCEyZwxuF8vBD27h0OtKZS30Gxhq4uxC5LGFwlxqn3MONUSg0OjEpjALwV7jmbRSheRxYNPyeZoUSFr5y9N+Q6IvoJQwXk8ee7zXRfGCYnL5aJ1hTHwcBj4XX1xK/CNPIRAxUX4PLoPa2vdRL4bOmQEwu4Z6ww/BcmEZcTVPFqZSq3AFr8euO19DTVq1MjTLSMTNwQEBMDf3x8fAj/CxEQIoYkxXF1d0pzrS5cuDR5PfzozxmC3aaMW4MWWR8uy4/IkI9tJewQik9+QDKGTcP7imSyd87WnmPceqWQRwETzY6KwyaRysiiQkWdUiJq1asDNzS3t2WfeZXk5alavCxd1T60zakuVKSTqoGlehszUR6JIxNbnPdMcRclCKlNdXr/8MW4CTh26TLLXbiaBKoqvgSyDT6I0Cht8u6Vl+P42AzhTv3v3biyeuQl9XfMXDCHtPvl0QaB5ZQg0VCYy2eiXKyOhMFZidiMBulYoHk63KqJVn/9IhQMfePA6ewEeHh4M1AV+bN68GVOnTkWdysSheYcNif6euyNIgTOl5wGevpKh7YhInNtiBw93zecOmrA9YHw0LB8K8bdR4QdH0YQ/tg2LgKEicIIEN1pfIhJPzzoQB5YfW8Xj+cdn8ZmbkhlkyclmzDDUB5blq8gjQNxpJwl51N9nPU1N6pb6fs1e/YgaF70fo2LFit/JGhQUhLrubng30JgExMn9fdXnKo06fcZjztx56bQSExOxdNafiLh7FFuacUkwrNzppHdmL/SOgE+kEh1OJqdSRnhT1s7I9fZee5FtyczrajWZ+9/ylcKHZAFVEo9ciZRGzcp8tGskhKV55rZ6F4hlIFsEmCyuPf6UITAoPEvDfvdqztg0TYbGdXS7psiWIT1WvAmSo1aP8BSpnLYjbLAb/Xq8F+zQLAIsApkQ4BpzjCOdrTysu/8fe9cBFsXVRc/sLrssHREQkGLvBTsW7L13o7EbaxI1ib3GHjX23lvsvWs0dmMXVMACoiBd+vY2/xvyx4ggLLAV5n3ffDs789599503O/vKvedWWcwOqDJBw35hETA/BAJiTuFZ+HrcsPGBHWX4MfNZRQrGiiMkSmCeAhom7BZjXMcm4pBGQGjpaE91FUvoBk4OXKWHK1dT3pvPLe7AERCnWQt7Gw5lQaKNCcgcmU/8Hv75pGBrzYEbccoowRzOWefe2oC79YgIM9eKiJF8Bdja2OD23UeYNNQGU0fYgHDmotfPYnTpNx/DR4zURlxGHsaRduCAXgh8cgM9W3LgW4kHMSF8TxfTCI+iEZXAxbX7Yg0xxDwrkqAbKaTrZ4HZpOlLjn72dtbVuFyem0gsFTCRe/kEQIbcJzk5JcPZ2NbWSpyWJnqiUqnnkPzXycEmFgEWgf8QYOwUye4nqvLBq2HDFdRX0KpKclplX4rvJKll5cmrI/Syrip0o6oS5xMPvsN/JQ14JiXR5Xxf/qZxUFehV9ueNvwfXAHa+lYVjP7SZrTwu3sUx7F0ASTpp6h0awN1hZFtuVXnT9dPBXmQetKpNG27pwFlUVm35A/aqiA9Hg6b9QHKqHMeRvFA/HZqvEp5icfbaO2jrco6zfdCJUFncQSdWmO50eeFv8ddxfLYx5qT9sEmZ5TxUhWAIbI2tHDkI4pj75WlD9SPSTTZG7OwohEHQ6poZ6PLCLkeqcSgaxQuX7uJmjVrZpH7+YUN69dh5uSf8HKgNYnSaPTu+ly1fJ2ryD6A48ZUXP3rOpo0aYLHjx+jbt26+KuXLfzc8zf+zZcibKFPCOwktua3HFtj/9GTn67p8oQhBGL6eeKYsfgQ+IL+Q+hFVTSCfeEtZTr5Pcehv++OAgeh0iU+rCwWARaBvCPA2NZufTyAFitS/tBAPTDvEgp1iWmE7Hjh69evqfzYuMfExMDd3Z3x9blP1oI4U6ZMqrNkydIcByCzZ8+i589fwORhBkNkmZBNLALmjUCOD7x5N800tSf+OWsrl+EPvb3Xw5pZJC+K6dELGSYtT8PDYDU8S3pi1OgxGDFiBGzI4r6+k0QiwaSJP+LE4QPY4M9BOxNlacoLDlIVneFUu/6JnESqdSROtSW+Gqk2J7k3yATiB8KoNbTekZyy5XovJOEvhHKPI+D5oyx5V69egwkTxmN6k1sZUWazZCjEF25EbkIsdR93798qsLMtEy3SzsYeY2udgNBCf9HksusOuUqMLYF9cOPOX1pH6GWijfy+fAXWr9uAck4NUd6uFco4NiAsUNmvizGT6hhRCC68X4hylb1x6Mj+DEbdL/VhHHRHfzcOr1+FwtW2LOy4HuBBACmSIKNT8C4+EPb2DmjYqBFat22O5s2bZxsN5XO5Dx8+RL8+A2CvKYuOPrM/v8WeGxCB9ymPceTlJPy+8neMHj0KHE72zwqjUnp6Oq5fv44rf/6F61dv4uWbYJQsXh52hGDAEsWIc6IdUjXvkaaMgUSdhGW//4YBA/pny0I2e9YcrFq5Bg3cvoWva48cnUWTpJEI+ngJwckXUbV6ZSz6bV6eHP98a9SFl7wDqru2NyCy/1R19t1sTJg3AIMHDy5w3dOmzsDurYcwoOJG4gBsekysBW5gDgKuv9uCUNllvAl7mWUMs23bNiyfvR29y63MQULut4LiryD67Wqct/6cGCr3cv/muEwWE5aSZ19jpcLyFpZobJy9oX/V0clnLImQ9sNtYrChccLeQ8fg6+urE7m5CTl//jx2bN+Ks+cuwN2Zj/6EeXTuWAdCCPH191NuMs35/q4T6fh1QzLuHXCHa/GCbXKICUFMpTYfsFHpAz8L/c8HzBl3VncWga8hEE9IcJrIX+L2UTdUKKX95u3X5JnzdY/m70XR8eo2pA1/m3M7WN1ZBEwMgdI2Fjjt68IrdayzjZVtDuuZtutTIZZIiXFr5ncRE7WScbB9/g0/1wi2p8MUmPhAgL8fPYWX1z8GK8xGTpMGddDPIx0z6xZs7GFi2Jq1OsGJaux4SeNmHBfvEiVwthGgmw+NMVW58LChsp13Mw0++EqOmYQg8MY+d5TxMor9mlnjburK33wkxZDZGrwO/ZAjgd7hQwcxbuwIrJxsgwGdrL76vJh6e7XRL4S812r1jkqXsRFttYGLzcMiwCKgdwQoOfF54k9qeIVEidENAaPeVTaBCqLTg/Ewcg8kykRUdOmMSs4tdEKIaQJNY1UoJAgwe6ungyajtPg1tgvdjDK2UhMdFsliFZtl8WRWSHch0N4qJPBq0wxHS0tMgYb63s9XoJk40N62UzPTH+NW7pqE/Ueu5erUwQBw+PBhLJo7Go8PORAjy5ztnCTE8dat2XtVmohmJvD25MhLpGNm06UTcZqdZmlpWZsQMfG6dO6EDh07Ua1atYKrqyu5rX1i7Co2bdpIT5kylRRSP5NI5N+QkxDtJbA5WQTMAgHmR+lDDsa5v6oD16oe8TavIdLISxbn2ciqCd01da28rasL3XmM02x5EgHdwgikDES3fKWr6a/QOWwTvcL6DOVr0ThfMoxRSKIRoZWknIbf6w8Ot1QLY6jw1TpphRiSVT5060cXKYfqzGNjvPRmzWY6eNtKteOplkZZ9FU8/QjxyJtq+a2SRnHk9u3xQd0nwpk7ytLZKJ3QPO21ekiJ9tREl+ZGNXp4K/uIyiG/0aftX1H2HCYgnemk0/LdWEzP+ic6tYXVJ8XUb6+COjMI31XSYEF9LglukvP46N+CYSlqDLsO2HhVxvqtO1GxIsN5kHNqUq8WRhR7jb4VMu+75FzKtO/eiVbh13elcPNhYCZFa1Yohd+rJ6IR62ibCRdDfWl6UoPJK7ajZ8+eOq1yw9q1mD1lGpZynNHNSOQdccSeornoHfzKTUZVF8aUgE0sAqaDQII4HC8//omY1Mdwsi6Pxt6j2XXbPHRPnCgUW59k+NjuJ8UG5KFoocxK7DQudWjftvWJk6e1G5zkgAKzphIdHf3JZiOHrBm3xGIxvL296MTEpCHkwp7c8rP3WQRMGYEC/4BMuXEmpltPR1vO3sATJYWebkZZF9ALHGo1jVfhSgS8VCA4VIF3EUq8j1LhHYlMEJOshloDNG/mD3//ZvCtVStjgd7T0zNHhyldK8pEI5z6y084/McerPen0KEQGB0zE81Jf0nBEfOwkO+JugXcCGcmEXXTwzC2wdkCDc6YCLbv+WfxJPBBtt0YGBiY8Qz0qDQflZ1bZZunsF+8H70fT5MO4cy5U3lyyPsSl99/X4Eja/9Ga++fv7ylt+9MxNuNAT3x8MnfJOJs+RzrefnyJQZ/OwzxESK0954JJ6usTG45CvjsJuNkdvXDCixcPB+NGjXE1St/YdH838ji0RoUt/L5LGfupyJFEmJFrzIOBdKhIi65KrUSzvzycLOtCDebSuByCs87OndETDsHYxSQKH0PZiIUL3sJBZ0OLoTg0gI4CX1If1Ugz5Y3cdrXfr1bQ6txMnQ6SpS3xLIVi8FENf/px8mQf7RG51K/5rv/NbQKNz9swYvE81i6fAkhjxieqxFDowZN4ZzSHNVc2xmsI+LFYbgcNwfhkaH5rpOZDDHO6EEPw9Gj7DKDO/vnW3EdFmTeJavudUS7dh1w4cK5bCXv2rULC6etJRFt12d7X9uL19+uQdXE61homTfDgS/ly2kNNijisY4cgyvzMc3PEvZmznAZQxxux96iEc1xyXC4zW909S+x0vZ7WlpaRkR2Znzz4N5d7Nm3Hw5Cihjb0fAi7PLeZMzvU5KHsqX5qEGib/mSI7+s89rqZOh8I2YlIDpehdPrSxAD+rxPLcM/KFG7SxRuWVbKF0mModvL1sciYMoIqMi4qYkyBMt/c0Tn5kXXUPpDrApl20eK5QqaoT1n2RFN+aFldTN1BGxs+djvZMlpcbSzjXWVL6KNZqf8TfK//us7H9x5/DzT7ZUrV2L94tm434sH6xwibkema9DomBKnLl6Bn59fhoydO7ZhysTx2NqMg7aFgCgvEzBm+iWRGEszUYY9azXD5p17YWeXlfyNIcNaumg+Du3YiMudSeQlEoUpu7QlSI5V5PhzJ+tsmx0+5nYtJU0NR7/3mDd3BmbNWaC1+kw0rLNnz+LX2ZMRHx+Nod0s4VeDhyYkyi0TwauwJCaiLeNoK5XRPqRNSYWlXWw7WARYBMwHAQFHEGQrKFFpcM0tlKHJW80HpayaMsSzmx50wzIvezSx/48c7mpKOmZEJqNjxYUoW+yfsWvW0uwVFgHDIyBTpeOPJ0PxHUVhvGVxwyvw/xpTNCrMlsZIjyuSZMThlmF8PWM0ZfRTMZ/Hww/EmWJOzzZWvPk/FBP6mCHJ6YYDqQiIaY8t2/bmiBKzl+rm6oSkux4kWmze9gHCiP3QhoNpiqOXRLL4JA1PwEcSz4JKVihoF4mUduJbUOoSzlx5Kz8hv1UDoWXTOpZoMCAFF/68r5XTSY6KZ3Pz9OlTdL9+/SCVyn4kt9dlk4W9xCJgVggQd/5oULwSv3l0p793JpEmCnHq8Xar6oEoEcfsXpqVQU9LSTm10n8CZVF3nEn1jzrmKeQHOmh6iN4SznvjqaZRKnHC3pt2utuBRAhlgiwbNmlS5EhoeIqmn/jk7Q9OR2o61H9Hn6LKUtV4/zlv6kh0rmKY8VrltJd0WvXlFN/IdnIOATM1i60PcGpZNMlVb0NmWCgeS5+zf6oRfvd3hlGa5uNL8E72g59tPHa1pOBoqd1vh9n7GHmDhtTeGweOnUKpUqWybUZYWBi2bNmC4cOHf7LL7Ni6GdqrH2JENcP/PrJVUkcX9wTLcZ7fBCfOXshkWzdr5gxQV1dgej2hjmpixeQFAQ2xN2hxWoOJv21B375981I0U14lebevXbkKC+bOxc9cJ4zkFQOXzFGNkRhbuS7iSFg4N0fLspOMoQJbJ4vAVxFgbEIjPp7FrjLOcOX/R0o8/m0MonlV0KPKiq+WZW9kRSAqLQj7nn1PKzWyxeTujKw5Cv0VCztr6k7Z0s61hDYe3NNnr6BYMcOTl2g0GjLW8aEjIiLZdZdC/8gV7gYaZ+RSuDH9snWVhALqwZkNJWxaNjDPwb+SOApcuSfFHyRi1embEjQizgLtPHioTqJWVSvOhU0OESxS5TQaEgO5NbsOoWPHjl9i89XvjAMPY1xz9dIFxMdEoWzFyujSvSdx1vX/apnPbyQnJ2PWtMk4dmg/Vjei0KWMeTM5Jcs0WHZfjl1BCozlu2Qcll+JAvo5DtqcK8hEojpxsO1aYwNK2OTsOJmTvHNhC+FYTo6r1y7nlC3jXkpKCoYMGo5bN+6gsdsoVHFunW/HtlwrM9EMHyXvcDx0CvoP7o0lvy2EQJC3xZD4+HiUKVUeE+pcNFgLz72bh3Gze2Ys5mRX6aNHj9Cv9wBYKT3RouR42Ap0z7THOF1SRpp0Z9dm9pr5I6CPZ4om79XHsUdxN2YXpk2fgslTJmVLLsEsKtna2OHHOgUjOMhLL1yP2IhWQ8pgzpzZeSmWkffPP//EN8S5tq7zQNRx653n8uZegHlv34xejxQSGfmPA3vQrFmzXJt06tQpjBzyPYn0uxnW/PxPWo8GjsMoZQIGCZxyrVPbDCFqKaYrPiCGJ8OyFkKzd1qIEmkw5iaNJMLAvGbzdjRs2FArKBISErCWsCaGBD6GtbU1qvrWRecuXVChQgWtyjOZJBIJalYqh1+rpqJ72ZzHnIyezxLUePpRjT0kgpcVMRof0M0WfdvboKz3fwtmWlduAhkZVnr/QdFgmPnnjtP+Of/zrgSjxifhDr8iiTqu3QaUCTSXVYFFwOQRGKR6i0ZDeJg22sHkddWXguv3p9Iz1iSdSE2ne+qrDlYui0AhRaAWcazdWdKGU3pzK2ubuiXyZq827BqNluOXf1ozCAkJQdOG9bGuEdnAzmVdjjEyqX9UieOnz2WQ5DERLmfX4WBoZQutGeALaZ+YTLPepapRaXcaFi1cSOa507XW6+DBg/hpzHDc6G4BT9vsx3wh8hT2AABAAElEQVTnwxX47poEe5e5ooO/4Q3KtG4MmzELAsy+wYw1Ihy6TGPj5t3o0KFDljz5vdCwflUs/zEZDX0t8yvC5MqlE6Ior1YRkpQ0TXOiXPYslSanNasQiwCLgJkjYCHkCJ85WnlV+LbGRorPNc99amP2wblXc9GZH4C+xbOfY7+VyTE0NB6+nkNRv+S3xlSVrZtFIBMCSdJI7H86AksEjujNZ3jIjJeYCLeHFUn0LGm0VEPjVirUjLHjDS01YiamKi3z6jtbM1trakPJErySG2cVt21a1/zfqVJig1Ks4Qeki6SERPPrawBr1qzB6/vzsW6Gnb4xzpD/x5l0HLpZFafP/aW3+u7cIXYqjRsz8keRY4veKmIFswjoFwHGWk8a61CD85Ls+3YRv9d8W6y+Zm3J3rzCalMTLv+IGi8X04P5M6lvhRP1i64OpfcX+6siKlWFoOOGr79sdViftqKUT7ZoOC9WazpHPDGqXoFT5qrf/X2UctzdNPvFQ20blM988dWO0s/2u1JVy+XNbi+f1X0qJiLrRMUavqNjbWsaxX67V3oY7etYV/17yR5G7f9KQYtVLbgjOYMsfzZK/3/qkC9OBotbqEPLl6IFLRbxOGcGwkvyHEfaUijtoH0QiHPv1Jj4gI+zl64iO8J4JjrcurVrsGDuLBIRl4PxNbkIStRgUpArngS/wY0bNzB5SHfc6PKFcib8VUkCNqUqaIjJunG6AhCRT+Y8jVwLT9XgzzhLBCepsWvfQbRv3z5LSxgn44ebJ2NtY5N6HLLoWdgvXHmvROdTIgQEBKBGjRq5Npexfzx69ChWzFsATXQc5moc0cjiP6KyXAXoIQMzDx0qjcZbEsSlW9XfiW26edpj6QEaVqSREWACXZ0KmQJ71RusL1UcQk7277tR4clwdRuPSs4tjKyx+VXPBCHaEziGVqjE2zTQjDS/FuRZ45I2VtQ1QlrmTnzVrP6dC67YLcKaA8CVv+6gbNmyeRZakAKMTT4hFaHfv3//E5GzqiCy2LIsAsZCwCiTNGM11sD12pKX1tPpIx28pn3naFYjNObldvxPMaYvT0IJwkS5lEQ6q0GicRUkBSWq0eCgCN27dsHaDZvg6po5GhtjeLd25XIc2L+fRFezwLiqnCyGV8wkbGcwiYrxUINhI75D/0FDMpidIiMjwZS/c/0qdu3Zg+ZeAsyuRaOyFlE2CtImfZeNJYsp31+WIjyOwmZLH1TWw+a3iER0rJ4Win6+2+FsXSrPTWLYcK9GroRMGEmi1z6EpWX+jJ7IHyl27dqNy+ev4u6Dm6jj0RPtyvySZ33MtcDzuIu49mENJk/9BVOnTQGXq92CTK3q9VGD+x1K2lU1SNNXP2qPt+/foHjx/5iPnzx5gr69+kMgd0Mbr0kFciIzSCPYSlgEDIwA85/6JPY4bkdtxcw5M/DLLz9ncrjt0KYLrKMaomLxZgbRbPfLwTh/9RiqVtXuvcH8xrt37YXiVGW08JwAS55xF8EMAhKpJFESgY2P+qKMTwU0bNQAAwcPyHCqtbDI35Bu0cIlWL18Pdp6TYe3g2++mnHi+QT0lUVhnB7Y55noh0yU27XKOCzyt8TgKobdQMoXIDkUYiLJrghQYXuwBmVJ5PUho75HuXLlMhZ/k5KScPHiRSxfNA91HKSYV4eGt13m/10VsfQ5/FqBpYFc2Ll6Ysa8RejcuXOm3y7z2962bRtGjhyJWQ1tMb1OwcbKzDh35VMZVgfK0butDaYS5zhzY74PJQvtjb6Nwq5FLmjfJGfniN0n07FliRhnePkneMnhEWBvsQgUeQRmKaNAtZNj3a//jduLGijkfZR296l8OGn30aLWdra9LAJ5QKClgIsxJH/HYVUE1Bw/ocBekL+l4jRCsue6OQWOtlbgc2h8V5mLIZW48LDJfmPwSx2ZsdBbYmBRoVjmcdmX+djvhkeAIYlpfVqFqQuWY+So0flWYPy40bAOPo0ZVaXEmCH75ywsRY1OZ0UY0scOc37Qnrwl30qxBfONAIkYD0vfcIwcMQSbt+7MUU5sbCwYYj7GaMzBwSFjbp1jgf/fdCluh8BjjnAr4L6ENnUZMo+GzDfr9I0SvXijmEpsj9Ybsm62LhYBFoEihYCrgGP5xsehjk3PKospDlWwdasihdwXjT3zcjr6CUPQpZj9F3cyf01VqTHibQIE1nXRvsIcsJhnxof99h8CafJ4rLnfFY1LfoP6noMhtMj52fqvZP7OJMoUrPi7PVZaeeqUxDN/2vxX6qYyHZvlH8XXlWmWfA5FuXtxlR2bCQUd/K1RqxIfJLopBMR1jMulEE/sTMIilXhLjrBIlfrSXYnocZBcaGlBJdAc6lpauuY8kcx4Y8b9V4NOzprb21K/WfCoynPHOQpH9rLj5DWKq0600LOQAVNEaNNrKQYPZgIOZ586d2iKER1eoWsL6+wz6OFq5+9T0a3/YgwndkH6TNu3b6d//PEHWiKRMhunIfqsi5XNIqAHBOr7cPiXHttX/vRnkkgiU/qLQtXVrXxwuvQorqCQOpT8FvunenHcFeqwbQjHnmMea0izxcM0V5zf08LBV01qAVR2YqDKtbyC0+jEHu0WcvXwIMsTk3DOqxpd/EW37BcN9VDn5yIT211Qr+rD447r/+mn9PltvZ2T8QzaDo9Vv+ZVM8oz4ZHygo6oMp9ytrDVWxtzEzzo3W68TrdTrbY9ZVKT1tai8iqVizPHNj2Uc6gtBw3c8qbeB0Iq6ndMgdMXr8LPzy8LDFFRUWjRxA993VMxxZfzab38OhnvjnlgjVdvI8Dn89G7S3t0Ud5C7/I5E71nqUDHF25HKbH7DQdXIlSws3dAzZo1UcW3HqpWq5ax3stEyfv3sLEpmE3Z7t27cXXVRGzRLg6TjlvKivsSgdfJaky5DzxJoFG1UgW4lvQGly/Em+BghL96DbVcgQ48O4zmF0dFbv7sxr+ss6DflcSWq58kEnL72mhecbbe12diRa9x6dV8qIkd/rA6+wuqPlu+ECOQLk/AoWej0cpWianujrkGmvrxXQpsXceiqkubQoyKfpuWKovDroARtEyZekdJK5votzadSWdepozhLnMwAwDmkxmIMBHP3MhRwtGOaqNUoYGlgBL+PNjBsn9HG66X+9fHKvvOiDB1tQpniX8O8x/+eTp8+DDGjB4JD4+S+Pve/YwANZ/f1/ZcoVAgLS0N4eHhYMYCjI+Js7Mz6tWtQz95GrBLrVYP01YWm49FwFQQMMrk1FQary89iHPt0ca1LNuf21jCivMVoyF91Z1fuYyjwB7CPvPz4kTiJCDAmGqCXP/E81tXnESDWx9UiCATSmLMhzquPNQtwSUDWvZxZBxrf/hTirBYChsE3qjOy9lBIb99wJRLJYurNdJDMbD2XhQTlsyTqKexJ/B3/E7sP7gXbdu2zVPZ3DJv2rQJOxdfQRufouNk+zkmLz9ex/Wodahb3xcbt6yDt7f357cznfftNQCyF2VQ3bVDpuv6+nIqbCbGzuyNevXqkmiW34IndSGOtZNJNGvzWLTWFy6sXBYBbRH4x+H2GG5Hb8NshonwuxHwLlkKQ6rsIZGfDeMEs+SOP0Ti9K9GzyYTGixbthyLFixGNeeO8HNjjDwMw0ytLY6GyBcvDsW15MV4HRak0+oSExPRo2tvxISJ0b3MEuQ1esSZoCnoKAnDz5bMvFk/iXG4XaWIw2ZlPBY3tcSgysxcnU3v09S4F6MGM47lky3GqsW5aOyRP6drbdBkxsn9L4tRvKQFdv3mjBJmZFh+5poYI+d+xJ197ijtmRWj9X+k4tRaBQ7yymgDBZuHRYBFIJ8ILFHGIL6JBLuW6+8/I5+qGaRYCnlvu/q/lymUcCEVphukUrYSFgHTRqCntaVgAfFlreDrWwPjJ/xEOTk5Yf6YvrjUgTZtzVntjIbAzQ9KTAgohuCw9zrRgSHZq1ODRCNqT8HX5eubfQyz/UBCAJlMnL6PrSsB9xzy6kQxVkieEHhHjKpKtYlEQkJCJiI+Zj3h8ePH2LFtPQ4dOoL2/jYY1ImLdp8R8DDrIou2pGLmmiTExMSgRIkS2db9/bhRkMYdw/Z5hjVuzFYZPV08cF5Efzc74YNYSlckVUj0VA0rlkWARaDoISC04AjSart147YqM4HddNVB/zPrxOeCxuFyJcaGKffErK1Oi0hEkLIYelVdVyTX1nNHic2xjTjZHiMRZoXgYJEiCZeVItRw64ZaHv30tlfEOPceDhyNTgT+RZYu4JqwXcZHYsPwXiNHpEaRcbyzIOccBV7IpFBQNGqWFWBAb+sMskq7/xM7MUQmjKPKtYcyzaXb0vS/A2UCLgepfAvcSUmnn2k0eEWa/oYckeT415GFeU8ym3N1bKw4fgI+GsjkdJkBnW2oIV1tLf1qmoaRONFPr+nqPUJGusMVN24/zbaeuLg4VKrgg8Q7bnqzH8quYjVZwOg5MRX1m4/HtBmzs8uis2sMIVCd2rXol69CrsnlqpY6E8wKYhHQPwKje1g4Lttq453Fo0lFXnzNRWEq2sKWc7PcBE7xQkikLVbLUf3lYk1Jui691ObIv+92/aNegBr2SVdhPXedRvh9MIfiMjbzppEka8ppai2fxCk98uuEC/rW9N6A71QJ8ic8+2UN9F1VFvmpP95R9eUl8xgiaUOmA+dEmDkvWfnQokrWDXU9KzJO/B60VSnFwVLDjPYgHkt5imHvjtMX7N9RXMr4P2HGQW6XfBH2q5bSW1tbUd3L5g8auYpG2X1yPHnxCp6enpl6UiaTwd7eDmOqWmBJ48x2ONtfyHFYUhk3/n6YUUYikaCYgz0SR9l8csLNJEyPXxhS+jE3NXgscsCSlWszCOn/jYqnx2ozRC9cuBBppxbhV7/M+Oi73qIsn3EKP/CSOIW/UiE4RQVXHg9leAJ4E78uH1oATw4fXuQoxRXAzgR+q1/rq2gyf/xGEg2BY320rTCL+CLo573yIe05HkfsRGRqACYQm+kRAiesliXgnnNrNC3F8BqziUUgMwLR6SE48vwH4lhrh25O2tngRhAH9v6hSRjX4GJmYey3fCEgU4mwl0S2TZN+CJVqZOXzJaRghYSkeD8HO+44sURdrby3hbRrS2ur6uX5Fl6EyKOUBw/WVhxYCykSAEZ/S/pRcSqs/UOCey8oWFlSaOunxo/f2n1a6/l2ahqq+o0ngeJmat3aWTOnYsO6lbi63Rk1K2X+75bJNYiOV2P5rhTF3jOiMJGYrkcEi7QWnnNG5iU/hAS0629nZ9MgPV0sdHR0RKWK5WnfWrWpihUrUUwwJWb8wuxHBwU9o69fv4nU1FSaVqufSeWKkaT8P4OunOth7xZhBPT3ayyCoJLf41gXR+6ywBMlrZwc9DNI0zWs76OVaDc8Bm2JsdKv9Swh4LGPhK4xzk1eDHGsHU8ca0NjgfUCH9TQo2Ptv7rcI4OGAdJ4fFfvOOlz7RlHgxOu4HjIrCxGVP/KZT4DAwOxZdM2HDhwEBTNg5tDaYgVyeAJNdi7fxcaNMh5QaxVs/ZwTGhqsKiOn+tuaucMbk/ijuJJ/Am0aN4c8xbOQfXq1T+p+f3YH/H6Egf1S/b7dE3fJymyGPDIxNmG76Tvqlj5LAKFGgHGsDQi9SmJalrLYO1UqmVY+aAd5ArZpzrT09OxatVqrPx9NdxtqqBO8W/gaV/j0/2ieqIhC+ibAnsi4MXDLAvfn2MilUoxcvhoXLzwJ+yFxImJTMyiPr6GX4NGGD5yCHr16vVVh+aqVarBk+uPWg7987S4d+f9NiDmFI5a540g43O9tT1njMJWKmKxVZmQ4XA7kHW41RY6nea7H6PCt8TJ4KcRDpg41EGnsvUpbMaqJDAGOtd2uUFo+Q/58dItKXiwQ4MtPB99Vs3KZhFgEfg/AutJhPLntdNxeINhDQNMpQN2HE/Dz0sTzxJjys6mohOrB4uAnhFgHLUm2Ntad5MpVC5uJVzRo3t3dOrSlfL39ycRgDKvVS5evBiJJxZigV/+jEX03BZWvAkgMPyqEnWGz8PEiRM/abNl82aMGj0alsRavbiDLaoTtvoR48ajU6dOYDartEnDBw2APOAstjXP/ExmV5Zx9B34pwTjh9hj2iiHTxt92eVlrxkWATEhIIqMVYE4iZJNX4BsCmdsAmujxUfCgF+9RwJ+nDgrI8rBu/BQvA9/hZCQYCTEx2LHfBu0bqg/8kltdDREHoYUpErXD+K4RPV3xEf5gCHqZOtgEWARKLQIWFhyhHGe9jUd+lZbwW706rib/wxdgia4i6GueSOc3RGXgh0f5ehXfROcrL5OqKtjdVlxZoDArXfbUCf+PKYLXTNpyxBkbyd7w5sVJFqJZQlUcOuOKs6tdBrtltkn2vyoL1KI022iY+boFZmUMYMvKQSvEyRS7xFeEt4SR6vhvWwxbawD/nW8NYMmmISKjDOrb+9kbN19EfXr18+kE7MHVrmCN7bP5aFFA8Ym1PApMUWN4o3eY+yY77Bq9Xqt55350fTcuXPo06ePhji1lCbl3+dHBluGRcCQCFiBs3Om0G3IqFzImQeI3qkf0yrO7XITqfKWmf97DKmvvuq6kBqEnuHb6bU2l6iqvLr6qkZnch8rb2KcvDctHPOM4tiYRn/QojhINlSmO0c8pSzJmrIxUtrL17jasI3G6XHHfzaVDaiEeGMwKlx+rX50wD33xUod6vXr+mT68hYFztiWM/gcrlRqiOZ+hUmcimTMaYyUopLA7flcer/dQ8qda9y50gX5fqxRTMCU+hz8WItX4PXnB2S9dMorEg0uIDgTtMt+W4ID6xbhry5cWH5mn60hNjmdz2tQq8sQLFux+lOZaZN/hur6Jsw3oLNpCnGGaX1ag+7Dvse8BYs+6WLIk2Hf9kO92DMkWEVmRx1D6lCY6wpPVWP2LTkuvVegMd8GfThOaEcCb/Apg796dQbz3yopOqW/QnOfMWjkNUhnchlBCeK3CCLPY3D8BXgRp90JBKuOFvZZgonVEoWjXfUNcLZmphFsYhH4B4Gg+Mu4GroYG0q5wNcmb/PpgaEfyZ7bDJQtljUaOotv/hFg/DF2PB1KixQfTxJyjR75l5RryXK2NtRiwhPX/vsB9haj+9hZeBJnWnNIxRsTh9SQcLi65j4naNakDsZ2j0Sfdlk4n7JtKrHdUqzcncoj9O8Ms89BcqiyzfjfRYb9rgHZg+5kbWX9vUqt4bdu1QK9+/SjOnbsCMapNr+JIQYfM2YUfePGLWYNiGF2M87AJ78NYMsZBAGDT9IM0irDV1JfKKCu3d7nLqxlJs4HT4LlaDssBpuaCtGxNGtMZ+hHRk0Yn+bdlWPvcwX2WpaBrwEca/9t4wJZPP4U+qBb1d//vaT1J8PEsytgJBztnVC9qi+UKiXi4+PwIToC5Z39UNG+LcoVa/zVBYf3KU9wOWYhwsJfZwkrr1QqUaZ0WZTkNEZLnx+01qkoZWScvsKTH+Jl2iW8TrhDjNjS0a7sJNRx1+d4ryghzLaVRaBwI3Dj3VYkWTxDfHIkrHnFUcGuDSoXbwVrNhp1th0vU6Vj+d02ePDgAerWzbohN3nSVFw48ADdynx9jpUkjcTzhHMISroImiLGs5WqZRgAMM7NjwL+hn+5gWhSYsxX/zezVYxcjEoLwknCcvbIpgwcOIaZhDMOt3MUUbiAZBzqaoUaZhRZ9Ws4mtv1NDmNfiS6rTVhMDuzWbvoGcZuo5JEIeswOhbliMF9zYp8HFspxwFuGWOrxdbPIlCkEPhNEYOklhJsXVw0I9qW7xiR+uadinGyvVWkOp5tbGFEgDHw6UiOPnY21s1pUCVUajVVv25ttGnbnvJv2jRjzMrna7/G161dS/Tj3ke3fDKyF0aQ2TZlRWDZUzXWPqdR2scb9wNeIGqkPYr9n0Dl89xhxPB5+XMOHqQ74vDJM6hSpcrnt7Ocv3nzBuXLl8e13rZooMXGIsOeP+NvGU5GKnFyQwn4fsGGm6UC9gKLgBkhcPORFO1Gxoqkcro5UfuRGanOqsoiwCJgAghYUBbX7C3dmg6rtZPicws/QYExIGfWeE88G4YrlT3yVf3NVBEmRSShe+XfCemmb7YyFtz0gyWHCz5XiA4V5rFGdNmiVHguvkm8jbjQZThm5Z5ro0Rkb/g0cbo9RZxI7ylTYWvhCE/HevAu1hAOlh6wE7h8NWKyiETJjUh9gg9Jf+NFwlXUs7DFr4REuSovb8aduSppQhmCiIH1LHxAjLUCv88qho5NrfK8/2JCzTGoKk0GfUR0og26dOmM6Kj3OHzsIg4ud0ff9oxNo2mkB89kOH5FgeOEECoqToJqVcrAw8MTXt7l4OVTFj4+PvD29s44nJzyRxjOOBZXqFCejoz8QBhvMdI0Ws5qwSKQPQJ2FCdkv3Xpin4W2hk1z5VE0TuVaThXZgzlb1M2e6FmepUhOO/0dpM6SCyhD9k9N8zmeQGw+qiJRWdJdVrw7SWK65b9+LAA4vNVVPX2KpRnh6h7poUa1NH0c2WvNe+sklZL4dlM/C/4xOf39XUuuxgJzuJHyqRLHtoxCOpIka7fx6qc71rxllrpn1z9c5UXS2Jwj2ejvFV+okHb+7kOLoFzVBOFq3kt+N0+v2yw88fKG/hV/i16VVRhUROuzoMSOW9KBVdoi+vXr+PJkycYPnw4rpN18PpfrIMffa3AzGfWePI8JIOIkAFAQyKRt29JgtOIAvFbQ8O9zn64qYJtw2+wYv1mg/VDdhVVL18Km2onohYJGMWmgiPARFbe+lyOJffk6MxzwFS+G5w5RvvpF7xBOUhgbNpOEAe6xWT+LCFtrOLWDR52NUmgkWokoFBWp22a1iBd8RFphIAqXhyKKDJPf0uCpdgTZ9omxJa/D8+GfNrkOp9Ukno9Up5jmv+dHLRjbxUGBG6/34pHH/ZDRMjLmnsPRSPv7KeLt99vwpvYo9hV1hXuhLA4r+k9iWI75G0aRtY7m9eibH4tEYgVvcbugFG0UiMbTYps0bJYTtlshELMhYYaO2mYPfeXoQ58W2vzJC+YtDwVxcpOxLTpM3JqL7Zt24YbZ6Zj72LbHPN97WZwqAI3H8tw45FUHJ+oUYmlGvh4WHBJVF+hZwkut6yXBRr5WmaQOzO2p44NPyA65iPs7LSLBv21erO7/vbtW/j5NaDT0lKuy2TKFtnlYa8VTQRYJ9uC9buLlZAKXDPNyWV4TzuzeCNeuCXBoF/isKOlNVoTY3c2GRYBJvrCoHMSDOY6YxK/RBZWG31qk0Y24RoT1py6xCmzqksbfVaVo2yFWoqld1qQCQgHdby6wJpygaNlyQwWZ1frshnXcxTA3mQRYBFgEWARYBEwIAKMIUqs6CWSpB+QropFkiIcr2LuYWjNbWRBLmfDcX2qyTDO73rcH1NIdPHBgvwzM+VHx1BS92DZW9T2prCutTAT02Z+5LFl8oYAQ9bS+6IYFNlYOLG+BPh805/SxSSoULN7FH5VeKAP69iftw5nc7MI6ACBqcpIOPVSY/GkvEXd0UHVRhfxd4AM7UfHhKem0yx1rNF7g1UgFwQYy6GRtlbC9hwez1silXGqVq6EFi1bonETf6phw4ZwcdFdVOq0tDS4uzrjwzArdiyXS8fo6/ZDwmjf/bySGAVXREpqOgJehiGJRF0SfsZir6+69S03KFGNOn+k4fLly2jdunWO1W3buhVzp0zApc48lHHQzoaPcegdeEUCj9IW2LnEBcUdtSuXoyLsTRYBE0Bg29E0xbgFH9MVSjD/CdEmoBKrAosAi4BpI1CeGAm+HFxjE+VmW9G0NTVz7RjCw5uvfsLpigUjvYsgRnK/vE+Cml8KVd36ooJT0wxjzd8J0WMlgQZ7y/9jXP9epsCA0FhUc+uFJt55J2g0c7iLhPoMofKb1/NxwSp/jttfgsTYAERpFGCMe+2IQTBjFMx8cinTXzf+si26/K4meGxXJWC5Og7r5jmhXwftnNB0qQMry3gIBITI8dd9OVbslaJM2UrYt/84PD09tVZo+bKl9K/z5ilEIjGzoCrRuiCbkUXAgAgQI0VluEM1ng155+clHZAnYbI0ht7s1Z/+tlhds7B11LZ9b0jQiVqvfqNHCRZSvS0Zm3nTTWqNGs2kpdSctr9xedW+MQlFFbcWqi2TztDtnt8windbws27uNt3sMrxbhuD1q96lYLUb65qlHc9Dfp7KNc+QvPzR3dOH4Fh9+4qpb1SHyk9imssZ/sGL1eoS2naY4LVb3l7eRXwVxKuDsFceT+ULRGPzW04cBIatLszaa9U0zj4Wo1FwdZYvnpdBhnlixcvMGzYUMhkcsSOcoC9wDBjeSZ6be1DSuw5ehotW7bMpKehvwQGBqJfO3887WPQR8PQzdR7fczzNZ8Ef9rzQoGNAh80JWRLRTkx0aIVoJFK5s3M3DmFHM4UD+7EEVcXEXxTNCpUSw/FT42vFWWYC23bZSoRTodMhpUyHDvLOINPQmoyay81A16jfdnJqO3eHYz9ZkDsKTyN2ocWthQmuTsQIr38/8fEkg2iHq/jMK7BReJfYtAhUaHtx6817M77HfTdyH2pco00r4a3zJ90b2shtbZ+DYHV2mnFbSoXElLzl28V6PSDCqHhMV+DLeN66xb18Uu/CLRtbBjSz3V/pOJhRHPs3nskR70KcjMhISHDHsfCgrtfqVQPKIgstmzhQMAwo/HCgdXnreDZWlNXu7eyrrt7kYtZUH3eC5Shy6hYnOhgjdqu7B/v552p7/MYsQZDiWOtKsUCmwWl4GYENqCfpLEIta+DZuWn58quo288WPksAiwCLAIsAiwCLAK6Q+AdiRJ/LmgSrloTdm5uVvY93dWUvaS9ikTC8hmFxU0tMbiK4evPXquicZVZDP7mkhi+hI1/zvhiZjHGu01YyLqNjMNxi7KFOmJC0XgC2VaaGwKj1O/QZCwXPw60NzfVC6xvr4mxomOXJT8TQbpgoSywPqyAIo1AE4qiptnb2baQyxX8OrV90alzV4pxpK1VqxY4Bdhwyyuqo4cPQanXhzG+lulEpMlrG8w5/7wHSoS5t8ShYyc/NWPPnj24snICtvh/umT2J6fClJgV5IjA4FcQEgrfnNJ2wng79efx+LUuhWFV+DllzXTvBolsO+wvCb7pYoNFPzmZBQFNpgawX1gEskFg9b5U1fRVSUkSKV2L3I7KJgt7iUWARYBFYICAa733x/qnKAHPmkVDjwgkS6Ow9+lAHCxXAt6W2o9RtFEpVaUG4wRYzOLr+/aMY+6ZJBECpDReSiSQkahGXnYVUcqpDco7NYGtoLg2VbF5TAyBh1FH4Bq1HyuFJUxMs8KrjpREKZqtisIVfir+WOWCxrXZuXDh7e3sW7b7ZDqGzEjA/j/24Zv+2tlKJiUlEaeXcnRiYtIiInVm9pLZqywCRkOgqgvFuxviUDXfnjP3lSL0lRAnP5dW9By3DoXKo2lezHn1yvhbOGr7imvD0X20I132emdxTVVKna4Uv9mvJtEH0n3tVD7ty/Jqb/xdl83UWtbFSvXV1LBiXGGv0lqXKWhGWq5GfNWjNP28lEHtqC1939KPhZUpN0Kqbqj0hzwRa9Uy9evKc4zyvE2JOomTiQnqPXZ3DFJ/oiYOC+QDIbN6jn2dKJTWkuTRUP1h7HoOk0i6i14VQ0Dwa1haGn987OdbBTO8I9CKDRyVr0djO4lYO/uWDNP57hjKrhXkC8P8FiqT9gpD6h6BDd8pvyLYckZEgIlqGpp4A+GJ1xAtjoSrwApVrQRoSCKRNrG3gctX1u2YNboksrbnwOXCipt/p9rsmn7oYwr2JNugX80dJBKz4cYJ2elS2K+9S3mMQy9+0ZCotgzrSepX2utMzDj6CAXUnCpl+Zbzf3C0bdPIMM6lX9FHr5cbDkzDkpXH4O+fvdGCSqWCrY0Qafc9YWFhuOFz1x9S0azjVEz86Re9tv/x48do0qQJLZVKu5CK2JDSekXbtIUb7uk2bRy01s7KkrPAx5074cEhD2trK93+MWqtRB4yJqeq0aBPFMaWs8CoasafDOVBdbPOykQYm0kmLYeDVdgi8IGfhXGYUc8oUjBRnoyBtffATqC7iCdm3Tms8iaDAE02NOPFb8mz6QyhRdEz9jeZjmAV0QoBJgp3ujw+41kV8uyIM5vpjwG0ahibqdAgcD1sNaTxl3DW2lMnbHt5BUZG3umz5B9wlUrFvs5WqMOSuuQVwnznT5Bo4H88HXOIU8GQ7vneS893/fkpuHp3KtavS8dFi/Jw4HzdkDA/stkyLAIsAl9HwE8RjG0bndCkds6OTl+XYJ53UtLUcPV/LyWkn8xLUm2erWC1NjMEmE2Y1fa2tn1UGo1Fz+5dMWDgYKpFixbg8Yz/vzd25HBYBhzBIj/j62Jm/aoTdWniSOGwMR0paemZHE/379+P00vHY1fTwvWaOksYbxe99cCToFef8FMqlWTTzeLT989PmI25hfPmYsOalVjmR6FPee03j3cEyTHrvhRLfnHCd71N24Dy8zaz5zkjIJVp8DhIAdfiXJR05UJoWXTWYw5fFDGOABKFkh6lVmNfzkixd1kEWASKEAIbXKx8Rn9Xez/hb2G3+fXR78x47U3SbZx/NQczPYqhczHj7PHm1jaxmkS/TUjF7oR01PboDz+vIWyEi9xAM5H7m+91wgnL4qjELVrrMyYCPxI0SnyvjkCKqwL717qgHGvEbypdYxA9YhJUcG8WgZ07dmDI0KFa1bl37x569OjRtEQirUEKvNCqEJuJRUD/CAxsY2G37oBN6QIvgHwg0dBbpL9Vt7CrjP0+g7m8PEbG1X9T81dDGrHvqBayWFMeTbHAZq9JLyaME3dRB3jyYdn3qEEcD3NClFYrIVnlTTc5vYMq0bp5Tln1ci/yyCk8mTJZ6Xi1VfaLh3qpFUioc5y+usKJalbPMOOzWPJ/VKZdJB0prGHQSZ1v2mvVcs9+vN6ODK+bYdMD8Ts0e72RvuAQTllS+nNMkdESrJKPxxPqNHZ35KCBG7sP8mVPM/Ylrc+oMXbqPPw4YeKXt3P9Hh4ejsTExE/7XTVr1sy1TG4ZOrT0R2v1I4ypwdq354bV5/cvhCsx5rIEfblOGc61Fuw60efwGOx8iSwOd4s1RfMy4w1WJ1tR/hEQKZJw+fV8yKQhGONqhfYOtiYb1OKH8ASEKIToVnkFnKy88t9otmSOCKTIYrD50Te0hUB+SsjneoikmspCSwq1q/A1w7vb2XZtYQUroUlPJ3JsX15vbj+Whluvm2HXV6LGLlu6GOFPf8eGWYb3+Rg4NQW0tT/2/HFM7wT23bp2pq9evRIqEsvK5xVDNn/hQMCgEzUzh6y9nTV19NGRklbmstD+06KPCLknxXESvZbdaNX/08c41i6+L8fGp3JMJYxAw43ICPRSLUMH0Xt0rLIEpR3r6b/xbA0sAloioFBLcDJ4Eoqp32OKuy3KCwVg2Lo7hkSgrtd3qFfyWy0lsdlYBAyDQLo8Aavvd8HaUu5oQSbVjOHKiaR0rI8TkQnsMnjZF3yx0DAtYWspCgioyCbo4cAxaK5MxlKhq9GaHEX0mCiPQJxAhi3thfB1YTcuDNEZ10kUryHXJHh8vCQ8zMDJmTFY7Pt9AuSPuNjB82HnK4Z4SNg6ijwCqRoVakiDEX7NE05FjK15+OwE6Y5j6dPJQ7CqyD8ILAD6QKCataXggBpU5e+GD8X4iT9TZcqU0Uc9BZL57t07NKhTE2saqNCljPaOiwWqlC2cBYHQFDW6/WWF0MiYLPe6dGyP0Ec3cLy9BXzsjW5Tl0W//F749Z4C3FY/YMHCxZgxYwZu7lmO7mX5WPRYjTZt22LpyrUoWbJkFvFyuRzTJv2MA3t24Fh7HmppOa9QkTXaufdl2PdGgV1LXNCuif6MtrIozV4oMAJPguX4YbEEco0Lpkyfjy5dukAgEGTIjYiIwKoVS3Do4B5smGmDri2KRvRGhki1y/ex4uevFY9TRfQ3BIzoAgPNCmARYBEwRwS4Ao5lamPvIVZ+noPZ/X3Sg9HpweQIwcf0F4TQNQQx4g/EyZSCkGsBK0LqZs3lQUGIAeUksoRCo844ZMS5TkPWpCy5AgjIwSVkmhKVFCpaBU9LG1QVctHXyRpVrCzBMzPj1GOJqVgenQr/0hNQswRDtM8mU0TgCFm/H6tMxLcCNsKNKfTPHRLFcZgqHBsWOaF3W9N0qDcFnAqjDq/CFWg1Ihndew7A/IVLYW+fs3Ho7l07iVPusH+hYH7ASf9+YT9ZBIyBAPE+XD3F0u3HiTrcD5aQMZO/KFTlLHCmrpT9gWvLLRyOTqdTn+Gb8F30Bpu/qIo807XtWCOZjoPW5zWWo56QIa1xjfg1iW8g3dGQ7p4aSvGMEN3ylEs52nqDL8Wv7Wywn1dSrz9VsxppeLPGOBqkzpuPpOjzfbw6mFfNYIvATPTqb6XRdGL1pQafT6rJvMw+cIZmg81FTgUew1mh26Sm1dir+A2HlSuxrjUnY+1ZtzUUDmlvktVocVyKqbPm4ucpzJZp7klD/hvWr1uH+XNmYmAFCp08aeK4zM1k2xEQr8LhMBp7X9GYMmMWJv78C7gksmNuiVn/b9qkMe4/fISIEfZwNoOAV7m1yRD3GTubqTdkOP9Sg9PCcnDlGJSTwBBNNMs6uks+wNZrBHzdupql/oVdaTWxk2HI9JLTH2KZdzESqda8xrkSYq98M02E0ylKvJLKkaiQkrVNPoQ8K8jV8oxDRmw1BWQ9lM/hkuAsPFiSTwEJv8oksVoFCcFASshU+OSd4WFTCsVtqsLFpjJ8HGrDlgTJKuqJebfuDhiOylUicWl70Q5op1LRKNU2Drv2nUbLli0zPRrdu7aHheIhDi1nglQZfEiZoQvTVy2HJyEi3hobN+9C8+bNPxF/ZFJWB1+ePHmCxo0a0VKZrD4R91AHIlkRZoSAcZ5wMwKIqFqKhPh+euh3F/vOzc3DaCM4VAH/AdE439ka1Z1ZpwZ9Pm5PyCRx0l8yJKZQmGXhgfb8nBff9akLI/sjGQi1Eb9HRa+hqFuSsbcxv5RGIkUGxJ9AcNJl0BxVRgMkEjGquLZCPdcBKCb0NL9GsRqDYXs5GDgKPRwpjHfLflGy9+t4VC81C2WL+bGIsQiYDAIb73fEFh9bVMxmch0ikeG7t/Fo5PMjarp1MxmdWUW0R4BZRHkQfQBByecgIpHfizkUR0JSPEo71UYtpz7wdjA8g6f22n89Z7r8I/Y/HYopZDFlqIAJpGa8lEIwXqGIw35iODTWV4DxtQWw5bNTEH32SF8S7agkYfhcP9c8FsFS0zVo0CMaA9KKY6xF0V6o0udzwcpmEfgXAcZ4cKZDJAIvefx7qUh8fohVoVz7SJFMQZtHyO8i0Stm38ipNtbWCxr6NeDs2LWb8vAwzd9UbGwsNqxbgw1rV2NxAy4GVmTXCY395D0la4ljA1zwNCT0q6ooFAoEBAQgLCwMcXFxSE9PR3pqMkRpKUhPSYFYlIbEj4lISk5GUkoaksjGqpoYu3gVs0YFJx4q2cjh70bBvyTZROUZf+ydKqdRbq8MqSIJ1CQcZ+WyPlhWIxVtff4x/mCMcIZeo+Hr3wZ7DxzO1gDnyJEjGDV0IJ71t0TxPLAFM3WPvyXB0zQNDq1yRc1K/zhrfhV89oZREGA2bHv9lAolrzphRj4MZ+fc5zLMs7SDRJ+6duUsQkKCkPAxCSnk95Bw27NQR7sNfClHr4lxorhE9YN0Md2HdFiiUTqNrZRFgEXAkAhQXIobZsN38hleaw9lZWHcfUhDNvzzuhgC178jduBJ9DHUtbXGACchGtqZx7795+0wxPlHpQrDwhJQ3KE5WpWdYjSDJ0O01ZzqePXxBm6+WYIbVh5wZo2gTa7rGOLQPsowtO5kiVWzipFoGMafR5kcSIVYocdBcizZrsCl22nwrVmBzFtpME4f795Ho2ZFIbq30GBUHzsyV/3nuVi+K0U1Z11yikRKNyawvCrE0LBNM2EEHCjO4y3WPrVaWhQ4kG22reyQHqqOonjU3fI/czz52dv3ZFvQRC8yhthtwtap30loep/dE5NdIL0sP4LZ6vG01fcvKUqgn77VtotULw5Bc2eGqltCkMHxCl2/DUEbl6kcz7QyWN1pMx+q2yXEcI+vLqEtRAXKt/VIGlYuTVfdFlQyWBv9016rR7t1or539je4F7fX83mq/vyZ3O6CYTodZF2Q78dqxQRMrc/Bj7V47Nwnm6eSIZmadFeNK4n2OHr6PKpVq5ZNrqyXYmJi0K5FU9Tgx2FzU06esF35VIFVzzmYOGkKfvplEvj8/0hfg4KCsHzxApw5fQrL/Ch8U5Fdr8+KfvZXkmUa9DghQQmREBsFPsSJzuA/5ewVY69+QuCcIhXjiS3G4Fq7YMM3rq3eJ6WK+IlUmYZDz0ajtmUaFng6ZZDzFXFIsjRfRvaXzyWlYV+iHB9VFInI/AsqFm9G3vtF8x0TkRqIPYGj8cMAO6yZXjwLXkXpwoFzIszbTJy4FTQcbDn4ZTAf/TuZFkEdM8+LiFHhwi0pAl8DIeEUImMU+BAjQjFHG/h4e8DbuxRKlamEZs1bwc/PD3Z2eZtnMYQjDerXo589C/xTrlC1LUrPQFFvq04nLoUMTKGNNXV/XD+7Ckt+cvpvpG/ijewxNhbOhNl8tT/L0K/LrpIRQ58rEUpcCCUv43BinMuxxAiOM9qSzWxTYDKOJczLVVKD0MJrOBr6jNBl07OVJVIk4m3yA1RzaZenSXS2wsjFiNQAXHq/BI2a1cO6Davh5uaWbVYm6srwISPx9mU0Ktp2QBnH+hCSRWMKHAh47GZ6tqAZ+SLTt8eDfsa8kg5o65izPfvv0UkIF/ZCI69BRtaarZ5F4B8E5Cox1t5rj8c1SucIySejFccWaFVmsk7eizlWyN7MFwISZSpUGjkSxOEIF9/Gm5Qb5D9nDfr165etPGYStnrVGvw6dz4auY1ADZfOZLEld7bFbIV9dpGJjixWJqGETYXPrurnNFkaRZxth2OChQ2+t8zdSFk/WmSWyoxZdis+YpfyI8o4cNC2LBftS1mwxDCZYSrwt5BENVqdTMe1ve6oXsE8NiZIVCY07x+DXdxSaEieWTaxCLAI6A+B2cooOPZTYv7EorW502tCrOjYn5IJBNnt+kOXlVzIEehkY211vFnTpryDh49Q1taGW4c4deoUZk6eCGtlCjq4ydGlNA/2AgqO5KAJ6CnEkTCNHGKyfiUlfGWMX2XFYlxYEx9GHmscbFKP5WvCFt/rhjVeE2Ndfae0tDQcO3YMi3+dBSHZSN7kD/hqGQ1W17r1uUJh4NxN6N27d4boNs0aox0VgLHV/1t2ZyLQdj6rBO1RDddv/51FBZlMBncXJ9zrZQEvspmX1xRByF2GXRWDR34b+353RckSBrNhy6uqRSr/9QdSNB8aA6mUMG8XMDrKiRMnsG/TaBxbWTTmE8wcauLSj6J7AXKJWEr/SB6cQ0Xq4WEbyyJQdBCg+1VdUWTJSRli3jX3u2Khlxu6OeXNAKboPCLZt1RKDIDqBL5BnRId0K78rOwzsVcNigBD+rnsTgv04ztgpdCdNe40KPraVaYk+0KjVO/wxlGKZ+c8PjlValeazVXUEIhJUGHCb4mSs9clUuJwO4S0/2xRw4Btr/EQsAAleWZfWeiiZ+KGCeJIzWm1hPqr7A9ULSsv4zVYRzWHyGJR59VSerzlCqqrYIiOpOpWzFtVCPpLm9LCEXcpTrGyuhWeR2nyi+NV9tYhnBY3zuR9IS6PdX2enSbEasdtvehi19tSXGfh57f0di7Z/Rpuh4JVr467G2TBcMKSRHXIAZp7wDZnWyRdNTiB2GjUSHtFi2r8TtznC27zkhe9OoZt1ChllTXzrXfqBNvHyhuYJx+InhWVWNSEROkzAZLJvOBhyLzfXlFD4e2Hk2fPExIZ7X7GDAFoJUKSOdwnHT/5/kOSWRCdw1LUeBSnJvtWNOqT9fiKxfLmsFuQugtL2VixBtV3p2E63x0j2WiTJtmtceQdW5nY7ncoMxG1PBheTDYZGwEpsRHdHzgcNnQiTlb0NrY6ZlW/guzV/vQuHndFMvzkdxlcPc83TBWcc68WIkp5HnG3fExVRVavfCCQRMYly3eJse24DGvWbkG/b7QPZnjy5EkMGNBfLZFIS5GqI/NRPVvEzBBgnWyz6TBrK86OWhUt+lzd4W5tYWEeEF27L8U3E+Jwu6ctSubDyCgbGAr1JYal6e8YNV58VCEgRoNXiRq8TVcjWaFBSb4FvLl8+ECAirQQVXnk4AphY+BFBm06IEQtRTdxJOr6ELYVj3+M1LQpl9c8YkUynkQdxKOow+hNNv76WdjiO5UUg+seyauoTPlj0l9i9/ORCA4OQrly5TLdy8uXgwcPYvIPv2Jw5Z15Kcbm1SMCATGncPvdamwt7YLK2UQAza7q5kFR6FF9K4pb+WR3m73GImAUBA4GDsMvTmloYp+7gaaCGK1MfJ+IDyiJHlVWgs9lCS+M0mlfVBoYew5vuafxIjjwizt5+9qyeRt8CBGhT4UVeSv4Re7H0Sdg82E3alAWJMJrIkra10Z9n5Fwtcn//+AXVWT5KlIk4WDAd+hLUZhtIs62XyoZqVYgmIxrXpAjjCNDBC1HpFqJWBJ5oaSQizJ2XFQvwUENVy4akr0tT3a8+yWE2X6ffEdCCCw4OLM5ewKTbAsZ+eJ+woQ2aU4SLvMrwK2ILtYZuQvY6osIAvUUwTiwvTjqVrMsIi0GgkMVqP9NVJxIQhuGirzIIFv4G0qMD3ZaW1kN/uvaNapOnToGa/DFixcxZEA/DC6nwZx6XNYA22DI67cixpHUfkMqiYyj0W9F2Uj/8OEDmjRsgKaOxOG2mU5smrKpJftLVwl54eLI0rj96L952bBB30IQfBYrG2U28Pko1aDhMRVWbd2DHj16ZBK4YcMGBGybhjXEgKogiYkoPPiKBPXrWmL9XGfY2WTWoSCy2bJ5Q6D3T2noOWTtVwmw8iJtxLBvUMX5MiYOdshLsUKRlyEKO3RBjFlrk9JjP6pTyHhnEWnYbnJIC0UD2UawCBRdBCgLjqV6dJ0DlL1l0ZrGKMga4e4nAzCKBAzoW5x1ri3IT0BMxp0DQ+NRonhXNPEZUxBRbFkdIUDTGtx9vwOBUQcwme+EwQJHNgqRjrDVpZjpyg8I9BDh6h8lYEXW2NnEIpATAhoy12ci4m49li47/qdYLZPTMgserqWk07dIucfkeEoOSU4y2HssAnlAoJQ9xXnx1qG6wYwRNkrjsUjxkT7oM5TqbK9dFMQ8tMfgWadFn1ZvSbhPHDnfcC05BoNR63ZKNWK0lJTVWPTcy+GVbqV1OX1klG6pra74Q3dulVmT9CH+qzKfz1ygeXt9P+24t3nBFgG/WkPmG/KbMVBO/Vsl+qukQRZNmw+JVvk+s+PNtHLPrIievnVND9U0dvLTLHbvapD2/duMtfHXsTD6nuaUw8sCDabC1SGYK++HciXisakNB07s2OxfiL/6OfK6Cg1GLsbYceO+mufzG0zgm3q1fdHDk0ShbWoeJO6f618YzxnC1laHRFhq4YUuxF6bTaaFQLRGgd/kSTivlqFjpYXwdqhlWgoWUW2Y4CtM5NrGlkmY6uFYRFHQXbPHv0uE1Ko5iW77k+6Emokkxr43TLIJr/4s2tFszaS78qVmMglo2XjQR9Rr2AU7d+/XSoZcLkeNGtXpt2/DjiiV6r5aFWIzmS0C5uFBaiB4uVx862jH2fzsREkrN2eDzinz3ULGeKL98BjUIBJ+rW8Y9q58K2ukgjGEzecIYXXf/1yJeDGNPrxi6GvhhApc8zQmTiQst+VTX8BN6IWuVX7TuUOihlYhLOk+XkQfRnTacwznO2IkvxicOP/9JmZLYxFcoiuJODok370amvQ3gul9CHzB7CkULKlUKtja2GN8nXOwMNN+LRgCplGaeR9dDVuGuOQr2FnGBc5k50jbtDMuCdc19Qib9kxti7D5WAQMgkCc6A3OBo3FlcoeeapvbUwyjibT6Ft9ExwJIzmbjIfAxfDFGDW7M0aMGFFgJRhSh4W/bEa3MovzLUumEmHd3x3wwb7yp6jHKvL+PEWikq1SpOEjxUNtEtG7knMrWFnodqGUYcr/O2InrkfsAI/EgY9zZEaQ5pcYNvmLhHXusCYRtxQitPW2QL/KFhkRcTnEkZhNmRF4n6ZGo6PpuLzTHbUqm8+GyLjZH/H8khpHeGVhwfZr5k5lv7EI6ACBQJUEo4XheHXNUwfSzEdErd4fUp4GK3oRja+aj9aspkZCgLK2FDxzcXOrcvv2Hcrd3TBj+oSEBHzTsyuUH4KwpyUFV6sC2Z4YCTq22twQKLNXgQfPX8HDI2/zzNzkans/NTUV/g1qo69znE7Y6LWt94cbSlg3GoBV6zd9KhIWFoYGtWvicGsKfl8Eijj2RoFpT4V4GPACrq6uGWUG9e8L99DTmOunu3Xwi++UGHVNghF9bDH3h2IwF9LNTyCa+YlMrkGzoSno1mcipk6fne/WTBw/BoqEI1g/0z7fMgpbwfAPSqzalyrfc0qkJuu20eliejPhh2N2iqMLW1vZ9rAIFFYELDmCNw29h5Vp6DmoSC14iQlh4JaHPXG+kgeK52GfqbA+B7pqV5RciV6vY9CH7FmUsCmvK7GsHB0g8IHsx999uw4J4jfZ7svroApWRD4RIIMoDFaFw76+Bn+sciYRwIrU6zifqLHFskNARaK3BRECwJuPZLh0R5J+87GMR/xyxRRFXRaJNetJmXvkMDwbV3bKstdMHQGGGXxSQ57Nj2dsy+p2I1mLll9WpGKENIpe4NaFnuDSzKwXLlPIHkmVlws1takumGG90STb0lJSTqVsMp5jUe97o+lHy9MhWV2KbvP0KmVfpaIWT4lusihT03DavRLt9KwLpW0EzoLUrP4gQlKHi7T6gZdB/uw9mr/X/C724rTh638dS0MWg0qmBdMxVRdRjjzDOZVHkHld+aDF9Cn7EMqBk3cHlURNHBaQiLUyq+fY14lCaQeD+FsX5DEyqbLDrtFo+/MqDB48OEe9Tp06hVHDBmFxXTW+qcDPMS970zAIJBIS0uYHRRikdsVYgYthKmVryRWBADJuWKNIwRVlGgkQ0QH1vUiUVGJTzybTQeD+h32Ijf8Df5R1AZe1MdNZx5xLSsPGZHsMqFn0AqDtfDoEg/rFYtEEJ53hyQoyPQQYv5davRPRsGlvrN+4TSsFHz58CH9/f1omkzGhcA9pVYjN9L/27gNOivJu4Ph/Zvv14xodlCKIiKjBHnuworHEksSaWKKxxDdqEl+7KZY3amISo9FEo8ZESdSoMUVj74qAUgQBgeMaXN/bOvM+cwiClNtddnZnd37D57i73Wee8n2em515ykzBCeTkwrAAVHYOBrTX/nXPkNJ9dyuchZfvzI3KYWc3ymsnVshInua1vplZT6n9w4cxufbViBygV8h1/mFSX4RPwXol3i3/q+6G0+Etlx2HnSST66dLwFu63mGgH6yn0y7rfFdWtL8py9vfFi0ZllO9ZXKyuvPRdp6tL8A4qHeFTJl4o4yszHxxTp+64Lh71okyd95sGTVq1EDZ3er7Xzn4CPEtnyK7Djlmq+F4Mz2BuLrT0psrHpb/LrtH1J3St7ig2wo388NLZIi5XO4YXSN+Pf3+3T3nLJOzd39MXXxyQppeLRE6FwJ/+/BiOat8hUyvLk87uX92dMs1yzvlmB1vUXctm7rZ/a2bGzw65xIp9VXKtBGnM8Fls0qZv2g99eBX7x2bnc+bQ44Q77JJstvQ4zPPkNrzgbe/Jvd7Q7LLVgYxes2k/F0NVD6knhz/gVqYO7piJW4nMwAAQABJREFUJxkyaG8ZXbWb1JeOXb9AN5WMfNr5gcxveko+av23fCtQI/8TqJWQlv6xOpW08hnmCdWheV1shezYoMtPDgjKDoMYaNmwPi74b1ji6ua7D9yydoHChu859ef+Cfeqw2rPxgq52pebxU1OtSBfCNghcEniU9ntAk0u/qb9g/h25D+TOO+b2SWX3bx6pnqKxLZ9mGeSOPsUisD25WUlH0zdZWrpM/94TistTb2fJdMCWh33P7npBrnj1p/JfQfqcvBIX6ZRsV+BCFz+alyGnXiVXHHFFXnN8UMPPSQ3XnaevHG8V/Up5maYYFFHUr48Myb3/uGhjZ5Se83/XiXX33iTdF5QJX7Pxnl5rTEhzy+PS1VAkzMnBdS188bvZxPxHtXffu1bffLj79XIuSfx1Lxs2g4U12rVNnY9sV3uuf8v8pWvfGWg4Ovfnz17thw+fX/53bVBOWy/3E0UXJ+BAvshHjfl2ZfD8vsnunueeanPFwrI6+q86DZVjGfUF5P6C6w+yW5xCwT14NKpQ48defD2F9v3wedQwgff+4ZcOzgm08o5rttRRSd/3CoTRl4hO9R+2Y7oiTMLAtYY+rsrH+3/2kONIfxE9edvP8DYfRaSJYqtCHSr8ZoZ8Y/l2FNCct2lPA1nK1S8laHA82/0yZlXtfaoJ6l81B02j1DRrM4wKnYrHgGrU+LoKvGcERZj3z28pbGT/IMqjvBVSOUGD0jIV3EXJiJyeO8S42vVXzJ/M+IktY6hcE9ZH2t/X85Y9kfznrKXte29O+aLdIvpfr33y4llE3aUwFG/Sf0pB1uMLbM3ko3vSPRPM4wTwktyOsHgrTO+k2jqeN1b9fO9M8t4mns1j/uT2fnaKK2i3P45Bp7Jn8jy8p0lmMH8ujSLJef2LJVg2bj4g6NPz+nAQ/Wsq4wbSh/Uv+Q7MOUs95m9ckf0EnlPe1IeOFKXPYbkrdmnnGcnB7zjg6Rc/VpYrrn6apk8ZRepqKiQ5uZm+f3dd8lrb7wpp+3ol+umeSSUo7EBJ1s5IW+xpClHPdYrY7vK5dbgSCdkydV5eFrNP7sqqk7J1Vzm3dSC2h3rDlLz9HL6Mehq/3QL/+zCG2Qf7R05dzDX6+napRL+5pVrZGnoGNlv1NmpBC+aMKZpyG/eOUlO/1qX3HZ5+jcMKRoIlxSkL2LIN67skrc+1ORXv7lfjj766M2W3JrbM/Pxx+X8886Ss48LGT+9p8X6cJimvt7e7A68WLAChdvLkR3y6tKQNuvGiwcNveSblQV1VXbyxU1S1ZaU27/MAKPVFHpiplz2Qp+8ssSQ63zD5Ygc3OkrO00we7FYi3FmqYU4byV6pU1MWaOi7rC+1OtlossodZI/WnVsDlMLjkfpAZniCWV8x5aduz+RY6beK4NC2/7Uo6eX3CCHnjhVfnLL9erur+ldiKi7QMiOO0yWqaVnqAuZg7OH6fKYOiPN8tcPL5IZ5VG5YEiVWE9YnDJroVw4baZUBYes11nW8b48Me9yOaWmRC4akvkFihX/1Fkfyw+//Nr6uPkBAScJ/HvRT+Ug/XX5el3m7XxeOCLfWdIqY+sOl/1GX7jRU7fvfedE+VGDIQdVlcviSFROX9QsB429Sj3J9CAnMRR0XmLqRhL3zvm6PPzn38v06dPTLsvTTz8t3zjlTPn6hOw8mXjmrPPkaqNbDlUDoZlsTUZcfeaHpdGMywr1dNqlKhLre6nmkSr1WV+tnlJbpc4FJup+maZuwDHKhRNw2pXHLbEm+auxRu6ZXiJfGZ3TMaNMqjUn+8xV1w+HPdkjb/x5mIxVTwAulG3pyrjsdXyj3GyMkCPVDWHYEEAgOwLWNeTE3rnS9OooKStN71osOznIfSxR1XdQuvuSeNK6TBaJ5T4HpOhQAV/A63na4w8c8n//d5uce+55OekvffXVV+Wk42bIyaPj/RMZPDwRx6HNI/vZaleDNOMfjEhbe6cEAlu/0V32U984xvb2dpk0fozcv19c9h+Ru/PDhHpkzzNL4jJvTVIq/JpMUDfIOTCH6W+ssOlvcTWh5Ya3I/LAwpjc95N6OYI++E2RbHiluS0hOx/fIc2tnQPGPm/ePDnkoH3ktssCcvIRjJEMCDZAgFnzonLnQ519jzzda6i/z2sTCblT7cK50gBuvI2ATQITfHrgw+Mm3qiPq9nXpiScG21XtEUeeu9UeWmn4c7NZIHnrDEWl5M/XiPn7/lsgZfEPdm3bpz96pK7pG31y/LX0uEyXPX7s+VHoEWNzRwSXyC/va2Wa4T8VIErUr3/b93GOde0xtU5+Q6qwMtcUWh3F9Ia9DqmSvOc1Wca0/bxlsdPClSXT1djyOVq3NfpW4caj923e3Fyx5IR8vcx53uCBfoADENNoD/w4zuTTRG/+UDFW46bP3p179nGv2s/MUNnvJC3RhF/59eGZ8GvjaOWvJ0zn57FS+Rfux1g1Lx/VE4GsNr2fdL40/fL9BOmW8NH9m2LlsVltxNWGksCO+ekXKM75xnvT7hSHxOos69QX4h50kc/TeynnamdGbpiwDabVGOVD8R+Kn+J3y53HarLsWM51/0CJ7+6QOAHL/ZJbEmpXK8PT+uBCy6gyWkR/64W1n430iL7bHe+TB3yVdEL4Fwsp0AOTay19xP5x0ffkWcmDHZoDgs/Wwv7onLhp3E5Uz1Ey43bMwt/LE2x/8jKl2ljbqn/pJon8B91M7S/PZ+QWQtMCQU94vWYssdOhpx6ZEjGq3nI+gZze155V92E6rxV4VjCvDEWk1uVU9wtVsVczpxMGnMgoFZeqj19yF6h/R+/vaGkkO6oNm9xTPY9daU8f2y565/OlVSTsX72VlQ9nS4q/xcYKUcx4T5nf2oHqyfZTp5w/RafyphuRqxB8/vmnCZLli2S2trU7vgx46hjZNmcHjly5LXiKdCO2nSdsh2+uedjaeyeJy09c6Wle4609DXKHuVlctFgdXwJbfpU7z+0tMvbvYZUqJOFGdUh2bU0lNFTazdXjrub1sjr5jSZPv6qzb3NawjkTcD6O3ly7vnyzx2HiTdLd2BtVpNY/rK6W+ZFTKn0GHL50EFS5d24bzmcNOSupnZ5rjMiCQn0P922rnyKDC6fKMMrJqsn+4TyZlLICc9peVaemH+9LFq0SMaMGZNSUU456RvS/E6p7DP8zJTCpxLogbdPkvu8QZm6lSfZphIPYVIT6FIDM5dFP5WPfL3ywFEhmVybs7G/1DKYh1Bf+0ePjN8jJLdemdp5Vx6yuNkkn3slLN+8uFX+7h8nYz2bnqtsdideRACBrQrcFm2S2HF9cssParYarpjePO6i5u6//qf3u6pMfyimclGWtASmB33eG9Si2t0a6uu0yy+/Qjvn3HNzMnA9f/58Oe3kE6SiZ7n8+suajCjPyTyatHAInBuBJ1Qf723LR8lbH3yYmwS3kop1x9Vzzz5Tet75W//TlLcS1JVvdUVNuVidh77Xacifbm+QqRPzuzC6mCvhhEu7ZPf9L5Yrf7j5/sGEmmX+/csukif++qD85bZK2U092ZjNHoEX3uyT037Q0qOecvtwT9i4WKUSsSclYkUAgQ0ENPX02mVDKyYNP3ny7ZquubP/Kp6MyB2vT5fXJ49W409uncawQauw4ceHW9vlqcgOMmPHm22InSjtFvig6Wl5ffFtMqt8nIR4io7d3FuM/+Z4k7y5Xaf85+HBG02q2+IOvIFABgIPPNltnn9tW2c4au6udl+cQRTs4iwBqwP+2GrNc3bYNKYe6KtIfM1fXWbdkLmkCI7nhqFuKN6zOBHzluovj7tUr/OVO0s/xdzM7lspey28zbw8+CvtsMDJKe6Vm2APR+6UX+i3G6EL5qlLhfz0h0QePzkxZLLu2evP9+fsRP3FQ7+a6Bnb6i2/fIrt0O2nvZC4cGzEe9vl9o6XPfNSWM65vC0x27uT7Rd914ZXylxfVfz5cRfn7O6KZy/7o8zpCiZ+Uf73rZbvmehDcmfsUrlyT10umurNyfiM7Y2IBBBIU2CVmoe754Pdcq9/e9nHZ+8C/zSz5qrgr8V75NieT+T8aY9t9AAiVyEUcGGT6mZYv3zjMPnL+AYZEeBGDXZU5WtdvXJzW4WcNOV3dkRfEHF2RFbJ7947Q751oiZ3XmXvuWJBgJDJLQo8+XyvfPua1nAsbj6nxljvUAFf3GJg3nC0QM4uep2ioB4O8P0htd5rZj0+vLSywCayXXnralnwWp88Mr3UKZx5yccf1V3Vr/hvRC71DZbz/XVcZOehFu6OtMrM8sly2IRrspb6gwvOlD8/+QeZNs16avrmt+bmZjn26OOla4VHjt7+uo2eBLn5PXh1nUB3tE3+tegmWd45S06rq5SvVIZkbCg/Ha/r8vTF7y3xhBw4d7EcMfZy2XXoV7/4Nr8jkFOBdjWA8scPzpEZVR65ctignKY9UGLd6tFrr6iL19+19klrwqueenu5jK/Zb6DdeH8DgQ9b/yX/bbxT7v/DvXLMMcds8M6mP+48aVfZ1XeujKjMzsDN6vCn8tj7Z8n8inGbJsYrtgt8mozKebGlEqpKyN2Hl8jIArseyCbQSyvicsYLYZn37EgptOui63/RLjMf7JMnfeOkjLtHZrNZEJcLBaLqDu1jeuZI6+ujpLTEHYv9nn1ZLdj/Qetrq9uT+7iwyou1yNZdIyarr1G6LqMrvbKjrmljEp7gDt2RaIlaSCu77jJFpu21jzZlyhQ5/PDDxe/P3SCbdYOX0085UTyrP5F79leZrNj4BjvFWimUa2CBD1oTcthTCXn+pVdl6tSpA++wQYiPP/5Y/ufiC+X5/74oB2xXKmNKYurGUKa6i6ouTy7TZLVaDvftc8+TH119rYRCqd2kqaenRw7ad09ZuXi+LD6rYoPU+HGdwPJuQ85Sg2N6tUf+eKsasB+y1bli63bj+wAC4T5DJs5ok/se+JscfPDBm4R+/fXX5fivHikXneqVK84uYzxgEyF7X7j1/o74NXe1d4T7TOuRmgvtTY3YEXClgPVBHZ44aF9jxsQbdB83FZNXP71Pgl1/k1tHOatffl3rnB+OyMXLVqvb0ZfIqOo9RRND2sOLpKV3mVw4pFJOqa1cF9RR359r75bvLW2U49TY7o71hzkqb2QmPYFYMiy/feMYeVY91XY8x4z08LIYullN5D0gOl8e/XW9HDAttWuuLCZPVC4SuOqONbE7/tj5cU9Y3bFcnTO4qOiFWtR6lfGvqifTnh0xjcmH+iqTJ/qrSg9Wi2mDRbCYNpVKOaNnafJ1I66/PP5SbUKwMJ/29L0VM5MPrH5fHi//WD2d1zk3/X0v/rJ8J3q8GTp/tqaX5d7WulFf351jjN1/ebU++vRTUmkO2xym7bW35JXjTk0Oen267Z3q3T+bZew9Z5n+r3uHbHO+txbB7Q90yu/v6E08H5pge8fiDp0Lkk+op0zvXbb91rKUtfee6pgjX1/6qPls5VLNu5kbR70bf1Guj35TTpgYl5v29UjA67qp61mzJqLCF7hRrQN4aa5HZgbHqiem8reQrxq9ta9Z/lk+SQ6feEO+skC6WRAw1ZyXhz74liztmidPTBjtuHnxVhGT6jzqR8vXyL87umSMmoNaFRotCSMis1v+JZPVw67u2q5WQmqig9O2PnUzncPnNcoxO92lHhK0g9Oyl/P8WPN+f/3OSTKs3isrXhiZ8/RJsPAE3ldr3p57tc944vne7vc+iob8Pq3D75P31QLcF9Wf11uqRG+or97CK1nx59hNZ2d7l4S0f73+0NCSnXdw1sKygZpZJGrI5KNWyA27BOTYsbmbBDhQvnL5/pqIISf8LSwju0vl5+qptQGXdD7m0jjdtA7p/VRGb3+JTKo/NN1dNwpv3Unm3jmnyoOP3ivTp0/f6L11v9xyy61y0/U/lSNG/6+MGbTHupf5PoBAT2y1PLfweukLz+ufjDCxxDmdv1vLekydOfyupUNmrukTj7da6ssmSr16gmfAWypBb7nE1QIpawA5luyTuKG+1O/JZK+66AhLUv0u6oLEq8J69Qrxq+8lvqr+E/y60u3Va+48hm7Nuxjes+4U1NyzUNb0LVftQj35Ndkl8US3ujiNqjoPicdT0v/d5yntX5zvUwMhfvVawFvW35asu+OvCX8iq9VTnZd1z5dDqyrk7LoyR15wb66+rIvZ2xo75JmOiBw67ocyofaAzQXjtc0IhOOd8vSSa6VmlE9mPvEXaWho2CRUPB6XcdtPkL2qLpCxg/be5P10XjDUE1V/++YMeTBQI9PU8YktvwLvJHrl1L7FcvNBQTl1QmFdH2RLrjduyu6Pdskvb6yXI/cvyVa0OYknmTTliDObZeiCoNzmo+MqJ+gkUrQCN8VWScmpMbnhkuqiLeOGBYvGTCnZbUlCnUJZB39jw/f42bEC1mzVGVUBObcvIXvvP9wb+dJgb9mUWo9n5zrPFhetJg1Tpj2elBvv+oN89avp3cjJOge0FnU1NjZKTU2N7LLLLlJXV5cy0MKFC+WCb50h3cs+lN8dIDJG3byHDYEtCSzqSKqbn5hSM2aK6h97XOrrrfmYm26tra1y4jFHita8UH7zZXOLbX/DPa0bq5zzksh5l14hV/zgRyktTozFYvK9iy6Qpx97RB46VJdd1UAh26YC1iLp0/4dlt13DcqvrqsruBvXbFqi/LxiqGP14P2b5d1Z82XEiBEbZeK8c86UF/8zU578ZaWMU9ftbPkV+Ie6UclxlzT39kXME1RO/pHf3JA6AkUhcKBfDz23U/0h3sPHXalpjHtuVKnvNs6Uj1bcLQ+Pq5cqrzPOpXuThsyYv1IO2uEnsn31tI3yu+4Xa9xq5tyLZf9Qq3xPLbjN5vantg65p6VXYqZPhpaPl4YK68GGakhM/bP6nftiTRKJtakxj1618Ff9UzemK/EPliGVU2WIGmerK90um9khrjwLvLniTzKy8VG5OZT7BTZ5Lrrjkv9xvFFmje2Wf/1xcErXW44rABkqCIFEwpSDzmzsfW9+/M+9YeOsgsi0OzN5VHlI+2syonmuCw01z/TXqDUzbpqWuXGl/zi8Su6Ot5tPbX+edoA6dym0bXWiRybNu8nYVz9Zvl/6c8eseGgzmuXo3slm4BvPap6hu+Wc1ehpkr5f7WQevWKWFlQ3t8zF9tzO+yTNk0o9JafaexPzvr98IhW//SC+/KlhtnZCnfGjlkT3017vPWWjbeX7vXoYx73JePKjHf83JxdU3epaqGH2NeaDFW9owz3bry/bkuR8uSb6NRk/uFV+8xVdakKO+XNan0d+QCCXAi1hQ6app9f+2red7F+gT37PpZfdaY3tWiCn7f6olAdy85lmd3mIXy1mNRLS3LtQVnTNlabOt9U84rBisXrPTDX3vEpC/noJ+q37d4vqPdMlluiU5q73ZFXvEhmnHlr1/SHlsrNa9JqtrUc9TOeUj5ulYdDhcvCYS7cY7X8W3SJ762/JmbUljulbeLcnLBcsWSOnTvmt6lf8/LN9i4Vw0Ru9sXZ5eM4FUlLeIu88XiODqhhLd1H1Z7Wo1vy1N2dH5MW3I8nnXg13vz03Ggz69b919Rq3qoTezWpiRJaWgBt6c2rV4trZd/6gpuHs4ysK7irtLfWHc+x5TfLeyeVSFSi47KfVGDcX+L2WhBz31165zDtUzv7sxGZz4XgtPwKX9TXJW6FR8tWdfp7Rid3q8DJ5ZOEFMm/BXBk8eONBwGXLlsnhXzlKgn0j5bBRV4pHt7UPKz+ANqXa2D1PHpt7Uf8J/3E1PHlkHXNMTdpbFInKS1198lBbt1QEh8mkwSfIzg1HqvbFSe46Jyd/X9n1ocxe9Zh81PaC7F1RJicPCskEdXE7yEf9rau3NeqJ0NaF+Xb1M2T/7b677mW+pyCwoO0leW7ZT+XOu26X008/bZM9TjnpG7LibV0OGPGdTd5L5QWr82bm7O/K82XbyXYedy7oTMUpH2GsO86fFFkkX1L9QXcerBbm6264RNpY+gp1l8yuwV65/2ebX9CxcWhn/bZKLW6YdmyjXBMfJsf53bFA0Fk1QG6KQaBJHQe/HJ8vbW+PKobipFSGXU9Y0fH+vNhxKvALKe1AoFwLVIe88iN1/6TvnLdzQL90t2CgfhuetPxqo+pbeiYm1//4Z3LhhReqm/BsOqdk/vz5cv3//kCe+8c/5JxJXjl3J48MLv28H84a9L76bbWiaLnIL+++V4499lh1c6e11yFJNTi2YMEC+fnNP5HHHn9cjhvrk2t316RuG/Kca3DSc46AdaPB+z9KyJMr/LJ4TUyCAb9094blJHXTyO/tLOpRzZu231RyH1Y3Vznxn4aM2ONwuf+Pj6Tcjzd79myxngB916GVctbEz/8mUknTTWGeWxqXc/8blrNOKJfrLhokPp/7rikyrW9rkW3lHiulubVdSkrW3vjHmoR84uENcv/1ISnlWJoprW37zf8kJvud1hjuDhvXRqNyi20JETECxSng8Wm+l72e0J4n7XSrNrxicnGWMoulmq+e5vDsxz+WbzdUyWl15RLI4xMdjp7fJPuM+7GMqtp1wBI2dn8kM+dcKE9PHLbNi4QvX9YmT69ZLT/c72X1dBvGQgbEd0mAfy64SY7rel++HWQisBOq/FN1Q+gDowtkzjPDZbjqZ2dDwC6BJjUesvNxK8JrOo3zVHfUg3alQ7wZC+wyZqT3xUXPjqxIqptzHHtBS/K/r0T0W0LDza8FBrm2U+Uv0TXyvb5V5q9GnGyeXrNHwTk8vOZt85zlfzJ/X/qmPsI7NuPGkc0dDXUH0f37Rie16Tfrvskn57wTKrHoOYk/e07y+M6FmXVUpomx8m9PyzuXXBqvfuFQWycOxt5plfB3Xk5GXhpua7l2/9qK5IzFNZ4LQ5veAD5Nmq0Gn9K1MPGLkV/3Hls1ZavhsvVmw+xrE98N3uo91H+CdBpr5MfR06U99LY8OkPPuE87W3kjHgScIvD4xzH58fMJ+W9ognhcfCMOp9SHlY9DelfIpAnXyegU+nqclG/yYp9AS+8i+e2735TbRg+Rw6q3be79K1298sMVYfnW7o/3PxhooFz/95M7ZXz833LZ0EEDBbXt/dVq7vG1KzpkVp8pX5/yO6kMbryuw7aECzjit1Y8Ii8svVuuPCck1383f3VXwIRkfTMCfWrOyI13d8Rv/0NnOBw1j1BBXttMMF6yUSDnF9o2luWLUevlpdrTh+1X8uU/39ZQWI9l+qwkv3iwUx77U5c8N6Psi2Ur+t/vmxOVa1+OykOh7WU3nvTmyPp+Nd4jM3oWyXm7PyK1JaMzyuMTS6+UG+68VI455pj1+z/yyCNy6qmnyvQxl8mXhlk3xWdLVcBagHj/rG/JPWOGqwWIPCExVben13TJTSs75IAx/yOTG6xzETYnCUTUE2kfmnWm7B5SnwvDqyWYx0k0TnIZKC9hNXB39PxPZcSgQ2T6+KsGCs77Gwj0xtbIz984Uo484mj5+9NPbvCOqLtO1crpEx+QMn/6F8SGmZBHZ50nB8fXyE9sHjDZKNP8krJAQq3kuTT6qcwLdctfjyuVBpdN5p5pdejPi8msJ4arRUOFd5n4nzf65JQLWuQp/zgZ5wmmXO8ERACBtQJfMxfJJTeVylEHuOM64vrftBs3/Lr95kRCfkAbcIzA4HKfPDqiQt/1z0eWldn15NcXlsflj4t0WdTtEa+a0jW5KiGHDDVkunpCos+T2uefqc4ZmsOmLO0yRC0Glh2qPRJQ027c/GQKx7QiMjKgQK9abFv76w454xunyH0PPJRyu3300Ufl9ivOlReOKbi5kAOaZDvAvXOjcs1bfXLTpTVy3snbNgCf7bw5Nb6FS2Oyw5Er+rP3P2fWyk0XlYvfn9ox2allckO+2juTcui3V/UuXBaf3d1jHqfK3OSGclNGBDIU+JlX939/3xHflH1GnqVOG/k8zcSxK9oic5qfko9bn1Pn46tkXEmFDFcn4vXqXHyIXz2Twlwba1I9maI5LtKW0NR3Q31FpS3WJ0F148NBgVp1HeDvf3pFwohJU7hR3cQzKMcMKpHjB5Wp+FREm9l+07RG3pK95dBxV27m3c2/FE9G5N53TpCfDQ/JXhmO2f3o09XSXXGC7DXi9M0nwquuFPio9d+ydPHt8q/SEa4sv1MLHTEN2Ss6T+77Za0cvGf2nnrj1PKSr/wKvPJupP9cPBI191I5mZPf3JD6BgJ1pSHt0553tttokCoaNeTwbzUn3n0/6rmjZKQ2w1+1wS7u+fHdRK8c37vM+G7dgeZNQ4+2dRFjtlWT6hi/78L/S3bHKs3flb/qmLspzOidmmjf7SjNf+ANOfeMvXhdMtT9T3P6rOdz4vHkkAlG6NZJemBv+xZ6GKsj0vblp0zj3VG2dkoN2mup+ag5RrNzPuyL8W45N9JstEz+aU4uPvdZcHtySOIgs8Zb5Z2Z+IX87jCPfGW0rWuis/1nTnwI2C5w9csRWTI/IHcHR9ueFgmkLrBS9Q3t3b1Ezt/zKQmwTiF1OBeEfHjWGXJxTbccUlWeUWlvWrFG5mu7yBETrk9r/1+/eaTcPbpcJpZsdEmxPo4PwxH5fWuvvN4dlj4jKcNKRoihejp11d/cG++U9mi7+lmkzhdS/aV+qVU3Ba7zqLHhDeYBxlUn6qq4Jm1JTeaH+6Q9EZNhZWNlYsNXZULtAfwtrNdO74c+5f/E/KtVf/McefTnFXLE/u6Ye5WeEqEzEZi9ICpTjlspgYAcrm5+/I9M4mCf9AU0rSa4JP3dnL2HZkq5Rzerh9Z7dL1An0TVuiYpXnUX90J/eq1HXaqrdT4pb11RU7rVV4N6aqlHbO2zSDlPBPxcwDoZ61Sdh2H1PeSrVgPR29YhElWdqJovKfUNdRJWJ2ttrW1Skka8uscnRlKNlLt8SxoJ6Y2tlkFqhnCgQI95TqjChJqA0RpPSol6Cp41ycLjC0hSTb5gy4+ANXG9N9YmId2UCuvDpMi2gN8j0VjS9lJZH8Ftql17PCUS9GZ20W97Jh2agDUpK5xYI76AV+pqayWqrlDaWldLqa9WTUTPPNP98cZXq1lfCalTTx7w2nC+U6LaVzgH7StzBefv2anqxzrnGV6muerJtgl1/bGq15RhDR513Nh8Qy+r8kpPR8KxldihFjyFe0ypV39fug1/X44teJFkrDzkk+4+zu/zUZ1hMylhvyH1tTmfi5Gz4lbV+aSjdW37iqp+h+bVybihaStzlgES2qyAZppV6my/oq5E0wNb+OzZ7I4Oe7G62ivt7c79fHQYF9lJU6BmkE9Wr8ny56O6MGkKi+g+v+qXG5zSNU7jyhVSoz4r/AX8t5om/bYFV8YdMVN6VNXVVusSCjqzb6NueEBaV9D/tW2Vzd5qcr+0ticN1ezV9HGtTYn0d3p5avwtydWxeoQQsEPAUxdoTrZG7X30z7Zk3JQyda5b4/OE1I1ZKtTC2s33s2xLEuybXQFrTKIv3iGGGZPxDWXSuaavP4H+saNEUioCmTW3sJpgZRoRNbktvZHvsHpCWa/hVWNW6d/wMbsyxJZtgfK6IdLduirtaK022hltlgrNI1Xqi82ZAi1mXErKNakoz8/5/7CxIVm5aO3xy5lC5CqbAp3dhqm+EqqP0zqopDEzK7NceIaEGpOr+oZmtrc79tIMc9TIod4tnvqpoUdpakuYcauvQPNqQRfefMW6IUqTETcDar5ZvVcdMD/bhjfUy4rmlnW/OvJ7TI0fr4p3mVVarao7ZzzvZbWpPnnUaYFWWr/eMld4Zm+T6Q35NX9Nte1Jxrt7JN7VaeoNwYzK6ZtYLfF57QPmM7kybA6r82g+tSDFjk2dzsmnjQlzhG7v3eVWqb+xCm9IyvXMvNIpe2uiRy3yiZkVAU2rtO5Z5MJr3xGjQrJ8Gedf6bQbt4Vt6TUkaHikPIPruOF1ZbKitcdtZDktr6nOTZYbcTWvstR1cysrh46UzsZPc+pdKIlZfTDd6oZ/9ao/L51hUasfsVk9DdZ6gIs1Bz3dLanaYlg9wGWwSnfdFlFz+doTany2v40WzkP7qoaNlI6V7mxfSWv+VUyde2pJGVyri13nluvaiBu/j9qpVJbN7XVV0WPqZupNrUlD9QE1q4IzuG9j7ftGlj2oNSw+WR3S2RCwR8BaKJxg0Z89uMQq3phXEn4msdIU7BHw9fklHorZEzmxul6gojspXeWfXwy7HgSArArUqJu1rB5E+8oqKpGtFxisJho2ael3BK6PgB8Q2IrAkKa4rBq8bTfy2Ur0vOVygdFmRJZqm7/jp8tpKH4WBLbrCMuSKmdMrMpCcYjCYQJj1/TKokHc7dZh1VI02Zlg9qm7afOkraKpUIcVxGwKt2mDS2odli2yUyQCat7u+2qu09QiKQ7FcJhAcKFHIuPtv0mmw4pNdnIkUL+kQVq2s+ZCsSGQfYFdjV55T+f6MfuyxPiZwGvq+95oIGCHwLQFFfLWDl12RE2cCCCAgK0C0xrb5a2h9i82t7UQRO5Ygd3fC8s7uzL+6NgKKvCMDZk/TFZN4J7kBV6Njs0+7cuxVVMUGdvb6JLX9IqiKAuFcJ6Auh/atfm5faLzLMgRAggggAACGwlYdwViQwABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEhfYFKDsMA2fTb2QAABBwiU+WsckAuygAACCCCAAAIIuEfg7fAyHhLjnuqmpAgggAACCCCAAAIIIIAAAgg4VoBFto6tGjKGAAIIIJBPAcNkkW0+/UkbAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAoLgFdtGDWnGXkNIhgAACCCCAAAIIIIAAAggggEAhCLDIthBqiTwigAACCORcwDS5UWbO0UkQAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAgRwKsMg2h9gkhQACCCBQOAIe3Vc4mSWnCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCQtgCLbNMmYwcEEEAAATcIeHS/G4pJGRFAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBwrQCLbF1b9RQcAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBwrwCLbN1b9zkpecLISTIkggACCCCAAAIIIIAAAjkQSCRykAhJIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACORJgkW2OoN2ajNetBafcCCCAAAIIIIAAAggUoUB3L3fRKcJqpUgIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAgi4VoBFtq6tegqOAAIIIIAAAggggAACCKQnEIuZ6e1AaATSEFhZJu1pBCcoAmkJLIn6GtPagcAIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIOAKAa8ZTriioBQyPwJG0hTTo+UncVItegEjLmImOIYVfUXnqYBGRBfTpH3lib/ok42q8y/Tw0K1oq/oPBXQiBhihmlfeeIv+mRLkob0cg1Z9PWcrwJqXq1b9VFU5yt90i1yAU0i9IEVeR3nsXhGJKnOv7h+zGMVFHXSRpT2VdQVnOfCJTXVP0H/V55roXiTT2rq+EX7Kt4KznPJTE00ofsrz7VQvMkbUfX5yPl98VZwnkuWjMZpX3mug2JOnvP7Yq5dB5RN13QxOAFzQE0UZRaMGP1fRVmxTimU9RgiwymZIR/FJpCMWvNzGB8qtnp1SnmScdqXU+qiGPORjNH/VYz16pQyGXHal1PqohjzkWD8sRir1TFlMlX/l3f/SXc7JkNkpPgEPImkJL2e4isYJXKEgGZ1UgR4ILcjKqMIM6GHDTFKaF9FWLWOKJKnW30+lvP56IjKKMJMlLe2S3cda9SKsGodUaTyjmbprmpwRF7IRBEKaJq6SxOTdIqwZh1RJE21L7U5Ii9kovgEBrculVF1o4uvYJTIEQJDmhfL8IYxjsgLmSg+gRHRxTIkQPsqvpp1RolGRT+WhsA4Z2SGXBSdQNeHC3asmLRD0ZWLAjlDIOiLSmR8wBmZIRdFJxD0RCQyIVh05aJAzhAY2zdXKkM7OSMz5KLoBExNM1QPa9GViwI5Q2BMjzp+TeL45YzaKL5cdEYWS2WQ/q/iq1lnlGh86B0p3X53Z2SGXBSdwLjO2VI+aeeiKxcFcoZAKBGWvkklzsgMuSg6gVC8T7WvUNGViwI5Q2BS+F0JluzmjMyQi6IT0DTdYPVQ0VUrBUIAAQQQQAABBBBAAAEE7BHwaJo9ERMrAggggAACCCCAAAIIIIAAAgggkKJAnX+0P8WgBEMAAQQcJdDS87Gj8kNmEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAYK0Ai2xpCQgggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAII2Ciws+6zMXaiRgABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAgUwFWGSbqRz7IYAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIFKpBMmgWac7KNAAIIIIAAAggggAACCCCQS4G+5SuG5jI90kIAAQQQQAABBBBAAAEEEEAAAQQQQCDXAiyyzbU46SGAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACeRZoWZXIcw5IHgEEEEAAAQQQQAABBBBAoBAExpW0jS6EfJJHBBBAAAEEEEAAAQQQQAABBBBAAAEEMhVgkW2mcuyHAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCBSowOAShogKtOrINgIIIIAAAggggAACCCCQU4HSENePOQUnMQQQQAABBBBAAAEEEEAAAQQQQACBnAvQA5ZzchJEAAEEEEAAAQQQQAABBApTIGGahZlxco0AAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCGxGgEW2m0HhJQQQQAABBBBAAAEEEEAAgU0FVvexyHZTFV5BAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAgUIVYJFtodYc+UYAAQQQQAABBBBAAAEEciwwuM6T4xRJDgEEEEAAAQQQQAABBBBAAAEEENhYoCOyYuMX+A0BBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQTyLtDePSfveSADCCCAQKYCLLLNVI79EEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQRyKjBOzJymR2IIIIAAAggggAACCCCAAAIIIIAAAggggAACCCAwsMBkPTBwIEIggAACDhVgka1DK4ZsIYAAAggggAACCCCAAAJOE1htVLU6LU/kBwEEEEAAAQQQQAABBBBAAAEEEEAAgWwJrAkvy1ZUxIMAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIFAgAlpwv+92FUheyeZGAqYmpunRTMNnmobHo4n4dE3zqGXT6kdbNuue0OG4KSVa6muzdZUbg7tJ21If+Y7UUBmImKZ41d1GtDTaRDbzreteMYxENqMkLgTWC3h0vySN2Prf+QGBbAp4vUFJJCLZjJK4CkAgYURFnbtJULPrbG0tQonuk7ARLwARd2cxps6Rdd20zuFtg0iqE/iEYZoJ68RNtT5T1+Oi6erkSet/JZOENV+w14xHSjPZl30QGEhA85d0m7Fw+UDheB+BTAQ0f1mnGeupzGTf4t1H9S0l46VlPptPTooXcH3JKksC0hmOrv+dHxDIpkBVaUA6emlf2TQlrs8FqlX7ah+gffUm1JhAUFPXL/Zdu3yeI34qJoFBNSWyZnW4mIrkyLJEY6YZi6tOBo9XYWtbf7yrmfSpQRV1C3mzf1xHF4/67tH0PI3xrANV45ySMGNWz0V/v1kqR5taX5m0xXvWRVFw39eNsfk8QZX3VErsjCJa/ZseVV/+Ir+EqPVVqPbVJVHVJpOK3qP6W3XN64xKIBcpCRhmUkw1xmd3X3xKmflCoHp/ubTEur/wKr8Wu4D1AW2NCaiWKX6vJj6fOvrbcCxtaCiT5ubC/Xws9nZQ6OUbPLhMmppSb1+m+hztUx+mSUONS+le5w2Mm4mg5tN8atJT9qvG+GyALm6oa2mRoDp56p/Yl/2UCjrGuJqDaF3PqKsSGRGs0lqinQVTnrhq3+oyzAzqPnWW6MlbvmOmupJS5z0hrdTWiwpTfX71mWE1sK3OrrTsltdMREx1Wap5Atl52lmit9fUgmom7Qb9WJ5hNZJcuTq9eoolRU8aZklog4jSi6E/dEy184Q6CbDkMth9/S7WuUSfaZhluuqkS3NLqtm7fWr+iE9Vnd+GQ16a2Sm64IOrK6SpnSn4RVexKRbI6t+JqYk5SfWDNdzqy3Ifz9BgtTRG2lPMDcEQ2Fig1+rD84TUi5v/6CgvHy3d3Us33onf8ixgHU/iYpiJ/v5X7xbqLs+ZTCn5oaEaaexL7fwrrM6trfn6epbPM1PKaIaBrL5Ha31Bybad4mWYeu53s/qzouqE1Kojq698S8eV3ORMjSOVDpGe7k/VFZmVMzUHV9VDwC/iVX1ubPkTiCdMMxq1VjimMF6Zv2wOmLJePfpPXs9+P2EC64BUhRHAWkph93IK6zo7nZ5Pr5GUhJ7dzpXCqA135NJqDxmv0MgCkWYYkrR6pNkQsEHAk1AnwV6OXzbQEqUS8CcSqn0xKcdtjWHdJVw651KZGJX1tUskVJ3JruyTYwHrPCoHtwtZN1XWaoLW6OQ2jlBqaoTI5Boyx23FNclp2hrV1UL7ck2F57agav5Ci5rYRfvaDHvfZl7jpfQEhhqfSJO+fXo7ERqBFAVGGItklT42xdAEQyA9gVHGAmnUd9jqTumOCWw1Mt50lcCYqoWyomO8q8qcp8JaU0+sa/6yTNK3Jupaiwjzva0b6Un1thJVbap91RZ2+7LK7AT7dOreamhWf5bd/Zvp5MmOsNWtC2RF3drPR6uenPJ3YkdZiz1OJ7bV6pZ58mn9xGKnp3xbELCOKdaYgF3jArW1c2RJ2+QtpM7LCGybQF3dB/JJ65R0I7HOVa0ZuNaX27Z1Q8OS6jmu24Cs8qrjYv+dgkzjbYnoXyooAivv1m3783zr/v5x4Fy0sc+uO+2oo/6/FeucOxvb5vKpN3ws5tBxmUSvZenWYVo2zkutNpdpfqxzEOv6k/GgTJrB1vcZbLwlS/RpWw/Eu0UvsO5vLNv9PHWNs+STobsUvR8FtEdAfSZute+xrLNXOip5xoI9+tsWq3VMsbPvYNtyl9re9Y3vq+PX1JQCW+UtxP5XK9/ZOMdLCckBgazyWlu2P+vWxpre/9Xq+NWrjl/rL7rV7g64NkuvEMUZ2ro+taolo/FKp5Booq1c196dkifygQACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCBQ5ALDG7hBeZFXMcVDAAEEEEAAAQQQQAABBBBAoCAEWGRbENVEJhFAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQSyKcAi22xqEhcCCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIBAQQiwyLYgqolMIoAAAgjkWsBM9uU6SdJDAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEHCxQGRQxMWlp+gIIIAAAggggEB+BFhkmx93UkUAAQQQcLhAUNMcnkOyhwACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCBQTAKlvmIqDWVBAAEEEEAAAQQKQ4BFtoVRT+QSAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQKBqBN1p3fbVoCkNBHCeQbN7hZcdligwhgAACCCCAAAIIIIAAAgg4UoBFto6sFjKFAAKpCJhGLJVghEEAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQGATARbZbkLCCwggUCgCPtEKJavkEwEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEECgCATeaK9oKYJiUAQEEHChQGvXbBeWmiIjgAACCCAwsACLbAc2IgQCCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAgggUFQCizqNoioPhUEAAQQQQAABBBBAAAEEEEAAAQQQQAABBHImULvj/JylRUIIIIBAFgWmeoJZjI2oEEAAAQQQKB4Bzb/7BZ3FUxxK4ggB0/CJmfRrpqn7NV28IhrPG3VEzRRdJryqfSXMzyeCWT/1iSk+T6joylo0BTJNSRgxMc2kBFX9eRxcMJ/mkbjKZy62uGq3UWXj1f2i6+qo6ZDN7K+viKjjuZSo+mITVVMiEfW/dbzx6gH1AZeZi1cPqb+FPkhtFEgaCdHNuKhasjEVZ0YdUseSPnWsZcutgPWJETONtZ8cmh4XzRNTB4nPT1Rymx3bUtM9wbCRjJTYlgARu1rA4yvtScZ7y1yNQOFtE/D4yrqS8Z4K2xIolIjNREnQY3h97jtFsrWGqoJB6YhEbE2DyN0rUB0KSXsf14/ubQGZldzqv0iq/wx1RWJ9t76sTnqv6nbyejTxfNYpV1tVIm0d4cwSYS8EBhCory6VlvbeAULx9pYEYnGRaNQa7whm3Ae5pbitfl/DTPR/qc568asDhK/A+tDqveXSkujeUhF5HYFtEqj3Vqj21bVNcWRz5/5+RzUukVR/r7rmVeNIHvXdySNsmZXeUGO+1vih1cNqqp+9nx2biq2kg32V0hR37hQd6zzS6tQ2+/99/vPa19Trn51X6urc0hoi060vVVf939XP1jknW/4EhtZVSGOrc45f+ZMgZTsEhtVXysqW3B+/rOva3j519NF8vcU47mZHXRVinIGSoU3RcOPgQsw7eXa+gL9kWFMsvJL25fyqKsgc+ktHrIr1Lh/izMybmjqtt87Q1Zf6ed33/tdM6zS+/3V1+bXu/fXn8/2B1X/95/3Wjirsutc2+u7MghdNrkZUVsvyzvaiKI91TWmo68kNv6zry/4v9Z7VwKxZmFb70tX//e1M/W+9Zn2xZV9ghL9GlsdWbzbihDp4WDMPDVVBVh+Upr7Wfrdqpoi2/jaZUH1uqsTqYOhTDdH/WfsrolLmpSgjArWyPNqWl7RJtHgF1J+sOjap45KvSmKqf9XT31ee/TUX1joXQ80/Ly1Z2+dZvKLOLVk8odZqRFU3te5VA82aVfU52wLlox/1vrT4Z0wwzBm5+xJSp7pxNQDjc1/JKXEuBHSxjp6BXCRFGi4U0KVXta9SF5acIudCIO5JiC+Z/ZP7XOSdNJwv4JF2SUq18zNanDlc15voV8Wzvopw09QMVpNryCKsWWcUSVMjRLQvZ9RF8eVC9U+0qv4Jjl/FV7WOKFGg/BOJdm/viLyQieITCFQukmjn2OIrGCVyhEDJ9osk/AntyxGVUYSZKBmzUMKLxxdhySiSEwQCslCNENG+nFAXxZiHoCxQN9vcoRiLRpkcIBCUeap9TXRATshCMQqU7ThHej6aXIxFo0wOECjb6QPpmTslXzmxxt+4QWe+9HOSrjZHjQ9xgp8Ta/cloqbGf6BuNUX7cl/V56TEqn3NUu2LC8icaLsvkbL6N6SnZU/3FZwS50QgJO+rh1pNzUlaJOI+AdqX++o8lyWmfeVSO69pWffZKM91DjRNW8kNPnKtTnoIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACrhJoqbWeBcOGAAIIIIAAAggggAACCCCAAAII5E9gdiKmnhPIhgACCCCAAAJfFGCR7RdF+B0BBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAwJECbQZzwBxZMUWSqRVGwiiSolAMBwoMrfM4MFdkCQEEEEAAAQQQQAABBBBAwGkCrX2m07JEfopIYLx3j1eLqDgUxWECi8yow3JEdopJ4JOKvmIqDmVxoACLbB1YKWQJAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQGBTgQp9x/c3fZVXEMiOQJ1np/eyExOxIIAAAggggAACCCCAAAIIIIAAApkJlHbv82Jme7IXAgggkF+Bibt685sBUi9qgR3G+Iu6fBQu/wIsss1/HZADBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBGwTWGTO2cm2yIkYAQQQQACBAhZgkW0BVx5ZRwABBBBAAIHCFDATPYWZcXKNAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIFCQAlN2idYUZMbJNAIIIIAAAjYLsMjWZmCiRwABBBBAAAEEvigQ0jgF+6IJvyOAAAIIIIAAAggggAACCCCAQG4FOrqN3CZIaq4SWLTYy13mXFXjFBYBBBBAAAEEEEAAAQQQQAABBBBAAAEEEECgEAR0rRBySR4RQAABBBDIvQArPHJvTooIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAQF4FAq3j5+Y1AyRe1ALDZMzioi4ghUMAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQKBoBFtkWTVVSEAQQQAABBBBAAAEEEEAAAQQQQAABBBBAAIFiEmjslmQxlYeyIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCDgNAEW2TqtRsgPAggggAACCCCAAALbIBAXcxv2ZlcEEEAAAQQQQAABBBBwkkCNMXahk/JDXhBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQKDYBFtkWW41SHgRcJJBgEZGLapuiIoAAAgikKqBrNc2phiUcAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAJuFvBOHXmvm8tP2W0W0HRdTMOwORWid6uAal9h1b5K3Fp+ym23QEIl4LU7EeJ3qUCvETVL9YDm0uJTbJsFNE0T0+RptjYzuzd61b5UA3Nv+Sm5vQK0L3t93R67ro5fBscvtzcDu8q/ItERHV5dFbArfuJ1twDn9+6uf7tLr2mq/96k/95uZ7fGT/tya83nqNxcP+YI2p3JcPxyZ73nqtRaQ7OYvW/kKjnScZlAf/vqedNlpaa4uRLQG5rE6Ho7V8mRjssEOP9yWYXnuLiaaJrJgzxyrO6e5Ghf7qnrfJRUjQ9pzP/Kh7w70tTrV4oxcpY7CriBJDoAAA/SSURBVEspcy6g1zXSvnKu7p4E9TqOX+6p7dyXVFfnX16/zgKi3NO7KEVrkFsttGVDwBYB2pctrES6VsDQvN26KeV4IGCHgN/jXaP68GvsiJs4EegXYAk3DcE2AdW41DAkGwK2CXD8so3W9RFbbYvuCdc3A7sAtvfXLlNxj7crfuJ1uQCLiFzeAGwufn/74gPSZmV3R68WcrMhYIsAn4+2sBLpBgIcvzbA4MesCuge1T/BHJ2smhLZ5wIede5F+/rcg5+yKrC6S6SG9pVVUyL7ggDnX18A4desCfSvsWV8O2ueRLSxAO1rYw9+y7KAmpzD/JwsmxLdegH6J9ZT8IMNAvRP2IBKlOsFPPSvrrfgh+wLqPN7Rrezz0qMCCCAAAJFIKCLxmM8iqAeKQICCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIZC5Q0zritcz3Zk8EEEAAAQQQQAABBBBAAAEEEEAAAQScL8AiW+fXETlEAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQyLIAi2yzDEp0CCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAs4XYJGt8+uIHCKAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAghkWYBFtlkGJToEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAAB5wuwyNb5dUQOEUAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBApYoDdiFHDuyToCCCCAAAIIIIAAAggggAACCBSDwOLZ/mXFUA7KgAACCCCAQLYFWGSbbVHiQwABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEBgA4HSxqFvbfArPyKQVYHEooa3sxohkSGAAAIIIIAAAggggAACCBSlwBh/3dKiLBiFQgABBBBAYBsFWGS7jYDsjgACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAgjkS8CreZL5Spt0EUAAAQQQQAABBBBAAAEEEEAAAQQQQACBQhdgkW2h1yD5RwABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEhbQNvJV92Z9l7sgECKAh7RjKSYLOZO0Ytg6Qn4ND0ZNw1PensRGoHUBEKaJ9FnJr2phSYUAukJlGreeK+Z8KW3F6ERSE2gUvfFOo24P7XQhEIgPYFqjz/anowF0tuL0AikJlDrCUTbklHaV2pchEpToMET7GtORkJp7kZwBFISGKLa1yraV0pWBEpfYLg3FF6R6CtJf0/2QGBggZHe0t5PE72lA4ckBALpC2znLetZkugpS39P9kBgYIEx3rLuxYme8oFDEgKB9AXGe8u7Fia6K9Lfkz0QGFhggq+8a36c9jWwFCEyEdjRV9n5UbyzMpN92QeBgQR28ld2zo3RvgZy4v3MBHb2V3XMjnVUZbY3eyGwdYFdVPuaRfvaOhLvZiywq7+6/b1Ye3XGEbAjAlsR2D1Y3f5OhPa1FSLe2gaB3X2D1rwTXzNoG6LI9a6aSnD9lymi1qaYmnrFes1ap6Kp16yf1cuffVe/9e/Q/58K1P+72sH6bn1b+71/l3U/90fXH67/Zf7LUGC/mmp5eXV7hnuzWzEJqL9LMdV/pvXf+p+tP9N1r699y3q/P0R/2LXvr31BBVR/k1Zw9a1/t6m+6uj7sXY1/14z1r1mvf+FL2sfNgTSFtgzUPu098WyUQwQpU3HDqkKqJONuDrksYgoVTDCpSegaWH10cokw/TUCJ2igDp+darjF5+RKXoRLD0B1b5W077SMyN0GgKaNKtLRo5faZARNHUB1TGxUvVI1KW+ByERSF1Ata9ltK/UvQiZnoA6/2pV518N6e1FaARSFDClSXXs075S5CJYugLaSjUmNDjdvQiPQIoCn4rUDkkxLMEQSEtAnd8vMaVmaFo7ERiBFAVU+1qs2tewFIMTDIG0BNSEu4WGOWh4WjsRGIEUBXRT5hsB2leKXARLU0D1f31kBqpGpLkbwRFISUBNOp9r+mlfKWERKG0BdX4/x/RXjkx7R3ZAIAUB9fn4Ae0rBSiCZCSgizbL8FeMymhndkJgAAH1+fi+WUb7GoCJtzMU6G9fgfLRGe7ObghsVcA3KiHxNdwjc6tIbnpTHXC2sm393c93XB9Ond+/os7vD/j8LX5CIHsCanxoAU8YzZ4nMSGAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAKuEnj93WjMVQWmsAggUFQCLLItquqkMAgggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggEDuBKbppa/lLjVSQgABBLIrwCLb7HoSGwIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggEABCLDItgAqiSwigAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIZFeARbbZ9SQ2BBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAgQIQYJFtAVQSWUQAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBDIrgCLbLPrSWwIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACBSDAItsCqCSyiAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIJBdARbZZteT2BBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQKQMD7fLyrALJJFgtVQBddM8Qo1OyTb4cL6KLphpgOzyXZK1QB3dS9hjqCsSFgh4BHNG+S45cdtMSpBDya7kuaHL9oDPYI+EQLxDl+2YNLrOIVLZigfdESbBLwiB5M0j9hky7R+kwtFNfon6Al2COg2lcJ7cseW2IV8Wt6SYzrR5qCTQLq+rGM60ebcIlWAppeFuX4RUuwSSCgaeVRk/N7m3hdH21QtPII/V+ubwd2AQREr4jS/2UXr+vjDar2FaF9ub4d2AUQEq2ij89Hu3hdH686/6rk/Mv1zcA2ANW+qmhftvG6PuKQ6JV9nH+5vh3YBRDSVfsymF9ol6/b4y0RvSrM8cvtzcC28peIpzIsSdviJ2J3C5SKVu094U13I1B6uwUYgbRb2N3xM4PC3fVvc+k1gzUeNhO7OnpNaF+ubgA2F14zmMNqM7Gro9fMKGPcrm4B9hZeMxmDtFfY5bEbEZcDUHw7BTSTOWB2+hJ3GAIEbBPQjDDn97bpErFu9jCHgmZgn4BqX2wI2CWgmd18PtqFS7xC+6IR2CmgmV0cv+wEdnnctC+XNwC7i6+OX2wI2CWgmZ18PtqFS7zq/L6D9kU7sE2A45dttESsBGhfNAM7Bfh8tFOXuDWD83tagX0Cutmu2xc7MSOAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggg4EwBFtk6s17IFQIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggICNAiyytRGXqBFAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQScKcAiW2fWC7lCAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQsFGARbY24hI1AggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAgDMFWGTrzHohVwgggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAI2CrDI1kZcokYAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBwpgCLbJ1ZL+QKAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBBAwEYBFtnaiEvUCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAs4UYJGtM+uFXCGAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAgjYKKCpuA+zMX6iRsBQBCzmph3YJUD7skuWeC2BpPryQIGATQK0L5tgibZfgPZFQ7BTgPZlpy5xJxSBFwYEbBKgfdkES7T9ArQvGoKdAnEVuc/OBIjb1QK0L1dXv+2Fp33ZTuzqBGhfrq5+2wtP+7Kd2NUJxFTp/a4WoPB2CtC+7NQlbtoXbcBOAdqXnbrETfuiDdgpQPuyU5e4aV+0ATsFaF926hI37Ys2YKcA7ctOXeL++P8BgyyWWopYfcEAAAAASUVORK5CYII="

/***/ }),

/***/ "./src/inTouch.js":
/*!************************!*\
  !*** ./src/inTouch.js ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = isTouch;
function isTouch() {
    return 'ontouchstart' in document.documentElement;
}

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _EasterEgg = __webpack_require__(/*! ./EasterEgg */ "./src/EasterEgg.js");

var _EasterEgg2 = _interopRequireDefault(_EasterEgg);

var _bird = __webpack_require__(/*! ./images/bird.svg */ "./src/images/bird.svg");

var _bird2 = _interopRequireDefault(_bird);

var _world = __webpack_require__(/*! ./images/world.png */ "./src/images/world.png");

var _world2 = _interopRequireDefault(_world);

var _star = __webpack_require__(/*! ./images/star.svg */ "./src/images/star.svg");

var _star2 = _interopRequireDefault(_star);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var uccello = new _EasterEgg2.default({
    canvas: 'canvas', // string for querySelector,
    images: {
        bird: _bird2.default, // from import
        world: _world2.default,
        star: _star2.default
    }
});

uccello.load(function () {
    uccello.play();
});

window.uccello = uccello;

/***/ }),

/***/ "./src/promiseLoader.js":
/*!******************************!*\
  !*** ./src/promiseLoader.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (img) {
    return new Promise(function (resolve) {
        _paper2.default.project.importSVG(img, function (item) {
            resolve(item);
        });
    });
};

var _paper = __webpack_require__(/*! paper */ "./node_modules/paper/dist/paper-full.js");

var _paper2 = _interopRequireDefault(_paper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./src/starSystem.js":
/*!***************************!*\
  !*** ./src/starSystem.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _paper = __webpack_require__(/*! paper */ "./node_modules/paper/dist/paper-full.js");

var _paper2 = _interopRequireDefault(_paper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var StarSystem = function () {
    function StarSystem(_ref) {
        var item = _ref.item,
            bounds = _ref.bounds;

        _classCallCheck(this, StarSystem);

        this.item = item;
        this.item.sendToBack();

        this.star = new _paper2.default.Symbol(this.item);
        this.stars = [];
        this.amount = this.calculateAmount();

        this.init(bounds);
    }

    _createClass(StarSystem, [{
        key: 'init',
        value: function init(bounds) {
            for (var i = 0; i < this.amount; i++) {
                var rand = new _paper2.default.Point.random();
                rand.x *= bounds.size.width;
                rand.y *= bounds.size.height;
                var placed = this.star.place(rand);
                placed.scale(i / this.amount + 0.01);
                placed.rotation = rand.x * 20;
                this.stars.push(placed);
                placed.sendToBack();
            }
        }
    }, {
        key: 'animate',
        value: function animate(_ref2) {
            var bounds = _ref2.bounds,
                speed = _ref2.speed;

            for (var i = 0; i < this.stars.length; i++) {
                var el = this.stars[i];
                el.position.x -= el.scaling.x * speed;
                if (el.position.x < 0) {
                    el.position.x = bounds.size.width;
                }
            }
        }
    }, {
        key: 'calculateAmount',
        value: function calculateAmount() {
            return window.innerWidth * 0.02;
        }
    }]);

    return StarSystem;
}();

exports.default = StarSystem;

/***/ }),

/***/ "./src/tail.js":
/*!*********************!*\
  !*** ./src/tail.js ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _paper = __webpack_require__(/*! paper */ "./node_modules/paper/dist/paper-full.js");

var _paper2 = _interopRequireDefault(_paper);

var _inTouch = __webpack_require__(/*! ./inTouch */ "./src/inTouch.js");

var _inTouch2 = _interopRequireDefault(_inTouch);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var colors = ['black', '#17DD58', 'black', '#55FEED', 'black', '#0B7AF4', 'black', '#C811F4', 'black', '#FF3194', 'black', '#ED1130', 'black', '#FFD522', 'black'];
var tempPoint = new _paper2.default.Point();

var Tail = function () {
    function Tail() {
        _classCallCheck(this, Tail);

        this.coloredStrokeWidth = undefined;
        this.blackStrokeWidth = undefined;
        this.speed = this.setSpeed();

        this.tailLength = this.getMaxLength();
        this.tails = [];
        this.init();
        this.totalHeight = this.getTotalHeight();
    }

    _createClass(Tail, [{
        key: 'init',
        value: function init() {
            this.setStrokeWidth();
            this.tail = this.initPath();
            for (var i = 0; i < colors.length; i++) {
                var stroke = void 0;
                i % 2 === 0 ? stroke = this.blackStrokeWidth : stroke = this.coloredStrokeWidth;

                if (i === 0) {
                    this.tails.push(this.tail);
                } else {
                    var cloned = this.tail.clone();
                    cloned.strokeColor = colors[i];
                    cloned.strokeWidth = stroke;
                    this.tails.push(cloned);
                }
            }
            this.group = new _paper2.default.Group(this.tails);
        }
    }, {
        key: 'initPath',
        value: function initPath() {
            var tail = new _paper2.default.Path({
                strokeColor: colors[0],
                strokeWidth: this.blackStrokeWidth
            });

            return tail;
        }
    }, {
        key: 'setSpeed',
        value: function setSpeed() {
            return (0, _inTouch2.default)() ? Math.floor(window.innerWidth * 0.018) : Math.floor(window.innerWidth * 0.0055);
        }
    }, {
        key: 'getTotalHeight',
        value: function getTotalHeight() {
            var height = 0;
            for (var i = 0; i < this.tails.length; i++) {
                height += this.tails[i].strokeWidth;
            }

            return height;
        }
    }, {
        key: 'getMaxLength',
        value: function getMaxLength() {
            var length = window.innerWidth / this.speed * 0.8;

            return length;
        }
    }, {
        key: 'animate',
        value: function animate(bird) {
            // let progress = -Math.floor(this.tails.length * 0.5)
            var progress = 0;
            for (var i = 0; i < this.tails.length; i++) {
                var tail = this.tails[i];
                tempPoint = bird.position.clone();

                if (i > 0) {
                    progress += (this.tails[i - 1].strokeWidth + this.tails[i].strokeWidth) * 0.5;
                }
                // i === 0 ? (progress = 0) : (progress += this.tails[i - 1].strokeWidth)
                tempPoint.x = bird.position.x + bird.bounds.width * 0.1;
                tempPoint.y = bird.position.y + progress - this.totalHeight * 0.5;

                tail.add(tempPoint);
                tail.position.x -= this.speed;

                if (tail.segments.length > this.tailLength) {
                    tail.segments[0].remove();
                    tail.segments.unshift();
                }

                tail.smooth();
            }
        }
    }, {
        key: 'setStrokeWidth',
        value: function setStrokeWidth() {
            if (_inTouch2.default) {
                this.coloredStrokeWidth = 12;
                this.blackStrokeWidth = 0.5;
            } else {
                this.coloredStrokeWidth = 14;
                this.blackStrokeWidth = 1;
            }
        }
    }]);

    return Tail;
}();

exports.default = Tail;

/***/ }),

/***/ "./src/world.js":
/*!**********************!*\
  !*** ./src/world.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _paper = __webpack_require__(/*! paper */ "./node_modules/paper/dist/paper-full.js");

var _paper2 = _interopRequireDefault(_paper);

var _inTouch = __webpack_require__(/*! ./inTouch */ "./src/inTouch.js");

var _inTouch2 = _interopRequireDefault(_inTouch);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var World = function () {
    function World(_ref) {
        var item = _ref.item,
            bounds = _ref.bounds;

        _classCallCheck(this, World);

        this.slice = item;
        (0, _inTouch2.default)() ? this.slice.scale(0.6) : '';

        this.slices = [item, item.clone()];
        this.viewBounds = bounds;
    }

    _createClass(World, [{
        key: 'resize',
        value: function resize(bounds) {
            this.bounds = bounds;

            this.setPosition(bounds);

            // this.slices[1].scale(0.6)
        }
    }, {
        key: 'setPosition',
        value: function setPosition(bounds) {
            this.disposeHorizzontally();
            for (var i = 0; i < this.slices.length; i++) {
                this.slices[i].position.y = bounds.height - this.slices[0].bounds.height * 0.5;
            }
        }
    }, {
        key: 'disposeHorizzontally',
        value: function disposeHorizzontally() {
            this.slices[1].position.x = this.slices[0].position.x + this.slices[0].bounds.size.width * 1;
        }
    }, {
        key: 'animate',
        value: function animate(speed) {
            if (this.slices[0].bounds.right < 0) {
                var _ref2 = [this.slices[0], this.slices[1]];
                this.slices[1] = _ref2[0];
                this.slices[0] = _ref2[1];

                this.disposeHorizzontally();
            }
            for (var i = 0; i < this.slices.length; i++) {
                this.slices[i].position.x -= speed;
            }
        }
    }]);

    return World;
}();

exports.default = World;

/***/ }),

/***/ 0:
/*!********************************!*\
  !*** ./node/self.js (ignored) ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 1:
/*!**********************************!*\
  !*** ./node/extend.js (ignored) ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

/******/ });
//# sourceMappingURL=app.bundle.js.map