import UccelloDiCadamuro from './EasterEgg'
import bird from './images/bird.svg'
import world from './images/world.png'
import star from './images/star.svg'

const uccello = new UccelloDiCadamuro({
    canvas: 'canvas', // string for querySelector,
    images: {
        bird, // from import
        world,
        star
    }
})

uccello.load(() => {
    uccello.play()
})

window.uccello = uccello
