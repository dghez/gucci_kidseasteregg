import paper from 'paper'
import isTouch from './inTouch'

const colors = ['black', '#17DD58', 'black', '#55FEED', 'black', '#0B7AF4', 'black', '#C811F4', 'black', '#FF3194', 'black', '#ED1130', 'black', '#FFD522', 'black']
let tempPoint = new paper.Point()
class Tail {
    constructor() {
        this.coloredStrokeWidth = undefined
        this.blackStrokeWidth = undefined
        this.speed = this.setSpeed()

        this.tailLength = this.getMaxLength()
        this.tails = []
        this.init()
        this.totalHeight = this.getTotalHeight()
    }

    init() {
        this.setStrokeWidth()
        this.tail = this.initPath()
        for (let i = 0; i < colors.length; i++) {
            let stroke
            i % 2 === 0 ? (stroke = this.blackStrokeWidth) : (stroke = this.coloredStrokeWidth)

            if (i === 0) {
                this.tails.push(this.tail)
            } else {
                var cloned = this.tail.clone()
                cloned.strokeColor = colors[i]
                cloned.strokeWidth = stroke
                this.tails.push(cloned)
            }
        }
        this.group = new paper.Group(this.tails)
    }

    initPath() {
        var tail = new paper.Path({
            strokeColor: colors[0],
            strokeWidth: this.blackStrokeWidth
        })

        return tail
    }

    setSpeed() {
        return isTouch() ? Math.floor(window.innerWidth * 0.018) : Math.floor(window.innerWidth * 0.0055)
    }

    getTotalHeight() {
        let height = 0
        for (let i = 0; i < this.tails.length; i++) {
            height += this.tails[i].strokeWidth
        }

        return height
    }

    getMaxLength() {
        let length = (window.innerWidth / this.speed) * 0.8

        return length
    }

    animate(bird) {
        // let progress = -Math.floor(this.tails.length * 0.5)
        let progress = 0
        for (let i = 0; i < this.tails.length; i++) {
            var tail = this.tails[i]
            tempPoint = bird.position.clone()

            if (i > 0) {
                progress += (this.tails[i - 1].strokeWidth + this.tails[i].strokeWidth) * 0.5
            }
            // i === 0 ? (progress = 0) : (progress += this.tails[i - 1].strokeWidth)
            tempPoint.x = bird.position.x + bird.bounds.width * 0.1
            tempPoint.y = bird.position.y + progress - this.totalHeight * 0.5

            tail.add(tempPoint)
            tail.position.x -= this.speed

            if (tail.segments.length > this.tailLength) {
                tail.segments[0].remove()
                tail.segments.unshift()
            }

            tail.smooth()
        }
    }

    setStrokeWidth() {
        if (isTouch) {
            this.coloredStrokeWidth = 12
            this.blackStrokeWidth = 0.5
        } else {
            this.coloredStrokeWidth = 14
            this.blackStrokeWidth = 1
        }
    }
}

export default Tail
