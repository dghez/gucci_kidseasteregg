import paper from 'paper'
import isTouch from './inTouch'

class World {
    constructor({ item, bounds }) {
        this.slice = item
        isTouch() ? this.slice.scale(0.6) : ''

        this.slices = [item, item.clone()]
        this.viewBounds = bounds
    }

    resize(bounds) {
        this.bounds = bounds

        this.setPosition(bounds)

        // this.slices[1].scale(0.6)
    }

    setPosition(bounds) {
        this.disposeHorizzontally()
        for (var i = 0; i < this.slices.length; i++) {
            this.slices[i].position.y = bounds.height - this.slices[0].bounds.height * 0.5
        }
    }

    disposeHorizzontally() {
        this.slices[1].position.x = this.slices[0].position.x + this.slices[0].bounds.size.width * 1
    }

    animate(speed) {
        if (this.slices[0].bounds.right < 0) {
            [this.slices[1], this.slices[0]] = [this.slices[0], this.slices[1]]
            this.disposeHorizzontally()
        }
        for (var i = 0; i < this.slices.length; i++) {
            this.slices[i].position.x -= speed
        }
    }
}

export default World
