import paper from 'paper'

class StarSystem {
    constructor({ item, bounds }) {
        this.item = item
        this.item.sendToBack()

        this.star = new paper.Symbol(this.item)
        this.stars = []
        this.amount = this.calculateAmount()

        this.init(bounds)
    }

    init(bounds) {
        for (let i = 0; i < this.amount; i++) {
            let rand = new paper.Point.random()
            rand.x *= bounds.size.width
            rand.y *= bounds.size.height
            let placed = this.star.place(rand)
            placed.scale(i / this.amount + 0.01)
            placed.rotation = rand.x * 20
            this.stars.push(placed)
            placed.sendToBack()
        }
    }

    animate({ bounds, speed }) {
        for (let i = 0; i < this.stars.length; i++) {
            var el = this.stars[i]
            el.position.x -= el.scaling.x * speed
            if (el.position.x < 0) {
                el.position.x = bounds.size.width
            }
        }
    }

    calculateAmount() {
        return window.innerWidth * 0.02
    }
}

export default StarSystem
