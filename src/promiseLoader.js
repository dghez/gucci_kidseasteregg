import paper from 'paper'

export default function(img) {
    return new Promise(function(resolve) {
        paper.project.importSVG(img, item => {
            resolve(item)
        })
    })
}
