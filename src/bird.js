import paper from 'paper'
import Tail from './tail'
import isTouch from './inTouch'

let tempPoint = new paper.Point()
let limitDelta = 10
let deltaMovementX

class Bird {
    constructor({ item, bounds, xOffset }) {
        this.item = item
        isTouch() ? this.item.scale(0.78) : ''
        this.item.position.x = xOffset * bounds.size.width * 0.01
        this.currentSide = 'right'
        this.mousePos = new paper.Point()
        this.sinFactor = isTouch() ? 3.5 : 2
        this.easeFactor = isTouch() ? 0.6 : 0.07
        this.tail = new Tail()
        this.tail.group.insertBelow(this.item)
    }

    animate(time) {
        deltaMovementX = (this.mousePos.x - this.item.position.x) * this.easeFactor
        this.item.position.y += (this.mousePos.y - this.item.position.y) * this.easeFactor + Math.sin(time * 4) * this.sinFactor
        this.item.position.x += deltaMovementX
        this.tail.animate(this.item, time)

        this.handleFlipSide()
    }

    handleFlipSide() {
        if (Math.abs(deltaMovementX) < 2 && this.currentSide === 'left') {
            this.rotate('right')
        }
    }

    rotate(side) {
        this.currentSide = side
        this.item.scale(-1, 1)
    }

    onMouseMove(evt) {
        this.mousePos = evt.point.clone()
        let delta = this.mousePos.x - tempPoint.x

        if (delta > limitDelta || delta < -limitDelta) {
            if (delta > 0) {
                // move mouse right
                if (this.currentSide != 'right') {
                    this.rotate('right')
                }
            } else {
                // move mouse left
                if (this.currentSide != 'left') {
                    this.rotate('left')
                }
            }
        }

        tempPoint = this.mousePos.clone()
    }
}

export default Bird
