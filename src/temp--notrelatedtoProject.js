import paper from 'paper'

const colors = ['black', '#17DD58', 'black', '#55FEED', 'black', '#0B7AF4', 'black', '#C811F4', 'black', '#FF3194', 'black', '#ED1130', 'black', '#FFD522', 'black']
let tempPoint = new paper.Point()
class Tail {
    constructor() {
        this.coloredStrokeWidth = 14
        this.blackStrokeWidth = 1
        this.tails = []
        this.tailsBlack = []
        this.tailsColored = []
        this.init()
        this.totalHeight = this.getTotalHeight()
    }

    init() {
        this.tail = this.initPath()

        for (let i = 0; i < colors.length; i += 2) {
            if (i % 2 === 0) {
                var cloned = this.tail.clone()
                cloned.strokeColor = 'black'
                cloned.strokeWidth = this.blackStrokeWidth
                this.tailsBlack.push(cloned)
            }
        }
        for (let i = 0; i < colors.length; i++) {
            if (i % 2 != 0) {
                var cloned = this.tail.clone()
                cloned.strokeColor = 'red'
                cloned.strokeWidth = this.coloredStrokeWidth
                this.tailsColored.push(cloned)
            }
        }

        console.log(this.tailsBlack)
        console.log(this.tailsColored)

        let progressB = 0,
            progressC = 0
        for (let i = 0; i < this.tailsBlack.length + this.tailsColored.length; i++) {
            if (i % 2 === 0) {
                console.log('blakc')

                this.tails.push(this.tailsBlack[progressB])
                progressB++
            } else {
                console.log('col')

                this.tails.push(this.tailsColored[progressC])
                progressC++
            }
        }

        console.log(this.tails)

        // for (let i = 0; i < this.tail.length; i++) {
        //     if (i % 2 != 0) {
        //         this.tailsBlack.push(this.tails[i])
        //         // this.tails[i].insertAbove(this.tails[14])
        //     }
        // }

        this.group = new paper.Group(this.tailsBlack)
    }

    initPath() {
        var tail = new paper.Path({
            strokeColor: colors[0],
            strokeWidth: this.blackStrokeWidth
        })

        for (let i = 0; i < 100; i++) {
            tail.add(new paper.Point(400 + i * -15, 0))
        }

        return tail
    }

    getTotalHeight() {
        let height = 0
        // for (let i = 0; i < this.tails.length; i++) {
        //     height += this.tails[i].strokeWidth
        // }

        return height
    }

    animate(bird) {
        // let progress = -Math.floor(this.tails.length * 0.5)
        let progress = 0
        for (let i = 0; i < this.tails.length; i++) {
            var tail = this.tails[i]
            tempPoint = bird.position.clone()

            if (i > 0) {
                progress += (this.tails[i - 1].strokeWidth + this.tails[i].strokeWidth) * 0.5
            }
            // i === 0 ? (progress = 0) : (progress += this.tails[i - 1].strokeWidth)
            tempPoint.y = bird.position.y + progress - this.totalHeight * 0.5

            tail.add(tempPoint)
            tail.position.x -= 10

            if (tail.segments.length > 3) {
                tail.segments[0].remove()
                tail.segments.unshift()
            }

            tail.smooth()
        }
        // this.group.position.x = bird.position.x + bird.bounds.width * 0.2 - this.group.bounds.width * 0.5
    }
}

export default Tail
