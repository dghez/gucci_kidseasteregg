import paper, { Raster } from 'paper'
import World from './world'
import Bird from './bird'
import StarSystem from './starSystem'
import promiseLoader from './promiseLoader'
import isTouch from './inTouch'

const settings = {
    bird: {
        init: {
            position: {
                x: 20 // % of the screen
            }
        }
    },
    world: {
        speed: isTouch() ? 0.015 * window.innerWidth : 0.008 * window.innerWidth
    },
    stars: {
        speed: isTouch() ? 0.013 * window.innerWidth : 0.008 * window.innerWidth
    }
}

class App {
    constructor(options) {
        this.images = options.images
        this.init(options)

        // this.debug()

        window.addEventListener('resize', () => {
            this.resize()
        })

        window.addEventListener('orientationchange', () => {
            setTimeout(() => {
                this.resize()
            }, 400)
        })
    }

    init({ canvas, images }) {
        // set canvas
        this.canvas = document.querySelector(`${canvas}`)

        // init paper project
        paper.setup(this.canvas)
        this.setSizeCanvas()
        this.project = paper.project
        this.viewBounds = paper.view.bounds

        // load all assets
        // this.load()
    }

    load(callback) {
        const worldPromise = new Promise(resolve => {
            let img = new Raster(this.images.world)
            img.onLoad = function() {
                resolve(img)
            }
        })

        Promise.all([promiseLoader(this.images.bird), worldPromise, promiseLoader(this.images.star)]).then(value => {
            [this.birdItem, this.worldItem, this.starItem] = value
            this.ready()
            this.resize()
            callback()
        })
    }

    ready() {
        this.birdItem.insertBelow(this.worldItem)
        this.world = new World({ item: this.worldItem, bounds: this.viewBounds })
        this.bird = new Bird({ item: this.birdItem, bounds: this.viewBounds, xOffset: settings.bird.init.position.x })
        this.starSystem = new StarSystem({ item: this.starItem, bounds: this.viewBounds, layer: this.project.activeLayer, speed: settings.stars.speed })

        this.onMouseMove()
        // this.animate()
    }

    play() {
        this.animate()
    }

    pause() {
        paper.view.pause()
    }

    animate() {
        paper.view.onFrame = event => {
            this.world.animate(settings.world.speed)
            this.bird.animate(event.time)
            this.starSystem.animate({ bounds: this.viewBounds, time: event.time, speed: settings.stars.speed })
        }
    }

    resize() {
        this.setSizeCanvas()
        this.viewBounds = paper.view.bounds
        this.world.resize(this.viewBounds)
    }

    setSizeCanvas() {
        paper.view.viewSize = new paper.Size(window.innerWidth, window.innerHeight)
    }

    debug() {
        paper.view.onKeyDown = event => {
            if (event.key == 'space') this.project.activeLayer.selected = !this.project.activeLayer.selected
        }
    }

    onMouseMove() {
        paper.view.onMouseMove = evt => {
            this.bird.onMouseMove(evt)
        }
    }
}

export default App
